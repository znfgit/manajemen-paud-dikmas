<?php

		include('../mpdf.php');

		$mpdf=new \mPDF('c');
		$mpdf->SetTitle('Kartu Sekolah '."nama variabel");
		$mpdf->mirrorMargins = 1;	// Use different Odd/Even headers and footers and mirror margins

		$footer = '<table><tr>
				<td align="left"><span><i>Kartu Sekolah '."nama tabel".'</i></span></td>
				<td align="right" width="100%">{PAGENO}</td>
			</tr></table>';

		$mpdf->SetHTMLFooter($footer);
		$mpdf->SetHTMLFooter($footer,'E');

		ob_start();
		include ('template.php');
		// include dirname(__FILE__).D.'Templates'.D.'kartusekolah.php';
		$content = ob_get_clean();

		$stylesheet = file_get_contents('style.css');
		// $mpdf->imgdisdik = file_get_contents(dirname(__FILE__).D.'Templates'.D.'disdik.png');
		$mpdf->WriteHTML($stylesheet,1);
		$mpdf->WriteHTML($content);
		$mpdf->Output('Kartu Sekolah '."nama pariabel".'.pdf');
		// $mpdf->Output('Kartu Sekolah '.$sekolah->getNama().'.pdf');
		// exit;
	// }

?>