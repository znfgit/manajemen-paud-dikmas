<body>
<h2 align="center">Rekap Sekolah per Wilayah</h2>
<table border="1" cellspacing="0" cellpadding="0">
  <col width="258" />
  <col width="128" />
  <col width="134" />
  <col width="170" />
  <col width="92" />
  <col width="122" />
  <col width="83" />
  <col width="120" />
  <col width="131" />
  <col width="125" />
  <col width="130" />
  <col width="108" />
  <col width="134" />
  <tr>
    <td width="195" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">Satuan Pendidikan</td>
    <td width="84" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">NPSN</td>
    <td width="148" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">Kecamatan</td>
    <td width="148" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">Kabupaten</td>
    <td colspan="3" align="center" valign="middle" bgcolor="#0099CC">PTK</td>
    <td colspan="5" align="center" valign="middle" bgcolor="#0099CC">Peserta Didik</td>
    <td width="78" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">Rombel</td>
  </tr>
  <tr>
    <td width="47" align="center" valign="middle" bgcolor="#0099CC">Guru</td>
    <td width="64" align="center" valign="middle" bgcolor="#0099CC">Pegawai</td>
    <td width="46" align="center" valign="middle" bgcolor="#0099CC">Total</td>
    <td width="73" align="center" valign="middle" bgcolor="#0099CC">Kelas 10</td>
    <td width="56" align="center" valign="middle" bgcolor="#0099CC">Kelas 11</td>
    <td width="63" align="center" valign="middle" bgcolor="#0099CC"> Kelas 12 </td>
    <td width="61" align="center" valign="middle" bgcolor="#0099CC">Kelas 13</td>
    <td width="50" align="center" valign="middle" bgcolor="#0099CC">Total</td>
  </tr>
  <?php
  foreach ($return as $r) {
  ?>
  <tr>
    <td><?php echo $r['desk']?></td>
    <td><?php echo $r['npsn']?></td>
    <td><?php echo $r['kecamatan']?></td>
    <td><?php echo $r['kabupaten']?></td>
    <td align="right"><?php echo $r['ptk']?></td>
    <td align="right"><?php echo $r['pegawai']?></td>
    <td align="right"><?php echo $r['ptk_total']?></td>
    <td align="right"><?php echo $r['pd_kelas_10']?></td>
    <td align="right"><?php echo $r['pd_kelas_11']?></td>
    <td align="right"><?php echo $r['pd_kelas_12']?></td>
    <td align="right"><?php echo $r['pd_kelas_13']?></td>
    <td align="right"><?php echo $r['pd']?></td>
    <td align="right"><?php echo $r['rombel']?></td></td>
  </tr>
  <?php 

    $total_ptk = $total_ptk + $r['ptk'];
    $total_pegawai = $total_pegawai + $r['pegawai'];
    $total_ptk_total = $total_ptk_total + $r['ptk_total'];
    $total_pd_kelas_10 = $total_pd_kelas_10 + $r['pd_kelas_10'];
    $total_pd_kelas_11 = $total_pd_kelas_11 + $r['pd_kelas_11'];
    $total_pd_kelas_12 = $total_pd_kelas_12 + $r['pd_kelas_12'];
    $total_pd_kelas_13 = $total_pd_kelas_13 + $r['pd_kelas_13'];
    $total_pd = $total_pd + $r['pd'];
    $total_rombel = $total_rombel + $r['rombel'];

  }
  ?>

  <tr>
    <td colspan="4">Total</td>
    <td align="right"><?php echo $total_ptk; ?></td>
    <td align="right"><?php echo $total_pegawai; ?></td>
    <td align="right"><?php echo $total_ptk_total; ?></td>
    <td align="right"><?php echo $total_pd_kelas_10 ?></td>
    <td align="right"><?php echo $total_pd_kelas_11 ?></td>
    <td align="right"><?php echo $total_pd_kelas_12 ?></td>
    <td align="right"><?php echo $total_pd_kelas_13 ?></td>
    <td align="right"><?php echo $total_pd ?></td>
    <td align="right"><?php echo $total_rombel ?></td></td>
  </tr>
  
</table>
</body>