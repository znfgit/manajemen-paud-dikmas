<?php
namespace wajar_dikdas;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use wajar_dikdas\Model\SekolahPeer;

class Sekolah
{
	public function jumlahPerstatus($chart, $where, $request){
		$sql = "";
		$bentukPendidikanId = ($request->get('bentuk')) ? $request->get('bentuk') : 5;
		if($chart == "pie"){
			$sql = "select 
						status_sekolah as ket, 
						count(*) as jumlah 
					FROM 
						sekolah 
					WHERE 
						bentuk_pendidikan_id = {$bentukPendidikanId}
						{$where}
					GROUP BY 
						status_sekolah 
					ORDER BY 
						status_sekolah";
		}else if($chart == "bar"){
			$cakupan = getCakupan($request->get('wilayah'));
			$sql = "SELECT
						{$cakupan} as ket,
						status_sekolah as tipe,
						count(*) as jumlah
					FROM
						sekolah
					WHERE
						bentuk_pendidikan_id = {$bentukPendidikanId}
						{$where}
					GROUP BY
						{$cakupan},
						status_sekolah
					ORDER BY
						status_sekolah,{$cakupan} DESC";
		}
		return $sql;
	}
	
	public function jumlahPerAkreditasi($chart, $where, $request){
		$sql = "";
		$bentukPendidikanId = ($request->get('bentuk')) ? $request->get('bentuk') : 5;
		if($chart == "pie"){
			$sql = "select
					isnull(nama_akreditasi,'Belum Terisi') as ket,
					count(*) as jumlah
				FROM
					sekolah
				WHERE
					bentuk_pendidikan_id = {$bentukPendidikanId}
					{$where}
				GROUP BY
					nama_akreditasi
				ORDER BY
					nama_akreditasi";
		}else if($chart == "bar"){
		$cakupan = getCakupan($request->get('wilayah'));
		$sql = "SELECT
					{$cakupan} as ket,
					isnull(nama_akreditasi,'Belum Terisi') as tipe,
					count(*) as jumlah
				FROM
					sekolah
				WHERE
					bentuk_pendidikan_id = {$bentukPendidikanId}
					{$where}
				GROUP BY
					{$cakupan},
					nama_akreditasi
				ORDER BY
					nama_akreditasi";
		}
		return $sql;
	}
	
	public function jumlahSekolahPerPrasarana($chart, $where, $request){
		$bentukPendidikanId = ($request->get('bentuk')) ? $request->get('bentuk') : 5;
		$sql = "SELECT
					'Jumlah Sekolah' as ket,
					count(sekolah_id) as jumlah
				FROM
					sekolah
				where
					bentuk_pendidikan_id = {$bentukPendidikanId}
					{$where}
				UNION
				select
					ket,
					count(jumlah) as jumlah
				from 
					(select 
						j_r_kepsek as \"Sekolah Memiliki Ruang Kepsek\", j_r_guru as \"Sekolah Memiliki Ruang Guru\", j_lab_ipa as \"Sekolah Memiliki Lab. IPA\", j_r_perpustakaan \"Sekolah Memiliki Ruang Perpustakaan\"
					FROM
						rekap_prasarana
					where
					bentuk_pendidikan_id = {$bentukPendidikanId}
					and semester_id = ".SMT_BERJALAN."
					{$where}
				) tbl
				unpivot
				(
				  jumlah
				  for ket in (\"Sekolah Memiliki Ruang Kepsek\", \"Sekolah Memiliki Ruang Guru\", \"Sekolah Memiliki Lab. IPA\", \"Sekolah Memiliki Ruang Perpustakaan\")
				) unpiv
				GROUP BY ket;";
					
		return $sql;
	}

	public function getSekolahPerStatus($wilayahId, $request){
		$sql = "";
		$bentukPendidikanId = ($request->get('bentuk')) ? $request->get('bentuk') : 5;
		$cakupan = getCakupan($request->get('wilayah'));
		$semesterId = ($_REQUEST["semester_id"]) ? $_REQUEST["semester_id"] : SMT_BERJALAN;

		if($request->get('wilayah') == 2){
	        $where = "AND provinsi_id = ".$wilayahId;
	    }else if($request->get('wilayah') == 3){
	        $where = "AND kabupaten_kota_id = ".$wilayahId;
	    }

		$sql = "select
					{$cakupan} as wilayah,
					sum(ISNULL(j_negeri, 0)) as negeri,
					sum(ISNULL(j_swasta, 0)) as swasta,
					sum(ISNULL(j_negeri, 0)+ISNULL(j_swasta, 0)) as total
				from 
					rekap_sekolah
				where 
					bentuk_pendidikan_id = {$bentukPendidikanId}
					and semester_id = ".$semesterId."
					{$where}
				GROUP BY
					{$cakupan}
				ORDER BY
					{$cakupan}";

		$data = getDataBySql($sql);

		return $data;

	}

	public function getSekolahPerWaktuPenyelenggaraan($wilayahId, $request){
		$sql = "";
		$bentukPendidikanId = ($request->get('bentuk')) ? $request->get('bentuk') : 5;
		$cakupan = getCakupan($request->get('wilayah'));
		$semesterId = ($_REQUEST["semester_id"]) ? $_REQUEST["semester_id"] : SMT_BERJALAN;
		
		if($request->get('wilayah') == 2){
	        $where = "AND provinsi_id = ".$wilayahId;
	    }else if($request->get('wilayah') == 3){
	        $where = "AND kabupaten_kota_id = ".$wilayahId;
	    }
	    
	    $sql = "select
					{$cakupan} wilayah,
					sum(ISNULL(j_waktu_pagi, 0)) pagi,
					sum(ISNULL(j_waktu_siang, 0)) siang,
					sum(ISNULL(j_waktu_kombinasi, 0)) kombinasi,
					sum(ISNULL(j_waktu_sore, 0)) sore,
					sum(ISNULL(j_waktu_malam, 0)) malam,
					sum(ISNULL(j_waktu_lainnya, 0)) lainnya,
					sum(ISNULL(j_waktu_pagi, 0)+ISNULL(j_waktu_siang, 0)+ISNULL(j_waktu_kombinasi, 0)+ISNULL(j_waktu_sore, 0)+ISNULL(j_waktu_malam, 0)+ISNULL(j_waktu_lainnya, 0)) total
				from 
					rekap_sekolah
				where 
					bentuk_pendidikan_id = {$bentukPendidikanId}
					and semester_id = ".$semesterId."
					{$where}
				GROUP BY
					{$cakupan}
				ORDER BY
					{$cakupan}";

		$data = getDataBySql($sql);

		return $data;

	}

	public function getSekolahPerAkreditasi($wilayahId, $request){
		$sql = "";
		$bentukPendidikanId = ($request->get('bentuk')) ? $request->get('bentuk') : 5;
		$cakupan = getCakupan($request->get('wilayah'));
		$semesterId = ($_REQUEST["semester_id"]) ? $_REQUEST["semester_id"] : SMT_BERJALAN;

		if($request->get('wilayah') == 2){
	        $where = "AND provinsi_id = ".$wilayahId;
	    }else if($request->get('wilayah') == 3){
	        $where = "AND kabupaten_kota_id = ".$wilayahId;
	    }

		$sql = "select
					{$cakupan} wilayah,
					sum(ISNULL(j_akreditasi_a, 0)) A,
					sum(ISNULL(j_akreditasi_b, 0)) B,
					sum(ISNULL(j_akreditasi_c, 0)) C,
					sum(ISNULL(j_akreditasi_belum, 0)) belum,
					sum(ISNULL(j_akreditasi_tidak, 0)) tidak,
					sum(ISNULL(j_akreditasi_a, 0)+ISNULL(j_akreditasi_b, 0)+ISNULL(j_akreditasi_c, 0)+ISNULL(j_akreditasi_belum, 0)+ISNULL(j_akreditasi_tidak, 0)) total
				from 
					rekap_sekolah
				where 
					bentuk_pendidikan_id = {$bentukPendidikanId}
					and semester_id = ".$semesterId."
					{$where}
				GROUP BY
					{$cakupan}
				ORDER BY
					{$cakupan}";

		$data = getDataBySql($sql);

		return $data;

	}

	public function getSekolahPerSumberListrik($wilayahId, $request){
		$sql = "";
		$bentukPendidikanId = ($request->get('bentuk')) ? $request->get('bentuk') : 5;
		$cakupan = getCakupan($request->get('wilayah'));
		$semesterId = ($_REQUEST["semester_id"]) ? $_REQUEST["semester_id"] : SMT_BERJALAN;

		if($request->get('wilayah') == 2){
	        $where = "AND provinsi_id = ".$wilayahId;
	    }else if($request->get('wilayah') == 3){
	        $where = "AND kabupaten_kota_id = ".$wilayahId;
	    }

		$sql = "select
					{$cakupan} wilayah,
					sum(ISNULL(j_listrik_ada, 0)) ada,
					sum(ISNULL(j_listrik_tidak, 0)) tidak_ada,
					sum(ISNULL(j_listrik_ada, 0)+ISNULL(j_listrik_tidak, 0)) total
				from 
					rekap_sekolah
				where 
					bentuk_pendidikan_id = {$bentukPendidikanId}
					and semester_id = ".$semesterId."
					{$where}
				GROUP BY
					{$cakupan}
				ORDER BY
					{$cakupan}";

		$data = getDataBySql($sql);

		return $data;
	}

	public function getSekolahPerSanitasi($wilayahId, $request){
		$sql = "";
		$bentukPendidikanId = ($request->get('bentuk')) ? $request->get('bentuk') : 5;
		$cakupan = getCakupan($request->get('wilayah'));
		$semesterId = ($_REQUEST["semester_id"]) ? $_REQUEST["semester_id"] : SMT_BERJALAN;

		if($request->get('wilayah') == 2){
	        $where = "AND provinsi_id = ".$wilayahId;
	    }else if($request->get('wilayah') == 3){
	        $where = "AND kabupaten_kota_id = ".$wilayahId;
	    }

		$sql = "select
					{$cakupan} wilayah,
					sum(ISNULL(j_sanitasi_ada, 0)) ada,
					sum(ISNULL(j_sanitasi_tidak, 0)) tidak_ada,
					sum(ISNULL(j_sanitasi_ada, 0)+ISNULL(j_sanitasi_tidak, 0)) total
				from 
					rekap_sekolah
				where 
					bentuk_pendidikan_id = {$bentukPendidikanId}
					and semester_id = ".$semesterId."
					{$where}
				GROUP BY
					{$cakupan}
				ORDER BY
					{$cakupan}";

		$data = getDataBySql($sql);

		return $data;
	}

	public function getSekolahPerAksesInternet($wilayahId, $request){
		$sql = "";
		$bentukPendidikanId = ($request->get('bentuk')) ? $request->get('bentuk') : 5;
		$cakupan = getCakupan($request->get('wilayah'));
		$semesterId = ($_REQUEST["semester_id"]) ? $_REQUEST["semester_id"] : SMT_BERJALAN;

		if($request->get('wilayah') == 2){
	        $where = "AND provinsi_id = ".$wilayahId;
	    }else if($request->get('wilayah') == 3){
	        $where = "AND kabupaten_kota_id = ".$wilayahId;
	    }

		$sql = "select
					{$cakupan} wilayah,
					sum(ISNULL(j_internet_ada, 0)) ada,
					sum(ISNULL(j_internet_tidak, 0)) tidak_ada,
					sum(ISNULL(j_internet_tidak, 0)+ISNULL(j_internet_ada, 0)) total
				from 
					rekap_sekolah
				where 
					bentuk_pendidikan_id = {$bentukPendidikanId}
					and semester_id = ".$semesterId."
					{$where}
				GROUP BY
					{$cakupan}
				ORDER BY
					{$cakupan}";

		$data = getDataBySql($sql);

		return $data;
	}

	public function getSekolahPerMbs($wilayahId, $request){
		$sql = "";
		$bentukPendidikanId = ($request->get('bentuk')) ? $request->get('bentuk') : 5;
		$cakupan = getCakupan($request->get('wilayah'));
		$semesterId = ($_REQUEST["semester_id"]) ? $_REQUEST["semester_id"] : SMT_BERJALAN;

		if($request->get('wilayah') == 2){
	        $where = "AND provinsi_id = ".$wilayahId;
	    }else if($request->get('wilayah') == 3){
	        $where = "AND kabupaten_kota_id = ".$wilayahId;
	    }

		$sql = "select
					{$cakupan} wilayah,
					sum(ISNULL(j_mbs_ya, 0)) ya,
					sum(ISNULL(j_mbs_tidak, 0)) tidak,
					sum(ISNULL(j_mbs_tidak, 0)+ISNULL(j_mbs_ya, 0)) total
				from 
					rekap_sekolah
				where 
					bentuk_pendidikan_id = {$bentukPendidikanId}
					and semester_id = ".$semesterId."
					{$where}
				GROUP BY
					{$cakupan}
				ORDER BY
					{$cakupan}";

		$data = getDataBySql($sql);

		return $data;
	}

	public function getSekolahPerWilayahKhusus($wilayahId, $request){
		$sql = "";
		$bentukPendidikanId = ($request->get('bentuk')) ? $request->get('bentuk') : 5;
		$cakupan = getCakupan($request->get('wilayah'));
		$semesterId = ($_REQUEST["semester_id"]) ? $_REQUEST["semester_id"] : SMT_BERJALAN;

		if($request->get('wilayah') == 2){
	        $where = "AND provinsi_id = ".$wilayahId;
	    }else if($request->get('wilayah') == 3){
	        $where = "AND kabupaten_kota_id = ".$wilayahId;
	    }

		$sql = "select
					{$cakupan} wilayah,
					sum(ISNULL(j_wil_khusus_ya, 0)) ya,
					sum(ISNULL(j_wil_khusus_tidak, 0)) tidak,
					sum(ISNULL(j_wil_khusus_tidak, 0)+ISNULL(j_wil_khusus_ya, 0)) total
				from 
					rekap_sekolah
				where 
					bentuk_pendidikan_id = {$bentukPendidikanId}
					and semester_id = ".$semesterId."
					{$where}
				GROUP BY
					{$cakupan}
				ORDER BY
					{$cakupan}";

		$data = getDataBySql($sql);

		return $data;
	}
	
	public function getKartuSekolah(Request $request, Application $app){
		include('/../src/mpdf/mpdf.php');
		$id = $request->get('id');
		$server = $_SERVER["HTTP_HOST"];
	
		$sekolah = SekolahPeer::retrieveByPK($id);
		
		// Data untuk table parasana
		$sql = "SELECT
			a.jenis_prasarana_id,
			a.nama_jenis_prasarana,
			a.nama_kepemilikan_sarpras,
			count(a.prasarana_id) as jumlah,
			a.panjang*a.lebar as luas,
			a.tipe_kerusakan_id
		FROM
			prasarana a
		WHERE
			a.sekolah_id = '".$sekolah->getSekolahId()."'
			and a.jenis_prasarana_id in (1,10,2,22,23,24,26,27,28,29,30)
		GROUP BY
			a.jenis_prasarana_id,
			a.nama_jenis_prasarana,
			a.nama_kepemilikan_sarpras,
			a.panjang*a.lebar,
			a.tipe_kerusakan_id
		ORDER BY a.jenis_prasarana_id";
		$dataPrasarana = getDataBySql($sql);
		foreach ($dataPrasarana as $d){
			if(is_array($arrPrasarana[$d["jenis_prasarana_id"]]["nama"]) && ($arrPrasarana[$d["jenis_prasarana_id"]]["kepemilikan"] == $d["nama_kepemilikan_sarpras"]) && ($arrPrasarana[$d["jenis_prasarana_id"]]["luas"] == $d["luas"])){
				if($d["tipe_kerusakan_id"] == 1 || $d["tipe_kerusakan_id"] == 6){
					$arrPrasarana[$d["jenis_prasarana_id"]]["baik"] += $d["jumlah"];
				}elseif($d["tipe_kerusakan_id"] == 2){
					$arrPrasarana[$d["jenis_prasarana_id"]]["rusak_ringan"] += $d["jumlah"];
				}elseif($d["tipe_kerusakan_id"] == 3){
					$arrPrasarana[$d["jenis_prasarana_id"]]["rusak_sedang"] += $d["jumlah"];
				}elseif($d["tipe_kerusakan_id"] == 4){
					$arrPrasarana[$d["jenis_prasarana_id"]]["rusak_berat"] += $d["jumlah"];
				}elseif($d["tipe_kerusakan_id"] == 5){
					$arrPrasarana[$d["jenis_prasarana_id"]]["rusak_total"] += $d["jumlah"];
				}
			}else{
				$arrPrasarana[$d["jenis_prasarana_id"]]["nama"] = $d["nama_jenis_prasarana"];
				$arrPrasarana[$d["jenis_prasarana_id"]]["kepemilikan"] = $d["nama_kepemilikan_sarpras"];
				$arrPrasarana[$d["jenis_prasarana_id"]]["jumlah"] = $d["jumlah"];
				$arrPrasarana[$d["jenis_prasarana_id"]]["luas"] = $d["luas"];
				$arrPrasarana[$d["jenis_prasarana_id"]]["total_luas"] = $d["luas"]*$d["jumlah"];
				$arrPrasarana[$d["jenis_prasarana_id"]]["baik"] = ($d["tipe_kerusakan_id"] == 1 || $d["tipe_kerusakan_id"] == 6) ? $d["jumlah"] : null;
				$arrPrasarana[$d["jenis_prasarana_id"]]["rusak_ringan"] = ($d["tipe_kerusakan_id"] == 2) ? $d["jumlah"] : null;
				$arrPrasarana[$d["jenis_prasarana_id"]]["rusak_sedang"] = ($d["tipe_kerusakan_id"] == 3) ? $d["jumlah"] : null;
				$arrPrasarana[$d["jenis_prasarana_id"]]["rusak_berat"] = ($d["tipe_kerusakan_id"] == 5) ? $d["jumlah"] : null;
				$arrPrasarana[$d["jenis_prasarana_id"]]["rusak_total"] = ($d["tipe_kerusakan_id"] == 5) ? $d["jumlah"] : null;
			}
		}
	
		// Data untuk table sarana
		$sql = "SELECT
			a.jenis_id,
			a.nama_jenis,
			count(a.jenis_id) as jumlah,
			sum(case when status_kelaikan = 1 then 1 else 0 end) laik,
			sum(case when status_kelaikan = 2 then 1 else 0 end) tidak_laik
		FROM
			sarana a
		WHERE
			a.sekolah_id = '".$sekolah->getSekolahId()."'
			and a.jenis_id in(1,2,3,4,7,60,200,199,37,28,29,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,30,60,26,27,61,62,63,64,65,9)
		GROUP BY
			a.jenis_id,
			a.nama_jenis,
			a.sekolah_id";
		$dataSarana = getDataBySql($sql);
		foreach ($dataSarana as $d){
			$arrSarana[$d["jenis_id"]] = $d;
		}
	
		// Data jumlah ptk
		$sql = "SELECT
					nama_jenis_ptk,
					count(*) as jumlah,
					sum(case when status_kepegawaian_id = 1 then 1 else 0 end) id_1,
					sum(case when status_kepegawaian_id = 2 then 1 else 0 end) id_2,
					sum(case when status_kepegawaian_id = 3 then 1 else 0 end) id_3,
					sum(case when status_kepegawaian_id = 4 then 1 else 0 end) id_4,
					sum(case when status_kepegawaian_id = 5 then 1 else 0 end) id_5,
					sum(case when status_kepegawaian_id = 6 then 1 else 0 end) id_6,
					sum(case when status_kepegawaian_id = 7 then 1 else 0 end) id_7,
					sum(case when status_kepegawaian_id = 8 then 1 else 0 end) id_8,
					sum(case when status_kepegawaian_id = 9 then 1 else 0 end) id_9,
					sum(case when status_kepegawaian_id in(10,99) then 1 else 0 end) id_99,
					Sum(case when jenis_kelamin = 'L' then 1 else 0 end) AS l,
					Sum(case when jenis_kelamin = 'P' then 1 else 0 end) AS p,
					Sum(case when ijazah_terakhir_id not in(23,30,35,40) then 1 else 0 end) AS d3,
					Sum(case when ijazah_terakhir_id in(23,30) then 1 else 0 end) AS s1_d4,
					Sum(case when ijazah_terakhir_id in(35,40) then 1 else 0 end) AS s2,
					Sum(case when (year(GETDATE())-year(tanggal_lahir)) < 35 then 1 else 0 end) AS u_35,
					Sum(case when (year(GETDATE())-year(tanggal_lahir)) >= 35 and (year(GETDATE())-year(tanggal_lahir)) <= 50 then 1 else 0 end) AS u_35_50,
					Sum(case when (year(GETDATE())-year(tanggal_lahir)) > 50 then 1 else 0 end) AS u_50,
					Sum(case when sudah_sertifikasi = 1 then 1 else 0 end) AS sertifikasi
				FROM
					ptk
				WHERE
					sekolah_id = '".$sekolah->getSekolahId()."'
				GROUP BY
					nama_jenis_ptk";
		$dataPtk = getDataBySql($sql);
		
		// Data Peserta Didik
		$sql = "SELECT
			a.tingkat_pendidikan_id,
			a.jenis_kelamin,
			year(GETDATE())-year(a.tanggal_lahir) as umur,
			a.nama_agama,
			a.penerima_kps,
			a.nama_kebutuhan_khusus,
			a.nama_kurikulum
		FROM
			peserta_didik a
		WHERE
			a.sekolah_id = '".$sekolah->getSekolahId()."'
			and a.tingkat_pendidikan_id is not NULL
			and a.jenis_keluar_id is NULL
		ORDER BY a.kebutuhan_khusus_id";
		$data = getDataBySql($sql);
		foreach ($data as $d){
			$arrPd[$d["tingkat_pendidikan_id"].$d["nama_kurikulum"]][$d["jenis_kelamin"]] += 1;
			if($d["umur"] < 7){
				$arrPd[$d["tingkat_pendidikan_id"].$d["nama_kurikulum"]]["<7"] += 1;
			}elseif($d["umur"] >= 7 && $d["umur"] <= 12){
				$arrPd[$d["tingkat_pendidikan_id"].$d["nama_kurikulum"]]["7-12"] += 1;
			}elseif($d["umur"] >= 13 && $d["umur"] <= 15){
				$arrPd[$d["tingkat_pendidikan_id"].$d["nama_kurikulum"]]["13-15"] += 1;
			}elseif($d["umur"] > 15){
				$arrPd[$d["tingkat_pendidikan_id"].$d["nama_kurikulum"]][">15"] += 1;
			}
			if($d["penerima_kps"] == 1){
				$arrPd[$d["tingkat_pendidikan_id"].$d["nama_kurikulum"]]["penerima_kps"] += 1;
			}
	
			$arrPdAgama[$d["tingkat_pendidikan_id"].$d["nama_kurikulum"]][$d["nama_agama"]][$d["jenis_kelamin"]] += 1;
			$arrPdAgama[$d["tingkat_pendidikan_id"].$d["nama_kurikulum"]]["total"][$d["jenis_kelamin"]] += 1;
	
			$arrPdKk[$d["tingkat_pendidikan_id".$d["nama_kurikulum"]]][$d["nama_kebutuhan_khusus"]] += 1;
			$arrPdKk[$d["tingkat_pendidikan_id"].$d["nama_kurikulum"]]["total"] += 1;
	
			$arrKebutuhanKhusus[$d["nama_kebutuhan_khusus"]] = $d["nama_kebutuhan_khusus"];
		}
	
		// Data rombongan belajar
		$sql = "SELECT
			a.tingkat_pendidikan_id,
			sum(a.jumlah_siswa) as jumlah_siswa,
			count(*) as jumlah,
			a.nama_kurikulum
		FROM
			rombongan_belajar a
		WHERE
			a.sekolah_id = '".$sekolah->getSekolahId()."'
			and a.semester_id = 20141
		GROUP BY
			a.tingkat_pendidikan_id,
			a.nama_kurikulum
		order by a.tingkat_pendidikan_id asc";
		$dataRombel = getDataBySql($sql);
	
		$mpdf=new \mPDF('c');
		$mpdf->SetTitle('Kartu Sekolah '.$sekolah->getNama());
		$mpdf->mirrorMargins = 1;	// Use different Odd/Even headers and footers and mirror margins
			
		$footer = '<table><tr>
				<td align="left"><span><i>Kartu Sekolah '.$sekolah->getNama().'</i></span></td>
				<td align="right" width="100%">{PAGENO}</td>
			</tr></table>';
	
		$mpdf->SetHTMLFooter($footer);
		$mpdf->SetHTMLFooter($footer,'E');
	
		ob_start();
		include dirname(__FILE__).D.'Templates'.D.'kartusekolah.php';
		$content = ob_get_clean();
	
		$stylesheet = file_get_contents(dirname(__FILE__).D.'Templates'.D.'style.css');
		$mpdf->imgdisdik = file_get_contents(dirname(__FILE__).D.'Templates'.D.'disdik.png');
		$mpdf->WriteHTML($stylesheet,1);
		$mpdf->WriteHTML($content);
		$mpdf->Output('Kartu Sekolah '.$sekolah->getNama().'.pdf');
		exit;
	}

}