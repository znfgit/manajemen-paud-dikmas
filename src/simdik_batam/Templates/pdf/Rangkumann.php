<?php



$html ='
<body>
<h2 align="center">Rekap Sekolah per Wilayah</h2>
<table border="1" cellspacing="0" cellpadding="0">
  <col width="170" />
  <col width="90" />
  <col width="122" />
  <col width="76" />
  <col width="120" />
  <col width="131" />
  <col width="125" />
  <col width="130" />
  <col width="108" />
  <col width="134" />
  <tr>
    <td width="244" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">Wilayah</td>
    <td colspan="3" align="center" valign="middle" bgcolor="#0099CC">PTK</td>
    <td colspan="5" align="center" valign="middle" bgcolor="#0099CC">Peserta Didik</td>
    <td width="87" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">Rombel</td>
  </tr>
  <tr>
    <td width="105" align="center" valign="middle" bgcolor="#0099CC">Guru</td>
    <td width="103" align="center" valign="middle" bgcolor="#0099CC">Pegawai</td>
    <td width="101" align="center" valign="middle" bgcolor="#0099CC">Total</td>
    <td width="107" align="center" valign="middle" bgcolor="#0099CC">Kelas 10</td>
    <td width="89" align="center" valign="middle" bgcolor="#0099CC">Kelas 11</td>
    <td width="91" align="center" valign="middle" bgcolor="#0099CC"> Kelas 12 </td>
    <td width="72" align="center" valign="middle" bgcolor="#0099CC">Kelas 13</td>
    <td width="77" align="center" valign="middle" bgcolor="#0099CC">Total</td>
  </tr>
  <tr>
    <td>Prop. Gorontalo</td>
    <td align="right" valign="middle">3007</td>
    <td align="right" valign="middle">429</td>
    <td align="right" valign="middle">3436</td>
    <td align="right" valign="middle">11600</td>
    <td align="right" valign="middle">10293</td>
    <td align="right" valign="middle">9548</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">30547</td>
    <td align="right" valign="middle">1210</td>
  </tr>
  <tr>
    <td>Prop. Papua</td>
    <td align="right" valign="middle">6086</td>
    <td align="right" valign="middle">650</td>
    <td align="right" valign="middle">6736</td>
    <td align="right" valign="middle">12194</td>
    <td align="right" valign="middle">10858</td>
    <td align="right" valign="middle">11873</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">34914</td>
    <td align="right" valign="middle">1461</td>
  </tr>
  <tr>
    <td>Prop. Sulawesi Utara</td>
    <td align="right" valign="middle">7799</td>
    <td align="right" valign="middle">728</td>
    <td align="right" valign="middle">8527</td>
    <td align="right" valign="middle">18278</td>
    <td align="right" valign="middle">17563</td>
    <td align="right" valign="middle">19464</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">55306</td>
    <td align="right" valign="middle">2448</td>
  </tr>
</table>

</body>
';

//==============================================================
//==============================================================
//==============================================================
include("../mpdf.php");

$mpdf=new mPDF('c','A4-L'); 
// $mpdf=new mPDF('c','A4','','',32,25,27,25,16,13); 

$mpdf->SetDisplayMode('fullpage');

$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

// LOAD a stylesheet
$stylesheet = file_get_contents('mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html,2);

$mpdf->Output('mpdf.pdf','I');
exit;
//==============================================================
//==============================================================
//==============================================================


?>