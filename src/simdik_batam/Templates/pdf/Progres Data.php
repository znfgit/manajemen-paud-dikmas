<?php



$html ='
<body>
<h2 align="center">Progres Data</h2>
<table cellspacing="0" cellpadding="0" border="1">
  <col width="170" />
  <col width="64" />
  <col width="94" />
  <col width="64" span="2" />
  <col width="90" />
  <col width="64" span="2" />
  <col width="94" />
  <col width="64" />
  <col width="131" />
  <col width="127" />
  <col width="77" />
  <col width="161" />
  <tr>
    <td width="169" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">Wilayah</td>
    <td colspan="3" align="center" valign="middle" bgcolor="#0099CC">SMA</td>
    <td colspan="3" align="center" valign="middle" bgcolor="#0099CC">SMK</td>
    <td colspan="3" align="center" valign="middle" bgcolor="#0099CC">SMLB</td>
    <td width="93" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">Sekolah Total</td>
    <td width="145" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">Data Masuk    Total </td>
    <td width="73" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">Sisa Total</td>
    <td width="127" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">%</td>
  </tr>
  <tr>
    <td width="59" align="center" valign="middle" bgcolor="#0099CC">Total </td>
    <td width="87" align="center" valign="middle" bgcolor="#0099CC">Data Masuk</td>
    <td width="38" align="center" valign="middle" bgcolor="#0099CC">Sisa</td>
    <td width="56" align="center" valign="middle" bgcolor="#0099CC">Total </td>
    <td width="75" align="center" valign="middle" bgcolor="#0099CC">Data Masuk</td>
    <td width="35" align="center" valign="middle" bgcolor="#0099CC">Sisa</td>
    <td width="33" align="center" valign="middle" bgcolor="#0099CC">Total </td>
    <td width="86" align="center" valign="middle" bgcolor="#0099CC">Data Masuk</td>
    <td width="37" align="center" valign="middle" bgcolor="#0099CC">Sisa</td>
  </tr>
  <tr>
    <td>Prop. Bengkulu</td>
    <td align="right" valign="middle">126</td>
    <td align="right" valign="middle">108</td>
    <td align="right" valign="middle">18</td>
    <td align="right" valign="middle">86</td>
    <td align="right" valign="middle">73</td>
    <td align="right" valign="middle">13</td>
    <td align="right" valign="middle">3</td>
    <td align="right" valign="middle">1</td>
    <td align="right" valign="middle">2</td>
    <td align="right" valign="middle">215</td>
    <td align="right" valign="middle">182</td>
    <td align="right" valign="middle">33</td>
    <td align="right" valign="middle">84.6511627900</td>
  </tr>
  <tr>
    <td>Prop. Bangka Belitung</td>
    <td align="right" valign="middle">66</td>
    <td align="right" valign="middle">55</td>
    <td align="right" valign="middle">11</td>
    <td align="right" valign="middle">54</td>
    <td align="right" valign="middle">41</td>
    <td align="right" valign="middle">13</td>
    <td align="right" valign="middle">2</td>
    <td align="right" valign="middle">2</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">122</td>
    <td align="right" valign="middle">98</td>
    <td align="right" valign="middle">24</td>
    <td align="right" valign="middle">80.3278688500</td>
  </tr>
  <tr>
    <td>Prop. Sumatera Selatan</td>
    <td align="right" valign="middle">558</td>
    <td align="right" valign="middle">426</td>
    <td align="right" valign="middle">132</td>
    <td align="right" valign="middle">254</td>
    <td align="right" valign="middle">208</td>
    <td align="right" valign="middle">46</td>
    <td align="right" valign="middle">2</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">2</td>
    <td align="right" valign="middle">814</td>
    <td align="right" valign="middle">634</td>
    <td align="right" valign="middle">180</td>
    <td align="right" valign="middle">77.8869778800</td>
  </tr>
  <tr>
    <td>Prop. Aceh</td>
    <td align="right" valign="middle">473</td>
    <td align="right" valign="middle">366</td>
    <td align="right" valign="middle">107</td>
    <td align="right" valign="middle">188</td>
    <td align="right" valign="middle">149</td>
    <td align="right" valign="middle">39</td>
    <td align="right" valign="middle">9</td>
    <td align="right" valign="middle">6</td>
    <td align="right" valign="middle">3</td>
    <td align="right" valign="middle">670</td>
    <td align="right" valign="middle">521</td>
    <td align="right" valign="middle">149</td>
    <td align="right" valign="middle">77.7611940200</td>
  </tr>
</table>
</body>
';

//==============================================================
//==============================================================
//==============================================================
include("../mpdf.php");

$mpdf=new mPDF('c','A4-L'); 
// $mpdf=new mPDF('c','A4','','',32,25,27,25,16,13); 

$mpdf->SetDisplayMode('fullpage');

$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

// LOAD a stylesheet
$stylesheet = file_get_contents('mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html,2);

$mpdf->Output('mpdf.pdf','I');
exit;
//==============================================================
//==============================================================
//==============================================================


?>