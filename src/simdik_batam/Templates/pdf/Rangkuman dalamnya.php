<?php



$html ='
<body>
<h2 align="center">Rekap Sekolah per Wilayah</h2>
<table border="1" cellpadding="0" cellspacing="0">
  <col width="128" />
  <col width="134" />
  <col width="170" />
  <col width="92" />
  <col width="122" />
  <col width="83" />
  <col width="120" />
  <col width="131" />
  <col width="125" />
  <col width="130" />
  <col width="108" />
  <col width="134" />
  <tr>
    <td width="93" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">NPSN</td>
    <td width="174" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">Kecamatan</td>
    <td width="186" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">Kabupaten</td>
    <td colspan="3" align="center" valign="middle" bgcolor="#0099CC">PTK</td>
    <td colspan="5" align="center" valign="middle" bgcolor="#0099CC">Peserta Didik</td>
    <td width="70" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">Rombel</td>
  </tr>
  <tr>
    <td width="79" align="center" valign="middle" bgcolor="#0099CC">Guru</td>
    <td width="77" align="center" valign="middle" bgcolor="#0099CC">Pegawai</td>
    <td width="78" align="center" valign="middle" bgcolor="#0099CC">Total</td>
    <td width="78" align="center" valign="middle" bgcolor="#0099CC">Kelas 10</td>
    <td width="76" align="center" valign="middle" bgcolor="#0099CC">Kelas 11</td>
    <td width="69" align="center" valign="middle" bgcolor="#0099CC"> Kelas 12 </td>
    <td width="60" align="center" valign="middle" bgcolor="#0099CC">Kelas 13</td>
    <td width="56" align="center" valign="middle" bgcolor="#0099CC">Total</td>
  </tr>
  <tr>
    <td>40500679</td>
    <td>Kec. Limboto Barat</td>
    <td>Kab. Gorontalo</td>
    <td align="right">13</td>
    <td align="right">0</td>
    <td align="right">13</td>
    <td align="right">6</td>
    <td align="right">9</td>
    <td align="right">16</td>
    <td align="right">0</td>
    <td align="right">31</td>
    <td align="right">13</td>
  </tr>
  <tr>
    <td>40500238</td>
    <td>Kec. Tilamuta</td>
    <td>Kab. Boalemo</td>
    <td align="right">0</td>
    <td align="right">0</td>
    <td align="right">0</td>
    <td align="right">0</td>
    <td align="right">0</td>
    <td align="right">0</td>
    <td align="right">0</td>
    <td align="right">0</td>
    <td align="right">0</td>
  </tr>
  <tr>
    <td>40502733</td>
    <td>Kec. Bonepantai</td>
    <td>Kab. Bone Bolango</td>
    <td align="right">0</td>
    <td align="right">0</td>
    <td align="right">0</td>
    <td align="right">0</td>
    <td align="right">0</td>
    <td align="right">0</td>
    <td align="right">0</td>
    <td align="right">0</td>
    <td align="right">0</td>
  </tr>
</table>

</body>
';

//==============================================================
//==============================================================
//==============================================================
include("../mpdf.php");

$mpdf=new mPDF('c','A4-L'); 
// $mpdf=new mPDF('c','A4','','',32,25,27,25,16,13); 

$mpdf->SetDisplayMode('fullpage');

$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

// LOAD a stylesheet
$stylesheet = file_get_contents('mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html,2);

$mpdf->Output('mpdf.pdf','I');
exit;
//==============================================================
//==============================================================
//==============================================================


?>