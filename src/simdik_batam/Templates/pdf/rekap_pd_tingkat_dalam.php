<body>
<h2 align="center">Rekap Peserta Didik Per Tingkat</h2>
<table border="1" cellpadding="0" cellspacing="0">
  <col width="128" />
  <col width="134" />
  <col width="170" />
  <col width="92" />
  <col width="122" />
  <col width="83" />
  <col width="120" />
  <col width="131" />
  <col width="125" />
  <col width="130" />
  <col width="108" />
  <col width="134" />
  <col width="113" />
  <col width="122" />
  <col width="125" />
  <tr>
    <td width="198" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">Satuan Pendidikan</td>
    <td width="66" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">NPSN</td>
    <td width="95" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">Kecamatan</td>
    <td width="93" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">Kabupaten</td>
    <td colspan="3" align="center" valign="middle" bgcolor="#0099CC">Kelas    10</td>
    <td colspan="3" align="center" valign="middle" bgcolor="#0099CC">Kelas 11</td>
    <td colspan="3" align="center" valign="middle" bgcolor="#0099CC">Kelas 12</td>
    <td colspan="3" align="center" valign="middle" bgcolor="#0099CC">Kelas 13</td>
  </tr>
  <tr>
    <td width="46" align="center" valign="middle" bgcolor="#0099CC">Laki - laki</td>
    <td width="71" align="center" valign="middle" bgcolor="#0099CC">Perempuan</td>
    <td width="35" align="center" valign="middle" bgcolor="#0099CC">Total</td>
    <td width="44" align="center" valign="middle" bgcolor="#0099CC">Laki - laki</td>
    <td width="71" align="center" valign="middle" bgcolor="#0099CC">Perempuan</td>
    <td width="35" align="center" valign="middle" bgcolor="#0099CC">Total</td>
    <td width="45" align="center" valign="middle" bgcolor="#0099CC">Laki - laki</td>
    <td width="72" align="center" valign="middle" bgcolor="#0099CC">Perempuan</td>
    <td width="36" align="center" valign="middle" bgcolor="#0099CC">Total</td>
    <td width="44" align="center" valign="middle" bgcolor="#0099CC">Laki - laki</td>
    <td width="68" align="center" valign="middle" bgcolor="#0099CC">Perempuan</td>
    <td width="45" align="center" valign="middle" bgcolor="#0099CC">Total</td>
  </tr>
  <tr>
    <td>Sentra PK-LK SLB Negeri Kabupaten    Gorontalo</td>
    <td>40500679</td>
    <td>Kec. Limboto Barat</td>
    <td>Kab. Gorontalo</td>
    <td align="right" valign="middle">4</td>
    <td align="right" valign="middle">2</td>
    <td align="right" valign="middle">6</td>
    <td align="right" valign="middle">5</td>
    <td align="right" valign="middle">4</td>
    <td align="right" valign="middle">9</td>
    <td align="right" valign="middle">12</td>
    <td align="right" valign="middle">4</td>
    <td align="right" valign="middle">16</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
  </tr>
  <tr>
    <td>SLB KABUPATEN BOALEMO</td>
    <td>40500238</td>
    <td>Kec. Tilamuta</td>
    <td>Kab. Boalemo</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
  </tr>
  <tr>
    <td>SLB Negeri Bonepantai</td>
    <td>40502733</td>
    <td>Kec. Bonepantai</td>
    <td>Kab. Bone Bolango</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
  </tr>
</table>
</body>
