<body>
<h2 align="center">Progres Data</h2>
<table cellspacing="0" cellpadding="0" border="1">
  <col width="170" />
  <col width="64" />
  <col width="94" />
  <col width="64" span="2" />
  <col width="90" />
  <col width="64" span="2" />
  <col width="94" />
  <col width="64" />
  <col width="131" />
  <col width="127" />
  <col width="77" />
  <col width="161" />
  <tr>
    <td width="169" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">Wilayah</td>
    <td colspan="3" align="center" valign="middle" bgcolor="#0099CC">SMA</td>
    <td colspan="3" align="center" valign="middle" bgcolor="#0099CC">SMK</td>
    <td colspan="3" align="center" valign="middle" bgcolor="#0099CC">SMLB</td>
    <td width="93" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">Sekolah Total</td>
    <td width="145" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">Data Masuk    Total </td>
    <td width="73" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">Sisa Total</td>
    <td width="127" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">%</td>
  </tr>
  <tr>
    <td width="59" align="center" valign="middle" bgcolor="#0099CC">Total </td>
    <td width="87" align="center" valign="middle" bgcolor="#0099CC">Data Masuk</td>
    <td width="38" align="center" valign="middle" bgcolor="#0099CC">Sisa</td>
    <td width="56" align="center" valign="middle" bgcolor="#0099CC">Total </td>
    <td width="75" align="center" valign="middle" bgcolor="#0099CC">Data Masuk</td>
    <td width="35" align="center" valign="middle" bgcolor="#0099CC">Sisa</td>
    <td width="33" align="center" valign="middle" bgcolor="#0099CC">Total </td>
    <td width="86" align="center" valign="middle" bgcolor="#0099CC">Data Masuk</td>
    <td width="37" align="center" valign="middle" bgcolor="#0099CC">Sisa</td>
  </tr>
  <?php 
  foreach ($return as $r) {
    ?>
    <tr>
      <td><?php echo $r['nama']; ?></td>
      <td align="right" valign="middle"><?php echo $r['sma']; ?></td>
      <td align="right" valign="middle"><?php echo $r['kirim_sma']; ?></td>
      <td align="right" valign="middle"><?php echo ($r['sma'] - $r['kirim_sma']); ?></td>
      <td align="right" valign="middle"><?php echo $r['smk']; ?></td>
      <td align="right" valign="middle"><?php echo $r['kirim_smk']; ?></td>
      <td align="right" valign="middle"><?php echo ($r['smk'] - $r['kirim_smk']); ?></td>
      <td align="right" valign="middle"><?php echo $r['smlb']; ?></td>
      <td align="right" valign="middle"><?php echo $r['kirim_smlb']; ?></td>
      <td align="right" valign="middle"><?php echo ($r['smlb'] - $r['kirim_smlb']); ?></td>
      <td align="right" valign="middle"><?php echo ($r['sma'] + $r['smk'] + $r['smlb']); ?></td>
      <td align="right" valign="middle"><?php echo ($r['kirim_sma'] + $r['kirim_smk'] + $r['kirim_smlb']); ?></td>
      <td align="right" valign="middle"><?php echo (($r['sma'] + $r['smk'] + $r['smlb']) - ($r['kirim_sma'] + $r['kirim_smk'] + $r['kirim_smlb'])); ?></td>
      <td align="right" valign="middle"><?php echo $r['persen']; ?></td>
    </tr>
    
    <?php

    $total_sma = $total_sma + $r['sma'];
    $total_kirim_sma = $total_kirim_sma + $r['kirim_sma'];
    $total_smk = $total_smk + $r['smk'];
    $total_smlb = $total_smlb + $r['smlb'];
    $total_kirim_smk = $total_kirim_smk + $r['kirim_smk'];
    $total_kirim_smlb = $total_kirim_smlb + $r['kirim_smlb'];
  }
  ?>

  <tr>
    <td><?php echo 'Total'; ?></td>
    <td align="right" valign="middle"><?php echo $total_sma; ?></td>
    <td align="right" valign="middle"><?php echo $total_kirim_sma; ?></td>
    <td align="right" valign="middle"><?php echo ($total_sma - $total_kirim_sma); ?></td>
    <td align="right" valign="middle"><?php echo $total_smk; ?></td>
    <td align="right" valign="middle"><?php echo $total_kirim_smk; ?></td>
    <td align="right" valign="middle"><?php echo ($total_smk - $total_kirim_smk); ?></td>
    <td align="right" valign="middle"><?php echo $total_smlb; ?></td>
    <td align="right" valign="middle"><?php echo $total_kirim_smlb; ?></td>
    <td align="right" valign="middle"><?php echo ($total_smlb - $total_kirim_smlb); ?></td>
    <td align="right" valign="middle"><?php echo ($total_sma + $total_smk + $total_smlb); ?></td>
    <td align="right" valign="middle"><?php echo ($total_kirim_sma + $total_kirim_smk + $total_kirim_smlb); ?></td>
    <td align="right" valign="middle"><?php echo (($total_sma + $total_smk + $total_smlb) - ($total_kirim_sma + $total_kirim_smk + $total_kirim_smlb)); ?></td>
    <td align="right" valign="middle"><?php echo (
      (
        ($total_kirim_sma + $total_kirim_smk + $total_kirim_smlb) / ($total_sma + $total_smk + $total_smlb)
      ) * 100 
    ); ?></td>
  </tr>

</table>
</body>
