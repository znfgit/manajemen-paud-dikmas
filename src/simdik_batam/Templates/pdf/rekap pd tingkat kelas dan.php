<?php



$html ='
<body>
<h2 align="center">Rekap PD Tingkat Kelas dan Jenis Kelamin</h2>
<table border="1" cellspacing="0" cellpadding="0">
  <col width="170" />
  <col width="92" />
  <col width="122" />
  <col width="83" />
  <col width="120" />
  <col width="131" />
  <col width="125" />
  <col width="130" />
  <col width="108" />
  <col width="134" />
  <col width="113" />
  <col width="122" />
  <col width="125" />
  <tr>
    <td width="245" rowspan="2" align="center" valign="middle" bgcolor="#0099CC">Wilayah</td>
    <td colspan="3" align="center" valign="middle" bgcolor="#0099CC">Kelas    10</td>
    <td colspan="3" align="center" valign="middle" bgcolor="#0099CC">Kelas 11</td>
    <td colspan="3" align="center" valign="middle" bgcolor="#0099CC">Kelas 12</td>
    <td colspan="3" align="center" valign="middle" bgcolor="#0099CC">Kelas 13</td>
  </tr>
  <tr>
    <td width="76" align="center" valign="middle" bgcolor="#0099CC">Laki - laki</td>
    <td width="76" align="center" valign="middle" bgcolor="#0099CC">Perempuan</td>
    <td width="52" align="center" valign="middle" bgcolor="#0099CC">Total</td>
    <td width="68" align="center" valign="middle" bgcolor="#0099CC">Laki - laki</td>
    <td width="80" align="center" valign="middle" bgcolor="#0099CC">Perempuan</td>
    <td width="49" align="center" valign="middle" bgcolor="#0099CC">Total</td>
    <td width="92" align="center" valign="middle" bgcolor="#0099CC">Laki - laki</td>
    <td width="90" align="center" valign="middle" bgcolor="#0099CC">Perempuan</td>
    <td width="52" align="center" valign="middle" bgcolor="#0099CC">Total</td>
    <td width="93" align="center" valign="middle" bgcolor="#0099CC">Laki - laki</td>
    <td width="86" align="center" valign="middle" bgcolor="#0099CC">Perempuan</td>
    <td width="37" align="center" valign="middle" bgcolor="#0099CC">Total</td>
  </tr>
  <tr>
    <td>Prop. Gorontalo</td>
    <td align="right" valign="middle">5673</td>
    <td align="right" valign="middle">5927</td>
    <td align="right" valign="middle">11600</td>
    <td align="right" valign="middle">4958</td>
    <td align="right" valign="middle">5335</td>
    <td align="right" valign="middle">10293</td>
    <td align="right" valign="middle">4593</td>
    <td align="right" valign="middle">4955</td>
    <td align="right" valign="middle">9548</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
  </tr>
  <tr>
    <td>Prop. Papua</td>
    <td align="right" valign="middle">6904</td>
    <td align="right" valign="middle">5290</td>
    <td align="right" valign="middle">12194</td>
    <td align="right" valign="middle">6111</td>
    <td align="right" valign="middle">4747</td>
    <td align="right" valign="middle">10858</td>
    <td align="right" valign="middle">6753</td>
    <td align="right" valign="middle">5120</td>
    <td align="right" valign="middle">11873</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
  </tr>
  <tr>
    <td>Prop. Sulawesi Utara</td>
    <td align="right" valign="middle">8948</td>
    <td align="right" valign="middle">9330</td>
    <td align="right" valign="middle">18278</td>
    <td align="right" valign="middle">8555</td>
    <td align="right" valign="middle">9008</td>
    <td align="right" valign="middle">17563</td>
    <td align="right" valign="middle">9439</td>
    <td align="right" valign="middle">10025</td>
    <td align="right" valign="middle">19464</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
    <td align="right" valign="middle">0</td>
  </tr>
</table>


</body>
';

//==============================================================
//==============================================================
//==============================================================
include("../mpdf.php");

$mpdf=new mPDF('c','A4-L'); 
// $mpdf=new mPDF('c','A4','','',32,25,27,25,16,13); 

$mpdf->SetDisplayMode('fullpage');

$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

// LOAD a stylesheet
$stylesheet = file_get_contents('mpdfstyletables.css');
$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text

$mpdf->WriteHTML($html,2);

$mpdf->Output('mpdf.pdf','I');
exit;
//==============================================================
//==============================================================
//==============================================================


?>