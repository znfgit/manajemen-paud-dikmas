<?php

namespace library\Templates;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;


echo '<?xml version="1.0"?>';
echo '<?mso-application progid="Excel.Sheet"?>';
?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author>khalid</Author>
  <LastAuthor>khalid</LastAuthor>
  <Created>2014-11-05T06:10:04Z</Created>
  <LastSaved>2014-11-05T06:15:27Z</LastSaved>
  <Version>15.00</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>7755</WindowHeight>
  <WindowWidth>20490</WindowWidth>
  <WindowTopX>0</WindowTopX>
  <WindowTopY>0</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s69">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s77">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
  <Style ss:ID="s78">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
  <Style ss:ID="s79">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#D9D9D9" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s80">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#D9D9D9" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s82">
   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom" ss:WrapText="1"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s83">
   <Alignment ss:Horizontal="Right" ss:Vertical="Bottom" ss:WrapText="1"/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Lembar1">
  <Table ss:ExpandedColumnCount="13" ss:ExpandedRowCount="<?php echo ($i+5); ?>" x:FullColumns="1"
   x:FullRows="1" ss:DefaultRowHeight="15">
   <Column ss:AutoFitWidth="0" ss:Width="23.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="135"/>
   <Column ss:StyleID="s83" ss:AutoFitWidth="0" ss:Width="77.25" ss:Span="10"/>
   <Row ss:Height="15.75">
    <Cell ss:MergeAcross="12" ss:StyleID="s77"><Data ss:Type="String">Jumlah PTK Berdasarkan Jenis PTK </Data></Cell>
   </Row>
   <Row ss:Height="15.75">
    <Cell ss:MergeAcross="12" ss:StyleID="s77"><Data ss:Type="String"><?php echo $wilayah; ?></Data></Cell>
   </Row>
   <Row ss:Height="15.75">
    <Cell ss:MergeAcross="12" ss:StyleID="s78"><Data ss:Type="String"><?php echo $today; ?></Data></Cell>
   </Row>
   <Row ss:Height="30">
    <Cell ss:StyleID="s79"><Data ss:Type="String">No</Data></Cell>
    <Cell ss:StyleID="s79"><Data ss:Type="String">Wilayah</Data></Cell>
    <Cell ss:StyleID="s80"><Data ss:Type="String">Guru Kelas</Data></Cell>
    <Cell ss:StyleID="s80"><Data ss:Type="String">Guru Mapel</Data></Cell>
    <Cell ss:StyleID="s80"><Data ss:Type="String">Guru BK</Data></Cell>
    <Cell ss:StyleID="s80"><Data ss:Type="String">Guru Inklusi</Data></Cell>
    <Cell ss:StyleID="s80"><Data ss:Type="String">Tenaga Administrasi Sekolah</Data></Cell>
    <Cell ss:StyleID="s80"><Data ss:Type="String">Guru Pendamping</Data></Cell>
    <Cell ss:StyleID="s80"><Data ss:Type="String">Guru Magang</Data></Cell>
    <Cell ss:StyleID="s80"><Data ss:Type="String">Guru TIK</Data></Cell>
    <Cell ss:StyleID="s80"><Data ss:Type="String">Laboran</Data></Cell>
    <Cell ss:StyleID="s80"><Data ss:Type="String">Pustakawan</Data></Cell>
    <Cell ss:StyleID="s80"><Data ss:Type="String">Lainnya</Data></Cell>
   </Row>
   <?php echo $str; ?>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <Selected/>
   <LeftColumnVisible>1</LeftColumnVisible>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>5</ActiveRow>
     <ActiveCol>2</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>
