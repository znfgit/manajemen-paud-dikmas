<?php

namespace library\Templates;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;


echo '<?xml version="1.0"?>';
echo '<?mso-application progid="Excel.Sheet"?>';
?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author>Reyzal</Author>
  <LastAuthor>khalid</LastAuthor>
  <Created>2015-02-21T16:06:07Z</Created>
  <LastSaved>2015-02-23T12:04:19Z</LastSaved>
  <Version>15.00</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>7995</WindowHeight>
  <WindowWidth>20115</WindowWidth>
  <WindowTopX>120</WindowTopX>
  <WindowTopY>120</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s16">
   <Borders/>
   <Interior/>
  </Style>
  <Style ss:ID="s17">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Interior/>
  </Style>
  <Style ss:ID="s18">
   <Alignment ss:Vertical="Center"/>
   <Borders/>
   <Interior/>
  </Style>
  <Style ss:ID="s19">
   <Alignment ss:Vertical="Center"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="14" ss:Color="#000000"/>
   <Interior/>
  </Style>
  <Style ss:ID="s20">
   <Alignment ss:Vertical="Center"/>
  </Style>
  <Style ss:ID="s22">
   <Alignment ss:Vertical="Center"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="18" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
  <Style ss:ID="s23">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Interior ss:Color="#00B0F0" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s24">
   <Alignment ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Arial" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
   <Interior ss:Color="#00B0F0" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s25">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="18" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Sheet1">
  <Table ss:ExpandedColumnCount="17" ss:ExpandedRowCount="4" x:FullColumns="1"
   x:FullRows="1" ss:DefaultRowHeight="15">
   <Column ss:AutoFitWidth="0" ss:Width="193.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="139.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="70.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="127.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="93.75" ss:Span="1"/>
   <Column ss:Index="7" ss:AutoFitWidth="0" ss:Width="249.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="167.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="145.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="93.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="97.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="81"/>
   <Column ss:AutoFitWidth="0" ss:Width="100.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="84.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="91.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="93.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="48.75"/>
   <Row ss:AutoFitHeight="0" ss:Height="27.75">
    <Cell ss:MergeAcross="8" ss:StyleID="s25"><Data ss:Type="String">Sekolah Detail PTK</Data></Cell>
    <Cell ss:StyleID="s22"/>
    <Cell ss:StyleID="s22"/>
    <Cell ss:StyleID="s22"/>
    <Cell ss:StyleID="s22"/>
    <Cell ss:StyleID="s22"/>
    <Cell ss:StyleID="s22"/>
    <Cell ss:StyleID="s22"/>
    <Cell ss:StyleID="s20"/>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:StyleID="s23"><Data ss:Type="String">Nama</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">NIK </Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">Jenis Kelamin</Data></Cell>
    <Cell ss:StyleID="s24"><Data ss:Type="String">Tempat Lahir</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">Tanggal Lahir</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">Nama Ibu Kandung</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">Alamat Jalan</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">Status Kepegawaian</Data></Cell>
    <Cell ss:StyleID="s23"><Data ss:Type="String">NIP</Data></Cell>
    <Cell ss:StyleID="s17"/>
    <Cell ss:StyleID="s17"/>
    <Cell ss:StyleID="s17"/>
    <Cell ss:StyleID="s18"/>
    <Cell ss:StyleID="s17"/>
    <Cell ss:StyleID="s17"/>
    <Cell ss:StyleID="s17"/>
    <Cell ss:StyleID="s19"/>
   </Row>
   <Row>
    <Cell ss:StyleID="s18"><Data ss:Type="String">[nama]</Data></Cell>
    <Cell ss:StyleID="s18"><Data ss:Type="String">[nik]</Data></Cell>
    <Cell ss:StyleID="s18"><Data ss:Type="String">[jk]</Data></Cell>
    <Cell ss:StyleID="s18"><Data ss:Type="String">[tl]</Data></Cell>
    <Cell ss:StyleID="s18"><Data ss:Type="String">[tl]</Data></Cell>
    <Cell ss:StyleID="s18"><Data ss:Type="String">[Nama Ibu Kandung]</Data></Cell>
    <Cell ss:StyleID="s18"><Data ss:Type="String">[Alamat Jalan]</Data></Cell>
    <Cell ss:StyleID="s18"><Data ss:Type="String">[Status Kepegawaian]</Data></Cell>
    <Cell ss:StyleID="s18"><Data ss:Type="String">[NIP]</Data></Cell>
    <Cell ss:StyleID="s16"/>
    <Cell ss:StyleID="s16"/>
    <Cell ss:StyleID="s16"/>
    <Cell ss:StyleID="s18"/>
    <Cell ss:StyleID="s16"/>
    <Cell ss:StyleID="s16"/>
    <Cell ss:StyleID="s16"/>
    <Cell ss:StyleID="s18"/>
   </Row>
   <Row>
    <Cell ss:StyleID="s16"/>
    <Cell ss:StyleID="s16"/>
    <Cell ss:StyleID="s16"/>
    <Cell ss:StyleID="s16"/>
    <Cell ss:StyleID="s16"/>
    <Cell ss:StyleID="s16"/>
    <Cell ss:StyleID="s16"/>
    <Cell ss:StyleID="s16"/>
    <Cell ss:StyleID="s16"/>
    <Cell ss:StyleID="s16"/>
    <Cell ss:StyleID="s16"/>
    <Cell ss:StyleID="s16"/>
    <Cell ss:StyleID="s16"/>
    <Cell ss:StyleID="s16"/>
    <Cell ss:StyleID="s16"/>
    <Cell ss:StyleID="s16"/>
    <Cell ss:StyleID="s16"/>
   </Row>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <Print>
    <ValidPrinterInfo/>
    <HorizontalResolution>-3</HorizontalResolution>
    <VerticalResolution>-3</VerticalResolution>
   </Print>
   <Selected/>
   <LeftColumnVisible>4</LeftColumnVisible>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>3</ActiveRow>
     <ActiveCol>6</ActiveCol>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
 <Worksheet ss:Name="Sheet2">
  <Table ss:ExpandedColumnCount="1" ss:ExpandedRowCount="1" x:FullColumns="1"
   x:FullRows="1" ss:DefaultRowHeight="15">
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
 <Worksheet ss:Name="Sheet3">
  <Table ss:ExpandedColumnCount="1" ss:ExpandedRowCount="1" x:FullColumns="1"
   x:FullRows="1" ss:DefaultRowHeight="15">
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>
