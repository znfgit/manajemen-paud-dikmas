<?php

namespace library\Templates;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;


echo '<?xml version="1.0"?>';
echo '<?mso-application progid="Excel.Sheet"?>';
?>

<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author>Reyzal</Author>
  <LastAuthor>Reyzal</LastAuthor>
  <Created>2014-12-21T15:54:40Z</Created>
  <LastSaved>2014-12-21T18:31:00Z</LastSaved>
  <Version>12.00</Version>
 </DocumentProperties>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>7995</WindowHeight>
  <WindowWidth>20115</WindowWidth>
  <WindowTopX>240</WindowTopX>
  <WindowTopY>120</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s63">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
  </Style>
  <Style ss:ID="s64">
   <Interior ss:Color="#92D050" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s65">
   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s70">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Interior ss:Color="#92D050" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s75">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Interior ss:Color="#92D050" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s76">
   <Interior ss:Color="#92D050" ss:Pattern="Solid"/>
   <NumberFormat ss:Format="d\-mmm"/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Sheet1">
  <Table ss:ExpandedColumnCount="12" ss:ExpandedRowCount="<?php echo ($i+5); ?>" x:FullColumns="1"
   x:FullRows="1" ss:DefaultRowHeight="15">
   <Column ss:AutoFitWidth="0" ss:Width="200.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="66"/>
   <Column ss:AutoFitWidth="0" ss:Width="100.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="91.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="60.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="61.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="91.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="108.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="84.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="107.25"/>
   <Column ss:Width="109.5"/>
   <Row ss:AutoFitHeight="0">
    <Cell ss:MergeAcross="11" ss:StyleID="s63"><Data ss:Type="String">Peserta Didik Umur</Data></Cell>
   </Row>
   <Row ss:Index="3" ss:AutoFitHeight="0">
    <Cell ss:MergeDown="1" ss:StyleID="s75"><Data ss:Type="String">satuan Pendidikan</Data></Cell>
    <Cell ss:MergeDown="1" ss:StyleID="s75"><Data ss:Type="String"></Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s70"><Data ss:Type="String"></Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s70"><Data ss:Type="String">Kelompok Usia (Tahun) SMA/SMK/SLB/SMLB</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s70"><Data ss:Type="String"></Data></Cell>
    <Cell ss:StyleID="s65"/>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:Index="3" ss:StyleID="s64"><Data ss:Type="String">Kecamatan</Data></Cell>
    <Cell ss:StyleID="s76"><Data ss:Type="String">Kabupaten</Data></Cell>
    <Cell ss:StyleID="s64"><Data ss:Type="String">Total</Data></Cell>
    <Cell ss:StyleID="s64"><Data ss:Type="String">&lt;15</Data></Cell>
    <Cell ss:StyleID="s64"><Data ss:Type="String">15=18</Data></Cell>
    <Cell ss:StyleID="s64"><Data ss:Type="String">&gt;18</Data></Cell>
    <Cell ss:StyleID="s64"><Data ss:Type="String"></Data></Cell>
    <Cell ss:StyleID="s64"><Data ss:Type="String"></Data></Cell>
    <Cell ss:StyleID="s64"><Data ss:Type="String"></Data></Cell>
   </Row>
 <?php echo $usia; ?>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <Unsynced/>
   <Selected/>
   <Panes>
    <Pane>
     <Number>3</Number>
     <RangeSelection>R1C1:R1C12</RangeSelection>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
 <Worksheet ss:Name="Sheet2">
  <Table ss:ExpandedColumnCount="1" ss:ExpandedRowCount="1" x:FullColumns="1"
   x:FullRows="1" ss:DefaultRowHeight="15">
   <Row ss:AutoFitHeight="0"/>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <Unsynced/>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
 <Worksheet ss:Name="Sheet3">
  <Table ss:ExpandedColumnCount="1" ss:ExpandedRowCount="1" x:FullColumns="1"
   x:FullRows="1" ss:DefaultRowHeight="15">
   <Row ss:AutoFitHeight="0"/>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <Unsynced/>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>
