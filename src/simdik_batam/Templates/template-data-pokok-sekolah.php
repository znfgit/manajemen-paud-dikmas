<?php


echo '<?xml version="1.0"?>';
echo '<?mso-application progid="Excel.Sheet"?>';
?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author>khalid</Author>
  <LastAuthor>khalid</LastAuthor>
  <Created>2014-09-13T02:47:04Z</Created>
  <LastSaved>2014-09-13T03:12:19Z</LastSaved>
  <Version>15.00</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>7755</WindowHeight>
  <WindowWidth>20490</WindowWidth>
  <WindowTopX>0</WindowTopX>
  <WindowTopY>0</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="m438339840">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#BFBFBF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="m438339860">
   <Alignment ss:Horizontal="Left" ss:Vertical="Top"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s72">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
  </Style>
  <Style ss:ID="s78">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s79">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
  </Style>
  <Style ss:ID="s82">
   <Alignment ss:Vertical="Bottom"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="14" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
  <Style ss:ID="s83">
   <Alignment ss:Vertical="Bottom"/>
  </Style>
  <Style ss:ID="s85">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="14" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
  <Style ss:ID="s86">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
  </Style>
  <Style ss:ID="s88">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#BFBFBF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s93">
   <Alignment ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
  <Style ss:ID="s96">
   <Alignment ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Interior ss:Color="#BFBFBF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s97">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Interior ss:Color="#BFBFBF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s110">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Interior ss:Color="#BFBFBF" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s115">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior/>
  </Style>
  <Style ss:ID="s117">
   <Alignment ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>
   </Borders>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior/>
  </Style>
 </Styles>
 <Worksheet ss:Name="daftar Sekolah">
  <Table ss:ExpandedColumnCount="36" ss:ExpandedRowCount="<?php echo (6+$i); ?>" x:FullColumns="1"
   x:FullRows="1" ss:DefaultRowHeight="15">
   <Column ss:StyleID="s72" ss:AutoFitWidth="0" ss:Width="27.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="161.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="87.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="89.25"/>
   <Column ss:Width="105"/>
   <Column ss:AutoFitWidth="0" ss:Width="99.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="90.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="100.5"/>
   <Column ss:Width="49.5"/>
   <Column ss:Width="54"/>
   <Column ss:Width="92.25"/>
   <Column ss:Width="89.25"/>
   <Column ss:Width="91.5"/>
   <Column ss:Width="93"/>
   <Column ss:AutoFitWidth="0" ss:Width="71.25"/>
   <Column ss:Width="82.5"/>
   <Column ss:Width="72.75"/>
   <Column ss:Width="66"/>
   <Column ss:Width="111"/>
   <Column ss:Width="90.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="95.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="93"/>
   <Column ss:AutoFitWidth="0" ss:Width="116.25"/>
   <Column ss:Width="115.5"/>
   <Column ss:Width="129.75"/>
   <Column ss:Width="134.25"/>
   <Column ss:Width="171"/>
   <Column ss:Width="81.75"/>
   <Column ss:Width="117.75"/>
   <Column ss:Width="72"/>
   <Column ss:Width="65.25"/>
   <Column ss:Width="93.75"/>
   <Column ss:Width="115.5"/>
   <Column ss:Width="42.75"/>
   <Column ss:Width="97.5"/>
   <Column ss:Width="129.75"/>
   <Row ss:Height="18.75">
    <Cell ss:MergeAcross="1" ss:StyleID="s85"><Data ss:Type="String">Data Pokok Sekolah</Data></Cell>
    <Cell ss:StyleID="s82"/>
    <Cell ss:StyleID="s82"/>
    <Cell ss:StyleID="s82"/>
    <Cell ss:StyleID="s82"/>
    <Cell ss:StyleID="s82"/>
    <Cell ss:StyleID="s82"/>
   </Row>
   <Row>
    <Cell ss:MergeAcross="1" ss:StyleID="s86"><ss:Data ss:Type="String"
      xmlns="http://www.w3.org/TR/REC-html40"><Font html:Color="#000000">Sumber: </Font><B><Font
        html:Color="#000000"><?php echo LABEL." ".LEVEL." ".KABKOTA." (".URL.")"; ?></Font></B></ss:Data></Cell>
    <Cell ss:StyleID="s83"/>
    <Cell ss:StyleID="s96"><Data ss:Type="String">Kata Kunci:</Data></Cell>
    <Cell ss:StyleID="s93"><Data ss:Type="String"><?php echo $keyword; ?></Data></Cell>
    <Cell ss:StyleID="s96"><Data ss:Type="String">Kabupaten/Kota:</Data></Cell>
    <Cell ss:StyleID="s93"><Data ss:Type="String"><?php echo $namaKab; ?></Data></Cell>
    <Cell ss:StyleID="s110"><Data ss:Type="String">Bentuk Pendidikan:</Data></Cell>
    <Cell ss:MergeAcross="1" ss:StyleID="s117"><Data ss:Type="String"><?php echo $bp_id_str; ?></Data></Cell>
   </Row>
   <Row>
    <Cell ss:MergeAcross="1" ss:StyleID="s86"><Data ss:Type="String">Tanggal Unduh: <?php echo $today; ?></Data></Cell>
    <Cell ss:StyleID="s83"/>
    <Cell ss:StyleID="s97"><Data ss:Type="String">Propinsi:</Data></Cell>
    <Cell ss:StyleID="s93"><Data ss:Type="String"><?php echo $namaProp; ?></Data></Cell>
    <Cell ss:StyleID="s96"><Data ss:Type="String">Kecamatan:</Data></Cell>
    <Cell ss:StyleID="s93"><Data ss:Type="String"><?php echo $namaKec; ?></Data></Cell>
    <Cell ss:StyleID="s110"><Data ss:Type="String">Status Sekolah:</Data></Cell>
    <Cell ss:MergeAcross="1" ss:StyleID="s115"><Data ss:Type="String"><?php echo $ss_id_str; ?></Data></Cell>
   </Row>
   <Row ss:Index="5" ss:AutoFitHeight="0" ss:Height="21">
    <Cell ss:StyleID="s88"><Data ss:Type="String">No.</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">Nama Sekolah</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">NSS</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">NPSN</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">Bentuk Pendidikan</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">Status Sekolah</Data></Cell>
    <Cell ss:MergeAcross="1" ss:StyleID="m438339840"><Data ss:Type="String">Alamat Jalan</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">RT</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">RW</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">Nama Dusun</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">Desa/Kelurahan</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">Kecamatan</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">Kabupaten/Kota</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">Propinsi</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">Kode Pos</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">Lintang</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">Bujur</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">Nomor Telepon</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">Nomor Fax</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">Email</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">Website</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">SK Pendirian Sekolah</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">Tanggal SK Pendirian</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">Status kepemilikan</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">SK Izin Operasional</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">Tanggal SK Izin Operasional</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">No Rekening</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">Nama Bank</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">Cabang KCP Unit</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">Atas Nama Rekening</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">MBS</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">Luas Tanah Milik</Data></Cell>
    <Cell ss:StyleID="s88"><Data ss:Type="String">Luas Tanah Bukan Milik</Data></Cell>
   </Row>
   <?php echo $str; ?>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <Print>
    <ValidPrinterInfo/>
    <HorizontalResolution>600</HorizontalResolution>
    <VerticalResolution>300</VerticalResolution>
   </Print>
   <Zoom>110</Zoom>
   <Selected/>
   <FreezePanes/>
   <FrozenNoSplit/>
   <SplitVertical>2</SplitVertical>
   <LeftColumnRightPane>2</LeftColumnRightPane>
   <ActivePane>1</ActivePane>
   <Panes>
    <Pane>
     <Number>3</Number>
    </Pane>
    <Pane>
     <Number>1</Number>
     <ActiveRow>2</ActiveRow>
     <ActiveCol>0</ActiveCol>
     <RangeSelection>R3C1:R3C2</RangeSelection>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>
