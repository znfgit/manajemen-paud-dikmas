<?php

echo '<?xml version="1.0"?>';
echo '<?mso-application progid="Excel.Sheet"?>';
?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author>khalid</Author>
  <LastAuthor>khalid</LastAuthor>
  <LastPrinted>2014-10-06T02:13:20Z</LastPrinted>
  <Created>2014-10-06T01:40:07Z</Created>
  <LastSaved>2014-10-06T02:13:52Z</LastSaved>
  <Version>15.00</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>7755</WindowHeight>
  <WindowWidth>20490</WindowWidth>
  <WindowTopX>0</WindowTopX>
  <WindowTopY>0</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s73">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
  <Style ss:ID="s74">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
  </Style>
  <Style ss:ID="s202">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="20" ss:Color="#548235"
    ss:Bold="1"/>
  </Style>
  <Style ss:ID="s205">
   <Alignment ss:Vertical="Bottom"/>
  </Style>
  <Style ss:ID="s207">
   <Alignment ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
  </Style>
  <Style ss:ID="s235">
   <Alignment ss:Vertical="Center"/>
  </Style>
  <Style ss:ID="s272">
   <Alignment ss:Horizontal="Center" ss:Vertical="Bottom"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#FFFFFF"/>
   <Interior ss:Color="#548235" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s300">
   <Borders/>
   <Interior ss:Color="#F2F2F2" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s301">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#1F4E78"
    ss:Bold="1"/>
   <Interior ss:Color="#F2F2F2" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s352">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
   <Borders/>
   <Interior ss:Color="#F2F2F2" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s393">
   <Alignment ss:Horizontal="Left" ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#203764"
    ss:Bold="1"/>
   <Interior ss:Color="#F2F2F2" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s444">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#FFC000" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s468">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"
    ss:Bold="1"/>
   <Interior ss:Color="#FFC000" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s469">
   <Alignment ss:Horizontal="Left" ss:Vertical="Center"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#203764"
    ss:Bold="1"/>
   <Interior ss:Color="#F2F2F2" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s470">
   <Alignment ss:Vertical="Center"/>
   <Interior ss:Color="#FFC000" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s471">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="22" ss:Color="#FFFFFF"
    ss:Bold="1"/>
   <Interior ss:Color="#375623" ss:Pattern="Solid"/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Profil Sekolah">
  <Table ss:ExpandedColumnCount="12" ss:ExpandedRowCount="50" x:FullColumns="1"
   x:FullRows="1" ss:DefaultRowHeight="15">
   <Column ss:AutoFitWidth="0" ss:Width="98.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="9.75"/>
   <Column ss:Index="6" ss:AutoFitWidth="0" ss:Width="9"/>
   <Column ss:Width="114"/>
   <Column ss:AutoFitWidth="0" ss:Width="9.75"/>
   <Row>
    <Cell ss:MergeAcross="10" ss:StyleID="s272"><Data ss:Type="String">Profil Sekolah</Data></Cell>
    <Cell ss:StyleID="s205"/>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:MergeAcross="10" ss:MergeDown="1" ss:StyleID="s471"><Data
      ss:Type="String"><?php echo $namaSekolah; ?></Data></Cell>
    <Cell ss:StyleID="s202"/>
   </Row>
   <Row ss:AutoFitHeight="0">
    <Cell ss:Index="12" ss:StyleID="s202"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell><Data ss:Type="String">Tahun Ajaran:</Data></Cell>
    <Cell ss:MergeAcross="9" ss:StyleID="s73"><Data ss:Type="String"><?php echo $ta_str; ?></Data></Cell>
    <Cell ss:StyleID="s207"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell><Data ss:Type="String">Tanggal:</Data></Cell>
    <Cell ss:MergeAcross="9" ss:StyleID="s73"><Data ss:Type="String"><?php echo $today; ?></Data></Cell>
    <Cell ss:StyleID="s207"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell><Data ss:Type="String">Pengunduh:</Data></Cell>
    <Cell ss:MergeAcross="9" ss:StyleID="s73"><Data ss:Type="String"><?php echo 'Admin'?></Data></Cell>
    <Cell ss:StyleID="s207"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell><Data ss:Type="String">Sumber:</Data></Cell>
    <Cell ss:MergeAcross="9" ss:StyleID="s73"><Data ss:Type="String"><?php echo URL; ?></Data></Cell>
    <Cell ss:StyleID="s207"/>
   </Row>
   <Row ss:Index="8" ss:AutoFitHeight="0" ss:Height="20.0625" ss:StyleID="s235">
    <Cell ss:MergeAcross="4" ss:StyleID="s468"><Data ss:Type="String">IDENTITAS SEKOLAH</Data></Cell>
    <Cell ss:StyleID="s470"/>
    <Cell ss:StyleID="s470"/>
    <Cell ss:StyleID="s470"/>
    <Cell ss:StyleID="s470"/>
    <Cell ss:StyleID="s470"/>
    <Cell ss:StyleID="s470"/>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s300"><Data ss:Type="String">Nama</Data></Cell>
    <Cell ss:StyleID="s300"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="8" ss:StyleID="s301"><Data ss:Type="String"><?php echo $namaSekolah; ?></Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s300"><Data ss:Type="String">NPSN</Data></Cell>
    <Cell ss:StyleID="s300"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="8" ss:StyleID="s301"><Data ss:Type="String"><?php echo $npsn; ?></Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s300"><Data ss:Type="String">Bentuk Pendidikan</Data></Cell>
    <Cell ss:StyleID="s300"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="8" ss:StyleID="s301"><Data ss:Type="String"><?php echo $bentukPendidikan; ?></Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s300"><Data ss:Type="String">Status</Data></Cell>
    <Cell ss:StyleID="s300"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="8" ss:StyleID="s301"><Data ss:Type="String"><?php echo $status; ?></Data></Cell>
   </Row>
   <Row ss:Index="14" ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:MergeAcross="4" ss:StyleID="s444"><Data ss:Type="String">LOKASI SEKOLAH</Data></Cell>
    <Cell ss:StyleID="s235"/>
    <Cell ss:MergeAcross="4" ss:StyleID="s444"><Data ss:Type="String">DATA PELENGKAP</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s352"><Data ss:Type="String">Alamat</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s301"><Data ss:Type="String"><?php echo $alamatJalan?></Data></Cell>
    <Cell ss:StyleID="s74"/>
    <Cell ss:StyleID="s352"><Data ss:Type="String">SK Pendirian Sekolah</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s301"><Data ss:Type="String"><?php echo $skPendirianSekolah; ?></Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s352"><Data ss:Type="String">RT</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s301"><Data ss:Type="String"><?php echo $rt; ?></Data></Cell>
    <Cell ss:StyleID="s74"/>
    <Cell ss:StyleID="s352"><Data ss:Type="String">Tgl SK Pendirian</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s301"><Data ss:Type="String"><?php echo $tanggalSkPendirian; ?></Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s352"><Data ss:Type="String">RW</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s301"><Data ss:Type="String"><?php echo $rw; ?></Data></Cell>
    <Cell ss:StyleID="s74"/>
    <Cell ss:StyleID="s352"><Data ss:Type="String">Status Kepemilikan</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s301"><Data ss:Type="String"><?php echo $statusKepemilikan; ?></Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s352"><Data ss:Type="String">Nama Dusun</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s301"><Data ss:Type="String"><?php echo $namaDusun; ?></Data></Cell>
    <Cell ss:StyleID="s74"/>
    <Cell ss:StyleID="s352"><Data ss:Type="String">SK Izin Operasional</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s301"><Data ss:Type="String"><?php echo $skIzinOperasional; ?></Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s352"><Data ss:Type="String">Desa/Kelurahan</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s301"><Data ss:Type="String"><?php echo $desaKelurahan; ?></Data></Cell>
    <Cell ss:StyleID="s74"/>
    <Cell ss:StyleID="s352"><Data ss:Type="String">Tgl SK Izin</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s301"><Data ss:Type="String"><?php echo $tanggalSkIzinOperasional ?></Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s352"><Data ss:Type="String">Kecamatan</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s301"><Data ss:Type="String"><?php echo $kecamatan; ?></Data></Cell>
    <Cell ss:StyleID="s74"/>
    <Cell ss:StyleID="s352"><Data ss:Type="String">SK Akreditasi</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s301"><Data ss:Type="String">-</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s352"><Data ss:Type="String">Kabupaten/Kota</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s301"><Data ss:Type="String"><?php echo $namaKab ?></Data></Cell>
    <Cell ss:StyleID="s74"/>
    <Cell ss:StyleID="s352"><Data ss:Type="String">Tgl SK Akreditasi</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s301"><Data ss:Type="String">-</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s352"><Data ss:Type="String">Propinsi</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s301"><Data ss:Type="String"><?php echo $namaProp; ?></Data></Cell>
    <Cell ss:StyleID="s74"/>
    <Cell ss:StyleID="s352"><Data ss:Type="String">No.Rekening</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s301"><Data ss:Type="String"><?php echo $nomorRekening ?></Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s352"><Data ss:Type="String">Kode Pos</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s301"><Data ss:Type="String"><?php echo $kodePos ?></Data></Cell>
    <Cell ss:StyleID="s74"/>
    <Cell ss:StyleID="s352"><Data ss:Type="String">Nama Bank</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s301"><Data ss:Type="String"><?php echo $namBank ?></Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s352"><Data ss:Type="String">Lintang</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s301"><Data ss:Type="String"><?php echo $lintang ?></Data></Cell>
    <Cell ss:StyleID="s74"/>
    <Cell ss:StyleID="s352"><Data ss:Type="String">Cabang KCP Unit</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s301"><Data ss:Type="String"><?php echo $cabangKcpUnit ?></Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s352"><Data ss:Type="String">Bujur</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s301"><Data ss:Type="String"><?php echo $bujur ?></Data></Cell>
    <Cell ss:StyleID="s74"/>
    <Cell ss:StyleID="s352"><Data ss:Type="String">MBS</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s301"><Data ss:Type="String"><?php echo $mbs ?></Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s74"/>
    <Cell ss:StyleID="s74"/>
    <Cell ss:StyleID="s74"/>
    <Cell ss:StyleID="s74"/>
    <Cell ss:StyleID="s74"/>
    <Cell ss:StyleID="s74"/>
    <Cell ss:StyleID="s352"><Data ss:Type="String">Luas Tanah Milik</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s301"><Data ss:Type="String"><?php echo $luasTanahMilik ?></Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:MergeAcross="4" ss:StyleID="s468"><Data ss:Type="String">PTK</Data></Cell>
    <Cell ss:StyleID="s74"/>
    <Cell ss:StyleID="s352"><Data ss:Type="String">Luas Tanah Bukan Milik</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s301"><Data ss:Type="String"><?php echo $luasTanahBukanMilik ?></Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s352"><Data ss:Type="String">PTK Terdaftar</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s393"><Data ss:Type="String"><?php echo $countPtk ?></Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s352"><Data ss:Type="String">PTK Laki-laki</Data></Cell>
    <Cell ss:StyleID="s300"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s393"><Data ss:Type="String"><?php echo $countPtkLaki ?></Data></Cell>
    <Cell ss:Index="7" ss:MergeAcross="4" ss:StyleID="s444"><Data ss:Type="String">KONTAK SEKOLAH</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s352"><Data ss:Type="String">PTK Perempuan</Data></Cell>
    <Cell ss:StyleID="s300"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s393"><Data ss:Type="String"><?php echo $countPtkPerempuan ?></Data></Cell>
    <Cell ss:Index="7" ss:StyleID="s352"><Data ss:Type="String">Nomor Telepon</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s469"><Data ss:Type="String"><?php echo $nomorTelepon ?></Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s352"><Data ss:Type="String">Guru</Data></Cell>
    <Cell ss:StyleID="s300"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s393"><Data ss:Type="String"><?php echo $countGuru ?></Data></Cell>
    <Cell ss:Index="7" ss:StyleID="s352"><Data ss:Type="String">Nomor Fax</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s393"><Data ss:Type="String"><?php echo $nomorFax ?></Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s352"><Data ss:Type="String">Pengawas</Data></Cell>
    <Cell ss:StyleID="s300"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s393"><Data ss:Type="String"><?php echo $countPengawas ?></Data></Cell>
    <Cell ss:Index="7" ss:StyleID="s352"><Data ss:Type="String">Email</Data></Cell>
    <Cell ss:StyleID="s352"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s393"><Data ss:Type="String"><?php echo $email ?></Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s352"><Data ss:Type="String">Pegawai Administrasi</Data></Cell>
    <Cell ss:StyleID="s300"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s393"><Data ss:Type="String"><?php echo $countTA ?></Data></Cell>
    <Cell ss:Index="7" ss:StyleID="s352"><Data ss:Type="String">Website</Data></Cell>
    <Cell ss:StyleID="s300"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s393"><Data ss:Type="String"><?php echo $website ?></Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625"/>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:MergeAcross="4" ss:StyleID="s468"><Data ss:Type="String">PESERTA DIDIK</Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s352"><Data ss:Type="String">-</Data></Cell>
    <Cell ss:StyleID="s300"><Data ss:Type="String"></Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s393"><Data ss:Type="String"></Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s352"><Data ss:Type="String">PD Total</Data></Cell>
    <Cell ss:StyleID="s300"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s393"><Data ss:Type="String"><?php echo $pdTotal ?></Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s352"><Data ss:Type="String">PD Laki-laki</Data></Cell>
    <Cell ss:StyleID="s300"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s393"><Data ss:Type="String"><?php echo $pdLaki ?></Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625">
    <Cell ss:StyleID="s352"><Data ss:Type="String">PD Perempuan</Data></Cell>
    <Cell ss:StyleID="s300"><Data ss:Type="String">:</Data></Cell>
    <Cell ss:MergeAcross="2" ss:StyleID="s393"><Data ss:Type="String"><?php echo $pdPerempuan ?></Data></Cell>
   </Row>
   <Row ss:AutoFitHeight="0" ss:Height="20.0625" ss:Span="10"/>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.25" x:Right="0.25" x:Top="0.75"/>
   </PageSetup>
   <Print>
    <ValidPrinterInfo/>
    <PaperSizeIndex>9</PaperSizeIndex>
    <HorizontalResolution>600</HorizontalResolution>
    <VerticalResolution>300</VerticalResolution>
   </Print>
   <Selected/>
   <Panes>
    <Pane>
     <Number>3</Number>
     <ActiveRow>1</ActiveRow>
     <RangeSelection>R2C1:R3C11</RangeSelection>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>
