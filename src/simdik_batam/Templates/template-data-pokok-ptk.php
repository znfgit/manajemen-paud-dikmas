<?php


echo '<?xml version="1.0"?>';
echo '<?mso-application progid="Excel.Sheet"?>';
?>
<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:o="urn:schemas-microsoft-com:office:office"
 xmlns:x="urn:schemas-microsoft-com:office:excel"
 xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"
 xmlns:html="http://www.w3.org/TR/REC-html40">
 <DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">
  <Author>Reyzal</Author>
  <LastAuthor>khalid</LastAuthor>
  <Created>2015-03-05T10:00:40Z</Created>
  <LastSaved>2015-03-07T17:13:52Z</LastSaved>
  <Version>15.00</Version>
 </DocumentProperties>
 <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">
  <AllowPNG/>
 </OfficeDocumentSettings>
 <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">
  <WindowHeight>7995</WindowHeight>
  <WindowWidth>16275</WindowWidth>
  <WindowTopX>240</WindowTopX>
  <WindowTopY>120</WindowTopY>
  <ProtectStructure>False</ProtectStructure>
  <ProtectWindows>False</ProtectWindows>
 </ExcelWorkbook>
 <Styles>
  <Style ss:ID="Default" ss:Name="Normal">
   <Alignment ss:Vertical="Bottom"/>
   <Borders/>
   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>
   <Interior/>
   <NumberFormat/>
   <Protection/>
  </Style>
  <Style ss:ID="s16">
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Microsoft Sans Serif" x:Family="Swiss" ss:Size="11"
    ss:Color="#FFFFFF"/>
   <Interior ss:Color="#00B0F0" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s17">
   <Alignment ss:Vertical="Bottom"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Microsoft Sans Serif" x:Family="Swiss" ss:Size="11"
    ss:Color="#FFFFFF"/>
   <Interior ss:Color="#00B0F0" ss:Pattern="Solid"/>
  </Style>
  <Style ss:ID="s19">
   <Font ss:FontName="Microsoft Sans Serif" x:Family="Swiss" ss:Size="11"
    ss:Color="#000000"/>
  </Style>
  <Style ss:ID="s20">
   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>
   <Borders>
    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"
     ss:Color="#000000"/>
   </Borders>
   <Font ss:FontName="Arial Black" x:Family="Swiss" ss:Size="18" ss:Color="#0D0D0D"
    ss:Bold="1"/>
   <Interior ss:Color="#95B3D7" ss:Pattern="Solid"/>
  </Style>
 </Styles>
 <Worksheet ss:Name="Sheet1">
  <Table ss:ExpandedColumnCount="48" ss:ExpandedRowCount="<?php echo ($count+4); ?>" x:FullColumns="1"
   x:FullRows="1" ss:DefaultRowHeight="15">
   <Column ss:AutoFitWidth="0" ss:Width="170.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="131.25"/>
   <Column ss:Width="69"/>
   <Column ss:AutoFitWidth="0" ss:Width="126"/>
   <Column ss:AutoFitWidth="0" ss:Width="89.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="111"/>
   <Column ss:AutoFitWidth="0" ss:Width="84.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="110.25"/>
   <Column ss:Width="117.75"/>
   <Column ss:Width="67.5"/>
   <Column ss:Width="51"/>
   <Column ss:AutoFitWidth="0" ss:Width="123.75"/>
   <Column ss:Width="20.25"/>
   <Column ss:Width="24.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="89.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="105.75"/>
   <Column ss:Width="71.25" ss:Span="2"/>
   <Column ss:Index="20" ss:Width="53.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="105"/>
   <Column ss:AutoFitWidth="0" ss:Width="100.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="107.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="129"/>
   <Column ss:AutoFitWidth="0" ss:Width="95.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="81"/>
   <Column ss:Width="79.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="120"/>
   <Column ss:Width="102"/>
   <Column ss:Width="124.5"/>
   <Column ss:Width="107.25"/>
   <Column ss:Width="126"/>
   <Column ss:Width="75.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="140.25"/>
   <Column ss:Width="97.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="114"/>
   <Column ss:Width="78"/>
   <Column ss:Width="109.5"/>
   <Column ss:AutoFitWidth="0" ss:Width="89.25"/>
   <Column ss:Width="152.25"/>
   <Column ss:Width="120.75"/>
   <Column ss:Width="149.25"/>
   <Column ss:Width="97.5"/>
   <Column ss:Width="78.75"/>
   <Column ss:Width="123.75"/>
   <Column ss:AutoFitWidth="0" ss:Width="98.25"/>
   <Column ss:Width="83.25"/>
   <Column ss:AutoFitWidth="0" ss:Width="150"/>
   <Row ss:Height="27">
    <Cell ss:MergeAcross="46" ss:StyleID="s20"/>
   </Row>
   <Row>
    <Cell ss:StyleID="s16"><Data ss:Type="String">Nama</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">NIP</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">Jenis kelamin</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">Tempat lahir</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">Tanggal lahir</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">NIK</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">Niy Nigk</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">NUPTK</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">Status Kepegawaian</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">Jenis PTK</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">Agama</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">Alamat jalan </Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">RT</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">RW</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">Nama dusun</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">Desa kelurahan</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">Kecamatan</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">Kabupaten</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">Propinsi</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">Kode pos</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">No telepon rumah</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">No HP</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">Email</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">Sekolah</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">Status Keaktifan</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">SK CPNS</Data></Cell>
    <Cell ss:StyleID="s16"><Data ss:Type="String">Tanggal CPNS</Data></Cell>
    <Cell ss:StyleID="s17"><Data ss:Type="String">SK pengangkatan</Data></Cell>
    <Cell ss:StyleID="s17"><Data ss:Type="String">TMT pengangkatan</Data></Cell>
    <Cell ss:StyleID="s17"><Data ss:Type="String">Lembaga Pengangkat</Data></Cell>
    <Cell ss:StyleID="s17"><Data ss:Type="String">Pangkat Golongan</Data></Cell>
    <Cell ss:StyleID="s17"><Data ss:Type="String">Keahlian Laboratorium</Data></Cell>
    <Cell ss:StyleID="s17"><Data ss:Type="String">Sumber Gaji</Data></Cell>
    <Cell ss:StyleID="s17"><Data ss:Type="String">Nama ibu kandung</Data></Cell>
    <Cell ss:StyleID="s17"><Data ss:Type="String">Status perkawinan</Data></Cell>
    <Cell ss:StyleID="s17"><Data ss:Type="String">Nama suami istri</Data></Cell>
    <Cell ss:StyleID="s17"><Data ss:Type="String">NIP suami istri</Data></Cell>
    <Cell ss:StyleID="s17"><Data ss:Type="String">Pekerjaan suami istri</Data></Cell>
    <Cell ss:StyleID="s17"><Data ss:Type="String">TMT PNS</Data></Cell>
    <Cell ss:StyleID="s17"><Data ss:Type="String">Sudah lisensi Kepala sekolah</Data></Cell>
    <Cell ss:StyleID="s17"><Data ss:Type="String">Jumlah sekolah binaan</Data></Cell>
    <Cell ss:StyleID="s17"><Data ss:Type="String">Pernah diklat kepengawasan</Data></Cell>
    <Cell ss:StyleID="s17"><Data ss:Type="String">Mampu handle KK</Data></Cell>
    <Cell ss:StyleID="s17"><Data ss:Type="String">Keahlian braille</Data></Cell>
    <Cell ss:StyleID="s17"><Data ss:Type="String">Keahlian bahasa isyarat</Data></Cell>
    <Cell ss:StyleID="s17"><Data ss:Type="String">NPWP</Data></Cell>
    <Cell ss:StyleID="s17"><Data ss:Type="String">Kewarnegaraan</Data></Cell>
   </Row>
   <?php echo $str; ?>
   <Row>
    <Cell ss:Index="14"><Data ss:Type="String"> </Data></Cell>
   </Row>
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <Print>
    <ValidPrinterInfo/>
    <HorizontalResolution>-3</HorizontalResolution>
    <VerticalResolution>-3</VerticalResolution>
   </Print>
   <Selected/>
   <FreezePanes/>
   <FrozenNoSplit/>
   <SplitHorizontal>2</SplitHorizontal>
   <TopRowBottomPane>2</TopRowBottomPane>
   <SplitVertical>1</SplitVertical>
   <LeftColumnRightPane>1</LeftColumnRightPane>
   <ActivePane>0</ActivePane>
   <Panes>
    <Pane>
     <Number>3</Number>
    </Pane>
    <Pane>
     <Number>1</Number>
    </Pane>
    <Pane>
     <Number>2</Number>
    </Pane>
    <Pane>
     <Number>0</Number>
     <ActiveRow>3</ActiveRow>
    </Pane>
   </Panes>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
 <Worksheet ss:Name="Sheet2">
  <Table ss:ExpandedColumnCount="1" ss:ExpandedRowCount="1" x:FullColumns="1"
   x:FullRows="1" ss:DefaultRowHeight="15">
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
 <Worksheet ss:Name="Sheet3">
  <Table ss:ExpandedColumnCount="1" ss:ExpandedRowCount="1" x:FullColumns="1"
   x:FullRows="1" ss:DefaultRowHeight="15">
  </Table>
  <WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">
   <PageSetup>
    <Header x:Margin="0.3"/>
    <Footer x:Margin="0.3"/>
    <PageMargins x:Bottom="0.75" x:Left="0.7" x:Right="0.7" x:Top="0.75"/>
   </PageSetup>
   <ProtectObjects>False</ProtectObjects>
   <ProtectScenarios>False</ProtectScenarios>
  </WorksheetOptions>
 </Worksheet>
</Workbook>
