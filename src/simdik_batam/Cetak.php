<?php
namespace simdik_batam;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use simdik_batam\Model\SekolahPeer;
use simdik_batam\Model\Sekolah;
use simdik_batam\Model\MstWilayahPeer;
use simdik_batam\Model\MstWilayah;
use simdik_batam\Model\Pengguna;
use simdik_batam\Model\PenggunaPeer;
use simdik_batam\Model\BentukPendidikanPeer;
use simdik_batam\Model\StatusKepemilikanPeer;
use simdik_batam\Model\PtkPeer;
use simdik_batam\Model\PtkTerdaftarPeer;
use simdik_batam\Model\PesertaDidikPeer;
use simdik_batam\Model\RegistrasiPesertaDidikPeer;
use simdik_batam\Model\AnggotaRombelPeer;
use simdik_batam\Model\RombonganBelajarPeer;
use simdik_batam\Model\TahunAjaranPeer;

class Cetak{

	function get(Request $request, Application $app){
		$ref = System::databaseCompatibility();

		$model = $request->get('model');
		$today = date('Y-m-d H:i:s');
		$id = $request->get('id');
		$tahun_ajaran_id = $request->get('tahun_ajaran_id') ? $tahun_ajaran_id : TA_BERJALAN;
		$tipe = $request->get('tipe');
		
		$str = '';
		$arr = array();
		$fetchArr = array();

		switch ($model) {
			case 'Mutasi':
				
				$sql = "SELECT
							s.nama as nama_sekolah,
							w.nama as kode_wilayah_berangkat_str,
							pg.nama as pengguna_id_str,
							pg.jabatan_lembaga,
							pg.nip_nim,
							js.nama as jenis_surat_id_str,
							m.*
						FROM
							adm.mutasi m
						JOIN sekolah s ON m.sekolah_id = s.sekolah_id
						JOIN {$ref}mst_wilayah w on w.kode_wilayah = m.kode_wilayah_berangkat
						JOIN pengguna pg on pg.pengguna_id = m.pengguna_id
						JOIN adm.jenis_surat js on js.jenis_surat_id = m.jenis_surat_id
						WHERE
							m.mutasi_id = '".$id."'";
				$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;
					}

					$count++;
					
					array_push($fetchArr, $arr);
				}

				// return $sql;die;
				
				if($tipe == 'pdf'){
					$today = date('Y-m-d h:i:s');

					include "../src/simdik_batam/mpdf/mpdf.php";

					$mpdf=new \mPDF('c');
					$mpdf->SetTitle('Surat Mutasi');
					$mpdf->mirrorMargins = 1;	// Use different Odd/Even headers and footers and mirror margins
						
					// $footer = '<table><tr>
					// 		<td align="left"><span><i>Surat Mutasi</i></span></td>
					// 		<td align="right" width="100%"></td>
					// 	</tr></table>';
				
					// $mpdf->SetHTMLFooter($footer);
					// $mpdf->SetHTMLFooter($footer,'E');
				
					ob_start();
					include "Templates/template-surat-mutasi.php";

					$content = ob_get_clean();
				
					// $stylesheet = file_get_contents(dirname(__FILE__).D.'Templates'.D.'style.css');
					// $mpdf->imgdisdik = file_get_contents(dirname(__FILE__).D.'Templates'.D.'disdik.png');
					// $mpdf->WriteHTML($stylesheet,1);
					$mpdf->WriteHTML($content);
					$mpdf->Output('Surat-Mutasi-'.$fetchArr[0]['nama'].'-['.$today.'].pdf');
					exit;
				}else{
					include "Templates/template-surat-mutasi.php";

				}

				break;

		}

		return false;
	}

}