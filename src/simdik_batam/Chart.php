<?php
namespace simdik_batam;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use simdik_batam\System;

class Chart
{	
	public function initdb(){
		$serverName = "(local)";
		$connectionInfo = array( "Database"=>"rekap_dapodikmen", "UID"=>"report", "PWD"=>"ReportDikmen",'ReturnDatesAsStrings'=>true);
		/* Connect using Windows Authentication. */
		$con_rekap = sqlsrv_connect( $serverName, $connectionInfo);

		return $con_rekap;
	}



	// get cache

	function getCache(Request $request, Application $app){
		$ref = System::databaseCompatibility();

		$chart = $request->get('chart');
		$model = $request->get('model');
		$tahun_ajaran_id	= $request->get('tahun_ajaran_id') ? $request->get('tahun_ajaran_id') : TA_BERJALAN;
		$semester_id	= $request->get('semester_id') ? $request->get('semester_id') : SEMESTER_BERJALAN;

		$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
		$params_bp = System::getParamsBp($bentuk_pendidikan_id);

		$session_kode_wilayah = $request->get('kode_wilayah') ? $request->get('kode_wilayah') : $app['session']->get('kode_wilayah');
		$session_id_level_wilayah = $request->get('id_level_wilayah') ? $request->get('id_level_wilayah') : $app['session']->get('id_level_wilayah');

		$con = $this->initdb();

		$return = array();
		$finalArr = array();

		switch ($model) {
			case 'ProgresData':

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = 's.provinsi';
						$group_wilayah_1 = 's.kode_wilayah_provinsi';
						$group_wilayah_2 = 's.id_level_wilayah_provinsi';
						$group_wilayah_3 = 's.mst_kode_wilayah_provinsi';
						$params_wilayah ='';
						break;
					case 1:
						$col_wilayah = 's.kabupaten';
						$group_wilayah_1 = 's.kode_wilayah_kabupaten';
						$group_wilayah_2 = 's.id_level_wilayah_kabupaten';
						$group_wilayah_3 = 's.mst_kode_wilayah_kabupaten';
						$params_wilayah = ' and s.mst_kode_wilayah_kabupaten = '.$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = 's.kecamatan';
						$group_wilayah_1 = 's.kode_wilayah_kecamatan';
						$group_wilayah_2 = 's.id_level_wilayah_kecamatan';
						$group_wilayah_3 = 's.mst_kode_wilayah_kecamatan';
						$params_wilayah = ' and s.mst_kode_wilayah_kecamatan = '.$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$group_wilayah_1} AS kode_wilayah,
							{$col_wilayah} AS nama,
							{$group_wilayah_3} AS mst_kode_wilayah,
							{$group_wilayah_2} AS id_level_wilayah,
							SUM (
								CASE
								WHEN bentuk_pendidikan_id = 13 THEN
									1
								ELSE
									0
								END
							) AS sma,
							SUM (
								CASE
								WHEN pd > 0
								AND bentuk_pendidikan_id = 13 THEN
									1
								ELSE
									0
								END
							) AS kirim_sma,
							SUM (
								CASE
								WHEN bentuk_pendidikan_id = 15 THEN
									1
								ELSE
									0
								END
							) AS smk,
							SUM (
								CASE
								WHEN pd > 0
								AND bentuk_pendidikan_id = 15 THEN
									1
								ELSE
									0
								END
							) AS kirim_smk,
							SUM (
								CASE
								WHEN bentuk_pendidikan_id = 14 THEN
									1
								ELSE
									0
								END
							) AS smlb,
							SUM (
								CASE
								WHEN pd > 0
								AND bentuk_pendidikan_id = 14 THEN
									1
								ELSE
									0
								END
							) AS kirim_smlb,
							SUM (1) AS total,
							SUM (CASE WHEN pd > 0 THEN 1 ELSE 0 END) AS kirim_total,
							(
									(
										SUM(CASE WHEN s.bentuk_pendidikan_id=13 AND pd > 0 THEN 1 ELSE 0 END) + 
										SUM(CASE WHEN s.bentuk_pendidikan_id=15 AND pd > 0 THEN 1 ELSE 0 END) +
										SUM(CASE WHEN s.bentuk_pendidikan_id=14 AND pd > 0 THEN 1 ELSE 0 END)
									) /
									CAST(
										(SUM(CASE WHEN s.bentuk_pendidikan_id=13 THEN 1 ELSE 0 END) + 
										SUM(CASE WHEN s.bentuk_pendidikan_id=15 THEN 1 ELSE 0 END) +
										SUM(CASE WHEN s.bentuk_pendidikan_id=14 THEN 1 ELSE 0 END)) as NUMERIC(9,2)
									)
								) * 100 as persen
						FROM
							rekap_sekolah s
						WHERE
						semester_id = {$semester_id}
						{$params_bp}
						{$params_wilayah}
						GROUP BY
							{$group_wilayah_1},
							{$col_wilayah},
							{$group_wilayah_2},
							{$group_wilayah_3}
						order by
							persen desc";

				// return $sql;die;

				$stmt = sqlsrv_query( $con, $sql );

				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					array_push($return, $row);

					$rowCount++;
				}
				
				break;
			case 'SekolahRangkumanSp':
				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = 's.provinsi';
						$group_wilayah_1 = 's.kode_wilayah_provinsi';
						$group_wilayah_2 = 's.id_level_wilayah_provinsi';
						$params_wilayah ='';
						break;
					case 1:
						$col_wilayah = 's.kabupaten';
						$group_wilayah_1 = 's.kode_wilayah_kabupaten';
						$group_wilayah_2 = 's.id_level_wilayah_kabupaten';
						$params_wilayah = ' and s.mst_kode_wilayah_kabupaten = '.$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = 's.kecamatan';
						$group_wilayah_1 = 's.kode_wilayah_kecamatan';
						$group_wilayah_2 = 's.id_level_wilayah_kecamatan';
						$params_wilayah = ' and s.mst_kode_wilayah_kecamatan = '.$session_kode_wilayah;
						break;
					case 3:
						$params_wilayah = ' and s.kode_wilayah_kecamatan = '.$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							nama as desk,
							s.*,
							(s.ptk + s.pegawai) as ptk_total
						FROM
							rekap_sekolah s
						WHERE
							s.semester_id = {$semester_id}
							{$params_wilayah}
							{$params_bp}
						order by s.nama";

				// return $sql;die;

				$stmt = sqlsrv_query( $con, $sql );

				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					array_push($return, $row);

					$rowCount++;
				}

				break;
			
			case 'SekolahRangkuman':
				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = 's.provinsi';
						$group_wilayah_1 = 's.kode_wilayah_provinsi';
						$group_wilayah_2 = 's.id_level_wilayah_provinsi';
						$params_wilayah ='';
						break;
					case 1:
						$col_wilayah = 's.kabupaten';
						$group_wilayah_1 = 's.kode_wilayah_kabupaten';
						$group_wilayah_2 = 's.id_level_wilayah_kabupaten';
						$params_wilayah = ' and s.mst_kode_wilayah_kabupaten = '.$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = 's.kecamatan';
						$group_wilayah_1 = 's.kode_wilayah_kecamatan';
						$group_wilayah_2 = 's.id_level_wilayah_kecamatan';
						$params_wilayah = ' and s.mst_kode_wilayah_kecamatan = '.$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} AS desk,
							{$group_wilayah_1} AS kode_wilayah,
							{$group_wilayah_2} AS id_level_wilayah,
							sum(s.pd_kelas_10) as pd_kelas_10,
							sum(s.pd_kelas_11) as pd_kelas_11,
							sum(s.pd_kelas_12) as pd_kelas_12,
							sum(s.pd_kelas_13) as pd_kelas_13,
							sum(s.pd) as pd,
							sum(s.ptk) as ptk,
							sum(s.ptk + s.pegawai) as ptk_total,
							sum(s.pegawai) as pegawai,
							sum(s.rombel) as rombel
						FROM
							rekap_sekolah s
						where 
							s.semester_id = {$semester_id}
							{$params_bp}
							{$params_wilayah}
						GROUP BY
							{$group_wilayah_1},
							{$group_wilayah_2},
							{$col_wilayah}";

				// return $sql;die;

				$stmt = sqlsrv_query( $con, $sql );

				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					array_push($return, $row);

					$rowCount++;
				}

				break;
			case 'PdTingkatKelasJenisKelamin':
				// return "tes";die;

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = 's.provinsi';
						$group_wilayah_1 = 's.kode_wilayah_provinsi';
						$group_wilayah_2 = 's.id_level_wilayah_provinsi';
						$params_wilayah ='';
						break;
					case 1:
						$col_wilayah = 's.kabupaten';
						$group_wilayah_1 = 's.kode_wilayah_kabupaten';
						$group_wilayah_2 = 's.id_level_wilayah_kabupaten';
						$params_wilayah = ' and s.mst_kode_wilayah_kabupaten = '.$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = 's.kecamatan';
						$group_wilayah_1 = 's.kode_wilayah_kecamatan';
						$group_wilayah_2 = 's.id_level_wilayah_kecamatan';
						$params_wilayah = ' and s.mst_kode_wilayah_kecamatan = '.$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} AS desk,
							{$group_wilayah_1} AS kode_wilayah,
							{$group_wilayah_2} AS id_level_wilayah,
							sum(s.pd_kelas_10) as pd_kelas_10,
							sum(s.pd_kelas_10_laki) as pd_kelas_10_laki,
							sum(s.pd_kelas_10_perempuan) as pd_kelas_10_perempuan,
							sum(s.pd_kelas_11) as pd_kelas_11,
							sum(s.pd_kelas_11_laki) as pd_kelas_11_laki,
							sum(s.pd_kelas_11_perempuan) as pd_kelas_11_perempuan,
							sum(s.pd_kelas_12) as pd_kelas_12,
							sum(s.pd_kelas_12_laki) as pd_kelas_12_laki,
							sum(s.pd_kelas_12_perempuan) as pd_kelas_12_perempuan,
							sum(s.pd_kelas_13) as pd_kelas_13,
							sum(s.pd_kelas_13_laki) as pd_kelas_13_laki,
							sum(s.pd_kelas_13_perempuan) as pd_kelas_13_perempuan
						FROM
							rekap_sekolah s
						where 
							s.semester_id = {$semester_id}
							{$params_bp}
							{$params_wilayah}
						GROUP BY
							{$group_wilayah_1},
							{$group_wilayah_2},
							{$col_wilayah}";

				// return $sql;die;

				$stmt = sqlsrv_query( $con, $sql );

				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					array_push($return, $row);

					$rowCount++;
				}

				break;
			case 'SekolahPerStatus':
				switch ($session_id_level_wilayah) {
					case 0:
						$params_wilayah ='';
						break;
					case 1:
						$params_wilayah = ' and s.mst_kode_wilayah_kabupaten = '.$session_kode_wilayah;
						break;
					case 2:
						$params_wilayah = ' and s.mst_kode_wilayah_kecamatan = '.$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
					COUNT (1) AS jumlah,
					(case when s.status_sekolah = 1 then 'NEGERI' else 'SWASTA' end) as desk
				FROM
					rekap_sekolah s
				where 
					s.semester_id = {$semester_id}
					{$params_bp}
					{$params_wilayah}
				group by s.status_sekolah";
				
				$stmt = sqlsrv_query( $con, $sql );

				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					array_push($return, $row);

					$rowCount++;
				}

				break;
			case 'SebaranSekolahPerStatus':
				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = 's.provinsi';
						$group_wilayah_1 = 's.kode_wilayah_provinsi';
						$params_wilayah ='';
						break;
					case 1:
						$col_wilayah = 's.kabupaten';
						$group_wilayah_1 = 's.kode_wilayah_kabupaten';
						$params_wilayah = ' and s.mst_kode_wilayah_kabupaten = '.$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = 's.kecamatan';
						$group_wilayah_1 = 's.kode_wilayah_kecamatan';
						$params_wilayah = ' and s.mst_kode_wilayah_kecamatan = '.$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} AS desk,
							sum(case when s.status_sekolah = 1 then 1 else 0 end) as negeri,
							sum(case when s.status_sekolah = 2 then 1 else 0 end) as swasta
						FROM
							rekap_sekolah s
						where 
							s.semester_id = {$semester_id}
							{$params_bp}
							{$params_wilayah}
						GROUP BY
							{$group_wilayah_1},
							{$col_wilayah}";

				$stmt = sqlsrv_query( $con, $sql );

				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					array_push($return, $row);

					$rowCount++;
				}

				break;
			
			case 'SekolahPerBentukPendidikan':
				switch ($session_id_level_wilayah) {
					case 0:
						$params_wilayah ='';
						break;
					case 1:
						$params_wilayah = ' and s.mst_kode_wilayah_kabupaten = '.$session_kode_wilayah;
						break;
					case 2:
						$params_wilayah = ' and s.mst_kode_wilayah_kecamatan = '.$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
					COUNT (1) AS jumlah,
					(case 
						when 
							s.bentuk_pendidikan_id = 13 then 'SMA' 
						when 
							s.bentuk_pendidikan_id = 14 then 'SMLB'
						when
							s.bentuk_pendidikan_id = 15 then 'SMK'
						when
							s.bentuk_pendidikan_id = 29 then 'SLB' 
					end) as desk
				FROM
					rekap_sekolah s
				where 
					s.semester_id = {$semester_id}
					{$params_bp}
					{$params_wilayah}
				group by s.bentuk_pendidikan_id";
				
				$stmt = sqlsrv_query( $con, $sql );

				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					array_push($return, $row);

					$rowCount++;
				}

				// return $sql;

				break;

			case 'SebaranSekolahPerBentukPendidikan':
				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = 's.provinsi';
						$group_wilayah_1 = 's.kode_wilayah_provinsi';
						$params_wilayah ='';
						break;
					case 1:
						$col_wilayah = 's.kabupaten';
						$group_wilayah_1 = 's.kode_wilayah_kabupaten';
						$params_wilayah = ' and s.mst_kode_wilayah_kabupaten = '.$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = 's.kecamatan';
						$group_wilayah_1 = 's.kode_wilayah_kecamatan';
						$params_wilayah = ' and s.mst_kode_wilayah_kecamatan = '.$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} AS desk,
							sum(case when s.bentuk_pendidikan_id = 13 then 1 else 0 end) as sma,
							sum(case when s.bentuk_pendidikan_id = 14 then 1 else 0 end) as smlb,
							sum(case when s.bentuk_pendidikan_id = 15 then 1 else 0 end) as smk,
							sum(case when s.bentuk_pendidikan_id = 29 then 1 else 0 end) as slb
						FROM
							rekap_sekolah s
						where 
							s.semester_id = {$semester_id}
							{$params_bp}
							{$params_wilayah}
						GROUP BY
							{$group_wilayah_1},
							{$col_wilayah}";

				$stmt = sqlsrv_query( $con, $sql );

				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					array_push($return, $row);

					$rowCount++;
				}

				break;
			case 'PesertaDidikPerJenisKelamin':
				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = 's.provinsi';
						$group_wilayah_1 = 's.kode_wilayah_provinsi';
						$params_wilayah ='';
						break;
					case 1:
						$col_wilayah = 's.kabupaten';
						$group_wilayah_1 = 's.kode_wilayah_kabupaten';
						$params_wilayah = ' and s.mst_kode_wilayah_kabupaten = '.$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = 's.kecamatan';
						$group_wilayah_1 = 's.kode_wilayah_kecamatan';
						$params_wilayah = ' and s.mst_kode_wilayah_kecamatan = '.$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} AS desk,
							(sum(pd_kelas_10_laki) + sum(pd_kelas_11_laki) + sum(pd_kelas_12_laki) + sum(pd_kelas_13_laki)) as 'laki-laki',
							(sum(pd_kelas_10_perempuan) + sum(pd_kelas_11_perempuan) + sum(pd_kelas_12_perempuan) + sum(pd_kelas_13_perempuan)) as 'perempuan'
						FROM
							rekap_sekolah s
						where 
							s.semester_id = {$semester_id}
							{$params_bp}
							{$params_wilayah}
						GROUP BY
							{$group_wilayah_1},
							{$col_wilayah}";

							// return $sql;die;

				$stmt = sqlsrv_query( $con, $sql );

				$laki_total = 0;

				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					$laki +=  $row['laki-laki'];
					$perempuan +=  $row['perempuan'];
				}

				$arr = array();

				$arr['desk'] = 'Laki-laki';
				$arr['jumlah'] = $laki;

				array_push($return, $arr);


				$arr['desk'] = 'Perempuan';
				$arr['jumlah'] = $perempuan;

				array_push($return, $arr);

				$rowCount = 2;

				break;
			case 'SebaranPdJenisKelamin':
				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = 's.provinsi';
						$group_wilayah_1 = 's.kode_wilayah_provinsi';
						$params_wilayah ='';
						break;
					case 1:
						$col_wilayah = 's.kabupaten';
						$group_wilayah_1 = 's.kode_wilayah_kabupaten';
						$params_wilayah = ' and s.mst_kode_wilayah_kabupaten = '.$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = 's.kecamatan';
						$group_wilayah_1 = 's.kode_wilayah_kecamatan';
						$params_wilayah = ' and s.mst_kode_wilayah_kecamatan = '.$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} AS desk,
							(sum(pd_kelas_10_laki) + sum(pd_kelas_11_laki) + sum(pd_kelas_12_laki) + sum(pd_kelas_13_laki)) as 'laki-laki',
							(sum(pd_kelas_10_perempuan) + sum(pd_kelas_11_perempuan) + sum(pd_kelas_12_perempuan) + sum(pd_kelas_13_perempuan)) as 'perempuan'
						FROM
							rekap_sekolah s
						where 
							s.semester_id = {$semester_id}
							{$params_bp}
							{$params_wilayah}
						GROUP BY
							{$group_wilayah_1},
							{$col_wilayah}";

							// return $sql;die;

				$stmt = sqlsrv_query( $con, $sql );

				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					array_push($return, $row);

					$rowCount++;
				}
				break;
			default:
				
				break;
		}

		$finalArr['results'] = $rowCount;
		$finalArr['rows'] = $return;

		return json_encode($finalArr);		
	}




	// get real time

	function get(Request $request, Application $app){
		$ref = System::databaseCompatibility();

		$chart = $request->get('chart');
		$model = $request->get('model');
		$tahun_ajaran_id	= $request->get('tahun_ajaran_id') ? $request->get('tahun_ajaran_id') : TA_BERJALAN;
		$semester_id	= $request->get('semester_id') ? $request->get('semester_id') : SEMESTER_BERJALAN;

		$bentukPendidikanId = $request->get('bentuk_pendidikan_id');

		$session_kode_wilayah = $app['session']->get('kode_wilayah');
		$session_id_level_wilayah = $app['session']->get('id_level_wilayah');
		
		switch($model){
			case 'SebaranPdAngkaSiswaMiskin':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} as desk,
							sum(case when pd.penerima_KPS = 1 then 1 else 0 end) as jumlah
						FROM
							peserta_didik pd
						JOIN registrasi_peserta_didik rpd ON pd.peserta_didik_id = rpd.peserta_didik_id
						JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
						JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
						JOIN sekolah s on s.sekolah_id = rb.sekolah_id
						JOIN {$ref}mst_wilayah w1 on w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 on w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 on w3.kode_wilayah = w2.mst_kode_wilayah
						WHERE
							pd.Soft_delete = 0
						AND rpd.Soft_delete = 0
						AND ar.Soft_delete = 0
						AND rb.Soft_delete = 0
						{$params_bp}
						AND rpd.jenis_keluar_id IS NULL
						AND rb.semester_id = {$semester_id}
						{$params_wilayah} GROUP BY {$col_wilayah}";

				break;
			case 'SebaranPrasaranaPerJenis':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				$sql = "select * from {$ref}mst_wilayah where mst_kode_wilayah = {$session_kode_wilayah}";

				$fetch = getDataBySql($sql);

			 	$columns = '';

			 	switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.kode_wilayah";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.kode_wilayah";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.kode_wilayah";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}


				foreach ($fetch as $f) {
					
					// $nama_wilayah = str_replace(' ','_',strtolower(str_replace('Kec. ', '', $f['nama'])));

					$columns .= "sum(case when {$col_wilayah} = ".$f['kode_wilayah']." then 1 else 0 end) as col_".$f['kode_wilayah'].",";
					
				}

				$sql = "SELECT
							jp.jenis_prasarana_id,
							jp.nama,
							{$columns}
							count(1) as total
						FROM
							prasarana p
						JOIN {$ref}jenis_prasarana jp ON jp.jenis_prasarana_id = p.jenis_prasarana_id
						JOIN sekolah s ON s.sekolah_id = p.sekolah_id
						JOIN {$ref}mst_wilayah w1 ON w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						where p.soft_delete = 0
						and s.soft_delete = 0
						{$params_bp}
						{$params_wilayah}
						GROUP BY
							jp.jenis_prasarana_id,
							jp.nama";

				break;

			case 'SebaranPdPerJumlahRombel':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);
				// $sql = "SELECT
				// 			s.nama,
				// 			count(1) AS total,
				// 			sum(case when (select count(1) from anggota_rombel where rombongan_belajar_id = rb.rombongan_belajar_id) < 10 then 1 else 0 END) as kurang_10,
				// 			sum(case when (select count(1) from anggota_rombel where rombongan_belajar_id = rb.rombongan_belajar_id) >= 10 AND (select count(1) from anggota_rombel where rombongan_belajar_id = rb.rombongan_belajar_id) < 20 then 1 else 0 END) as sepuluh_duapuluh,
				// 			sum(case when (select count(1) from anggota_rombel where rombongan_belajar_id = rb.rombongan_belajar_id) >= 20 AND (select count(1) from anggota_rombel where rombongan_belajar_id = rb.rombongan_belajar_id) <= 32 then 1 else 0 END) as duapuluhsatu_tigapuluhdua,
				// 			sum(case when (select count(1) from anggota_rombel where rombongan_belajar_id = rb.rombongan_belajar_id) > 32 then 1 else 0 END) as lebih_32
				// 		FROM
				// 			rombongan_belajar rb
				// 		join sekolah s on s.sekolah_id = rb.sekolah_id
				// 		join mst_wilayah w on w.kode_wilayah = s.kode_wilayah
				// 		where rb.semester_id = 20141
				// 		GROUP BY
				// 				s.nama";
				$sql = "SELECT
							rr.*,
							s.bentuk_pendidikan_id
						FROM
							rekap_rombel_per_jumlah rr
						JOIN sekolah s ON s.sekolah_id = rr.sekolah_id
						where s.Soft_delete = 0
						{$params_bp}";

				// if(!empty($bentukPendidikanId)){
				// 	$sql .= " and s.bentuk_pendidikan_id = ".$bentukPendidikanId;
				// }

				// return $sql;die;
				break;
			case 'SebaranPdAngkaMengulang':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} as desk,
							count(1) as jumlah
						FROM
							peserta_didik pd
						JOIN registrasi_peserta_didik rpd ON rpd.peserta_didik_id = pd.peserta_didik_id
						JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
						JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
						JOIN sekolah s ON rpd.sekolah_id = s.sekolah_id
						JOIN {$ref}mst_wilayah w1 ON w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						WHERE
							pd.Soft_delete = 0
						AND rpd.Soft_delete = 0
						AND jenis_keluar_id IS NULL
						AND ar.jenis_pendaftaran_id = 5
						AND rb.semester_id = {$semester_id}
						{$params_bp}
						{$params_wilayah}
						group by {$col_wilayah}
						order by {$col_wilayah}";
				break;
			case 'SebaranPdAngkaPutusSekolah':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} as desk,
							count(1) as jumlah
						FROM
							peserta_didik pd
						JOIN registrasi_peserta_didik rpd ON rpd.peserta_didik_id = pd.peserta_didik_id
						JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
						JOIN sekolah s ON rpd.sekolah_id = s.sekolah_id
						JOIN {$ref}mst_wilayah w1 ON w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						WHERE
							pd.Soft_delete = 0
						AND rpd.Soft_delete = 0
						AND jenis_keluar_id = '5'
						{$params_bp}
						{$params_wilayah}
						group by {$col_wilayah}
						order by {$col_wilayah}";
				break;
			case 'SebaranPdPerUmur':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				if(DBMS == 'mssql'){
					$kriteria = "sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) < 7 then 1 else 0 end) as kurang_tujuh,
								sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) >= 7 AND (DATEDIFF(hour,pd.tanggal_lahir,GETDATE())/8766) <= 12 then 1 else 0 end) as tujuh_duabelas,
								sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) > 12 then 1 else 0 end) as lebih_duabelas,
								sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) < 15 then 1 else 0 end) as kurang_limabelas,
								sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) >= 15 AND (DATEDIFF(hour,pd.tanggal_lahir,GETDATE())/8766) <= 18 then 1 else 0 end) as limabelas_delapanbelas,
								sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) > 18 then 1 else 0 end) as lebih_delapanbelas,
								sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) < 19 then 1 else 0 end) as kurang_sembilanbelas,
								sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) >= 19 AND (DATEDIFF(hour,pd.tanggal_lahir,GETDATE())/8766) <= 21 then 1 else 0 end) as sembilanbelas_duapuluhsatu,
								sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) > 21 then 1 else 0 end) as lebih_duapuluhsatu";
				}else{
					$kriteria = "sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) < 7 then 1 else 0 end) as kurang_tujuh,
								sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) >= 7 AND (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) <= 12 then 1 else 0 end) as tujuh_duabelas,
								sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) > 12 then 1 else 0 end) as lebih_duabelas,
								sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) < 13 then 1 else 0 end) as kurang_tigabelas,
								sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) >= 13 AND (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) <= 18 then 1 else 0 end) as tigabelas_delapanbelas,
								sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) > 18 then 1 else 0 end) as lebih_delapanbelas,
								sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) < 19 then 1 else 0 end) as kurang_sembilanbelas,
								sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) >= 19 AND (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) <= 21 then 1 else 0 end) as sembilanbelas_duapuluhsatu,
								sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) > 21 then 1 else 0 end) as lebih_duapuluhsatu";
				}

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							s.sekolah_id,
							s.bentuk_pendidikan_id,
							s.nama,
							w1.nama as kecamatan,
							w2.nama as kabupaten,
							-- w1.kode_wilayah AS kode_wilayah_kecamatan,
							-- w1.nama as kecamatan,
							count(1) AS total,
							{$kriteria}
						FROM
							peserta_didik pd
						JOIN registrasi_peserta_didik rpd ON pd.peserta_didik_id = rpd.peserta_didik_id
						JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
						JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
						JOIN sekolah s ON s.sekolah_id = rpd.sekolah_id
						JOIN {$ref}mst_wilayah w1 ON w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						WHERE
							pd.Soft_delete = 0
						AND rpd.Soft_delete = 0
						AND s.Soft_delete = 0
						AND rpd.jenis_keluar_id IS NULL
						AND rb.semester_id = {$semester_id}
						{$params_bp}
						{$params_wilayah}
						GROUP BY
							s.sekolah_id,
							s.bentuk_pendidikan_id,
							s.nama,
							w1.nama,
							w2.nama
						order BY
							w2.nama, w1.nama, s.nama asc";

							// return $sql;die;
				break;
			case 'SebaranPtkPerMataPelajaran':
				
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$tahun_ajaran_id = substr($semester_id, 0,4);

				$sql = "SELECT
							{$col_wilayah} as desk,
							sum(case when pb.mata_pelajaran_id = 2 or pb.mata_pelajaran_id = 4020 then 1 else 0 end) as guru_kelas_sd,
							sum(case when pb.mata_pelajaran_id = 3 or pb.mata_pelajaran_id = 101001000 then 1 else 0 end) as pendidikan_agama,
							sum(case when pb.mata_pelajaran_id = 4 or pb.mata_pelajaran_id = 2000 or pb.mata_pelajaran_id = 200010000 then 1 else 0 end) as pkn,
							sum(case when pb.mata_pelajaran_id = 5 or pb.mata_pelajaran_id = 3010 or pb.mata_pelajaran_id = 300110000 then 1 else 0 end) as bahasa_indonesia,
							sum(case when pb.mata_pelajaran_id = 7 or pb.mata_pelajaran_id = 3020 or pb.mata_pelajaran_id = 300210000 then 1 else 0 end) as bahasa_inggris,
							sum(case when pb.mata_pelajaran_id = 8 or pb.mata_pelajaran_id = 6000 or pb.mata_pelajaran_id = 600030000 then 1 else 0 end) as seni_budaya,
							sum(case when pb.mata_pelajaran_id = 9 or pb.mata_pelajaran_id = 500010000 then 1 else 0 end) as pjok,
							sum(case when pb.mata_pelajaran_id = 10 or pb.mata_pelajaran_id = 802000300 then 1 else 0 end) as tik,
							sum(case when pb.mata_pelajaran_id = 11 or pb.mata_pelajaran_id = 4200 or pb.mata_pelajaran_id = 401000000 then 1 else 0 end) as matematika,
							sum(case when pb.mata_pelajaran_id = 12 or pb.mata_pelajaran_id = 4300 or pb.mata_pelajaran_id = 401100000 then 1 else 0 end) as ipa,
							sum(case when pb.mata_pelajaran_id = 13 or pb.mata_pelajaran_id = 4400 or pb.mata_pelajaran_id = 401200000 then 1 else 0 end) as ips
						FROM
							ptk ptk
						JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
						JOIN (select DISTINCT(ptk_terdaftar_id),mata_pelajaran_id,soft_delete,semester_id from pembelajaran WHERE Soft_delete = 0) pb on pb.ptk_terdaftar_id = ptkd.ptk_terdaftar_id
						JOIN sekolah s on s.sekolah_id = ptkd.sekolah_id
						JOIN {$ref}mst_wilayah w1 on w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 on w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 on w3.kode_wilayah = w2.mst_kode_wilayah
						WHERE
							ptk.Soft_delete = 0
						AND ptkd.Soft_delete = 0
						AND pb.Soft_delete = 0
						AND ptkd.jenis_keluar_id IS NULL
						AND ptkd.tahun_ajaran_id = {$tahun_ajaran_id}
						AND pb.semester_id = {$semester_id}
						{$params_bp}
						{$params_wilayah}
						group by {$col_wilayah}
						order by {$col_wilayah} asc";

						// return $sql;die;

				break;
			case 'SebaranPtkStatusKepegawaian':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} AS desk,
							sum(CASE WHEN ptk.status_kepegawaian_id = 1 THEN 1 ELSE 0 END) AS 'pns',
							sum(CASE WHEN ptk.status_kepegawaian_id = 2 THEN 1 ELSE 0 END) AS 'pns_diperbantukan',
							sum(CASE WHEN ptk.status_kepegawaian_id = 3 THEN 1 ELSE 0 END) AS 'pns_depag',
							sum(CASE WHEN ptk.status_kepegawaian_id = 4 THEN 1 ELSE 0 END) AS 'gty_pty',
							sum(CASE WHEN ptk.status_kepegawaian_id = 5 THEN 1 ELSE 0 END) AS 'gtt_ptt_provinsi',
							sum(CASE WHEN ptk.status_kepegawaian_id = 6 THEN 1 ELSE 0 END) AS 'gtt_ptt_kabupaten',
							sum(CASE WHEN ptk.status_kepegawaian_id = 7 THEN 1 ELSE 0 END) AS 'guru_bantu_pusat',
							sum(CASE WHEN ptk.status_kepegawaian_id = 8 THEN 1 ELSE 0 END) AS 'guru_honor_sekolah',
							sum(CASE WHEN ptk.status_kepegawaian_id = 9 THEN 1 ELSE 0 END) AS 'tenaga_honor_sekolah',
							sum(CASE WHEN ptk.status_kepegawaian_id = 10 THEN 1 ELSE 0 END) AS 'cpns',
							sum(CASE WHEN ptk.status_kepegawaian_id = 99 THEN 1 ELSE 0 END) AS 'lainnya'
						FROM
							ptk ptk
						JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
						JOIN {$ref}tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
						JOIN sekolah s ON ptkd.sekolah_id = s.sekolah_id
						JOIN {$ref}mst_wilayah w1 ON w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						WHERE
							ptk.soft_delete = 0
						AND ptkd.soft_delete = 0
						AND ptkd.jenis_keluar_id IS NULL
						AND (
							(
								ptkd.tgl_ptk_keluar >= ta.tanggal_mulai
								AND ptkd.tgl_ptk_keluar < ta.tanggal_selesai
							)
							OR ptkd.tgl_ptk_keluar IS NULL
						)
						{$params_bp}
						{$params_wilayah}
						AND ptkd.tahun_ajaran_id = {$tahun_ajaran_id}
						GROUP BY
							{$col_wilayah}
						order by {$col_wilayah} asc";

							// return $sql;die;

				break;
			case 'SebaranPtkJenisPtk':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} AS desk,
							sum(CASE WHEN ptk.jenis_ptk_id = 3 THEN 1 ELSE 0 END) AS 'guru_kelas',
							sum(CASE WHEN ptk.jenis_ptk_id = 4 THEN 1 ELSE 0 END) AS 'guru_mata_pelajaran',
							sum(CASE WHEN ptk.jenis_ptk_id = 5 THEN 1 ELSE 0 END) AS 'guru_bk',
							sum(CASE WHEN ptk.jenis_ptk_id = 6 THEN 1 ELSE 0 END) AS 'guru_inklusi',
							sum(CASE WHEN ptk.jenis_ptk_id = 7 THEN 1 ELSE 0 END) AS 'pengawas_satuan_pendidikan',
							sum(CASE WHEN ptk.jenis_ptk_id = 8 THEN 1 ELSE 0 END) AS 'pengawas_plb',
							sum(CASE WHEN ptk.jenis_ptk_id = 9 THEN 1 ELSE 0 END) AS 'pengawas_mata_pelajaran',
							sum(CASE WHEN ptk.jenis_ptk_id = 10 THEN 1 ELSE 0 END) AS 'pengawas_bidang',
							sum(CASE WHEN ptk.jenis_ptk_id = 11 THEN 1 ELSE 0 END) AS 'tenaga_administrasi_sekolah',
							sum(CASE WHEN ptk.jenis_ptk_id = 12 THEN 1 ELSE 0 END) AS 'guru_pendamping',
							sum(CASE WHEN ptk.jenis_ptk_id = 13 THEN 1 ELSE 0 END) AS 'guru_magang',
							sum(CASE WHEN ptk.jenis_ptk_id = 14 THEN 1 ELSE 0 END) AS 'guru_tik',
							sum(CASE WHEN ptk.jenis_ptk_id = 30 THEN 1 ELSE 0 END) AS 'laboran',
							sum(CASE WHEN ptk.jenis_ptk_id = 40 THEN 1 ELSE 0 END) AS 'pustakawan',
							sum(CASE WHEN ptk.jenis_ptk_id = 99 THEN 1 ELSE 0 END) AS 'lainnya'
						FROM
							ptk ptk
						JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
						JOIN {$ref}tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
						JOIN sekolah s ON ptkd.sekolah_id = s.sekolah_id
						JOIN {$ref}mst_wilayah w1 ON w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						WHERE
							ptk.soft_delete = 0
						AND ptkd.soft_delete = 0
						AND ptkd.jenis_keluar_id IS NULL
						AND (
							(
								ptkd.tgl_ptk_keluar >= ta.tanggal_mulai
								AND ptkd.tgl_ptk_keluar < ta.tanggal_selesai
							)
							OR ptkd.tgl_ptk_keluar IS NULL
						)
						{$params_bp}
						{$params_wilayah}
						AND ptkd.tahun_ajaran_id = {$tahun_ajaran_id}
						GROUP BY
							{$col_wilayah}
						order by {$col_wilayah} asc";

				break;
			case 'PtkPerJenisPtk':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							jp.jenis_ptk AS desk,
							count(*) AS jumlah
						FROM
							ptk ptk
						JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
						JOIN {$ref}tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
						JOIN sekolah s ON ptkd.sekolah_id = s.sekolah_id
						JOIN {$ref}mst_wilayah w1 ON w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						LEFT JOIN {$ref}jenis_ptk jp on jp.jenis_ptk_id = ptk.jenis_ptk_id
						WHERE
							ptk.soft_delete = 0
						AND ptkd.soft_delete = 0
						AND ptkd.jenis_keluar_id IS NULL
						AND (
							(
								ptkd.tgl_ptk_keluar >= ta.tanggal_mulai
								AND ptkd.tgl_ptk_keluar < ta.tanggal_selesai
							)
							OR ptkd.tgl_ptk_keluar IS NULL
						)
						{$params_bp}
						{$params_wilayah}
						AND ptkd.tahun_ajaran_id = {$tahun_ajaran_id}
						GROUP BY
							jp.jenis_ptk,
							jp.jenis_ptk_id
						order by jp.jenis_ptk_id";
				break;
			case 'PtkPerStatusKepegawaian':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							sk.nama AS desk,
							sk.status_kepegawaian_id,
							count(*) AS jumlah
						FROM
							ptk ptk
						JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
						JOIN {$ref}tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
						JOIN sekolah s ON ptkd.sekolah_id = s.sekolah_id
						JOIN {$ref}mst_wilayah w1 ON w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						LEFT JOIN {$ref}status_kepegawaian sk on sk.status_kepegawaian_id = ptk.status_kepegawaian_id
						WHERE
							ptk.soft_delete = 0
						AND ptkd.soft_delete = 0
						AND ptkd.jenis_keluar_id IS NULL
						AND (
							(
								ptkd.tgl_ptk_keluar >= ta.tanggal_mulai
								AND ptkd.tgl_ptk_keluar < ta.tanggal_selesai
							)
							OR ptkd.tgl_ptk_keluar IS NULL
						)
						{$params_bp}
						{$params_wilayah}
						AND ptkd.tahun_ajaran_id = {$tahun_ajaran_id}
						GROUP BY
							sk.nama,
							sk.status_kepegawaian_id
						order by sk.status_kepegawaian_id asc";
				break;
			case 'SebaranSekolahPerWaktuPenyelenggaraan':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} as desk,
							sum(case when sl.waktu_penyelenggaraan_id = 1 then 1 else 0 end) as pagi,
							sum(case when sl.waktu_penyelenggaraan_id = 2 then 1 else 0 end) as siang,
							sum(case when sl.waktu_penyelenggaraan_id = 3 then 1 else 0 end) as kombinasi,
							sum(case when sl.waktu_penyelenggaraan_id = 4 then 1 else 0 end) as sore,
							sum(case when sl.waktu_penyelenggaraan_id = 5 then 1 else 0 end) as malam,
							sum(case when sl.waktu_penyelenggaraan_id = 6 then 1 else 0 end) as sehari_penuh_5,
							sum(case when sl.waktu_penyelenggaraan_id = 7 then 1 else 0 end) as sehari_penuh_6,
							sum(case when sl.waktu_penyelenggaraan_id = 9 then 1 else 0 end) as lainnya
						FROM
							sekolah s
							JOIN {$ref}mst_wilayah w1 ON w1.kode_wilayah = s.kode_wilayah
							JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
							JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
							JOIN sekolah_longitudinal sl ON s.sekolah_id = sl.sekolah_id
							LEFT JOIN {$ref}waktu_penyelenggaraan ON sl.waktu_penyelenggaraan_id = waktu_penyelenggaraan.waktu_penyelenggaraan_id
						WHERE
							s.Soft_delete = 0
						AND sl.semester_id = {$semester_id}
						AND s.kode_wilayah != 000000		
						{$params_bp}
						{$params_wilayah}			
						GROUP BY
							{$col_wilayah}";

				// return $sql;die;

				break;
			case 'SekolahPerWaktuPenyelenggaraan':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							wp.nama AS desk,
							count(*) AS jumlah
						FROM
							sekolah s
						JOIN {$ref}mst_wilayah w1 ON w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						JOIN sekolah_longitudinal sl ON s.sekolah_id = sl.sekolah_id
						LEFT JOIN {$ref}waktu_penyelenggaraan wp ON sl.waktu_penyelenggaraan_id = wp.waktu_penyelenggaraan_id
						WHERE
							s.Soft_delete = 0
						AND sl.semester_id = {$semester_id}
						AND w1.kode_wilayah != 000000
						{$params_bp}
						{$params_wilayah}
						GROUP BY
							wp.nama,
							wp.waktu_penyelenggaraan_id
						order by wp.waktu_penyelenggaraan_id asc";

				// return $sql;die;

				break;
			case 'SekolahPerBentukPendidikan':
				$sql = "SELECT
							bp.nama as desk,
							count(*) as jumlah
						FROM
							sekolah
							LEFT JOIN {$ref}bentuk_pendidikan bp on sekolah.bentuk_pendidikan_id = bp.bentuk_pendidikan_id
						WHERE
							sekolah.Soft_delete = 0
						GROUP BY
							bp.nama";
				break;
			case 'SebaranSekolahPerBentukPendidikan':
				$sql = "SELECT
							w.nama as desk,
							sum(case when sekolah.bentuk_pendidikan_id = 5 then 1 else 0 end) as sd,
							sum(case when sekolah.bentuk_pendidikan_id = 6 then 1 else 0 end) as smp,
							sum(case when sekolah.bentuk_pendidikan_id = 29 then 1 else 0 end) as slb,
							sum(case when sekolah.bentuk_pendidikan_id = 13 then 1 else 0 end) as sma,
							sum(case when sekolah.bentuk_pendidikan_id = 15 then 1 else 0 end) as smk,
							sum(case when sekolah.bentuk_pendidikan_id = 14 then 1 else 0 end) as smlb
						FROM
							sekolah
							LEFT JOIN {$ref}bentuk_pendidikan on sekolah.bentuk_pendidikan_id = bentuk_pendidikan.bentuk_pendidikan_id
							LEFT JOIN {$ref}mst_wilayah w on sekolah.kode_wilayah = w.kode_wilayah
						WHERE
							w.kode_wilayah != 000000
						GROUP BY
							w.nama";
				break;
			case 'SekolahPerStatus' :
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				$sql = "SELECT
							(case when status_sekolah = 1 then 'NEGERI' else 'SWASTA' end) as desk,
							count(*) as jumlah
						FROM
							sekolah s
						WHERE
							Soft_delete = 0
							{$params_bp}
						GROUP BY
							s.status_sekolah";
				break;
			case 'SebaranSekolahPerStatus':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				$sql = "SELECT
							w.nama as desk,
							sum(case when s.status_sekolah = 1 then 1 else 0 end) as negeri,
							sum(case when s.status_sekolah = 2 then 1 else 0 end) as swasta
						FROM
							sekolah s
							LEFT JOIN {$ref}mst_wilayah w on s.kode_wilayah = w.kode_wilayah
						WHERE
							s.Soft_delete = 0
						AND s.kode_wilayah != 000000
							{$params_bp}					
						GROUP BY
							w.nama";
				break;
			case 'PesertaDidikPerJenisKelamin' :
					$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
					$params_bp = System::getParamsBp($bentuk_pendidikan_id);

					$sql = "select
						d.jenis_kelamin as desk,
						count(*) as jumlah
					from
						anggota_rombel a
						join rombongan_belajar b on a.rombongan_belajar_id = b.rombongan_belajar_id
						join registrasi_peserta_didik c on a.peserta_didik_id = c.peserta_didik_id
						join peserta_didik d on c.peserta_didik_id = d.peserta_didik_id
						join sekolah s on d.sekolah_id = s.sekolah_id
					where
						a.soft_delete = 0
						and b.soft_delete = 0
						and c.soft_delete = 0
						and d.soft_delete = 0
						and b.semester_id = '".SEMESTER_BERJALAN."'
						and c.jenis_keluar_id is null
						and d.jenis_kelamin in ('L','P')
						".$params_bp."
					GROUP BY
						d.jenis_kelamin";
				break;
			case 'PesertaDidikPerAgama' :
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "select 
							f.nama as desk,
							f.agama_id,
							count(*) as jumlah
						from anggota_rombel a
							join rombongan_belajar b on a.rombongan_belajar_id = b.rombongan_belajar_id
							join registrasi_peserta_didik c on a.peserta_didik_id = c.peserta_didik_id
							join peserta_didik d on c.peserta_didik_id = d.peserta_didik_id
							join sekolah s on d.sekolah_id = s.sekolah_id
							JOIN {$ref}mst_wilayah w1 on w1.kode_wilayah = s.kode_wilayah
							JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
							JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
							join {$ref}agama f on d.agama_id = f.agama_id
						where
							a.soft_delete = 0
							and b.soft_delete = 0
							and c.soft_delete = 0
							and d.soft_delete = 0
							and b.semester_id = {$semester_id}
							and c.jenis_keluar_id is null
							{$params_bp}
							{$params_wilayah}
						GROUP BY
							f.nama,
							f.agama_id
						order by f.agama_id
						";
					// return $sql;die;
				break;
			case 'PtkPerJenisKelamin' :
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT 
							ptk.jenis_kelamin as desk,
							count(*) as jumlah
						FROM   ptk ptk 
						       JOIN ptk_terdaftar ptkd 
						         ON ptk.ptk_id = ptkd.ptk_id 
						       JOIN {$ref}tahun_ajaran ta 
						         ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id 
									 JOIN sekolah s
										 ON ptkd.sekolah_id = s.sekolah_id
							JOIN {$ref}mst_wilayah w1 on w1.kode_wilayah = s.kode_wilayah
							JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
							JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						WHERE  ptk.soft_delete = 0 
						       AND ptkd.soft_delete = 0 
						       AND ptkd.jenis_keluar_id IS NULL 
						       AND ( ( ptkd.tgl_ptk_keluar >= ta.tanggal_mulai 
						               AND ptkd.tgl_ptk_keluar < ta.tanggal_selesai ) 
						              OR ptkd.tgl_ptk_keluar IS NULL ) 
						    AND ptkd.tahun_ajaran_id = {$tahun_ajaran_id} 
						    AND ptk.jenis_kelamin != '*'
							{$params_bp}
							{$params_wilayah}
						GROUP BY
							ptk.jenis_kelamin";
				break;
			case 'PtkPerAgama' :
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							a.nama as desk,
							a.agama_id,
							count(*) as jumlah
						FROM   ptk ptk
							JOIN ptk_terdaftar ptkd
								ON ptk.ptk_id = ptkd.ptk_id
							JOIN {$ref}tahun_ajaran ta
								ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
							JOIN sekolah s
								ON ptkd.sekolah_id = s.sekolah_id
							JOIN {$ref}agama a
								ON ptk.agama_id = a.agama_id
							JOIN {$ref}mst_wilayah w1 on w1.kode_wilayah = s.kode_wilayah
							JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
							JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						WHERE  ptk.soft_delete = 0
							AND ptkd.soft_delete = 0
							AND ptkd.jenis_keluar_id IS NULL
							AND ( ( ptkd.tgl_ptk_keluar >= ta.tanggal_mulai
								AND ptkd.tgl_ptk_keluar < ta.tanggal_selesai )
								OR ptkd.tgl_ptk_keluar IS NULL )
							AND ptkd.tahun_ajaran_id = {$tahun_ajaran_id}
							{$params_bp}
							{$params_wilayah}
						GROUP BY
							a.nama,
							a.agama_id
						ORDER BY a.agama_id asc";
				break;
			case 'SebaranPdPerAgama':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} AS desk,
							sum(CASE WHEN d.agama_id = 1 THEN 1 ELSE 0 END) AS islam,
							sum(CASE WHEN d.agama_id = 2 THEN 1 ELSE 0 END) AS kristen,
							sum(CASE WHEN d.agama_id = 3 THEN 1 ELSE 0 END) AS katholik,
							sum(CASE WHEN d.agama_id = 4 THEN 1 ELSE 0 END) AS hindu,
							sum(CASE WHEN d.agama_id = 5 THEN 1 ELSE 0 END) AS budha,
							sum(CASE WHEN d.agama_id = 6 THEN 1 ELSE 0 END) AS konghucu
						FROM
							anggota_rombel a
						JOIN rombongan_belajar b ON a.rombongan_belajar_id = b.rombongan_belajar_id
						JOIN registrasi_peserta_didik c ON a.peserta_didik_id = c.peserta_didik_id
						JOIN peserta_didik d ON c.peserta_didik_id = d.peserta_didik_id
						JOIN sekolah s ON d.sekolah_id = s.sekolah_id
						JOIN {$ref}agama f ON d.agama_id = f.agama_id
						JOIN {$ref}mst_wilayah w1 on w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						WHERE
							a.soft_delete = 0
						AND b.soft_delete = 0
						AND c.soft_delete = 0
						AND d.soft_delete = 0
						AND b.semester_id = {$semester_id}
						AND c.jenis_keluar_id IS NULL
						{$params_bp}
						{$params_wilayah}
						GROUP BY
							{$col_wilayah}
						order by {$col_wilayah}";

						// return $sql;die;
				break;
			case 'SebaranPtkPerAgama':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} AS desk,
							sum(CASE WHEN ptk.agama_id = 1 THEN 1 ELSE 0 END) AS islam,
							sum(CASE WHEN ptk.agama_id = 2 THEN 1 ELSE 0 END) AS kristen,
							sum(CASE WHEN ptk.agama_id = 3 THEN 1 ELSE 0 END) AS katholik,
							sum(CASE WHEN ptk.agama_id = 4 THEN 1 ELSE 0 END) AS hindu,
							sum(CASE WHEN ptk.agama_id = 5 THEN 1 ELSE 0 END) AS budha,
							sum(CASE WHEN ptk.agama_id = 6 THEN 1 ELSE 0 END) AS konghucu
						FROM
							ptk ptk
						JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
						JOIN {$ref}tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
						JOIN sekolah s ON ptkd.sekolah_id = s.sekolah_id
						JOIN {$ref}agama a ON ptk.agama_id = a.agama_id
						JOIN {$ref}mst_wilayah w1 on w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						WHERE
							ptk.soft_delete = 0
						AND ptkd.soft_delete = 0
						AND ptkd.jenis_keluar_id IS NULL
						AND (
							(
								ptkd.tgl_ptk_keluar >= ta.tanggal_mulai
								AND ptkd.tgl_ptk_keluar < ta.tanggal_selesai
							)
							OR ptkd.tgl_ptk_keluar IS NULL
						)
						AND ptkd.tahun_ajaran_id = {$tahun_ajaran_id}
						{$params_bp}
						{$params_wilayah}
						GROUP BY
							{$col_wilayah}
						order by {$col_wilayah} asc";
				break;
			case 'SebaranPdJenisKelamin':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} AS desk,
							sum(CASE WHEN d.jenis_kelamin = 'L' THEN 1 ELSE 0 END) AS 'laki-laki',
							sum(CASE WHEN d.jenis_kelamin = 'P' THEN 1 ELSE 0 END) AS 'perempuan'
						FROM
							anggota_rombel a
						JOIN rombongan_belajar b ON a.rombongan_belajar_id = b.rombongan_belajar_id
						JOIN registrasi_peserta_didik c ON a.peserta_didik_id = c.peserta_didik_id
						JOIN peserta_didik d ON c.peserta_didik_id = d.peserta_didik_id
						JOIN sekolah s ON d.sekolah_id = s.sekolah_id
						JOIN {$ref}mst_wilayah w1 on w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						WHERE
							a.soft_delete = 0
						AND b.soft_delete = 0
						AND c.soft_delete = 0
						AND d.soft_delete = 0
						AND b.semester_id = {$semester_id}
						AND c.jenis_keluar_id IS NULL
						{$params_bp}
						{$params_wilayah}
						AND w1.kode_wilayah != 000000
						GROUP BY
							{$col_wilayah}";
				break;
			case 'SebaranPtkJenisKelamin':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} AS desk,
							sum(CASE WHEN ptk.jenis_kelamin = 'L' THEN 1 ELSE 0 END) AS 'laki-laki',
							sum(CASE WHEN ptk.jenis_kelamin = 'P' THEN 1 ELSE 0 END) AS 'perempuan'
						FROM
							ptk ptk
						JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
						JOIN {$ref}tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
						JOIN sekolah s ON ptkd.sekolah_id = s.sekolah_id
						JOIN {$ref}mst_wilayah w1 on w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						WHERE
							ptk.soft_delete = 0
						AND ptkd.soft_delete = 0
						AND ptkd.jenis_keluar_id IS NULL
						AND (
							(
								ptkd.tgl_ptk_keluar >= ta.tanggal_mulai
								AND ptkd.tgl_ptk_keluar < ta.tanggal_selesai
							)
							OR ptkd.tgl_ptk_keluar IS NULL
						)
						AND ptkd.tahun_ajaran_id = {$tahun_ajaran_id}
						{$params_bp}
						{$params_wilayah}
						GROUP BY
							{$col_wilayah}
						order by {$col_wilayah} asc";
				break;
		}

		$data = getDataBySql($sql);
		
		return tableJson($data, count($data), array("bentuk_pendidikan"));
	}
}

?>