<?php
namespace simdik_batam;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use simdik_batam\Model\Sekolah;
use simdik_batam\Model\SekolahPeer;
use simdik_batam\Model\MstWilayahPeer;
use simdik_batam\Model\Pengguna;
use simdik_batam\Model\PenggunaPeer;
use simdik_batam\Model\Mutasi;
use simdik_batam\Model\MutasiPeer;
use simdik_batam\Model\JenisSurat;
use simdik_batam\Model\JenisSuratPeer;
use simdik_batam\Model\ApkApm;
use simdik_batam\Model\ApkApmPeer;

use \Suin\RSSWriter\Feed;
use \Suin\RSSWriter\Channel;
use \Suin\RSSWriter\Item;

class System
{
	public function initdb(){
		$serverName = "(local)";
		$connectionInfo = array( "Database"=>"rekap_dapodikmen", "UID"=>"report", "PWD"=>"ReportDikmen",'ReturnDatesAsStrings'=>true);
		/* Connect using Windows Authentication. */
		$con_rekap = sqlsrv_connect( $serverName, $connectionInfo);

		return $con_rekap;
	}

	function getDeskripsi(){	
		$filename = 'desc.txt';

		$return = file_get_contents($filename, FILE_USE_INCLUDE_PATH);

		if(LEVEL == 'dikdas'){
			$level = 'dasar';
		}else if(LEVEL == 'dikmen'){
			$level = 'menengah';
		}else if(LEVEL == 'dikdasmen'){
			$level = 'dasar dan menengah';
		}

		if(LABEL == 'Profil'){
			$label = LABEL.' Pendidikan';
		}else{
			$label = LABEL;
		}

		$return = str_replace('%label%', $label, $return);
		$return = str_replace('%kabkota%', KABKOTA, $return);
		$return = str_replace('%tingkat%', $level, $return);

		return $return;
		// return 'tes_as';
	}

	function databaseCompatibility(){
		if(DBMS == 'mssql'){
			$ref = 'ref.';
		}else{
			$ref = '';
		}

		return $ref;
	}

	function pagingCompatibility($column, $start, $limit){
		if(DBMS == 'mssql'){
			$sql = " ORDER BY {$column} OFFSET {$start} ROWS FETCH NEXT {$limit} ROWS ONLY";
		}else{
			$sql = " ORDER BY {$column} LIMIT {$start},{$limit}";
		}

		return $sql;
	}

	function getGuid(){

		if(DBMS == 'mssql'){
			$sql = "select NEWID() as guid";
			$fetch = getDataBySql($sql);

			$return = $fetch[0]['guid'];
		}else{

			if (function_exists('com_create_guid')){
	        	$return = com_create_guid();
		    }else{
		        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
		        $charid = strtoupper(md5(uniqid(rand(), true)));
		        $hyphen = chr(45);// "-"
		        $uuid = chr(123)// "{"
		                .substr($charid, 0, 8).$hyphen
		                .substr($charid, 8, 4).$hyphen
		                .substr($charid,12, 4).$hyphen
		                .substr($charid,16, 4).$hyphen
		                .substr($charid,20,12)
		                .chr(125);// "}"
		        $return = $uuid;
		    }
		}

		return $return;
	}

	function testing(){
		return "tes output aplikasi";
	}

	function get(Request $request, Application $app){
		$ref = $this->databaseCompatibility();

		$model = $request->get('model');
		$start = $request->get('start') ? $request->get('start') : 0;
		$limit = $request->get('limit') ? $request->get('limit') : 20;

		$tahun_ajaran_id	= $request->get('tahun_ajaran_id') ? $request->get('tahun_ajaran_id') : TA_BERJALAN;
		$semester_id	= $request->get('semester_id') ? $request->get('semester_id') : SEMESTER_BERJALAN;

		$session_kode_wilayah = $request->get('kode_wilayah') ? $request->get('kode_wilayah') : $app['session']->get('kode_wilayah');
		$session_id_level_wilayah = $request->get('id_level_wilayah') ? $request->get('id_level_wilayah') : $app['session']->get('id_level_wilayah');

		$count = 0;

		$fetchArr = array();

		$c = new \Criteria();
		
		switch ($model) {
			case 'JenisPtk':
				$query = $request->get('query');

				$sql = "select * from {$ref}jenis_ptk where expired_date is null";

				if($query){
					$sql .= " and jenis_ptk like '%{$query}%'";
				}

				$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
					
					array_push($fetchArr, $arr);

					$count++;
				}

				
				$prim = 'jenis_ptk_id';

				break;
			case 'Jurusan':
				$bp_id = $request->get('bp_id');
				$query = $request->get('query');

				$sql = "select * from {$ref}jurusan where expired_date is null";

				if($bp_id){
					switch ($bp_id) {
						case 13:
							$sql .= " and level_bidang_id = 11 and untuk_sma = 1";
							break;
						case 14:
							$sql .= " and level_bidang_id = 12 AND jurusan_id > 90900000";
							break;
						case 29:
							$sql .= " and level_bidang_id = 12 AND jurusan_id > 90900000";
							break;
						case 15:
							$sql .= " and level_bidang_id = 12 and untuk_smk = 1";
							break;
						default:
							# code...
							break;
					}
				}

				if($query){
					$sql .= " and nama_jurusan like '%{$query}%'";
				}

				$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
					
					array_push($fetchArr, $arr);

					$count++;
				}

				
				$prim = 'jurusan_id';

				break;
			case 'customReporting':
				
				$tabel = $request->get('tabel');

				if($tabel == 'sekolah'){

					if($session_id_level_wilayah == 0){
						$params_wilayah = ' and w3.mst_kode_wilayah = '.$session_kode_wilayah;
						$group1 = 'w3.nama';
						$group2 = 'w3.kode_wilayah';
						$group3 = 'w3.id_level_wilayah';
					}else if($session_id_level_wilayah == 1){
						$params_wilayah = ' and w2.mst_kode_wilayah = '.$session_kode_wilayah;
						$group1 = 'w2.nama';
						$group2 = 'w2.kode_wilayah';
						$group3 = 'w2.id_level_wilayah';
					}else if($session_id_level_wilayah == 2){
						$params_wilayah = ' and w1.mst_kode_wilayah = '.$session_kode_wilayah;
						$group1 = 'w1.nama';
						$group2 = 'w1.kode_wilayah';
						$group3 = 'w1.id_level_wilayah';
					}

					if($request->get('status_sekolah')){
						$param1 = " and s.status_sekolah = ".$request->get('status_sekolah');
					}

					if($request->get('bentuk_pendidikan_id')){
						$param2 = " and s.bentuk_pendidikan_id = ".$request->get('bentuk_pendidikan_id');
					}

					if($request->get('status_kepemilikan_id')){
						$param3 = " and s.status_kepemilikan_id = ".$request->get('status_kepemilikan_id');
					}

					if($request->get('wilayah_terpencil')){
						$param4 = " and sl.wilayah_terpencil = ".$request->get('wilayah_terpencil');
					}

					if($request->get('wilayah_perbatasan')){
						$param5 = " and sl.wilayah_perbatasan = ".$request->get('wilayah_perbatasan');
					}

					if($request->get('wilayah_transmigrasi')){
						$param6 = " and sl.wilayah_transmigrasi = ".$request->get('wilayah_transmigrasi');
					}

					if($request->get('wilayah_adat_terpencil')){
						$param7 = " and sl.wilayah_adat_terpencil = ".$request->get('wilayah_adat_terpencil');
					}

					if($request->get('wilayah_bencana_alam')){
						$param8 = " and sl.wilayah_bencana_alam = ".$request->get('wilayah_bencana_alam');
					}

					if($request->get('kriteria_agregat') == 0){
						$group1 = 'w3.nama';
						$group2 = 'w3.kode_wilayah';
						$group3 = 'w3.id_level_wilayah';
					}else if($request->get('kriteria_agregat') == 1){
						$group1 = 'w2.nama';
						$group2 = 'w2.kode_wilayah';
						$group3 = 'w2.id_level_wilayah';
					}else if($request->get('kriteria_agregat') == 2){
						$group1 = 'w1.nama';
						$group2 = 'w1.kode_wilayah';
						$group3 = 'w1.id_level_wilayah';
					}

					if($request->get('agregat') == 'true'){
						$koloms = $group1.",".
									$group2.",".
									$group3.",".
									"count(1) as jumlah";

						$groupKoloms = "GROUP by ".$group1.",".
										$group2.",".
										$group3;
					}else{
						$koloms = "s.nama, 
									s.npsn, 
									bp.nama as bentuk_pendidikan,
									(case when s.status_sekolah = 1 then 'Negeri' else 'Swasta' end) as status,
									s.alamat_jalan,
									s.desa_kelurahan,
									w1.nama as kecamatan,
									w2.nama as kabupaten,
									w3.nama as propinsi";
						$groupKoloms = '';
					}

					$sql = "select 
							{$koloms} 
						from 
							sekolah s
						JOIN {$ref}mst_wilayah w1 ON s.kode_wilayah=w1.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w1.mst_kode_wilayah=w2.kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w2.mst_kode_wilayah=w3.kode_wilayah
						JOIN {$ref}bentuk_pendidikan bp on bp.bentuk_pendidikan_id = s.bentuk_pendidikan_id
						JOIN sekolah_longitudinal sl ON sl.sekolah_id=s.sekolah_id
						where s.soft_delete = 0
						and sl.semester_id = {$semester_id}
						{$param1}
						{$param2}
						{$param3}
						{$param4}
						{$param5}
						{$param6}
						{$param7}
						{$param8}
						{$params_wilayah} 
						{$groupKoloms}";
					// return $sql;die;
				}else if($tabel == 'query'){

					$sql = $request->get('query');

				}


				$fetch = getDataBySql($sql);

				$arrKolom = array();
				$x = 0;

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

						if($x < sizeof($f)){
							array_push($arrKolom, $fc);
						}
						$x++;

					}

					$count++;
					
					array_push($fetchArr, $arr);
				}

				break;
			case 'PeringkatPengiriman':
				
				$sql = "SELECT w1.nama,w1.kode_wilayah,w1.id_level_wilayah,w1.mst_kode_wilayah,
							SUM(CASE WHEN s.bentuk_pendidikan_id=5 THEN 1 ELSE 0 END) AS sd,
							SUM(CASE WHEN s.bentuk_pendidikan_id=6 THEN 1 ELSE 0 END) AS smp,
							SUM(CASE WHEN s.bentuk_pendidikan_id=7 THEN 1 ELSE 0 END) AS sdlb,
							SUM(CASE WHEN s.bentuk_pendidikan_id=8 THEN 1 ELSE 0 END) AS smplb,
							SUM(CASE WHEN s.bentuk_pendidikan_id=29 THEN 1 ELSE 0 END) AS slb,
							SUM(CASE WHEN s.bentuk_pendidikan_id=13 THEN 1 ELSE 0 END) AS sma,
							SUM(CASE WHEN s.bentuk_pendidikan_id=15 THEN 1 ELSE 0 END) AS smk,
							SUM(CASE WHEN s.bentuk_pendidikan_id=14 THEN 1 ELSE 0 END) AS smlb,
							SUM(CASE WHEN s.bentuk_pendidikan_id=5 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) AS kirim_sd,
							SUM(CASE WHEN s.bentuk_pendidikan_id=6 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) AS kirim_smp,
							SUM(CASE WHEN s.bentuk_pendidikan_id=7 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) AS kirim_sdlb,
							SUM(CASE WHEN s.bentuk_pendidikan_id=8 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) AS kirim_smplb,
							SUM(CASE WHEN s.bentuk_pendidikan_id=29 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) AS kirim_slb,
							SUM(CASE WHEN s.bentuk_pendidikan_id=13 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) AS kirim_sma,
							SUM(CASE WHEN s.bentuk_pendidikan_id=15 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) AS kirim_smk,
							SUM(CASE WHEN s.bentuk_pendidikan_id=14 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) AS kirim_smlb,
							(
								(
									SUM(CASE WHEN s.bentuk_pendidikan_id=5 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) +
									SUM(CASE WHEN s.bentuk_pendidikan_id=6 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) +
									SUM(CASE WHEN s.bentuk_pendidikan_id=7 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) +
									SUM(CASE WHEN s.bentuk_pendidikan_id=8 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) +
									SUM(CASE WHEN s.bentuk_pendidikan_id=29 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) +
									SUM(CASE WHEN s.bentuk_pendidikan_id=13 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) + 
									SUM(CASE WHEN s.bentuk_pendidikan_id=15 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END) +
									SUM(CASE WHEN s.bentuk_pendidikan_id=14 AND l.begin_sync IS NOT NULL THEN 1 ELSE 0 END)
								) /
								
									(
									SUM(CASE WHEN s.bentuk_pendidikan_id=5 THEN 1 ELSE 0 END) +
									SUM(CASE WHEN s.bentuk_pendidikan_id=6 THEN 1 ELSE 0 END) +
									SUM(CASE WHEN s.bentuk_pendidikan_id=7 THEN 1 ELSE 0 END) +
									SUM(CASE WHEN s.bentuk_pendidikan_id=8 THEN 1 ELSE 0 END) +
									SUM(CASE WHEN s.bentuk_pendidikan_id=29 THEN 1 ELSE 0 END) +
									SUM(CASE WHEN s.bentuk_pendidikan_id=13 THEN 1 ELSE 0 END) + 
									SUM(CASE WHEN s.bentuk_pendidikan_id=15 THEN 1 ELSE 0 END) +
									SUM(CASE WHEN s.bentuk_pendidikan_id=14 THEN 1 ELSE 0 END)
								)
							) * 100 as persen
						FROM sekolah s
							JOIN {$ref}mst_wilayah w1 ON s.kode_wilayah=w1.kode_wilayah
							JOIN {$ref}mst_wilayah w2 ON w1.mst_kode_wilayah=w2.kode_wilayah
							LEFT JOIN (
								SELECT 
									sekolah_id,
									MAX(begin_sync) AS begin_sync
								FROM sync_log
								WHERE alamat_ip NOT LIKE 'prefil%'
								GROUP BY sekolah_id) l ON s.sekolah_id=l.sekolah_id
						WHERE s.Soft_delete = 0
						and w1.mst_kode_wilayah = ".KODE_WILAYAH."  
						GROUP BY w1.nama,w1.kode_wilayah,w1.id_level_wilayah,w1.mst_kode_wilayah
						order by persen desc";

				$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
					
					array_push($fetchArr, $arr);
				}

				break;
			case 'AnalisisKebutuhanPrasarana':
				$sql = "select kode_wilayah,nama from {$ref}mst_wilayah where mst_kode_wilayah = ".KODE_WILAYAH;

				$prim = 'kode_wilayah';

				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');

				if($bentuk_pendidikan_id == 5){
					$params_bp = ' AND s.bentuk_pendidikan_id in (5,7,9)';
				}else if($bentuk_pendidikan_id == 6){
					$params_bp = ' AND s.bentuk_pendidikan_id in (6,8,10)';
				}else if($bentuk_pendidikan_id == 13){
					$params_bp = ' AND s.bentuk_pendidikan_id in (13,14,15,16,17)';
				}

				$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}

					$sql_sekolah = "SELECT
								count(1) AS total,
								sum(case when s.status_sekolah = 1 then 1 else 0 end) as negeri,
								sum(case when s.status_sekolah = 2 then 1 else 0 end) as swasta
							FROM
								sekolah s
							WHERE
								s.soft_delete = 0
								{$params_bp}
								AND s.kode_wilayah = ".$arr['kode_wilayah'];

					$fetch_sekolah = getDataBySql($sql_sekolah);

					foreach ($fetch_sekolah as $fs) {
						foreach ($fs as $fcs => $vs) {
							$arr[$fcs] = $vs;

						}
					}

					$sqkelas = "SELECT
									sum(case when pr.jenis_prasarana_id = 1 then 1 else 0 end) as kelas_total,
									sum(case when s.status_sekolah = 1 AND pr.jenis_prasarana_id = 1 then 1 else 0 end) as kelas_negeri,
									sum(case when s.status_sekolah = 2 AND pr.jenis_prasarana_id = 1 then 1 else 0 end) as kelas_swasta,
									sum(case when pr.jenis_prasarana_id = 2 then 1 else 0 end) as lab_ipa_total,
									sum(case when s.status_sekolah = 1 AND pr.jenis_prasarana_id = 2 then 1 else 0 end) as lab_ipa_negeri,
									sum(case when s.status_sekolah = 2 AND pr.jenis_prasarana_id = 2 then 1 else 0 end) as lab_ipa_swasta,
									sum(case when pr.jenis_prasarana_id = 8 then 1 else 0 end) as lab_komputer_total,
									sum(case when s.status_sekolah = 1 AND pr.jenis_prasarana_id = 8 then 1 else 0 end) as lab_komputer_negeri,
									sum(case when s.status_sekolah = 2 AND pr.jenis_prasarana_id = 8 then 1 else 0 end) as lab_komputer_swasta,
									sum(case when pr.jenis_prasarana_id = 10 then 1 else 0 end) as perpustakaan_total,
									sum(case when s.status_sekolah = 1 AND pr.jenis_prasarana_id = 10 then 1 else 0 end) as perpustakaan_negeri,
									sum(case when s.status_sekolah = 2 AND pr.jenis_prasarana_id = 10 then 1 else 0 end) as perpustakaan_swasta
								FROM
									prasarana pr
									JOIN sekolah s on s.sekolah_id = pr.sekolah_id
								WHERE
									pr.Soft_delete = 0
									AND s.Soft_delete = 0
									{$params_bp}
									AND s.kode_wilayah = ".$arr['kode_wilayah'];

					$fetckelas = getDataBySql($sqkelas);

					foreach ($fetckelas as $fkelas) {
						foreach ($fkelas as $fckelas => $vkelas) {
							$arr[$fckelas] = $vkelas;

						}
					}

					array_push($fetchArr, $arr);

					$count++;
				}

				break;
			case 'AnalisisKebutuhanGuruDanKelas':
				$sql = "select kode_wilayah,nama from {$ref}mst_wilayah where mst_kode_wilayah = ".KODE_WILAYAH;

				$prim = 'kode_wilayah';

				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');

				if($bentuk_pendidikan_id == 5){
					$params_bp = ' AND s.bentuk_pendidikan_id in (5,7,9)';
				}else if($bentuk_pendidikan_id == 6){
					$params_bp = ' AND s.bentuk_pendidikan_id in (6,8,10)';
				}else if($bentuk_pendidikan_id == 13){
					$params_bp = ' AND s.bentuk_pendidikan_id in (13,14,15,16,17)';
				}

				$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}

					$sql_sekolah = "SELECT
								count(1) AS total,
								sum(case when s.status_sekolah = 1 then 1 else 0 end) as negeri,
								sum(case when s.status_sekolah = 2 then 1 else 0 end) as swasta
							FROM
								sekolah s
							WHERE
								s.soft_delete = 0
								{$params_bp}
								AND s.kode_wilayah = ".$arr['kode_wilayah'];

					$fetch_sekolah = getDataBySql($sql_sekolah);

					foreach ($fetch_sekolah as $fs) {
						foreach ($fs as $fcs => $vs) {
							$arr[$fcs] = $vs;

						}
					}


					$sql_siswa = "SELECT
									count(1) AS pd_total,
									sum(case when s.status_sekolah = 1 then 1 else 0 end) as pd_negeri,
									sum(case when s.status_sekolah = 2 then 1 else 0 end) as pd_swasta
								FROM
									peserta_didik pd
									JOIN registrasi_peserta_didik rpd on pd.peserta_didik_id = rpd.peserta_didik_id
									JOIN anggota_rombel ar on ar.peserta_didik_id = pd.peserta_didik_id
									JOIN rombongan_belajar rb on rb.rombongan_belajar_id = ar.rombongan_belajar_id
									JOIN sekolah s on s.sekolah_id = rb.sekolah_id
								WHERE
									rpd.Soft_delete = 0
									AND pd.Soft_delete = 0
									AND ar.Soft_delete = 0
									AND rb.Soft_delete = 0
									AND s.soft_delete = 0
									AND rpd.jenis_keluar_id is NULL
									{$params_bp}
									AND rb.semester_id = ".SEMESTER_BERJALAN."
									AND s.kode_wilayah = ".$arr['kode_wilayah'];

					$fetch_siswa = getDataBySql($sql_siswa);

					foreach ($fetch_siswa as $fsis) {
						foreach ($fsis as $fcsis => $vsis) {
							$arr[$fcsis] = $vsis;

						}
					}

					$sql_rb = "SELECT
									count(1) AS rb_total,
									sum(case when s.status_sekolah = 1 then 1 else 0 end) as rb_negeri,
									sum(case when s.status_sekolah = 2 then 1 else 0 end) as rb_swasta
								FROM
									rombongan_belajar rb 
									JOIN sekolah s on s.sekolah_id = rb.sekolah_id
								WHERE
									rb.Soft_delete = 0
									AND s.soft_delete = 0
									{$params_bp}
									AND rb.semester_id = ".SEMESTER_BERJALAN."
									AND s.kode_wilayah = ".$arr['kode_wilayah'];

					$fetch_rb = getDataBySql($sql_rb);

					foreach ($fetch_rb as $frb) {
						foreach ($frb as $fcrb => $vrb) {
							$arr[$fcrb] = $vrb;

						}
					}

					$sqkelas = "SELECT
									count(1) as pr_total,
									sum(case when s.status_sekolah = 1 then 1 else 0 end) as pr_negeri,
									sum(case when s.status_sekolah = 2 then 1 else 0 end) as pr_swasta
								FROM
									prasarana pr
									JOIN sekolah s on s.sekolah_id = pr.sekolah_id
								WHERE
									pr.jenis_prasarana_id = 1
									AND pr.Soft_delete = 0
									AND s.Soft_delete = 0
									{$params_bp}
									AND s.kode_wilayah = ".$arr['kode_wilayah'];

					$fetckelas = getDataBySql($sqkelas);

					foreach ($fetckelas as $fkelas) {
						foreach ($fkelas as $fckelas => $vkelas) {
							$arr[$fckelas] = $vkelas;

						}
					}

					if($bentuk_pendidikan_id == 5){
						$sql_guru = "SELECT
										count(1) as guru_total,
										sum(case when s.status_sekolah = 1 then 1 else 0 end) as guru_negeri,
										sum(case when s.status_sekolah = 2 then 1 else 0 end) as guru_swasta
									FROM
										ptk ptk
										JOIN ptk_terdaftar ptkd on ptkd.ptk_id = ptk.ptk_id
										JOIN sekolah s on s.sekolah_id = ptkd.sekolah_id
									WHERE
										ptk.jenis_ptk_id = 3
										AND ptk.Soft_delete = 0
										AND ptkd.Soft_delete = 0
										AND s.Soft_delete = 0
										AND ptkd.jenis_keluar_id is null
										{$params_bp}
										AND s.kode_wilayah = ".$arr['kode_wilayah'];

						$fetch_guru = getDataBySql($sql_guru);

						foreach ($fetch_guru as $f_guru) {
							foreach ($f_guru as $fc_guru => $v_guru) {
								$arr[$fc_guru] = $v_guru;

							}
						}

						$arr['guru_p'] = $arr['rb_total'];
						$arr['guru_a'] = $arr['guru_total'];
						$arr['guru_k'] = $arr['rb_total'] - $arr['guru_total'];
						$arr['guru_l'] = $arr['guru_total'] - $arr['rb_total'];
					}else if($bentuk_pendidikan_id == 6){


					}else if($bentuk_pendidikan_id == 13){

						
					}

					$arr['kelas_p'] = $arr['rb_total'];
					$arr['kelas_a'] = $arr['pr_total'];
					$arr['kelas_k'] = $arr['rb_total'] - $arr['pr_total'];
					$arr['kelas_l'] = $arr['pr_total'] - $arr['rb_total'];


					array_push($fetchArr, $arr);

					$count++;
				}

				break;
			case 'ApkApm':

				$tanggal = date('Y-m-d');
				$prim = 'apk_apm_id';

				$sql = "select top 1 * from adm.apk_apm order by tanggal DESC";

				$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;
					}

					$sql_jumlah_pd = "SELECT
										sum(case when s.bentuk_pendidikan_id in (5,7,9) then 1 else 0 end) as jumlah_siswa_sd,
										sum(case when s.bentuk_pendidikan_id in (5,7,9) AND (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) >= 6 AND (DATEDIFF(hour,pd.tanggal_lahir,GETDATE())/8766) <= 12 then 1 else 0 end) as jumlah_siswa_sd_6_12,
										
										sum(case when s.bentuk_pendidikan_id in (6,8,10) then 1 else 0 end) as jumlah_siswa_smp,
										sum(case when s.bentuk_pendidikan_id in (6,8,10) AND (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) >= 13 AND (DATEDIFF(hour,pd.tanggal_lahir,GETDATE())/8766) <= 15 then 1 else 0 end) as jumlah_siswa_smp_13_15,
										
										sum(case when s.bentuk_pendidikan_id in (13,14,15,16,17) then 1 else 0 end) as jumlah_siswa_sma,
										sum(case when s.bentuk_pendidikan_id in (13,14,15,16,17) AND (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) >= 16 AND (DATEDIFF(hour,pd.tanggal_lahir,GETDATE())/8766) <= 19 then 1 else 0 end) as jumlah_siswa_sma_16_19
									FROM
										peserta_didik pd
									JOIN registrasi_peserta_didik rpd ON pd.peserta_didik_id = rpd.peserta_didik_id
									JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
									JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
									JOIN sekolah s ON s.sekolah_id = rpd.sekolah_id
									JOIN ref.mst_wilayah w1 ON w1.kode_wilayah = s.kode_wilayah
									JOIN ref.mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
									JOIN ref.mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
									WHERE
										pd.Soft_delete = 0
									AND rpd.Soft_delete = 0
									AND s.Soft_delete = 0
									AND rpd.jenis_keluar_id IS NULL
									AND rb.semester_id = 20141
									 AND s.bentuk_pendidikan_id in (5,6,7,8,9,10,29,13,14,15,16)";

					$fetch_jumlah_pd = getDataBySql($sql_jumlah_pd);

					foreach ($fetch_jumlah_pd as $fjpd) {
						
						foreach ($fjpd as $fj => $v) {
							$arr[$fj] = $v;
						}

					}

					$count++;

					$arr['apk_sd'] = round((($arr['jumlah_siswa_sd'] / $arr['jumlah_penduduk_6_12']) * 100),2) ." %";
					$arr['apk_smp'] = round((($arr['jumlah_siswa_smp'] / $arr['jumlah_penduduk_13_15']) * 100),2) ." %";
					$arr['apk_sma'] = round((($arr['jumlah_siswa_sma'] / $arr['jumlah_penduduk_16_19']) * 100),2) ." %";

					$arr['apm_sd'] = round((($arr['jumlah_siswa_sd_6_12'] / $arr['jumlah_penduduk_6_12']) * 100),2) ." %";
					$arr['apm_smp'] = round((($arr['jumlah_siswa_smp_13_15'] / $arr['jumlah_penduduk_13_15']) * 100),2) ." %";
					$arr['apm_sma'] = round((($arr['jumlah_siswa_sma_16_19'] / $arr['jumlah_penduduk_16_19']) * 100),2) ." %";
					
					array_push($fetchArr, $arr);
				}
				
				break;

			case 'JenisSurat':
				$jenis_surat_id = $request->get('jenis_surat_id');

				$sql = "select * from adm.jenis_surat";

				if(!empty($jenis_surat_id)){
					$sql .= " where jenis_surat_id = '".$jenis_surat_id."'";
				}

				$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;
					}

					$count++;
					
					array_push($fetchArr, $arr);
				}
				break;
			case 'JenisPrasarana':
				$query = $request->get('query');
				$jenis_prasarana_id = $request->get('jenis_prasarana_id');

				$sql = "select * from {$ref}jenis_prasarana jp where expired_date is null";

				if(!empty($query)){
					$sql .= " AND jp.nama like '%".$query."%'";
				}

				if(!empty($jenis_prasarana_id)){
					$sql .= " AND jp.jenis_prasarana_id = ".$jenis_prasarana_id;
				}

				$fetch = getDataBySql($sql);
				
				foreach ($fetch as $f) {
					$count++;
				}				

				// $sql .= " ORDER BY pd.nama LIMIT {$start},{$limit}";
				$sql .= $this->pagingCompatibility('jp.nama',$start,$limit);

				// echo $sql;die;
			 	$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}

					$count++;
					
					array_push($fetchArr, $arr);
				}

				break;

			case 'Mutasi':
				
				$mutasi_id = $request->get('mutasi_id');


				// $sql = "select * from adm.mutasi where soft_delete = 0";

				// $fetch = getDataBySql($sql);

				// foreach ($fetch as $f) {
					
				// 	foreach ($f as $fc => $v) {
				// 		$arr[$fc] = $v;

				// 	}
					
				// 	array_push($fetchArr, $arr);
				// }

				$c = new \Criteria();
				$c->add(MutasiPeer::SOFT_DELETE, 0);

				if(!empty($mutasi_id)){
					$c->add(MutasiPeer::MUTASI_ID, $mutasi_id);
				}

				$count = MutasiPeer::doCount($c);

				$c->setOffset($start);
				$c->setLimit($limit);

				$mutasis = MutasiPeer::doSelect($c);

				foreach ($mutasis as $mt) {
					$arr = $mt->toArray(\BasePeer::TYPE_FIELDNAME);

					$sekolah_id =  $mt->getSekolahId();
					$jenis_surat_id =  $mt->getJenisSuratId();
					$pengguna_id =  $mt->getPenggunaId();

					$arr['nipd'] = $mt->getNis();
					$arr['nisn'] = $mt->getNisn();

					$sql = "select nama from sekolah where sekolah_id = '".$sekolah_id."'";
					$fetch = getDataBySql($sql);

					foreach ($fetch as $f) {
						foreach ($f as $fc => $v) {
							$arr['sekolah_id_str'] = $v;

						}
					}

					$sql = "select nama as jenis_surat_id_str from adm.jenis_surat where jenis_surat_id = '".$jenis_surat_id."'";
					$fetch = getDataBySql($sql);

					foreach ($fetch as $f) {
						foreach ($f as $fc => $v) {
							$arr[$fc] = $v;

						}
					}

					$sql = "select nama as pengguna_id_str, jabatan_lembaga from pengguna where pengguna_id = '".$pengguna_id."'";
					$fetch = getDataBySql($sql);

					foreach ($fetch as $f) {
						foreach ($f as $fc => $v) {
							$arr[$fc] = $v;
						}
					}

					array_push($fetchArr, $arr);
				}
				break;
			case 'Prasarana':
				$prim 				= 'prasarana_id';
				$sekolah_id			= $request->get('sekolah_id');
				$prasarana_id	= $request->get('prasarana_id');
				$jenis_prasarana_id	= $request->get('jenis_prasarana_id');
				$keyword	= $request->get('keyword');
				// $semester_id		= $request->get('semester_id') ? $request->get('semester_id') : SEMESTER_BERJALAN;

				// $count = SekolahPeer::doCount($c);

				$sql = "SELECT
						s.nama as sekolah_id_str,
						jp.nama as jenis_prasarana_id_str,	
						sk.nama as kepemilikan_sarpras_id_str,	
						p.*
						FROM
							prasarana p
						JOIN sekolah s ON s.sekolah_id = p.sekolah_id
						JOIN {$ref}jenis_prasarana jp ON jp.jenis_prasarana_id = p.jenis_prasarana_id
						JOIN {$ref}status_kepemilikan sk on sk.status_kepemilikan_id = p.kepemilikan_sarpras_id
						WHERE
							p.soft_delete = 0
							and s.Soft_delete = 0
							";

				if(!empty($keyword)){
					$sql .= " AND p.nama like '%".$keyword."%'";
				}

				if(!empty($prasarana_id)){
					$sql .= " AND p.prasarana_id = '".$prasarana_id."'";
				}

				if(!empty($sekolah_id)){
					$sql .= " AND p.sekolah_id = '".$sekolah_id."'";
				}

				if(!empty($jenis_prasarana_id)){
					$sql .= " AND jp.jenis_prasarana_id = '".$jenis_prasarana_id."'";
				}

				// if(!empty($semester_id)){
				// 	$sql .= " AND rb.semester_id = ".$semester_id;
				// }

				$fetch = getDataBySql($sql);
				
				foreach ($fetch as $f) {
					$count++;
				}				

				// $sql .= " ORDER BY pd.nama LIMIT {$start},{$limit}";
				$sql .= $this->pagingCompatibility('p.nama',$start,$limit);

				// echo $sql;die;
			 	$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
					
					array_push($fetchArr, $arr);
				}	

				break;
			case 'BentukPendidikan':
				if(LEVEL == 'dikdas'){
					if(INCLUDE_DEPAG == true){
						$bp_id = '(5,6,29,7,8,9,10)';
					}else{
						$bp_id = '(5,6,29)';
					}

				}else if(LEVEL == 'dikmen'){
					if(INCLUDE_DEPAG == true){
						$bp_id = '(13,14,15,16,17,29)';
					}else{
						$bp_id = '(13,14,15,29)';
					}

				}else{
					if(INCLUDE_DEPAG == true){
						$bp_id = '(5,6,7,8,9,10,29,13,14,15,16,17)';
					}else{
						$bp_id = '(5,6,7,8,29,13,14,15)';
					}
				}

				$sql = "select * from {$ref}bentuk_pendidikan where bentuk_pendidikan_id in ".$bp_id;

				$fetch = getDataBySql($sql);
				
				foreach ($fetch as $f) {
					$count++;
				}				

				// $sql .= " ORDER BY nama LIMIT {$start},{$limit}";
				$sql .= $this->pagingCompatibility('nama',$start,$limit);


			 	$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
					
					array_push($fetchArr, $arr);
				}

				if(LEVEL == 'dikdas'){
					$arrSemua = array();
					$arrSemua['bentuk_pendidikan_id'] = 991;

					if(INCLUDE_DEPAG == true){
						$arrSemua['nama'] = 'Rekap SD/SDLB/MI';
					}else{
						$arrSemua['nama'] = 'Rekap SD/SDLB';
					}
					

					array_push($fetchArr, $arrSemua);

					$arrSemua = array();
					$arrSemua['bentuk_pendidikan_id'] = 992;

					if(INCLUDE_DEPAG == true){
						$arrSemua['nama'] = 'Rekap SMP/SMPLB/MTs';
					}else{
						$arrSemua['nama'] = 'Rekap SMP/SMPLB';
					}
					
					array_push($fetchArr, $arrSemua);

				}else if(LEVEL == 'dikmen'){
					$arrSemua = array();
					$arrSemua['bentuk_pendidikan_id'] = 993;
					
					if(INCLUDE_DEPAG == true){
						$arrSemua['nama'] = 'Rekap SMA/SMALB/MA/SMK/SLB';
					}else{
						$arrSemua['nama'] = 'Rekap SMA/SMALB/SMK/SLB';
					}

					array_push($fetchArr, $arrSemua);
				} else{
					$arrSemua = array();
					$arrSemua['bentuk_pendidikan_id'] = 991;
					if(INCLUDE_DEPAG == true){
						$arrSemua['nama'] = 'Rekap SD/SDLB/MI';
					}else{
						$arrSemua['nama'] = 'Rekap SD/SDLB';
					}

					array_push($fetchArr, $arrSemua);

					$arrSemua = array();
					$arrSemua['bentuk_pendidikan_id'] = 992;
					if(INCLUDE_DEPAG == true){
						$arrSemua['nama'] = 'Rekap SMP/SMPLB/MTs';
					}else{
						$arrSemua['nama'] = 'Rekap SMP/SMPLB';
					}

					array_push($fetchArr, $arrSemua);
					
					$arrSemua = array();
					$arrSemua['bentuk_pendidikan_id'] = 993;
					if(INCLUDE_DEPAG == true){
						$arrSemua['nama'] = 'Rekap SMA/SMALB/MA/SMK/SLB';
					}else{
						$arrSemua['nama'] = 'Rekap SMA/SMALB/SMK/SLB';
					}

					array_push($fetchArr, $arrSemua);
				}

				$arrSemua = array();
				$arrSemua['bentuk_pendidikan_id'] = 999;
				$arrSemua['nama'] = 'Semua';

				array_push($fetchArr, $arrSemua);
				
				break;
			case 'RekapTotalCache':

				$con = $this->initdb();
				$return = array();
				$finalArr = array();
				
				$semester_id = $request->get('semester_id') ? $request->get('semester_id') : SEMESTER_BERJALAN;
				$tahun_ajaran_id = substr($semester_id, 0,4);
				$kode_wilayah = $request->get('kode_wilayah');
				$id_level_wilayah = $request->get('id_level_wilayah');

				switch ($id_level_wilayah) {
					case 0:
						$params_lv = 'and s.mst_kode_wilayah_provinsi = '.$kode_wilayah;
						break;
					case 1:
						$params_lv = 'and s.mst_kode_wilayah_kabupaten = '.$kode_wilayah;
						break;
					case 2:
						$params_lv = 'and s.mst_kode_wilayah_kecamatan = '.$kode_wilayah;
						break;
					default:
						# code...
						break;
				}
				
				$sql = "SELECT
							'Sekolah' AS desk,
							count(1) AS total,
							sum(CASE WHEN s.bentuk_pendidikan_id = 29 THEN 1 ELSE 0 END) AS slb,
							sum(CASE WHEN s.bentuk_pendidikan_id = 13 THEN 1 ELSE 0 END) AS sma,
							sum(CASE WHEN s.bentuk_pendidikan_id = 14 THEN 1 ELSE 0 END) AS smlb,
							sum(CASE WHEN s.bentuk_pendidikan_id = 15 THEN 1 ELSE 0 END) AS smk
						FROM
							rekap_sekolah s
						WHERE
							s.semester_id = {$semester_id}
							{$params_lv}

						UNION

						select
							'Guru' as desk,
							sum(s.ptk) as total,
							sum(case when s.bentuk_pendidikan_id = 29 then s.ptk else 0 end) as slb,
							sum(case when s.bentuk_pendidikan_id = 13 then s.ptk else 0 end) as sma,
							sum(case when s.bentuk_pendidikan_id = 14 then s.ptk else 0 end) as smlb,
							sum(case when s.bentuk_pendidikan_id = 15 then s.ptk else 0 end) as smk
						FROM
							rekap_sekolah s 
						where
							s.semester_id = {$semester_id}
							{$params_lv}

						UNION

						select
							'Pegawai' as desk,
							sum(s.pegawai) as total,
							sum(case when s.bentuk_pendidikan_id = 29 then s.pegawai else 0 end) as slb,
							sum(case when s.bentuk_pendidikan_id = 13 then s.pegawai else 0 end) as sma,
							sum(case when s.bentuk_pendidikan_id = 14 then s.pegawai else 0 end) as smlb,
							sum(case when s.bentuk_pendidikan_id = 15 then s.pegawai else 0 end) as smk
						FROM
							rekap_sekolah s 
						where
							s.semester_id = {$semester_id}
							{$params_lv}

						UNION

						select 
							'Peserta Didik' as desk,
							sum(s.pd) as total,
							sum(case when s.bentuk_pendidikan_id = 29 then s.pd else 0 end) as slb,
							sum(case when s.bentuk_pendidikan_id = 13 then s.pd else 0 end) as sma,
							sum(case when s.bentuk_pendidikan_id = 14 then s.pd else 0 end) as smlb,
							sum(case when s.bentuk_pendidikan_id = 15 then s.pd else 0 end) as smk
						from
							rekap_sekolah s 
						WHERE
							s.semester_id = {$semester_id}
							{$params_lv}

						UNION
						select
							'Rombel' as desk,
							sum(s.rombel) as total,
							sum(case when s.bentuk_pendidikan_id = 29 then s.rombel else 0 end) as slb,
							sum(case when s.bentuk_pendidikan_id = 13 then s.rombel else 0 end) as sma,
							sum(case when s.bentuk_pendidikan_id = 14 then s.rombel else 0 end) as smlb,
							sum(case when s.bentuk_pendidikan_id = 15 then s.rombel else 0 end) as smk
						from
							rekap_sekolah s 
						WHERE
							s.semester_id = {$semester_id}
							{$params_lv}";

				$stmt = sqlsrv_query( $con, $sql );

				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					array_push($return, $row);

					$rowCount++;
				}

				$finalArr['results'] = $rowCount;
				$finalArr['rows'] = $return;

				return json_encode($finalArr);

				break;
			case 'RekapTotal':
				$semester_id = $request->get('semester_id') ? $request->get('semester_id') : SEMESTER_BERJALAN;
				$tahun_ajaran_id = substr($semester_id, 0,4);
				$kode_wilayah = $request->get('kode_wilayah');
				$id_level_wilayah = $request->get('id_level_wilayah');
				
				switch ($id_level_wilayah) {
					case 0:
						$params_lv = 'and w3.mst_kode_wilayah = '.$kode_wilayah;
						break;
					case 1:
						$params_lv = 'and w2.mst_kode_wilayah = '.$kode_wilayah;
						break;
					case 2:
						$params_lv = 'and w1.mst_kode_wilayah = '.$kode_wilayah;
						break;
					default:
						# code...
						break;
				}
				
				$sql = "SELECT
							'Sekolah' AS desk,
							count(1) AS total,
							sum(CASE WHEN s.bentuk_pendidikan_id = 29 THEN 1 ELSE 0 END) AS slb,
							sum(CASE WHEN s.bentuk_pendidikan_id = 13 THEN 1 ELSE 0 END) AS sma,
							sum(CASE WHEN s.bentuk_pendidikan_id = 14 THEN 1 ELSE 0 END) AS smlb,
							sum(CASE WHEN s.bentuk_pendidikan_id = 15 THEN 1 ELSE 0 END) AS smk
						FROM
							sekolah s
						JOIN {$ref}mst_wilayah w1 ON w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						WHERE
							s.Soft_delete = 0
							{$params_lv}
						UNION

						SELECT
							'PTK Terdaftar' AS desk,
							count(1) AS total,
							sum(CASE WHEN s.bentuk_pendidikan_id = 29 THEN 1 ELSE 0 END) AS slb,
							sum(CASE WHEN s.bentuk_pendidikan_id = 13 THEN 1 ELSE 0 END) AS sma,
							sum(CASE WHEN s.bentuk_pendidikan_id = 14 THEN 1 ELSE 0 END) AS smlb,
							sum(CASE WHEN s.bentuk_pendidikan_id = 15 THEN 1 ELSE 0 END) AS smk
						FROM
							ptk ptk
						JOIN ptk_terdaftar ptkd ON ptkd.ptk_id = ptk.ptk_id
						JOIN {$ref}mst_wilayah w1 ON ptk.kode_wilayah = w1.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w1.mst_kode_wilayah = w2.kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w2.mst_kode_wilayah = w3.kode_wilayah
						JOIN sekolah s ON s.sekolah_id = ptkd.sekolah_id
						JOIN {$ref}tahun_ajaran ta on ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
						WHERE
							ptk.Soft_delete = 0
						AND ptkd.Soft_delete = 0
						AND ptkd.tahun_ajaran_id = ".$tahun_ajaran_id."
						AND (
								(
									ptkd.jenis_keluar_id is null and ptkd.tgl_ptk_keluar is null
								) OR (
									ptkd.jenis_keluar_id is not null and ptkd.tgl_ptk_keluar > ta.tanggal_selesai
								)
							)
						{$params_lv}

						UNION

						SELECT
							'Peserta Didik' AS desk,
							count(1) AS total,
							sum(CASE WHEN s.bentuk_pendidikan_id = 29 THEN 1 ELSE 0 END) AS slb,
							sum(CASE WHEN s.bentuk_pendidikan_id = 13 THEN 1 ELSE 0 END) AS sma,
							sum(CASE WHEN s.bentuk_pendidikan_id = 14 THEN 1 ELSE 0 END) AS smlb,
							sum(CASE WHEN s.bentuk_pendidikan_id = 15 THEN 1 ELSE 0 END) AS smk
						FROM
							peserta_didik pd
						JOIN registrasi_peserta_didik rpd ON rpd.peserta_didik_id = pd.peserta_didik_id
						JOIN anggota_rombel ar on ar.peserta_didik_id = pd.peserta_didik_id
						JOIN rombongan_belajar rb on rb.rombongan_belajar_id = ar.rombongan_belajar_id
						JOIN sekolah s ON s.sekolah_id = rpd.sekolah_id
						JOIN {$ref}mst_wilayah w1 ON w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						JOIN {$ref}semester sm on sm.semester_id = rb.semester_id
						WHERE
							pd.Soft_delete = 0
						AND rpd.Soft_delete = 0
						AND ar.Soft_delete = 0
						AND rb.Soft_delete = 0
						AND rb.semester_id = '".$semester_id."'
						AND (
									(
										rpd.jenis_keluar_id is null and rpd.tanggal_keluar is null
									) OR (
										rpd.jenis_keluar_id is not null and rpd.tanggal_keluar > sm.tanggal_selesai
									) OR (
										rpd.jenis_keluar_id is null
									)
								)
						{$params_lv}";

						return $sql;die;

				$fetch = getDataBySql($sql);
				
				foreach ($fetch as $f) {
					$count++;
				}	

				// $sql .= "LIMIT {$start},{$limit}";

			 	$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
					
					array_push($fetchArr, $arr);
				}

				break;


			case 'Peran':
				
				$sql = "select * from {$ref}peran where expired_date is null";

				$fetch = getDataBySql($sql);
				
				foreach ($fetch as $f) {
					$count++;
				}				

				$sql .= $this->pagingCompatibility('nama',$start,$limit);

			 	$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
					
					array_push($fetchArr, $arr);
				}

				break;
			case 'Pengguna':
				$keyword = $request->get('keyword');
				$peran_id = $request->get('peran_id');
				$pengguna_id = $request->get('pengguna_id');
				
				$sql = "select png.*, pr.nama as peran_id_str, w.nama as kode_wilayah_str from pengguna png join {$ref}peran pr on pr.peran_id = png.peran_id join {$ref}mst_wilayah w on w.kode_wilayah = png.kode_wilayah where png.soft_delete = 0 and png.peran_id != 10";

				if(!empty($keyword)){
					$sql .= " AND (png.username like '%".$keyword."%' or png.nama like '%".$keyword."%') ";	
				}

				if(!empty($pengguna_id)){
					$sql .= " AND pengguna_id = '".$pengguna_id."'";	
				}

				if(!empty($peran_id)){
					$sql .= " AND png.peran_id = ".$peran_id;	
				}
				
				$fetch = getDataBySql($sql);
				
				foreach ($fetch as $f) {
					$count++;
				}				

				$sql .= $this->pagingCompatibility('png.nama',$start,$limit);

			 	$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
					
					array_push($fetchArr, $arr);
				}

				break;
			case 'AnggotaRombel':
				$peserta_didik_id = $request->get('peserta_didik_id');
				
				$sql = "SELECT
							ar.*, sm.semester_id AS semester_id,
							sm.nama AS semester_id_str,
							ta.tahun_ajaran_id AS tahun_ajaran_id,
							ta.nama AS tahun_ajaran_id_str,
							s.nama AS sekolah_id_str,
							rb.nama AS rombongan_belajar_id_str,
							jp.nama AS jenis_pendaftaran_id_str
						FROM
							anggota_rombel ar
						JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
						JOIN {$ref}semester sm ON sm.semester_id = rb.semester_id
						JOIN {$ref}tahun_ajaran ta ON ta.tahun_ajaran_id = sm.tahun_ajaran_id
						JOIN sekolah s ON s.sekolah_id = rb.sekolah_id
						JOIN {$ref}jenis_pendaftaran jp ON jp.jenis_pendaftaran_id = ar.jenis_pendaftaran_id
						WHERE
							ar.Soft_delete = 0
						AND rb.soft_delete = 0
						and s.soft_delete = 0";

				if(!empty($peserta_didik_id)){
					$sql .= " AND ar.peserta_didik_id = '".$peserta_didik_id."'";
				}

				$fetch = getDataBySql($sql);
				
				foreach ($fetch as $f) {
					$count++;
				}				

				// $sql .= " ORDER BY sm.semester_id DESC LIMIT {$start},{$limit}";

				$sql .= $this->pagingCompatibility('sm.semester_id DESC',$start,$limit);

			 	$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
					
					array_push($fetchArr, $arr);
				}

				break;

			case 'Pembelajaran':
				$ptk_id = $request->get('ptk_id');
				
				$sql = "SELECT
							pmb.*, rb.nama AS rombongan_belajar_id_str,
							s.nama AS sekolah_id_str,
							sm.nama AS semester_id_str,
							ta.nama AS tahun_ajaran_id_str,
							mp.nama as mata_pelajaran_id_str
						FROM
							pembelajaran pmb
						JOIN ptk_terdaftar ptkt ON ptkt.ptk_terdaftar_id = pmb.ptk_terdaftar_id
						JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = pmb.rombongan_belajar_id
						JOIN sekolah s ON s.sekolah_id = rb.sekolah_id
						JOIN {$ref}semester sm ON sm.semester_id = rb.semester_id
						JOIN {$ref}tahun_ajaran ta ON ta.tahun_ajaran_id = ptkt.tahun_ajaran_id
						join {$ref}mata_pelajaran mp on mp.mata_pelajaran_id = pmb.mata_pelajaran_id
						WHERE
							pmb.soft_delete = 0
						AND rb.Soft_delete = 0
						AND ptkt.Soft_delete = 0";

				if(!empty($ptk_id)){
					$sql .= " AND ptkt.ptk_id = '".$ptk_id."'";
				}

				$fetch = getDataBySql($sql);
				
				foreach ($fetch as $f) {
					$count++;
				}				

				// $sql .= " ORDER BY ptkt.tahun_ajaran_id DESC LIMIT {$start},{$limit}";

				$sql .= $this->pagingCompatibility('ptkt.tahun_ajaran_id DESC',$start,$limit);

			 	$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
					
					array_push($fetchArr, $arr);
				}

				break;

			case 'PtkTerdaftar':
				$ptk_id = $request->get('ptk_id');
				
				$sql = "SELECT
							ptkt.*,
							s.nama as sekolah_id_str,
							ta.nama as tahun_ajaran_id_str
						FROM
							ptk_terdaftar ptkt
						join sekolah s on s.sekolah_id = ptkt.sekolah_id
						join {$ref}tahun_ajaran ta on ta.tahun_ajaran_id = ptkt.tahun_ajaran_id
						WHERE
							ptkt.soft_delete = 0
							AND s.soft_delete = 0";

				if(!empty($ptk_id)){
					$sql .= " AND ptkt.ptk_id = '".$ptk_id."'";
				}

				$fetch = getDataBySql($sql);
				
				foreach ($fetch as $f) {
					$count++;
				}				

				// $sql .= " ORDER BY ptkt.tahun_ajaran_id DESC LIMIT {$start},{$limit}";

				$sql .= $this->pagingCompatibility('ptkt.tahun_ajaran_id DESC',$start,$limit);

			 	$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
					
					array_push($fetchArr, $arr);
				}

				break;
			case 'KeahlianLaboratorium':
				$sql = "select * from {$ref}keahlian_laboratorium where expired_date is null ";

				$fetch = getDataBySql($sql);
				
				foreach ($fetch as $f) {
					$count++;
				}				

				// $sql .= " ORDER BY nama LIMIT {$start},{$limit}";

				$sql .= $this->pagingCompatibility('nama',$start,$limit);

			 	$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
					
					array_push($fetchArr, $arr);
				}
				
				break;
			case 'TahunAjaran':

				$sql = "select * from {$ref}tahun_ajaran where expired_date is null ";

				$fetch = getDataBySql($sql);
				
				foreach ($fetch as $f) {
					$count++;
				}				

				// $sql .= " ORDER BY nama LIMIT {$start},{$limit}";
				$sql .= $this->pagingCompatibility('nama',$start,$limit);

			 	$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
					
					array_push($fetchArr, $arr);
				}
				
				break;

			case 'Semester':
				
				$sql = "select * from {$ref}semester where expired_date is null ";

				$fetch = getDataBySql($sql);
				
				foreach ($fetch as $f) {
					$count++;
				}				

				// $sql .= " ORDER BY nama LIMIT {$start},{$limit}";
				$sql .= $this->pagingCompatibility('nama',$start,$limit);

			 	$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
					
					array_push($fetchArr, $arr);
				}
				break;

			case 'MstWilayah':
				$session_kode_wilayah = $app['session']->get('kode_wilayah');
				$session_id_level_wilayah = $app['session']->get('id_level_wilayah');
				$mode = $request->get('mode');
				$query = $request->get('query');
				$kode_wilayah = $request->get('kode_wilayah');

				if($mode == 'bebas'){
					$sql = "select 
								* 
							from 
								{$ref}mst_wilayah 
							where 
								id_level_wilayah = 1
							and nama like '%{$query}%'
							and expired_date is null";

				 	$fetch = getDataBySql($sql);

					foreach ($fetch as $f) {
						
						foreach ($f as $fc => $v) {
							$arr[$fc] = $v;

						}

						$induk = $arr['mst_kode_wilayah'];

						//get induk wilayah
						$sql = "select * from {$ref}mst_wilayah where kode_wilayah = ".$induk;

						$fetch = getDataBySql($sql);

						$arr['nama_render'] = $arr['nama'] . ' - ' . $fetch[0]['nama'];
						// end of induk wilayah
						
						array_push($fetchArr, $arr);
					}

					$sql = "select 
								* 
							from 
								{$ref}mst_wilayah 
							where 
								id_level_wilayah = 2
							and nama like '%{$query}%'
							and expired_date is null";

				 	$fetch = getDataBySql($sql);

					foreach ($fetch as $f) {
						
						foreach ($f as $fc => $v) {
							$arr[$fc] = $v;

						}

						$induk = $arr['mst_kode_wilayah'];

						//get induk wilayah
						$sql = "select * from {$ref}mst_wilayah where kode_wilayah = ".$induk;

						$fetch = getDataBySql($sql);

						$arr['nama_render'] = $arr['nama'] . ' - ' . $fetch[0]['nama'];
						// end of induk wilayah
						
						array_push($fetchArr, $arr);
					}


				}else if($mode == 'propinsi'){

					$sql = "select 
								* 
							from 
								{$ref}mst_wilayah 
							where 
								id_level_wilayah = 1
							and expired_date is null";

				 	$fetch = getDataBySql($sql);

					foreach ($fetch as $f) {
						
						foreach ($f as $fc => $v) {
							$arr[$fc] = $v;

						}

						$arr['nama_render'] = $arr['nama'];

						array_push($fetchArr, $arr);
					}

				}else if($mode == 'kabupaten'){

					$sql = "select 
								* 
							from 
								{$ref}mst_wilayah 
							where 
								id_level_wilayah = 2
							and expired_date is null";

					if($kode_wilayah){
						$sql .= " and mst_kode_wilayah = ".$kode_wilayah;
					}else{
						$sql .= " and mst_kode_wilayah = ".$session_kode_wilayah;
					}

				 	$fetch = getDataBySql($sql);

					foreach ($fetch as $f) {
						
						foreach ($f as $fc => $v) {
							$arr[$fc] = $v;

						}

						$arr['nama_render'] = $arr['nama'];

						array_push($fetchArr, $arr);
					}

				}else if($mode == 'kecamatan'){

					$sql = "select 
								* 
							from 
								{$ref}mst_wilayah 
							where 
								id_level_wilayah = 3
							and expired_date is null";

					if($kode_wilayah){
						$sql .= " and mst_kode_wilayah = ".$kode_wilayah;
					}else{
						$sql .= " and mst_kode_wilayah = ".$session_kode_wilayah;
					}

				 	$fetch = getDataBySql($sql);

					foreach ($fetch as $f) {
						
						foreach ($f as $fc => $v) {
							$arr[$fc] = $v;

						}

						$arr['nama_render'] = $arr['nama'];

						array_push($fetchArr, $arr);
					}

				}else if($mode == 'kolom'){
					$sql = "select 
								* 
							from 
								{$ref}mst_wilayah 
							where 
								mst_kode_wilayah = ".$session_kode_wilayah;

					$fetch = getDataBySql($sql);

					foreach ($fetch as $f) {
						
						foreach ($f as $fc => $v) {
							$arr[$fc] = $v;

						}

						$arr['nama_render'] = $arr['nama'];

						array_push($fetchArr, $arr);
					}
				}else{
					$sql = "select 
								* 
							from 
								{$ref}mst_wilayah 
							where 
								kode_wilayah = ".$kode_wilayah;

					$fetch = getDataBySql($sql);

					foreach ($fetch as $f) {
						
						foreach ($f as $fc => $v) {
							$arr[$fc] = $v;

						}

						$arr['nama_render'] = $arr['nama'];

						array_push($fetchArr, $arr);
					}
				}

				break;

			case 'propinsi':
				$kode_wilayah = $request->get('kode_wilayah');
				
				$sql = "select * from {$ref}mst_wilayah where id_level_wilayah = 1 and kode_wilayah != 000000";

				if(!empty($kode_wilayah)){
					$sql .= " and kode_wilayah = ".$kode_wilayah;
				}

				$fetch = getDataBySql($sql);
				
				foreach ($fetch as $f) {
					$count++;
				}				

				// $sql .= " ORDER BY nama LIMIT {$start},{$limit}";
				$sql .= $this->pagingCompatibility('kode_wilayah',$start,$limit);

			 	$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
					
					array_push($fetchArr, $arr);
				}

				break;

			case 'Sekolah':
				$prim 		= 'sekolah_id';
				$keyword 	= $request->get('keyword');
				$query 		= $request->get('query');
				$prop 		= $request->get('prop');
				$kab 		= $request->get('kab');
				$kec 		= $request->get('kec');
				$bp 		= $request->get('bp');
				$ss 		= $request->get('ss');
				$sekolah_id	= $request->get('sekolah_id');

				// $count = SekolahPeer::doCount($c);

				$sql = "SELECT
							*,
							s.nama as 'nama_sekolah',
							w1.nama AS kecamatan,
							w2.nama AS kabupaten,
							w3.nama AS propinsi,
							w1.kode_wilayah AS kode_kecamatan,
							w2.kode_wilayah AS kode_kab,
							w3.kode_wilayah AS kode_prop,
							bp.nama as bentuk_pendidikan_id_str,
							sk.nama as status_kepemilikan_id_str
						FROM
							sekolah s
						JOIN {$ref}mst_wilayah w1 ON s.kode_wilayah = w1.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w1.mst_kode_wilayah = w2.kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w2.mst_kode_wilayah = w3.kode_wilayah
						JOIN {$ref}bentuk_pendidikan bp on bp.bentuk_pendidikan_id = s.bentuk_pendidikan_id
						JOIN {$ref}status_kepemilikan sk on sk.status_kepemilikan_id = s.status_kepemilikan_id
						WHERE
							s.Soft_delete = 0";

				if(!empty($sekolah_id)){
					$sql .= " AND s.sekolah_id = '".$sekolah_id."'";
				}

				if(!empty($prop)){
					$sql .= " AND w3.kode_wilayah = '".$prop."'";
				}

				if(!empty($kab)){
					$sql .= " AND w2.kode_wilayah = '".$kab."'";
				}

				if(!empty($kec)){
					$sql .= " AND s.kode_wilayah = '".$kec."'";
				}

				if(!empty($keyword)){
					$sql .= " AND (s.nama like '%".$keyword."%' or s.npsn like '%".$keyword."%') ";	
				}

				if(!empty($query)){
					$sql .= " AND (s.nama like '%".$query."%' or s.npsn like '%".$query."%') ";	
				}

				if(!empty($bp)){
					$sql .= $this->getParamsBp($bp);
					
				}

				if(!empty($ss)){
					if($ss != 999){
						$sql .= " AND s.status_sekolah = ".$ss;	
					}
				}

				// return $sql;die;

				$fetch = getDataBySql($sql);
				
				foreach ($fetch as $f) {
					$count++;
				}				

				// $sql .= " ORDER BY s.nama LIMIT {$start},{$limit}";
				$sql .= $this->pagingCompatibility('s.nama',$start,$limit);

			 	$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}

					if($app['session']->get('peran_id') == 1){
						$arr['koreggen'] = strtoupper(base_convert($arr['kode_registrasi'], 10, 32));
					}
					
					array_push($fetchArr, $arr);
				}
				// return $sql;die;				

				break;
			case 'Ptk':
				
				$prim 				= 'ptk_id';
				$sekolah_id			= $request->get('sekolah_id');
				$ptk_id				= $request->get('ptk_id');
				$tahun_ajaran_id	= $request->get('tahun_ajaran_id') ? $request->get('tahun_ajaran_id') : TA_BERJALAN;

				// $count = SekolahPeer::doCount($c);

				$sql = "SELECT
							ptk.*, ptkd.*, jptk.jenis_ptk AS jenis_ptk_id_str,
							sk.nama AS status_kepegawaian_id_str,
							skp.nama AS status_keaktifan_id_str,
							ptk.nama AS 'nama_ptk',
							w1.nama AS kecamatan,
							w2.nama AS kabupaten,
							w3.nama AS propinsi,
							w1.kode_wilayah AS kode_kecamatan,
							w2.kode_wilayah AS kode_kab,
							w3.kode_wilayah AS kode_prop,
							ag.nama as agama_id_str,
							lp.nama as lembaga_pengangkat_id_str,
							pg.nama as pangkat_golongan_id_str,
							sg.nama as sumber_gaji_id_str,
							kl.nama as keahlian_laboratorium_id_str
						FROM
							ptk ptk
						JOIN {$ref}mst_wilayah w1 ON ptk.kode_wilayah = w1.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w1.mst_kode_wilayah = w2.kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w2.mst_kode_wilayah = w3.kode_wilayah
						JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
						JOIN {$ref}tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
						LEFT JOIN {$ref}status_kepegawaian sk ON sk.status_kepegawaian_id = ptk.status_kepegawaian_id
						LEFT JOIN {$ref}jenis_ptk jptk ON jptk.jenis_ptk_id = ptk.jenis_ptk_id
						LEFT JOIN {$ref}status_keaktifan_pegawai skp ON skp.status_keaktifan_id = ptk.status_keaktifan_id
						LEFT JOIN {$ref}lembaga_pengangkat lp ON lp.lembaga_pengangkat_id = ptk.lembaga_pengangkat_id
						LEFT JOIN {$ref}pangkat_golongan pg ON pg.pangkat_golongan_id = ptk.pangkat_golongan_id
						LEFT JOIN {$ref}agama ag on ag.agama_id = ptk.agama_id
						LEFT JOIN {$ref}sumber_gaji sg on sg.sumber_gaji_id = ptk.sumber_gaji_id
						LEFT JOIN {$ref}keahlian_laboratorium kl on kl.keahlian_laboratorium_id = ptk.keahlian_laboratorium_id
						WHERE
							ptk.Soft_delete = 0
						AND ptkd.Soft_delete = 0
						AND (
								(
									ptkd.jenis_keluar_id is null and ptkd.tgl_ptk_keluar is null
								) OR (
									ptkd.jenis_keluar_id is not null and ptkd.tgl_ptk_keluar > ta.tanggal_selesai
								) OR (
									ptkd.jenis_keluar_id is null
								)
							)
						";

				if(!empty($ptk_id)){
					$sql .= " AND ptk.ptk_id = '".$ptk_id."'";
				}

				if(!empty($sekolah_id)){
					$sql .= " AND ptk.entry_sekolah_id = '".$sekolah_id."'";
				}

				if(!empty($tahun_ajaran_id)){
					$sql .= " AND ptkd.tahun_ajaran_id = ".$tahun_ajaran_id;
				}

				$fetch = getDataBySql($sql);
				
				foreach ($fetch as $f) {
					$count++;
				}				

				// $sql .= " ORDER BY ptk.nama LIMIT {$start},{$limit}";
				$sql .= $this->pagingCompatibility('ptk.nama',$start,$limit);

// 				echo $sql;die;
			 	$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
					
					array_push($fetchArr, $arr);
				}	

				break;
			case 'PesertaDidik':
				$prim 				= 'peserta_didik_id';
				$sekolah_id			= $request->get('sekolah_id');
				$peserta_didik_id	= $request->get('peserta_didik_id');
				$semester_id		= $request->get('semester_id') ? $request->get('semester_id') : SEMESTER_BERJALAN;

				// $count = SekolahPeer::doCount($c);

				$sql = "SELECT
							pd.*, rpd.*,
							s.nama as 'sekolah_id_str'
						FROM
							registrasi_peserta_didik rpd
						INNER JOIN peserta_didik pd ON rpd.peserta_didik_id = pd.peserta_didik_id
						INNER JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
						INNER JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
						JOIN {$ref}semester sm ON sm.semester_id = rb.semester_id
						JOIN sekolah s on s.sekolah_id = rpd.sekolah_id
						WHERE
							rpd.Soft_delete = 0
						AND pd.Soft_delete = 0
						AND (
								(
									rpd.jenis_keluar_id is null and rpd.tanggal_keluar is null
								) OR (
									rpd.jenis_keluar_id is not null and rpd.tanggal_keluar > sm.tanggal_selesai
								) OR (
									rpd.jenis_keluar_id is null
								)
							)
						AND rb.semester_id = '".$semester_id."'
						AND rb.soft_delete = 0
						AND ar.soft_delete = 0";

				if(!empty($peserta_didik_id)){
					$sql .= " AND pd.peserta_didik_id = '".$peserta_didik_id."'";
				}

				if(!empty($sekolah_id)){
					$sql .= " AND rpd.sekolah_id = '".$sekolah_id."'";
				}

				if(!empty($semester_id)){
					$sql .= " AND rb.semester_id = ".$semester_id;
				}

				$fetch = getDataBySql($sql);
				
				foreach ($fetch as $f) {
					$count++;
				}				

				// $sql .= " ORDER BY pd.nama LIMIT {$start},{$limit}";
				$sql .= $this->pagingCompatibility('pd.nama',$start,$limit);

				// echo $sql;die;
			 	$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
					
					array_push($fetchArr, $arr);
				}	

				break;
			case 'PesertaDidikSimpel':
				$prim 				= 'peserta_didik_id';
				$keyword			= $request->get('keyword');
				$peserta_didik_id	= $request->get('peserta_didik_id');
				$semester_id 		= $request->get('semester_id') ? $request->get('semester_id') : SEMESTER_BERJALAN;

				$session_kode_wilayah = $app['session']->get('kode_wilayah');
				$session_id_level_wilayah = $app['session']->get('id_level_wilayah');

				$query 				= $request->get('query');

				$sql = "select 
							* ,
							pd.nama as nama
						from 
							peserta_didik pd 
							join registrasi_peserta_didik rpd on rpd.peserta_didik_id = pd.peserta_didik_id
							join sekolah s on s.sekolah_id = rpd.sekolah_id
							join {$ref}mst_wilayah w1 on w1.kode_wilayah = s.kode_wilayah
							join {$ref}mst_wilayah w2 on w2.kode_wilayah = w1.mst_kode_wilayah
							join {$ref}mst_wilayah w3 on w3.kode_wilayah = w2.mst_kode_wilayah
						where 
							pd.soft_delete = 0
						and rpd.soft_delete = 0
						and pd.soft_delete = 0
						and rpd.jenis_keluar_id is null";

				if(!empty($keyword) ){
					$sql .= " AND (pd.nama like '%".$keyword."%' or pd.nisn like '%".$keyword."%')";
				}

				if(!empty($query) ){
					$sql .= " AND (pd.nama like '%".$query."%' or pd.nisn like '%".$query."%')";
				}

				if($session_id_level_wilayah == 0){
					$sql .= " AND w3.mst_kode_wilayah = ".$session_kode_wilayah;
				}else if($session_id_level_wilayah == 1){
					$sql .= " AND w2.mst_kode_wilayah = ".$session_kode_wilayah;
				}else if($session_id_level_wilayah == 2){
					$sql .= " AND w1.mst_kode_wilayah = ".$session_kode_wilayah;
				}

				// return $sql;die;

				$fetchCount = getDataBySql($sql);
				
				foreach ($fetchCount as $ff) {
					$count++;
				}				

				// $sql .= " ORDER BY pd.nama LIMIT {$start},{$limit}";
				$sql .= $this->pagingCompatibility('pd.nama',$start,$limit);

			 	$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
					
					array_push($fetchArr, $arr);
				}	
				


				break;
			case 'DataPokokPesertaDidik':
				$prim 				= 'peserta_didik_id';
				$keyword			= $request->get('keyword');
				$peserta_didik_id	= $request->get('peserta_didik_id');
				$semester_id 		= $request->get('semester_id') ? $request->get('semester_id') : SEMESTER_BERJALAN;

				$prop 				= $request->get('prop');
				$kab 				= $request->get('kab');
				$kec 				= $request->get('kec');
				$ss 				= $request->get('ss');
				$bp_id 				= $request->get('bp_id');

				$session_kode_wilayah = $app['session']->get('kode_wilayah');
				$session_id_level_wilayah = $app['session']->get('id_level_wilayah');

				$query 				= $request->get('query');

				$modul = $request->get('modul');

				if($modul == 'mutasi'){
					$select_mutasi = "pk.nama as pekerjaan_id_ayah_str,";
					$join_mutasi = "LEFT JOIN {$ref}pekerjaan pk on pk.pekerjaan_id = pd.pekerjaan_id_ayah";
				}else{
					$select_mutasi = '';
					$join_mutasi = '';
				}
				
				$sqlCount = "select count(1) as jumlah from peserta_didik pd
							JOIN registrasi_peserta_didik rpd ON rpd.peserta_didik_id = pd.peserta_didik_id
							JOIN anggota_rombel ar on ar.peserta_didik_id = pd.peserta_didik_id
							JOIN rombongan_belajar rb on rb.rombongan_belajar_id = ar.rombongan_belajar_id
							JOIN sekolah s ON s.sekolah_id = rpd.sekolah_id
							JOIN {$ref}mst_wilayah w1 ON s.kode_wilayah = w1.kode_wilayah
							JOIN {$ref}mst_wilayah w2 ON w1.mst_kode_wilayah = w2.kode_wilayah
							JOIN {$ref}mst_wilayah w3 ON w2.mst_kode_wilayah = w3.kode_wilayah
							WHERE
								pd.Soft_delete = 0
							AND rpd.Soft_delete = 0
							AND rb.semester_id = {$semester_id}
							AND (
									(
										rpd.jenis_keluar_id is null and rpd.tanggal_keluar is null
									) OR (
										rpd.jenis_keluar_id is not null and rpd.tanggal_keluar > (select tanggal_selesai from {$ref}semester where semester_id = '".$semester_id."')
									) OR (
										rpd.jenis_keluar_id is null
									)
								)
							AND rb.jenis_rombel = 1";

				$sql = "select 
							pd.*,
							rpd.*,
							{$select_mutasi}
							rb.tingkat_pendidikan_id as kelas,
							ag.nama as agama_id_str,
							s.nama as sekolah_id_str 
						from 
							peserta_didik pd
						JOIN registrasi_peserta_didik rpd ON rpd.peserta_didik_id = pd.peserta_didik_id
						JOIN anggota_rombel ar on ar.peserta_didik_id = pd.peserta_didik_id
						JOIN rombongan_belajar rb on rb.rombongan_belajar_id = ar.rombongan_belajar_id
						JOIN sekolah s ON s.sekolah_id = rpd.sekolah_id
						JOIN {$ref}mst_wilayah w1 ON s.kode_wilayah = w1.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w1.mst_kode_wilayah = w2.kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w2.mst_kode_wilayah = w3.kode_wilayah
						LEFT JOIN {$ref}agama ag on ag.agama_id = pd.agama_id
						{$join_mutasi}
						WHERE
							pd.Soft_delete = 0
						AND rpd.Soft_delete = 0
						AND rb.semester_id = {$semester_id}
						AND (
								(
									rpd.jenis_keluar_id is null and rpd.tanggal_keluar is null
								) OR (
									rpd.jenis_keluar_id is not null and rpd.tanggal_keluar > (select tanggal_selesai from {$ref}semester where semester_id = '".$semester_id."')
								) OR (
									rpd.jenis_keluar_id is null
								)
							)
						AND rb.jenis_rombel = 1";

				if(!empty($keyword) ){
					$sql .= " AND (pd.nama like '%".$keyword."%' or pd.nisn like '%".$keyword."%')";
					$sqlCount .= " AND (pd.nama like '%".$keyword."%' or pd.nisn like '%".$keyword."%')";
				}

				if(!empty($query) ){
					$sql .= " AND (pd.nama like '%".$query."%' or pd.nisn like '%".$query."%')";
					$sqlCount .= " AND (pd.nama like '%".$query."%' or pd.nisn like '%".$query."%')";
				}

				if(!empty($peserta_didik_id)){
					$sql .= " AND pd.peserta_didik_id = '".$peserta_didik_id."'";
					$sqlCount .= " AND pd.peserta_didik_id = '".$peserta_didik_id."'";
				}

				if($session_id_level_wilayah == 0){
					$sql .= " AND w3.mst_kode_wilayah = ".$session_kode_wilayah;
					$sqlCount .= " AND w3.mst_kode_wilayah = ".$session_kode_wilayah;
				}else if($session_id_level_wilayah == 1){
					$sql .= " AND w2.mst_kode_wilayah = ".$session_kode_wilayah;
					$sqlCount .= " AND w2.mst_kode_wilayah = ".$session_kode_wilayah;
				}else if($session_id_level_wilayah == 2){
					$sql .= " AND w1.mst_kode_wilayah = ".$session_kode_wilayah;
					$sqlCount .= " AND w1.mst_kode_wilayah = ".$session_kode_wilayah;
				}

				//filter baru
				if($prop){
					$sql .= " AND w2.mst_kode_wilayah = ".$prop;
					$sqlCount .= " AND w2.mst_kode_wilayah = ".$prop;
				}

				if($kab){
					$sql .= " AND w1.mst_kode_wilayah = ".$kab;
					$sqlCount .= " AND w1.mst_kode_wilayah = ".$kab;
				}

				if($kec){
					$sql .= " AND w1.kode_wilayah = ".$kec;
					$sqlCount .= " AND w1.kode_wilayah = ".$kec;
				}

				if($bp_id){
					$sql .= $this->getParamsBp($bp_id);
					$sqlCount .= $this->getParamsBp($bp_id);
				}

				if($ss){
					if($ss != 999){	
						$sql .= ' and s.status_sekolah = '.$ss;
						$sqlCount .= ' and s.status_sekolah = '.$ss;
					}
				}
				

				// return $sql;die;
				
				$fetchCount = getDataBySql($sqlCount);
				
				foreach ($fetchCount as $ff) {
					$count = $ff['jumlah'];
				}				

				// $sql .= " ORDER BY pd.nama LIMIT {$start},{$limit}";
				$sql .= $this->pagingCompatibility('pd.nama',$start,$limit);

			 	$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
					
					array_push($fetchArr, $arr);
				}	
				break;
			case 'DataPokokPtk':
				$prim 				= 'ptk_id';
				$keyword			= $request->get('keyword');
				$ptk_id				= $request->get('ptk_id');
				$semester_id 		= $request->get('semester_id') ? $request->get('semester_id') : SEMESTER_BERJALAN;
				$tahun_ajaran_id 	= substr($semester_id, 0,4);

				$session_kode_wilayah = $app['session']->get('kode_wilayah');
				$session_id_level_wilayah = $app['session']->get('id_level_wilayah');

				$prop 				= $request->get('prop');
				$kab 				= $request->get('kab');
				$kec 				= $request->get('kec');
				$ss 				= $request->get('ss');
				$bp_id 				= $request->get('bp_id');

				$jurusan_id			= $request->get('jurusan_id');
				$jenis_ptk_id 		= $request->get('jenis_ptk_id');

				if($jurusan_id){
					$param_jur 	 = ",(SELECT
							count(1) as jumlah
							FROM
								pembelajaran pb
							JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = pb.rombongan_belajar_id
							JOIN jurusan_sp jsp ON jsp.jurusan_sp_id = rb.jurusan_sp_id
							JOIN ref.jurusan j ON j.jurusan_id = jsp.jurusan_id
							WHERE
							pb.Soft_delete = 0
							AND rb.Soft_delete = 0
							AND rb.semester_id = ".$semester_id."
							and pb.ptk_terdaftar_id = ptkd.ptk_terdaftar_id
							and j.jurusan_id = '".$jurusan_id."'
							GROUP BY j.nama_jurusan, j.jurusan_id) as jumlah";

					$where_jur = " and (SELECT
							count(1) as jumlah
							FROM
								pembelajaran pb
							JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = pb.rombongan_belajar_id
							JOIN jurusan_sp jsp ON jsp.jurusan_sp_id = rb.jurusan_sp_id
							JOIN ref.jurusan j ON j.jurusan_id = jsp.jurusan_id
							WHERE
							pb.Soft_delete = 0
							AND rb.Soft_delete = 0
							AND rb.semester_id = ".$semester_id."
							and pb.ptk_terdaftar_id = ptkd.ptk_terdaftar_id
							and j.jurusan_id = '".$jurusan_id."'
							GROUP BY j.nama_jurusan, j.jurusan_id) > 0";
				}else{
					$param_jur = '';
					$where_jur = '';
				}

				$sqlCount = "SELECT
							count(1) as jumlah
						FROM
							ptk ptk
						JOIN ptk_terdaftar ptkd on ptkd.ptk_id = ptk.ptk_id
						JOIN sekolah s on s.sekolah_id = ptkd.sekolah_id
						JOIN {$ref}mst_wilayah w1 ON s.kode_wilayah = w1.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w1.mst_kode_wilayah = w2.kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w2.mst_kode_wilayah = w3.kode_wilayah
						JOIN {$ref}tahun_ajaran ta on ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
						LEFT JOIN {$ref}jenis_ptk jp ON jp.jenis_ptk_id = ptk.jenis_ptk_id
						LEFT JOIN {$ref}agama ag ON ag.agama_id = ptk.agama_id
						LEFT JOIN {$ref}lembaga_pengangkat lp ON lp.lembaga_pengangkat_id = ptk.lembaga_pengangkat_id
						LEFT JOIN {$ref}pangkat_golongan pg ON pg.pangkat_golongan_id = ptk.pangkat_golongan_id
						LEFT JOIN {$ref}sumber_gaji sg ON sg.sumber_gaji_id = ptk.sumber_gaji_id
						LEFT JOIN {$ref}status_kepegawaian sk ON sk.status_kepegawaian_id = ptk.status_kepegawaian_id
						LEFT JOIN {$ref}status_keaktifan_pegawai skp ON skp.status_keaktifan_id = ptk.status_keaktifan_id
						LEFT JOIN {$ref}keahlian_laboratorium kl ON kl.keahlian_laboratorium_id = ptk.keahlian_laboratorium_id
						WHERE
							ptk.soft_delete = 0
							and ptkd.soft_delete = 0
							{$where_jur}
							AND (
									(
										ptkd.jenis_keluar_id is null and ptkd.tgl_ptk_keluar is null
									) OR (
										ptkd.jenis_keluar_id is not null and ptkd.tgl_ptk_keluar > ta.tanggal_selesai
									) OR (
										ptkd.jenis_keluar_id is null
									)
								)
						AND ta.tahun_ajaran_id = ".$tahun_ajaran_id;

				$sql = "SELECT
							ptk.*,
							ptk.nama AS 'nama_ptk',
							w1.nama AS kecamatan,
							w2.nama AS kabupaten,
							w3.nama AS propinsi,
							w1.kode_wilayah AS kode_kecamatan,
							w2.kode_wilayah AS kode_kab,
							w3.kode_wilayah AS kode_prop,
							jp.jenis_ptk as jenis_ptk_id_str,
							ag.nama as agama_id_str,
							lp.nama as lembaga_pengangkat_id_str,
							pg.nama as pangkat_golongan_id_str,
							sg.nama as sumber_gaji_id_str,
							sk.nama AS status_kepegawaian_id_str,
							skp.nama AS status_keaktifan_id_str,
							kl.nama as keahlian_laboratorium_id_str,
							ptkd.ptk_terdaftar_id
							
						FROM
							ptk ptk
						JOIN ptk_terdaftar ptkd on ptkd.ptk_id = ptk.ptk_id
						JOIN sekolah s on s.sekolah_id = ptkd.sekolah_id
						JOIN {$ref}mst_wilayah w1 ON s.kode_wilayah = w1.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w1.mst_kode_wilayah = w2.kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w2.mst_kode_wilayah = w3.kode_wilayah
						JOIN {$ref}tahun_ajaran ta on ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
						LEFT JOIN {$ref}jenis_ptk jp ON jp.jenis_ptk_id = ptk.jenis_ptk_id
						LEFT JOIN {$ref}agama ag ON ag.agama_id = ptk.agama_id
						LEFT JOIN {$ref}lembaga_pengangkat lp ON lp.lembaga_pengangkat_id = ptk.lembaga_pengangkat_id
						LEFT JOIN {$ref}pangkat_golongan pg ON pg.pangkat_golongan_id = ptk.pangkat_golongan_id
						LEFT JOIN {$ref}sumber_gaji sg ON sg.sumber_gaji_id = ptk.sumber_gaji_id
						LEFT JOIN {$ref}status_kepegawaian sk ON sk.status_kepegawaian_id = ptk.status_kepegawaian_id
						LEFT JOIN {$ref}status_keaktifan_pegawai skp ON skp.status_keaktifan_id = ptk.status_keaktifan_id
						LEFT JOIN {$ref}keahlian_laboratorium kl ON kl.keahlian_laboratorium_id = ptk.keahlian_laboratorium_id
						WHERE
							ptk.soft_delete = 0
							and ptkd.soft_delete = 0
							AND (
								(
									ptkd.jenis_keluar_id is null and ptkd.tgl_ptk_keluar is null
								) OR (
									ptkd.jenis_keluar_id is not null and ptkd.tgl_ptk_keluar > ta.tanggal_selesai
								) OR (
									ptkd.jenis_keluar_id is null
								)
							)
							{$where_jur}
							AND ta.tahun_ajaran_id = ".$tahun_ajaran_id;


				if(!empty($keyword)){
					$sql .= " AND (ptk.nama like '%".$keyword."%' or ptk.nuptk like '%".$keyword."%')";
					$sqlCount .= " AND (ptk.nama like '%".$keyword."%' or ptk.nuptk like '%".$keyword."%')";
				}

				if(!empty($ptk_id)){
					$sql .= " AND ptk.ptk_id = '".$ptk_id."'";
					$sqlCount .= " AND ptk.ptk_id = '".$ptk_id."'";
				}

				if($session_id_level_wilayah == 0){
					$sql .= " AND w3.mst_kode_wilayah = ".$session_kode_wilayah;
					$sqlCount .= " AND w3.mst_kode_wilayah = ".$session_kode_wilayah;
				}else if($session_id_level_wilayah == 1){
					$sql .= " AND w2.mst_kode_wilayah = ".$session_kode_wilayah;
					$sqlCount .= " AND w2.mst_kode_wilayah = ".$session_kode_wilayah;
				}else if($session_id_level_wilayah == 2){
					$sql .= " AND w1.mst_kode_wilayah = ".$session_kode_wilayah;
					$sqlCount .= " AND w1.mst_kode_wilayah = ".$session_kode_wilayah;
				}

				//filter baru
				if($prop){
					$sql .= " AND w2.mst_kode_wilayah = ".$prop;
					$sqlCount .= " AND w2.mst_kode_wilayah = ".$prop;
				}

				if($kab){
					$sql .= " AND w1.mst_kode_wilayah = ".$kab;
					$sqlCount .= " AND w1.mst_kode_wilayah = ".$kab;
				}

				if($kec){
					$sql .= " AND w1.kode_wilayah = ".$kec;
					$sqlCount .= " AND w1.kode_wilayah = ".$kec;
				}

				if($bp_id){
					$sql .= $this->getParamsBp($bp_id);
					$sqlCount .= $this->getParamsBp($bp_id);
				}

				if($ss){
					if($ss != 999){	
						$sql .= ' and s.status_sekolah = '.$ss;
						$sqlCount .= ' and s.status_sekolah = '.$ss;
					}
				}

				if($jenis_ptk_id){
					$sql .= " and ptk.jenis_ptk_id = ".$jenis_ptk_id;
					$sqlCount .= " and ptk.jenis_ptk_id = ".$jenis_ptk_id;
				}

				// return $sql;die;
				
				$fetchCount = getDataBySql($sqlCount);
				
				foreach ($fetchCount as $ff) {
					$count = $ff['jumlah'];
				}				

				// $sql .= " ORDER BY ptk.nama LIMIT {$start},{$limit}";
				$sql .= $this->pagingCompatibility('ptk.nama',$start,$limit);

			 	$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}

					
					array_push($fetchArr, $arr);
	
				}	
				break;
		}

		$return = array();

		$return['results'] = $count;
		$return['id'] = $prim;
		$return['start'] = $start;
		$return['limit'] = $limit;
		$return['rows'] = $fetchArr;
		$return['sql'] = $sql;

		if($arrKolom){
			$return['kolom'] = $arrKolom;
		}

		return json_encode($return);


	}

	function post(Request $request, Application $app){
		$model = $request->get('model');
		$session = $app['session']->get('pengguna_id');

		switch ($model) {
			case 'ApkApm':
				
				$return = array();
				
				$data = $request->get('data');

				$datadec = json_decode($data);
				$guid = $this->getGuid();

				// return $datadec->{'apk_apm_id'};


				$obj = new ApkApm();

				$obj->setApkApmId($guid);
				$obj->setJumlahPenduduk612($datadec->{'jumlah_penduduk_6_12'});
				$obj->setJumlahPenduduk1315($datadec->{'jumlah_penduduk_13_15'});
				$obj->setJumlahPenduduk1619($datadec->{'jumlah_penduduk_16_19'});
				$obj->setPenggunaId($session);
				$obj->setTanggal(date('Y-m-d H:i:s'));
				
				if($obj->save()){
					$return['success'] = true;
					$return['message'] = 'Berhasil Memperbarui data APK/APM';
				}else{
					$return['success'] = false;
					$return['message'] = 'Gagal Memperbarui data APK/APM';
				}

				break;
			case 'Mutasi':

				$peserta_didik_id = $request->get('peserta_didik_id');
				$alamat_jalan = $request->get('alamat_jalan');
				$alamat_yang_diikuti = $request->get('alamat_yang_diikuti');
				$hubungan_keluarga = $request->get('hubungan_keluarga');
				$kelas = $request->get('kelas');
				$kode_wilayah_berangkat = $request->get('kode_wilayah_berangkat');
				$maksud_tujuan = $request->get('maksud_tujuan');
				$nama = $request->get('nama');
				$nama_ayah = $request->get('nama_ayah');
				$nama_yang_diikuti = $request->get('nama_yang_diikuti');
				$nipd = $request->get('nipd');
				$nisn = $request->get('nisn');
				$nomor_surat = $request->get('nomor_surat');
				$pekerjaan = $request->get('pekerjaan');
				$pekerjaan_id_ayah_str = $request->get('pekerjaan_id_ayah_str');
				$sekolah_id = $request->get('sekolah_id');
				$tanggal_berangkat = $request->get('tanggal_berangkat');
				$tanggal_lahir = $request->get('tanggal_lahir');
				$tempat_lahir = $request->get('tempat_lahir');
				$jenis_surat_id = $request->get('jenis_surat_id');
				$pengguna_id = $request->get('pengguna_id');
				$keterangan_kepala_sekolah = $request->get('keterangan_kepala_sekolah');
				$keterangan_lain = $request->get('keterangan_lain');
				$tanggal_surat = $request->get('tanggal_surat');
				$mutasi_id = $request->get('mutasi_id');

				$obj = MutasiPeer::retrieveByPK($mutasi_id);

				if(!is_object($obj)){
					$obj = new Mutasi();

					$guid = $this->getGuid();

					$obj->setMutasiId($guid);
					$obj->setSoftDelete(0);

					$stats = 'Menambah';
					$perlu_reload = 'true';
				}else{
					$stats = 'Mengubah';
					$perlu_reload = 'false';
				}
				
				// $c = new \Criteria();

				$obj->setPesertaDidikId($peserta_didik_id);
				$obj->setNomorSurat($nomor_surat);
				$obj->setSekolahId($sekolah_id);
				$obj->setKodeWilayahBerangkat($kode_wilayah_berangkat);
				$obj->setMaksudTujuan($maksud_tujuan);
				$obj->setNamaYangDiikuti($nama_yang_diikuti);
				$obj->setHubunganKeluarga($hubungan_keluarga);
				$obj->setPekerjaan($pekerjaan);
				$obj->setAlamatYangDiikuti($alamat_yang_diikuti);
				$obj->setTanggalBerangkat($tanggal_berangkat);
				$obj->setNama($nama);
				$obj->setTempatLahir($tempat_lahir);
				$obj->setTanggalLahir($tanggal_lahir);
				$obj->setKelas($kelas);
				$obj->setNis($nipd);
				$obj->setNisn($nisn);
				$obj->setNamaAyah($nama_ayah);
				$obj->setPekerjaanIdAyahStr($pekerjaan_id_ayah_str);
				$obj->setAlamatJalan($alamat_jalan);
				$obj->setJenisSuratId($jenis_surat_id);
				$obj->setPenggunaId($pengguna_id);
				$obj->setKeteranganKepalaSekolah($keterangan_kepala_sekolah);
				$obj->setKeteranganLain($keterangan_lain);
				$obj->setTanggalSurat($tanggal_surat);

				if($obj->save()){
					return '{"success": true, "message": "Berhasil '.$stats.' Mutasi Peserta Didik dengan nama '.$nama.'. <br> Apakah Anda ingin melanjutkan edit atau cetak?", "mutasi_id" : "'.$obj->getMutasiId().'","perlu_reload": '.$perlu_reload.'}';
				}else{
					return '{"success": false, "message": "Gagal '.$stats.' Mutasi Peserta Didik dengan nama '.$nama.'"}';
				}


				break;
			case 'GantiPassword':
				
				$pengguna_id = $request->get('pengguna_id');
				$password_lama = $request->get('password_lama');
				$password_baru = $request->get('password_baru');

				$sql = "select lOWER( CONVERT(VARCHAR(32), HashBytes('MD5', '".$password_lama."'), 2) ) as iPassword,* from pengguna where pengguna_id = '{$pengguna_id}'";

				$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					$count++;
				}

				// return $count;die;

				if($count > 0){

					if( $fetch[0]['iPassword'] ==  $fetch[0]['password'] ) {

						$sql = "update pengguna set
								password = lOWER( CONVERT(VARCHAR(32), HashBytes('MD5', '{$password_baru}'), 2) ),
								last_update = '".date('Y-m-d H:i:s')."'
							where pengguna_id = '".$fetch[0]['pengguna_id']."'";
						

						$con = \Propel::getConnection(DBNAME);
						$stmt = $con->prepare($sql); 
						$stmt->execute();

						if($stmt){
							$return = array();
							$return['success'] = true;
							$return['message'] = 'Berhasil memperbarui password Pengguna';
						}else{
							$return = array();
							$return['success'] = false;
							$return['message'] = 'Gagal memperbarui password Pengguna';
						}

					} else{

						$return = array();
						$return['success'] = false;
						$return['message'] = 'Password Lama tidak sesuai!';
					}
				}else{

					$return = array();
					$return['success'] = false;
					$return['message'] = 'Pengguna tidak ditemukan!';
				}

				return json_encode($return);

				break;
			case 'Pengguna':
				
				$pengguna_id = $request->get('pengguna_id');
				$nama = $request->get('nama');
				$username = $request->get('username');
				$password = $request->get('password');
				$alamat = $request->get('alamat');
				$no_telepon = $request->get('no_telepon');
				$no_hp = $request->get('no_hp');
				$aktif = $request->get('aktif');
				$peran_id = $request->get('peran_id');
				$nip_nim = $request->get('nip_nim');
				$jabatan_lembaga = $request->get('jabatan_lembaga');
				$kode_wilayah = $request->get('kode_wilayah');

				$sql = "select * from pengguna where pengguna_id = '{$pengguna_id}'";

				// $con = \Propel::getConnection(DBNAME);
				// $stmt = $con->prepare($sql); 
				// $stmt->execute(); 
				$fetch = getDataBySql($sql);
				
				foreach ($fetch as $f) {
					$count++;
				}	
				// if($stmt){

				// return var_dump($fetch);die;
				if($count == 0){
					// $pengguna = new Pengguna();

					// $rand = rand('0','999999999999');

					// $pengguna->setPenggunaId($rand);
					$guid = $this->getGuid();
					$message = 'menambah';

					$sql = "insert into pengguna(
							pengguna_id,
							nama,
							username,
							password,
							alamat,
							no_telepon,
							no_hp,
							nip_nim,
							jabatan_lembaga,
							aktif,
							kode_wilayah,
							peran_id,
							last_update,
							last_sync,
							soft_delete,
							updater_id
						) values(
							'{$guid}',
							'{$nama}',
							'{$username}',
							lOWER( CONVERT(VARCHAR(32), HashBytes('MD5', '{$password}'), 2) ),
							'{$alamat}',
							'{$no_telepon}',
							'{$no_hp}',
							'{$nip_nim}',
							'{$jabatan_lembaga}',
							{$aktif},
							'{$kode_wilayah}',
							{$peran_id},
							'".date('Y-m-d H:i:s')."',
							'".date('Y-m-d H:i:s')."',
							0,
							'07517B9B-1961-43DA-9A55-925630F2EC9B'
						)";
				}else{
					$message = 'memperbarui';

					$sql = "update pengguna set
								nama = '{$nama}',
								alamat = '{$alamat}',
								no_telepon = '{$no_telepon}',
								no_hp = '{$no_hp}',
								nip_nim = '{$nip_nim}',
								jabatan_lembaga = '{$jabatan_lembaga}',
								aktif = {$aktif},
								kode_wilayah = '{$kode_wilayah}',
								peran_id = {$peran_id},
								last_update = '".date('Y-m-d H:i:s')."',
								last_sync = '".date('Y-m-d H:i:s')."',
								soft_delete = 0,
								updater_id = '07517B9B-1961-43DA-9A55-925630F2EC9B'
							where pengguna_id = '".$fetch[0]['pengguna_id']."'";
				}

				// return $sql;die;

				$con = \Propel::getConnection(DBNAME);
				$stmt = $con->prepare($sql); 
				$stmt->execute(); 

				// $pengguna->setNama($nama);
				// $pengguna->setUsername($username);
				
				// if(!empty($password)){
				// 	$pengguna->setPassword($password);	
				// }

				// $pengguna->setAlamat($alamat);
				// $pengguna->setNoTelepon($no_telepon);
				// $pengguna->setAktif($aktif);
				// $pengguna->setKodeWilayah(KODE_WILAYAH);
				// $pengguna->setPeranId($peran_id);
				// $pengguna->setLastUpdate(date('Y-m-d H:i:s'));
				// $pengguna->setLastSync(date('Y-m-d H:i:s'));
				// $pengguna->setSoftDelete(0);
				// $pengguna->setUpdaterId('07517B9B-1961-43DA-9A55-925630F2EC9B');

				if($stmt){
					$return = array();
					$return['success'] = true;
					$return['message'] = 'Berhasil '.$message.' Pengguna';
				}else{
					$return = array();
					$return['success'] = false;
					$return['message'] = 'Gagal '.$message.' Pengguna';
				}

				// return $sql;die;

				break;
			
		}

		return json_encode($return);
	}

	function delete(Request $request, Application $app){
		$model = $request->get('model');
		$return = array();
		
		switch ($model) {
			case 'Mutasi':
				
				$id = $request->get('mutasi_id');
				$cid = 'mutasi_id';
				// $obj = PenggunaPeer::retrieveByPK($request->get('pengguna_id'));
				$obj = 'adm.mutasi';
				
				break;
			case 'Pengguna':
				$id = $request->get('pengguna_id');
				$cid = 'pengguna_id';
				// $obj = PenggunaPeer::retrieveByPK($request->get('pengguna_id'));
				$obj = 'pengguna';

				break;
		}

		// $obj->setSoftDelete(1);

		$sql = "update {$obj} set soft_delete = 1 where {$cid} = '{$id}'";

		$con = \Propel::getConnection(DBNAME);
		$stmt = $con->prepare($sql); 
		$stmt->execute(); 

		if($stmt){
			$return['success'] = true;
			$return['message'] = 'Berhasil Menghapus Data';
		}else{
			$return['success'] = false;
			$return['message'] = 'Gagal Menghapus Data';
		}

		return json_encode($return);
	}

	function getParamsBp($bp){
		if(!empty($bp)){

			if($bp == 991){
				if(INCLUDE_DEPAG == true){
					$params_bp = " AND s.bentuk_pendidikan_id in (5,7,9)";
				}else{
					$params_bp = " AND s.bentuk_pendidikan_id in (5,7)";
				}

			}else if($bp == 992){
				if(INCLUDE_DEPAG == true){
					$params_bp = " AND s.bentuk_pendidikan_id in (6,8,10)";
				}else{
					$params_bp = " AND s.bentuk_pendidikan_id in (6,8)";
				}

			}else if($bp == 993){
				if(INCLUDE_DEPAG == true){
					$params_bp = " AND s.bentuk_pendidikan_id in (29,13,14,15,16)";
				}else{
					$params_bp = " AND s.bentuk_pendidikan_id in (29,13,14,15)";
				}
				
			}else if($bp == 999 || $bp == 'null'){
				if(INCLUDE_DEPAG == true){
					$params_bp = " AND s.bentuk_pendidikan_id in (5,6,7,8,9,10,29,13,14,15,16)";
				}else{
					$params_bp = " AND s.bentuk_pendidikan_id in (5,6,7,8,29,13,14,15)";
				}
				
			}else{
				$params_bp = " AND s.bentuk_pendidikan_id = ".$bp;
			}


		}else{

			if(INCLUDE_DEPAG == true){
				$params_bp = " AND s.bentuk_pendidikan_id in (5,6,7,8,9,10,29,13,14,15,16)";
			}else{
				$params_bp = " AND s.bentuk_pendidikan_id in (5,6,7,8,29,13,14,15)";
			}

		}
		
		return $params_bp;
	}

	function remoteFileExists($url) {
        $curl = curl_init($url);

        //don't fetch the actual page, you only want to check the connection is ok
        curl_setopt($curl, CURLOPT_NOBODY, true);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT ,0); 
        curl_setopt($curl, CURLOPT_TIMEOUT, 10000); //timeout in seconds

        set_time_limit(100);

        //do request
        $result = curl_exec($curl);

        $ret = false;

        //if request did not fail
        if ($result !== false) {
            //if request was ok, check response code
            $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);  

            if ($statusCode == 200) {
                $ret = true;   
            }
        }

        curl_close($curl);

        return $ret;
    }

	function getRss(Request $request, Application $app){
		// return 'tes';die;

        $path = 'cache';
        $filename = 'rss_berita.xml';
        $url = 'http://dapo.dikmen.kemdikbud.go.id/portal/web/rss/berita';

        $json = file_get_contents($url);  

        $json_decode = json_decode($json);

        $feed = new Feed();

        // if($feed){
        // 	return 'koke';
        // }else{
        // 	return 'nggak oke';
        // }

        $channel = new Channel();
        $channel
            ->title("RSS Berita Dapodikmen")
            ->description("Berita seputar Dapodikmen")
            ->url('http://dapo.dikmen.kemdikbud.go.id')
            ->appendTo($feed);

        foreach ($json_decode as $jd) {
            
            // RSS item
            $item = new Item();
            $item
                ->title($jd->{'judul'})
                ->description($jd->{'isi'})
                ->url($jd->{'link'})
                ->guid($jd->{'tanggal'}, true)
                ->appendTo($channel);

        }

        return new Response($feed,
            200,
            array('Content-Type' => 'application/xml')
        );
        
    }

    function genKoreg(Request $request, Application $app){
    	// $con = $this->initdb();
		$id = $request->get('id');

		$sql = "update dbo.sekolah set kode_registrasi = CONVERT(bigint,CONVERT(varbinary(6),newid(),1)) where sekolah_id = '".$id."'";
		
		$con = \Propel::getConnection(DBNAME);
		$stmt = $con->prepare($sql); 
		$stmt->execute();

		if($stmt){

			$sql_retrieve = "select kode_registrasi from dbo.sekolah where sekolah_id = '".$id."'";
			$fetch = getDataBySql($sql_retrieve);

			$encode = strtoupper(base_convert($fetch[0]['kode_registrasi'],10,32));

			return "{'success' : true, 'koreg' : '".$encode."'}";
		}else{
			return "{'success' : false}";
		}

    }

}