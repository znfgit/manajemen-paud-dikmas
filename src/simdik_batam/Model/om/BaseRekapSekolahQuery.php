<?php

namespace simdik_batam\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \PDO;
use \Propel;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use simdik_batam\Model\RekapSekolah;
use simdik_batam\Model\RekapSekolahPeer;
use simdik_batam\Model\RekapSekolahQuery;

/**
 * Base class that represents a query for the 'rekap_sekolah' table.
 *
 * 
 *
 * @method RekapSekolahQuery orderByKodeWilayah($order = Criteria::ASC) Order by the kode_wilayah column
 * @method RekapSekolahQuery orderByWilayah1($order = Criteria::ASC) Order by the wilayah1 column
 * @method RekapSekolahQuery orderByWilayah2($order = Criteria::ASC) Order by the wilayah2 column
 * @method RekapSekolahQuery orderByWilayah3($order = Criteria::ASC) Order by the wilayah3 column
 * @method RekapSekolahQuery orderByBentukPendidikanId($order = Criteria::ASC) Order by the bentuk_pendidikan_id column
 * @method RekapSekolahQuery orderByWilayah3Nama($order = Criteria::ASC) Order by the wilayah3_nama column
 * @method RekapSekolahQuery orderByWilayah2Nama($order = Criteria::ASC) Order by the wilayah2_nama column
 * @method RekapSekolahQuery orderByWilayah1Nama($order = Criteria::ASC) Order by the wilayah1_nama column
 * @method RekapSekolahQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method RekapSekolahQuery orderByNegeri($order = Criteria::ASC) Order by the Negeri column
 * @method RekapSekolahQuery orderBySwasta($order = Criteria::ASC) Order by the Swasta column
 *
 * @method RekapSekolahQuery groupByKodeWilayah() Group by the kode_wilayah column
 * @method RekapSekolahQuery groupByWilayah1() Group by the wilayah1 column
 * @method RekapSekolahQuery groupByWilayah2() Group by the wilayah2 column
 * @method RekapSekolahQuery groupByWilayah3() Group by the wilayah3 column
 * @method RekapSekolahQuery groupByBentukPendidikanId() Group by the bentuk_pendidikan_id column
 * @method RekapSekolahQuery groupByWilayah3Nama() Group by the wilayah3_nama column
 * @method RekapSekolahQuery groupByWilayah2Nama() Group by the wilayah2_nama column
 * @method RekapSekolahQuery groupByWilayah1Nama() Group by the wilayah1_nama column
 * @method RekapSekolahQuery groupByNama() Group by the nama column
 * @method RekapSekolahQuery groupByNegeri() Group by the Negeri column
 * @method RekapSekolahQuery groupBySwasta() Group by the Swasta column
 *
 * @method RekapSekolahQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method RekapSekolahQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method RekapSekolahQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method RekapSekolah findOne(PropelPDO $con = null) Return the first RekapSekolah matching the query
 * @method RekapSekolah findOneOrCreate(PropelPDO $con = null) Return the first RekapSekolah matching the query, or a new RekapSekolah object populated from the query conditions when no match is found
 *
 * @method RekapSekolah findOneByWilayah1(string $wilayah1) Return the first RekapSekolah filtered by the wilayah1 column
 * @method RekapSekolah findOneByWilayah2(string $wilayah2) Return the first RekapSekolah filtered by the wilayah2 column
 * @method RekapSekolah findOneByWilayah3(string $wilayah3) Return the first RekapSekolah filtered by the wilayah3 column
 * @method RekapSekolah findOneByBentukPendidikanId(int $bentuk_pendidikan_id) Return the first RekapSekolah filtered by the bentuk_pendidikan_id column
 * @method RekapSekolah findOneByWilayah3Nama(string $wilayah3_nama) Return the first RekapSekolah filtered by the wilayah3_nama column
 * @method RekapSekolah findOneByWilayah2Nama(string $wilayah2_nama) Return the first RekapSekolah filtered by the wilayah2_nama column
 * @method RekapSekolah findOneByWilayah1Nama(string $wilayah1_nama) Return the first RekapSekolah filtered by the wilayah1_nama column
 * @method RekapSekolah findOneByNama(string $nama) Return the first RekapSekolah filtered by the nama column
 * @method RekapSekolah findOneByNegeri(int $Negeri) Return the first RekapSekolah filtered by the Negeri column
 * @method RekapSekolah findOneBySwasta(int $Swasta) Return the first RekapSekolah filtered by the Swasta column
 *
 * @method array findByKodeWilayah(string $kode_wilayah) Return RekapSekolah objects filtered by the kode_wilayah column
 * @method array findByWilayah1(string $wilayah1) Return RekapSekolah objects filtered by the wilayah1 column
 * @method array findByWilayah2(string $wilayah2) Return RekapSekolah objects filtered by the wilayah2 column
 * @method array findByWilayah3(string $wilayah3) Return RekapSekolah objects filtered by the wilayah3 column
 * @method array findByBentukPendidikanId(int $bentuk_pendidikan_id) Return RekapSekolah objects filtered by the bentuk_pendidikan_id column
 * @method array findByWilayah3Nama(string $wilayah3_nama) Return RekapSekolah objects filtered by the wilayah3_nama column
 * @method array findByWilayah2Nama(string $wilayah2_nama) Return RekapSekolah objects filtered by the wilayah2_nama column
 * @method array findByWilayah1Nama(string $wilayah1_nama) Return RekapSekolah objects filtered by the wilayah1_nama column
 * @method array findByNama(string $nama) Return RekapSekolah objects filtered by the nama column
 * @method array findByNegeri(int $Negeri) Return RekapSekolah objects filtered by the Negeri column
 * @method array findBySwasta(int $Swasta) Return RekapSekolah objects filtered by the Swasta column
 *
 * @package    propel.generator.simdik_batam.Model.om
 */
abstract class BaseRekapSekolahQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseRekapSekolahQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'dapodikdasmen_batam', $modelName = 'simdik_batam\\Model\\RekapSekolah', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new RekapSekolahQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   RekapSekolahQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return RekapSekolahQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof RekapSekolahQuery) {
            return $criteria;
        }
        $query = new RekapSekolahQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   RekapSekolah|RekapSekolah[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = RekapSekolahPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(RekapSekolahPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RekapSekolah A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByKodeWilayah($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RekapSekolah A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [kode_wilayah], [wilayah1], [wilayah2], [wilayah3], [bentuk_pendidikan_id], [wilayah3_nama], [wilayah2_nama], [wilayah1_nama], [nama], [Negeri], [Swasta] FROM [rekap_sekolah] WHERE [kode_wilayah] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new RekapSekolah();
            $obj->hydrate($row);
            RekapSekolahPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return RekapSekolah|RekapSekolah[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|RekapSekolah[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RekapSekolahPeer::KODE_WILAYAH, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RekapSekolahPeer::KODE_WILAYAH, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the kode_wilayah column
     *
     * Example usage:
     * <code>
     * $query->filterByKodeWilayah('fooValue');   // WHERE kode_wilayah = 'fooValue'
     * $query->filterByKodeWilayah('%fooValue%'); // WHERE kode_wilayah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodeWilayah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByKodeWilayah($kodeWilayah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodeWilayah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kodeWilayah)) {
                $kodeWilayah = str_replace('*', '%', $kodeWilayah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::KODE_WILAYAH, $kodeWilayah, $comparison);
    }

    /**
     * Filter the query on the wilayah1 column
     *
     * Example usage:
     * <code>
     * $query->filterByWilayah1('fooValue');   // WHERE wilayah1 = 'fooValue'
     * $query->filterByWilayah1('%fooValue%'); // WHERE wilayah1 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $wilayah1 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByWilayah1($wilayah1 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($wilayah1)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $wilayah1)) {
                $wilayah1 = str_replace('*', '%', $wilayah1);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::WILAYAH1, $wilayah1, $comparison);
    }

    /**
     * Filter the query on the wilayah2 column
     *
     * Example usage:
     * <code>
     * $query->filterByWilayah2('fooValue');   // WHERE wilayah2 = 'fooValue'
     * $query->filterByWilayah2('%fooValue%'); // WHERE wilayah2 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $wilayah2 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByWilayah2($wilayah2 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($wilayah2)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $wilayah2)) {
                $wilayah2 = str_replace('*', '%', $wilayah2);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::WILAYAH2, $wilayah2, $comparison);
    }

    /**
     * Filter the query on the wilayah3 column
     *
     * Example usage:
     * <code>
     * $query->filterByWilayah3('fooValue');   // WHERE wilayah3 = 'fooValue'
     * $query->filterByWilayah3('%fooValue%'); // WHERE wilayah3 LIKE '%fooValue%'
     * </code>
     *
     * @param     string $wilayah3 The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByWilayah3($wilayah3 = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($wilayah3)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $wilayah3)) {
                $wilayah3 = str_replace('*', '%', $wilayah3);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::WILAYAH3, $wilayah3, $comparison);
    }

    /**
     * Filter the query on the bentuk_pendidikan_id column
     *
     * Example usage:
     * <code>
     * $query->filterByBentukPendidikanId(1234); // WHERE bentuk_pendidikan_id = 1234
     * $query->filterByBentukPendidikanId(array(12, 34)); // WHERE bentuk_pendidikan_id IN (12, 34)
     * $query->filterByBentukPendidikanId(array('min' => 12)); // WHERE bentuk_pendidikan_id >= 12
     * $query->filterByBentukPendidikanId(array('max' => 12)); // WHERE bentuk_pendidikan_id <= 12
     * </code>
     *
     * @param     mixed $bentukPendidikanId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByBentukPendidikanId($bentukPendidikanId = null, $comparison = null)
    {
        if (is_array($bentukPendidikanId)) {
            $useMinMax = false;
            if (isset($bentukPendidikanId['min'])) {
                $this->addUsingAlias(RekapSekolahPeer::BENTUK_PENDIDIKAN_ID, $bentukPendidikanId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($bentukPendidikanId['max'])) {
                $this->addUsingAlias(RekapSekolahPeer::BENTUK_PENDIDIKAN_ID, $bentukPendidikanId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::BENTUK_PENDIDIKAN_ID, $bentukPendidikanId, $comparison);
    }

    /**
     * Filter the query on the wilayah3_nama column
     *
     * Example usage:
     * <code>
     * $query->filterByWilayah3Nama('fooValue');   // WHERE wilayah3_nama = 'fooValue'
     * $query->filterByWilayah3Nama('%fooValue%'); // WHERE wilayah3_nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $wilayah3Nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByWilayah3Nama($wilayah3Nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($wilayah3Nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $wilayah3Nama)) {
                $wilayah3Nama = str_replace('*', '%', $wilayah3Nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::WILAYAH3_NAMA, $wilayah3Nama, $comparison);
    }

    /**
     * Filter the query on the wilayah2_nama column
     *
     * Example usage:
     * <code>
     * $query->filterByWilayah2Nama('fooValue');   // WHERE wilayah2_nama = 'fooValue'
     * $query->filterByWilayah2Nama('%fooValue%'); // WHERE wilayah2_nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $wilayah2Nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByWilayah2Nama($wilayah2Nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($wilayah2Nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $wilayah2Nama)) {
                $wilayah2Nama = str_replace('*', '%', $wilayah2Nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::WILAYAH2_NAMA, $wilayah2Nama, $comparison);
    }

    /**
     * Filter the query on the wilayah1_nama column
     *
     * Example usage:
     * <code>
     * $query->filterByWilayah1Nama('fooValue');   // WHERE wilayah1_nama = 'fooValue'
     * $query->filterByWilayah1Nama('%fooValue%'); // WHERE wilayah1_nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $wilayah1Nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByWilayah1Nama($wilayah1Nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($wilayah1Nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $wilayah1Nama)) {
                $wilayah1Nama = str_replace('*', '%', $wilayah1Nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::WILAYAH1_NAMA, $wilayah1Nama, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the Negeri column
     *
     * Example usage:
     * <code>
     * $query->filterByNegeri(1234); // WHERE Negeri = 1234
     * $query->filterByNegeri(array(12, 34)); // WHERE Negeri IN (12, 34)
     * $query->filterByNegeri(array('min' => 12)); // WHERE Negeri >= 12
     * $query->filterByNegeri(array('max' => 12)); // WHERE Negeri <= 12
     * </code>
     *
     * @param     mixed $negeri The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterByNegeri($negeri = null, $comparison = null)
    {
        if (is_array($negeri)) {
            $useMinMax = false;
            if (isset($negeri['min'])) {
                $this->addUsingAlias(RekapSekolahPeer::NEGERI, $negeri['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($negeri['max'])) {
                $this->addUsingAlias(RekapSekolahPeer::NEGERI, $negeri['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::NEGERI, $negeri, $comparison);
    }

    /**
     * Filter the query on the Swasta column
     *
     * Example usage:
     * <code>
     * $query->filterBySwasta(1234); // WHERE Swasta = 1234
     * $query->filterBySwasta(array(12, 34)); // WHERE Swasta IN (12, 34)
     * $query->filterBySwasta(array('min' => 12)); // WHERE Swasta >= 12
     * $query->filterBySwasta(array('max' => 12)); // WHERE Swasta <= 12
     * </code>
     *
     * @param     mixed $swasta The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function filterBySwasta($swasta = null, $comparison = null)
    {
        if (is_array($swasta)) {
            $useMinMax = false;
            if (isset($swasta['min'])) {
                $this->addUsingAlias(RekapSekolahPeer::SWASTA, $swasta['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($swasta['max'])) {
                $this->addUsingAlias(RekapSekolahPeer::SWASTA, $swasta['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapSekolahPeer::SWASTA, $swasta, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   RekapSekolah $rekapSekolah Object to remove from the list of results
     *
     * @return RekapSekolahQuery The current query, for fluid interface
     */
    public function prune($rekapSekolah = null)
    {
        if ($rekapSekolah) {
            $this->addUsingAlias(RekapSekolahPeer::KODE_WILAYAH, $rekapSekolah->getKodeWilayah(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
