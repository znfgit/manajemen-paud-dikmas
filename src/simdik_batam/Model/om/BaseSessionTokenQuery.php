<?php

namespace simdik_batam\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \PDO;
use \Propel;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use simdik_batam\Model\SessionToken;
use simdik_batam\Model\SessionTokenPeer;
use simdik_batam\Model\SessionTokenQuery;

/**
 * Base class that represents a query for the 'session_token' table.
 *
 * 
 *
 * @method SessionTokenQuery orderBySekolahId($order = Criteria::ASC) Order by the sekolah_id column
 * @method SessionTokenQuery orderByToken($order = Criteria::ASC) Order by the token column
 * @method SessionTokenQuery orderByDateCreated($order = Criteria::ASC) Order by the date_created column
 * @method SessionTokenQuery orderByDateExpired($order = Criteria::ASC) Order by the date_expired column
 *
 * @method SessionTokenQuery groupBySekolahId() Group by the sekolah_id column
 * @method SessionTokenQuery groupByToken() Group by the token column
 * @method SessionTokenQuery groupByDateCreated() Group by the date_created column
 * @method SessionTokenQuery groupByDateExpired() Group by the date_expired column
 *
 * @method SessionTokenQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method SessionTokenQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method SessionTokenQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method SessionToken findOne(PropelPDO $con = null) Return the first SessionToken matching the query
 * @method SessionToken findOneOrCreate(PropelPDO $con = null) Return the first SessionToken matching the query, or a new SessionToken object populated from the query conditions when no match is found
 *
 * @method SessionToken findOneByToken(string $token) Return the first SessionToken filtered by the token column
 * @method SessionToken findOneByDateCreated(string $date_created) Return the first SessionToken filtered by the date_created column
 * @method SessionToken findOneByDateExpired(string $date_expired) Return the first SessionToken filtered by the date_expired column
 *
 * @method array findBySekolahId(string $sekolah_id) Return SessionToken objects filtered by the sekolah_id column
 * @method array findByToken(string $token) Return SessionToken objects filtered by the token column
 * @method array findByDateCreated(string $date_created) Return SessionToken objects filtered by the date_created column
 * @method array findByDateExpired(string $date_expired) Return SessionToken objects filtered by the date_expired column
 *
 * @package    propel.generator.simdik_batam.Model.om
 */
abstract class BaseSessionTokenQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseSessionTokenQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'dapodikdasmen_batam', $modelName = 'simdik_batam\\Model\\SessionToken', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new SessionTokenQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   SessionTokenQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return SessionTokenQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof SessionTokenQuery) {
            return $criteria;
        }
        $query = new SessionTokenQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   SessionToken|SessionToken[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = SessionTokenPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(SessionTokenPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 SessionToken A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneBySekolahId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 SessionToken A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [sekolah_id], [token], [date_created], [date_expired] FROM [session_token] WHERE [sekolah_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new SessionToken();
            $obj->hydrate($row);
            SessionTokenPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return SessionToken|SessionToken[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|SessionToken[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return SessionTokenQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(SessionTokenPeer::SEKOLAH_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return SessionTokenQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(SessionTokenPeer::SEKOLAH_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the sekolah_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySekolahId('fooValue');   // WHERE sekolah_id = 'fooValue'
     * $query->filterBySekolahId('%fooValue%'); // WHERE sekolah_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sekolahId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SessionTokenQuery The current query, for fluid interface
     */
    public function filterBySekolahId($sekolahId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sekolahId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sekolahId)) {
                $sekolahId = str_replace('*', '%', $sekolahId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SessionTokenPeer::SEKOLAH_ID, $sekolahId, $comparison);
    }

    /**
     * Filter the query on the token column
     *
     * Example usage:
     * <code>
     * $query->filterByToken('fooValue');   // WHERE token = 'fooValue'
     * $query->filterByToken('%fooValue%'); // WHERE token LIKE '%fooValue%'
     * </code>
     *
     * @param     string $token The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SessionTokenQuery The current query, for fluid interface
     */
    public function filterByToken($token = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($token)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $token)) {
                $token = str_replace('*', '%', $token);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(SessionTokenPeer::TOKEN, $token, $comparison);
    }

    /**
     * Filter the query on the date_created column
     *
     * Example usage:
     * <code>
     * $query->filterByDateCreated('2011-03-14'); // WHERE date_created = '2011-03-14'
     * $query->filterByDateCreated('now'); // WHERE date_created = '2011-03-14'
     * $query->filterByDateCreated(array('max' => 'yesterday')); // WHERE date_created > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateCreated The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SessionTokenQuery The current query, for fluid interface
     */
    public function filterByDateCreated($dateCreated = null, $comparison = null)
    {
        if (is_array($dateCreated)) {
            $useMinMax = false;
            if (isset($dateCreated['min'])) {
                $this->addUsingAlias(SessionTokenPeer::DATE_CREATED, $dateCreated['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateCreated['max'])) {
                $this->addUsingAlias(SessionTokenPeer::DATE_CREATED, $dateCreated['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SessionTokenPeer::DATE_CREATED, $dateCreated, $comparison);
    }

    /**
     * Filter the query on the date_expired column
     *
     * Example usage:
     * <code>
     * $query->filterByDateExpired('2011-03-14'); // WHERE date_expired = '2011-03-14'
     * $query->filterByDateExpired('now'); // WHERE date_expired = '2011-03-14'
     * $query->filterByDateExpired(array('max' => 'yesterday')); // WHERE date_expired > '2011-03-13'
     * </code>
     *
     * @param     mixed $dateExpired The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return SessionTokenQuery The current query, for fluid interface
     */
    public function filterByDateExpired($dateExpired = null, $comparison = null)
    {
        if (is_array($dateExpired)) {
            $useMinMax = false;
            if (isset($dateExpired['min'])) {
                $this->addUsingAlias(SessionTokenPeer::DATE_EXPIRED, $dateExpired['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($dateExpired['max'])) {
                $this->addUsingAlias(SessionTokenPeer::DATE_EXPIRED, $dateExpired['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(SessionTokenPeer::DATE_EXPIRED, $dateExpired, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   SessionToken $sessionToken Object to remove from the list of results
     *
     * @return SessionTokenQuery The current query, for fluid interface
     */
    public function prune($sessionToken = null)
    {
        if ($sessionToken) {
            $this->addUsingAlias(SessionTokenPeer::SEKOLAH_ID, $sessionToken->getSekolahId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
