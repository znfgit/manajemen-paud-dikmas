<?php

namespace simdik_batam\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \PDO;
use \Propel;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use simdik_batam\Model\RekapRombelPerJumlah;
use simdik_batam\Model\RekapRombelPerJumlahPeer;
use simdik_batam\Model\RekapRombelPerJumlahQuery;

/**
 * Base class that represents a query for the 'adm.rekap_rombel_per_jumlah' table.
 *
 * 
 *
 * @method RekapRombelPerJumlahQuery orderBySekolahId($order = Criteria::ASC) Order by the sekolah_id column
 * @method RekapRombelPerJumlahQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method RekapRombelPerJumlahQuery orderByTotal($order = Criteria::ASC) Order by the total column
 * @method RekapRombelPerJumlahQuery orderByKurang10($order = Criteria::ASC) Order by the kurang_10 column
 * @method RekapRombelPerJumlahQuery orderBySepuluhDuapuluh($order = Criteria::ASC) Order by the sepuluh_duapuluh column
 * @method RekapRombelPerJumlahQuery orderByDuapuluhsatuTigapuluhdua($order = Criteria::ASC) Order by the duapuluhsatu_tigapuluhdua column
 * @method RekapRombelPerJumlahQuery orderByLebih32($order = Criteria::ASC) Order by the lebih_32 column
 *
 * @method RekapRombelPerJumlahQuery groupBySekolahId() Group by the sekolah_id column
 * @method RekapRombelPerJumlahQuery groupByNama() Group by the nama column
 * @method RekapRombelPerJumlahQuery groupByTotal() Group by the total column
 * @method RekapRombelPerJumlahQuery groupByKurang10() Group by the kurang_10 column
 * @method RekapRombelPerJumlahQuery groupBySepuluhDuapuluh() Group by the sepuluh_duapuluh column
 * @method RekapRombelPerJumlahQuery groupByDuapuluhsatuTigapuluhdua() Group by the duapuluhsatu_tigapuluhdua column
 * @method RekapRombelPerJumlahQuery groupByLebih32() Group by the lebih_32 column
 *
 * @method RekapRombelPerJumlahQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method RekapRombelPerJumlahQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method RekapRombelPerJumlahQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method RekapRombelPerJumlah findOne(PropelPDO $con = null) Return the first RekapRombelPerJumlah matching the query
 * @method RekapRombelPerJumlah findOneOrCreate(PropelPDO $con = null) Return the first RekapRombelPerJumlah matching the query, or a new RekapRombelPerJumlah object populated from the query conditions when no match is found
 *
 * @method RekapRombelPerJumlah findOneByNama(string $nama) Return the first RekapRombelPerJumlah filtered by the nama column
 * @method RekapRombelPerJumlah findOneByTotal(string $total) Return the first RekapRombelPerJumlah filtered by the total column
 * @method RekapRombelPerJumlah findOneByKurang10(string $kurang_10) Return the first RekapRombelPerJumlah filtered by the kurang_10 column
 * @method RekapRombelPerJumlah findOneBySepuluhDuapuluh(string $sepuluh_duapuluh) Return the first RekapRombelPerJumlah filtered by the sepuluh_duapuluh column
 * @method RekapRombelPerJumlah findOneByDuapuluhsatuTigapuluhdua(string $duapuluhsatu_tigapuluhdua) Return the first RekapRombelPerJumlah filtered by the duapuluhsatu_tigapuluhdua column
 * @method RekapRombelPerJumlah findOneByLebih32(string $lebih_32) Return the first RekapRombelPerJumlah filtered by the lebih_32 column
 *
 * @method array findBySekolahId(string $sekolah_id) Return RekapRombelPerJumlah objects filtered by the sekolah_id column
 * @method array findByNama(string $nama) Return RekapRombelPerJumlah objects filtered by the nama column
 * @method array findByTotal(string $total) Return RekapRombelPerJumlah objects filtered by the total column
 * @method array findByKurang10(string $kurang_10) Return RekapRombelPerJumlah objects filtered by the kurang_10 column
 * @method array findBySepuluhDuapuluh(string $sepuluh_duapuluh) Return RekapRombelPerJumlah objects filtered by the sepuluh_duapuluh column
 * @method array findByDuapuluhsatuTigapuluhdua(string $duapuluhsatu_tigapuluhdua) Return RekapRombelPerJumlah objects filtered by the duapuluhsatu_tigapuluhdua column
 * @method array findByLebih32(string $lebih_32) Return RekapRombelPerJumlah objects filtered by the lebih_32 column
 *
 * @package    propel.generator.simdik_batam.Model.om
 */
abstract class BaseRekapRombelPerJumlahQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseRekapRombelPerJumlahQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'dapodikdasmen_batam', $modelName = 'simdik_batam\\Model\\RekapRombelPerJumlah', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new RekapRombelPerJumlahQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   RekapRombelPerJumlahQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return RekapRombelPerJumlahQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof RekapRombelPerJumlahQuery) {
            return $criteria;
        }
        $query = new RekapRombelPerJumlahQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   RekapRombelPerJumlah|RekapRombelPerJumlah[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = RekapRombelPerJumlahPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(RekapRombelPerJumlahPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RekapRombelPerJumlah A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneBySekolahId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 RekapRombelPerJumlah A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [sekolah_id], [nama], [total], [kurang_10], [sepuluh_duapuluh], [duapuluhsatu_tigapuluhdua], [lebih_32] FROM [adm].[rekap_rombel_per_jumlah] WHERE [sekolah_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new RekapRombelPerJumlah();
            $obj->hydrate($row);
            RekapRombelPerJumlahPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return RekapRombelPerJumlah|RekapRombelPerJumlah[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|RekapRombelPerJumlah[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return RekapRombelPerJumlahQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(RekapRombelPerJumlahPeer::SEKOLAH_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return RekapRombelPerJumlahQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(RekapRombelPerJumlahPeer::SEKOLAH_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the sekolah_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySekolahId('fooValue');   // WHERE sekolah_id = 'fooValue'
     * $query->filterBySekolahId('%fooValue%'); // WHERE sekolah_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sekolahId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapRombelPerJumlahQuery The current query, for fluid interface
     */
    public function filterBySekolahId($sekolahId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sekolahId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sekolahId)) {
                $sekolahId = str_replace('*', '%', $sekolahId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapRombelPerJumlahPeer::SEKOLAH_ID, $sekolahId, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapRombelPerJumlahQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(RekapRombelPerJumlahPeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the total column
     *
     * Example usage:
     * <code>
     * $query->filterByTotal(1234); // WHERE total = 1234
     * $query->filterByTotal(array(12, 34)); // WHERE total IN (12, 34)
     * $query->filterByTotal(array('min' => 12)); // WHERE total >= 12
     * $query->filterByTotal(array('max' => 12)); // WHERE total <= 12
     * </code>
     *
     * @param     mixed $total The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapRombelPerJumlahQuery The current query, for fluid interface
     */
    public function filterByTotal($total = null, $comparison = null)
    {
        if (is_array($total)) {
            $useMinMax = false;
            if (isset($total['min'])) {
                $this->addUsingAlias(RekapRombelPerJumlahPeer::TOTAL, $total['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($total['max'])) {
                $this->addUsingAlias(RekapRombelPerJumlahPeer::TOTAL, $total['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapRombelPerJumlahPeer::TOTAL, $total, $comparison);
    }

    /**
     * Filter the query on the kurang_10 column
     *
     * Example usage:
     * <code>
     * $query->filterByKurang10(1234); // WHERE kurang_10 = 1234
     * $query->filterByKurang10(array(12, 34)); // WHERE kurang_10 IN (12, 34)
     * $query->filterByKurang10(array('min' => 12)); // WHERE kurang_10 >= 12
     * $query->filterByKurang10(array('max' => 12)); // WHERE kurang_10 <= 12
     * </code>
     *
     * @param     mixed $kurang10 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapRombelPerJumlahQuery The current query, for fluid interface
     */
    public function filterByKurang10($kurang10 = null, $comparison = null)
    {
        if (is_array($kurang10)) {
            $useMinMax = false;
            if (isset($kurang10['min'])) {
                $this->addUsingAlias(RekapRombelPerJumlahPeer::KURANG_10, $kurang10['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($kurang10['max'])) {
                $this->addUsingAlias(RekapRombelPerJumlahPeer::KURANG_10, $kurang10['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapRombelPerJumlahPeer::KURANG_10, $kurang10, $comparison);
    }

    /**
     * Filter the query on the sepuluh_duapuluh column
     *
     * Example usage:
     * <code>
     * $query->filterBySepuluhDuapuluh(1234); // WHERE sepuluh_duapuluh = 1234
     * $query->filterBySepuluhDuapuluh(array(12, 34)); // WHERE sepuluh_duapuluh IN (12, 34)
     * $query->filterBySepuluhDuapuluh(array('min' => 12)); // WHERE sepuluh_duapuluh >= 12
     * $query->filterBySepuluhDuapuluh(array('max' => 12)); // WHERE sepuluh_duapuluh <= 12
     * </code>
     *
     * @param     mixed $sepuluhDuapuluh The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapRombelPerJumlahQuery The current query, for fluid interface
     */
    public function filterBySepuluhDuapuluh($sepuluhDuapuluh = null, $comparison = null)
    {
        if (is_array($sepuluhDuapuluh)) {
            $useMinMax = false;
            if (isset($sepuluhDuapuluh['min'])) {
                $this->addUsingAlias(RekapRombelPerJumlahPeer::SEPULUH_DUAPULUH, $sepuluhDuapuluh['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($sepuluhDuapuluh['max'])) {
                $this->addUsingAlias(RekapRombelPerJumlahPeer::SEPULUH_DUAPULUH, $sepuluhDuapuluh['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapRombelPerJumlahPeer::SEPULUH_DUAPULUH, $sepuluhDuapuluh, $comparison);
    }

    /**
     * Filter the query on the duapuluhsatu_tigapuluhdua column
     *
     * Example usage:
     * <code>
     * $query->filterByDuapuluhsatuTigapuluhdua(1234); // WHERE duapuluhsatu_tigapuluhdua = 1234
     * $query->filterByDuapuluhsatuTigapuluhdua(array(12, 34)); // WHERE duapuluhsatu_tigapuluhdua IN (12, 34)
     * $query->filterByDuapuluhsatuTigapuluhdua(array('min' => 12)); // WHERE duapuluhsatu_tigapuluhdua >= 12
     * $query->filterByDuapuluhsatuTigapuluhdua(array('max' => 12)); // WHERE duapuluhsatu_tigapuluhdua <= 12
     * </code>
     *
     * @param     mixed $duapuluhsatuTigapuluhdua The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapRombelPerJumlahQuery The current query, for fluid interface
     */
    public function filterByDuapuluhsatuTigapuluhdua($duapuluhsatuTigapuluhdua = null, $comparison = null)
    {
        if (is_array($duapuluhsatuTigapuluhdua)) {
            $useMinMax = false;
            if (isset($duapuluhsatuTigapuluhdua['min'])) {
                $this->addUsingAlias(RekapRombelPerJumlahPeer::DUAPULUHSATU_TIGAPULUHDUA, $duapuluhsatuTigapuluhdua['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($duapuluhsatuTigapuluhdua['max'])) {
                $this->addUsingAlias(RekapRombelPerJumlahPeer::DUAPULUHSATU_TIGAPULUHDUA, $duapuluhsatuTigapuluhdua['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapRombelPerJumlahPeer::DUAPULUHSATU_TIGAPULUHDUA, $duapuluhsatuTigapuluhdua, $comparison);
    }

    /**
     * Filter the query on the lebih_32 column
     *
     * Example usage:
     * <code>
     * $query->filterByLebih32(1234); // WHERE lebih_32 = 1234
     * $query->filterByLebih32(array(12, 34)); // WHERE lebih_32 IN (12, 34)
     * $query->filterByLebih32(array('min' => 12)); // WHERE lebih_32 >= 12
     * $query->filterByLebih32(array('max' => 12)); // WHERE lebih_32 <= 12
     * </code>
     *
     * @param     mixed $lebih32 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return RekapRombelPerJumlahQuery The current query, for fluid interface
     */
    public function filterByLebih32($lebih32 = null, $comparison = null)
    {
        if (is_array($lebih32)) {
            $useMinMax = false;
            if (isset($lebih32['min'])) {
                $this->addUsingAlias(RekapRombelPerJumlahPeer::LEBIH_32, $lebih32['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($lebih32['max'])) {
                $this->addUsingAlias(RekapRombelPerJumlahPeer::LEBIH_32, $lebih32['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(RekapRombelPerJumlahPeer::LEBIH_32, $lebih32, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   RekapRombelPerJumlah $rekapRombelPerJumlah Object to remove from the list of results
     *
     * @return RekapRombelPerJumlahQuery The current query, for fluid interface
     */
    public function prune($rekapRombelPerJumlah = null)
    {
        if ($rekapRombelPerJumlah) {
            $this->addUsingAlias(RekapRombelPerJumlahPeer::SEKOLAH_ID, $rekapRombelPerJumlah->getSekolahId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
