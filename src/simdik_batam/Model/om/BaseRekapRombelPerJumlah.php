<?php

namespace simdik_batam\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelException;
use \PropelPDO;
use simdik_batam\Model\RekapRombelPerJumlah;
use simdik_batam\Model\RekapRombelPerJumlahPeer;
use simdik_batam\Model\RekapRombelPerJumlahQuery;

/**
 * Base class that represents a row from the 'adm.rekap_rombel_per_jumlah' table.
 *
 * 
 *
 * @package    propel.generator.simdik_batam.Model.om
 */
abstract class BaseRekapRombelPerJumlah extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'simdik_batam\\Model\\RekapRombelPerJumlahPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        RekapRombelPerJumlahPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the sekolah_id field.
     * @var        string
     */
    protected $sekolah_id;

    /**
     * The value for the nama field.
     * @var        string
     */
    protected $nama;

    /**
     * The value for the total field.
     * @var        string
     */
    protected $total;

    /**
     * The value for the kurang_10 field.
     * @var        string
     */
    protected $kurang_10;

    /**
     * The value for the sepuluh_duapuluh field.
     * @var        string
     */
    protected $sepuluh_duapuluh;

    /**
     * The value for the duapuluhsatu_tigapuluhdua field.
     * @var        string
     */
    protected $duapuluhsatu_tigapuluhdua;

    /**
     * The value for the lebih_32 field.
     * @var        string
     */
    protected $lebih_32;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Get the [sekolah_id] column value.
     * 
     * @return string
     */
    public function getSekolahId()
    {
        return $this->sekolah_id;
    }

    /**
     * Get the [nama] column value.
     * 
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [total] column value.
     * 
     * @return string
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Get the [kurang_10] column value.
     * 
     * @return string
     */
    public function getKurang10()
    {
        return $this->kurang_10;
    }

    /**
     * Get the [sepuluh_duapuluh] column value.
     * 
     * @return string
     */
    public function getSepuluhDuapuluh()
    {
        return $this->sepuluh_duapuluh;
    }

    /**
     * Get the [duapuluhsatu_tigapuluhdua] column value.
     * 
     * @return string
     */
    public function getDuapuluhsatuTigapuluhdua()
    {
        return $this->duapuluhsatu_tigapuluhdua;
    }

    /**
     * Get the [lebih_32] column value.
     * 
     * @return string
     */
    public function getLebih32()
    {
        return $this->lebih_32;
    }

    /**
     * Set the value of [sekolah_id] column.
     * 
     * @param string $v new value
     * @return RekapRombelPerJumlah The current object (for fluent API support)
     */
    public function setSekolahId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sekolah_id !== $v) {
            $this->sekolah_id = $v;
            $this->modifiedColumns[] = RekapRombelPerJumlahPeer::SEKOLAH_ID;
        }


        return $this;
    } // setSekolahId()

    /**
     * Set the value of [nama] column.
     * 
     * @param string $v new value
     * @return RekapRombelPerJumlah The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[] = RekapRombelPerJumlahPeer::NAMA;
        }


        return $this;
    } // setNama()

    /**
     * Set the value of [total] column.
     * 
     * @param string $v new value
     * @return RekapRombelPerJumlah The current object (for fluent API support)
     */
    public function setTotal($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->total !== $v) {
            $this->total = $v;
            $this->modifiedColumns[] = RekapRombelPerJumlahPeer::TOTAL;
        }


        return $this;
    } // setTotal()

    /**
     * Set the value of [kurang_10] column.
     * 
     * @param string $v new value
     * @return RekapRombelPerJumlah The current object (for fluent API support)
     */
    public function setKurang10($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kurang_10 !== $v) {
            $this->kurang_10 = $v;
            $this->modifiedColumns[] = RekapRombelPerJumlahPeer::KURANG_10;
        }


        return $this;
    } // setKurang10()

    /**
     * Set the value of [sepuluh_duapuluh] column.
     * 
     * @param string $v new value
     * @return RekapRombelPerJumlah The current object (for fluent API support)
     */
    public function setSepuluhDuapuluh($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sepuluh_duapuluh !== $v) {
            $this->sepuluh_duapuluh = $v;
            $this->modifiedColumns[] = RekapRombelPerJumlahPeer::SEPULUH_DUAPULUH;
        }


        return $this;
    } // setSepuluhDuapuluh()

    /**
     * Set the value of [duapuluhsatu_tigapuluhdua] column.
     * 
     * @param string $v new value
     * @return RekapRombelPerJumlah The current object (for fluent API support)
     */
    public function setDuapuluhsatuTigapuluhdua($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->duapuluhsatu_tigapuluhdua !== $v) {
            $this->duapuluhsatu_tigapuluhdua = $v;
            $this->modifiedColumns[] = RekapRombelPerJumlahPeer::DUAPULUHSATU_TIGAPULUHDUA;
        }


        return $this;
    } // setDuapuluhsatuTigapuluhdua()

    /**
     * Set the value of [lebih_32] column.
     * 
     * @param string $v new value
     * @return RekapRombelPerJumlah The current object (for fluent API support)
     */
    public function setLebih32($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->lebih_32 !== $v) {
            $this->lebih_32 = $v;
            $this->modifiedColumns[] = RekapRombelPerJumlahPeer::LEBIH_32;
        }


        return $this;
    } // setLebih32()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->sekolah_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->nama = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->total = ($row[$startcol + 2] !== null) ? (string) $row[$startcol + 2] : null;
            $this->kurang_10 = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->sepuluh_duapuluh = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->duapuluhsatu_tigapuluhdua = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->lebih_32 = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 7; // 7 = RekapRombelPerJumlahPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating RekapRombelPerJumlah object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(RekapRombelPerJumlahPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = RekapRombelPerJumlahPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(RekapRombelPerJumlahPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = RekapRombelPerJumlahQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(RekapRombelPerJumlahPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                RekapRombelPerJumlahPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = RekapRombelPerJumlahPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = RekapRombelPerJumlahPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getSekolahId();
                break;
            case 1:
                return $this->getNama();
                break;
            case 2:
                return $this->getTotal();
                break;
            case 3:
                return $this->getKurang10();
                break;
            case 4:
                return $this->getSepuluhDuapuluh();
                break;
            case 5:
                return $this->getDuapuluhsatuTigapuluhdua();
                break;
            case 6:
                return $this->getLebih32();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {
        if (isset($alreadyDumpedObjects['RekapRombelPerJumlah'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['RekapRombelPerJumlah'][$this->getPrimaryKey()] = true;
        $keys = RekapRombelPerJumlahPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getSekolahId(),
            $keys[1] => $this->getNama(),
            $keys[2] => $this->getTotal(),
            $keys[3] => $this->getKurang10(),
            $keys[4] => $this->getSepuluhDuapuluh(),
            $keys[5] => $this->getDuapuluhsatuTigapuluhdua(),
            $keys[6] => $this->getLebih32(),
        );

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = RekapRombelPerJumlahPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setSekolahId($value);
                break;
            case 1:
                $this->setNama($value);
                break;
            case 2:
                $this->setTotal($value);
                break;
            case 3:
                $this->setKurang10($value);
                break;
            case 4:
                $this->setSepuluhDuapuluh($value);
                break;
            case 5:
                $this->setDuapuluhsatuTigapuluhdua($value);
                break;
            case 6:
                $this->setLebih32($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = RekapRombelPerJumlahPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setSekolahId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setNama($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setTotal($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setKurang10($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setSepuluhDuapuluh($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setDuapuluhsatuTigapuluhdua($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setLebih32($arr[$keys[6]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(RekapRombelPerJumlahPeer::DATABASE_NAME);

        if ($this->isColumnModified(RekapRombelPerJumlahPeer::SEKOLAH_ID)) $criteria->add(RekapRombelPerJumlahPeer::SEKOLAH_ID, $this->sekolah_id);
        if ($this->isColumnModified(RekapRombelPerJumlahPeer::NAMA)) $criteria->add(RekapRombelPerJumlahPeer::NAMA, $this->nama);
        if ($this->isColumnModified(RekapRombelPerJumlahPeer::TOTAL)) $criteria->add(RekapRombelPerJumlahPeer::TOTAL, $this->total);
        if ($this->isColumnModified(RekapRombelPerJumlahPeer::KURANG_10)) $criteria->add(RekapRombelPerJumlahPeer::KURANG_10, $this->kurang_10);
        if ($this->isColumnModified(RekapRombelPerJumlahPeer::SEPULUH_DUAPULUH)) $criteria->add(RekapRombelPerJumlahPeer::SEPULUH_DUAPULUH, $this->sepuluh_duapuluh);
        if ($this->isColumnModified(RekapRombelPerJumlahPeer::DUAPULUHSATU_TIGAPULUHDUA)) $criteria->add(RekapRombelPerJumlahPeer::DUAPULUHSATU_TIGAPULUHDUA, $this->duapuluhsatu_tigapuluhdua);
        if ($this->isColumnModified(RekapRombelPerJumlahPeer::LEBIH_32)) $criteria->add(RekapRombelPerJumlahPeer::LEBIH_32, $this->lebih_32);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(RekapRombelPerJumlahPeer::DATABASE_NAME);
        $criteria->add(RekapRombelPerJumlahPeer::SEKOLAH_ID, $this->sekolah_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getSekolahId();
    }

    /**
     * Generic method to set the primary key (sekolah_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setSekolahId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getSekolahId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of RekapRombelPerJumlah (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setNama($this->getNama());
        $copyObj->setTotal($this->getTotal());
        $copyObj->setKurang10($this->getKurang10());
        $copyObj->setSepuluhDuapuluh($this->getSepuluhDuapuluh());
        $copyObj->setDuapuluhsatuTigapuluhdua($this->getDuapuluhsatuTigapuluhdua());
        $copyObj->setLebih32($this->getLebih32());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setSekolahId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return RekapRombelPerJumlah Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return RekapRombelPerJumlahPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new RekapRombelPerJumlahPeer();
        }

        return self::$peer;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->sekolah_id = null;
        $this->nama = null;
        $this->total = null;
        $this->kurang_10 = null;
        $this->sepuluh_duapuluh = null;
        $this->duapuluhsatu_tigapuluhdua = null;
        $this->lebih_32 = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(RekapRombelPerJumlahPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
