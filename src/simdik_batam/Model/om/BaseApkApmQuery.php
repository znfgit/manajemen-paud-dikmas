<?php

namespace simdik_batam\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \PDO;
use \Propel;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use simdik_batam\Model\ApkApm;
use simdik_batam\Model\ApkApmPeer;
use simdik_batam\Model\ApkApmQuery;

/**
 * Base class that represents a query for the 'adm.apk_apm' table.
 *
 * 
 *
 * @method ApkApmQuery orderByApkApmId($order = Criteria::ASC) Order by the apk_apm_id column
 * @method ApkApmQuery orderByTanggal($order = Criteria::ASC) Order by the tanggal column
 * @method ApkApmQuery orderByPenggunaId($order = Criteria::ASC) Order by the pengguna_id column
 * @method ApkApmQuery orderByJumlahPenduduk612($order = Criteria::ASC) Order by the jumlah_penduduk_6_12 column
 * @method ApkApmQuery orderByJumlahPenduduk1315($order = Criteria::ASC) Order by the jumlah_penduduk_13_15 column
 * @method ApkApmQuery orderByJumlahPenduduk1619($order = Criteria::ASC) Order by the jumlah_penduduk_16_19 column
 *
 * @method ApkApmQuery groupByApkApmId() Group by the apk_apm_id column
 * @method ApkApmQuery groupByTanggal() Group by the tanggal column
 * @method ApkApmQuery groupByPenggunaId() Group by the pengguna_id column
 * @method ApkApmQuery groupByJumlahPenduduk612() Group by the jumlah_penduduk_6_12 column
 * @method ApkApmQuery groupByJumlahPenduduk1315() Group by the jumlah_penduduk_13_15 column
 * @method ApkApmQuery groupByJumlahPenduduk1619() Group by the jumlah_penduduk_16_19 column
 *
 * @method ApkApmQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method ApkApmQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method ApkApmQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method ApkApm findOne(PropelPDO $con = null) Return the first ApkApm matching the query
 * @method ApkApm findOneOrCreate(PropelPDO $con = null) Return the first ApkApm matching the query, or a new ApkApm object populated from the query conditions when no match is found
 *
 * @method ApkApm findOneByTanggal(string $tanggal) Return the first ApkApm filtered by the tanggal column
 * @method ApkApm findOneByPenggunaId(string $pengguna_id) Return the first ApkApm filtered by the pengguna_id column
 * @method ApkApm findOneByJumlahPenduduk612(int $jumlah_penduduk_6_12) Return the first ApkApm filtered by the jumlah_penduduk_6_12 column
 * @method ApkApm findOneByJumlahPenduduk1315(int $jumlah_penduduk_13_15) Return the first ApkApm filtered by the jumlah_penduduk_13_15 column
 * @method ApkApm findOneByJumlahPenduduk1619(int $jumlah_penduduk_16_19) Return the first ApkApm filtered by the jumlah_penduduk_16_19 column
 *
 * @method array findByApkApmId(string $apk_apm_id) Return ApkApm objects filtered by the apk_apm_id column
 * @method array findByTanggal(string $tanggal) Return ApkApm objects filtered by the tanggal column
 * @method array findByPenggunaId(string $pengguna_id) Return ApkApm objects filtered by the pengguna_id column
 * @method array findByJumlahPenduduk612(int $jumlah_penduduk_6_12) Return ApkApm objects filtered by the jumlah_penduduk_6_12 column
 * @method array findByJumlahPenduduk1315(int $jumlah_penduduk_13_15) Return ApkApm objects filtered by the jumlah_penduduk_13_15 column
 * @method array findByJumlahPenduduk1619(int $jumlah_penduduk_16_19) Return ApkApm objects filtered by the jumlah_penduduk_16_19 column
 *
 * @package    propel.generator.simdik_batam.Model.om
 */
abstract class BaseApkApmQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseApkApmQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'dapodikdasmen_batam', $modelName = 'simdik_batam\\Model\\ApkApm', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ApkApmQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   ApkApmQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return ApkApmQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof ApkApmQuery) {
            return $criteria;
        }
        $query = new ApkApmQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   ApkApm|ApkApm[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = ApkApmPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(ApkApmPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApkApm A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByApkApmId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 ApkApm A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [apk_apm_id], [tanggal], [pengguna_id], [jumlah_penduduk_6_12], [jumlah_penduduk_13_15], [jumlah_penduduk_16_19] FROM [adm].[apk_apm] WHERE [apk_apm_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new ApkApm();
            $obj->hydrate($row);
            ApkApmPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return ApkApm|ApkApm[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|ApkApm[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return ApkApmQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(ApkApmPeer::APK_APM_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return ApkApmQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(ApkApmPeer::APK_APM_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the apk_apm_id column
     *
     * Example usage:
     * <code>
     * $query->filterByApkApmId('fooValue');   // WHERE apk_apm_id = 'fooValue'
     * $query->filterByApkApmId('%fooValue%'); // WHERE apk_apm_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $apkApmId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApkApmQuery The current query, for fluid interface
     */
    public function filterByApkApmId($apkApmId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($apkApmId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $apkApmId)) {
                $apkApmId = str_replace('*', '%', $apkApmId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ApkApmPeer::APK_APM_ID, $apkApmId, $comparison);
    }

    /**
     * Filter the query on the tanggal column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggal('2011-03-14'); // WHERE tanggal = '2011-03-14'
     * $query->filterByTanggal('now'); // WHERE tanggal = '2011-03-14'
     * $query->filterByTanggal(array('max' => 'yesterday')); // WHERE tanggal > '2011-03-13'
     * </code>
     *
     * @param     mixed $tanggal The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApkApmQuery The current query, for fluid interface
     */
    public function filterByTanggal($tanggal = null, $comparison = null)
    {
        if (is_array($tanggal)) {
            $useMinMax = false;
            if (isset($tanggal['min'])) {
                $this->addUsingAlias(ApkApmPeer::TANGGAL, $tanggal['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($tanggal['max'])) {
                $this->addUsingAlias(ApkApmPeer::TANGGAL, $tanggal['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApkApmPeer::TANGGAL, $tanggal, $comparison);
    }

    /**
     * Filter the query on the pengguna_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPenggunaId('fooValue');   // WHERE pengguna_id = 'fooValue'
     * $query->filterByPenggunaId('%fooValue%'); // WHERE pengguna_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $penggunaId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApkApmQuery The current query, for fluid interface
     */
    public function filterByPenggunaId($penggunaId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($penggunaId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $penggunaId)) {
                $penggunaId = str_replace('*', '%', $penggunaId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(ApkApmPeer::PENGGUNA_ID, $penggunaId, $comparison);
    }

    /**
     * Filter the query on the jumlah_penduduk_6_12 column
     *
     * Example usage:
     * <code>
     * $query->filterByJumlahPenduduk612(1234); // WHERE jumlah_penduduk_6_12 = 1234
     * $query->filterByJumlahPenduduk612(array(12, 34)); // WHERE jumlah_penduduk_6_12 IN (12, 34)
     * $query->filterByJumlahPenduduk612(array('min' => 12)); // WHERE jumlah_penduduk_6_12 >= 12
     * $query->filterByJumlahPenduduk612(array('max' => 12)); // WHERE jumlah_penduduk_6_12 <= 12
     * </code>
     *
     * @param     mixed $jumlahPenduduk612 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApkApmQuery The current query, for fluid interface
     */
    public function filterByJumlahPenduduk612($jumlahPenduduk612 = null, $comparison = null)
    {
        if (is_array($jumlahPenduduk612)) {
            $useMinMax = false;
            if (isset($jumlahPenduduk612['min'])) {
                $this->addUsingAlias(ApkApmPeer::JUMLAH_PENDUDUK_6_12, $jumlahPenduduk612['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jumlahPenduduk612['max'])) {
                $this->addUsingAlias(ApkApmPeer::JUMLAH_PENDUDUK_6_12, $jumlahPenduduk612['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApkApmPeer::JUMLAH_PENDUDUK_6_12, $jumlahPenduduk612, $comparison);
    }

    /**
     * Filter the query on the jumlah_penduduk_13_15 column
     *
     * Example usage:
     * <code>
     * $query->filterByJumlahPenduduk1315(1234); // WHERE jumlah_penduduk_13_15 = 1234
     * $query->filterByJumlahPenduduk1315(array(12, 34)); // WHERE jumlah_penduduk_13_15 IN (12, 34)
     * $query->filterByJumlahPenduduk1315(array('min' => 12)); // WHERE jumlah_penduduk_13_15 >= 12
     * $query->filterByJumlahPenduduk1315(array('max' => 12)); // WHERE jumlah_penduduk_13_15 <= 12
     * </code>
     *
     * @param     mixed $jumlahPenduduk1315 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApkApmQuery The current query, for fluid interface
     */
    public function filterByJumlahPenduduk1315($jumlahPenduduk1315 = null, $comparison = null)
    {
        if (is_array($jumlahPenduduk1315)) {
            $useMinMax = false;
            if (isset($jumlahPenduduk1315['min'])) {
                $this->addUsingAlias(ApkApmPeer::JUMLAH_PENDUDUK_13_15, $jumlahPenduduk1315['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jumlahPenduduk1315['max'])) {
                $this->addUsingAlias(ApkApmPeer::JUMLAH_PENDUDUK_13_15, $jumlahPenduduk1315['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApkApmPeer::JUMLAH_PENDUDUK_13_15, $jumlahPenduduk1315, $comparison);
    }

    /**
     * Filter the query on the jumlah_penduduk_16_19 column
     *
     * Example usage:
     * <code>
     * $query->filterByJumlahPenduduk1619(1234); // WHERE jumlah_penduduk_16_19 = 1234
     * $query->filterByJumlahPenduduk1619(array(12, 34)); // WHERE jumlah_penduduk_16_19 IN (12, 34)
     * $query->filterByJumlahPenduduk1619(array('min' => 12)); // WHERE jumlah_penduduk_16_19 >= 12
     * $query->filterByJumlahPenduduk1619(array('max' => 12)); // WHERE jumlah_penduduk_16_19 <= 12
     * </code>
     *
     * @param     mixed $jumlahPenduduk1619 The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ApkApmQuery The current query, for fluid interface
     */
    public function filterByJumlahPenduduk1619($jumlahPenduduk1619 = null, $comparison = null)
    {
        if (is_array($jumlahPenduduk1619)) {
            $useMinMax = false;
            if (isset($jumlahPenduduk1619['min'])) {
                $this->addUsingAlias(ApkApmPeer::JUMLAH_PENDUDUK_16_19, $jumlahPenduduk1619['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jumlahPenduduk1619['max'])) {
                $this->addUsingAlias(ApkApmPeer::JUMLAH_PENDUDUK_16_19, $jumlahPenduduk1619['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(ApkApmPeer::JUMLAH_PENDUDUK_16_19, $jumlahPenduduk1619, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   ApkApm $apkApm Object to remove from the list of results
     *
     * @return ApkApmQuery The current query, for fluid interface
     */
    public function prune($apkApm = null)
    {
        if ($apkApm) {
            $this->addUsingAlias(ApkApmPeer::APK_APM_ID, $apkApm->getApkApmId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
