<?php

namespace simdik_batam\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \PDO;
use \Propel;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use simdik_batam\Model\Mutasi;
use simdik_batam\Model\MutasiPeer;
use simdik_batam\Model\MutasiQuery;

/**
 * Base class that represents a query for the 'adm.mutasi' table.
 *
 * 
 *
 * @method MutasiQuery orderByMutasiId($order = Criteria::ASC) Order by the mutasi_id column
 * @method MutasiQuery orderByPesertaDidikId($order = Criteria::ASC) Order by the peserta_didik_id column
 * @method MutasiQuery orderBySoftDelete($order = Criteria::ASC) Order by the soft_delete column
 * @method MutasiQuery orderByNomorSurat($order = Criteria::ASC) Order by the nomor_surat column
 * @method MutasiQuery orderBySekolahId($order = Criteria::ASC) Order by the sekolah_id column
 * @method MutasiQuery orderByKodeWilayahBerangkat($order = Criteria::ASC) Order by the kode_wilayah_berangkat column
 * @method MutasiQuery orderByMaksudTujuan($order = Criteria::ASC) Order by the maksud_tujuan column
 * @method MutasiQuery orderByNamaYangDiikuti($order = Criteria::ASC) Order by the nama_yang_diikuti column
 * @method MutasiQuery orderByHubunganKeluarga($order = Criteria::ASC) Order by the hubungan_keluarga column
 * @method MutasiQuery orderByPekerjaan($order = Criteria::ASC) Order by the pekerjaan column
 * @method MutasiQuery orderByAlamatYangDiikuti($order = Criteria::ASC) Order by the alamat_yang_diikuti column
 * @method MutasiQuery orderByTanggalBerangkat($order = Criteria::ASC) Order by the tanggal_berangkat column
 * @method MutasiQuery orderByPenggunaId($order = Criteria::ASC) Order by the pengguna_id column
 * @method MutasiQuery orderBySekolahIdTujuan($order = Criteria::ASC) Order by the sekolah_id_tujuan column
 * @method MutasiQuery orderByNama($order = Criteria::ASC) Order by the nama column
 * @method MutasiQuery orderByTempatLahir($order = Criteria::ASC) Order by the tempat_lahir column
 * @method MutasiQuery orderByTanggalLahir($order = Criteria::ASC) Order by the tanggal_lahir column
 * @method MutasiQuery orderByKelas($order = Criteria::ASC) Order by the kelas column
 * @method MutasiQuery orderByNis($order = Criteria::ASC) Order by the NIS column
 * @method MutasiQuery orderByNisn($order = Criteria::ASC) Order by the NISN column
 * @method MutasiQuery orderByNamaAyah($order = Criteria::ASC) Order by the nama_ayah column
 * @method MutasiQuery orderByPekerjaanIdAyahStr($order = Criteria::ASC) Order by the pekerjaan_id_ayah_str column
 * @method MutasiQuery orderByAlamatJalan($order = Criteria::ASC) Order by the alamat_jalan column
 * @method MutasiQuery orderByJenisSuratId($order = Criteria::ASC) Order by the jenis_surat_id column
 * @method MutasiQuery orderByTanggalSurat($order = Criteria::ASC) Order by the tanggal_surat column
 * @method MutasiQuery orderByKeteranganKepalaSekolah($order = Criteria::ASC) Order by the keterangan_kepala_sekolah column
 * @method MutasiQuery orderByKeteranganLain($order = Criteria::ASC) Order by the keterangan_lain column
 *
 * @method MutasiQuery groupByMutasiId() Group by the mutasi_id column
 * @method MutasiQuery groupByPesertaDidikId() Group by the peserta_didik_id column
 * @method MutasiQuery groupBySoftDelete() Group by the soft_delete column
 * @method MutasiQuery groupByNomorSurat() Group by the nomor_surat column
 * @method MutasiQuery groupBySekolahId() Group by the sekolah_id column
 * @method MutasiQuery groupByKodeWilayahBerangkat() Group by the kode_wilayah_berangkat column
 * @method MutasiQuery groupByMaksudTujuan() Group by the maksud_tujuan column
 * @method MutasiQuery groupByNamaYangDiikuti() Group by the nama_yang_diikuti column
 * @method MutasiQuery groupByHubunganKeluarga() Group by the hubungan_keluarga column
 * @method MutasiQuery groupByPekerjaan() Group by the pekerjaan column
 * @method MutasiQuery groupByAlamatYangDiikuti() Group by the alamat_yang_diikuti column
 * @method MutasiQuery groupByTanggalBerangkat() Group by the tanggal_berangkat column
 * @method MutasiQuery groupByPenggunaId() Group by the pengguna_id column
 * @method MutasiQuery groupBySekolahIdTujuan() Group by the sekolah_id_tujuan column
 * @method MutasiQuery groupByNama() Group by the nama column
 * @method MutasiQuery groupByTempatLahir() Group by the tempat_lahir column
 * @method MutasiQuery groupByTanggalLahir() Group by the tanggal_lahir column
 * @method MutasiQuery groupByKelas() Group by the kelas column
 * @method MutasiQuery groupByNis() Group by the NIS column
 * @method MutasiQuery groupByNisn() Group by the NISN column
 * @method MutasiQuery groupByNamaAyah() Group by the nama_ayah column
 * @method MutasiQuery groupByPekerjaanIdAyahStr() Group by the pekerjaan_id_ayah_str column
 * @method MutasiQuery groupByAlamatJalan() Group by the alamat_jalan column
 * @method MutasiQuery groupByJenisSuratId() Group by the jenis_surat_id column
 * @method MutasiQuery groupByTanggalSurat() Group by the tanggal_surat column
 * @method MutasiQuery groupByKeteranganKepalaSekolah() Group by the keterangan_kepala_sekolah column
 * @method MutasiQuery groupByKeteranganLain() Group by the keterangan_lain column
 *
 * @method MutasiQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method MutasiQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method MutasiQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method Mutasi findOne(PropelPDO $con = null) Return the first Mutasi matching the query
 * @method Mutasi findOneOrCreate(PropelPDO $con = null) Return the first Mutasi matching the query, or a new Mutasi object populated from the query conditions when no match is found
 *
 * @method Mutasi findOneByPesertaDidikId(string $peserta_didik_id) Return the first Mutasi filtered by the peserta_didik_id column
 * @method Mutasi findOneBySoftDelete(int $soft_delete) Return the first Mutasi filtered by the soft_delete column
 * @method Mutasi findOneByNomorSurat(string $nomor_surat) Return the first Mutasi filtered by the nomor_surat column
 * @method Mutasi findOneBySekolahId(string $sekolah_id) Return the first Mutasi filtered by the sekolah_id column
 * @method Mutasi findOneByKodeWilayahBerangkat(string $kode_wilayah_berangkat) Return the first Mutasi filtered by the kode_wilayah_berangkat column
 * @method Mutasi findOneByMaksudTujuan(string $maksud_tujuan) Return the first Mutasi filtered by the maksud_tujuan column
 * @method Mutasi findOneByNamaYangDiikuti(string $nama_yang_diikuti) Return the first Mutasi filtered by the nama_yang_diikuti column
 * @method Mutasi findOneByHubunganKeluarga(string $hubungan_keluarga) Return the first Mutasi filtered by the hubungan_keluarga column
 * @method Mutasi findOneByPekerjaan(string $pekerjaan) Return the first Mutasi filtered by the pekerjaan column
 * @method Mutasi findOneByAlamatYangDiikuti(string $alamat_yang_diikuti) Return the first Mutasi filtered by the alamat_yang_diikuti column
 * @method Mutasi findOneByTanggalBerangkat(string $tanggal_berangkat) Return the first Mutasi filtered by the tanggal_berangkat column
 * @method Mutasi findOneByPenggunaId(string $pengguna_id) Return the first Mutasi filtered by the pengguna_id column
 * @method Mutasi findOneBySekolahIdTujuan(string $sekolah_id_tujuan) Return the first Mutasi filtered by the sekolah_id_tujuan column
 * @method Mutasi findOneByNama(string $nama) Return the first Mutasi filtered by the nama column
 * @method Mutasi findOneByTempatLahir(string $tempat_lahir) Return the first Mutasi filtered by the tempat_lahir column
 * @method Mutasi findOneByTanggalLahir(string $tanggal_lahir) Return the first Mutasi filtered by the tanggal_lahir column
 * @method Mutasi findOneByKelas(string $kelas) Return the first Mutasi filtered by the kelas column
 * @method Mutasi findOneByNis(string $NIS) Return the first Mutasi filtered by the NIS column
 * @method Mutasi findOneByNisn(string $NISN) Return the first Mutasi filtered by the NISN column
 * @method Mutasi findOneByNamaAyah(string $nama_ayah) Return the first Mutasi filtered by the nama_ayah column
 * @method Mutasi findOneByPekerjaanIdAyahStr(string $pekerjaan_id_ayah_str) Return the first Mutasi filtered by the pekerjaan_id_ayah_str column
 * @method Mutasi findOneByAlamatJalan(string $alamat_jalan) Return the first Mutasi filtered by the alamat_jalan column
 * @method Mutasi findOneByJenisSuratId(int $jenis_surat_id) Return the first Mutasi filtered by the jenis_surat_id column
 * @method Mutasi findOneByTanggalSurat(string $tanggal_surat) Return the first Mutasi filtered by the tanggal_surat column
 * @method Mutasi findOneByKeteranganKepalaSekolah(string $keterangan_kepala_sekolah) Return the first Mutasi filtered by the keterangan_kepala_sekolah column
 * @method Mutasi findOneByKeteranganLain(string $keterangan_lain) Return the first Mutasi filtered by the keterangan_lain column
 *
 * @method array findByMutasiId(string $mutasi_id) Return Mutasi objects filtered by the mutasi_id column
 * @method array findByPesertaDidikId(string $peserta_didik_id) Return Mutasi objects filtered by the peserta_didik_id column
 * @method array findBySoftDelete(int $soft_delete) Return Mutasi objects filtered by the soft_delete column
 * @method array findByNomorSurat(string $nomor_surat) Return Mutasi objects filtered by the nomor_surat column
 * @method array findBySekolahId(string $sekolah_id) Return Mutasi objects filtered by the sekolah_id column
 * @method array findByKodeWilayahBerangkat(string $kode_wilayah_berangkat) Return Mutasi objects filtered by the kode_wilayah_berangkat column
 * @method array findByMaksudTujuan(string $maksud_tujuan) Return Mutasi objects filtered by the maksud_tujuan column
 * @method array findByNamaYangDiikuti(string $nama_yang_diikuti) Return Mutasi objects filtered by the nama_yang_diikuti column
 * @method array findByHubunganKeluarga(string $hubungan_keluarga) Return Mutasi objects filtered by the hubungan_keluarga column
 * @method array findByPekerjaan(string $pekerjaan) Return Mutasi objects filtered by the pekerjaan column
 * @method array findByAlamatYangDiikuti(string $alamat_yang_diikuti) Return Mutasi objects filtered by the alamat_yang_diikuti column
 * @method array findByTanggalBerangkat(string $tanggal_berangkat) Return Mutasi objects filtered by the tanggal_berangkat column
 * @method array findByPenggunaId(string $pengguna_id) Return Mutasi objects filtered by the pengguna_id column
 * @method array findBySekolahIdTujuan(string $sekolah_id_tujuan) Return Mutasi objects filtered by the sekolah_id_tujuan column
 * @method array findByNama(string $nama) Return Mutasi objects filtered by the nama column
 * @method array findByTempatLahir(string $tempat_lahir) Return Mutasi objects filtered by the tempat_lahir column
 * @method array findByTanggalLahir(string $tanggal_lahir) Return Mutasi objects filtered by the tanggal_lahir column
 * @method array findByKelas(string $kelas) Return Mutasi objects filtered by the kelas column
 * @method array findByNis(string $NIS) Return Mutasi objects filtered by the NIS column
 * @method array findByNisn(string $NISN) Return Mutasi objects filtered by the NISN column
 * @method array findByNamaAyah(string $nama_ayah) Return Mutasi objects filtered by the nama_ayah column
 * @method array findByPekerjaanIdAyahStr(string $pekerjaan_id_ayah_str) Return Mutasi objects filtered by the pekerjaan_id_ayah_str column
 * @method array findByAlamatJalan(string $alamat_jalan) Return Mutasi objects filtered by the alamat_jalan column
 * @method array findByJenisSuratId(int $jenis_surat_id) Return Mutasi objects filtered by the jenis_surat_id column
 * @method array findByTanggalSurat(string $tanggal_surat) Return Mutasi objects filtered by the tanggal_surat column
 * @method array findByKeteranganKepalaSekolah(string $keterangan_kepala_sekolah) Return Mutasi objects filtered by the keterangan_kepala_sekolah column
 * @method array findByKeteranganLain(string $keterangan_lain) Return Mutasi objects filtered by the keterangan_lain column
 *
 * @package    propel.generator.simdik_batam.Model.om
 */
abstract class BaseMutasiQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseMutasiQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'dapodikdasmen_batam', $modelName = 'simdik_batam\\Model\\Mutasi', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new MutasiQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   MutasiQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return MutasiQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof MutasiQuery) {
            return $criteria;
        }
        $query = new MutasiQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   Mutasi|Mutasi[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = MutasiPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(MutasiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Mutasi A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByMutasiId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 Mutasi A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [mutasi_id], [peserta_didik_id], [soft_delete], [nomor_surat], [sekolah_id], [kode_wilayah_berangkat], [maksud_tujuan], [nama_yang_diikuti], [hubungan_keluarga], [pekerjaan], [alamat_yang_diikuti], [tanggal_berangkat], [pengguna_id], [sekolah_id_tujuan], [nama], [tempat_lahir], [tanggal_lahir], [kelas], [NIS], [NISN], [nama_ayah], [pekerjaan_id_ayah_str], [alamat_jalan], [jenis_surat_id], [tanggal_surat], [keterangan_kepala_sekolah], [keterangan_lain] FROM [adm].[mutasi] WHERE [mutasi_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new Mutasi();
            $obj->hydrate($row);
            MutasiPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return Mutasi|Mutasi[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|Mutasi[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(MutasiPeer::MUTASI_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(MutasiPeer::MUTASI_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the mutasi_id column
     *
     * Example usage:
     * <code>
     * $query->filterByMutasiId('fooValue');   // WHERE mutasi_id = 'fooValue'
     * $query->filterByMutasiId('%fooValue%'); // WHERE mutasi_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $mutasiId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByMutasiId($mutasiId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($mutasiId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $mutasiId)) {
                $mutasiId = str_replace('*', '%', $mutasiId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MutasiPeer::MUTASI_ID, $mutasiId, $comparison);
    }

    /**
     * Filter the query on the peserta_didik_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPesertaDidikId('fooValue');   // WHERE peserta_didik_id = 'fooValue'
     * $query->filterByPesertaDidikId('%fooValue%'); // WHERE peserta_didik_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pesertaDidikId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByPesertaDidikId($pesertaDidikId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pesertaDidikId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $pesertaDidikId)) {
                $pesertaDidikId = str_replace('*', '%', $pesertaDidikId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MutasiPeer::PESERTA_DIDIK_ID, $pesertaDidikId, $comparison);
    }

    /**
     * Filter the query on the soft_delete column
     *
     * Example usage:
     * <code>
     * $query->filterBySoftDelete(1234); // WHERE soft_delete = 1234
     * $query->filterBySoftDelete(array(12, 34)); // WHERE soft_delete IN (12, 34)
     * $query->filterBySoftDelete(array('min' => 12)); // WHERE soft_delete >= 12
     * $query->filterBySoftDelete(array('max' => 12)); // WHERE soft_delete <= 12
     * </code>
     *
     * @param     mixed $softDelete The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterBySoftDelete($softDelete = null, $comparison = null)
    {
        if (is_array($softDelete)) {
            $useMinMax = false;
            if (isset($softDelete['min'])) {
                $this->addUsingAlias(MutasiPeer::SOFT_DELETE, $softDelete['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($softDelete['max'])) {
                $this->addUsingAlias(MutasiPeer::SOFT_DELETE, $softDelete['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MutasiPeer::SOFT_DELETE, $softDelete, $comparison);
    }

    /**
     * Filter the query on the nomor_surat column
     *
     * Example usage:
     * <code>
     * $query->filterByNomorSurat('fooValue');   // WHERE nomor_surat = 'fooValue'
     * $query->filterByNomorSurat('%fooValue%'); // WHERE nomor_surat LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nomorSurat The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByNomorSurat($nomorSurat = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nomorSurat)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nomorSurat)) {
                $nomorSurat = str_replace('*', '%', $nomorSurat);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MutasiPeer::NOMOR_SURAT, $nomorSurat, $comparison);
    }

    /**
     * Filter the query on the sekolah_id column
     *
     * Example usage:
     * <code>
     * $query->filterBySekolahId('fooValue');   // WHERE sekolah_id = 'fooValue'
     * $query->filterBySekolahId('%fooValue%'); // WHERE sekolah_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sekolahId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterBySekolahId($sekolahId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sekolahId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sekolahId)) {
                $sekolahId = str_replace('*', '%', $sekolahId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MutasiPeer::SEKOLAH_ID, $sekolahId, $comparison);
    }

    /**
     * Filter the query on the kode_wilayah_berangkat column
     *
     * Example usage:
     * <code>
     * $query->filterByKodeWilayahBerangkat('fooValue');   // WHERE kode_wilayah_berangkat = 'fooValue'
     * $query->filterByKodeWilayahBerangkat('%fooValue%'); // WHERE kode_wilayah_berangkat LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodeWilayahBerangkat The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByKodeWilayahBerangkat($kodeWilayahBerangkat = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodeWilayahBerangkat)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kodeWilayahBerangkat)) {
                $kodeWilayahBerangkat = str_replace('*', '%', $kodeWilayahBerangkat);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MutasiPeer::KODE_WILAYAH_BERANGKAT, $kodeWilayahBerangkat, $comparison);
    }

    /**
     * Filter the query on the maksud_tujuan column
     *
     * Example usage:
     * <code>
     * $query->filterByMaksudTujuan('fooValue');   // WHERE maksud_tujuan = 'fooValue'
     * $query->filterByMaksudTujuan('%fooValue%'); // WHERE maksud_tujuan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $maksudTujuan The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByMaksudTujuan($maksudTujuan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($maksudTujuan)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $maksudTujuan)) {
                $maksudTujuan = str_replace('*', '%', $maksudTujuan);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MutasiPeer::MAKSUD_TUJUAN, $maksudTujuan, $comparison);
    }

    /**
     * Filter the query on the nama_yang_diikuti column
     *
     * Example usage:
     * <code>
     * $query->filterByNamaYangDiikuti('fooValue');   // WHERE nama_yang_diikuti = 'fooValue'
     * $query->filterByNamaYangDiikuti('%fooValue%'); // WHERE nama_yang_diikuti LIKE '%fooValue%'
     * </code>
     *
     * @param     string $namaYangDiikuti The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByNamaYangDiikuti($namaYangDiikuti = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($namaYangDiikuti)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $namaYangDiikuti)) {
                $namaYangDiikuti = str_replace('*', '%', $namaYangDiikuti);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MutasiPeer::NAMA_YANG_DIIKUTI, $namaYangDiikuti, $comparison);
    }

    /**
     * Filter the query on the hubungan_keluarga column
     *
     * Example usage:
     * <code>
     * $query->filterByHubunganKeluarga('fooValue');   // WHERE hubungan_keluarga = 'fooValue'
     * $query->filterByHubunganKeluarga('%fooValue%'); // WHERE hubungan_keluarga LIKE '%fooValue%'
     * </code>
     *
     * @param     string $hubunganKeluarga The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByHubunganKeluarga($hubunganKeluarga = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($hubunganKeluarga)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $hubunganKeluarga)) {
                $hubunganKeluarga = str_replace('*', '%', $hubunganKeluarga);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MutasiPeer::HUBUNGAN_KELUARGA, $hubunganKeluarga, $comparison);
    }

    /**
     * Filter the query on the pekerjaan column
     *
     * Example usage:
     * <code>
     * $query->filterByPekerjaan('fooValue');   // WHERE pekerjaan = 'fooValue'
     * $query->filterByPekerjaan('%fooValue%'); // WHERE pekerjaan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pekerjaan The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByPekerjaan($pekerjaan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pekerjaan)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $pekerjaan)) {
                $pekerjaan = str_replace('*', '%', $pekerjaan);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MutasiPeer::PEKERJAAN, $pekerjaan, $comparison);
    }

    /**
     * Filter the query on the alamat_yang_diikuti column
     *
     * Example usage:
     * <code>
     * $query->filterByAlamatYangDiikuti('fooValue');   // WHERE alamat_yang_diikuti = 'fooValue'
     * $query->filterByAlamatYangDiikuti('%fooValue%'); // WHERE alamat_yang_diikuti LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alamatYangDiikuti The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByAlamatYangDiikuti($alamatYangDiikuti = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alamatYangDiikuti)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $alamatYangDiikuti)) {
                $alamatYangDiikuti = str_replace('*', '%', $alamatYangDiikuti);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MutasiPeer::ALAMAT_YANG_DIIKUTI, $alamatYangDiikuti, $comparison);
    }

    /**
     * Filter the query on the tanggal_berangkat column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggalBerangkat('fooValue');   // WHERE tanggal_berangkat = 'fooValue'
     * $query->filterByTanggalBerangkat('%fooValue%'); // WHERE tanggal_berangkat LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tanggalBerangkat The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByTanggalBerangkat($tanggalBerangkat = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tanggalBerangkat)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tanggalBerangkat)) {
                $tanggalBerangkat = str_replace('*', '%', $tanggalBerangkat);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MutasiPeer::TANGGAL_BERANGKAT, $tanggalBerangkat, $comparison);
    }

    /**
     * Filter the query on the pengguna_id column
     *
     * Example usage:
     * <code>
     * $query->filterByPenggunaId('fooValue');   // WHERE pengguna_id = 'fooValue'
     * $query->filterByPenggunaId('%fooValue%'); // WHERE pengguna_id LIKE '%fooValue%'
     * </code>
     *
     * @param     string $penggunaId The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByPenggunaId($penggunaId = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($penggunaId)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $penggunaId)) {
                $penggunaId = str_replace('*', '%', $penggunaId);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MutasiPeer::PENGGUNA_ID, $penggunaId, $comparison);
    }

    /**
     * Filter the query on the sekolah_id_tujuan column
     *
     * Example usage:
     * <code>
     * $query->filterBySekolahIdTujuan('fooValue');   // WHERE sekolah_id_tujuan = 'fooValue'
     * $query->filterBySekolahIdTujuan('%fooValue%'); // WHERE sekolah_id_tujuan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $sekolahIdTujuan The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterBySekolahIdTujuan($sekolahIdTujuan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($sekolahIdTujuan)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $sekolahIdTujuan)) {
                $sekolahIdTujuan = str_replace('*', '%', $sekolahIdTujuan);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MutasiPeer::SEKOLAH_ID_TUJUAN, $sekolahIdTujuan, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MutasiPeer::NAMA, $nama, $comparison);
    }

    /**
     * Filter the query on the tempat_lahir column
     *
     * Example usage:
     * <code>
     * $query->filterByTempatLahir('fooValue');   // WHERE tempat_lahir = 'fooValue'
     * $query->filterByTempatLahir('%fooValue%'); // WHERE tempat_lahir LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tempatLahir The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByTempatLahir($tempatLahir = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tempatLahir)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tempatLahir)) {
                $tempatLahir = str_replace('*', '%', $tempatLahir);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MutasiPeer::TEMPAT_LAHIR, $tempatLahir, $comparison);
    }

    /**
     * Filter the query on the tanggal_lahir column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggalLahir('fooValue');   // WHERE tanggal_lahir = 'fooValue'
     * $query->filterByTanggalLahir('%fooValue%'); // WHERE tanggal_lahir LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tanggalLahir The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByTanggalLahir($tanggalLahir = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tanggalLahir)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tanggalLahir)) {
                $tanggalLahir = str_replace('*', '%', $tanggalLahir);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MutasiPeer::TANGGAL_LAHIR, $tanggalLahir, $comparison);
    }

    /**
     * Filter the query on the kelas column
     *
     * Example usage:
     * <code>
     * $query->filterByKelas('fooValue');   // WHERE kelas = 'fooValue'
     * $query->filterByKelas('%fooValue%'); // WHERE kelas LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kelas The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByKelas($kelas = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kelas)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kelas)) {
                $kelas = str_replace('*', '%', $kelas);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MutasiPeer::KELAS, $kelas, $comparison);
    }

    /**
     * Filter the query on the NIS column
     *
     * Example usage:
     * <code>
     * $query->filterByNis('fooValue');   // WHERE NIS = 'fooValue'
     * $query->filterByNis('%fooValue%'); // WHERE NIS LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nis The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByNis($nis = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nis)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nis)) {
                $nis = str_replace('*', '%', $nis);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MutasiPeer::NIS, $nis, $comparison);
    }

    /**
     * Filter the query on the NISN column
     *
     * Example usage:
     * <code>
     * $query->filterByNisn('fooValue');   // WHERE NISN = 'fooValue'
     * $query->filterByNisn('%fooValue%'); // WHERE NISN LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nisn The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByNisn($nisn = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nisn)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nisn)) {
                $nisn = str_replace('*', '%', $nisn);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MutasiPeer::NISN, $nisn, $comparison);
    }

    /**
     * Filter the query on the nama_ayah column
     *
     * Example usage:
     * <code>
     * $query->filterByNamaAyah('fooValue');   // WHERE nama_ayah = 'fooValue'
     * $query->filterByNamaAyah('%fooValue%'); // WHERE nama_ayah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $namaAyah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByNamaAyah($namaAyah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($namaAyah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $namaAyah)) {
                $namaAyah = str_replace('*', '%', $namaAyah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MutasiPeer::NAMA_AYAH, $namaAyah, $comparison);
    }

    /**
     * Filter the query on the pekerjaan_id_ayah_str column
     *
     * Example usage:
     * <code>
     * $query->filterByPekerjaanIdAyahStr('fooValue');   // WHERE pekerjaan_id_ayah_str = 'fooValue'
     * $query->filterByPekerjaanIdAyahStr('%fooValue%'); // WHERE pekerjaan_id_ayah_str LIKE '%fooValue%'
     * </code>
     *
     * @param     string $pekerjaanIdAyahStr The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByPekerjaanIdAyahStr($pekerjaanIdAyahStr = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($pekerjaanIdAyahStr)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $pekerjaanIdAyahStr)) {
                $pekerjaanIdAyahStr = str_replace('*', '%', $pekerjaanIdAyahStr);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MutasiPeer::PEKERJAAN_ID_AYAH_STR, $pekerjaanIdAyahStr, $comparison);
    }

    /**
     * Filter the query on the alamat_jalan column
     *
     * Example usage:
     * <code>
     * $query->filterByAlamatJalan('fooValue');   // WHERE alamat_jalan = 'fooValue'
     * $query->filterByAlamatJalan('%fooValue%'); // WHERE alamat_jalan LIKE '%fooValue%'
     * </code>
     *
     * @param     string $alamatJalan The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByAlamatJalan($alamatJalan = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($alamatJalan)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $alamatJalan)) {
                $alamatJalan = str_replace('*', '%', $alamatJalan);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MutasiPeer::ALAMAT_JALAN, $alamatJalan, $comparison);
    }

    /**
     * Filter the query on the jenis_surat_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJenisSuratId(1234); // WHERE jenis_surat_id = 1234
     * $query->filterByJenisSuratId(array(12, 34)); // WHERE jenis_surat_id IN (12, 34)
     * $query->filterByJenisSuratId(array('min' => 12)); // WHERE jenis_surat_id >= 12
     * $query->filterByJenisSuratId(array('max' => 12)); // WHERE jenis_surat_id <= 12
     * </code>
     *
     * @param     mixed $jenisSuratId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByJenisSuratId($jenisSuratId = null, $comparison = null)
    {
        if (is_array($jenisSuratId)) {
            $useMinMax = false;
            if (isset($jenisSuratId['min'])) {
                $this->addUsingAlias(MutasiPeer::JENIS_SURAT_ID, $jenisSuratId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jenisSuratId['max'])) {
                $this->addUsingAlias(MutasiPeer::JENIS_SURAT_ID, $jenisSuratId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(MutasiPeer::JENIS_SURAT_ID, $jenisSuratId, $comparison);
    }

    /**
     * Filter the query on the tanggal_surat column
     *
     * Example usage:
     * <code>
     * $query->filterByTanggalSurat('fooValue');   // WHERE tanggal_surat = 'fooValue'
     * $query->filterByTanggalSurat('%fooValue%'); // WHERE tanggal_surat LIKE '%fooValue%'
     * </code>
     *
     * @param     string $tanggalSurat The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByTanggalSurat($tanggalSurat = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($tanggalSurat)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $tanggalSurat)) {
                $tanggalSurat = str_replace('*', '%', $tanggalSurat);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MutasiPeer::TANGGAL_SURAT, $tanggalSurat, $comparison);
    }

    /**
     * Filter the query on the keterangan_kepala_sekolah column
     *
     * Example usage:
     * <code>
     * $query->filterByKeteranganKepalaSekolah('fooValue');   // WHERE keterangan_kepala_sekolah = 'fooValue'
     * $query->filterByKeteranganKepalaSekolah('%fooValue%'); // WHERE keterangan_kepala_sekolah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $keteranganKepalaSekolah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByKeteranganKepalaSekolah($keteranganKepalaSekolah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($keteranganKepalaSekolah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $keteranganKepalaSekolah)) {
                $keteranganKepalaSekolah = str_replace('*', '%', $keteranganKepalaSekolah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MutasiPeer::KETERANGAN_KEPALA_SEKOLAH, $keteranganKepalaSekolah, $comparison);
    }

    /**
     * Filter the query on the keterangan_lain column
     *
     * Example usage:
     * <code>
     * $query->filterByKeteranganLain('fooValue');   // WHERE keterangan_lain = 'fooValue'
     * $query->filterByKeteranganLain('%fooValue%'); // WHERE keterangan_lain LIKE '%fooValue%'
     * </code>
     *
     * @param     string $keteranganLain The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function filterByKeteranganLain($keteranganLain = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($keteranganLain)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $keteranganLain)) {
                $keteranganLain = str_replace('*', '%', $keteranganLain);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(MutasiPeer::KETERANGAN_LAIN, $keteranganLain, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   Mutasi $mutasi Object to remove from the list of results
     *
     * @return MutasiQuery The current query, for fluid interface
     */
    public function prune($mutasi = null)
    {
        if ($mutasi) {
            $this->addUsingAlias(MutasiPeer::MUTASI_ID, $mutasi->getMutasiId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
