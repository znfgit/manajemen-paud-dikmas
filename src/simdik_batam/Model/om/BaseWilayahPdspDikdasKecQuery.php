<?php

namespace simdik_batam\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \PDO;
use \Propel;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use simdik_batam\Model\WilayahPdspDikdasKec;
use simdik_batam\Model\WilayahPdspDikdasKecPeer;
use simdik_batam\Model\WilayahPdspDikdasKecQuery;

/**
 * Base class that represents a query for the 'ref.wilayah_pdsp_dikdas_kec' table.
 *
 * 
 *
 * @method WilayahPdspDikdasKecQuery orderByKecId($order = Criteria::ASC) Order by the kec_id column
 * @method WilayahPdspDikdasKecQuery orderByKodeWilayah($order = Criteria::ASC) Order by the kode_wilayah column
 * @method WilayahPdspDikdasKecQuery orderByKec($order = Criteria::ASC) Order by the kec_ column
 * @method WilayahPdspDikdasKecQuery orderByKab($order = Criteria::ASC) Order by the kab_ column
 * @method WilayahPdspDikdasKecQuery orderByProp($order = Criteria::ASC) Order by the prop_ column
 *
 * @method WilayahPdspDikdasKecQuery groupByKecId() Group by the kec_id column
 * @method WilayahPdspDikdasKecQuery groupByKodeWilayah() Group by the kode_wilayah column
 * @method WilayahPdspDikdasKecQuery groupByKec() Group by the kec_ column
 * @method WilayahPdspDikdasKecQuery groupByKab() Group by the kab_ column
 * @method WilayahPdspDikdasKecQuery groupByProp() Group by the prop_ column
 *
 * @method WilayahPdspDikdasKecQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method WilayahPdspDikdasKecQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method WilayahPdspDikdasKecQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method WilayahPdspDikdasKec findOne(PropelPDO $con = null) Return the first WilayahPdspDikdasKec matching the query
 * @method WilayahPdspDikdasKec findOneOrCreate(PropelPDO $con = null) Return the first WilayahPdspDikdasKec matching the query, or a new WilayahPdspDikdasKec object populated from the query conditions when no match is found
 *
 * @method WilayahPdspDikdasKec findOneByKodeWilayah(string $kode_wilayah) Return the first WilayahPdspDikdasKec filtered by the kode_wilayah column
 * @method WilayahPdspDikdasKec findOneByKec(string $kec_) Return the first WilayahPdspDikdasKec filtered by the kec_ column
 * @method WilayahPdspDikdasKec findOneByKab(string $kab_) Return the first WilayahPdspDikdasKec filtered by the kab_ column
 * @method WilayahPdspDikdasKec findOneByProp(string $prop_) Return the first WilayahPdspDikdasKec filtered by the prop_ column
 *
 * @method array findByKecId(int $kec_id) Return WilayahPdspDikdasKec objects filtered by the kec_id column
 * @method array findByKodeWilayah(string $kode_wilayah) Return WilayahPdspDikdasKec objects filtered by the kode_wilayah column
 * @method array findByKec(string $kec_) Return WilayahPdspDikdasKec objects filtered by the kec_ column
 * @method array findByKab(string $kab_) Return WilayahPdspDikdasKec objects filtered by the kab_ column
 * @method array findByProp(string $prop_) Return WilayahPdspDikdasKec objects filtered by the prop_ column
 *
 * @package    propel.generator.simdik_batam.Model.om
 */
abstract class BaseWilayahPdspDikdasKecQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseWilayahPdspDikdasKecQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'dapodikdasmen_batam', $modelName = 'simdik_batam\\Model\\WilayahPdspDikdasKec', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new WilayahPdspDikdasKecQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   WilayahPdspDikdasKecQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return WilayahPdspDikdasKecQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof WilayahPdspDikdasKecQuery) {
            return $criteria;
        }
        $query = new WilayahPdspDikdasKecQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   WilayahPdspDikdasKec|WilayahPdspDikdasKec[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = WilayahPdspDikdasKecPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(WilayahPdspDikdasKecPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 WilayahPdspDikdasKec A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByKecId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 WilayahPdspDikdasKec A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [kec_id], [kode_wilayah], [kec_], [kab_], [prop_] FROM [ref].[wilayah_pdsp_dikdas_kec] WHERE [kec_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new WilayahPdspDikdasKec();
            $obj->hydrate($row);
            WilayahPdspDikdasKecPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return WilayahPdspDikdasKec|WilayahPdspDikdasKec[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|WilayahPdspDikdasKec[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return WilayahPdspDikdasKecQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(WilayahPdspDikdasKecPeer::KEC_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return WilayahPdspDikdasKecQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(WilayahPdspDikdasKecPeer::KEC_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the kec_id column
     *
     * Example usage:
     * <code>
     * $query->filterByKecId(1234); // WHERE kec_id = 1234
     * $query->filterByKecId(array(12, 34)); // WHERE kec_id IN (12, 34)
     * $query->filterByKecId(array('min' => 12)); // WHERE kec_id >= 12
     * $query->filterByKecId(array('max' => 12)); // WHERE kec_id <= 12
     * </code>
     *
     * @param     mixed $kecId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return WilayahPdspDikdasKecQuery The current query, for fluid interface
     */
    public function filterByKecId($kecId = null, $comparison = null)
    {
        if (is_array($kecId)) {
            $useMinMax = false;
            if (isset($kecId['min'])) {
                $this->addUsingAlias(WilayahPdspDikdasKecPeer::KEC_ID, $kecId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($kecId['max'])) {
                $this->addUsingAlias(WilayahPdspDikdasKecPeer::KEC_ID, $kecId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(WilayahPdspDikdasKecPeer::KEC_ID, $kecId, $comparison);
    }

    /**
     * Filter the query on the kode_wilayah column
     *
     * Example usage:
     * <code>
     * $query->filterByKodeWilayah('fooValue');   // WHERE kode_wilayah = 'fooValue'
     * $query->filterByKodeWilayah('%fooValue%'); // WHERE kode_wilayah LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kodeWilayah The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return WilayahPdspDikdasKecQuery The current query, for fluid interface
     */
    public function filterByKodeWilayah($kodeWilayah = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kodeWilayah)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kodeWilayah)) {
                $kodeWilayah = str_replace('*', '%', $kodeWilayah);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(WilayahPdspDikdasKecPeer::KODE_WILAYAH, $kodeWilayah, $comparison);
    }

    /**
     * Filter the query on the kec_ column
     *
     * Example usage:
     * <code>
     * $query->filterByKec('fooValue');   // WHERE kec_ = 'fooValue'
     * $query->filterByKec('%fooValue%'); // WHERE kec_ LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kec The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return WilayahPdspDikdasKecQuery The current query, for fluid interface
     */
    public function filterByKec($kec = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kec)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kec)) {
                $kec = str_replace('*', '%', $kec);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(WilayahPdspDikdasKecPeer::KEC_, $kec, $comparison);
    }

    /**
     * Filter the query on the kab_ column
     *
     * Example usage:
     * <code>
     * $query->filterByKab('fooValue');   // WHERE kab_ = 'fooValue'
     * $query->filterByKab('%fooValue%'); // WHERE kab_ LIKE '%fooValue%'
     * </code>
     *
     * @param     string $kab The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return WilayahPdspDikdasKecQuery The current query, for fluid interface
     */
    public function filterByKab($kab = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($kab)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $kab)) {
                $kab = str_replace('*', '%', $kab);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(WilayahPdspDikdasKecPeer::KAB_, $kab, $comparison);
    }

    /**
     * Filter the query on the prop_ column
     *
     * Example usage:
     * <code>
     * $query->filterByProp('fooValue');   // WHERE prop_ = 'fooValue'
     * $query->filterByProp('%fooValue%'); // WHERE prop_ LIKE '%fooValue%'
     * </code>
     *
     * @param     string $prop The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return WilayahPdspDikdasKecQuery The current query, for fluid interface
     */
    public function filterByProp($prop = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($prop)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $prop)) {
                $prop = str_replace('*', '%', $prop);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(WilayahPdspDikdasKecPeer::PROP_, $prop, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   WilayahPdspDikdasKec $wilayahPdspDikdasKec Object to remove from the list of results
     *
     * @return WilayahPdspDikdasKecQuery The current query, for fluid interface
     */
    public function prune($wilayahPdspDikdasKec = null)
    {
        if ($wilayahPdspDikdasKec) {
            $this->addUsingAlias(WilayahPdspDikdasKecPeer::KEC_ID, $wilayahPdspDikdasKec->getKecId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
