<?php

namespace simdik_batam\Model\om;

use \Criteria;
use \Exception;
use \ModelCriteria;
use \PDO;
use \Propel;
use \PropelException;
use \PropelObjectCollection;
use \PropelPDO;
use simdik_batam\Model\JenisSurat;
use simdik_batam\Model\JenisSuratPeer;
use simdik_batam\Model\JenisSuratQuery;

/**
 * Base class that represents a query for the 'adm.jenis_surat' table.
 *
 * 
 *
 * @method JenisSuratQuery orderByJenisSuratId($order = Criteria::ASC) Order by the jenis_surat_id column
 * @method JenisSuratQuery orderByNama($order = Criteria::ASC) Order by the nama column
 *
 * @method JenisSuratQuery groupByJenisSuratId() Group by the jenis_surat_id column
 * @method JenisSuratQuery groupByNama() Group by the nama column
 *
 * @method JenisSuratQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method JenisSuratQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method JenisSuratQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method JenisSurat findOne(PropelPDO $con = null) Return the first JenisSurat matching the query
 * @method JenisSurat findOneOrCreate(PropelPDO $con = null) Return the first JenisSurat matching the query, or a new JenisSurat object populated from the query conditions when no match is found
 *
 * @method JenisSurat findOneByNama(string $nama) Return the first JenisSurat filtered by the nama column
 *
 * @method array findByJenisSuratId(int $jenis_surat_id) Return JenisSurat objects filtered by the jenis_surat_id column
 * @method array findByNama(string $nama) Return JenisSurat objects filtered by the nama column
 *
 * @package    propel.generator.simdik_batam.Model.om
 */
abstract class BaseJenisSuratQuery extends ModelCriteria
{
    /**
     * Initializes internal state of BaseJenisSuratQuery object.
     *
     * @param     string $dbName The dabase name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'dapodikdasmen_batam', $modelName = 'simdik_batam\\Model\\JenisSurat', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new JenisSuratQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param   JenisSuratQuery|Criteria $criteria Optional Criteria to build the query from
     *
     * @return JenisSuratQuery
     */
    public static function create($modelAlias = null, $criteria = null)
    {
        if ($criteria instanceof JenisSuratQuery) {
            return $criteria;
        }
        $query = new JenisSuratQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query 
     * @param     PropelPDO $con an optional connection object
     *
     * @return   JenisSurat|JenisSurat[]|mixed the result, formatted by the current formatter
     */
    public function findPk($key, $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = JenisSuratPeer::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is alredy in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getConnection(JenisSuratPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Alias of findPk to use instance pooling
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 JenisSurat A model object, or null if the key is not found
     * @throws PropelException
     */
     public function findOneByJenisSuratId($key, $con = null)
     {
        return $this->findPk($key, $con);
     }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return                 JenisSurat A model object, or null if the key is not found
     * @throws PropelException
     */
    protected function findPkSimple($key, $con)
    {
        $sql = 'SELECT [jenis_surat_id], [nama] FROM [adm].[jenis_surat] WHERE [jenis_surat_id] = :p0';
        try {
            $stmt = $con->prepare($sql);			
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $obj = new JenisSurat();
            $obj->hydrate($row);
            JenisSuratPeer::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     PropelPDO $con A connection object
     *
     * @return JenisSurat|JenisSurat[]|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($stmt);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     PropelPDO $con an optional connection object
     *
     * @return PropelObjectCollection|JenisSurat[]|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection($this->getDbName(), Propel::CONNECTION_READ);
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $stmt = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($stmt);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return JenisSuratQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(JenisSuratPeer::JENIS_SURAT_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return JenisSuratQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(JenisSuratPeer::JENIS_SURAT_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the jenis_surat_id column
     *
     * Example usage:
     * <code>
     * $query->filterByJenisSuratId(1234); // WHERE jenis_surat_id = 1234
     * $query->filterByJenisSuratId(array(12, 34)); // WHERE jenis_surat_id IN (12, 34)
     * $query->filterByJenisSuratId(array('min' => 12)); // WHERE jenis_surat_id >= 12
     * $query->filterByJenisSuratId(array('max' => 12)); // WHERE jenis_surat_id <= 12
     * </code>
     *
     * @param     mixed $jenisSuratId The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JenisSuratQuery The current query, for fluid interface
     */
    public function filterByJenisSuratId($jenisSuratId = null, $comparison = null)
    {
        if (is_array($jenisSuratId)) {
            $useMinMax = false;
            if (isset($jenisSuratId['min'])) {
                $this->addUsingAlias(JenisSuratPeer::JENIS_SURAT_ID, $jenisSuratId['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($jenisSuratId['max'])) {
                $this->addUsingAlias(JenisSuratPeer::JENIS_SURAT_ID, $jenisSuratId['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(JenisSuratPeer::JENIS_SURAT_ID, $jenisSuratId, $comparison);
    }

    /**
     * Filter the query on the nama column
     *
     * Example usage:
     * <code>
     * $query->filterByNama('fooValue');   // WHERE nama = 'fooValue'
     * $query->filterByNama('%fooValue%'); // WHERE nama LIKE '%fooValue%'
     * </code>
     *
     * @param     string $nama The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return JenisSuratQuery The current query, for fluid interface
     */
    public function filterByNama($nama = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($nama)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $nama)) {
                $nama = str_replace('*', '%', $nama);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(JenisSuratPeer::NAMA, $nama, $comparison);
    }

    /**
     * Exclude object from result
     *
     * @param   JenisSurat $jenisSurat Object to remove from the list of results
     *
     * @return JenisSuratQuery The current query, for fluid interface
     */
    public function prune($jenisSurat = null)
    {
        if ($jenisSurat) {
            $this->addUsingAlias(JenisSuratPeer::JENIS_SURAT_ID, $jenisSurat->getJenisSuratId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

}
