<?php

namespace simdik_batam\Model\om;

use \BaseObject;
use \BasePeer;
use \Criteria;
use \Exception;
use \PDO;
use \Persistent;
use \Propel;
use \PropelException;
use \PropelPDO;
use simdik_batam\Model\Mutasi;
use simdik_batam\Model\MutasiPeer;
use simdik_batam\Model\MutasiQuery;

/**
 * Base class that represents a row from the 'adm.mutasi' table.
 *
 * 
 *
 * @package    propel.generator.simdik_batam.Model.om
 */
abstract class BaseMutasi extends BaseObject implements Persistent
{
    /**
     * Peer class name
     */
    const PEER = 'simdik_batam\\Model\\MutasiPeer';

    /**
     * The Peer class.
     * Instance provides a convenient way of calling static methods on a class
     * that calling code may not be able to identify.
     * @var        MutasiPeer
     */
    protected static $peer;

    /**
     * The flag var to prevent infinit loop in deep copy
     * @var       boolean
     */
    protected $startCopy = false;

    /**
     * The value for the mutasi_id field.
     * @var        string
     */
    protected $mutasi_id;

    /**
     * The value for the peserta_didik_id field.
     * @var        string
     */
    protected $peserta_didik_id;

    /**
     * The value for the soft_delete field.
     * @var        int
     */
    protected $soft_delete;

    /**
     * The value for the nomor_surat field.
     * @var        string
     */
    protected $nomor_surat;

    /**
     * The value for the sekolah_id field.
     * @var        string
     */
    protected $sekolah_id;

    /**
     * The value for the kode_wilayah_berangkat field.
     * @var        string
     */
    protected $kode_wilayah_berangkat;

    /**
     * The value for the maksud_tujuan field.
     * @var        string
     */
    protected $maksud_tujuan;

    /**
     * The value for the nama_yang_diikuti field.
     * @var        string
     */
    protected $nama_yang_diikuti;

    /**
     * The value for the hubungan_keluarga field.
     * @var        string
     */
    protected $hubungan_keluarga;

    /**
     * The value for the pekerjaan field.
     * @var        string
     */
    protected $pekerjaan;

    /**
     * The value for the alamat_yang_diikuti field.
     * @var        string
     */
    protected $alamat_yang_diikuti;

    /**
     * The value for the tanggal_berangkat field.
     * @var        string
     */
    protected $tanggal_berangkat;

    /**
     * The value for the pengguna_id field.
     * @var        string
     */
    protected $pengguna_id;

    /**
     * The value for the sekolah_id_tujuan field.
     * @var        string
     */
    protected $sekolah_id_tujuan;

    /**
     * The value for the nama field.
     * @var        string
     */
    protected $nama;

    /**
     * The value for the tempat_lahir field.
     * @var        string
     */
    protected $tempat_lahir;

    /**
     * The value for the tanggal_lahir field.
     * @var        string
     */
    protected $tanggal_lahir;

    /**
     * The value for the kelas field.
     * @var        string
     */
    protected $kelas;

    /**
     * The value for the nis field.
     * @var        string
     */
    protected $nis;

    /**
     * The value for the nisn field.
     * @var        string
     */
    protected $nisn;

    /**
     * The value for the nama_ayah field.
     * @var        string
     */
    protected $nama_ayah;

    /**
     * The value for the pekerjaan_id_ayah_str field.
     * @var        string
     */
    protected $pekerjaan_id_ayah_str;

    /**
     * The value for the alamat_jalan field.
     * @var        string
     */
    protected $alamat_jalan;

    /**
     * The value for the jenis_surat_id field.
     * @var        int
     */
    protected $jenis_surat_id;

    /**
     * The value for the tanggal_surat field.
     * @var        string
     */
    protected $tanggal_surat;

    /**
     * The value for the keterangan_kepala_sekolah field.
     * @var        string
     */
    protected $keterangan_kepala_sekolah;

    /**
     * The value for the keterangan_lain field.
     * @var        string
     */
    protected $keterangan_lain;

    /**
     * Flag to prevent endless save loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInSave = false;

    /**
     * Flag to prevent endless validation loop, if this object is referenced
     * by another object which falls in this transaction.
     * @var        boolean
     */
    protected $alreadyInValidation = false;

    /**
     * Flag to prevent endless clearAllReferences($deep=true) loop, if this object is referenced
     * @var        boolean
     */
    protected $alreadyInClearAllReferencesDeep = false;

    /**
     * Get the [mutasi_id] column value.
     * 
     * @return string
     */
    public function getMutasiId()
    {
        return $this->mutasi_id;
    }

    /**
     * Get the [peserta_didik_id] column value.
     * 
     * @return string
     */
    public function getPesertaDidikId()
    {
        return $this->peserta_didik_id;
    }

    /**
     * Get the [soft_delete] column value.
     * 
     * @return int
     */
    public function getSoftDelete()
    {
        return $this->soft_delete;
    }

    /**
     * Get the [nomor_surat] column value.
     * 
     * @return string
     */
    public function getNomorSurat()
    {
        return $this->nomor_surat;
    }

    /**
     * Get the [sekolah_id] column value.
     * 
     * @return string
     */
    public function getSekolahId()
    {
        return $this->sekolah_id;
    }

    /**
     * Get the [kode_wilayah_berangkat] column value.
     * 
     * @return string
     */
    public function getKodeWilayahBerangkat()
    {
        return $this->kode_wilayah_berangkat;
    }

    /**
     * Get the [maksud_tujuan] column value.
     * 
     * @return string
     */
    public function getMaksudTujuan()
    {
        return $this->maksud_tujuan;
    }

    /**
     * Get the [nama_yang_diikuti] column value.
     * 
     * @return string
     */
    public function getNamaYangDiikuti()
    {
        return $this->nama_yang_diikuti;
    }

    /**
     * Get the [hubungan_keluarga] column value.
     * 
     * @return string
     */
    public function getHubunganKeluarga()
    {
        return $this->hubungan_keluarga;
    }

    /**
     * Get the [pekerjaan] column value.
     * 
     * @return string
     */
    public function getPekerjaan()
    {
        return $this->pekerjaan;
    }

    /**
     * Get the [alamat_yang_diikuti] column value.
     * 
     * @return string
     */
    public function getAlamatYangDiikuti()
    {
        return $this->alamat_yang_diikuti;
    }

    /**
     * Get the [tanggal_berangkat] column value.
     * 
     * @return string
     */
    public function getTanggalBerangkat()
    {
        return $this->tanggal_berangkat;
    }

    /**
     * Get the [pengguna_id] column value.
     * 
     * @return string
     */
    public function getPenggunaId()
    {
        return $this->pengguna_id;
    }

    /**
     * Get the [sekolah_id_tujuan] column value.
     * 
     * @return string
     */
    public function getSekolahIdTujuan()
    {
        return $this->sekolah_id_tujuan;
    }

    /**
     * Get the [nama] column value.
     * 
     * @return string
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * Get the [tempat_lahir] column value.
     * 
     * @return string
     */
    public function getTempatLahir()
    {
        return $this->tempat_lahir;
    }

    /**
     * Get the [tanggal_lahir] column value.
     * 
     * @return string
     */
    public function getTanggalLahir()
    {
        return $this->tanggal_lahir;
    }

    /**
     * Get the [kelas] column value.
     * 
     * @return string
     */
    public function getKelas()
    {
        return $this->kelas;
    }

    /**
     * Get the [nis] column value.
     * 
     * @return string
     */
    public function getNis()
    {
        return $this->nis;
    }

    /**
     * Get the [nisn] column value.
     * 
     * @return string
     */
    public function getNisn()
    {
        return $this->nisn;
    }

    /**
     * Get the [nama_ayah] column value.
     * 
     * @return string
     */
    public function getNamaAyah()
    {
        return $this->nama_ayah;
    }

    /**
     * Get the [pekerjaan_id_ayah_str] column value.
     * 
     * @return string
     */
    public function getPekerjaanIdAyahStr()
    {
        return $this->pekerjaan_id_ayah_str;
    }

    /**
     * Get the [alamat_jalan] column value.
     * 
     * @return string
     */
    public function getAlamatJalan()
    {
        return $this->alamat_jalan;
    }

    /**
     * Get the [jenis_surat_id] column value.
     * 
     * @return int
     */
    public function getJenisSuratId()
    {
        return $this->jenis_surat_id;
    }

    /**
     * Get the [tanggal_surat] column value.
     * 
     * @return string
     */
    public function getTanggalSurat()
    {
        return $this->tanggal_surat;
    }

    /**
     * Get the [keterangan_kepala_sekolah] column value.
     * 
     * @return string
     */
    public function getKeteranganKepalaSekolah()
    {
        return $this->keterangan_kepala_sekolah;
    }

    /**
     * Get the [keterangan_lain] column value.
     * 
     * @return string
     */
    public function getKeteranganLain()
    {
        return $this->keterangan_lain;
    }

    /**
     * Set the value of [mutasi_id] column.
     * 
     * @param string $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setMutasiId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->mutasi_id !== $v) {
            $this->mutasi_id = $v;
            $this->modifiedColumns[] = MutasiPeer::MUTASI_ID;
        }


        return $this;
    } // setMutasiId()

    /**
     * Set the value of [peserta_didik_id] column.
     * 
     * @param string $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setPesertaDidikId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->peserta_didik_id !== $v) {
            $this->peserta_didik_id = $v;
            $this->modifiedColumns[] = MutasiPeer::PESERTA_DIDIK_ID;
        }


        return $this;
    } // setPesertaDidikId()

    /**
     * Set the value of [soft_delete] column.
     * 
     * @param int $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setSoftDelete($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->soft_delete !== $v) {
            $this->soft_delete = $v;
            $this->modifiedColumns[] = MutasiPeer::SOFT_DELETE;
        }


        return $this;
    } // setSoftDelete()

    /**
     * Set the value of [nomor_surat] column.
     * 
     * @param string $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setNomorSurat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nomor_surat !== $v) {
            $this->nomor_surat = $v;
            $this->modifiedColumns[] = MutasiPeer::NOMOR_SURAT;
        }


        return $this;
    } // setNomorSurat()

    /**
     * Set the value of [sekolah_id] column.
     * 
     * @param string $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setSekolahId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sekolah_id !== $v) {
            $this->sekolah_id = $v;
            $this->modifiedColumns[] = MutasiPeer::SEKOLAH_ID;
        }


        return $this;
    } // setSekolahId()

    /**
     * Set the value of [kode_wilayah_berangkat] column.
     * 
     * @param string $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setKodeWilayahBerangkat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kode_wilayah_berangkat !== $v) {
            $this->kode_wilayah_berangkat = $v;
            $this->modifiedColumns[] = MutasiPeer::KODE_WILAYAH_BERANGKAT;
        }


        return $this;
    } // setKodeWilayahBerangkat()

    /**
     * Set the value of [maksud_tujuan] column.
     * 
     * @param string $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setMaksudTujuan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->maksud_tujuan !== $v) {
            $this->maksud_tujuan = $v;
            $this->modifiedColumns[] = MutasiPeer::MAKSUD_TUJUAN;
        }


        return $this;
    } // setMaksudTujuan()

    /**
     * Set the value of [nama_yang_diikuti] column.
     * 
     * @param string $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setNamaYangDiikuti($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama_yang_diikuti !== $v) {
            $this->nama_yang_diikuti = $v;
            $this->modifiedColumns[] = MutasiPeer::NAMA_YANG_DIIKUTI;
        }


        return $this;
    } // setNamaYangDiikuti()

    /**
     * Set the value of [hubungan_keluarga] column.
     * 
     * @param string $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setHubunganKeluarga($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->hubungan_keluarga !== $v) {
            $this->hubungan_keluarga = $v;
            $this->modifiedColumns[] = MutasiPeer::HUBUNGAN_KELUARGA;
        }


        return $this;
    } // setHubunganKeluarga()

    /**
     * Set the value of [pekerjaan] column.
     * 
     * @param string $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setPekerjaan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pekerjaan !== $v) {
            $this->pekerjaan = $v;
            $this->modifiedColumns[] = MutasiPeer::PEKERJAAN;
        }


        return $this;
    } // setPekerjaan()

    /**
     * Set the value of [alamat_yang_diikuti] column.
     * 
     * @param string $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setAlamatYangDiikuti($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alamat_yang_diikuti !== $v) {
            $this->alamat_yang_diikuti = $v;
            $this->modifiedColumns[] = MutasiPeer::ALAMAT_YANG_DIIKUTI;
        }


        return $this;
    } // setAlamatYangDiikuti()

    /**
     * Set the value of [tanggal_berangkat] column.
     * 
     * @param string $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setTanggalBerangkat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tanggal_berangkat !== $v) {
            $this->tanggal_berangkat = $v;
            $this->modifiedColumns[] = MutasiPeer::TANGGAL_BERANGKAT;
        }


        return $this;
    } // setTanggalBerangkat()

    /**
     * Set the value of [pengguna_id] column.
     * 
     * @param string $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setPenggunaId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pengguna_id !== $v) {
            $this->pengguna_id = $v;
            $this->modifiedColumns[] = MutasiPeer::PENGGUNA_ID;
        }


        return $this;
    } // setPenggunaId()

    /**
     * Set the value of [sekolah_id_tujuan] column.
     * 
     * @param string $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setSekolahIdTujuan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->sekolah_id_tujuan !== $v) {
            $this->sekolah_id_tujuan = $v;
            $this->modifiedColumns[] = MutasiPeer::SEKOLAH_ID_TUJUAN;
        }


        return $this;
    } // setSekolahIdTujuan()

    /**
     * Set the value of [nama] column.
     * 
     * @param string $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setNama($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama !== $v) {
            $this->nama = $v;
            $this->modifiedColumns[] = MutasiPeer::NAMA;
        }


        return $this;
    } // setNama()

    /**
     * Set the value of [tempat_lahir] column.
     * 
     * @param string $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setTempatLahir($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tempat_lahir !== $v) {
            $this->tempat_lahir = $v;
            $this->modifiedColumns[] = MutasiPeer::TEMPAT_LAHIR;
        }


        return $this;
    } // setTempatLahir()

    /**
     * Set the value of [tanggal_lahir] column.
     * 
     * @param string $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setTanggalLahir($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tanggal_lahir !== $v) {
            $this->tanggal_lahir = $v;
            $this->modifiedColumns[] = MutasiPeer::TANGGAL_LAHIR;
        }


        return $this;
    } // setTanggalLahir()

    /**
     * Set the value of [kelas] column.
     * 
     * @param string $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setKelas($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->kelas !== $v) {
            $this->kelas = $v;
            $this->modifiedColumns[] = MutasiPeer::KELAS;
        }


        return $this;
    } // setKelas()

    /**
     * Set the value of [nis] column.
     * 
     * @param string $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setNis($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nis !== $v) {
            $this->nis = $v;
            $this->modifiedColumns[] = MutasiPeer::NIS;
        }


        return $this;
    } // setNis()

    /**
     * Set the value of [nisn] column.
     * 
     * @param string $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setNisn($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nisn !== $v) {
            $this->nisn = $v;
            $this->modifiedColumns[] = MutasiPeer::NISN;
        }


        return $this;
    } // setNisn()

    /**
     * Set the value of [nama_ayah] column.
     * 
     * @param string $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setNamaAyah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->nama_ayah !== $v) {
            $this->nama_ayah = $v;
            $this->modifiedColumns[] = MutasiPeer::NAMA_AYAH;
        }


        return $this;
    } // setNamaAyah()

    /**
     * Set the value of [pekerjaan_id_ayah_str] column.
     * 
     * @param string $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setPekerjaanIdAyahStr($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->pekerjaan_id_ayah_str !== $v) {
            $this->pekerjaan_id_ayah_str = $v;
            $this->modifiedColumns[] = MutasiPeer::PEKERJAAN_ID_AYAH_STR;
        }


        return $this;
    } // setPekerjaanIdAyahStr()

    /**
     * Set the value of [alamat_jalan] column.
     * 
     * @param string $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setAlamatJalan($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->alamat_jalan !== $v) {
            $this->alamat_jalan = $v;
            $this->modifiedColumns[] = MutasiPeer::ALAMAT_JALAN;
        }


        return $this;
    } // setAlamatJalan()

    /**
     * Set the value of [jenis_surat_id] column.
     * 
     * @param int $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setJenisSuratId($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (int) $v;
        }

        if ($this->jenis_surat_id !== $v) {
            $this->jenis_surat_id = $v;
            $this->modifiedColumns[] = MutasiPeer::JENIS_SURAT_ID;
        }


        return $this;
    } // setJenisSuratId()

    /**
     * Set the value of [tanggal_surat] column.
     * 
     * @param string $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setTanggalSurat($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->tanggal_surat !== $v) {
            $this->tanggal_surat = $v;
            $this->modifiedColumns[] = MutasiPeer::TANGGAL_SURAT;
        }


        return $this;
    } // setTanggalSurat()

    /**
     * Set the value of [keterangan_kepala_sekolah] column.
     * 
     * @param string $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setKeteranganKepalaSekolah($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->keterangan_kepala_sekolah !== $v) {
            $this->keterangan_kepala_sekolah = $v;
            $this->modifiedColumns[] = MutasiPeer::KETERANGAN_KEPALA_SEKOLAH;
        }


        return $this;
    } // setKeteranganKepalaSekolah()

    /**
     * Set the value of [keterangan_lain] column.
     * 
     * @param string $v new value
     * @return Mutasi The current object (for fluent API support)
     */
    public function setKeteranganLain($v)
    {
        if ($v !== null && is_numeric($v)) {
            $v = (string) $v;
        }

        if ($this->keterangan_lain !== $v) {
            $this->keterangan_lain = $v;
            $this->modifiedColumns[] = MutasiPeer::KETERANGAN_LAIN;
        }


        return $this;
    } // setKeteranganLain()

    /**
     * Indicates whether the columns in this object are only set to default values.
     *
     * This method can be used in conjunction with isModified() to indicate whether an object is both
     * modified _and_ has some values set which are non-default.
     *
     * @return boolean Whether the columns in this object are only been set with default values.
     */
    public function hasOnlyDefaultValues()
    {
        // otherwise, everything was equal, so return true
        return true;
    } // hasOnlyDefaultValues()

    /**
     * Hydrates (populates) the object variables with values from the database resultset.
     *
     * An offset (0-based "start column") is specified so that objects can be hydrated
     * with a subset of the columns in the resultset rows.  This is needed, for example,
     * for results of JOIN queries where the resultset row includes columns from two or
     * more tables.
     *
     * @param array $row The row returned by PDOStatement->fetch(PDO::FETCH_NUM)
     * @param int $startcol 0-based offset column which indicates which restultset column to start with.
     * @param boolean $rehydrate Whether this object is being re-hydrated from the database.
     * @return int             next starting column
     * @throws PropelException - Any caught Exception will be rewrapped as a PropelException.
     */
    public function hydrate($row, $startcol = 0, $rehydrate = false)
    {
        try {

            $this->mutasi_id = ($row[$startcol + 0] !== null) ? (string) $row[$startcol + 0] : null;
            $this->peserta_didik_id = ($row[$startcol + 1] !== null) ? (string) $row[$startcol + 1] : null;
            $this->soft_delete = ($row[$startcol + 2] !== null) ? (int) $row[$startcol + 2] : null;
            $this->nomor_surat = ($row[$startcol + 3] !== null) ? (string) $row[$startcol + 3] : null;
            $this->sekolah_id = ($row[$startcol + 4] !== null) ? (string) $row[$startcol + 4] : null;
            $this->kode_wilayah_berangkat = ($row[$startcol + 5] !== null) ? (string) $row[$startcol + 5] : null;
            $this->maksud_tujuan = ($row[$startcol + 6] !== null) ? (string) $row[$startcol + 6] : null;
            $this->nama_yang_diikuti = ($row[$startcol + 7] !== null) ? (string) $row[$startcol + 7] : null;
            $this->hubungan_keluarga = ($row[$startcol + 8] !== null) ? (string) $row[$startcol + 8] : null;
            $this->pekerjaan = ($row[$startcol + 9] !== null) ? (string) $row[$startcol + 9] : null;
            $this->alamat_yang_diikuti = ($row[$startcol + 10] !== null) ? (string) $row[$startcol + 10] : null;
            $this->tanggal_berangkat = ($row[$startcol + 11] !== null) ? (string) $row[$startcol + 11] : null;
            $this->pengguna_id = ($row[$startcol + 12] !== null) ? (string) $row[$startcol + 12] : null;
            $this->sekolah_id_tujuan = ($row[$startcol + 13] !== null) ? (string) $row[$startcol + 13] : null;
            $this->nama = ($row[$startcol + 14] !== null) ? (string) $row[$startcol + 14] : null;
            $this->tempat_lahir = ($row[$startcol + 15] !== null) ? (string) $row[$startcol + 15] : null;
            $this->tanggal_lahir = ($row[$startcol + 16] !== null) ? (string) $row[$startcol + 16] : null;
            $this->kelas = ($row[$startcol + 17] !== null) ? (string) $row[$startcol + 17] : null;
            $this->nis = ($row[$startcol + 18] !== null) ? (string) $row[$startcol + 18] : null;
            $this->nisn = ($row[$startcol + 19] !== null) ? (string) $row[$startcol + 19] : null;
            $this->nama_ayah = ($row[$startcol + 20] !== null) ? (string) $row[$startcol + 20] : null;
            $this->pekerjaan_id_ayah_str = ($row[$startcol + 21] !== null) ? (string) $row[$startcol + 21] : null;
            $this->alamat_jalan = ($row[$startcol + 22] !== null) ? (string) $row[$startcol + 22] : null;
            $this->jenis_surat_id = ($row[$startcol + 23] !== null) ? (int) $row[$startcol + 23] : null;
            $this->tanggal_surat = ($row[$startcol + 24] !== null) ? (string) $row[$startcol + 24] : null;
            $this->keterangan_kepala_sekolah = ($row[$startcol + 25] !== null) ? (string) $row[$startcol + 25] : null;
            $this->keterangan_lain = ($row[$startcol + 26] !== null) ? (string) $row[$startcol + 26] : null;
            $this->resetModified();

            $this->setNew(false);

            if ($rehydrate) {
                $this->ensureConsistency();
            }
            $this->postHydrate($row, $startcol, $rehydrate);
            return $startcol + 27; // 27 = MutasiPeer::NUM_HYDRATE_COLUMNS.

        } catch (Exception $e) {
            throw new PropelException("Error populating Mutasi object", $e);
        }
    }

    /**
     * Checks and repairs the internal consistency of the object.
     *
     * This method is executed after an already-instantiated object is re-hydrated
     * from the database.  It exists to check any foreign keys to make sure that
     * the objects related to the current object are correct based on foreign key.
     *
     * You can override this method in the stub class, but you should always invoke
     * the base method from the overridden method (i.e. parent::ensureConsistency()),
     * in case your model changes.
     *
     * @throws PropelException
     */
    public function ensureConsistency()
    {

    } // ensureConsistency

    /**
     * Reloads this object from datastore based on primary key and (optionally) resets all associated objects.
     *
     * This will only work if the object has been saved and has a valid primary key set.
     *
     * @param boolean $deep (optional) Whether to also de-associated any related objects.
     * @param PropelPDO $con (optional) The PropelPDO connection to use.
     * @return void
     * @throws PropelException - if this object is deleted, unsaved or doesn't have pk match in db
     */
    public function reload($deep = false, PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("Cannot reload a deleted object.");
        }

        if ($this->isNew()) {
            throw new PropelException("Cannot reload an unsaved object.");
        }

        if ($con === null) {
            $con = Propel::getConnection(MutasiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        // We don't need to alter the object instance pool; we're just modifying this instance
        // already in the pool.

        $stmt = MutasiPeer::doSelectStmt($this->buildPkeyCriteria(), $con);
        $row = $stmt->fetch(PDO::FETCH_NUM);
        $stmt->closeCursor();
        if (!$row) {
            throw new PropelException('Cannot find matching row in the database to reload object values.');
        }
        $this->hydrate($row, 0, true); // rehydrate

        if ($deep) {  // also de-associate any related objects?

        } // if (deep)
    }

    /**
     * Removes this object from datastore and sets delete attribute.
     *
     * @param PropelPDO $con
     * @return void
     * @throws PropelException
     * @throws Exception
     * @see        BaseObject::setDeleted()
     * @see        BaseObject::isDeleted()
     */
    public function delete(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("This object has already been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(MutasiPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        try {
            $deleteQuery = MutasiQuery::create()
                ->filterByPrimaryKey($this->getPrimaryKey());
            $ret = $this->preDelete($con);
            if ($ret) {
                $deleteQuery->delete($con);
                $this->postDelete($con);
                $con->commit();
                $this->setDeleted(true);
            } else {
                $con->commit();
            }
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Persists this object to the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All modified related objects will also be persisted in the doSave()
     * method.  This method wraps all precipitate database operations in a
     * single transaction.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @throws Exception
     * @see        doSave()
     */
    public function save(PropelPDO $con = null)
    {
        if ($this->isDeleted()) {
            throw new PropelException("You cannot save an object that has been deleted.");
        }

        if ($con === null) {
            $con = Propel::getConnection(MutasiPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $con->beginTransaction();
        $isInsert = $this->isNew();
        try {
            $ret = $this->preSave($con);
            if ($isInsert) {
                $ret = $ret && $this->preInsert($con);
            } else {
                $ret = $ret && $this->preUpdate($con);
            }
            if ($ret) {
                $affectedRows = $this->doSave($con);
                if ($isInsert) {
                    $this->postInsert($con);
                } else {
                    $this->postUpdate($con);
                }
                $this->postSave($con);
                MutasiPeer::addInstanceToPool($this);
            } else {
                $affectedRows = 0;
            }
            $con->commit();

            return $affectedRows;
        } catch (Exception $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs the work of inserting or updating the row in the database.
     *
     * If the object is new, it inserts it; otherwise an update is performed.
     * All related objects are also updated in this method.
     *
     * @param PropelPDO $con
     * @return int             The number of rows affected by this insert/update and any referring fk objects' save() operations.
     * @throws PropelException
     * @see        save()
     */
    protected function doSave(PropelPDO $con)
    {
        $affectedRows = 0; // initialize var to track total num of affected rows
        if (!$this->alreadyInSave) {
            $this->alreadyInSave = true;

            if ($this->isNew() || $this->isModified()) {
                // persist changes
                if ($this->isNew()) {
                    $this->doInsert($con);
                } else {
                    $this->doUpdate($con);
                }
                $affectedRows += 1;
                $this->resetModified();
            }

            $this->alreadyInSave = false;

        }

        return $affectedRows;
    } // doSave()

    /**
     * Insert the row in the database.
     *
     * @param PropelPDO $con
     *
     * @throws PropelException
     * @see        doSave()
     */
    protected function doInsert(PropelPDO $con)
    {
        $criteria = $this->buildCriteria();
        $pk = BasePeer::doInsert($criteria, $con);
        $this->setNew(false);
    }

    /**
     * Update the row in the database.
     *
     * @param PropelPDO $con
     *
     * @see        doSave()
     */
    protected function doUpdate(PropelPDO $con)
    {
        $selectCriteria = $this->buildPkeyCriteria();
        $valuesCriteria = $this->buildCriteria();
        BasePeer::doUpdate($selectCriteria, $valuesCriteria, $con);
    }

    /**
     * Array of ValidationFailed objects.
     * @var        array ValidationFailed[]
     */
    protected $validationFailures = array();

    /**
     * Gets any ValidationFailed objects that resulted from last call to validate().
     *
     *
     * @return array ValidationFailed[]
     * @see        validate()
     */
    public function getValidationFailures()
    {
        return $this->validationFailures;
    }

    /**
     * Validates the objects modified field values and all objects related to this table.
     *
     * If $columns is either a column name or an array of column names
     * only those columns are validated.
     *
     * @param mixed $columns Column name or an array of column names.
     * @return boolean Whether all columns pass validation.
     * @see        doValidate()
     * @see        getValidationFailures()
     */
    public function validate($columns = null)
    {
        $res = $this->doValidate($columns);
        if ($res === true) {
            $this->validationFailures = array();

            return true;
        }

        $this->validationFailures = $res;

        return false;
    }

    /**
     * This function performs the validation work for complex object models.
     *
     * In addition to checking the current object, all related objects will
     * also be validated.  If all pass then <code>true</code> is returned; otherwise
     * an aggreagated array of ValidationFailed objects will be returned.
     *
     * @param array $columns Array of column names to validate.
     * @return mixed <code>true</code> if all validations pass; array of <code>ValidationFailed</code> objets otherwise.
     */
    protected function doValidate($columns = null)
    {
        if (!$this->alreadyInValidation) {
            $this->alreadyInValidation = true;
            $retval = null;

            $failureMap = array();


            if (($retval = MutasiPeer::doValidate($this, $columns)) !== true) {
                $failureMap = array_merge($failureMap, $retval);
            }



            $this->alreadyInValidation = false;
        }

        return (!empty($failureMap) ? $failureMap : true);
    }

    /**
     * Retrieves a field from the object by name passed in as a string.
     *
     * @param string $name name
     * @param string $type The type of fieldname the $name is of:
     *               one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *               BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *               Defaults to BasePeer::TYPE_PHPNAME
     * @return mixed Value of field.
     */
    public function getByName($name, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = MutasiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);
        $field = $this->getByPosition($pos);

        return $field;
    }

    /**
     * Retrieves a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @return mixed Value of field at $pos
     */
    public function getByPosition($pos)
    {
        switch ($pos) {
            case 0:
                return $this->getMutasiId();
                break;
            case 1:
                return $this->getPesertaDidikId();
                break;
            case 2:
                return $this->getSoftDelete();
                break;
            case 3:
                return $this->getNomorSurat();
                break;
            case 4:
                return $this->getSekolahId();
                break;
            case 5:
                return $this->getKodeWilayahBerangkat();
                break;
            case 6:
                return $this->getMaksudTujuan();
                break;
            case 7:
                return $this->getNamaYangDiikuti();
                break;
            case 8:
                return $this->getHubunganKeluarga();
                break;
            case 9:
                return $this->getPekerjaan();
                break;
            case 10:
                return $this->getAlamatYangDiikuti();
                break;
            case 11:
                return $this->getTanggalBerangkat();
                break;
            case 12:
                return $this->getPenggunaId();
                break;
            case 13:
                return $this->getSekolahIdTujuan();
                break;
            case 14:
                return $this->getNama();
                break;
            case 15:
                return $this->getTempatLahir();
                break;
            case 16:
                return $this->getTanggalLahir();
                break;
            case 17:
                return $this->getKelas();
                break;
            case 18:
                return $this->getNis();
                break;
            case 19:
                return $this->getNisn();
                break;
            case 20:
                return $this->getNamaAyah();
                break;
            case 21:
                return $this->getPekerjaanIdAyahStr();
                break;
            case 22:
                return $this->getAlamatJalan();
                break;
            case 23:
                return $this->getJenisSuratId();
                break;
            case 24:
                return $this->getTanggalSurat();
                break;
            case 25:
                return $this->getKeteranganKepalaSekolah();
                break;
            case 26:
                return $this->getKeteranganLain();
                break;
            default:
                return null;
                break;
        } // switch()
    }

    /**
     * Exports the object as an array.
     *
     * You can specify the key type of the array by passing one of the class
     * type constants.
     *
     * @param     string  $keyType (optional) One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     *                    BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                    Defaults to BasePeer::TYPE_PHPNAME.
     * @param     boolean $includeLazyLoadColumns (optional) Whether to include lazy loaded columns. Defaults to true.
     * @param     array $alreadyDumpedObjects List of objects to skip to avoid recursion
     *
     * @return array an associative array containing the field names (as keys) and field values
     */
    public function toArray($keyType = BasePeer::TYPE_PHPNAME, $includeLazyLoadColumns = true, $alreadyDumpedObjects = array())
    {
        if (isset($alreadyDumpedObjects['Mutasi'][$this->getPrimaryKey()])) {
            return '*RECURSION*';
        }
        $alreadyDumpedObjects['Mutasi'][$this->getPrimaryKey()] = true;
        $keys = MutasiPeer::getFieldNames($keyType);
        $result = array(
            $keys[0] => $this->getMutasiId(),
            $keys[1] => $this->getPesertaDidikId(),
            $keys[2] => $this->getSoftDelete(),
            $keys[3] => $this->getNomorSurat(),
            $keys[4] => $this->getSekolahId(),
            $keys[5] => $this->getKodeWilayahBerangkat(),
            $keys[6] => $this->getMaksudTujuan(),
            $keys[7] => $this->getNamaYangDiikuti(),
            $keys[8] => $this->getHubunganKeluarga(),
            $keys[9] => $this->getPekerjaan(),
            $keys[10] => $this->getAlamatYangDiikuti(),
            $keys[11] => $this->getTanggalBerangkat(),
            $keys[12] => $this->getPenggunaId(),
            $keys[13] => $this->getSekolahIdTujuan(),
            $keys[14] => $this->getNama(),
            $keys[15] => $this->getTempatLahir(),
            $keys[16] => $this->getTanggalLahir(),
            $keys[17] => $this->getKelas(),
            $keys[18] => $this->getNis(),
            $keys[19] => $this->getNisn(),
            $keys[20] => $this->getNamaAyah(),
            $keys[21] => $this->getPekerjaanIdAyahStr(),
            $keys[22] => $this->getAlamatJalan(),
            $keys[23] => $this->getJenisSuratId(),
            $keys[24] => $this->getTanggalSurat(),
            $keys[25] => $this->getKeteranganKepalaSekolah(),
            $keys[26] => $this->getKeteranganLain(),
        );

        return $result;
    }

    /**
     * Sets a field from the object by name passed in as a string.
     *
     * @param string $name peer name
     * @param mixed $value field value
     * @param string $type The type of fieldname the $name is of:
     *                     one of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                     BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     *                     Defaults to BasePeer::TYPE_PHPNAME
     * @return void
     */
    public function setByName($name, $value, $type = BasePeer::TYPE_PHPNAME)
    {
        $pos = MutasiPeer::translateFieldName($name, $type, BasePeer::TYPE_NUM);

        $this->setByPosition($pos, $value);
    }

    /**
     * Sets a field from the object by Position as specified in the xml schema.
     * Zero-based.
     *
     * @param int $pos position in xml schema
     * @param mixed $value field value
     * @return void
     */
    public function setByPosition($pos, $value)
    {
        switch ($pos) {
            case 0:
                $this->setMutasiId($value);
                break;
            case 1:
                $this->setPesertaDidikId($value);
                break;
            case 2:
                $this->setSoftDelete($value);
                break;
            case 3:
                $this->setNomorSurat($value);
                break;
            case 4:
                $this->setSekolahId($value);
                break;
            case 5:
                $this->setKodeWilayahBerangkat($value);
                break;
            case 6:
                $this->setMaksudTujuan($value);
                break;
            case 7:
                $this->setNamaYangDiikuti($value);
                break;
            case 8:
                $this->setHubunganKeluarga($value);
                break;
            case 9:
                $this->setPekerjaan($value);
                break;
            case 10:
                $this->setAlamatYangDiikuti($value);
                break;
            case 11:
                $this->setTanggalBerangkat($value);
                break;
            case 12:
                $this->setPenggunaId($value);
                break;
            case 13:
                $this->setSekolahIdTujuan($value);
                break;
            case 14:
                $this->setNama($value);
                break;
            case 15:
                $this->setTempatLahir($value);
                break;
            case 16:
                $this->setTanggalLahir($value);
                break;
            case 17:
                $this->setKelas($value);
                break;
            case 18:
                $this->setNis($value);
                break;
            case 19:
                $this->setNisn($value);
                break;
            case 20:
                $this->setNamaAyah($value);
                break;
            case 21:
                $this->setPekerjaanIdAyahStr($value);
                break;
            case 22:
                $this->setAlamatJalan($value);
                break;
            case 23:
                $this->setJenisSuratId($value);
                break;
            case 24:
                $this->setTanggalSurat($value);
                break;
            case 25:
                $this->setKeteranganKepalaSekolah($value);
                break;
            case 26:
                $this->setKeteranganLain($value);
                break;
        } // switch()
    }

    /**
     * Populates the object using an array.
     *
     * This is particularly useful when populating an object from one of the
     * request arrays (e.g. $_POST).  This method goes through the column
     * names, checking to see whether a matching key exists in populated
     * array. If so the setByName() method is called for that column.
     *
     * You can specify the key type of the array by additionally passing one
     * of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME,
     * BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM.
     * The default key type is the column's BasePeer::TYPE_PHPNAME
     *
     * @param array  $arr     An array to populate the object from.
     * @param string $keyType The type of keys the array uses.
     * @return void
     */
    public function fromArray($arr, $keyType = BasePeer::TYPE_PHPNAME)
    {
        $keys = MutasiPeer::getFieldNames($keyType);

        if (array_key_exists($keys[0], $arr)) $this->setMutasiId($arr[$keys[0]]);
        if (array_key_exists($keys[1], $arr)) $this->setPesertaDidikId($arr[$keys[1]]);
        if (array_key_exists($keys[2], $arr)) $this->setSoftDelete($arr[$keys[2]]);
        if (array_key_exists($keys[3], $arr)) $this->setNomorSurat($arr[$keys[3]]);
        if (array_key_exists($keys[4], $arr)) $this->setSekolahId($arr[$keys[4]]);
        if (array_key_exists($keys[5], $arr)) $this->setKodeWilayahBerangkat($arr[$keys[5]]);
        if (array_key_exists($keys[6], $arr)) $this->setMaksudTujuan($arr[$keys[6]]);
        if (array_key_exists($keys[7], $arr)) $this->setNamaYangDiikuti($arr[$keys[7]]);
        if (array_key_exists($keys[8], $arr)) $this->setHubunganKeluarga($arr[$keys[8]]);
        if (array_key_exists($keys[9], $arr)) $this->setPekerjaan($arr[$keys[9]]);
        if (array_key_exists($keys[10], $arr)) $this->setAlamatYangDiikuti($arr[$keys[10]]);
        if (array_key_exists($keys[11], $arr)) $this->setTanggalBerangkat($arr[$keys[11]]);
        if (array_key_exists($keys[12], $arr)) $this->setPenggunaId($arr[$keys[12]]);
        if (array_key_exists($keys[13], $arr)) $this->setSekolahIdTujuan($arr[$keys[13]]);
        if (array_key_exists($keys[14], $arr)) $this->setNama($arr[$keys[14]]);
        if (array_key_exists($keys[15], $arr)) $this->setTempatLahir($arr[$keys[15]]);
        if (array_key_exists($keys[16], $arr)) $this->setTanggalLahir($arr[$keys[16]]);
        if (array_key_exists($keys[17], $arr)) $this->setKelas($arr[$keys[17]]);
        if (array_key_exists($keys[18], $arr)) $this->setNis($arr[$keys[18]]);
        if (array_key_exists($keys[19], $arr)) $this->setNisn($arr[$keys[19]]);
        if (array_key_exists($keys[20], $arr)) $this->setNamaAyah($arr[$keys[20]]);
        if (array_key_exists($keys[21], $arr)) $this->setPekerjaanIdAyahStr($arr[$keys[21]]);
        if (array_key_exists($keys[22], $arr)) $this->setAlamatJalan($arr[$keys[22]]);
        if (array_key_exists($keys[23], $arr)) $this->setJenisSuratId($arr[$keys[23]]);
        if (array_key_exists($keys[24], $arr)) $this->setTanggalSurat($arr[$keys[24]]);
        if (array_key_exists($keys[25], $arr)) $this->setKeteranganKepalaSekolah($arr[$keys[25]]);
        if (array_key_exists($keys[26], $arr)) $this->setKeteranganLain($arr[$keys[26]]);
    }

    /**
     * Build a Criteria object containing the values of all modified columns in this object.
     *
     * @return Criteria The Criteria object containing all modified values.
     */
    public function buildCriteria()
    {
        $criteria = new Criteria(MutasiPeer::DATABASE_NAME);

        if ($this->isColumnModified(MutasiPeer::MUTASI_ID)) $criteria->add(MutasiPeer::MUTASI_ID, $this->mutasi_id);
        if ($this->isColumnModified(MutasiPeer::PESERTA_DIDIK_ID)) $criteria->add(MutasiPeer::PESERTA_DIDIK_ID, $this->peserta_didik_id);
        if ($this->isColumnModified(MutasiPeer::SOFT_DELETE)) $criteria->add(MutasiPeer::SOFT_DELETE, $this->soft_delete);
        if ($this->isColumnModified(MutasiPeer::NOMOR_SURAT)) $criteria->add(MutasiPeer::NOMOR_SURAT, $this->nomor_surat);
        if ($this->isColumnModified(MutasiPeer::SEKOLAH_ID)) $criteria->add(MutasiPeer::SEKOLAH_ID, $this->sekolah_id);
        if ($this->isColumnModified(MutasiPeer::KODE_WILAYAH_BERANGKAT)) $criteria->add(MutasiPeer::KODE_WILAYAH_BERANGKAT, $this->kode_wilayah_berangkat);
        if ($this->isColumnModified(MutasiPeer::MAKSUD_TUJUAN)) $criteria->add(MutasiPeer::MAKSUD_TUJUAN, $this->maksud_tujuan);
        if ($this->isColumnModified(MutasiPeer::NAMA_YANG_DIIKUTI)) $criteria->add(MutasiPeer::NAMA_YANG_DIIKUTI, $this->nama_yang_diikuti);
        if ($this->isColumnModified(MutasiPeer::HUBUNGAN_KELUARGA)) $criteria->add(MutasiPeer::HUBUNGAN_KELUARGA, $this->hubungan_keluarga);
        if ($this->isColumnModified(MutasiPeer::PEKERJAAN)) $criteria->add(MutasiPeer::PEKERJAAN, $this->pekerjaan);
        if ($this->isColumnModified(MutasiPeer::ALAMAT_YANG_DIIKUTI)) $criteria->add(MutasiPeer::ALAMAT_YANG_DIIKUTI, $this->alamat_yang_diikuti);
        if ($this->isColumnModified(MutasiPeer::TANGGAL_BERANGKAT)) $criteria->add(MutasiPeer::TANGGAL_BERANGKAT, $this->tanggal_berangkat);
        if ($this->isColumnModified(MutasiPeer::PENGGUNA_ID)) $criteria->add(MutasiPeer::PENGGUNA_ID, $this->pengguna_id);
        if ($this->isColumnModified(MutasiPeer::SEKOLAH_ID_TUJUAN)) $criteria->add(MutasiPeer::SEKOLAH_ID_TUJUAN, $this->sekolah_id_tujuan);
        if ($this->isColumnModified(MutasiPeer::NAMA)) $criteria->add(MutasiPeer::NAMA, $this->nama);
        if ($this->isColumnModified(MutasiPeer::TEMPAT_LAHIR)) $criteria->add(MutasiPeer::TEMPAT_LAHIR, $this->tempat_lahir);
        if ($this->isColumnModified(MutasiPeer::TANGGAL_LAHIR)) $criteria->add(MutasiPeer::TANGGAL_LAHIR, $this->tanggal_lahir);
        if ($this->isColumnModified(MutasiPeer::KELAS)) $criteria->add(MutasiPeer::KELAS, $this->kelas);
        if ($this->isColumnModified(MutasiPeer::NIS)) $criteria->add(MutasiPeer::NIS, $this->nis);
        if ($this->isColumnModified(MutasiPeer::NISN)) $criteria->add(MutasiPeer::NISN, $this->nisn);
        if ($this->isColumnModified(MutasiPeer::NAMA_AYAH)) $criteria->add(MutasiPeer::NAMA_AYAH, $this->nama_ayah);
        if ($this->isColumnModified(MutasiPeer::PEKERJAAN_ID_AYAH_STR)) $criteria->add(MutasiPeer::PEKERJAAN_ID_AYAH_STR, $this->pekerjaan_id_ayah_str);
        if ($this->isColumnModified(MutasiPeer::ALAMAT_JALAN)) $criteria->add(MutasiPeer::ALAMAT_JALAN, $this->alamat_jalan);
        if ($this->isColumnModified(MutasiPeer::JENIS_SURAT_ID)) $criteria->add(MutasiPeer::JENIS_SURAT_ID, $this->jenis_surat_id);
        if ($this->isColumnModified(MutasiPeer::TANGGAL_SURAT)) $criteria->add(MutasiPeer::TANGGAL_SURAT, $this->tanggal_surat);
        if ($this->isColumnModified(MutasiPeer::KETERANGAN_KEPALA_SEKOLAH)) $criteria->add(MutasiPeer::KETERANGAN_KEPALA_SEKOLAH, $this->keterangan_kepala_sekolah);
        if ($this->isColumnModified(MutasiPeer::KETERANGAN_LAIN)) $criteria->add(MutasiPeer::KETERANGAN_LAIN, $this->keterangan_lain);

        return $criteria;
    }

    /**
     * Builds a Criteria object containing the primary key for this object.
     *
     * Unlike buildCriteria() this method includes the primary key values regardless
     * of whether or not they have been modified.
     *
     * @return Criteria The Criteria object containing value(s) for primary key(s).
     */
    public function buildPkeyCriteria()
    {
        $criteria = new Criteria(MutasiPeer::DATABASE_NAME);
        $criteria->add(MutasiPeer::MUTASI_ID, $this->mutasi_id);

        return $criteria;
    }

    /**
     * Returns the primary key for this object (row).
     * @return string
     */
    public function getPrimaryKey()
    {
        return $this->getMutasiId();
    }

    /**
     * Generic method to set the primary key (mutasi_id column).
     *
     * @param  string $key Primary key.
     * @return void
     */
    public function setPrimaryKey($key)
    {
        $this->setMutasiId($key);
    }

    /**
     * Returns true if the primary key for this object is null.
     * @return boolean
     */
    public function isPrimaryKeyNull()
    {

        return null === $this->getMutasiId();
    }

    /**
     * Sets contents of passed object to values from current object.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param object $copyObj An object of Mutasi (or compatible) type.
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @param boolean $makeNew Whether to reset autoincrement PKs and make the object new.
     * @throws PropelException
     */
    public function copyInto($copyObj, $deepCopy = false, $makeNew = true)
    {
        $copyObj->setPesertaDidikId($this->getPesertaDidikId());
        $copyObj->setSoftDelete($this->getSoftDelete());
        $copyObj->setNomorSurat($this->getNomorSurat());
        $copyObj->setSekolahId($this->getSekolahId());
        $copyObj->setKodeWilayahBerangkat($this->getKodeWilayahBerangkat());
        $copyObj->setMaksudTujuan($this->getMaksudTujuan());
        $copyObj->setNamaYangDiikuti($this->getNamaYangDiikuti());
        $copyObj->setHubunganKeluarga($this->getHubunganKeluarga());
        $copyObj->setPekerjaan($this->getPekerjaan());
        $copyObj->setAlamatYangDiikuti($this->getAlamatYangDiikuti());
        $copyObj->setTanggalBerangkat($this->getTanggalBerangkat());
        $copyObj->setPenggunaId($this->getPenggunaId());
        $copyObj->setSekolahIdTujuan($this->getSekolahIdTujuan());
        $copyObj->setNama($this->getNama());
        $copyObj->setTempatLahir($this->getTempatLahir());
        $copyObj->setTanggalLahir($this->getTanggalLahir());
        $copyObj->setKelas($this->getKelas());
        $copyObj->setNis($this->getNis());
        $copyObj->setNisn($this->getNisn());
        $copyObj->setNamaAyah($this->getNamaAyah());
        $copyObj->setPekerjaanIdAyahStr($this->getPekerjaanIdAyahStr());
        $copyObj->setAlamatJalan($this->getAlamatJalan());
        $copyObj->setJenisSuratId($this->getJenisSuratId());
        $copyObj->setTanggalSurat($this->getTanggalSurat());
        $copyObj->setKeteranganKepalaSekolah($this->getKeteranganKepalaSekolah());
        $copyObj->setKeteranganLain($this->getKeteranganLain());
        if ($makeNew) {
            $copyObj->setNew(true);
            $copyObj->setMutasiId(NULL); // this is a auto-increment column, so set to default value
        }
    }

    /**
     * Makes a copy of this object that will be inserted as a new row in table when saved.
     * It creates a new object filling in the simple attributes, but skipping any primary
     * keys that are defined for the table.
     *
     * If desired, this method can also make copies of all associated (fkey referrers)
     * objects.
     *
     * @param boolean $deepCopy Whether to also copy all rows that refer (by fkey) to the current row.
     * @return Mutasi Clone of current object.
     * @throws PropelException
     */
    public function copy($deepCopy = false)
    {
        // we use get_class(), because this might be a subclass
        $clazz = get_class($this);
        $copyObj = new $clazz();
        $this->copyInto($copyObj, $deepCopy);

        return $copyObj;
    }

    /**
     * Returns a peer instance associated with this om.
     *
     * Since Peer classes are not to have any instance attributes, this method returns the
     * same instance for all member of this class. The method could therefore
     * be static, but this would prevent one from overriding the behavior.
     *
     * @return MutasiPeer
     */
    public function getPeer()
    {
        if (self::$peer === null) {
            self::$peer = new MutasiPeer();
        }

        return self::$peer;
    }

    /**
     * Clears the current object and sets all attributes to their default values
     */
    public function clear()
    {
        $this->mutasi_id = null;
        $this->peserta_didik_id = null;
        $this->soft_delete = null;
        $this->nomor_surat = null;
        $this->sekolah_id = null;
        $this->kode_wilayah_berangkat = null;
        $this->maksud_tujuan = null;
        $this->nama_yang_diikuti = null;
        $this->hubungan_keluarga = null;
        $this->pekerjaan = null;
        $this->alamat_yang_diikuti = null;
        $this->tanggal_berangkat = null;
        $this->pengguna_id = null;
        $this->sekolah_id_tujuan = null;
        $this->nama = null;
        $this->tempat_lahir = null;
        $this->tanggal_lahir = null;
        $this->kelas = null;
        $this->nis = null;
        $this->nisn = null;
        $this->nama_ayah = null;
        $this->pekerjaan_id_ayah_str = null;
        $this->alamat_jalan = null;
        $this->jenis_surat_id = null;
        $this->tanggal_surat = null;
        $this->keterangan_kepala_sekolah = null;
        $this->keterangan_lain = null;
        $this->alreadyInSave = false;
        $this->alreadyInValidation = false;
        $this->alreadyInClearAllReferencesDeep = false;
        $this->clearAllReferences();
        $this->resetModified();
        $this->setNew(true);
        $this->setDeleted(false);
    }

    /**
     * Resets all references to other model objects or collections of model objects.
     *
     * This method is a user-space workaround for PHP's inability to garbage collect
     * objects with circular references (even in PHP 5.3). This is currently necessary
     * when using Propel in certain daemon or large-volumne/high-memory operations.
     *
     * @param boolean $deep Whether to also clear the references on all referrer objects.
     */
    public function clearAllReferences($deep = false)
    {
        if ($deep && !$this->alreadyInClearAllReferencesDeep) {
            $this->alreadyInClearAllReferencesDeep = true;

            $this->alreadyInClearAllReferencesDeep = false;
        } // if ($deep)

    }

    /**
     * return the string representation of this object
     *
     * @return string
     */
    public function __toString()
    {
        return (string) $this->exportTo(MutasiPeer::DEFAULT_STRING_FORMAT);
    }

    /**
     * return true is the object is in saving state
     *
     * @return boolean
     */
    public function isAlreadyInSave()
    {
        return $this->alreadyInSave;
    }

}
