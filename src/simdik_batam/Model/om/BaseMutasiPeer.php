<?php

namespace simdik_batam\Model\om;

use \BasePeer;
use \Criteria;
use \PDO;
use \PDOStatement;
use \Propel;
use \PropelException;
use \PropelPDO;
use simdik_batam\Model\Mutasi;
use simdik_batam\Model\MutasiPeer;
use simdik_batam\Model\map\MutasiTableMap;

/**
 * Base static class for performing query and update operations on the 'adm.mutasi' table.
 *
 * 
 *
 * @package propel.generator.simdik_batam.Model.om
 */
abstract class BaseMutasiPeer
{

    /** the default database name for this class */
    const DATABASE_NAME = 'dapodikdasmen_batam';

    /** the table name for this class */
    const TABLE_NAME = 'adm.mutasi';

    /** the related Propel class for this table */
    const OM_CLASS = 'simdik_batam\\Model\\Mutasi';

    /** the related TableMap class for this table */
    const TM_CLASS = 'MutasiTableMap';

    /** The total number of columns. */
    const NUM_COLUMNS = 27;

    /** The number of lazy-loaded columns. */
    const NUM_LAZY_LOAD_COLUMNS = 0;

    /** The number of columns to hydrate (NUM_COLUMNS - NUM_LAZY_LOAD_COLUMNS) */
    const NUM_HYDRATE_COLUMNS = 27;

    /** the column name for the mutasi_id field */
    const MUTASI_ID = 'adm.mutasi.mutasi_id';

    /** the column name for the peserta_didik_id field */
    const PESERTA_DIDIK_ID = 'adm.mutasi.peserta_didik_id';

    /** the column name for the soft_delete field */
    const SOFT_DELETE = 'adm.mutasi.soft_delete';

    /** the column name for the nomor_surat field */
    const NOMOR_SURAT = 'adm.mutasi.nomor_surat';

    /** the column name for the sekolah_id field */
    const SEKOLAH_ID = 'adm.mutasi.sekolah_id';

    /** the column name for the kode_wilayah_berangkat field */
    const KODE_WILAYAH_BERANGKAT = 'adm.mutasi.kode_wilayah_berangkat';

    /** the column name for the maksud_tujuan field */
    const MAKSUD_TUJUAN = 'adm.mutasi.maksud_tujuan';

    /** the column name for the nama_yang_diikuti field */
    const NAMA_YANG_DIIKUTI = 'adm.mutasi.nama_yang_diikuti';

    /** the column name for the hubungan_keluarga field */
    const HUBUNGAN_KELUARGA = 'adm.mutasi.hubungan_keluarga';

    /** the column name for the pekerjaan field */
    const PEKERJAAN = 'adm.mutasi.pekerjaan';

    /** the column name for the alamat_yang_diikuti field */
    const ALAMAT_YANG_DIIKUTI = 'adm.mutasi.alamat_yang_diikuti';

    /** the column name for the tanggal_berangkat field */
    const TANGGAL_BERANGKAT = 'adm.mutasi.tanggal_berangkat';

    /** the column name for the pengguna_id field */
    const PENGGUNA_ID = 'adm.mutasi.pengguna_id';

    /** the column name for the sekolah_id_tujuan field */
    const SEKOLAH_ID_TUJUAN = 'adm.mutasi.sekolah_id_tujuan';

    /** the column name for the nama field */
    const NAMA = 'adm.mutasi.nama';

    /** the column name for the tempat_lahir field */
    const TEMPAT_LAHIR = 'adm.mutasi.tempat_lahir';

    /** the column name for the tanggal_lahir field */
    const TANGGAL_LAHIR = 'adm.mutasi.tanggal_lahir';

    /** the column name for the kelas field */
    const KELAS = 'adm.mutasi.kelas';

    /** the column name for the NIS field */
    const NIS = 'adm.mutasi.NIS';

    /** the column name for the NISN field */
    const NISN = 'adm.mutasi.NISN';

    /** the column name for the nama_ayah field */
    const NAMA_AYAH = 'adm.mutasi.nama_ayah';

    /** the column name for the pekerjaan_id_ayah_str field */
    const PEKERJAAN_ID_AYAH_STR = 'adm.mutasi.pekerjaan_id_ayah_str';

    /** the column name for the alamat_jalan field */
    const ALAMAT_JALAN = 'adm.mutasi.alamat_jalan';

    /** the column name for the jenis_surat_id field */
    const JENIS_SURAT_ID = 'adm.mutasi.jenis_surat_id';

    /** the column name for the tanggal_surat field */
    const TANGGAL_SURAT = 'adm.mutasi.tanggal_surat';

    /** the column name for the keterangan_kepala_sekolah field */
    const KETERANGAN_KEPALA_SEKOLAH = 'adm.mutasi.keterangan_kepala_sekolah';

    /** the column name for the keterangan_lain field */
    const KETERANGAN_LAIN = 'adm.mutasi.keterangan_lain';

    /** The default string format for model objects of the related table **/
    const DEFAULT_STRING_FORMAT = 'YAML';

    /**
     * An identiy map to hold any loaded instances of Mutasi objects.
     * This must be public so that other peer classes can access this when hydrating from JOIN
     * queries.
     * @var        array Mutasi[]
     */
    public static $instances = array();


    /**
     * holds an array of fieldnames
     *
     * first dimension keys are the type constants
     * e.g. MutasiPeer::$fieldNames[MutasiPeer::TYPE_PHPNAME][0] = 'Id'
     */
    protected static $fieldNames = array (
        BasePeer::TYPE_PHPNAME => array ('MutasiId', 'PesertaDidikId', 'SoftDelete', 'NomorSurat', 'SekolahId', 'KodeWilayahBerangkat', 'MaksudTujuan', 'NamaYangDiikuti', 'HubunganKeluarga', 'Pekerjaan', 'AlamatYangDiikuti', 'TanggalBerangkat', 'PenggunaId', 'SekolahIdTujuan', 'Nama', 'TempatLahir', 'TanggalLahir', 'Kelas', 'Nis', 'Nisn', 'NamaAyah', 'PekerjaanIdAyahStr', 'AlamatJalan', 'JenisSuratId', 'TanggalSurat', 'KeteranganKepalaSekolah', 'KeteranganLain', ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('mutasiId', 'pesertaDidikId', 'softDelete', 'nomorSurat', 'sekolahId', 'kodeWilayahBerangkat', 'maksudTujuan', 'namaYangDiikuti', 'hubunganKeluarga', 'pekerjaan', 'alamatYangDiikuti', 'tanggalBerangkat', 'penggunaId', 'sekolahIdTujuan', 'nama', 'tempatLahir', 'tanggalLahir', 'kelas', 'nis', 'nisn', 'namaAyah', 'pekerjaanIdAyahStr', 'alamatJalan', 'jenisSuratId', 'tanggalSurat', 'keteranganKepalaSekolah', 'keteranganLain', ),
        BasePeer::TYPE_COLNAME => array (MutasiPeer::MUTASI_ID, MutasiPeer::PESERTA_DIDIK_ID, MutasiPeer::SOFT_DELETE, MutasiPeer::NOMOR_SURAT, MutasiPeer::SEKOLAH_ID, MutasiPeer::KODE_WILAYAH_BERANGKAT, MutasiPeer::MAKSUD_TUJUAN, MutasiPeer::NAMA_YANG_DIIKUTI, MutasiPeer::HUBUNGAN_KELUARGA, MutasiPeer::PEKERJAAN, MutasiPeer::ALAMAT_YANG_DIIKUTI, MutasiPeer::TANGGAL_BERANGKAT, MutasiPeer::PENGGUNA_ID, MutasiPeer::SEKOLAH_ID_TUJUAN, MutasiPeer::NAMA, MutasiPeer::TEMPAT_LAHIR, MutasiPeer::TANGGAL_LAHIR, MutasiPeer::KELAS, MutasiPeer::NIS, MutasiPeer::NISN, MutasiPeer::NAMA_AYAH, MutasiPeer::PEKERJAAN_ID_AYAH_STR, MutasiPeer::ALAMAT_JALAN, MutasiPeer::JENIS_SURAT_ID, MutasiPeer::TANGGAL_SURAT, MutasiPeer::KETERANGAN_KEPALA_SEKOLAH, MutasiPeer::KETERANGAN_LAIN, ),
        BasePeer::TYPE_RAW_COLNAME => array ('MUTASI_ID', 'PESERTA_DIDIK_ID', 'SOFT_DELETE', 'NOMOR_SURAT', 'SEKOLAH_ID', 'KODE_WILAYAH_BERANGKAT', 'MAKSUD_TUJUAN', 'NAMA_YANG_DIIKUTI', 'HUBUNGAN_KELUARGA', 'PEKERJAAN', 'ALAMAT_YANG_DIIKUTI', 'TANGGAL_BERANGKAT', 'PENGGUNA_ID', 'SEKOLAH_ID_TUJUAN', 'NAMA', 'TEMPAT_LAHIR', 'TANGGAL_LAHIR', 'KELAS', 'NIS', 'NISN', 'NAMA_AYAH', 'PEKERJAAN_ID_AYAH_STR', 'ALAMAT_JALAN', 'JENIS_SURAT_ID', 'TANGGAL_SURAT', 'KETERANGAN_KEPALA_SEKOLAH', 'KETERANGAN_LAIN', ),
        BasePeer::TYPE_FIELDNAME => array ('mutasi_id', 'peserta_didik_id', 'soft_delete', 'nomor_surat', 'sekolah_id', 'kode_wilayah_berangkat', 'maksud_tujuan', 'nama_yang_diikuti', 'hubungan_keluarga', 'pekerjaan', 'alamat_yang_diikuti', 'tanggal_berangkat', 'pengguna_id', 'sekolah_id_tujuan', 'nama', 'tempat_lahir', 'tanggal_lahir', 'kelas', 'NIS', 'NISN', 'nama_ayah', 'pekerjaan_id_ayah_str', 'alamat_jalan', 'jenis_surat_id', 'tanggal_surat', 'keterangan_kepala_sekolah', 'keterangan_lain', ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, )
    );

    /**
     * holds an array of keys for quick access to the fieldnames array
     *
     * first dimension keys are the type constants
     * e.g. MutasiPeer::$fieldNames[BasePeer::TYPE_PHPNAME]['Id'] = 0
     */
    protected static $fieldKeys = array (
        BasePeer::TYPE_PHPNAME => array ('MutasiId' => 0, 'PesertaDidikId' => 1, 'SoftDelete' => 2, 'NomorSurat' => 3, 'SekolahId' => 4, 'KodeWilayahBerangkat' => 5, 'MaksudTujuan' => 6, 'NamaYangDiikuti' => 7, 'HubunganKeluarga' => 8, 'Pekerjaan' => 9, 'AlamatYangDiikuti' => 10, 'TanggalBerangkat' => 11, 'PenggunaId' => 12, 'SekolahIdTujuan' => 13, 'Nama' => 14, 'TempatLahir' => 15, 'TanggalLahir' => 16, 'Kelas' => 17, 'Nis' => 18, 'Nisn' => 19, 'NamaAyah' => 20, 'PekerjaanIdAyahStr' => 21, 'AlamatJalan' => 22, 'JenisSuratId' => 23, 'TanggalSurat' => 24, 'KeteranganKepalaSekolah' => 25, 'KeteranganLain' => 26, ),
        BasePeer::TYPE_STUDLYPHPNAME => array ('mutasiId' => 0, 'pesertaDidikId' => 1, 'softDelete' => 2, 'nomorSurat' => 3, 'sekolahId' => 4, 'kodeWilayahBerangkat' => 5, 'maksudTujuan' => 6, 'namaYangDiikuti' => 7, 'hubunganKeluarga' => 8, 'pekerjaan' => 9, 'alamatYangDiikuti' => 10, 'tanggalBerangkat' => 11, 'penggunaId' => 12, 'sekolahIdTujuan' => 13, 'nama' => 14, 'tempatLahir' => 15, 'tanggalLahir' => 16, 'kelas' => 17, 'nis' => 18, 'nisn' => 19, 'namaAyah' => 20, 'pekerjaanIdAyahStr' => 21, 'alamatJalan' => 22, 'jenisSuratId' => 23, 'tanggalSurat' => 24, 'keteranganKepalaSekolah' => 25, 'keteranganLain' => 26, ),
        BasePeer::TYPE_COLNAME => array (MutasiPeer::MUTASI_ID => 0, MutasiPeer::PESERTA_DIDIK_ID => 1, MutasiPeer::SOFT_DELETE => 2, MutasiPeer::NOMOR_SURAT => 3, MutasiPeer::SEKOLAH_ID => 4, MutasiPeer::KODE_WILAYAH_BERANGKAT => 5, MutasiPeer::MAKSUD_TUJUAN => 6, MutasiPeer::NAMA_YANG_DIIKUTI => 7, MutasiPeer::HUBUNGAN_KELUARGA => 8, MutasiPeer::PEKERJAAN => 9, MutasiPeer::ALAMAT_YANG_DIIKUTI => 10, MutasiPeer::TANGGAL_BERANGKAT => 11, MutasiPeer::PENGGUNA_ID => 12, MutasiPeer::SEKOLAH_ID_TUJUAN => 13, MutasiPeer::NAMA => 14, MutasiPeer::TEMPAT_LAHIR => 15, MutasiPeer::TANGGAL_LAHIR => 16, MutasiPeer::KELAS => 17, MutasiPeer::NIS => 18, MutasiPeer::NISN => 19, MutasiPeer::NAMA_AYAH => 20, MutasiPeer::PEKERJAAN_ID_AYAH_STR => 21, MutasiPeer::ALAMAT_JALAN => 22, MutasiPeer::JENIS_SURAT_ID => 23, MutasiPeer::TANGGAL_SURAT => 24, MutasiPeer::KETERANGAN_KEPALA_SEKOLAH => 25, MutasiPeer::KETERANGAN_LAIN => 26, ),
        BasePeer::TYPE_RAW_COLNAME => array ('MUTASI_ID' => 0, 'PESERTA_DIDIK_ID' => 1, 'SOFT_DELETE' => 2, 'NOMOR_SURAT' => 3, 'SEKOLAH_ID' => 4, 'KODE_WILAYAH_BERANGKAT' => 5, 'MAKSUD_TUJUAN' => 6, 'NAMA_YANG_DIIKUTI' => 7, 'HUBUNGAN_KELUARGA' => 8, 'PEKERJAAN' => 9, 'ALAMAT_YANG_DIIKUTI' => 10, 'TANGGAL_BERANGKAT' => 11, 'PENGGUNA_ID' => 12, 'SEKOLAH_ID_TUJUAN' => 13, 'NAMA' => 14, 'TEMPAT_LAHIR' => 15, 'TANGGAL_LAHIR' => 16, 'KELAS' => 17, 'NIS' => 18, 'NISN' => 19, 'NAMA_AYAH' => 20, 'PEKERJAAN_ID_AYAH_STR' => 21, 'ALAMAT_JALAN' => 22, 'JENIS_SURAT_ID' => 23, 'TANGGAL_SURAT' => 24, 'KETERANGAN_KEPALA_SEKOLAH' => 25, 'KETERANGAN_LAIN' => 26, ),
        BasePeer::TYPE_FIELDNAME => array ('mutasi_id' => 0, 'peserta_didik_id' => 1, 'soft_delete' => 2, 'nomor_surat' => 3, 'sekolah_id' => 4, 'kode_wilayah_berangkat' => 5, 'maksud_tujuan' => 6, 'nama_yang_diikuti' => 7, 'hubungan_keluarga' => 8, 'pekerjaan' => 9, 'alamat_yang_diikuti' => 10, 'tanggal_berangkat' => 11, 'pengguna_id' => 12, 'sekolah_id_tujuan' => 13, 'nama' => 14, 'tempat_lahir' => 15, 'tanggal_lahir' => 16, 'kelas' => 17, 'NIS' => 18, 'NISN' => 19, 'nama_ayah' => 20, 'pekerjaan_id_ayah_str' => 21, 'alamat_jalan' => 22, 'jenis_surat_id' => 23, 'tanggal_surat' => 24, 'keterangan_kepala_sekolah' => 25, 'keterangan_lain' => 26, ),
        BasePeer::TYPE_NUM => array (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, )
    );

    /**
     * Translates a fieldname to another type
     *
     * @param      string $name field name
     * @param      string $fromType One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                         BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @param      string $toType   One of the class type constants
     * @return string          translated name of the field.
     * @throws PropelException - if the specified name could not be found in the fieldname mappings.
     */
    public static function translateFieldName($name, $fromType, $toType)
    {
        $toNames = MutasiPeer::getFieldNames($toType);
        $key = isset(MutasiPeer::$fieldKeys[$fromType][$name]) ? MutasiPeer::$fieldKeys[$fromType][$name] : null;
        if ($key === null) {
            throw new PropelException("'$name' could not be found in the field names of type '$fromType'. These are: " . print_r(MutasiPeer::$fieldKeys[$fromType], true));
        }

        return $toNames[$key];
    }

    /**
     * Returns an array of field names.
     *
     * @param      string $type The type of fieldnames to return:
     *                      One of the class type constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME
     *                      BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM
     * @return array           A list of field names
     * @throws PropelException - if the type is not valid.
     */
    public static function getFieldNames($type = BasePeer::TYPE_PHPNAME)
    {
        if (!array_key_exists($type, MutasiPeer::$fieldNames)) {
            throw new PropelException('Method getFieldNames() expects the parameter $type to be one of the class constants BasePeer::TYPE_PHPNAME, BasePeer::TYPE_STUDLYPHPNAME, BasePeer::TYPE_COLNAME, BasePeer::TYPE_FIELDNAME, BasePeer::TYPE_NUM. ' . $type . ' was given.');
        }

        return MutasiPeer::$fieldNames[$type];
    }

    /**
     * Convenience method which changes table.column to alias.column.
     *
     * Using this method you can maintain SQL abstraction while using column aliases.
     * <code>
     *		$c->addAlias("alias1", TablePeer::TABLE_NAME);
     *		$c->addJoin(TablePeer::alias("alias1", TablePeer::PRIMARY_KEY_COLUMN), TablePeer::PRIMARY_KEY_COLUMN);
     * </code>
     * @param      string $alias The alias for the current table.
     * @param      string $column The column name for current table. (i.e. MutasiPeer::COLUMN_NAME).
     * @return string
     */
    public static function alias($alias, $column)
    {
        return str_replace(MutasiPeer::TABLE_NAME.'.', $alias.'.', $column);
    }

    /**
     * Add all the columns needed to create a new object.
     *
     * Note: any columns that were marked with lazyLoad="true" in the
     * XML schema will not be added to the select list and only loaded
     * on demand.
     *
     * @param      Criteria $criteria object containing the columns to add.
     * @param      string   $alias    optional table alias
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function addSelectColumns(Criteria $criteria, $alias = null)
    {
        if (null === $alias) {
            $criteria->addSelectColumn(MutasiPeer::MUTASI_ID);
            $criteria->addSelectColumn(MutasiPeer::PESERTA_DIDIK_ID);
            $criteria->addSelectColumn(MutasiPeer::SOFT_DELETE);
            $criteria->addSelectColumn(MutasiPeer::NOMOR_SURAT);
            $criteria->addSelectColumn(MutasiPeer::SEKOLAH_ID);
            $criteria->addSelectColumn(MutasiPeer::KODE_WILAYAH_BERANGKAT);
            $criteria->addSelectColumn(MutasiPeer::MAKSUD_TUJUAN);
            $criteria->addSelectColumn(MutasiPeer::NAMA_YANG_DIIKUTI);
            $criteria->addSelectColumn(MutasiPeer::HUBUNGAN_KELUARGA);
            $criteria->addSelectColumn(MutasiPeer::PEKERJAAN);
            $criteria->addSelectColumn(MutasiPeer::ALAMAT_YANG_DIIKUTI);
            $criteria->addSelectColumn(MutasiPeer::TANGGAL_BERANGKAT);
            $criteria->addSelectColumn(MutasiPeer::PENGGUNA_ID);
            $criteria->addSelectColumn(MutasiPeer::SEKOLAH_ID_TUJUAN);
            $criteria->addSelectColumn(MutasiPeer::NAMA);
            $criteria->addSelectColumn(MutasiPeer::TEMPAT_LAHIR);
            $criteria->addSelectColumn(MutasiPeer::TANGGAL_LAHIR);
            $criteria->addSelectColumn(MutasiPeer::KELAS);
            $criteria->addSelectColumn(MutasiPeer::NIS);
            $criteria->addSelectColumn(MutasiPeer::NISN);
            $criteria->addSelectColumn(MutasiPeer::NAMA_AYAH);
            $criteria->addSelectColumn(MutasiPeer::PEKERJAAN_ID_AYAH_STR);
            $criteria->addSelectColumn(MutasiPeer::ALAMAT_JALAN);
            $criteria->addSelectColumn(MutasiPeer::JENIS_SURAT_ID);
            $criteria->addSelectColumn(MutasiPeer::TANGGAL_SURAT);
            $criteria->addSelectColumn(MutasiPeer::KETERANGAN_KEPALA_SEKOLAH);
            $criteria->addSelectColumn(MutasiPeer::KETERANGAN_LAIN);
        } else {
            $criteria->addSelectColumn($alias . '.mutasi_id');
            $criteria->addSelectColumn($alias . '.peserta_didik_id');
            $criteria->addSelectColumn($alias . '.soft_delete');
            $criteria->addSelectColumn($alias . '.nomor_surat');
            $criteria->addSelectColumn($alias . '.sekolah_id');
            $criteria->addSelectColumn($alias . '.kode_wilayah_berangkat');
            $criteria->addSelectColumn($alias . '.maksud_tujuan');
            $criteria->addSelectColumn($alias . '.nama_yang_diikuti');
            $criteria->addSelectColumn($alias . '.hubungan_keluarga');
            $criteria->addSelectColumn($alias . '.pekerjaan');
            $criteria->addSelectColumn($alias . '.alamat_yang_diikuti');
            $criteria->addSelectColumn($alias . '.tanggal_berangkat');
            $criteria->addSelectColumn($alias . '.pengguna_id');
            $criteria->addSelectColumn($alias . '.sekolah_id_tujuan');
            $criteria->addSelectColumn($alias . '.nama');
            $criteria->addSelectColumn($alias . '.tempat_lahir');
            $criteria->addSelectColumn($alias . '.tanggal_lahir');
            $criteria->addSelectColumn($alias . '.kelas');
            $criteria->addSelectColumn($alias . '.NIS');
            $criteria->addSelectColumn($alias . '.NISN');
            $criteria->addSelectColumn($alias . '.nama_ayah');
            $criteria->addSelectColumn($alias . '.pekerjaan_id_ayah_str');
            $criteria->addSelectColumn($alias . '.alamat_jalan');
            $criteria->addSelectColumn($alias . '.jenis_surat_id');
            $criteria->addSelectColumn($alias . '.tanggal_surat');
            $criteria->addSelectColumn($alias . '.keterangan_kepala_sekolah');
            $criteria->addSelectColumn($alias . '.keterangan_lain');
        }
    }

    /**
     * Returns the number of rows matching criteria.
     *
     * @param      Criteria $criteria
     * @param      boolean $distinct Whether to select only distinct columns; deprecated: use Criteria->setDistinct() instead.
     * @param      PropelPDO $con
     * @return int Number of matching rows.
     */
    public static function doCount(Criteria $criteria, $distinct = false, PropelPDO $con = null)
    {
        // we may modify criteria, so copy it first
        $criteria = clone $criteria;

        // We need to set the primary table name, since in the case that there are no WHERE columns
        // it will be impossible for the BasePeer::createSelectSql() method to determine which
        // tables go into the FROM clause.
        $criteria->setPrimaryTableName(MutasiPeer::TABLE_NAME);

        if ($distinct && !in_array(Criteria::DISTINCT, $criteria->getSelectModifiers())) {
            $criteria->setDistinct();
        }

        if (!$criteria->hasSelectClause()) {
            MutasiPeer::addSelectColumns($criteria);
        }

        $criteria->clearOrderByColumns(); // ORDER BY won't ever affect the count
        $criteria->setDbName(MutasiPeer::DATABASE_NAME); // Set the correct dbName

        if ($con === null) {
            $con = Propel::getConnection(MutasiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }
        // BasePeer returns a PDOStatement
        $stmt = BasePeer::doCount($criteria, $con);

        if ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $count = (int) $row[0];
        } else {
            $count = 0; // no rows returned; we infer that means 0 matches.
        }
        $stmt->closeCursor();

        return $count;
    }
    /**
     * Selects one object from the DB.
     *
     * @param      Criteria $criteria object used to create the SELECT statement.
     * @param      PropelPDO $con
     * @return                 Mutasi
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelectOne(Criteria $criteria, PropelPDO $con = null)
    {
        $critcopy = clone $criteria;
        $critcopy->setLimit(1);
        $objects = MutasiPeer::doSelect($critcopy, $con);
        if ($objects) {
            return $objects[0];
        }

        return null;
    }
    /**
     * Selects several row from the DB.
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con
     * @return array           Array of selected Objects
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doSelect(Criteria $criteria, PropelPDO $con = null)
    {
        return MutasiPeer::populateObjects(MutasiPeer::doSelectStmt($criteria, $con));
    }
    /**
     * Prepares the Criteria object and uses the parent doSelect() method to execute a PDOStatement.
     *
     * Use this method directly if you want to work with an executed statement directly (for example
     * to perform your own object hydration).
     *
     * @param      Criteria $criteria The Criteria object used to build the SELECT statement.
     * @param      PropelPDO $con The connection to use
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return PDOStatement The executed PDOStatement object.
     * @see        BasePeer::doSelect()
     */
    public static function doSelectStmt(Criteria $criteria, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(MutasiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        if (!$criteria->hasSelectClause()) {
            $criteria = clone $criteria;
            MutasiPeer::addSelectColumns($criteria);
        }

        // Set the correct dbName
        $criteria->setDbName(MutasiPeer::DATABASE_NAME);

        // BasePeer returns a PDOStatement
        return BasePeer::doSelect($criteria, $con);
    }
    /**
     * Adds an object to the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doSelect*()
     * methods in your stub classes -- you may need to explicitly add objects
     * to the cache in order to ensure that the same objects are always returned by doSelect*()
     * and retrieveByPK*() calls.
     *
     * @param      Mutasi $obj A Mutasi object.
     * @param      string $key (optional) key to use for instance map (for performance boost if key was already calculated externally).
     */
    public static function addInstanceToPool($obj, $key = null)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if ($key === null) {
                $key = (string) $obj->getMutasiId();
            } // if key === null
            MutasiPeer::$instances[$key] = $obj;
        }
    }

    /**
     * Removes an object from the instance pool.
     *
     * Propel keeps cached copies of objects in an instance pool when they are retrieved
     * from the database.  In some cases -- especially when you override doDelete
     * methods in your stub classes -- you may need to explicitly remove objects
     * from the cache in order to prevent returning objects that no longer exist.
     *
     * @param      mixed $value A Mutasi object or a primary key value.
     *
     * @return void
     * @throws PropelException - if the value is invalid.
     */
    public static function removeInstanceFromPool($value)
    {
        if (Propel::isInstancePoolingEnabled() && $value !== null) {
            if (is_object($value) && $value instanceof Mutasi) {
                $key = (string) $value->getMutasiId();
            } elseif (is_scalar($value)) {
                // assume we've been passed a primary key
                $key = (string) $value;
            } else {
                $e = new PropelException("Invalid value passed to removeInstanceFromPool().  Expected primary key or Mutasi object; got " . (is_object($value) ? get_class($value) . ' object.' : var_export($value,true)));
                throw $e;
            }

            unset(MutasiPeer::$instances[$key]);
        }
    } // removeInstanceFromPool()

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      string $key The key (@see getPrimaryKeyHash()) for this instance.
     * @return   Mutasi Found object or null if 1) no instance exists for specified key or 2) instance pooling has been disabled.
     * @see        getPrimaryKeyHash()
     */
    public static function getInstanceFromPool($key)
    {
        if (Propel::isInstancePoolingEnabled()) {
            if (isset(MutasiPeer::$instances[$key])) {
                return MutasiPeer::$instances[$key];
            }
        }

        return null; // just to be explicit
    }
    
    /**
     * Clear the instance pool.
     *
     * @return void
     */
    public static function clearInstancePool($and_clear_all_references = false)
    {
      if ($and_clear_all_references)
      {
        foreach (MutasiPeer::$instances as $instance)
        {
          $instance->clearAllReferences(true);
        }
      }
        MutasiPeer::$instances = array();
    }
    
    /**
     * Method to invalidate the instance pool of all tables related to adm.mutasi
     * by a foreign key with ON DELETE CASCADE
     */
    public static function clearRelatedInstancePool()
    {
    }

    /**
     * Retrieves a string version of the primary key from the DB resultset row that can be used to uniquely identify a row in this table.
     *
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, a serialize()d version of the primary key will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return string A string version of PK or null if the components of primary key in result array are all null.
     */
    public static function getPrimaryKeyHashFromRow($row, $startcol = 0)
    {
        // If the PK cannot be derived from the row, return null.
        if ($row[$startcol] === null) {
            return null;
        }

        return (string) $row[$startcol];
    }

    /**
     * Retrieves the primary key from the DB resultset row
     * For tables with a single-column primary key, that simple pkey value will be returned.  For tables with
     * a multi-column primary key, an array of the primary key columns will be returned.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @return mixed The primary key of the row
     */
    public static function getPrimaryKeyFromRow($row, $startcol = 0)
    {

        return (string) $row[$startcol];
    }
    
    /**
     * The returned array will contain objects of the default type or
     * objects that inherit from the default.
     *
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function populateObjects(PDOStatement $stmt)
    {
        $results = array();
    
        // set the class once to avoid overhead in the loop
        $cls = MutasiPeer::getOMClass();
        // populate the object(s)
        while ($row = $stmt->fetch(PDO::FETCH_NUM)) {
            $key = MutasiPeer::getPrimaryKeyHashFromRow($row, 0);
            if (null !== ($obj = MutasiPeer::getInstanceFromPool($key))) {
                // We no longer rehydrate the object, since this can cause data loss.
                // See http://www.propelorm.org/ticket/509
                // $obj->hydrate($row, 0, true); // rehydrate
                $results[] = $obj;
            } else {
                $obj = new $cls();
                $obj->hydrate($row);
                $results[] = $obj;
                MutasiPeer::addInstanceToPool($obj, $key);
            } // if key exists
        }
        $stmt->closeCursor();

        return $results;
    }
    /**
     * Populates an object of the default type or an object that inherit from the default.
     *
     * @param      array $row PropelPDO resultset row.
     * @param      int $startcol The 0-based offset for reading from the resultset row.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     * @return array (Mutasi object, last column rank)
     */
    public static function populateObject($row, $startcol = 0)
    {
        $key = MutasiPeer::getPrimaryKeyHashFromRow($row, $startcol);
        if (null !== ($obj = MutasiPeer::getInstanceFromPool($key))) {
            // We no longer rehydrate the object, since this can cause data loss.
            // See http://www.propelorm.org/ticket/509
            // $obj->hydrate($row, $startcol, true); // rehydrate
            $col = $startcol + MutasiPeer::NUM_HYDRATE_COLUMNS;
        } else {
            $cls = MutasiPeer::OM_CLASS;
            $obj = new $cls();
            $col = $obj->hydrate($row, $startcol);
            MutasiPeer::addInstanceToPool($obj, $key);
        }

        return array($obj, $col);
    }

    /**
     * Returns the TableMap related to this peer.
     * This method is not needed for general use but a specific application could have a need.
     * @return TableMap
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function getTableMap()
    {
        return Propel::getDatabaseMap(MutasiPeer::DATABASE_NAME)->getTable(MutasiPeer::TABLE_NAME);
    }

    /**
     * Add a TableMap instance to the database for this peer class.
     */
    public static function buildTableMap()
    {
      $dbMap = Propel::getDatabaseMap(BaseMutasiPeer::DATABASE_NAME);
      if (!$dbMap->hasTable(BaseMutasiPeer::TABLE_NAME)) {
        $dbMap->addTableObject(new MutasiTableMap());
      }
    }

    /**
     * The class that the Peer will make instances of.
     *
     *
     * @return string ClassName
     */
    public static function getOMClass()
    {
        return MutasiPeer::OM_CLASS;
    }

    /**
     * Performs an INSERT on the database, given a Mutasi or Criteria object.
     *
     * @param      mixed $values Criteria or Mutasi object containing data that is used to create the INSERT statement.
     * @param      PropelPDO $con the PropelPDO connection to use
     * @return mixed           The new primary key.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doInsert($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(MutasiPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity
        } else {
            $criteria = $values->buildCriteria(); // build Criteria from Mutasi object
        }


        // Set the correct dbName
        $criteria->setDbName(MutasiPeer::DATABASE_NAME);

        try {
            // use transaction because $criteria could contain info
            // for more than one table (I guess, conceivably)
            $con->beginTransaction();
            $pk = BasePeer::doInsert($criteria, $con);
            $con->commit();
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }

        return $pk;
    }

    /**
     * Performs an UPDATE on the database, given a Mutasi or Criteria object.
     *
     * @param      mixed $values Criteria or Mutasi object containing data that is used to create the UPDATE statement.
     * @param      PropelPDO $con The connection to use (specify PropelPDO connection object to exert more control over transactions).
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function doUpdate($values, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(MutasiPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        $selectCriteria = new Criteria(MutasiPeer::DATABASE_NAME);

        if ($values instanceof Criteria) {
            $criteria = clone $values; // rename for clarity

            $comparison = $criteria->getComparison(MutasiPeer::MUTASI_ID);
            $value = $criteria->remove(MutasiPeer::MUTASI_ID);
            if ($value) {
                $selectCriteria->add(MutasiPeer::MUTASI_ID, $value, $comparison);
            } else {
                $selectCriteria->setPrimaryTableName(MutasiPeer::TABLE_NAME);
            }

        } else { // $values is Mutasi object
            $criteria = $values->buildCriteria(); // gets full criteria
            $selectCriteria = $values->buildPkeyCriteria(); // gets criteria w/ primary key(s)
        }

        // set the correct dbName
        $criteria->setDbName(MutasiPeer::DATABASE_NAME);

        return BasePeer::doUpdate($selectCriteria, $criteria, $con);
    }

    /**
     * Deletes all rows from the adm.mutasi table.
     *
     * @param      PropelPDO $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).
     * @throws PropelException
     */
    public static function doDeleteAll(PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(MutasiPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }
        $affectedRows = 0; // initialize var to track total num of affected rows
        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            $affectedRows += BasePeer::doDeleteAll(MutasiPeer::TABLE_NAME, $con, MutasiPeer::DATABASE_NAME);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            MutasiPeer::clearInstancePool();
            MutasiPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Performs a DELETE on the database, given a Mutasi or Criteria object OR a primary key value.
     *
     * @param      mixed $values Criteria or Mutasi object or primary key or array of primary keys
     *              which is used to create the DELETE statement
     * @param      PropelPDO $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *				if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
     public static function doDelete($values, PropelPDO $con = null)
     {
        if ($con === null) {
            $con = Propel::getConnection(MutasiPeer::DATABASE_NAME, Propel::CONNECTION_WRITE);
        }

        if ($values instanceof Criteria) {
            // invalidate the cache for all objects of this type, since we have no
            // way of knowing (without running a query) what objects should be invalidated
            // from the cache based on this Criteria.
            MutasiPeer::clearInstancePool();
            // rename for clarity
            $criteria = clone $values;
        } elseif ($values instanceof Mutasi) { // it's a model object
            // invalidate the cache for this single object
            MutasiPeer::removeInstanceFromPool($values);
            // create criteria based on pk values
            $criteria = $values->buildPkeyCriteria();
        } else { // it's a primary key, or an array of pks
            $criteria = new Criteria(MutasiPeer::DATABASE_NAME);
            $criteria->add(MutasiPeer::MUTASI_ID, (array) $values, Criteria::IN);
            // invalidate the cache for this object(s)
            foreach ((array) $values as $singleval) {
                MutasiPeer::removeInstanceFromPool($singleval);
            }
        }

        // Set the correct dbName
        $criteria->setDbName(MutasiPeer::DATABASE_NAME);

        $affectedRows = 0; // initialize var to track total num of affected rows

        try {
            // use transaction because $criteria could contain info
            // for more than one table or we could emulating ON DELETE CASCADE, etc.
            $con->beginTransaction();
            
            $affectedRows += BasePeer::doDelete($criteria, $con);
            MutasiPeer::clearRelatedInstancePool();
            $con->commit();

            return $affectedRows;
        } catch (PropelException $e) {
            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Validates all modified columns of given Mutasi object.
     * If parameter $columns is either a single column name or an array of column names
     * than only those columns are validated.
     *
     * NOTICE: This does not apply to primary or foreign keys for now.
     *
     * @param      Mutasi $obj The object to validate.
     * @param      mixed $cols Column name or array of column names.
     *
     * @return mixed TRUE if all columns are valid or the error message of the first invalid column.
     */
    public static function doValidate($obj, $cols = null)
    {
        $columns = array();

        if ($cols) {
            $dbMap = Propel::getDatabaseMap(MutasiPeer::DATABASE_NAME);
            $tableMap = $dbMap->getTable(MutasiPeer::TABLE_NAME);

            if (! is_array($cols)) {
                $cols = array($cols);
            }

            foreach ($cols as $colName) {
                if ($tableMap->hasColumn($colName)) {
                    $get = 'get' . $tableMap->getColumn($colName)->getPhpName();
                    $columns[$colName] = $obj->$get();
                }
            }
        } else {

        }

        return BasePeer::doValidate(MutasiPeer::DATABASE_NAME, MutasiPeer::TABLE_NAME, $columns);
    }

    /**
     * Retrieve a single object by pkey.
     *
     * @param      string $pk the primary key.
     * @param      PropelPDO $con the connection to use
     * @return Mutasi
     */
    public static function retrieveByPK($pk, PropelPDO $con = null)
    {

        if (null !== ($obj = MutasiPeer::getInstanceFromPool((string) $pk))) {
            return $obj;
        }

        if ($con === null) {
            $con = Propel::getConnection(MutasiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $criteria = new Criteria(MutasiPeer::DATABASE_NAME);
        $criteria->add(MutasiPeer::MUTASI_ID, $pk);

        $v = MutasiPeer::doSelect($criteria, $con);

        return !empty($v) > 0 ? $v[0] : null;
    }

    /**
     * Retrieve multiple objects by pkey.
     *
     * @param      array $pks List of primary keys
     * @param      PropelPDO $con the connection to use
     * @return Mutasi[]
     * @throws PropelException Any exceptions caught during processing will be
     *		 rethrown wrapped into a PropelException.
     */
    public static function retrieveByPKs($pks, PropelPDO $con = null)
    {
        if ($con === null) {
            $con = Propel::getConnection(MutasiPeer::DATABASE_NAME, Propel::CONNECTION_READ);
        }

        $objs = null;
        if (empty($pks)) {
            $objs = array();
        } else {
            $criteria = new Criteria(MutasiPeer::DATABASE_NAME);
            $criteria->add(MutasiPeer::MUTASI_ID, $pks, Criteria::IN);
            $objs = MutasiPeer::doSelect($criteria, $con);
        }

        return $objs;
    }

} // BaseMutasiPeer

// This is the static code needed to register the TableMap for this table with the main Propel class.
//
BaseMutasiPeer::buildTableMap();

