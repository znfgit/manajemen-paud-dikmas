<?php

namespace simdik_batam\Model;

use simdik_batam\Model\om\BaseBlockgrant;


/**
 * Skeleton subclass for representing a row from the 'blockgrant' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.simdik_batam.Model
 */
class Blockgrant extends BaseBlockgrant
{
}
