<?php

namespace simdik_batam\Model;

use simdik_batam\Model\om\BaseRekapRombelPerJumlah;


/**
 * Skeleton subclass for representing a row from the 'adm.rekap_rombel_per_jumlah' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.simdik_batam.Model
 */
class RekapRombelPerJumlah extends BaseRekapRombelPerJumlah
{
}
