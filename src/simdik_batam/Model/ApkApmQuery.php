<?php

namespace simdik_batam\Model;

use simdik_batam\Model\om\BaseApkApmQuery;


/**
 * Skeleton subclass for performing query and update operations on the 'adm.apk_apm' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.simdik_batam.Model
 */
class ApkApmQuery extends BaseApkApmQuery
{
}
