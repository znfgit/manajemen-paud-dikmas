<?php

namespace simdik_batam\Model;

use simdik_batam\Model\om\BaseJenisSurat;


/**
 * Skeleton subclass for representing a row from the 'adm.jenis_surat' table.
 *
 * 
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 * @package    propel.generator.simdik_batam.Model
 */
class JenisSurat extends BaseJenisSurat
{
}
