<?php

namespace simdik_batam\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'rekap_sekolah' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.simdik_batam.Model.map
 */
class RekapSekolahTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'simdik_batam.Model.map.RekapSekolahTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('rekap_sekolah');
        $this->setPhpName('RekapSekolah');
        $this->setClassname('simdik_batam\\Model\\RekapSekolah');
        $this->setPackage('simdik_batam.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('kode_wilayah', 'KodeWilayah', 'CHAR', true, 7, null);
        $this->addColumn('wilayah1', 'Wilayah1', 'CHAR', true, 7, null);
        $this->addColumn('wilayah2', 'Wilayah2', 'CHAR', true, 7, null);
        $this->addColumn('wilayah3', 'Wilayah3', 'CHAR', true, 7, null);
        $this->addColumn('bentuk_pendidikan_id', 'BentukPendidikanId', 'SMALLINT', true, 2, null);
        $this->addColumn('wilayah3_nama', 'Wilayah3Nama', 'VARCHAR', true, 80, null);
        $this->addColumn('wilayah2_nama', 'Wilayah2Nama', 'VARCHAR', true, 80, null);
        $this->addColumn('wilayah1_nama', 'Wilayah1Nama', 'VARCHAR', true, 80, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 50, null);
        $this->addColumn('Negeri', 'Negeri', 'INTEGER', false, 4, null);
        $this->addColumn('Swasta', 'Swasta', 'INTEGER', false, 4, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // RekapSekolahTableMap
