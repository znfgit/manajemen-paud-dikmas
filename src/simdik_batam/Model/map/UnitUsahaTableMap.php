<?php

namespace simdik_batam\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'unit_usaha' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.simdik_batam.Model.map
 */
class UnitUsahaTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'simdik_batam.Model.map.UnitUsahaTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('unit_usaha');
        $this->setPhpName('UnitUsaha');
        $this->setClassname('simdik_batam\\Model\\UnitUsaha');
        $this->setPackage('simdik_batam.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('unit_usaha_id', 'UnitUsahaId', 'CHAR', true, 16, null);
        $this->addForeignKey('kelompok_usaha_id', 'KelompokUsahaId', 'CHAR', 'ref.kelompok_usaha', 'kelompok_usaha_id', true, 8, null);
        $this->addForeignKey('kelompok_usaha_id', 'KelompokUsahaId', 'CHAR', 'ref.kelompok_usaha', 'kelompok_usaha_id', true, 8, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addColumn('nama_unit_usaha', 'NamaUnitUsaha', 'VARCHAR', true, 80, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SekolahRelatedBySekolahId', 'simdik_batam\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'simdik_batam\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('KelompokUsahaRelatedByKelompokUsahaId', 'simdik_batam\\Model\\KelompokUsaha', RelationMap::MANY_TO_ONE, array('kelompok_usaha_id' => 'kelompok_usaha_id', ), null, null);
        $this->addRelation('KelompokUsahaRelatedByKelompokUsahaId', 'simdik_batam\\Model\\KelompokUsaha', RelationMap::MANY_TO_ONE, array('kelompok_usaha_id' => 'kelompok_usaha_id', ), null, null);
        $this->addRelation('UnitUsahaKerjasamaRelatedByUnitUsahaId', 'simdik_batam\\Model\\UnitUsahaKerjasama', RelationMap::ONE_TO_MANY, array('unit_usaha_id' => 'unit_usaha_id', ), null, null, 'UnitUsahaKerjasamasRelatedByUnitUsahaId');
        $this->addRelation('UnitUsahaKerjasamaRelatedByUnitUsahaId', 'simdik_batam\\Model\\UnitUsahaKerjasama', RelationMap::ONE_TO_MANY, array('unit_usaha_id' => 'unit_usaha_id', ), null, null, 'UnitUsahaKerjasamasRelatedByUnitUsahaId');
    } // buildRelations()

} // UnitUsahaTableMap
