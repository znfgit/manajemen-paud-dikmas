<?php

namespace simdik_batam\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.errortype' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.simdik_batam.Model.map
 */
class ErrortypeTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'simdik_batam.Model.map.ErrortypeTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.errortype');
        $this->setPhpName('Errortype');
        $this->setClassname('simdik_batam\\Model\\Errortype');
        $this->setPackage('simdik_batam.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('idtype', 'Idtype', 'INTEGER', true, 4, null);
        $this->addColumn('kategori_error', 'KategoriError', 'INTEGER', false, 4, null);
        $this->addColumn('keterangan', 'Keterangan', 'VARCHAR', false, 255, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('VldRwyKepangkatanRelatedByIdtype', 'simdik_batam\\Model\\VldRwyKepangkatan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwyKepangkatansRelatedByIdtype');
        $this->addRelation('VldRwyKepangkatanRelatedByIdtype', 'simdik_batam\\Model\\VldRwyKepangkatan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwyKepangkatansRelatedByIdtype');
        $this->addRelation('VldRwyFungsionalRelatedByIdtype', 'simdik_batam\\Model\\VldRwyFungsional', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwyFungsionalsRelatedByIdtype');
        $this->addRelation('VldRwyFungsionalRelatedByIdtype', 'simdik_batam\\Model\\VldRwyFungsional', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwyFungsionalsRelatedByIdtype');
        $this->addRelation('VldRombelRelatedByIdtype', 'simdik_batam\\Model\\VldRombel', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRombelsRelatedByIdtype');
        $this->addRelation('VldRombelRelatedByIdtype', 'simdik_batam\\Model\\VldRombel', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRombelsRelatedByIdtype');
        $this->addRelation('VldPtkRelatedByIdtype', 'simdik_batam\\Model\\VldPtk', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPtksRelatedByIdtype');
        $this->addRelation('VldPtkRelatedByIdtype', 'simdik_batam\\Model\\VldPtk', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPtksRelatedByIdtype');
        $this->addRelation('VldPrestasiRelatedByIdtype', 'simdik_batam\\Model\\VldPrestasi', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPrestasisRelatedByIdtype');
        $this->addRelation('VldPrestasiRelatedByIdtype', 'simdik_batam\\Model\\VldPrestasi', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPrestasisRelatedByIdtype');
        $this->addRelation('VldPrasaranaRelatedByIdtype', 'simdik_batam\\Model\\VldPrasarana', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPrasaranasRelatedByIdtype');
        $this->addRelation('VldPrasaranaRelatedByIdtype', 'simdik_batam\\Model\\VldPrasarana', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPrasaranasRelatedByIdtype');
        $this->addRelation('VldPesertaDidikRelatedByIdtype', 'simdik_batam\\Model\\VldPesertaDidik', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPesertaDidiksRelatedByIdtype');
        $this->addRelation('VldPesertaDidikRelatedByIdtype', 'simdik_batam\\Model\\VldPesertaDidik', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPesertaDidiksRelatedByIdtype');
        $this->addRelation('VldPenghargaanRelatedByIdtype', 'simdik_batam\\Model\\VldPenghargaan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPenghargaansRelatedByIdtype');
        $this->addRelation('VldPenghargaanRelatedByIdtype', 'simdik_batam\\Model\\VldPenghargaan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPenghargaansRelatedByIdtype');
        $this->addRelation('VldPembelajaranRelatedByIdtype', 'simdik_batam\\Model\\VldPembelajaran', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPembelajaransRelatedByIdtype');
        $this->addRelation('VldPembelajaranRelatedByIdtype', 'simdik_batam\\Model\\VldPembelajaran', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPembelajaransRelatedByIdtype');
        $this->addRelation('VldPdLongRelatedByIdtype', 'simdik_batam\\Model\\VldPdLong', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPdLongsRelatedByIdtype');
        $this->addRelation('VldPdLongRelatedByIdtype', 'simdik_batam\\Model\\VldPdLong', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldPdLongsRelatedByIdtype');
        $this->addRelation('VldNonsekolahRelatedByIdtype', 'simdik_batam\\Model\\VldNonsekolah', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldNonsekolahsRelatedByIdtype');
        $this->addRelation('VldNonsekolahRelatedByIdtype', 'simdik_batam\\Model\\VldNonsekolah', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldNonsekolahsRelatedByIdtype');
        $this->addRelation('VldNilaiTestRelatedByIdtype', 'simdik_batam\\Model\\VldNilaiTest', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldNilaiTestsRelatedByIdtype');
        $this->addRelation('VldNilaiTestRelatedByIdtype', 'simdik_batam\\Model\\VldNilaiTest', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldNilaiTestsRelatedByIdtype');
        $this->addRelation('VldNilaiRaporRelatedByIdtype', 'simdik_batam\\Model\\VldNilaiRapor', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldNilaiRaporsRelatedByIdtype');
        $this->addRelation('VldNilaiRaporRelatedByIdtype', 'simdik_batam\\Model\\VldNilaiRapor', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldNilaiRaporsRelatedByIdtype');
        $this->addRelation('VldMouRelatedByIdtype', 'simdik_batam\\Model\\VldMou', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldMousRelatedByIdtype');
        $this->addRelation('VldMouRelatedByIdtype', 'simdik_batam\\Model\\VldMou', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldMousRelatedByIdtype');
        $this->addRelation('VldKesejahteraanRelatedByIdtype', 'simdik_batam\\Model\\VldKesejahteraan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldKesejahteraansRelatedByIdtype');
        $this->addRelation('VldKesejahteraanRelatedByIdtype', 'simdik_batam\\Model\\VldKesejahteraan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldKesejahteraansRelatedByIdtype');
        $this->addRelation('VldKaryaTulisRelatedByIdtype', 'simdik_batam\\Model\\VldKaryaTulis', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldKaryaTulissRelatedByIdtype');
        $this->addRelation('VldKaryaTulisRelatedByIdtype', 'simdik_batam\\Model\\VldKaryaTulis', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldKaryaTulissRelatedByIdtype');
        $this->addRelation('VldJurusanSpRelatedByIdtype', 'simdik_batam\\Model\\VldJurusanSp', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldJurusanSpsRelatedByIdtype');
        $this->addRelation('VldJurusanSpRelatedByIdtype', 'simdik_batam\\Model\\VldJurusanSp', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldJurusanSpsRelatedByIdtype');
        $this->addRelation('VldInpassingRelatedByIdtype', 'simdik_batam\\Model\\VldInpassing', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldInpassingsRelatedByIdtype');
        $this->addRelation('VldInpassingRelatedByIdtype', 'simdik_batam\\Model\\VldInpassing', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldInpassingsRelatedByIdtype');
        $this->addRelation('VldDemografiRelatedByIdtype', 'simdik_batam\\Model\\VldDemografi', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldDemografisRelatedByIdtype');
        $this->addRelation('VldDemografiRelatedByIdtype', 'simdik_batam\\Model\\VldDemografi', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldDemografisRelatedByIdtype');
        $this->addRelation('VldBukuPtkRelatedByIdtype', 'simdik_batam\\Model\\VldBukuPtk', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldBukuPtksRelatedByIdtype');
        $this->addRelation('VldBukuPtkRelatedByIdtype', 'simdik_batam\\Model\\VldBukuPtk', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldBukuPtksRelatedByIdtype');
        $this->addRelation('VldBeaPtkRelatedByIdtype', 'simdik_batam\\Model\\VldBeaPtk', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldBeaPtksRelatedByIdtype');
        $this->addRelation('VldBeaPtkRelatedByIdtype', 'simdik_batam\\Model\\VldBeaPtk', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldBeaPtksRelatedByIdtype');
        $this->addRelation('VldBeaPdRelatedByIdtype', 'simdik_batam\\Model\\VldBeaPd', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldBeaPdsRelatedByIdtype');
        $this->addRelation('VldBeaPdRelatedByIdtype', 'simdik_batam\\Model\\VldBeaPd', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldBeaPdsRelatedByIdtype');
        $this->addRelation('VldAnakRelatedByIdtype', 'simdik_batam\\Model\\VldAnak', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldAnaksRelatedByIdtype');
        $this->addRelation('VldAnakRelatedByIdtype', 'simdik_batam\\Model\\VldAnak', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldAnaksRelatedByIdtype');
        $this->addRelation('VldYayasanRelatedByIdtype', 'simdik_batam\\Model\\VldYayasan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldYayasansRelatedByIdtype');
        $this->addRelation('VldYayasanRelatedByIdtype', 'simdik_batam\\Model\\VldYayasan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldYayasansRelatedByIdtype');
        $this->addRelation('VldUnRelatedByIdtype', 'simdik_batam\\Model\\VldUn', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldUnsRelatedByIdtype');
        $this->addRelation('VldUnRelatedByIdtype', 'simdik_batam\\Model\\VldUn', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldUnsRelatedByIdtype');
        $this->addRelation('VldTunjanganRelatedByIdtype', 'simdik_batam\\Model\\VldTunjangan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldTunjangansRelatedByIdtype');
        $this->addRelation('VldTunjanganRelatedByIdtype', 'simdik_batam\\Model\\VldTunjangan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldTunjangansRelatedByIdtype');
        $this->addRelation('VldTugasTambahanRelatedByIdtype', 'simdik_batam\\Model\\VldTugasTambahan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldTugasTambahansRelatedByIdtype');
        $this->addRelation('VldTugasTambahanRelatedByIdtype', 'simdik_batam\\Model\\VldTugasTambahan', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldTugasTambahansRelatedByIdtype');
        $this->addRelation('VldSekolahRelatedByIdtype', 'simdik_batam\\Model\\VldSekolah', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldSekolahsRelatedByIdtype');
        $this->addRelation('VldSekolahRelatedByIdtype', 'simdik_batam\\Model\\VldSekolah', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldSekolahsRelatedByIdtype');
        $this->addRelation('VldSaranaRelatedByIdtype', 'simdik_batam\\Model\\VldSarana', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldSaranasRelatedByIdtype');
        $this->addRelation('VldSaranaRelatedByIdtype', 'simdik_batam\\Model\\VldSarana', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldSaranasRelatedByIdtype');
        $this->addRelation('VldRwyStrukturalRelatedByIdtype', 'simdik_batam\\Model\\VldRwyStruktural', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwyStrukturalsRelatedByIdtype');
        $this->addRelation('VldRwyStrukturalRelatedByIdtype', 'simdik_batam\\Model\\VldRwyStruktural', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwyStrukturalsRelatedByIdtype');
        $this->addRelation('VldRwySertifikasiRelatedByIdtype', 'simdik_batam\\Model\\VldRwySertifikasi', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwySertifikasisRelatedByIdtype');
        $this->addRelation('VldRwySertifikasiRelatedByIdtype', 'simdik_batam\\Model\\VldRwySertifikasi', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwySertifikasisRelatedByIdtype');
        $this->addRelation('VldRwyPendFormalRelatedByIdtype', 'simdik_batam\\Model\\VldRwyPendFormal', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwyPendFormalsRelatedByIdtype');
        $this->addRelation('VldRwyPendFormalRelatedByIdtype', 'simdik_batam\\Model\\VldRwyPendFormal', RelationMap::ONE_TO_MANY, array('idtype' => 'idtype', ), null, null, 'VldRwyPendFormalsRelatedByIdtype');
    } // buildRelations()

} // ErrortypeTableMap
