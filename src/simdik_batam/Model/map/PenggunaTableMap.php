<?php

namespace simdik_batam\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'pengguna' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.simdik_batam.Model.map
 */
class PenggunaTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'simdik_batam.Model.map.PenggunaTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('pengguna');
        $this->setPhpName('Pengguna');
        $this->setClassname('simdik_batam\\Model\\Pengguna');
        $this->setPackage('simdik_batam.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('pengguna_id', 'PenggunaId', 'CHAR', true, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', false, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', false, 16, null);
        $this->addForeignKey('lembaga_id', 'LembagaId', 'CHAR', 'lembaga_non_sekolah', 'lembaga_id', false, 16, null);
        $this->addForeignKey('lembaga_id', 'LembagaId', 'CHAR', 'lembaga_non_sekolah', 'lembaga_id', false, 16, null);
        $this->addColumn('yayasan_id', 'YayasanId', 'CHAR', false, 16, null);
        $this->addColumn('la_id', 'LaId', 'CHAR', false, 5, null);
        $this->addForeignKey('peran_id', 'PeranId', 'INTEGER', 'ref.peran', 'peran_id', true, 4, null);
        $this->addForeignKey('peran_id', 'PeranId', 'INTEGER', 'ref.peran', 'peran_id', true, 4, null);
        $this->addColumn('username', 'Username', 'VARCHAR', true, 50, null);
        $this->addColumn('password', 'Password', 'VARCHAR', true, 50, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 50, null);
        $this->addColumn('nip_nim', 'NipNim', 'VARCHAR', false, 9, null);
        $this->addColumn('jabatan_lembaga', 'JabatanLembaga', 'VARCHAR', false, 25, null);
        $this->addColumn('ym', 'Ym', 'VARCHAR', false, 20, null);
        $this->addColumn('skype', 'Skype', 'VARCHAR', false, 20, null);
        $this->addColumn('alamat', 'Alamat', 'VARCHAR', false, 80, null);
        $this->addForeignKey('kode_wilayah', 'KodeWilayah', 'CHAR', 'ref.mst_wilayah', 'kode_wilayah', true, 8, null);
        $this->addForeignKey('kode_wilayah', 'KodeWilayah', 'CHAR', 'ref.mst_wilayah', 'kode_wilayah', true, 8, null);
        $this->addColumn('no_telepon', 'NoTelepon', 'VARCHAR', false, 20, null);
        $this->addColumn('no_hp', 'NoHp', 'VARCHAR', false, 20, null);
        $this->addColumn('aktif', 'Aktif', 'NUMERIC', true, 3, 0);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('LembagaNonSekolahRelatedByLembagaId', 'simdik_batam\\Model\\LembagaNonSekolah', RelationMap::MANY_TO_ONE, array('lembaga_id' => 'lembaga_id', ), null, null);
        $this->addRelation('LembagaNonSekolahRelatedByLembagaId', 'simdik_batam\\Model\\LembagaNonSekolah', RelationMap::MANY_TO_ONE, array('lembaga_id' => 'lembaga_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'simdik_batam\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'simdik_batam\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('MstWilayahRelatedByKodeWilayah', 'simdik_batam\\Model\\MstWilayah', RelationMap::MANY_TO_ONE, array('kode_wilayah' => 'kode_wilayah', ), null, null);
        $this->addRelation('MstWilayahRelatedByKodeWilayah', 'simdik_batam\\Model\\MstWilayah', RelationMap::MANY_TO_ONE, array('kode_wilayah' => 'kode_wilayah', ), null, null);
        $this->addRelation('PeranRelatedByPeranId', 'simdik_batam\\Model\\Peran', RelationMap::MANY_TO_ONE, array('peran_id' => 'peran_id', ), null, null);
        $this->addRelation('PeranRelatedByPeranId', 'simdik_batam\\Model\\Peran', RelationMap::MANY_TO_ONE, array('peran_id' => 'peran_id', ), null, null);
        $this->addRelation('LogPenggunaRelatedByPenggunaId', 'simdik_batam\\Model\\LogPengguna', RelationMap::ONE_TO_MANY, array('pengguna_id' => 'pengguna_id', ), null, null, 'LogPenggunasRelatedByPenggunaId');
        $this->addRelation('LogPenggunaRelatedByPenggunaId', 'simdik_batam\\Model\\LogPengguna', RelationMap::ONE_TO_MANY, array('pengguna_id' => 'pengguna_id', ), null, null, 'LogPenggunasRelatedByPenggunaId');
        $this->addRelation('SasaranSurveyRelatedByPenggunaId', 'simdik_batam\\Model\\SasaranSurvey', RelationMap::ONE_TO_MANY, array('pengguna_id' => 'pengguna_id', ), null, null, 'SasaranSurveysRelatedByPenggunaId');
        $this->addRelation('SasaranSurveyRelatedByPenggunaId', 'simdik_batam\\Model\\SasaranSurvey', RelationMap::ONE_TO_MANY, array('pengguna_id' => 'pengguna_id', ), null, null, 'SasaranSurveysRelatedByPenggunaId');
        $this->addRelation('LembagaNonSekolahRelatedByPenggunaId', 'simdik_batam\\Model\\LembagaNonSekolah', RelationMap::ONE_TO_MANY, array('pengguna_id' => 'pengguna_id', ), null, null, 'LembagaNonSekolahsRelatedByPenggunaId');
        $this->addRelation('LembagaNonSekolahRelatedByPenggunaId', 'simdik_batam\\Model\\LembagaNonSekolah', RelationMap::ONE_TO_MANY, array('pengguna_id' => 'pengguna_id', ), null, null, 'LembagaNonSekolahsRelatedByPenggunaId');
    } // buildRelations()

} // PenggunaTableMap
