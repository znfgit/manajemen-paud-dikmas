<?php

namespace simdik_batam\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.wilayah_pdsp_dikdas_kec' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.simdik_batam.Model.map
 */
class WilayahPdspDikdasKecTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'simdik_batam.Model.map.WilayahPdspDikdasKecTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.wilayah_pdsp_dikdas_kec');
        $this->setPhpName('WilayahPdspDikdasKec');
        $this->setClassname('simdik_batam\\Model\\WilayahPdspDikdasKec');
        $this->setPackage('simdik_batam.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('kec_id', 'KecId', 'INTEGER', true, 4, null);
        $this->addColumn('kode_wilayah', 'KodeWilayah', 'CHAR', false, 6, null);
        $this->addColumn('kec_', 'Kec', 'VARCHAR', false, 80, null);
        $this->addColumn('kab_', 'Kab', 'VARCHAR', false, 80, null);
        $this->addColumn('prop_', 'Prop', 'VARCHAR', false, 80, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // WilayahPdspDikdasKecTableMap
