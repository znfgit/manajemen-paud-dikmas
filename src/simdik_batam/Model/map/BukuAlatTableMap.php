<?php

namespace simdik_batam\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'buku_alat' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.simdik_batam.Model.map
 */
class BukuAlatTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'simdik_batam.Model.map.BukuAlatTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('buku_alat');
        $this->setPhpName('BukuAlat');
        $this->setClassname('simdik_batam\\Model\\BukuAlat');
        $this->setPackage('simdik_batam.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('buku_alat_id', 'BukuAlatId', 'CHAR', true, 16, null);
        $this->addForeignKey('mata_pelajaran_id', 'MataPelajaranId', 'INTEGER', 'ref.mata_pelajaran', 'mata_pelajaran_id', true, 4, null);
        $this->addForeignKey('mata_pelajaran_id', 'MataPelajaranId', 'INTEGER', 'ref.mata_pelajaran', 'mata_pelajaran_id', true, 4, null);
        $this->addForeignKey('prasarana_id', 'PrasaranaId', 'CHAR', 'prasarana', 'prasarana_id', false, 16, null);
        $this->addForeignKey('prasarana_id', 'PrasaranaId', 'CHAR', 'prasarana', 'prasarana_id', false, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignKey('tingkat_pendidikan_id', 'TingkatPendidikanId', 'NUMERIC', 'ref.tingkat_pendidikan', 'tingkat_pendidikan_id', false, 4, null);
        $this->addForeignKey('tingkat_pendidikan_id', 'TingkatPendidikanId', 'NUMERIC', 'ref.tingkat_pendidikan', 'tingkat_pendidikan_id', false, 4, null);
        $this->addForeignKey('jenis_buku_alat_id', 'JenisBukuAlatId', 'NUMERIC', 'ref.jenis_buku_alat', 'jenis_buku_alat_id', true, 8, null);
        $this->addForeignKey('jenis_buku_alat_id', 'JenisBukuAlatId', 'NUMERIC', 'ref.jenis_buku_alat', 'jenis_buku_alat_id', true, 8, null);
        $this->addColumn('buku_alat', 'BukuAlat', 'VARCHAR', true, 60, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PrasaranaRelatedByPrasaranaId', 'simdik_batam\\Model\\Prasarana', RelationMap::MANY_TO_ONE, array('prasarana_id' => 'prasarana_id', ), null, null);
        $this->addRelation('PrasaranaRelatedByPrasaranaId', 'simdik_batam\\Model\\Prasarana', RelationMap::MANY_TO_ONE, array('prasarana_id' => 'prasarana_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'simdik_batam\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'simdik_batam\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('JenisBukuAlatRelatedByJenisBukuAlatId', 'simdik_batam\\Model\\JenisBukuAlat', RelationMap::MANY_TO_ONE, array('jenis_buku_alat_id' => 'jenis_buku_alat_id', ), null, null);
        $this->addRelation('JenisBukuAlatRelatedByJenisBukuAlatId', 'simdik_batam\\Model\\JenisBukuAlat', RelationMap::MANY_TO_ONE, array('jenis_buku_alat_id' => 'jenis_buku_alat_id', ), null, null);
        $this->addRelation('MataPelajaranRelatedByMataPelajaranId', 'simdik_batam\\Model\\MataPelajaran', RelationMap::MANY_TO_ONE, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null);
        $this->addRelation('MataPelajaranRelatedByMataPelajaranId', 'simdik_batam\\Model\\MataPelajaran', RelationMap::MANY_TO_ONE, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null);
        $this->addRelation('TingkatPendidikanRelatedByTingkatPendidikanId', 'simdik_batam\\Model\\TingkatPendidikan', RelationMap::MANY_TO_ONE, array('tingkat_pendidikan_id' => 'tingkat_pendidikan_id', ), null, null);
        $this->addRelation('TingkatPendidikanRelatedByTingkatPendidikanId', 'simdik_batam\\Model\\TingkatPendidikan', RelationMap::MANY_TO_ONE, array('tingkat_pendidikan_id' => 'tingkat_pendidikan_id', ), null, null);
        $this->addRelation('BukuAlatLongitudinalRelatedByBukuAlatId', 'simdik_batam\\Model\\BukuAlatLongitudinal', RelationMap::ONE_TO_MANY, array('buku_alat_id' => 'buku_alat_id', ), null, null, 'BukuAlatLongitudinalsRelatedByBukuAlatId');
        $this->addRelation('BukuAlatLongitudinalRelatedByBukuAlatId', 'simdik_batam\\Model\\BukuAlatLongitudinal', RelationMap::ONE_TO_MANY, array('buku_alat_id' => 'buku_alat_id', ), null, null, 'BukuAlatLongitudinalsRelatedByBukuAlatId');
    } // buildRelations()

} // BukuAlatTableMap
