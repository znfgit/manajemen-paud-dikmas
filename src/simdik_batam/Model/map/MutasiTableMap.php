<?php

namespace simdik_batam\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'adm.mutasi' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.simdik_batam.Model.map
 */
class MutasiTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'simdik_batam.Model.map.MutasiTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('adm.mutasi');
        $this->setPhpName('Mutasi');
        $this->setClassname('simdik_batam\\Model\\Mutasi');
        $this->setPackage('simdik_batam.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('mutasi_id', 'MutasiId', 'VARCHAR', true, 100, null);
        $this->addColumn('peserta_didik_id', 'PesertaDidikId', 'VARCHAR', false, 100, null);
        $this->addColumn('soft_delete', 'SoftDelete', 'INTEGER', false, 4, null);
        $this->addColumn('nomor_surat', 'NomorSurat', 'VARCHAR', false, 100, null);
        $this->addColumn('sekolah_id', 'SekolahId', 'VARCHAR', false, 100, null);
        $this->addColumn('kode_wilayah_berangkat', 'KodeWilayahBerangkat', 'VARCHAR', false, 10, null);
        $this->addColumn('maksud_tujuan', 'MaksudTujuan', 'VARCHAR', false, 200, null);
        $this->addColumn('nama_yang_diikuti', 'NamaYangDiikuti', 'VARCHAR', false, 100, null);
        $this->addColumn('hubungan_keluarga', 'HubunganKeluarga', 'VARCHAR', false, 100, null);
        $this->addColumn('pekerjaan', 'Pekerjaan', 'VARCHAR', false, 100, null);
        $this->addColumn('alamat_yang_diikuti', 'AlamatYangDiikuti', 'VARCHAR', false, 100, null);
        $this->addColumn('tanggal_berangkat', 'TanggalBerangkat', 'VARCHAR', false, 20, null);
        $this->addColumn('pengguna_id', 'PenggunaId', 'VARCHAR', false, 100, null);
        $this->addColumn('sekolah_id_tujuan', 'SekolahIdTujuan', 'VARCHAR', false, 100, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', false, 100, null);
        $this->addColumn('tempat_lahir', 'TempatLahir', 'VARCHAR', false, 100, null);
        $this->addColumn('tanggal_lahir', 'TanggalLahir', 'VARCHAR', false, 20, null);
        $this->addColumn('kelas', 'Kelas', 'VARCHAR', false, 100, null);
        $this->addColumn('NIS', 'Nis', 'VARCHAR', false, 100, null);
        $this->addColumn('NISN', 'Nisn', 'VARCHAR', false, 100, null);
        $this->addColumn('nama_ayah', 'NamaAyah', 'VARCHAR', false, 100, null);
        $this->addColumn('pekerjaan_id_ayah_str', 'PekerjaanIdAyahStr', 'VARCHAR', false, 100, null);
        $this->addColumn('alamat_jalan', 'AlamatJalan', 'VARCHAR', false, 100, null);
        $this->addColumn('jenis_surat_id', 'JenisSuratId', 'INTEGER', false, 4, null);
        $this->addColumn('tanggal_surat', 'TanggalSurat', 'VARCHAR', false, 20, null);
        $this->addColumn('keterangan_kepala_sekolah', 'KeteranganKepalaSekolah', 'VARCHAR', false, 100, null);
        $this->addColumn('keterangan_lain', 'KeteranganLain', 'VARCHAR', false, 100, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // MutasiTableMap
