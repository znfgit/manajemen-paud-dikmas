<?php

namespace simdik_batam\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.kebutuhan_khusus' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.simdik_batam.Model.map
 */
class KebutuhanKhususTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'simdik_batam.Model.map.KebutuhanKhususTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.kebutuhan_khusus');
        $this->setPhpName('KebutuhanKhusus');
        $this->setClassname('simdik_batam\\Model\\KebutuhanKhusus');
        $this->setPackage('simdik_batam.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('kebutuhan_khusus_id', 'KebutuhanKhususId', 'INTEGER', true, 4, null);
        $this->addColumn('kebutuhan_khusus', 'KebutuhanKhusus', 'VARCHAR', true, 40, null);
        $this->addColumn('kk_a', 'KkA', 'NUMERIC', true, 3, null);
        $this->addColumn('kk_b', 'KkB', 'NUMERIC', true, 3, null);
        $this->addColumn('kk_c', 'KkC', 'NUMERIC', true, 3, null);
        $this->addColumn('kk_c1', 'KkC1', 'NUMERIC', true, 3, null);
        $this->addColumn('kk_d', 'KkD', 'NUMERIC', true, 3, null);
        $this->addColumn('kk_d1', 'KkD1', 'NUMERIC', true, 3, null);
        $this->addColumn('kk_e', 'KkE', 'NUMERIC', true, 3, null);
        $this->addColumn('kk_f', 'KkF', 'NUMERIC', true, 3, null);
        $this->addColumn('kk_h', 'KkH', 'NUMERIC', true, 3, null);
        $this->addColumn('kk_i', 'KkI', 'NUMERIC', true, 3, null);
        $this->addColumn('kk_j', 'KkJ', 'NUMERIC', true, 3, null);
        $this->addColumn('kk_k', 'KkK', 'NUMERIC', true, 3, null);
        $this->addColumn('kk_n', 'KkN', 'NUMERIC', true, 3, null);
        $this->addColumn('kk_o', 'KkO', 'NUMERIC', true, 3, null);
        $this->addColumn('kk_p', 'KkP', 'NUMERIC', true, 3, null);
        $this->addColumn('kk_q', 'KkQ', 'NUMERIC', true, 3, null);
        $this->addColumn('untuk_lembaga', 'UntukLembaga', 'NUMERIC', true, 3, 0);
        $this->addColumn('untuk_ptk', 'UntukPtk', 'NUMERIC', true, 3, 0);
        $this->addColumn('untuk_pd', 'UntukPd', 'NUMERIC', true, 3, 0);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('JurusanSpRelatedByKebutuhanKhususId', 'simdik_batam\\Model\\JurusanSp', RelationMap::ONE_TO_MANY, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id', ), null, null, 'JurusanSpsRelatedByKebutuhanKhususId');
        $this->addRelation('JurusanSpRelatedByKebutuhanKhususId', 'simdik_batam\\Model\\JurusanSp', RelationMap::ONE_TO_MANY, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id', ), null, null, 'JurusanSpsRelatedByKebutuhanKhususId');
        $this->addRelation('PesertaDidikRelatedByKebutuhanKhususIdAyah', 'simdik_batam\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id_ayah', ), null, null, 'PesertaDidiksRelatedByKebutuhanKhususIdAyah');
        $this->addRelation('PesertaDidikRelatedByKebutuhanKhususIdIbu', 'simdik_batam\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id_ibu', ), null, null, 'PesertaDidiksRelatedByKebutuhanKhususIdIbu');
        $this->addRelation('PesertaDidikRelatedByKebutuhanKhususId', 'simdik_batam\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id', ), null, null, 'PesertaDidiksRelatedByKebutuhanKhususId');
        $this->addRelation('PesertaDidikRelatedByKebutuhanKhususIdAyah', 'simdik_batam\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id_ayah', ), null, null, 'PesertaDidiksRelatedByKebutuhanKhususIdAyah');
        $this->addRelation('PesertaDidikRelatedByKebutuhanKhususIdIbu', 'simdik_batam\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id_ibu', ), null, null, 'PesertaDidiksRelatedByKebutuhanKhususIdIbu');
        $this->addRelation('PesertaDidikRelatedByKebutuhanKhususId', 'simdik_batam\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id', ), null, null, 'PesertaDidiksRelatedByKebutuhanKhususId');
        $this->addRelation('SekolahRelatedByKebutuhanKhususId', 'simdik_batam\\Model\\Sekolah', RelationMap::ONE_TO_MANY, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id', ), null, null, 'SekolahsRelatedByKebutuhanKhususId');
        $this->addRelation('SekolahRelatedByKebutuhanKhususId', 'simdik_batam\\Model\\Sekolah', RelationMap::ONE_TO_MANY, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id', ), null, null, 'SekolahsRelatedByKebutuhanKhususId');
        $this->addRelation('JenisSertifikasi', 'simdik_batam\\Model\\JenisSertifikasi', RelationMap::ONE_TO_MANY, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id', ), null, null, 'JenisSertifikasis');
        $this->addRelation('PtkRelatedByMampuHandleKk', 'simdik_batam\\Model\\Ptk', RelationMap::ONE_TO_MANY, array('kebutuhan_khusus_id' => 'mampu_handle_kk', ), null, null, 'PtksRelatedByMampuHandleKk');
        $this->addRelation('PtkRelatedByMampuHandleKk', 'simdik_batam\\Model\\Ptk', RelationMap::ONE_TO_MANY, array('kebutuhan_khusus_id' => 'mampu_handle_kk', ), null, null, 'PtksRelatedByMampuHandleKk');
        $this->addRelation('RombonganBelajarRelatedByKebutuhanKhususId', 'simdik_batam\\Model\\RombonganBelajar', RelationMap::ONE_TO_MANY, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id', ), null, null, 'RombonganBelajarsRelatedByKebutuhanKhususId');
        $this->addRelation('RombonganBelajarRelatedByKebutuhanKhususId', 'simdik_batam\\Model\\RombonganBelajar', RelationMap::ONE_TO_MANY, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id', ), null, null, 'RombonganBelajarsRelatedByKebutuhanKhususId');
        $this->addRelation('ProgramInklusiRelatedByKebutuhanKhususId', 'simdik_batam\\Model\\ProgramInklusi', RelationMap::ONE_TO_MANY, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id', ), null, null, 'ProgramInklusisRelatedByKebutuhanKhususId');
        $this->addRelation('ProgramInklusiRelatedByKebutuhanKhususId', 'simdik_batam\\Model\\ProgramInklusi', RelationMap::ONE_TO_MANY, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id', ), null, null, 'ProgramInklusisRelatedByKebutuhanKhususId');
    } // buildRelations()

} // KebutuhanKhususTableMap
