<?php

namespace simdik_batam\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.jenis_buku_alat' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.simdik_batam.Model.map
 */
class JenisBukuAlatTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'simdik_batam.Model.map.JenisBukuAlatTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.jenis_buku_alat');
        $this->setPhpName('JenisBukuAlat');
        $this->setClassname('simdik_batam\\Model\\JenisBukuAlat');
        $this->setPackage('simdik_batam.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('jenis_buku_alat_id', 'JenisBukuAlatId', 'NUMERIC', true, 8, null);
        $this->addColumn('jenis_buku_alat', 'JenisBukuAlat', 'VARCHAR', true, 60, null);
        $this->addColumn('spm_qty_min_per_siswa', 'SpmQtyMinPerSiswa', 'NUMERIC', true, 5, 0);
        $this->addColumn('spm_qty_min_per_sekolah', 'SpmQtyMinPerSekolah', 'NUMERIC', true, 6, 0);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('BukuAlatRelatedByJenisBukuAlatId', 'simdik_batam\\Model\\BukuAlat', RelationMap::ONE_TO_MANY, array('jenis_buku_alat_id' => 'jenis_buku_alat_id', ), null, null, 'BukuAlatsRelatedByJenisBukuAlatId');
        $this->addRelation('BukuAlatRelatedByJenisBukuAlatId', 'simdik_batam\\Model\\BukuAlat', RelationMap::ONE_TO_MANY, array('jenis_buku_alat_id' => 'jenis_buku_alat_id', ), null, null, 'BukuAlatsRelatedByJenisBukuAlatId');
    } // buildRelations()

} // JenisBukuAlatTableMap
