<?php

namespace simdik_batam\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'rwy_pend_formal' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.simdik_batam.Model.map
 */
class RwyPendFormalTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'simdik_batam.Model.map.RwyPendFormalTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('rwy_pend_formal');
        $this->setPhpName('RwyPendFormal');
        $this->setClassname('simdik_batam\\Model\\RwyPendFormal');
        $this->setPackage('simdik_batam.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('riwayat_pendidikan_formal_id', 'RiwayatPendidikanFormalId', 'CHAR', true, 16, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('ptk_id', 'PtkId', 'CHAR', 'ptk', 'ptk_id', true, 16, null);
        $this->addForeignKey('bidang_studi_id', 'BidangStudiId', 'INTEGER', 'ref.bidang_studi', 'bidang_studi_id', true, 4, null);
        $this->addForeignKey('bidang_studi_id', 'BidangStudiId', 'INTEGER', 'ref.bidang_studi', 'bidang_studi_id', true, 4, null);
        $this->addForeignKey('jenjang_pendidikan_id', 'JenjangPendidikanId', 'NUMERIC', 'ref.jenjang_pendidikan', 'jenjang_pendidikan_id', true, 4, null);
        $this->addForeignKey('jenjang_pendidikan_id', 'JenjangPendidikanId', 'NUMERIC', 'ref.jenjang_pendidikan', 'jenjang_pendidikan_id', true, 4, null);
        $this->addForeignKey('gelar_akademik_id', 'GelarAkademikId', 'INTEGER', 'ref.gelar_akademik', 'gelar_akademik_id', false, 4, null);
        $this->addForeignKey('gelar_akademik_id', 'GelarAkademikId', 'INTEGER', 'ref.gelar_akademik', 'gelar_akademik_id', false, 4, null);
        $this->addColumn('satuan_pendidikan_formal', 'SatuanPendidikanFormal', 'VARCHAR', true, 80, null);
        $this->addColumn('fakultas', 'Fakultas', 'VARCHAR', false, 30, null);
        $this->addColumn('kependidikan', 'Kependidikan', 'NUMERIC', true, 3, null);
        $this->addColumn('tahun_masuk', 'TahunMasuk', 'NUMERIC', true, 6, null);
        $this->addColumn('tahun_lulus', 'TahunLulus', 'NUMERIC', false, 6, null);
        $this->addColumn('nim', 'Nim', 'VARCHAR', true, 12, null);
        $this->addColumn('status_kuliah', 'StatusKuliah', 'NUMERIC', true, 3, null);
        $this->addColumn('semester', 'Semester', 'NUMERIC', false, 4, null);
        $this->addColumn('ipk', 'Ipk', 'NUMERIC', true, 7, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PtkRelatedByPtkId', 'simdik_batam\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('PtkRelatedByPtkId', 'simdik_batam\\Model\\Ptk', RelationMap::MANY_TO_ONE, array('ptk_id' => 'ptk_id', ), null, null);
        $this->addRelation('BidangStudiRelatedByBidangStudiId', 'simdik_batam\\Model\\BidangStudi', RelationMap::MANY_TO_ONE, array('bidang_studi_id' => 'bidang_studi_id', ), null, null);
        $this->addRelation('BidangStudiRelatedByBidangStudiId', 'simdik_batam\\Model\\BidangStudi', RelationMap::MANY_TO_ONE, array('bidang_studi_id' => 'bidang_studi_id', ), null, null);
        $this->addRelation('GelarAkademikRelatedByGelarAkademikId', 'simdik_batam\\Model\\GelarAkademik', RelationMap::MANY_TO_ONE, array('gelar_akademik_id' => 'gelar_akademik_id', ), null, null);
        $this->addRelation('GelarAkademikRelatedByGelarAkademikId', 'simdik_batam\\Model\\GelarAkademik', RelationMap::MANY_TO_ONE, array('gelar_akademik_id' => 'gelar_akademik_id', ), null, null);
        $this->addRelation('JenjangPendidikanRelatedByJenjangPendidikanId', 'simdik_batam\\Model\\JenjangPendidikan', RelationMap::MANY_TO_ONE, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_id', ), null, null);
        $this->addRelation('JenjangPendidikanRelatedByJenjangPendidikanId', 'simdik_batam\\Model\\JenjangPendidikan', RelationMap::MANY_TO_ONE, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_id', ), null, null);
        $this->addRelation('VldRwyPendFormalRelatedByRiwayatPendidikanFormalId', 'simdik_batam\\Model\\VldRwyPendFormal', RelationMap::ONE_TO_MANY, array('riwayat_pendidikan_formal_id' => 'riwayat_pendidikan_formal_id', ), null, null, 'VldRwyPendFormalsRelatedByRiwayatPendidikanFormalId');
        $this->addRelation('VldRwyPendFormalRelatedByRiwayatPendidikanFormalId', 'simdik_batam\\Model\\VldRwyPendFormal', RelationMap::ONE_TO_MANY, array('riwayat_pendidikan_formal_id' => 'riwayat_pendidikan_formal_id', ), null, null, 'VldRwyPendFormalsRelatedByRiwayatPendidikanFormalId');
    } // buildRelations()

} // RwyPendFormalTableMap
