<?php

namespace simdik_batam\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'sarana' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.simdik_batam.Model.map
 */
class SaranaTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'simdik_batam.Model.map.SaranaTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sarana');
        $this->setPhpName('Sarana');
        $this->setClassname('simdik_batam\\Model\\Sarana');
        $this->setPackage('simdik_batam.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('sarana_id', 'SaranaId', 'CHAR', true, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignKey('sekolah_id', 'SekolahId', 'CHAR', 'sekolah', 'sekolah_id', true, 16, null);
        $this->addForeignKey('jenis_sarana_id', 'JenisSaranaId', 'INTEGER', 'ref.jenis_sarana', 'jenis_sarana_id', true, 4, null);
        $this->addForeignKey('jenis_sarana_id', 'JenisSaranaId', 'INTEGER', 'ref.jenis_sarana', 'jenis_sarana_id', true, 4, null);
        $this->addForeignKey('prasarana_id', 'PrasaranaId', 'CHAR', 'prasarana', 'prasarana_id', false, 16, null);
        $this->addForeignKey('prasarana_id', 'PrasaranaId', 'CHAR', 'prasarana', 'prasarana_id', false, 16, null);
        $this->addForeignKey('kepemilikan_sarpras_id', 'KepemilikanSarprasId', 'NUMERIC', 'ref.status_kepemilikan_sarpras', 'kepemilikan_sarpras_id', true, 3, null);
        $this->addForeignKey('kepemilikan_sarpras_id', 'KepemilikanSarprasId', 'NUMERIC', 'ref.status_kepemilikan_sarpras', 'kepemilikan_sarpras_id', true, 3, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 50, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PrasaranaRelatedByPrasaranaId', 'simdik_batam\\Model\\Prasarana', RelationMap::MANY_TO_ONE, array('prasarana_id' => 'prasarana_id', ), null, null);
        $this->addRelation('PrasaranaRelatedByPrasaranaId', 'simdik_batam\\Model\\Prasarana', RelationMap::MANY_TO_ONE, array('prasarana_id' => 'prasarana_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'simdik_batam\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('SekolahRelatedBySekolahId', 'simdik_batam\\Model\\Sekolah', RelationMap::MANY_TO_ONE, array('sekolah_id' => 'sekolah_id', ), null, null);
        $this->addRelation('JenisSaranaRelatedByJenisSaranaId', 'simdik_batam\\Model\\JenisSarana', RelationMap::MANY_TO_ONE, array('jenis_sarana_id' => 'jenis_sarana_id', ), null, null);
        $this->addRelation('JenisSaranaRelatedByJenisSaranaId', 'simdik_batam\\Model\\JenisSarana', RelationMap::MANY_TO_ONE, array('jenis_sarana_id' => 'jenis_sarana_id', ), null, null);
        $this->addRelation('StatusKepemilikanSarprasRelatedByKepemilikanSarprasId', 'simdik_batam\\Model\\StatusKepemilikanSarpras', RelationMap::MANY_TO_ONE, array('kepemilikan_sarpras_id' => 'kepemilikan_sarpras_id', ), null, null);
        $this->addRelation('StatusKepemilikanSarprasRelatedByKepemilikanSarprasId', 'simdik_batam\\Model\\StatusKepemilikanSarpras', RelationMap::MANY_TO_ONE, array('kepemilikan_sarpras_id' => 'kepemilikan_sarpras_id', ), null, null);
        $this->addRelation('SaranaLongitudinalRelatedBySaranaId', 'simdik_batam\\Model\\SaranaLongitudinal', RelationMap::ONE_TO_MANY, array('sarana_id' => 'sarana_id', ), null, null, 'SaranaLongitudinalsRelatedBySaranaId');
        $this->addRelation('SaranaLongitudinalRelatedBySaranaId', 'simdik_batam\\Model\\SaranaLongitudinal', RelationMap::ONE_TO_MANY, array('sarana_id' => 'sarana_id', ), null, null, 'SaranaLongitudinalsRelatedBySaranaId');
        $this->addRelation('VldSaranaRelatedBySaranaId', 'simdik_batam\\Model\\VldSarana', RelationMap::ONE_TO_MANY, array('sarana_id' => 'sarana_id', ), null, null, 'VldSaranasRelatedBySaranaId');
        $this->addRelation('VldSaranaRelatedBySaranaId', 'simdik_batam\\Model\\VldSarana', RelationMap::ONE_TO_MANY, array('sarana_id' => 'sarana_id', ), null, null, 'VldSaranasRelatedBySaranaId');
    } // buildRelations()

} // SaranaTableMap
