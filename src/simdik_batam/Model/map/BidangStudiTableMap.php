<?php

namespace simdik_batam\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.bidang_studi' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.simdik_batam.Model.map
 */
class BidangStudiTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'simdik_batam.Model.map.BidangStudiTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.bidang_studi');
        $this->setPhpName('BidangStudi');
        $this->setClassname('simdik_batam\\Model\\BidangStudi');
        $this->setPackage('simdik_batam.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('bidang_studi_id', 'BidangStudiId', 'INTEGER', true, 4, null);
        $this->addForeignKey('kelompok_bidang_studi_id', 'KelompokBidangStudiId', 'INTEGER', 'ref.bidang_studi', 'bidang_studi_id', false, 4, null);
        $this->addColumn('kode', 'Kode', 'CHAR', false, 3, null);
        $this->addColumn('bidang_studi', 'BidangStudi', 'VARCHAR', true, 40, null);
        $this->addColumn('kelompok', 'Kelompok', 'NUMERIC', true, 3, null);
        $this->addColumn('jenjang_paud', 'JenjangPaud', 'NUMERIC', true, 3, null);
        $this->addColumn('jenjang_tk', 'JenjangTk', 'NUMERIC', true, 3, null);
        $this->addColumn('jenjang_sd', 'JenjangSd', 'NUMERIC', true, 3, null);
        $this->addColumn('jenjang_smp', 'JenjangSmp', 'NUMERIC', true, 3, null);
        $this->addColumn('jenjang_sma', 'JenjangSma', 'NUMERIC', true, 3, null);
        $this->addColumn('jenjang_tinggi', 'JenjangTinggi', 'NUMERIC', true, 3, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('BidangStudiRelatedByKelompokBidangStudiId', 'simdik_batam\\Model\\BidangStudi', RelationMap::MANY_TO_ONE, array('kelompok_bidang_studi_id' => 'bidang_studi_id', ), null, null);
        $this->addRelation('MapBidangMataPelajaran', 'simdik_batam\\Model\\MapBidangMataPelajaran', RelationMap::ONE_TO_MANY, array('bidang_studi_id' => 'bidang_studi_id', ), null, null, 'MapBidangMataPelajarans');
        $this->addRelation('BidangStudiRelatedByBidangStudiId', 'simdik_batam\\Model\\BidangStudi', RelationMap::ONE_TO_MANY, array('bidang_studi_id' => 'kelompok_bidang_studi_id', ), null, null, 'BidangStudisRelatedByBidangStudiId');
        $this->addRelation('PtkRelatedByPengawasBidangStudiId', 'simdik_batam\\Model\\Ptk', RelationMap::ONE_TO_MANY, array('bidang_studi_id' => 'pengawas_bidang_studi_id', ), null, null, 'PtksRelatedByPengawasBidangStudiId');
        $this->addRelation('PtkRelatedByPengawasBidangStudiId', 'simdik_batam\\Model\\Ptk', RelationMap::ONE_TO_MANY, array('bidang_studi_id' => 'pengawas_bidang_studi_id', ), null, null, 'PtksRelatedByPengawasBidangStudiId');
        $this->addRelation('PengawasTerdaftarRelatedByBidangStudiId', 'simdik_batam\\Model\\PengawasTerdaftar', RelationMap::ONE_TO_MANY, array('bidang_studi_id' => 'bidang_studi_id', ), null, null, 'PengawasTerdaftarsRelatedByBidangStudiId');
        $this->addRelation('PengawasTerdaftarRelatedByBidangStudiId', 'simdik_batam\\Model\\PengawasTerdaftar', RelationMap::ONE_TO_MANY, array('bidang_studi_id' => 'bidang_studi_id', ), null, null, 'PengawasTerdaftarsRelatedByBidangStudiId');
        $this->addRelation('RwyPendFormalRelatedByBidangStudiId', 'simdik_batam\\Model\\RwyPendFormal', RelationMap::ONE_TO_MANY, array('bidang_studi_id' => 'bidang_studi_id', ), null, null, 'RwyPendFormalsRelatedByBidangStudiId');
        $this->addRelation('RwyPendFormalRelatedByBidangStudiId', 'simdik_batam\\Model\\RwyPendFormal', RelationMap::ONE_TO_MANY, array('bidang_studi_id' => 'bidang_studi_id', ), null, null, 'RwyPendFormalsRelatedByBidangStudiId');
        $this->addRelation('RwySertifikasiRelatedByBidangStudiId', 'simdik_batam\\Model\\RwySertifikasi', RelationMap::ONE_TO_MANY, array('bidang_studi_id' => 'bidang_studi_id', ), null, null, 'RwySertifikasisRelatedByBidangStudiId');
        $this->addRelation('RwySertifikasiRelatedByBidangStudiId', 'simdik_batam\\Model\\RwySertifikasi', RelationMap::ONE_TO_MANY, array('bidang_studi_id' => 'bidang_studi_id', ), null, null, 'RwySertifikasisRelatedByBidangStudiId');
    } // buildRelations()

} // BidangStudiTableMap
