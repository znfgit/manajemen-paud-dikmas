<?php

namespace simdik_batam\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'session_token' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.simdik_batam.Model.map
 */
class SessionTokenTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'simdik_batam.Model.map.SessionTokenTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('session_token');
        $this->setPhpName('SessionToken');
        $this->setClassname('simdik_batam\\Model\\SessionToken');
        $this->setPackage('simdik_batam.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('sekolah_id', 'SekolahId', 'CHAR', true, 16, null);
        $this->addColumn('token', 'Token', 'CHAR', true, 20, null);
        $this->addColumn('date_created', 'DateCreated', 'TIMESTAMP', true, 16, null);
        $this->addColumn('date_expired', 'DateExpired', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // SessionTokenTableMap
