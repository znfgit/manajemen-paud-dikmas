<?php

namespace simdik_batam\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.negara' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.simdik_batam.Model.map
 */
class NegaraTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'simdik_batam.Model.map.NegaraTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.negara');
        $this->setPhpName('Negara');
        $this->setClassname('simdik_batam\\Model\\Negara');
        $this->setPackage('simdik_batam.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('negara_id', 'NegaraId', 'CHAR', true, 2, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 45, null);
        $this->addColumn('luar_negeri', 'LuarNegeri', 'NUMERIC', true, 3, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PesertaDidikRelatedByKewarganegaraan', 'simdik_batam\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('negara_id' => 'kewarganegaraan', ), null, null, 'PesertaDidiksRelatedByKewarganegaraan');
        $this->addRelation('PesertaDidikRelatedByKewarganegaraan', 'simdik_batam\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('negara_id' => 'kewarganegaraan', ), null, null, 'PesertaDidiksRelatedByKewarganegaraan');
        $this->addRelation('MstWilayah', 'simdik_batam\\Model\\MstWilayah', RelationMap::ONE_TO_MANY, array('negara_id' => 'negara_id', ), null, null, 'MstWilayahs');
        $this->addRelation('PtkRelatedByKewarganegaraan', 'simdik_batam\\Model\\Ptk', RelationMap::ONE_TO_MANY, array('negara_id' => 'kewarganegaraan', ), null, null, 'PtksRelatedByKewarganegaraan');
        $this->addRelation('PtkRelatedByKewarganegaraan', 'simdik_batam\\Model\\Ptk', RelationMap::ONE_TO_MANY, array('negara_id' => 'kewarganegaraan', ), null, null, 'PtksRelatedByKewarganegaraan');
    } // buildRelations()

} // NegaraTableMap
