<?php

namespace simdik_batam\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'pembelajaran' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.simdik_batam.Model.map
 */
class PembelajaranTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'simdik_batam.Model.map.PembelajaranTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('pembelajaran');
        $this->setPhpName('Pembelajaran');
        $this->setClassname('simdik_batam\\Model\\Pembelajaran');
        $this->setPackage('simdik_batam.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('pembelajaran_id', 'PembelajaranId', 'CHAR', true, 16, null);
        $this->addForeignKey('rombongan_belajar_id', 'RombonganBelajarId', 'CHAR', 'rombongan_belajar', 'rombongan_belajar_id', true, 16, null);
        $this->addForeignKey('rombongan_belajar_id', 'RombonganBelajarId', 'CHAR', 'rombongan_belajar', 'rombongan_belajar_id', true, 16, null);
        $this->addForeignKey('semester_id', 'SemesterId', 'CHAR', 'ref.semester', 'semester_id', true, 5, null);
        $this->addForeignKey('semester_id', 'SemesterId', 'CHAR', 'ref.semester', 'semester_id', true, 5, null);
        $this->addForeignKey('mata_pelajaran_id', 'MataPelajaranId', 'INTEGER', 'ref.mata_pelajaran', 'mata_pelajaran_id', true, 4, null);
        $this->addForeignKey('mata_pelajaran_id', 'MataPelajaranId', 'INTEGER', 'ref.mata_pelajaran', 'mata_pelajaran_id', true, 4, null);
        $this->addForeignKey('ptk_terdaftar_id', 'PtkTerdaftarId', 'CHAR', 'ptk_terdaftar', 'ptk_terdaftar_id', true, 16, null);
        $this->addForeignKey('ptk_terdaftar_id', 'PtkTerdaftarId', 'CHAR', 'ptk_terdaftar', 'ptk_terdaftar_id', true, 16, null);
        $this->addColumn('sk_mengajar', 'SkMengajar', 'VARCHAR', true, 40, null);
        $this->addColumn('tanggal_sk_mengajar', 'TanggalSkMengajar', 'VARCHAR', true, 20, null);
        $this->addColumn('jam_mengajar_per_minggu', 'JamMengajarPerMinggu', 'NUMERIC', true, 4, null);
        $this->addColumn('status_di_kurikulum', 'StatusDiKurikulum', 'NUMERIC', true, 3, null);
        $this->addColumn('nama_mata_pelajaran', 'NamaMataPelajaran', 'VARCHAR', true, 50, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('PtkTerdaftarRelatedByPtkTerdaftarId', 'simdik_batam\\Model\\PtkTerdaftar', RelationMap::MANY_TO_ONE, array('ptk_terdaftar_id' => 'ptk_terdaftar_id', ), null, null);
        $this->addRelation('PtkTerdaftarRelatedByPtkTerdaftarId', 'simdik_batam\\Model\\PtkTerdaftar', RelationMap::MANY_TO_ONE, array('ptk_terdaftar_id' => 'ptk_terdaftar_id', ), null, null);
        $this->addRelation('RombonganBelajarRelatedByRombonganBelajarId', 'simdik_batam\\Model\\RombonganBelajar', RelationMap::MANY_TO_ONE, array('rombongan_belajar_id' => 'rombongan_belajar_id', ), null, null);
        $this->addRelation('RombonganBelajarRelatedByRombonganBelajarId', 'simdik_batam\\Model\\RombonganBelajar', RelationMap::MANY_TO_ONE, array('rombongan_belajar_id' => 'rombongan_belajar_id', ), null, null);
        $this->addRelation('MataPelajaranRelatedByMataPelajaranId', 'simdik_batam\\Model\\MataPelajaran', RelationMap::MANY_TO_ONE, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null);
        $this->addRelation('MataPelajaranRelatedByMataPelajaranId', 'simdik_batam\\Model\\MataPelajaran', RelationMap::MANY_TO_ONE, array('mata_pelajaran_id' => 'mata_pelajaran_id', ), null, null);
        $this->addRelation('SemesterRelatedBySemesterId', 'simdik_batam\\Model\\Semester', RelationMap::MANY_TO_ONE, array('semester_id' => 'semester_id', ), null, null);
        $this->addRelation('SemesterRelatedBySemesterId', 'simdik_batam\\Model\\Semester', RelationMap::MANY_TO_ONE, array('semester_id' => 'semester_id', ), null, null);
        $this->addRelation('VldPembelajaranRelatedByPembelajaranId', 'simdik_batam\\Model\\VldPembelajaran', RelationMap::ONE_TO_MANY, array('pembelajaran_id' => 'pembelajaran_id', ), null, null, 'VldPembelajaransRelatedByPembelajaranId');
        $this->addRelation('VldPembelajaranRelatedByPembelajaranId', 'simdik_batam\\Model\\VldPembelajaran', RelationMap::ONE_TO_MANY, array('pembelajaran_id' => 'pembelajaran_id', ), null, null, 'VldPembelajaransRelatedByPembelajaranId');
    } // buildRelations()

} // PembelajaranTableMap
