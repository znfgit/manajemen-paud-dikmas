<?php

namespace simdik_batam\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.jenjang_pendidikan' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.simdik_batam.Model.map
 */
class JenjangPendidikanTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'simdik_batam.Model.map.JenjangPendidikanTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.jenjang_pendidikan');
        $this->setPhpName('JenjangPendidikan');
        $this->setClassname('simdik_batam\\Model\\JenjangPendidikan');
        $this->setPackage('simdik_batam.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('jenjang_pendidikan_id', 'JenjangPendidikanId', 'NUMERIC', true, 4, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 25, null);
        $this->addColumn('jenjang_lembaga', 'JenjangLembaga', 'NUMERIC', true, 3, null);
        $this->addColumn('jenjang_orang', 'JenjangOrang', 'NUMERIC', true, 3, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('TingkatPendidikan', 'simdik_batam\\Model\\TingkatPendidikan', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_id', ), null, null, 'TingkatPendidikans');
        $this->addRelation('TemplateUn', 'simdik_batam\\Model\\TemplateUn', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_id', ), null, null, 'TemplateUns');
        $this->addRelation('PesertaDidikRelatedByJenjangPendidikanIbu', 'simdik_batam\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_ibu', ), null, null, 'PesertaDidiksRelatedByJenjangPendidikanIbu');
        $this->addRelation('PesertaDidikRelatedByJenjangPendidikanAyah', 'simdik_batam\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_ayah', ), null, null, 'PesertaDidiksRelatedByJenjangPendidikanAyah');
        $this->addRelation('PesertaDidikRelatedByJenjangPendidikanWali', 'simdik_batam\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_wali', ), null, null, 'PesertaDidiksRelatedByJenjangPendidikanWali');
        $this->addRelation('PesertaDidikRelatedByJenjangPendidikanIbu', 'simdik_batam\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_ibu', ), null, null, 'PesertaDidiksRelatedByJenjangPendidikanIbu');
        $this->addRelation('PesertaDidikRelatedByJenjangPendidikanAyah', 'simdik_batam\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_ayah', ), null, null, 'PesertaDidiksRelatedByJenjangPendidikanAyah');
        $this->addRelation('PesertaDidikRelatedByJenjangPendidikanWali', 'simdik_batam\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_wali', ), null, null, 'PesertaDidiksRelatedByJenjangPendidikanWali');
        $this->addRelation('Kurikulum', 'simdik_batam\\Model\\Kurikulum', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_id', ), null, null, 'Kurikulums');
        $this->addRelation('RwyPendFormalRelatedByJenjangPendidikanId', 'simdik_batam\\Model\\RwyPendFormal', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_id', ), null, null, 'RwyPendFormalsRelatedByJenjangPendidikanId');
        $this->addRelation('RwyPendFormalRelatedByJenjangPendidikanId', 'simdik_batam\\Model\\RwyPendFormal', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_id', ), null, null, 'RwyPendFormalsRelatedByJenjangPendidikanId');
        $this->addRelation('AnakRelatedByJenjangPendidikanId', 'simdik_batam\\Model\\Anak', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_id', ), null, null, 'AnaksRelatedByJenjangPendidikanId');
        $this->addRelation('AnakRelatedByJenjangPendidikanId', 'simdik_batam\\Model\\Anak', RelationMap::ONE_TO_MANY, array('jenjang_pendidikan_id' => 'jenjang_pendidikan_id', ), null, null, 'AnaksRelatedByJenjangPendidikanId');
    } // buildRelations()

} // JenjangPendidikanTableMap
