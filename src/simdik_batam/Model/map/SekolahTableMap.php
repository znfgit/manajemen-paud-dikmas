<?php

namespace simdik_batam\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'sekolah' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.simdik_batam.Model.map
 */
class SekolahTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'simdik_batam.Model.map.SekolahTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('sekolah');
        $this->setPhpName('Sekolah');
        $this->setClassname('simdik_batam\\Model\\Sekolah');
        $this->setPackage('simdik_batam.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('sekolah_id', 'SekolahId', 'CHAR', true, 16, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 80, null);
        $this->addColumn('nama_nomenklatur', 'NamaNomenklatur', 'VARCHAR', false, 80, null);
        $this->addColumn('nss', 'Nss', 'CHAR', false, 12, null);
        $this->addColumn('npsn', 'Npsn', 'CHAR', false, 8, null);
        $this->addForeignKey('bentuk_pendidikan_id', 'BentukPendidikanId', 'SMALLINT', 'ref.bentuk_pendidikan', 'bentuk_pendidikan_id', true, 2, null);
        $this->addForeignKey('bentuk_pendidikan_id', 'BentukPendidikanId', 'SMALLINT', 'ref.bentuk_pendidikan', 'bentuk_pendidikan_id', true, 2, null);
        $this->addColumn('alamat_jalan', 'AlamatJalan', 'VARCHAR', true, 80, null);
        $this->addColumn('rt', 'Rt', 'NUMERIC', false, 4, null);
        $this->addColumn('rw', 'Rw', 'NUMERIC', false, 4, null);
        $this->addColumn('nama_dusun', 'NamaDusun', 'VARCHAR', false, 40, null);
        $this->addColumn('desa_kelurahan', 'DesaKelurahan', 'VARCHAR', true, 40, null);
        $this->addForeignKey('kode_wilayah', 'KodeWilayah', 'CHAR', 'ref.mst_wilayah', 'kode_wilayah', true, 8, null);
        $this->addForeignKey('kode_wilayah', 'KodeWilayah', 'CHAR', 'ref.mst_wilayah', 'kode_wilayah', true, 8, null);
        $this->addColumn('kode_pos', 'KodePos', 'CHAR', false, 5, null);
        $this->addColumn('lintang', 'Lintang', 'NUMERIC', false, 12, null);
        $this->addColumn('bujur', 'Bujur', 'NUMERIC', false, 12, null);
        $this->addColumn('nomor_telepon', 'NomorTelepon', 'VARCHAR', false, 20, null);
        $this->addColumn('nomor_fax', 'NomorFax', 'VARCHAR', false, 20, null);
        $this->addColumn('email', 'Email', 'VARCHAR', false, 50, null);
        $this->addColumn('website', 'Website', 'VARCHAR', false, 100, null);
        $this->addForeignKey('kebutuhan_khusus_id', 'KebutuhanKhususId', 'INTEGER', 'ref.kebutuhan_khusus', 'kebutuhan_khusus_id', true, 4, null);
        $this->addForeignKey('kebutuhan_khusus_id', 'KebutuhanKhususId', 'INTEGER', 'ref.kebutuhan_khusus', 'kebutuhan_khusus_id', true, 4, null);
        $this->addColumn('status_sekolah', 'StatusSekolah', 'NUMERIC', true, 3, null);
        $this->addColumn('sk_pendirian_sekolah', 'SkPendirianSekolah', 'VARCHAR', false, 40, null);
        $this->addColumn('tanggal_sk_pendirian', 'TanggalSkPendirian', 'VARCHAR', false, 20, null);
        $this->addForeignKey('status_kepemilikan_id', 'StatusKepemilikanId', 'NUMERIC', 'ref.status_kepemilikan', 'status_kepemilikan_id', true, 3, null);
        $this->addForeignKey('status_kepemilikan_id', 'StatusKepemilikanId', 'NUMERIC', 'ref.status_kepemilikan', 'status_kepemilikan_id', true, 3, null);
        $this->addColumn('yayasan_id', 'YayasanId', 'CHAR', false, 16, null);
        $this->addColumn('sk_izin_operasional', 'SkIzinOperasional', 'VARCHAR', false, 40, null);
        $this->addColumn('tanggal_sk_izin_operasional', 'TanggalSkIzinOperasional', 'VARCHAR', false, 20, null);
        $this->addColumn('no_rekening', 'NoRekening', 'VARCHAR', false, 20, null);
        $this->addColumn('nama_bank', 'NamaBank', 'VARCHAR', false, 20, null);
        $this->addColumn('cabang_kcp_unit', 'CabangKcpUnit', 'VARCHAR', false, 40, null);
        $this->addColumn('rekening_atas_nama', 'RekeningAtasNama', 'VARCHAR', false, 50, null);
        $this->addColumn('mbs', 'Mbs', 'NUMERIC', true, 3, null);
        $this->addColumn('luas_tanah_milik', 'LuasTanahMilik', 'NUMERIC', true, 9, null);
        $this->addColumn('luas_tanah_bukan_milik', 'LuasTanahBukanMilik', 'NUMERIC', true, 9, null);
        $this->addColumn('kode_registrasi', 'KodeRegistrasi', 'BIGINT', false, 8, null);
        $this->addColumn('flag', 'Flag', 'CHAR', false, 1, null);
        $this->addColumn('pic_id', 'PicId', 'CHAR', false, 16, null);
        $this->addColumn('Last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Soft_delete', 'SoftDelete', 'NUMERIC', true, 3, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        $this->addColumn('Updater_ID', 'UpdaterId', 'CHAR', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('BentukPendidikanRelatedByBentukPendidikanId', 'simdik_batam\\Model\\BentukPendidikan', RelationMap::MANY_TO_ONE, array('bentuk_pendidikan_id' => 'bentuk_pendidikan_id', ), null, null);
        $this->addRelation('BentukPendidikanRelatedByBentukPendidikanId', 'simdik_batam\\Model\\BentukPendidikan', RelationMap::MANY_TO_ONE, array('bentuk_pendidikan_id' => 'bentuk_pendidikan_id', ), null, null);
        $this->addRelation('KebutuhanKhususRelatedByKebutuhanKhususId', 'simdik_batam\\Model\\KebutuhanKhusus', RelationMap::MANY_TO_ONE, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id', ), null, null);
        $this->addRelation('KebutuhanKhususRelatedByKebutuhanKhususId', 'simdik_batam\\Model\\KebutuhanKhusus', RelationMap::MANY_TO_ONE, array('kebutuhan_khusus_id' => 'kebutuhan_khusus_id', ), null, null);
        $this->addRelation('MstWilayahRelatedByKodeWilayah', 'simdik_batam\\Model\\MstWilayah', RelationMap::MANY_TO_ONE, array('kode_wilayah' => 'kode_wilayah', ), null, null);
        $this->addRelation('MstWilayahRelatedByKodeWilayah', 'simdik_batam\\Model\\MstWilayah', RelationMap::MANY_TO_ONE, array('kode_wilayah' => 'kode_wilayah', ), null, null);
        $this->addRelation('StatusKepemilikanRelatedByStatusKepemilikanId', 'simdik_batam\\Model\\StatusKepemilikan', RelationMap::MANY_TO_ONE, array('status_kepemilikan_id' => 'status_kepemilikan_id', ), null, null);
        $this->addRelation('StatusKepemilikanRelatedByStatusKepemilikanId', 'simdik_batam\\Model\\StatusKepemilikan', RelationMap::MANY_TO_ONE, array('status_kepemilikan_id' => 'status_kepemilikan_id', ), null, null);
        $this->addRelation('BukuAlatRelatedBySekolahId', 'simdik_batam\\Model\\BukuAlat', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'BukuAlatsRelatedBySekolahId');
        $this->addRelation('BukuAlatRelatedBySekolahId', 'simdik_batam\\Model\\BukuAlat', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'BukuAlatsRelatedBySekolahId');
        $this->addRelation('JurusanSpRelatedBySekolahId', 'simdik_batam\\Model\\JurusanSp', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'JurusanSpsRelatedBySekolahId');
        $this->addRelation('JurusanSpRelatedBySekolahId', 'simdik_batam\\Model\\JurusanSp', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'JurusanSpsRelatedBySekolahId');
        $this->addRelation('SekolahLongitudinalRelatedBySekolahId', 'simdik_batam\\Model\\SekolahLongitudinal', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'SekolahLongitudinalsRelatedBySekolahId');
        $this->addRelation('SekolahLongitudinalRelatedBySekolahId', 'simdik_batam\\Model\\SekolahLongitudinal', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'SekolahLongitudinalsRelatedBySekolahId');
        $this->addRelation('PesertaDidikRelatedBySekolahId', 'simdik_batam\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'PesertaDidiksRelatedBySekolahId');
        $this->addRelation('PesertaDidikRelatedBySekolahId', 'simdik_batam\\Model\\PesertaDidik', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'PesertaDidiksRelatedBySekolahId');
        $this->addRelation('SanitasiRelatedBySekolahId', 'simdik_batam\\Model\\Sanitasi', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'SanitasisRelatedBySekolahId');
        $this->addRelation('SanitasiRelatedBySekolahId', 'simdik_batam\\Model\\Sanitasi', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'SanitasisRelatedBySekolahId');
        $this->addRelation('RegistrasiPesertaDidikRelatedBySekolahId', 'simdik_batam\\Model\\RegistrasiPesertaDidik', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'RegistrasiPesertaDidiksRelatedBySekolahId');
        $this->addRelation('RegistrasiPesertaDidikRelatedBySekolahId', 'simdik_batam\\Model\\RegistrasiPesertaDidik', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'RegistrasiPesertaDidiksRelatedBySekolahId');
        $this->addRelation('PenggunaRelatedBySekolahId', 'simdik_batam\\Model\\Pengguna', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'PenggunasRelatedBySekolahId');
        $this->addRelation('PenggunaRelatedBySekolahId', 'simdik_batam\\Model\\Pengguna', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'PenggunasRelatedBySekolahId');
        $this->addRelation('PesertaDidikBaruRelatedBySekolahId', 'simdik_batam\\Model\\PesertaDidikBaru', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'PesertaDidikBarusRelatedBySekolahId');
        $this->addRelation('PesertaDidikBaruRelatedBySekolahId', 'simdik_batam\\Model\\PesertaDidikBaru', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'PesertaDidikBarusRelatedBySekolahId');
        $this->addRelation('SaranaRelatedBySekolahId', 'simdik_batam\\Model\\Sarana', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'SaranasRelatedBySekolahId');
        $this->addRelation('SaranaRelatedBySekolahId', 'simdik_batam\\Model\\Sarana', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'SaranasRelatedBySekolahId');
        $this->addRelation('AkreditasiSpRelatedBySekolahId', 'simdik_batam\\Model\\AkreditasiSp', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'AkreditasiSpsRelatedBySekolahId');
        $this->addRelation('AkreditasiSpRelatedBySekolahId', 'simdik_batam\\Model\\AkreditasiSp', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'AkreditasiSpsRelatedBySekolahId');
        $this->addRelation('GugusSekolahRelatedBySekolahId', 'simdik_batam\\Model\\GugusSekolah', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'GugusSekolahsRelatedBySekolahId');
        $this->addRelation('GugusSekolahRelatedBySekolahId', 'simdik_batam\\Model\\GugusSekolah', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'GugusSekolahsRelatedBySekolahId');
        $this->addRelation('PtkTerdaftarRelatedBySekolahId', 'simdik_batam\\Model\\PtkTerdaftar', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'PtkTerdaftarsRelatedBySekolahId');
        $this->addRelation('PtkTerdaftarRelatedBySekolahId', 'simdik_batam\\Model\\PtkTerdaftar', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'PtkTerdaftarsRelatedBySekolahId');
        $this->addRelation('BlockgrantRelatedBySekolahId', 'simdik_batam\\Model\\Blockgrant', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'BlockgrantsRelatedBySekolahId');
        $this->addRelation('BlockgrantRelatedBySekolahId', 'simdik_batam\\Model\\Blockgrant', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'BlockgrantsRelatedBySekolahId');
        $this->addRelation('MouRelatedBySekolahId', 'simdik_batam\\Model\\Mou', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'MousRelatedBySekolahId');
        $this->addRelation('MouRelatedBySekolahId', 'simdik_batam\\Model\\Mou', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'MousRelatedBySekolahId');
        $this->addRelation('UnitUsahaRelatedBySekolahId', 'simdik_batam\\Model\\UnitUsaha', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'UnitUsahasRelatedBySekolahId');
        $this->addRelation('UnitUsahaRelatedBySekolahId', 'simdik_batam\\Model\\UnitUsaha', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'UnitUsahasRelatedBySekolahId');
        $this->addRelation('SyncSessionRelatedBySekolahId', 'simdik_batam\\Model\\SyncSession', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'SyncSessionsRelatedBySekolahId');
        $this->addRelation('SyncSessionRelatedBySekolahId', 'simdik_batam\\Model\\SyncSession', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'SyncSessionsRelatedBySekolahId');
        $this->addRelation('PtkRelatedByEntrySekolahId', 'simdik_batam\\Model\\Ptk', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'entry_sekolah_id', ), null, null, 'PtksRelatedByEntrySekolahId');
        $this->addRelation('PtkRelatedByEntrySekolahId', 'simdik_batam\\Model\\Ptk', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'entry_sekolah_id', ), null, null, 'PtksRelatedByEntrySekolahId');
        $this->addRelation('WtSrcSyncLogRelatedBySekolahId', 'simdik_batam\\Model\\WtSrcSyncLog', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'WtSrcSyncLogsRelatedBySekolahId');
        $this->addRelation('WtSrcSyncLogRelatedBySekolahId', 'simdik_batam\\Model\\WtSrcSyncLog', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'WtSrcSyncLogsRelatedBySekolahId');
        $this->addRelation('TableSyncLogRelatedBySekolahId', 'simdik_batam\\Model\\TableSyncLog', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'TableSyncLogsRelatedBySekolahId');
        $this->addRelation('TableSyncLogRelatedBySekolahId', 'simdik_batam\\Model\\TableSyncLog', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'TableSyncLogsRelatedBySekolahId');
        $this->addRelation('WtDstSyncLogRelatedBySekolahId', 'simdik_batam\\Model\\WtDstSyncLog', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'WtDstSyncLogsRelatedBySekolahId');
        $this->addRelation('WtDstSyncLogRelatedBySekolahId', 'simdik_batam\\Model\\WtDstSyncLog', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'WtDstSyncLogsRelatedBySekolahId');
        $this->addRelation('PrasaranaRelatedBySekolahId', 'simdik_batam\\Model\\Prasarana', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'PrasaranasRelatedBySekolahId');
        $this->addRelation('PrasaranaRelatedBySekolahId', 'simdik_batam\\Model\\Prasarana', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'PrasaranasRelatedBySekolahId');
        $this->addRelation('RombonganBelajarRelatedBySekolahId', 'simdik_batam\\Model\\RombonganBelajar', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'RombonganBelajarsRelatedBySekolahId');
        $this->addRelation('RombonganBelajarRelatedBySekolahId', 'simdik_batam\\Model\\RombonganBelajar', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'RombonganBelajarsRelatedBySekolahId');
        $this->addRelation('SasaranSurveyRelatedBySekolahId', 'simdik_batam\\Model\\SasaranSurvey', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'SasaranSurveysRelatedBySekolahId');
        $this->addRelation('SasaranSurveyRelatedBySekolahId', 'simdik_batam\\Model\\SasaranSurvey', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'SasaranSurveysRelatedBySekolahId');
        $this->addRelation('WsrcSyncLogRelatedBySekolahId', 'simdik_batam\\Model\\WsrcSyncLog', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'WsrcSyncLogsRelatedBySekolahId');
        $this->addRelation('WsrcSyncLogRelatedBySekolahId', 'simdik_batam\\Model\\WsrcSyncLog', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'WsrcSyncLogsRelatedBySekolahId');
        $this->addRelation('SasaranPengawasanRelatedBySekolahId', 'simdik_batam\\Model\\SasaranPengawasan', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'SasaranPengawasansRelatedBySekolahId');
        $this->addRelation('SasaranPengawasanRelatedBySekolahId', 'simdik_batam\\Model\\SasaranPengawasan', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'SasaranPengawasansRelatedBySekolahId');
        $this->addRelation('WdstSyncLogRelatedBySekolahId', 'simdik_batam\\Model\\WdstSyncLog', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'WdstSyncLogsRelatedBySekolahId');
        $this->addRelation('WdstSyncLogRelatedBySekolahId', 'simdik_batam\\Model\\WdstSyncLog', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'WdstSyncLogsRelatedBySekolahId');
        $this->addRelation('SyncLogRelatedBySekolahId', 'simdik_batam\\Model\\SyncLog', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'SyncLogsRelatedBySekolahId');
        $this->addRelation('SyncLogRelatedBySekolahId', 'simdik_batam\\Model\\SyncLog', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'SyncLogsRelatedBySekolahId');
        $this->addRelation('WPendingJobRelatedBySekolahId', 'simdik_batam\\Model\\WPendingJob', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'WPendingJobsRelatedBySekolahId');
        $this->addRelation('WPendingJobRelatedBySekolahId', 'simdik_batam\\Model\\WPendingJob', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'WPendingJobsRelatedBySekolahId');
        $this->addRelation('ProgramInklusiRelatedBySekolahId', 'simdik_batam\\Model\\ProgramInklusi', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'ProgramInklusisRelatedBySekolahId');
        $this->addRelation('ProgramInklusiRelatedBySekolahId', 'simdik_batam\\Model\\ProgramInklusi', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'ProgramInklusisRelatedBySekolahId');
        $this->addRelation('PtkBaruRelatedBySekolahId', 'simdik_batam\\Model\\PtkBaru', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'PtkBarusRelatedBySekolahId');
        $this->addRelation('PtkBaruRelatedBySekolahId', 'simdik_batam\\Model\\PtkBaru', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'PtkBarusRelatedBySekolahId');
        $this->addRelation('VldSekolahRelatedBySekolahId', 'simdik_batam\\Model\\VldSekolah', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'VldSekolahsRelatedBySekolahId');
        $this->addRelation('VldSekolahRelatedBySekolahId', 'simdik_batam\\Model\\VldSekolah', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'VldSekolahsRelatedBySekolahId');
        $this->addRelation('AnggotaGugusRelatedBySekolahId', 'simdik_batam\\Model\\AnggotaGugus', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'AnggotaGugussRelatedBySekolahId');
        $this->addRelation('AnggotaGugusRelatedBySekolahId', 'simdik_batam\\Model\\AnggotaGugus', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'AnggotaGugussRelatedBySekolahId');
        $this->addRelation('TugasTambahanRelatedBySekolahId', 'simdik_batam\\Model\\TugasTambahan', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'TugasTambahansRelatedBySekolahId');
        $this->addRelation('TugasTambahanRelatedBySekolahId', 'simdik_batam\\Model\\TugasTambahan', RelationMap::ONE_TO_MANY, array('sekolah_id' => 'sekolah_id', ), null, null, 'TugasTambahansRelatedBySekolahId');
    } // buildRelations()

} // SekolahTableMap
