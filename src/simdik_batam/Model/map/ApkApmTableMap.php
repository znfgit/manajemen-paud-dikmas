<?php

namespace simdik_batam\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'adm.apk_apm' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.simdik_batam.Model.map
 */
class ApkApmTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'simdik_batam.Model.map.ApkApmTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('adm.apk_apm');
        $this->setPhpName('ApkApm');
        $this->setClassname('simdik_batam\\Model\\ApkApm');
        $this->setPackage('simdik_batam.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('apk_apm_id', 'ApkApmId', 'VARCHAR', true, 100, null);
        $this->addColumn('tanggal', 'Tanggal', 'TIMESTAMP', false, 16, null);
        $this->addColumn('pengguna_id', 'PenggunaId', 'VARCHAR', false, 100, null);
        $this->addColumn('jumlah_penduduk_6_12', 'JumlahPenduduk612', 'INTEGER', false, 4, null);
        $this->addColumn('jumlah_penduduk_13_15', 'JumlahPenduduk1315', 'INTEGER', false, 4, null);
        $this->addColumn('jumlah_penduduk_16_19', 'JumlahPenduduk1619', 'INTEGER', false, 4, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // ApkApmTableMap
