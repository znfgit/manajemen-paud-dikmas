<?php

namespace simdik_batam\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'ref.status_kepemilikan_sarpras' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.simdik_batam.Model.map
 */
class StatusKepemilikanSarprasTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'simdik_batam.Model.map.StatusKepemilikanSarprasTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('ref.status_kepemilikan_sarpras');
        $this->setPhpName('StatusKepemilikanSarpras');
        $this->setClassname('simdik_batam\\Model\\StatusKepemilikanSarpras');
        $this->setPackage('simdik_batam.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('kepemilikan_sarpras_id', 'KepemilikanSarprasId', 'NUMERIC', true, 3, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 20, null);
        $this->addColumn('create_date', 'CreateDate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('last_update', 'LastUpdate', 'TIMESTAMP', true, 16, null);
        $this->addColumn('expired_date', 'ExpiredDate', 'TIMESTAMP', false, 16, null);
        $this->addColumn('last_sync', 'LastSync', 'TIMESTAMP', true, 16, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
        $this->addRelation('SaranaRelatedByKepemilikanSarprasId', 'simdik_batam\\Model\\Sarana', RelationMap::ONE_TO_MANY, array('kepemilikan_sarpras_id' => 'kepemilikan_sarpras_id', ), null, null, 'SaranasRelatedByKepemilikanSarprasId');
        $this->addRelation('SaranaRelatedByKepemilikanSarprasId', 'simdik_batam\\Model\\Sarana', RelationMap::ONE_TO_MANY, array('kepemilikan_sarpras_id' => 'kepemilikan_sarpras_id', ), null, null, 'SaranasRelatedByKepemilikanSarprasId');
        $this->addRelation('PrasaranaRelatedByKepemilikanSarprasId', 'simdik_batam\\Model\\Prasarana', RelationMap::ONE_TO_MANY, array('kepemilikan_sarpras_id' => 'kepemilikan_sarpras_id', ), null, null, 'PrasaranasRelatedByKepemilikanSarprasId');
        $this->addRelation('PrasaranaRelatedByKepemilikanSarprasId', 'simdik_batam\\Model\\Prasarana', RelationMap::ONE_TO_MANY, array('kepemilikan_sarpras_id' => 'kepemilikan_sarpras_id', ), null, null, 'PrasaranasRelatedByKepemilikanSarprasId');
    } // buildRelations()

} // StatusKepemilikanSarprasTableMap
