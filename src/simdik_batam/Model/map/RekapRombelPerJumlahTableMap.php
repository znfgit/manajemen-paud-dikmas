<?php

namespace simdik_batam\Model\map;

use \RelationMap;
use \TableMap;


/**
 * This class defines the structure of the 'adm.rekap_rombel_per_jumlah' table.
 *
 *
 *
 * This map class is used by Propel to do runtime db structure discovery.
 * For example, the createSelectSql() method checks the type of a given column used in an
 * ORDER BY clause to know whether it needs to apply SQL to make the ORDER BY case-insensitive
 * (i.e. if it's a text column type).
 *
 * @package    propel.generator.simdik_batam.Model.map
 */
class RekapRombelPerJumlahTableMap extends TableMap
{

    /**
     * The (dot-path) name of this class
     */
    const CLASS_NAME = 'simdik_batam.Model.map.RekapRombelPerJumlahTableMap';

    /**
     * Initialize the table attributes, columns and validators
     * Relations are not initialized by this method since they are lazy loaded
     *
     * @return void
     * @throws PropelException
     */
    public function initialize()
    {
        // attributes
        $this->setName('adm.rekap_rombel_per_jumlah');
        $this->setPhpName('RekapRombelPerJumlah');
        $this->setClassname('simdik_batam\\Model\\RekapRombelPerJumlah');
        $this->setPackage('simdik_batam.Model');
        $this->setUseIdGenerator(false);
        // columns
        $this->addPrimaryKey('sekolah_id', 'SekolahId', 'CHAR', true, 36, null);
        $this->addColumn('nama', 'Nama', 'VARCHAR', true, 80, null);
        $this->addColumn('total', 'Total', 'BIGINT', true, 8, null);
        $this->addColumn('kurang_10', 'Kurang10', 'DECIMAL', false, 25, null);
        $this->addColumn('sepuluh_duapuluh', 'SepuluhDuapuluh', 'DECIMAL', false, 25, null);
        $this->addColumn('duapuluhsatu_tigapuluhdua', 'DuapuluhsatuTigapuluhdua', 'DECIMAL', false, 25, null);
        $this->addColumn('lebih_32', 'Lebih32', 'DECIMAL', false, 25, null);
        // validators
    } // initialize()

    /**
     * Build the RelationMap objects for this table relationships
     */
    public function buildRelations()
    {
    } // buildRelations()

} // RekapRombelPerJumlahTableMap
