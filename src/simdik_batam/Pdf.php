<?php
namespace simdik_batam;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use simdik_batam\Model\SekolahPeer;
use simdik_batam\Model\Sekolah;
use simdik_batam\Model\MstWilayahPeer;
use simdik_batam\Model\MstWilayah;
use simdik_batam\Model\Pengguna;
use simdik_batam\Model\PenggunaPeer;
use simdik_batam\Model\BentukPendidikanPeer;
use simdik_batam\Model\StatusKepemilikanPeer;
use simdik_batam\Model\PtkPeer;
use simdik_batam\Model\PtkTerdaftarPeer;
use simdik_batam\Model\PesertaDidikPeer;
use simdik_batam\Model\RegistrasiPesertaDidikPeer;
use simdik_batam\Model\AnggotaRombelPeer;
use simdik_batam\Model\RombonganBelajarPeer;
use simdik_batam\Model\TahunAjaranPeer;

class Pdf{

	function get(Request $request, Application $app){
		$return = array();

		$nama = $request->get('nama');
		
		include("mpdf/mpdf.php");

		$mpdf=new \mPDF('c','A4-L'); 
		$mpdf->SetTitle('PdF');
		// $mpdf=new mPDF('c','A4','','',32,25,27,25,16,13); 

		$mpdf->SetDisplayMode('fullpage');

		$mpdf->packTableData = true;

		$mpdf->list_indent_first_level = 0;	// 1 or 0 - whether to indent the first level of a list

		// LOAD a stylesheet
		$stylesheet = file_get_contents(dirname(__FILE__).D.'Templates'.D.'pdf'.D.'mpdfstyletables.css');
		// $stylesheet = file_get_contents('mpdfstyletables.css');

		$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
		$params_bp = System::getParamsBp($bentuk_pendidikan_id);
		
		if($request->get('tahun_ajaran_id') == 'null'){
			$tahun_ajaran_id = TA_BERJALAN;
		}else if($request->get('tahun_ajaran_id') == null){
			$tahun_ajaran_id = TA_BERJALAN;
		}else{
			$tahun_ajaran_id = $request->get('tahun_ajaran_id');
		}

		if($request->get('semester_id') == 'null'){
			$semester_id = SEMESTER_BERJALAN;
		}else if($request->get('semester_id') == null){
			$semester_id = SEMESTER_BERJALAN;
		}else{
			$semester_id = $request->get('semester_id');
		}

		$session_kode_wilayah = $request->get('kode_wilayah') ? $request->get('kode_wilayah') : $app['session']->get('kode_wilayah');
		$session_id_level_wilayah = $request->get('id_level_wilayah') ? $request->get('id_level_wilayah') : $app['session']->get('id_level_wilayah');

		
		switch ($nama) {
			case 'progres_data':
				$con = Chart::initdb();

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = 's.provinsi';
						$group_wilayah_1 = 's.kode_wilayah_provinsi';
						$group_wilayah_2 = 's.id_level_wilayah_provinsi';
						$group_wilayah_3 = 's.mst_kode_wilayah_provinsi';
						$params_wilayah ='';
						break;
					case 1:
						$col_wilayah = 's.kabupaten';
						$group_wilayah_1 = 's.kode_wilayah_kabupaten';
						$group_wilayah_2 = 's.id_level_wilayah_kabupaten';
						$group_wilayah_3 = 's.mst_kode_wilayah_kabupaten';
						$params_wilayah = ' and s.mst_kode_wilayah_kabupaten = '.$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = 's.kecamatan';
						$group_wilayah_1 = 's.kode_wilayah_kecamatan';
						$group_wilayah_2 = 's.id_level_wilayah_kecamatan';
						$group_wilayah_3 = 's.mst_kode_wilayah_kecamatan';
						$params_wilayah = ' and s.mst_kode_wilayah_kecamatan = '.$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$group_wilayah_1} AS kode_wilayah,
							{$col_wilayah} AS nama,
							{$group_wilayah_3} AS mst_kode_wilayah,
							{$group_wilayah_2} AS id_level_wilayah,
							SUM (
								CASE
								WHEN bentuk_pendidikan_id = 13 THEN
									1
								ELSE
									0
								END
							) AS sma,
							SUM (
								CASE
								WHEN pd > 0
								AND bentuk_pendidikan_id = 13 THEN
									1
								ELSE
									0
								END
							) AS kirim_sma,
							SUM (
								CASE
								WHEN bentuk_pendidikan_id = 15 THEN
									1
								ELSE
									0
								END
							) AS smk,
							SUM (
								CASE
								WHEN pd > 0
								AND bentuk_pendidikan_id = 15 THEN
									1
								ELSE
									0
								END
							) AS kirim_smk,
							SUM (
								CASE
								WHEN bentuk_pendidikan_id = 14 THEN
									1
								ELSE
									0
								END
							) AS smlb,
							SUM (
								CASE
								WHEN pd > 0
								AND bentuk_pendidikan_id = 14 THEN
									1
								ELSE
									0
								END
							) AS kirim_smlb,
							SUM (1) AS total,
							SUM (CASE WHEN pd > 0 THEN 1 ELSE 0 END) AS kirim_total,
							(
									(
										SUM(CASE WHEN s.bentuk_pendidikan_id=13 AND pd > 0 THEN 1 ELSE 0 END) + 
										SUM(CASE WHEN s.bentuk_pendidikan_id=15 AND pd > 0 THEN 1 ELSE 0 END) +
										SUM(CASE WHEN s.bentuk_pendidikan_id=14 AND pd > 0 THEN 1 ELSE 0 END)
									) /
									CAST(
										(SUM(CASE WHEN s.bentuk_pendidikan_id=13 THEN 1 ELSE 0 END) + 
										SUM(CASE WHEN s.bentuk_pendidikan_id=15 THEN 1 ELSE 0 END) +
										SUM(CASE WHEN s.bentuk_pendidikan_id=14 THEN 1 ELSE 0 END)) as NUMERIC(9,2)
									)
								) * 100 as persen
						FROM
							rekap_sekolah s
						WHERE
						semester_id = {$semester_id}
						{$params_bp}
						{$params_wilayah}
						GROUP BY
							{$group_wilayah_1},
							{$col_wilayah},
							{$group_wilayah_2},
							{$group_wilayah_3}
						order by
							persen desc";

				// return $sql;die;

				$stmt = sqlsrv_query( $con, $sql );

				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					array_push($return, $row);

					$rowCount++;
				}


				ob_start();
				include dirname(__FILE__).D.'Templates'.D.'pdf'.D.'progres_data.php';
				$html = ob_get_clean();
				break;
			case 'sekolah_rangkuman_sp':
				$con = Chart::initdb();

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = 's.provinsi';
						$group_wilayah_1 = 's.kode_wilayah_provinsi';
						$group_wilayah_2 = 's.id_level_wilayah_provinsi';
						$params_wilayah ='';
						break;
					case 1:
						$col_wilayah = 's.kabupaten';
						$group_wilayah_1 = 's.kode_wilayah_kabupaten';
						$group_wilayah_2 = 's.id_level_wilayah_kabupaten';
						$params_wilayah = ' and s.mst_kode_wilayah_kabupaten = '.$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = 's.kecamatan';
						$group_wilayah_1 = 's.kode_wilayah_kecamatan';
						$group_wilayah_2 = 's.id_level_wilayah_kecamatan';
						$params_wilayah = ' and s.mst_kode_wilayah_kecamatan = '.$session_kode_wilayah;
						break;
					case 3:
						$params_wilayah = ' and s.kode_wilayah_kecamatan = '.$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT top 300
							nama as desk,
							s.*,
							(s.ptk + s.pegawai) as ptk_total
						FROM
							rekap_sekolah s
						WHERE
							s.semester_id = {$semester_id}
							{$params_wilayah}
							{$params_bp}
						order by s.nama";

				// return $sql;die;

				$stmt = sqlsrv_query( $con, $sql );

				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					array_push($return, $row);

					$rowCount++;
				}

				ob_start();
				include dirname(__FILE__).D.'Templates'.D.'pdf'.D.'sekolah_rangkuman_sp.php';
				$html = ob_get_clean();
				break;
			case 'rangkuman':
				$con = Chart::initdb();

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = 's.provinsi';
						$group_wilayah_1 = 's.kode_wilayah_provinsi';
						$group_wilayah_2 = 's.id_level_wilayah_provinsi';
						$params_wilayah ='';
						break;
					case 1:
						$col_wilayah = 's.kabupaten';
						$group_wilayah_1 = 's.kode_wilayah_kabupaten';
						$group_wilayah_2 = 's.id_level_wilayah_kabupaten';
						$params_wilayah = ' and s.mst_kode_wilayah_kabupaten = '.$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = 's.kecamatan';
						$group_wilayah_1 = 's.kode_wilayah_kecamatan';
						$group_wilayah_2 = 's.id_level_wilayah_kecamatan';
						$params_wilayah = ' and s.mst_kode_wilayah_kecamatan = '.$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} AS desk,
							{$group_wilayah_1} AS kode_wilayah,
							{$group_wilayah_2} AS id_level_wilayah,
							sum(s.pd_kelas_10) as pd_kelas_10,
							sum(s.pd_kelas_11) as pd_kelas_11,
							sum(s.pd_kelas_12) as pd_kelas_12,
							sum(s.pd_kelas_13) as pd_kelas_13,
							sum(s.pd) as pd,
							sum(s.ptk) as ptk,
							sum(s.ptk + s.pegawai) as ptk_total,
							sum(s.pegawai) as pegawai,
							sum(s.rombel) as rombel
						FROM
							rekap_sekolah s
						where 
							s.semester_id = {$semester_id}
							{$params_bp}
							{$params_wilayah}
						GROUP BY
							{$group_wilayah_1},
							{$group_wilayah_2},
							{$col_wilayah}";

				// return $sql;die;

				$stmt = sqlsrv_query( $con, $sql );

				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					array_push($return, $row);

					$rowCount++;
				}

				ob_start();
				include dirname(__FILE__).D.'Templates'.D.'pdf'.D.'rangkuman.php';
				$html = ob_get_clean();
				break;
			case 'rangkuman_dalam':
				# code...
					ob_start();
					include dirname(__FILE__).D.'Templates'.D.'pdf'.D.'rangkuman_dalam.php';
					$html = ob_get_clean();
				break;
			case 'rekap_pd_tingkat':
				# code...
					ob_start();
					include dirname(__FILE__).D.'Templates'.D.'pdf'.D.'rekap_pd_tingkat.php';
					$html = ob_get_clean();
				break;
			case 'rekap_pd_tingkat_dalam':
				# code...
					ob_start();
					include dirname(__FILE__).D.'Templates'.D.'pdf'.D.'rekap_pd_tingkat_dalam.php';
					$html = ob_get_clean();
				break;

			default:
				ob_start();
				include dirname(__FILE__).D.'Templates'.D.'pdf'.D.'progres_data.php';
				$html = ob_get_clean();
			break;
		}

		$mpdf->WriteHTML($stylesheet,1);	// The parameter 1 tells that this is css/style only and no body/html/text
		$mpdf->WriteHTML($html,2);

		$mpdf->Output('Pdf '.$nama.'.pdf','I');
		exit;

		return "pdf ".$nama;
	}

}