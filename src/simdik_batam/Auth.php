<?php
namespace simdik_batam;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use simdik_batam\Model\PenggunaPeer;

class Auth{

	function tesCross(){
		return System::databaseCompatibility();
	}

	function session(Request $request, Application $app){
		$ref = System::databaseCompatibility();

		if(WORK_OFFLINE == true){
			return '{"session":true}';
		}else{
			if($app['session']->get('username')){
				$array = array();

				$array['session'] = true;
				$array['username'] = $app['session']->get('username');
				$array['nama'] = $app['session']->get('nama');
				$array['peran_id'] = $app['session']->get('peran_id');
				$array['pengguna_id'] = $app['session']->get('pengguna_id');
				$array['id_level_wilayah'] = $app['session']->get('id_level_wilayah');
				$array['kode_wilayah'] = $app['session']->get('kode_wilayah');
				$array['sekolah_id'] = $app['session']->get('sekolah_id');
				$array['level'] = LEVEL;
				$array['kabkota'] = KABKOTA;
				$array['labels'] = LABEL;
				$array['show_level'] = SHOW_LEVEL;
				$array['theme'] = THEME;
				$array['forceLogin'] = FORCE_LOGIN;
				$array['show_login_button'] = SHOW_LOGIN_BUTTON;
				$array['include_depag'] = INCLUDE_DEPAG;
				$array['beranda_type'] = BERANDA_TYPE;

				$sql = "select * from {$ref}peran where peran_id = ".$array['peran_id'];

				$fetch = getDataBySql($sql);
								
				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
				}
				
				$array['peran_id_str'] = $arr['nama'];

				$sql = "select * from {$ref}level_wilayah where id_level_wilayah = ".$array['id_level_wilayah'];

				$fetch = getDataBySql($sql);
								
				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
				}

				$array['id_level_wilayah_str'] = $arr['level_wilayah'];

				$sql = "select * from {$ref}mst_wilayah where kode_wilayah = ".$array['kode_wilayah'];

				$fetch = getDataBySql($sql);
								
				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
				}

				$array['kode_wilayah_str'] = $arr['nama'];
				$array['mst_kode_wilayah'] = $arr['mst_kode_wilayah'];

				$sql = "select * from {$ref}semester where semester_id = ".SEMESTER_BERJALAN;

				$fetch = getDataBySql($sql);
								
				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}
				}

				$array['semester_id_str'] = $arr['nama'];

				return json_encode($array);

			}else{
				$array = array();

				$array['session'] = false;
				$array['level'] = LEVEL;
				$array['kabkota'] = KABKOTA;
				$array['labels'] = LABEL;
				$array['forceLogin'] = FORCE_LOGIN;
				$array['theme'] = THEME;
				$array['show_level'] = SHOW_LEVEL;
				$array['show_login_button'] = SHOW_LOGIN_BUTTON;
				$array['include_depag'] = INCLUDE_DEPAG;
				$array['beranda_type'] = BERANDA_TYPE;

				return json_encode($array);
			}
		}
	}

	function login(Request $request, Application $app){
		$return = array();
		 
		$username = ($request->get('username')) ? $request->get('username') : 0;
		$password = ($request->get('password')) ? $request->get('password') : 0;

		$username = illegal_char_remover($username);
		$password = illegal_char_remover($password);

		// $c = new \Criteria();
		// $c->add(PenggunaPeer::USERNAME, $username);
		
		// $logins = PenggunaPeer::doSelect($c);

		$sql = "select lOWER( CONVERT(VARCHAR(32), HashBytes('MD5', '".$password."'), 2) ) as iPassword,* from pengguna where username = '{$username}' AND Soft_delete = 0
			AND aktif = 1";

		$fetch = getDataBySql($sql);
				
		foreach ($fetch as $f) {
			$count++;
		}

		// return var_dump($logins);
	
		if ($count > 0) {

			// return $fetch[0]['iPassword'].' - '.$fetch[0]['password'];die;
			if ($fetch[0]['password'] === $fetch[0]['iPassword']) {
	
				$app['session']->set('username', $username);
				$app['session']->set('nama', $fetch[0]['nama']);
				$app['session']->set('peran_id', $fetch[0]['peran_id']);
				$app['session']->set('ptk_id', $fetch[0]['pengguna_id']);
				$app['session']->set('kode_wilayah', $fetch[0]['kode_wilayah']);
				$app['session']->set('sekolah_id', $fetch[0]['sekolah_id']);

				// level wilayah stuff

				$sql_c1 = "select top 1 * from ref.mst_wilayah where kode_wilayah = ".$fetch[0]['kode_wilayah'];
				$fetch_c1 = getDataBySql($sql_c1);

				$app['session']->set('id_level_wilayah', $fetch_c1[0]['id_level_wilayah']);
				
				// end of level wilayah stuff


				$return['success'] = true;
				$return['message'] = "Login berhasil";
								
				// return "{ 'success' : true, 'message' : 'Login berhasil' }";
	
			} else {

				$return['success'] = false;
				$return['message'] = "<b>Password</b> yg anda masukan salah";
	
				// return "{ 'success': false,'message': '<b>Password</b> yg anda masukan salah' }";
			}
		}else{

			$return['success'] = false;
			$return['message'] = "<b>".$username."</b> tidak terdaftar. Mohon masukkan username lain";
		}
		
		return json_encode($return);
	
	}

	function logout(Request $request, Application $app){
		$app['session']->clear();
		return $app->redirect('.');
		// return var_dump($app['session']);
	}

}