<?php
namespace simdik_batam;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use simdik_batam\Model\SekolahPeer;
use simdik_batam\Model\Sekolah;
use simdik_batam\Model\MstWilayahPeer;
use simdik_batam\Model\MstWilayah;
use simdik_batam\Model\Pengguna;
use simdik_batam\Model\PenggunaPeer;
use simdik_batam\Model\BentukPendidikanPeer;
use simdik_batam\Model\StatusKepemilikanPeer;
use simdik_batam\Model\PtkPeer;
use simdik_batam\Model\PtkTerdaftarPeer;
use simdik_batam\Model\PesertaDidikPeer;
use simdik_batam\Model\RegistrasiPesertaDidikPeer;
use simdik_batam\Model\AnggotaRombelPeer;
use simdik_batam\Model\RombonganBelajarPeer;
use simdik_batam\Model\TahunAjaranPeer;

class Excel{

	function get(Request $request, Application $app){
		$ref = System::databaseCompatibility();

		$model = $request->get('model');

		$today = date('Y-m-d H:i:s');
		$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
		$params_bp = System::getParamsBp($bentuk_pendidikan_id);
		
		if($request->get('tahun_ajaran_id') == 'null'){
			$tahun_ajaran_id = TA_BERJALAN;
		}else if($request->get('tahun_ajaran_id') == null){
			$tahun_ajaran_id = TA_BERJALAN;
		}else{
			$tahun_ajaran_id = $request->get('tahun_ajaran_id');
		}

		if($request->get('semester_id') == 'null'){
			$semester_id = SEMESTER_BERJALAN;
		}else if($request->get('semester_id') == null){
			$semester_id = SEMESTER_BERJALAN;
		}else{
			$semester_id = $request->get('semester_id');
		}

		$session_kode_wilayah = $request->get('kode_wilayah') ? $request->get('kode_wilayah') : $app['session']->get('kode_wilayah');
		$session_id_level_wilayah = $request->get('id_level_wilayah') ? $request->get('id_level_wilayah') : $app['session']->get('id_level_wilayah');

		$str = '';

		switch ($model) {
			case 'DataPokokPtk':
				$keyword = $request->get('keyword');
				$prop = $request->get('prop');
				$kab = $request->get('kab');
				$kec = $request->get('kec');
				$bp_id = $request->get('bp_id');
				$ss = $request->get('ss');
				$jurusan_id = $request->get('jurusan_id');
				$jenis_ptk_id = $request->get('jenis_ptk_id');

				// if($request->get('semester_id') == 'null'){
				// 	$request->get('semester_id') = null;
				// }

				$semester_id 		= $request->get('semester_id') ? $request->get('semester_id') : SEMESTER_BERJALAN;
				$tahun_ajaran_id 	= substr($semester_id, 0,4);

				$session_kode_wilayah = $app['session']->get('kode_wilayah');
				$session_id_level_wilayah = $app['session']->get('id_level_wilayah');

				if($keyword == 'null'){
					$keyword = null;
				}
				if($prop == 'null'){
					$prop = null;
				}
				if($kab == 'null'){
					$kab = null;
				}
				if($kec == 'null'){
					$kec = null;
				}
				if($bp_id == 'null'){
					$bp_id = null;
				}
				if($ss == 'null'){
					$ss = null;
				}
				if($jurusan_id == 'null'){
					$jurusan_id = null;
				}
				if($jenis_ptk_id == 'null'){
					$jenis_ptk_id = null;
				}
				if($semester_id == 'null'){
					$semester_id = SEMESTER_BERJALAN;
				}
				if($tahun_ajaran_id == 'null'){
					$tahun_ajaran_id = TA_BERJALAN;
				}

				if($jurusan_id){
					$param_jur 	 = ",(SELECT
							count(1) as jumlah
							FROM
								pembelajaran pb
							JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = pb.rombongan_belajar_id
							JOIN jurusan_sp jsp ON jsp.jurusan_sp_id = rb.jurusan_sp_id
							JOIN ref.jurusan j ON j.jurusan_id = jsp.jurusan_id
							WHERE
							pb.Soft_delete = 0
							AND rb.Soft_delete = 0
							AND rb.semester_id = ".$semester_id."
							and pb.ptk_terdaftar_id = ptkd.ptk_terdaftar_id
							and j.jurusan_id = '".$jurusan_id."'
							GROUP BY j.nama_jurusan, j.jurusan_id) as jumlah";

					$where_jur = " and (SELECT
							count(1) as jumlah
							FROM
								pembelajaran pb
							JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = pb.rombongan_belajar_id
							JOIN jurusan_sp jsp ON jsp.jurusan_sp_id = rb.jurusan_sp_id
							JOIN ref.jurusan j ON j.jurusan_id = jsp.jurusan_id
							WHERE
							pb.Soft_delete = 0
							AND rb.Soft_delete = 0
							AND rb.semester_id = ".$semester_id."
							and pb.ptk_terdaftar_id = ptkd.ptk_terdaftar_id
							and j.jurusan_id = '".$jurusan_id."'
							GROUP BY j.nama_jurusan, j.jurusan_id) > 0";
				}else{
					$param_jur = '';
					$where_jur = '';
				}

				$sql = "SELECT
							ptk.*,
							ptk.nama AS 'nama_ptk',
							w1.nama AS kecamatan,
							w2.nama AS kabupaten,
							w3.nama AS propinsi,
							w1.kode_wilayah AS kode_kecamatan,
							w2.kode_wilayah AS kode_kab,
							w3.kode_wilayah AS kode_prop,
							jp.jenis_ptk as jenis_ptk_id_str,
							ag.nama as agama_id_str,
							lp.nama as lembaga_pengangkat_id_str,
							pg.nama as pangkat_golongan_id_str,
							sg.nama as sumber_gaji_id_str,
							sk.nama AS status_kepegawaian_id_str,
							skp.nama AS status_keaktifan_id_str,
							kl.nama as keahlian_laboratorium_id_str,
							ptkd.ptk_terdaftar_id,
							s.nama as sekolah_id_str
						FROM
							ptk ptk
						JOIN ptk_terdaftar ptkd on ptkd.ptk_id = ptk.ptk_id
						JOIN sekolah s on s.sekolah_id = ptkd.sekolah_id
						JOIN {$ref}mst_wilayah w1 ON s.kode_wilayah = w1.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w1.mst_kode_wilayah = w2.kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w2.mst_kode_wilayah = w3.kode_wilayah
						JOIN {$ref}tahun_ajaran ta on ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
						LEFT JOIN {$ref}jenis_ptk jp ON jp.jenis_ptk_id = ptk.jenis_ptk_id
						LEFT JOIN {$ref}agama ag ON ag.agama_id = ptk.agama_id
						LEFT JOIN {$ref}lembaga_pengangkat lp ON lp.lembaga_pengangkat_id = ptk.lembaga_pengangkat_id
						LEFT JOIN {$ref}pangkat_golongan pg ON pg.pangkat_golongan_id = ptk.pangkat_golongan_id
						LEFT JOIN {$ref}sumber_gaji sg ON sg.sumber_gaji_id = ptk.sumber_gaji_id
						LEFT JOIN {$ref}status_kepegawaian sk ON sk.status_kepegawaian_id = ptk.status_kepegawaian_id
						LEFT JOIN {$ref}status_keaktifan_pegawai skp ON skp.status_keaktifan_id = ptk.status_keaktifan_id
						LEFT JOIN {$ref}keahlian_laboratorium kl ON kl.keahlian_laboratorium_id = ptk.keahlian_laboratorium_id
						WHERE
							ptk.soft_delete = 0
							and ptkd.soft_delete = 0
							AND (
								(
									ptkd.jenis_keluar_id is null and ptkd.tgl_ptk_keluar is null
								) OR (
									ptkd.jenis_keluar_id is not null and ptkd.tgl_ptk_keluar > ta.tanggal_selesai
								) OR (
									ptkd.jenis_keluar_id is null
								)
							)
							{$where_jur}
							AND ta.tahun_ajaran_id = ".$tahun_ajaran_id;


				if(!empty($keyword)){
					$sql .= " AND (ptk.nama like '%".$keyword."%' or ptk.nuptk like '%".$keyword."%')";
				}

				if(!empty($ptk_id)){
					$sql .= " AND ptk.ptk_id = '".$ptk_id."'";
				}

				if($session_id_level_wilayah == 0){
					$sql .= " AND w3.mst_kode_wilayah = ".$session_kode_wilayah;
				}else if($session_id_level_wilayah == 1){
					$sql .= " AND w2.mst_kode_wilayah = ".$session_kode_wilayah;
				}else if($session_id_level_wilayah == 2){
					$sql .= " AND w1.mst_kode_wilayah = ".$session_kode_wilayah;
				}

				//filter baru
				if($prop){
					$sql .= " AND w2.mst_kode_wilayah = ".$prop;
				}

				if($kab){
					$sql .= " AND w1.mst_kode_wilayah = ".$kab;
				}

				if($kec){
					$sql .= " AND w1.kode_wilayah = ".$kec;
					}

				if($bp_id){
					$sql .= System::getParamsBp($bp_id);
				}

				if($ss){
					if($ss != 999){	
						$sql .= ' and s.status_sekolah = '.$ss;
					}
				}

				if($jenis_ptk_id){
					$sql .= " and ptk.jenis_ptk_id = ".$jenis_ptk_id;
				}

				// return $sql;die;

			 	$fetch = getDataBySql($sql);

				foreach ($fetch as $f) {
					
					foreach ($f as $fc => $v) {
						$arr[$fc] = $v;

					}

					$str .= '<Row>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['nama'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['nip'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['jenis_kelamin'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['tempat_lahir'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['tanggal_lahir'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['nik'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['niy_nigk'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['nuptk'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['status_kepegawaian_id_str'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['jenis_ptk_id_str'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['agama_id_str'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['alamat_jalan'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['rt'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['rw'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['nama_dusun'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['desa_kelurahan'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['kecamatan'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['kabupaten'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['propinsi'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['kode_pos'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['no_telepon_rumah'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['no_hp'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['email'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['sekolah_id_str'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['status_keaktifan_id_str'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['sk_cpns'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['tgl_cpns'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['sk_pengangkatan'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['tmt_pengangkatan'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['lembaga_pengangkat_id_str'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['pangkat_golongan_id_str'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['keahlian_laboratorium_id_str'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['sumber_gaji_id_str'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['nama_ibu_kandung'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['status_perkawinan'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['nama_suami_istri'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['nip_suami_istri'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['pekerjaan_suami_istri'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['tmt_pns'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['sudah_lisensi_kepala_sekolah'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['jumlah_sekolah_binaan'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['pernah_diklat_kepengawasan'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['mampu_handle_kk'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['keahlian_braille'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['keahlian_bhs_isyarat'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['npwp'].'</Data></Cell>
							    <Cell ss:StyleID="s19"><Data ss:Type="String">'.$f['kewarganegaraan'].'</Data></Cell>
							   </Row>';

					$count++;

					array_push($fetchArr, $arr);
	
				}	
				

				$fileName = "Data Pokok PTK ";
				$fileTemplate = "template-data-pokok-ptk.php";
				break;
			case 'SekolahPtk':
				$sekolah_id			= $request->get('sekolah_id');
				$ptk_id				= $request->get('ptk_id');
				$tahun_ajaran_id	= $request->get('tahun_ajaran_id') ? $request->get('tahun_ajaran_id') : TA_BERJALAN;

				// $count = SekolahPeer::doCount($c);

				$sql = "SELECT
							ptk.*, ptkd.*, jptk.jenis_ptk AS jenis_ptk_id_str,
							sk.nama AS status_kepegawaian_id_str,
							skp.nama AS status_keaktifan_id_str,
							ptk.nama AS 'nama_ptk',
							w1.nama AS kecamatan,
							w2.nama AS kabupaten,
							w3.nama AS propinsi,
							w1.kode_wilayah AS kode_kecamatan,
							w2.kode_wilayah AS kode_kab,
							w3.kode_wilayah AS kode_prop,
							ag.nama as agama_id_str,
							lp.nama as lembaga_pengangkat_id_str,
							pg.nama as pangkat_golongan_id_str,
							sg.nama as sumber_gaji_id_str,
							kl.nama as keahlian_laboratorium_id_str
						FROM
							ptk ptk
						JOIN {$ref}mst_wilayah w1 ON ptk.kode_wilayah = w1.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w1.mst_kode_wilayah = w2.kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w2.mst_kode_wilayah = w3.kode_wilayah
						JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
						JOIN {$ref}tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
						LEFT JOIN {$ref}status_kepegawaian sk ON sk.status_kepegawaian_id = ptk.status_kepegawaian_id
						LEFT JOIN {$ref}jenis_ptk jptk ON jptk.jenis_ptk_id = ptk.jenis_ptk_id
						LEFT JOIN {$ref}status_keaktifan_pegawai skp ON skp.status_keaktifan_id = ptk.status_keaktifan_id
						LEFT JOIN {$ref}lembaga_pengangkat lp ON lp.lembaga_pengangkat_id = ptk.lembaga_pengangkat_id
						LEFT JOIN {$ref}pangkat_golongan pg ON pg.pangkat_golongan_id = ptk.pangkat_golongan_id
						LEFT JOIN {$ref}agama ag on ag.agama_id = ptk.agama_id
						LEFT JOIN {$ref}sumber_gaji sg on sg.sumber_gaji_id = ptk.sumber_gaji_id
						LEFT JOIN {$ref}keahlian_laboratorium kl on kl.keahlian_laboratorium_id = ptk.keahlian_laboratorium_id
						WHERE
							ptk.Soft_delete = 0
						AND ptkd.Soft_delete = 0
						AND (
								(
									ptkd.jenis_keluar_id is null and ptkd.tgl_ptk_keluar is null
								) OR (
									ptkd.jenis_keluar_id is not null and ptkd.tgl_ptk_keluar > ta.tanggal_selesai
								) OR (
									ptkd.jenis_keluar_id is null
								)
							)
						";

				if(!empty($ptk_id)){
					$sql .= " AND ptk.ptk_id = '".$ptk_id."'";
				}

				if(!empty($sekolah_id)){
					$sql .= " AND ptkd.sekolah_id = '".$sekolah_id."'";
				}

				if(!empty($tahun_ajaran_id)){
					$sql .= " AND ptkd.tahun_ajaran_id = ".$tahun_ajaran_id;
				}

				$fetch = getDataBySql($sql);
				
				foreach ($fetch as $f) {
					
					
					$count++;
				}				

				break;
			case 'SekolahRangkumanSp2':
				$con = Chart::initdb();

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = 's.provinsi';
						$group_wilayah_1 = 's.kode_wilayah_provinsi';
						$group_wilayah_2 = 's.id_level_wilayah_provinsi';
						$params_wilayah ='';
						break;
					case 1:
						$col_wilayah = 's.kabupaten';
						$group_wilayah_1 = 's.kode_wilayah_kabupaten';
						$group_wilayah_2 = 's.id_level_wilayah_kabupaten';
						$params_wilayah = ' and s.mst_kode_wilayah_kabupaten = '.$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = 's.kecamatan';
						$group_wilayah_1 = 's.kode_wilayah_kecamatan';
						$group_wilayah_2 = 's.id_level_wilayah_kecamatan';
						$params_wilayah = ' and s.mst_kode_wilayah_kecamatan = '.$session_kode_wilayah;
						break;
					case 3:
						$params_wilayah = ' and s.kode_wilayah_kecamatan = '.$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							nama as desk,
							s.*,
							(s.ptk + s.pegawai) as ptk_total
						FROM
							rekap_sekolah s
						WHERE
							s.semester_id = {$semester_id}
							{$params_wilayah}
							{$params_bp}
						order by s.nama";

				// return $sql;die;

				$stmt = sqlsrv_query( $con, $sql );

				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					// array_push($return, $row);

					$str .= '<Row>
						    <Cell><Data ss:Type="String">'.$row['desk'].'</Data></Cell>
						    <Cell><Data ss:Type="String">'.$row['npsn'].'</Data></Cell>
						    <Cell><Data ss:Type="String">'.$row['kecamatan'].'</Data></Cell>
						    <Cell><Data ss:Type="String">'.$row['kabupaten'].'</Data></Cell>
						    <Cell ss:StyleID="s87"><Data ss:Type="String">'.$row['ptk'].'</Data></Cell>
						    <Cell ss:StyleID="s87"><Data ss:Type="String">'.$row['pegawai'].'</Data></Cell>
						    <Cell ss:StyleID="s87"><Data ss:Type="String">'.$row['ptk_total'].'</Data></Cell>
						    <Cell ss:StyleID="s88"><Data ss:Type="String">'.$row['pd_kelas_10'].'</Data></Cell>
						    <Cell ss:StyleID="s88"><Data ss:Type="String">'.$row['pd_kelas_11'].'</Data></Cell>
						    <Cell ss:StyleID="s88"><Data ss:Type="String">'.$row['pd_kelas_12'].'</Data></Cell>
						    <Cell ss:StyleID="s88"><Data ss:Type="String">'.$row['pd_kelas_13'].'</Data></Cell>
						    <Cell ss:StyleID="s88"><Data ss:Type="String">'.$row['pd'].'</Data></Cell>
						    <Cell ss:StyleID="s88"><Data ss:Type="String">'.$row['rombel'].'</Data></Cell>
						    <Cell ss:StyleID="s23"/>
						    <Cell ss:StyleID="s23"/>
						    <Cell ss:StyleID="s23"/>
						    <Cell ss:StyleID="s23"/>
						   </Row>';

					$rowCount++;
				}

				$fileName = "Rekap Sekolah";
				$fileTemplate = "template-rangkuman-sp.php";

				break;
			case 'SekolahRangkumanSp':
				$con = Chart::initdb();

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = 's.provinsi';
						$group_wilayah_1 = 's.kode_wilayah_provinsi';
						$group_wilayah_2 = 's.id_level_wilayah_provinsi';
						$params_wilayah ='';
						break;
					case 1:
						$col_wilayah = 's.kabupaten';
						$group_wilayah_1 = 's.kode_wilayah_kabupaten';
						$group_wilayah_2 = 's.id_level_wilayah_kabupaten';
						$params_wilayah = ' and s.mst_kode_wilayah_kabupaten = '.$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = 's.kecamatan';
						$group_wilayah_1 = 's.kode_wilayah_kecamatan';
						$group_wilayah_2 = 's.id_level_wilayah_kecamatan';
						$params_wilayah = ' and s.mst_kode_wilayah_kecamatan = '.$session_kode_wilayah;
						break;
					case 3:
						$params_wilayah = ' and s.kode_wilayah_kecamatan = '.$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							nama as desk,
							s.*,
							(s.ptk + s.pegawai) as ptk_total
						FROM
							rekap_sekolah s
						WHERE
							s.semester_id = {$semester_id}
							{$params_wilayah}
							{$params_bp}
						order by s.nama";

				// return $sql;die;

				$stmt = sqlsrv_query( $con, $sql );

				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					// array_push($return, $row);

					$str .= '<Row>
						    <Cell><Data ss:Type="String">'.$row['desk'].'</Data></Cell>
						    <Cell><Data ss:Type="String">'.$row['npsn'].'</Data></Cell>
						    <Cell><Data ss:Type="String">'.$row['kecamatan'].'</Data></Cell>
						    <Cell><Data ss:Type="String">'.$row['kabupaten'].'</Data></Cell>
						    <Cell ss:StyleID="s82"><Data ss:Type="String">'.$row['pd_kelas_10_laki'].'</Data></Cell>
						    <Cell ss:StyleID="s82"><Data ss:Type="String">'.$row['pd_kelas_10_perempuan'].'</Data></Cell>
						    <Cell ss:StyleID="s82"><Data ss:Type="String">'.$row['pd_kelas_10'].'</Data></Cell>
						    <Cell ss:StyleID="s82"><Data ss:Type="String">'.$row['pd_kelas_11_laki'].'</Data></Cell>
						    <Cell ss:StyleID="s82"><Data ss:Type="String">'.$row['pd_kelas_11_perempuan'].'</Data></Cell>
						    <Cell ss:StyleID="s82"><Data ss:Type="String">'.$row['pd_kelas_11'].'</Data></Cell>
						    <Cell ss:StyleID="s82"><Data ss:Type="String">'.$row['pd_kelas_12_laki'].'</Data></Cell>
						    <Cell ss:StyleID="s82"><Data ss:Type="String">'.$row['pd_kelas_12_perempuan'].'</Data></Cell>
						    <Cell ss:StyleID="s82"><Data ss:Type="String">'.$row['pd_kelas_12'].'</Data></Cell>
						    <Cell ss:StyleID="s82"><Data ss:Type="String">'.$row['pd_kelas_13_laki'].'</Data></Cell>
						    <Cell ss:StyleID="s82"><Data ss:Type="String">'.$row['pd_kelas_13_perempuan'].'</Data></Cell>
						    <Cell ss:StyleID="s82"><Data ss:Type="String">'.$row['pd_kelas_13'].'</Data></Cell>
						    <Cell ss:StyleID="s23"/>
						   </Row>';

					$rowCount++;
				}

				$fileName = "Rekap Peserta Didik Per Tingkat dan Jenis kelamin Satuan Pendidikan";
				$fileTemplate = "template-pd-tingkat-jk-sp.php";

				break;

			case 'RekapPdTingkatJenisKelamin':
				$con = Chart::initdb();

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = 's.provinsi';
						$group_wilayah_1 = 's.kode_wilayah_provinsi';
						$group_wilayah_2 = 's.id_level_wilayah_provinsi';
						$params_wilayah ='';
						break;
					case 1:
						$col_wilayah = 's.kabupaten';
						$group_wilayah_1 = 's.kode_wilayah_kabupaten';
						$group_wilayah_2 = 's.id_level_wilayah_kabupaten';
						$params_wilayah = ' and s.mst_kode_wilayah_kabupaten = '.$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = 's.kecamatan';
						$group_wilayah_1 = 's.kode_wilayah_kecamatan';
						$group_wilayah_2 = 's.id_level_wilayah_kecamatan';
						$params_wilayah = ' and s.mst_kode_wilayah_kecamatan = '.$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} AS desk,
							{$group_wilayah_1} AS kode_wilayah,
							{$group_wilayah_2} AS id_level_wilayah,
							sum(s.pd_kelas_10) as pd_kelas_10,
							sum(s.pd_kelas_10_laki) as pd_kelas_10_laki,
							sum(s.pd_kelas_10_perempuan) as pd_kelas_10_perempuan,
							sum(s.pd_kelas_11) as pd_kelas_11,
							sum(s.pd_kelas_11_laki) as pd_kelas_11_laki,
							sum(s.pd_kelas_11_perempuan) as pd_kelas_11_perempuan,
							sum(s.pd_kelas_12) as pd_kelas_12,
							sum(s.pd_kelas_12_laki) as pd_kelas_12_laki,
							sum(s.pd_kelas_12_perempuan) as pd_kelas_12_perempuan,
							sum(s.pd_kelas_13) as pd_kelas_13,
							sum(s.pd_kelas_13_laki) as pd_kelas_13_laki,
							sum(s.pd_kelas_13_perempuan) as pd_kelas_13_perempuan
						FROM
							rekap_sekolah s
						where 
							s.semester_id = {$semester_id}
							{$params_bp}
							{$params_wilayah}
						GROUP BY
							{$group_wilayah_1},
							{$group_wilayah_2},
							{$col_wilayah}";

				// return $sql;die;

				$stmt = sqlsrv_query( $con, $sql );

				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					$str .= '<Row>
							    <Cell><Data ss:Type="String">'.$row['desk'].'</Data></Cell>
							    <Cell><Data ss:Type="String">'.$row['pd_kelas_10_laki'].'</Data></Cell>
							    <Cell><Data ss:Type="String">'.$row['pd_kelas_10_perempuan'].'</Data></Cell>
							    <Cell><Data ss:Type="String">'.$row['pd_kelas_10'].'</Data></Cell>
							    <Cell ss:StyleID="s80"><Data ss:Type="String">'.$row['pd_kelas_11_laki'].'</Data></Cell>
							    <Cell ss:StyleID="s80"><Data ss:Type="String">'.$row['pd_kelas_11_perempuan'].'</Data></Cell>
							    <Cell ss:StyleID="s80"><Data ss:Type="String">'.$row['pd_kelas_11'].'</Data></Cell>
							    <Cell ss:StyleID="s80"><Data ss:Type="String">'.$row['pd_kelas_12_laki'].'</Data></Cell>
							    <Cell ss:StyleID="s80"><Data ss:Type="String">'.$row['pd_kelas_12_perempuan'].'</Data></Cell>
							    <Cell ss:StyleID="s80"><Data ss:Type="String">'.$row['pd_kelas_12'].'</Data></Cell>
							    <Cell ss:StyleID="s80"><Data ss:Type="String">'.$row['pd_kelas_13_laki'].'</Data></Cell>
							    <Cell ss:StyleID="s80"><Data ss:Type="String">'.$row['pd_kelas_13_perempuan'].'</Data></Cell>
							    <Cell ss:StyleID="s80"><Data ss:Type="String">'.$row['pd_kelas_13'].'</Data></Cell>
							    <Cell ss:StyleID="s23"/>
							   </Row>';

					$rowCount++;
				}

				$fileName = "Rekap Peserta Didik Per Tingkat dan Jenis kelamin";
				$fileTemplate = "template-pd-tingkat-jk.php";

				break;
			case 'Rangkuman':
				$con = Chart::initdb();

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = 's.provinsi';
						$group_wilayah_1 = 's.kode_wilayah_provinsi';
						$group_wilayah_2 = 's.id_level_wilayah_provinsi';
						$params_wilayah ='';
						break;
					case 1:
						$col_wilayah = 's.kabupaten';
						$group_wilayah_1 = 's.kode_wilayah_kabupaten';
						$group_wilayah_2 = 's.id_level_wilayah_kabupaten';
						$params_wilayah = ' and s.mst_kode_wilayah_kabupaten = '.$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = 's.kecamatan';
						$group_wilayah_1 = 's.kode_wilayah_kecamatan';
						$group_wilayah_2 = 's.id_level_wilayah_kecamatan';
						$params_wilayah = ' and s.mst_kode_wilayah_kecamatan = '.$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} AS desk,
							{$group_wilayah_1} AS kode_wilayah,
							{$group_wilayah_2} AS id_level_wilayah,
							sum(s.pd_kelas_10) as pd_kelas_10,
							sum(s.pd_kelas_11) as pd_kelas_11,
							sum(s.pd_kelas_12) as pd_kelas_12,
							sum(s.pd_kelas_13) as pd_kelas_13,
							sum(s.pd) as pd,
							sum(s.ptk) as ptk,
							sum(s.ptk + s.pegawai) as ptk_total,
							sum(s.pegawai) as pegawai,
							sum(s.rombel) as rombel
						FROM
							rekap_sekolah s
						where 
							s.semester_id = {$semester_id}
							{$params_bp}
							{$params_wilayah}
						GROUP BY
							{$group_wilayah_1},
							{$group_wilayah_2},
							{$col_wilayah}";

				// return $sql;die;

				$stmt = sqlsrv_query( $con, $sql );

				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					// array_push($return, $row);

					$str .= '<Row>
							    <Cell><Data ss:Type="String">'.$row['desk'].'</Data></Cell>
							    <Cell><Data ss:Type="String">'.$row['ptk'].'</Data></Cell>
							    <Cell><Data ss:Type="String">'.$row['pegawai'].'</Data></Cell>
							    <Cell><Data ss:Type="String">'.$row['ptk_total'].'</Data></Cell>
							    <Cell ss:StyleID="s18"><Data ss:Type="String">'.$row['pd_kelas_10'].'</Data></Cell>
							    <Cell ss:StyleID="s18"><Data ss:Type="String">'.$row['pd_kelas_11'].'</Data></Cell>
							    <Cell ss:StyleID="s18"><Data ss:Type="String">'.$row['pd_kelas_12'].'</Data></Cell>
							    <Cell ss:StyleID="s18"><Data ss:Type="String">'.$row['pd_kelas_13'].'</Data></Cell>
							    <Cell ss:StyleID="s18"><Data ss:Type="String">'.$row['pd'].'</Data></Cell>
							    <Cell ss:StyleID="s18"><Data ss:Type="String">'.$row['rombel'].'</Data></Cell>
							    <Cell ss:StyleID="s18"/>
							    <Cell ss:StyleID="s18"/>
							    <Cell ss:StyleID="s18"/>
							    <Cell ss:StyleID="s18"/>
							   </Row>';

					$rowCount++;
				}

				$fileName = "Rekap Rangkuman Data Pendidikan";
				$fileTemplate = "template-rangkuman.php";

				break;
			case 'ProgresData':
				$con = Chart::initdb();

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = 's.provinsi';
						$group_wilayah_1 = 's.kode_wilayah_provinsi';
						$group_wilayah_2 = 's.id_level_wilayah_provinsi';
						$group_wilayah_3 = 's.mst_kode_wilayah_provinsi';
						$params_wilayah ='';
						break;
					case 1:
						$col_wilayah = 's.kabupaten';
						$group_wilayah_1 = 's.kode_wilayah_kabupaten';
						$group_wilayah_2 = 's.id_level_wilayah_kabupaten';
						$group_wilayah_3 = 's.mst_kode_wilayah_kabupaten';
						$params_wilayah = ' and s.mst_kode_wilayah_kabupaten = '.$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = 's.kecamatan';
						$group_wilayah_1 = 's.kode_wilayah_kecamatan';
						$group_wilayah_2 = 's.id_level_wilayah_kecamatan';
						$group_wilayah_3 = 's.mst_kode_wilayah_kecamatan';
						$params_wilayah = ' and s.mst_kode_wilayah_kecamatan = '.$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$group_wilayah_1} AS kode_wilayah,
							{$col_wilayah} AS nama,
							{$group_wilayah_3} AS mst_kode_wilayah,
							{$group_wilayah_2} AS id_level_wilayah,
							SUM (
								CASE
								WHEN bentuk_pendidikan_id = 13 THEN
									1
								ELSE
									0
								END
							) AS sma,
							SUM (
								CASE
								WHEN pd > 0
								AND bentuk_pendidikan_id = 13 THEN
									1
								ELSE
									0
								END
							) AS kirim_sma,
							SUM (
								CASE
								WHEN bentuk_pendidikan_id = 15 THEN
									1
								ELSE
									0
								END
							) AS smk,
							SUM (
								CASE
								WHEN pd > 0
								AND bentuk_pendidikan_id = 15 THEN
									1
								ELSE
									0
								END
							) AS kirim_smk,
							SUM (
								CASE
								WHEN bentuk_pendidikan_id = 14 THEN
									1
								ELSE
									0
								END
							) AS smlb,
							SUM (
								CASE
								WHEN pd > 0
								AND bentuk_pendidikan_id = 14 THEN
									1
								ELSE
									0
								END
							) AS kirim_smlb,
							SUM (1) AS total,
							SUM (CASE WHEN pd > 0 THEN 1 ELSE 0 END) AS kirim_total,
							(
									(
										SUM(CASE WHEN s.bentuk_pendidikan_id=13 AND pd > 0 THEN 1 ELSE 0 END) + 
										SUM(CASE WHEN s.bentuk_pendidikan_id=15 AND pd > 0 THEN 1 ELSE 0 END) +
										SUM(CASE WHEN s.bentuk_pendidikan_id=14 AND pd > 0 THEN 1 ELSE 0 END)
									) /
									CAST(
										(SUM(CASE WHEN s.bentuk_pendidikan_id=13 THEN 1 ELSE 0 END) + 
										SUM(CASE WHEN s.bentuk_pendidikan_id=15 THEN 1 ELSE 0 END) +
										SUM(CASE WHEN s.bentuk_pendidikan_id=14 THEN 1 ELSE 0 END)) as NUMERIC(9,2)
									)
								) * 100 as persen
						FROM
							rekap_sekolah s
						WHERE
						semester_id = {$semester_id}
						{$params_bp}
						{$params_wilayah}
						GROUP BY
							{$group_wilayah_1},
							{$col_wilayah},
							{$group_wilayah_2},
							{$group_wilayah_3}
						order by
							persen desc";

				// return $sql;die;

				$stmt = sqlsrv_query( $con, $sql );

				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					// array_push($return, $row);

					$str .= '<Row>
						    <Cell><Data ss:Type="String">'.$row['nama'].'</Data></Cell>
						    <Cell><Data ss:Type="String">'.$row['sma'].'</Data></Cell>
						    <Cell><Data ss:Type="String">'.$row['kirim_sma'].'</Data></Cell>
						    <Cell><Data ss:Type="String">'.($row['sma'] - $row['kirim_sma']).'</Data></Cell>
						    <Cell><Data ss:Type="String">'.$row['smk'].'</Data></Cell>
						    <Cell><Data ss:Type="String">'.$row['kirim_smk'].'</Data></Cell>
						    <Cell><Data ss:Type="String">'.($row['smk'] - $row['kirim_smk']).'</Data></Cell>
						    <Cell><Data ss:Type="String">'.$row['smlb'].'</Data></Cell>
						    <Cell><Data ss:Type="String">'.$row['kirim_smlb'].'</Data></Cell>
						    <Cell><Data ss:Type="String">'.($row['smlb'] - $row['kirim_smlb']).'</Data></Cell>
						    <Cell><Data ss:Type="String">'.($row['sma'] + $row['smk'] + $row['smlb']).'</Data></Cell>
						    <Cell><Data ss:Type="String">'.($row['kirim_sma'] + $row['kirim_smk'] + $row['kirim_smlb']).'</Data></Cell>
						    <Cell><Data ss:Type="String">'.(($row['sma'] + $row['smk'] + $row['smlb']) - ($row['kirim_sma'] + $row['kirim_smk'] + $row['kirim_smlb'])) .'</Data></Cell>
						    <Cell><Data ss:Type="String">'.$row['persen'].'</Data></Cell>
						   </Row>';

					$rowCount++;
				}

				$fileName = "Progres Data";
				$fileTemplate = "template-progres-data.php";

				break;
			case 'SebaranPrasaranaPerJenis':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.kode_wilayah";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.kode_wilayah";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.kode_wilayah";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "select * from {$ref}mst_wilayah where mst_kode_wilayah = {$session_kode_wilayah}";

				$fetch = getDataBySql($sql);

			 	$columns = '';

			 	$str_header = '<Row ss:Index="3" ss:AutoFitHeight="0">
							    <Cell ss:StyleID="s64"><Data ss:Type="String">Jenis Prasarana</Data></Cell>
							    <Cell ss:StyleID="s64"><Data ss:Type="String">Total</Data></Cell>';

				foreach ($fetch as $f) {
					
					$kecCount++;
					// $nama_wilayah = str_replace(' ','_',strtolower(str_replace('Kec. ', '', $f['nama'])));

					$columns .= "sum(case when {$col_wilayah} = ".$f['kode_wilayah']." then 1 else 0 end) as col_".$f['kode_wilayah'].",";
					
					$str_header .= '<Cell ss:StyleID="s64"><Data ss:Type="String">'.$f['nama'].'</Data></Cell>';				
				}

				$str_header .= '</Row>';

				$sql = "SELECT
							jp.jenis_prasarana_id,
							jp.nama,
							{$columns}
							count(1) as total
						FROM
							prasarana p
						JOIN {$ref}jenis_prasarana jp ON jp.jenis_prasarana_id = p.jenis_prasarana_id
						JOIN sekolah s ON s.sekolah_id = p.sekolah_id
						JOIN {$ref}mst_wilayah w1 ON w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						where p.soft_delete = 0
						and s.soft_delete = 0
						{$params_bp}
						{$params_wilayah}
						GROUP BY
							jp.jenis_prasarana_id,
							jp.nama";

				
				$data = getDataBySql($sql);
				$i = 0;
				foreach ($data as $d) {
					$i++;

					$ps .= '<Row ss:AutoFitHeight="0">
							    <Cell><Data ss:Type="String">'.$d['nama'].'</Data></Cell>
							    <Cell><Data ss:Type="Number">'.$d['total'].'</Data></Cell>';

					foreach ($fetch as $f2) {

						$cols = 'col_'.spasi_remover($f2['kode_wilayah']);
						
						$ps .= '<Cell><Data ss:Type="Number">'.$d[$cols].'</Data></Cell>';
					}

					$ps .=  '</Row>';

				}

				
				$fileName = "Prasanara Jenis Prasarana";
				$fileTemplate = "template-prasarana-jenis-prasarana.php";
				break;
			case 'SebaranPdJenisKelamin':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				$con = Chart::initdb();

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = 's.provinsi';
						$group_wilayah_1 = 's.kode_wilayah_provinsi';
						$params_wilayah ='';
						break;
					case 1:
						$col_wilayah = 's.kabupaten';
						$group_wilayah_1 = 's.kode_wilayah_kabupaten';
						$params_wilayah = ' and s.mst_kode_wilayah_kabupaten = '.$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = 's.kecamatan';
						$group_wilayah_1 = 's.kode_wilayah_kecamatan';
						$params_wilayah = ' and s.mst_kode_wilayah_kecamatan = '.$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} AS desk,
							(sum(pd_kelas_10_laki) + sum(pd_kelas_11_laki) + sum(pd_kelas_12_laki) + sum(pd_kelas_13_laki)) as 'laki-laki',
							(sum(pd_kelas_10_perempuan) + sum(pd_kelas_11_perempuan) + sum(pd_kelas_12_perempuan) + sum(pd_kelas_13_perempuan)) as 'perempuan'
						FROM
							rekap_sekolah s
						where 
							s.semester_id = {$semester_id}
							{$params_bp}
							{$params_wilayah}
						GROUP BY
							{$group_wilayah_1},
							{$col_wilayah}";

							// return $sql;die;

				$stmt = sqlsrv_query( $con, $sql );

				$i = 0;
				
				while($d = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					$i++;
					$pdjk .= '   <Row ss:AutoFitHeight="0">
					    <Cell><Data ss:Type="String">'.$d['desk'].'</Data></Cell>
					    <Cell><Data ss:Type="Number">'.$d['laki-laki'].'</Data></Cell>
					    <Cell><Data ss:Type="Number">'.$d['perempuan'].'</Data></Cell>
							   </Row>';

				}

				
				$fileName = "Peserta Didik Jenis Kelamin";
				$fileTemplate = "template-peserta-didik-jenis-kelamin.php";
				break;
			case 'SebaranPdAngkaPutusSekolah':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} as desk,
							count(1) as jumlah
						FROM
							peserta_didik pd
						JOIN registrasi_peserta_didik rpd ON rpd.peserta_didik_id = pd.peserta_didik_id
						JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
						JOIN sekolah s ON rpd.sekolah_id = s.sekolah_id
						JOIN {$ref}mst_wilayah w1 ON w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						WHERE
							pd.Soft_delete = 0
						AND rpd.Soft_delete = 0
						AND jenis_keluar_id = '5'
						{$params_bp}
						{$params_wilayah}
						group by {$col_wilayah}
						order by {$col_wilayah}";
												// return $sql; die;
							
				$data = getDataBySql($sql);
				$i = 0;
				foreach ($data as $d) {
					$i++;
					$angkaputus .= '<Row ss:AutoFitHeight="0">
								        <Cell><Data ss:Type="String">'.$d['desk'].'</Data></Cell>
							     	    <Cell><Data ss:Type="Number">'.$d['jumlah'].'</Data></Cell>
						       		   </Row>';

				}

				
				$fileName = "Peserta Didik Angka Putus Sekolah";
				$fileTemplate = "template-peserta-didik-angka-putus-sekolah.php";
				break;
			case 'SebaranPdAngkaMengulang':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} as desk,
							count(1) as jumlah
						FROM
							peserta_didik pd
						JOIN registrasi_peserta_didik rpd ON rpd.peserta_didik_id = pd.peserta_didik_id
						JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
						JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
						JOIN sekolah s ON rpd.sekolah_id = s.sekolah_id
						JOIN {$ref}mst_wilayah w1 ON w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						WHERE
							pd.Soft_delete = 0
						AND rpd.Soft_delete = 0
						AND jenis_keluar_id IS NULL
						AND ar.jenis_pendaftaran_id = 5
						AND rb.semester_id = {$semester_id}
						{$params_bp}
						{$params_wilayah}
						group by {$col_wilayah}
						order by {$col_wilayah}";
							
				$data = getDataBySql($sql);
				$i = 0;
				foreach ($data as $d) {
					$i++;
					$angkamengulang .= '<Row ss:AutoFitHeight="0">
								        	<Cell><Data ss:Type="String">'.$d['desk'].'</Data></Cell>
							     	    	<Cell><Data ss:Type="Number">'.$d['jumlah'].'</Data></Cell>
						       		   </Row>';

				}

				
				$fileName = "Peserta Didik Angka Mengulang";
				$fileTemplate = "template-peserta-didik-angka-mengulang.php";
				break;
			case 'SebaranPdPerAgama':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} AS desk,
							sum(CASE WHEN d.agama_id = 1 THEN 1 ELSE 0 END) AS islam,
							sum(CASE WHEN d.agama_id = 2 THEN 1 ELSE 0 END) AS kristen,
							sum(CASE WHEN d.agama_id = 3 THEN 1 ELSE 0 END) AS katholik,
							sum(CASE WHEN d.agama_id = 4 THEN 1 ELSE 0 END) AS hindu,
							sum(CASE WHEN d.agama_id = 5 THEN 1 ELSE 0 END) AS budha,
							sum(CASE WHEN d.agama_id = 6 THEN 1 ELSE 0 END) AS konghucu
						FROM
							anggota_rombel a
						JOIN rombongan_belajar b ON a.rombongan_belajar_id = b.rombongan_belajar_id
						JOIN registrasi_peserta_didik c ON a.peserta_didik_id = c.peserta_didik_id
						JOIN peserta_didik d ON c.peserta_didik_id = d.peserta_didik_id
						JOIN sekolah s ON d.sekolah_id = s.sekolah_id
						JOIN {$ref}agama f ON d.agama_id = f.agama_id
						JOIN {$ref}mst_wilayah w1 on w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						WHERE
							a.soft_delete = 0
						AND b.soft_delete = 0
						AND c.soft_delete = 0
						AND d.soft_delete = 0
						AND b.semester_id = {$semester_id}
						AND c.jenis_keluar_id IS NULL
						{$params_bp}
						{$params_wilayah}
						GROUP BY
							{$col_wilayah}
						order by {$col_wilayah}";

				$data = getDataBySql($sql);
				$i = 0;
				foreach ($data as $d) {
					$i++;
							$pdagama .= '  <Row ss:AutoFitHeight="0">
										    <Cell><Data ss:Type="String">'.$d['desk'].'</Data></Cell>
										    <Cell><Data ss:Type="Number">'.$d['islam'].'</Data></Cell>
										    <Cell><Data ss:Type="Number">'.$d['kristen'].'</Data></Cell>
										    <Cell><Data ss:Type="Number">'.$d['katholik'].'</Data></Cell>
										    <Cell><Data ss:Type="Number">'.$d['hindu'].'</Data></Cell>
										    <Cell><Data ss:Type="Number">'.$d['budha'].'</Data></Cell>
										    <Cell><Data ss:Type="Number">'.$d['konghucu'].'</Data></Cell>

										   </Row>';

				}

				
				$fileName = "Peserta Didik Agama";
				$fileTemplate = "template-pd-agama.php";
				break;
			case 'SebaranPdPerUmur':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				if(DBMS == 'mssql'){
					$kriteria = "sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) < 7 then 1 else 0 end) as kurang_tujuh,
								sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) >= 7 AND (DATEDIFF(hour,pd.tanggal_lahir,GETDATE())/8766) <= 12 then 1 else 0 end) as tujuh_duabelas,
								sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) > 12 then 1 else 0 end) as lebih_duabelas,
								sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) < 15 then 1 else 0 end) as kurang_limabelas,
								sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) >= 15 AND (DATEDIFF(hour,pd.tanggal_lahir,GETDATE())/8766) <= 18 then 1 else 0 end) as limabelas_delapanbelas,
								sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) > 18 then 1 else 0 end) as lebih_delapanbelas,
								sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) < 19 then 1 else 0 end) as kurang_sembilanbelas,
								sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) >= 19 AND (DATEDIFF(hour,pd.tanggal_lahir,GETDATE())/8766) <= 21 then 1 else 0 end) as sembilanbelas_duapuluhsatu,
								sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) > 21 then 1 else 0 end) as lebih_duapuluhsatu";
				}else{
					$kriteria = "sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) < 7 then 1 else 0 end) as kurang_tujuh,
								sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) >= 7 AND (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) <= 12 then 1 else 0 end) as tujuh_duabelas,
								sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) > 12 then 1 else 0 end) as lebih_duabelas,
								sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) < 13 then 1 else 0 end) as kurang_tigabelas,
								sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) >= 13 AND (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) <= 18 then 1 else 0 end) as tigabelas_delapanbelas,
								sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) > 18 then 1 else 0 end) as lebih_delapanbelas,
								sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) < 19 then 1 else 0 end) as kurang_sembilanbelas,
								sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) >= 19 AND (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) <= 21 then 1 else 0 end) as sembilanbelas_duapuluhsatu,
								sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) > 21 then 1 else 0 end) as lebih_duapuluhsatu";
				}

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							s.sekolah_id,
							s.bentuk_pendidikan_id,
							s.nama,
							w1.nama as kecamatan,
							w2.nama as kabupaten,
							-- w1.kode_wilayah AS kode_wilayah_kecamatan,
							-- w1.nama as kecamatan,
							count(1) AS total,
							{$kriteria}
						FROM
							peserta_didik pd
						JOIN registrasi_peserta_didik rpd ON pd.peserta_didik_id = rpd.peserta_didik_id
						JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
						JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
						JOIN sekolah s ON s.sekolah_id = rpd.sekolah_id
						JOIN {$ref}mst_wilayah w1 ON w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						WHERE
							pd.Soft_delete = 0
						AND rpd.Soft_delete = 0
						AND s.Soft_delete = 0
						AND rpd.jenis_keluar_id IS NULL
						AND rb.semester_id = {$semester_id}
						{$params_bp}
						{$params_wilayah}
						GROUP BY
							s.sekolah_id,
							s.bentuk_pendidikan_id,
							s.nama,
							w1.nama,
							w2.nama
						order BY
							w2.nama, w1.nama, s.nama asc";

				$data = getDataBySql($sql);
				$i = 0;

				foreach ($data as $d) {
					$i++;
					$usia .= '<Row ss:AutoFitHeight="0">
							    <Cell><Data ss:Type="String">'.$d['nama'].'</Data></Cell>
							    <Cell><Data ss:Type="String"></Data></Cell>
							    <Cell><Data ss:Type="String">'.$d['kecamatan'].'</Data></Cell>
							    <Cell><Data ss:Type="String">'.$d['kabupaten'].'</Data></Cell>
							    <Cell><Data ss:Type="Number">'.$d['total'].'</Data></Cell>
							    <Cell><Data ss:Type="Number">'.$d['kurang_limabelas'].'</Data></Cell>
							    <Cell><Data ss:Type="Number">'.$d['limabelas_delapanbelas'].'</Data></Cell>
							    <Cell><Data ss:Type="Number">'.$d['lebih_delapanbelas'].'</Data></Cell>
							    <Cell><Data ss:Type="String"></Data></Cell>
							    <Cell><Data ss:Type="String"></Data></Cell>
							    <Cell><Data ss:Type="String"></Data></Cell>
							   </Row>';

				}

				
				$fileName = "Peserta Didik usia";
				$fileTemplate = "template-peserta-didik-umur.php";
				break;
			case 'SebaranPtkStatusKepegawaian':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} AS desk,
							sum(CASE WHEN ptk.status_kepegawaian_id = 1 THEN 1 ELSE 0 END) AS 'pns',
							sum(CASE WHEN ptk.status_kepegawaian_id = 2 THEN 1 ELSE 0 END) AS 'pns_diperbantukan',
							sum(CASE WHEN ptk.status_kepegawaian_id = 3 THEN 1 ELSE 0 END) AS 'pns_depag',
							sum(CASE WHEN ptk.status_kepegawaian_id = 4 THEN 1 ELSE 0 END) AS 'gty_pty',
							sum(CASE WHEN ptk.status_kepegawaian_id = 5 THEN 1 ELSE 0 END) AS 'gtt_ptt_provinsi',
							sum(CASE WHEN ptk.status_kepegawaian_id = 6 THEN 1 ELSE 0 END) AS 'gtt_ptt_kabupaten',
							sum(CASE WHEN ptk.status_kepegawaian_id = 7 THEN 1 ELSE 0 END) AS 'guru_bantu_pusat',
							sum(CASE WHEN ptk.status_kepegawaian_id = 8 THEN 1 ELSE 0 END) AS 'guru_honor_sekolah',
							sum(CASE WHEN ptk.status_kepegawaian_id = 9 THEN 1 ELSE 0 END) AS 'tenaga_honor_sekolah',
							sum(CASE WHEN ptk.status_kepegawaian_id = 10 THEN 1 ELSE 0 END) AS 'cpns',
							sum(CASE WHEN ptk.status_kepegawaian_id = 99 THEN 1 ELSE 0 END) AS 'lainnya'
						FROM
							ptk ptk
						JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
						JOIN {$ref}tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
						JOIN sekolah s ON ptkd.sekolah_id = s.sekolah_id
						JOIN {$ref}mst_wilayah w1 ON w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						WHERE
							ptk.soft_delete = 0
						AND ptkd.soft_delete = 0
						AND ptkd.jenis_keluar_id IS NULL
						AND (
							(
								ptkd.tgl_ptk_keluar >= ta.tanggal_mulai
								AND ptkd.tgl_ptk_keluar < ta.tanggal_selesai
							)
							OR ptkd.tgl_ptk_keluar IS NULL
						)
						{$params_bp}
						{$params_wilayah}
						AND ptkd.tahun_ajaran_id = {$tahun_ajaran_id}
						GROUP BY
							{$col_wilayah}
						order by {$col_wilayah} asc";

							/*return $sql;die;*/
				$data = getDataBySql($sql);
				$i = 0;
				foreach ($data as $d) {
					$i++;
					$rey .= '<Row ss:AutoFitHeight="0">
							  	<Cell ><Data ss:Type="String">'.$d['desk'].'</Data></Cell>
							    <Cell ><Data ss:Type="Number">'.$d['pns'].'</Data></Cell>
							    <Cell ><Data ss:Type="Number">'.$d['pns_diperbantukan'].'</Data></Cell>
							    <Cell ><Data ss:Type="Number">'.$d['pns_depag'].'</Data></Cell>
							    <Cell ><Data ss:Type="Number">'.$d['gty_pty'].'</Data></Cell>
							    <Cell ><Data ss:Type="Number">'.$d['gtt_ptt_provinsi'].'</Data></Cell>
							    <Cell ><Data ss:Type="Number">'.$d['gtt_ptt_kabupaten'].'</Data></Cell>
							    <Cell ><Data ss:Type="Number">'.$d['guru_bantu_pusat'].'</Data></Cell>
							    <Cell ><Data ss:Type="Number">'.$d['guru_honor_sekolah'].'</Data></Cell>
							    <Cell ><Data ss:Type="Number">'.$d['tenaga_honor_sekolah'].'</Data></Cell>
							    <Cell ><Data ss:Type="Number">'.$d['cpns'].'</Data></Cell>
							    <Cell ><Data ss:Type="Number">'.$d['lainnya'].'</Data></Cell>
							   </Row>';

				}

				
				$fileName = "PTK Status Kepegawaian";
				$fileTemplate = "template-ptk-status-kepegawaian.php";
				break;

			case 'SebaranPtkPerMataPelajaran':
				
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$tahun_ajaran_id = substr($semester_id, 0,4);

				$sql = "SELECT
							{$col_wilayah} as desk,
							sum(case when pb.mata_pelajaran_id = 2 or pb.mata_pelajaran_id = 4020 then 1 else 0 end) as guru_kelas_sd,
							sum(case when pb.mata_pelajaran_id = 3 or pb.mata_pelajaran_id = 101001000 then 1 else 0 end) as pendidikan_agama,
							sum(case when pb.mata_pelajaran_id = 4 or pb.mata_pelajaran_id = 2000 or pb.mata_pelajaran_id = 200010000 then 1 else 0 end) as pkn,
							sum(case when pb.mata_pelajaran_id = 5 or pb.mata_pelajaran_id = 3010 or pb.mata_pelajaran_id = 300110000 then 1 else 0 end) as bahasa_indonesia,
							sum(case when pb.mata_pelajaran_id = 7 or pb.mata_pelajaran_id = 3020 or pb.mata_pelajaran_id = 300210000 then 1 else 0 end) as bahasa_inggris,
							sum(case when pb.mata_pelajaran_id = 8 or pb.mata_pelajaran_id = 6000 or pb.mata_pelajaran_id = 600030000 then 1 else 0 end) as seni_budaya,
							sum(case when pb.mata_pelajaran_id = 9 or pb.mata_pelajaran_id = 500010000 then 1 else 0 end) as pjok,
							sum(case when pb.mata_pelajaran_id = 10 or pb.mata_pelajaran_id = 802000300 then 1 else 0 end) as tik,
							sum(case when pb.mata_pelajaran_id = 11 or pb.mata_pelajaran_id = 4200 or pb.mata_pelajaran_id = 401000000 then 1 else 0 end) as matematika,
							sum(case when pb.mata_pelajaran_id = 12 or pb.mata_pelajaran_id = 4300 or pb.mata_pelajaran_id = 401100000 then 1 else 0 end) as ipa,
							sum(case when pb.mata_pelajaran_id = 13 or pb.mata_pelajaran_id = 4400 or pb.mata_pelajaran_id = 401200000 then 1 else 0 end) as ips
						FROM
							ptk ptk
						JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
						JOIN (select DISTINCT(ptk_terdaftar_id),mata_pelajaran_id,soft_delete,semester_id from pembelajaran WHERE Soft_delete = 0) pb on pb.ptk_terdaftar_id = ptkd.ptk_terdaftar_id
						JOIN sekolah s on s.sekolah_id = ptkd.sekolah_id
						JOIN {$ref}mst_wilayah w1 on w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 on w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 on w3.kode_wilayah = w2.mst_kode_wilayah
						WHERE
							ptk.Soft_delete = 0
						AND ptkd.Soft_delete = 0
						AND pb.Soft_delete = 0
						AND ptkd.jenis_keluar_id IS NULL
						AND ptkd.tahun_ajaran_id = {$tahun_ajaran_id}
						AND pb.semester_id = {$semester_id}
						{$params_bp}
						{$params_wilayah}
						group by {$col_wilayah}
						order by {$col_wilayah} asc";

							/*return $sql;die;*/
				$data = getDataBySql($sql);
				$i = 0;
				foreach ($data as $d) {
					$i++;
					$matapelajaran .= '<Row ss:AutoFitHeight="0">
							    <Cell><Data ss:Type="String">'.$d['desk'].'</Data></Cell>
							    <Cell><Data ss:Type="Number">'.$d['matematika'].'</Data></Cell>
							    <Cell><Data ss:Type="Number">'.$d['bahasa_indonesia'].'</Data></Cell>
							    <Cell><Data ss:Type="Number">'.$d['bahasa_inggris'].'</Data></Cell>
							    <Cell><Data ss:Type="Number">'.$d['pkn'].'</Data></Cell>
							    <Cell><Data ss:Type="Number">'.$d['pendidikan_agama'].'</Data></Cell>
							    <Cell><Data ss:Type="Number">'.$d['pjok'].'</Data></Cell>
							    <Cell><Data ss:Type="Number">'.$d['seni_budaya'].'</Data></Cell>
							    <Cell><Data ss:Type="Number">'.$d['tik'].'</Data></Cell>
							    <Cell><Data ss:Type="Number">'.$d['ipa'].'</Data></Cell>
							    <Cell><Data ss:Type="Number">'.$d['ips'].'</Data></Cell>
							   </Row>';

				}

				
				$fileName = "PTK Mata Pelajaran";
				$fileTemplate = "template-ptk-mata-pelajaran.php";
				break;

			case 'ptkJenisKelamin':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} AS desk,
							sum(CASE WHEN ptk.jenis_kelamin = 'L' THEN 1 ELSE 0 END) AS 'laki-laki',
							sum(CASE WHEN ptk.jenis_kelamin = 'P' THEN 1 ELSE 0 END) AS 'perempuan'
						FROM
							ptk ptk
						JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
						JOIN {$ref}tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
						JOIN sekolah s ON ptkd.sekolah_id = s.sekolah_id
						JOIN {$ref}mst_wilayah w1 on w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						WHERE
							ptk.soft_delete = 0
						AND ptkd.soft_delete = 0
						AND ptkd.jenis_keluar_id IS NULL
						AND (
							(
								ptkd.tgl_ptk_keluar >= ta.tanggal_mulai
								AND ptkd.tgl_ptk_keluar < ta.tanggal_selesai
							)
							OR ptkd.tgl_ptk_keluar IS NULL
						)
						AND ptkd.tahun_ajaran_id = {$tahun_ajaran_id}
						{$params_bp}
						{$params_wilayah}
						GROUP BY
							{$col_wilayah}
						order by {$col_wilayah} asc";	

				$data = getDataBySql($sql);
				$i = 0;
				foreach ($data as $d) {
					$i++;
					$pjk .= '<Row ss:AutoFitHeight="0">
							    <Cell><Data ss:Type="String">'.$d['desk'].'</Data></Cell>
							    <Cell><Data ss:Type="Number">'.$d['laki-laki'].'</Data></Cell>
							    <Cell><Data ss:Type="Number">'.$d['perempuan'].'</Data></Cell>
							   </Row>';

				}

				
				$fileName = "PTK Jenis kelamin";
				$fileTemplate = "template-ptk-jenis-kelamin.php";
				break;

			case 'SebaranPtkPerAgama':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} AS desk,
							sum(CASE WHEN ptk.agama_id = 1 THEN 1 ELSE 0 END) AS islam,
							sum(CASE WHEN ptk.agama_id = 2 THEN 1 ELSE 0 END) AS kristen,
							sum(CASE WHEN ptk.agama_id = 3 THEN 1 ELSE 0 END) AS katholik,
							sum(CASE WHEN ptk.agama_id = 4 THEN 1 ELSE 0 END) AS hindu,
							sum(CASE WHEN ptk.agama_id = 5 THEN 1 ELSE 0 END) AS budha,
							sum(CASE WHEN ptk.agama_id = 6 THEN 1 ELSE 0 END) AS konghucu
						FROM
							ptk ptk
						JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
						JOIN {$ref}tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
						JOIN sekolah s ON ptkd.sekolah_id = s.sekolah_id
						JOIN {$ref}agama a ON ptk.agama_id = a.agama_id
						JOIN {$ref}mst_wilayah w1 on w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						WHERE
							ptk.soft_delete = 0
						AND ptkd.soft_delete = 0
						AND ptkd.jenis_keluar_id IS NULL
						AND (
							(
								ptkd.tgl_ptk_keluar >= ta.tanggal_mulai
								AND ptkd.tgl_ptk_keluar < ta.tanggal_selesai
							)
							OR ptkd.tgl_ptk_keluar IS NULL
						)
						AND ptkd.tahun_ajaran_id = {$tahun_ajaran_id}
						{$params_bp}
						{$params_wilayah}
						GROUP BY
							{$col_wilayah}
						order by {$col_wilayah} asc";

				$data = getDataBySql($sql);
				$i = 0;
				foreach ($data as $d) {
					$i++;
							$agama .= '<Row ss:AutoFitHeight="0">
									    <Cell><Data ss:Type="String">'.$d['desk'].'</Data></Cell>
									    <Cell><Data ss:Type="Number">'.$d['islam'].'</Data></Cell>
									    <Cell><Data ss:Type="Number">'.$d['kristen'].'</Data></Cell>
									    <Cell><Data ss:Type="Number">'.$d['katholik'].'</Data></Cell>
									    <Cell><Data ss:Type="Number">'.$d['hindu'].'</Data></Cell>
									    <Cell><Data ss:Type="Number">'.$d['budha'].'</Data></Cell>
									    <Cell><Data ss:Type="Number">'.$d['konghucu'].'</Data></Cell>
									</Row>';

				}

				
				$fileName = "PTK Agama";
				$fileTemplate = "template-ptk-agama.php";
				
				break;
			case 'SekolahPerWaktuPenyelenggaraan':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} as desk,
							sum(case when sl.waktu_penyelenggaraan_id = 1 then 1 else 0 end) as pagi,
							sum(case when sl.waktu_penyelenggaraan_id = 2 then 1 else 0 end) as siang,
							sum(case when sl.waktu_penyelenggaraan_id = 3 then 1 else 0 end) as kombinasi,
							sum(case when sl.waktu_penyelenggaraan_id = 4 then 1 else 0 end) as sore,
							sum(case when sl.waktu_penyelenggaraan_id = 5 then 1 else 0 end) as malam,
							sum(case when sl.waktu_penyelenggaraan_id = 6 then 1 else 0 end) as sehari_penuh_5,
							sum(case when sl.waktu_penyelenggaraan_id = 7 then 1 else 0 end) as sehari_penuh_6,
							sum(case when sl.waktu_penyelenggaraan_id = 9 then 1 else 0 end) as lainnya
						FROM
							sekolah s
							JOIN {$ref}mst_wilayah w1 ON w1.kode_wilayah = s.kode_wilayah
							JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
							JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
							JOIN sekolah_longitudinal sl ON s.sekolah_id = sl.sekolah_id
							LEFT JOIN {$ref}waktu_penyelenggaraan ON sl.waktu_penyelenggaraan_id = waktu_penyelenggaraan.waktu_penyelenggaraan_id
						WHERE
							s.Soft_delete = 0
						AND sl.semester_id = {$semester_id}
						AND s.kode_wilayah != 000000		
						{$params_bp}
						{$params_wilayah}			
						GROUP BY
							{$col_wilayah}";
						/*return $sql; die;	*/
				$data = getDataBySql($sql);
				$i = 0;
				foreach ($data as $d) {
					$i++;
					$sel .= '<Row>
							    <Cell><Data ss:Type="String">'.$d['desk'].'</Data></Cell>
							    <Cell><Data ss:Type="Number">'.$d['pagi'].'</Data></Cell>
							    <Cell><Data ss:Type="Number">'.$d['siang'].'</Data></Cell>
							    <Cell><Data ss:Type="Number">'.$d['kombinasi'].'</Data></Cell>
							    <Cell><Data ss:Type="Number">'.$d['sore'].'</Data></Cell>
							    <Cell><Data ss:Type="Number">'.$d['malam'].'</Data></Cell>
							    <Cell><Data ss:Type="Number">'.$d['sehari_penuh_5'].'</Data></Cell>
							    <Cell><Data ss:Type="Number">'.$d['sehari_penuh_6'].'</Data></Cell>
							    <Cell><Data ss:Type="Number">'.$d['lainnya'].'</Data></Cell>
							</Row>';

				}

				
				$fileName = "Sekolah PerWaktu Penyelenggaraan";
				$fileTemplate = "template-sekolah-waktu-penyelenggaraan.php";

				break;

			case 'TestingExcel':
				$bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				if(DBMS == 'mssql'){
					$kriteria = "sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) < 7 then 1 else 0 end) as kurang_tujuh,
								sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) >= 7 AND (DATEDIFF(hour,pd.tanggal_lahir,GETDATE())/8766) <= 12 then 1 else 0 end) as tujuh_duabelas,
								sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) > 12 then 1 else 0 end) as lebih_duabelas,
								sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) < 13 then 1 else 0 end) as kurang_tigabelas,
								sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) >= 13 AND (DATEDIFF(hour,pd.tanggal_lahir,GETDATE())/8766) <= 18 then 1 else 0 end) as tigabelas_delapanbelas,
								sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) > 18 then 1 else 0 end) as lebih_delapanbelas,
								sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) < 19 then 1 else 0 end) as kurang_sembilanbelas,
								sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) >= 19 AND (DATEDIFF(hour,pd.tanggal_lahir,GETDATE())/8766) <= 21 then 1 else 0 end) as sembilanbelas_duapuluhsatu,
								sum(case when (DATEDIFF(HOUR,pd.tanggal_lahir,GETDATE())/8766) > 21 then 1 else 0 end) as lebih_duapuluhsatu";
				}else{
					$kriteria = "sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) < 7 then 1 else 0 end) as kurang_tujuh,
								sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) >= 7 AND (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) <= 12 then 1 else 0 end) as tujuh_duabelas,
								sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) > 12 then 1 else 0 end) as lebih_duabelas,
								sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) < 13 then 1 else 0 end) as kurang_tigabelas,
								sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) >= 13 AND (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) <= 18 then 1 else 0 end) as tigabelas_delapanbelas,
								sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) > 18 then 1 else 0 end) as lebih_delapanbelas,
								sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) < 19 then 1 else 0 end) as kurang_sembilanbelas,
								sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) >= 19 AND (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) <= 21 then 1 else 0 end) as sembilanbelas_duapuluhsatu,
								sum(case when (DATE_FORMAT(FROM_DAYS(TO_DAYS(NOW())-TO_DAYS(pd.tanggal_lahir)), '%Y')+0) > 21 then 1 else 0 end) as lebih_duapuluhsatu";
				}

				$sql = "SELECT
							s.sekolah_id,
							s.bentuk_pendidikan_id,
							s.nama,
							-- w1.kode_wilayah AS kode_wilayah_kecamatan,
							-- w1.nama as kecamatan,
							count(1) AS total,
							{$kriteria}
						FROM
							peserta_didik pd
						JOIN registrasi_peserta_didik rpd ON pd.peserta_didik_id = rpd.peserta_didik_id
						JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
						JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
						JOIN sekolah s ON s.sekolah_id = rpd.sekolah_id
						JOIN {$ref}mst_wilayah w1 ON w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						WHERE
							pd.Soft_delete = 0
						AND rpd.Soft_delete = 0
						AND s.Soft_delete = 0
						AND rpd.jenis_keluar_id IS NULL
						AND rb.semester_id = 20141
						{$params_bp}
						GROUP BY
							s.sekolah_id,
							s.bentuk_pendidikan_id,
							s.nama";

				$data = getDataBySql($sql);
				$i = 0;

				foreach ($data as $d) {
					$i++;
					$tes .= '<Row ss:AutoFitHeight="0">
							    <Cell><Data ss:Type="String">'.$d['sekolah_id'].'</Data></Cell>
							    <Cell><Data ss:Type="String">'.$d['nama'].'</Data></Cell>
							   </Row>';

				}

				
				$fileName = "testing bro";
				$fileTemplate = "template-testing-excel.php";

				// $tes = "oke bro";

				break;
			case 'dataPokokSekolah':
				
				$keyword = $request->get('keyword');
				$kec = $request->get('kec');
				$bp_id = $request->get('bp_id');
				$ss_id = $request->get('ss_id');

				$c = new \Criteria();
				$c->add(MstWilayahPeer::KODE_WILAYAH, KODE_WILAYAH);
				$fetchs = MstWilayahPeer::doSelect($c);

				foreach ($fetchs as $f) {
					$namaKab = $f->getNama();

					$c = new \Criteria();
					$c->add(MstWilayahPeer::KODE_WILAYAH, $f->getMstKodeWilayah());

					$fetchs = MstWilayahPeer::doSelect($c);

					foreach ($fetchs as $r) {
						$namaProp = $r->getNama();
					}

				}

				if(!empty($bp_id)){
					$d = new \Criteria();
					$d->add(BentukPendidikanPeer::BENTUK_PENDIDIKAN_ID, $bp_id);
					$fetchs = BentukPendidikanPeer::doSelect($d);

					foreach ($fetchs as $f) {
						$bp_id_str = $f->getNama();
					}

				}else{
					$bp_id_str = 'Semua';
				}

				if($ss_id == 1){
					$ss_id_str = 'Negeri';
				}else if($ss_id == 2){
					$ss_id_str = 'Swasta';
				}else{
					$ss_id_str = 'Semua';
				}

				if(empty($kec)){
					$namaKec = 'Semua';
				}else{
					$c = new \Criteria();
					$c->add(MstWilayahPeer::KODE_WILAYAH, $kec);
					$fetchs = MstWilayahPeer::doSelect($c);

					foreach ($fetchs as $f) {
						$namaKec = $f->getNama();
					}
				}
	
				$fileName = "Data Pokok Sekolah - ".spasi_remover($namaKab);
				$fileTemplate = "template-data-pokok-sekolah.php";

				// now get the data
				$c = new \Criteria();
				$c->add(SekolahPeer::SOFT_DELETE,0);
				$c->addJoin(SekolahPeer::BENTUK_PENDIDIKAN_ID,BentukPendidikanPeer::BENTUK_PENDIDIKAN_ID,\Criteria::JOIN);
				$c->addJoin(SekolahPeer::KODE_WILAYAH,MstWilayahPeer::KODE_WILAYAH,\Criteria::JOIN);
				$c->addJoin(SekolahPeer::STATUS_KEPEMILIKAN_ID,StatusKepemilikanPeer::STATUS_KEPEMILIKAN_ID,\Criteria::JOIN);

				if(!empty($kec)){
					$c->add(SekolahPeer::KODE_WILAYAH, $kec);
				}

				if(!empty($bp_id)){
					$c->add(SekolahPeer::BENTUK_PENDIDIKAN_ID, $bp_id, \Criteria::EQUAL);	
				}
				
				if(!empty($ss_id)){
					$c->add(SekolahPeer::STATUS_SEKOLAH, $ss_id, \Criteria::EQUAL);	
				}

				if(!empty($keyword)){
					// $c->add();
					$az = $c->getNewCriterion(SekolahPeer::NAMA, '%'.$keyword.'%', \Criteria::LIKE);
			        $bz = $c->getNewCriterion(SekolahPeer::NPSN, '%'.$keyword.'%', \Criteria::LIKE);
			        
					$az->addOr($bz);
        			$c->add($az);
				}

				$fetchs = SekolahPeer::doSelect($c);

				foreach ($fetchs as $f) {
					$i++;

					switch ($f->getStatusSekolah()) {
						case 1:
							$status = 'Negeri';
							break;
						case 2:
							$status = 'Swasta';
							break;
					}

					switch ($f->getMbs()) {
						case 1:
							$mbs = 'Ya';
							break;
						case 2:
							$mbs = 'Tidak';
							break;
					}
					
					$str .= '<Row>
							    <Cell ss:StyleID="s78"><Data ss:Type="Number">'.$i.'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getNama().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getNss().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getNpsn().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getBentukPendidikan()->getNama().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$status.'</Data></Cell>
							    <Cell ss:MergeAcross="1" ss:StyleID="m438339860"><Data ss:Type="String">'.$f->getAlamatJalan().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getRt().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getRw().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getNamaDusun().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getDesaKelurahan().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getMstWilayah()->getNama().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$namaKab.'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$namaProp.'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getKodePos().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getLintang().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getBujur().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getNomorTelepon().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getNomorFax().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getEmail().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getWebsite().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getSkPendirianSekolah().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getTanggalSkPendirian().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getStatusKepemilikan()->getNama().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getSkIzinOperasional().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getTanggalSkIzinOperasional().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getNoRekening().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getNamaBank().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getCabangKcpUnit().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getRekeningAtasNama().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$mbs.'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getLuasTanahMilik().'</Data></Cell>
							    <Cell ss:StyleID="s79"><Data ss:Type="String">'.$f->getLuasTanahBukanMilik().'</Data></Cell>
							   </Row>';

				}

				break;
			case 'ProfilSekolah':
				$sekolah_id = $request->get('sekolah_id');

				$sql = "select 
							s.*,
							bp.nama as bentuk_pendidikan_id_str,
							w1.nama as kecamatan,
							w2.nama as kabupaten,
							w3.nama as propinsi,
							sk.nama as status_kepemilikan_id_str 
						from 
							sekolah s
						join {$ref}bentuk_pendidikan bp on bp.bentuk_pendidikan_id = s.bentuk_pendidikan_id
						join {$ref}mst_wilayah w1 on w1.kode_wilayah = s.kode_wilayah
						join {$ref}mst_wilayah w2 on w2.kode_wilayah = w1.mst_kode_wilayah
						join {$ref}mst_wilayah w3 on w3.kode_wilayah = w2.mst_kode_wilayah
						join {$ref}status_kepemilikan sk on sk.status_kepemilikan_id = s.status_kepemilikan_id
						where 
							s.soft_delete = 0 
						and s.sekolah_id = '{$sekolah_id}'";

				$data = getDataBySql($sql);
				$i = 0;

				foreach ($data as $d) {
					$i++;

					$namaSekolah = $d['nama'];
					$npsn = $d['npsn'];
					$bentukPendidikan = $d['bentuk_pendidikan_id_str'];
					$alamatJalan = $d['alamat_jalan'];
					$rt = $d['rt'];
					$rw = $d['rw'];
					$namaDusun = $d['nama_dusun'];
					$desaKelurahan= $d['desa_kelurahan'];
					$kecamatan = $d['kecamatan'];
					$kodePos = $d['kode_pos'];
					$lintang = $d['lintang'];
					$bujur = $d['bujur'];
					$nomorTelepon = $d['nomor_telepon'];
					$nomorFax = $d['nomor_fax'];
					$email = $d['email'];
					$website = $d['website'];
					$skPendirianSekolah = $d['sk_pendirian_sekolah'];
					$tanggalSkPendirian = $d['tanggal_sk_pendirian'];
					$statusKepemilikan = $d['yayasan'];
					$skIzinOperasional = $d['sk_izin_operasional'];
					$tanggalSkIzinOperasional = $d['tanggal_sk_izin_operasional'];
					$nomorRekening = $d['nomor_rekening'];
					$namaBank = $d['nama_bank'];
					$cabangKcpUnit = $d['cabang_kcp_unit'];
					$rekeningAtasNama = $d['rekening_atas_nama'];
					$luasTanahMilik = $d['luas_tanah_milik'];
					$luasTanahBukanMilik = $d['luas_tanah_bukan_milik'];
					$namaProp = $d['propinsi'];
					$namaKab = $d['kabupaten'];

					switch ($d['status_sekolah']) {
						case 1:
							$status = 'Negeri';
							break;
						case 2:
							$status = 'Swasta';
							break;
					}

					switch ($d['mbs']) {
						case 1:
							$mbs = 'Ya';
							break;
						case 2:
							$mbs = 'Tidak';
							break;
					}

					$sql_ptk = "select 
								count(1) as total,
								sum(case when ptk.jenis_kelamin = 'L' then 1 else 0 end) as 'ptk_laki', 
								sum(case when ptk.jenis_kelamin = 'P' then 1 else 0 end) as 'ptk_perempuan', 
								sum(case when ptk.jenis_ptk_id in (3,4,5,6) then 1 else 0 end) as 'ptk_guru', 
								sum(case when ptk.jenis_ptk_id > 11 then 1 else 0 end) as 'ptk_lain', 
								sum(case when ptk.jenis_ptk_id = 11 then 1 else 0 end) as 'ptk_ta' 
							from 
								ptk ptk 
							join ptk_terdaftar ptkt on ptkt.ptk_id = ptk.ptk_id
							where 
								ptk.soft_delete = 0
							and ptkt.soft_delete = 0
							and ptkt.sekolah_id = '".$d['sekolah_id']."'
							";

					$data_ptk = getDataBySql($sql_ptk);

					foreach ($data_ptk as $dptk) {
						$countPtk = $dptk['total'];
						$countPtkLaki = $dptk['ptk_laki'];
						$countPtkPerempuan = $dptk['ptk_perempuan'];
						$countGuru = $dptk['ptk_guru'];
						$countTA = $dptk['ptk_ta'];
					}

					$sql_ta = "select nama from {$ref}tahun_ajaran where tahun_ajaran_id = ".TA_BERJALAN;

					$data_ta = getDataBySql($sql_ta);

					foreach ($data_ta as $dta) {
						$ta_str = $dta['nama'];
					}

					// return json_encode($data_ptk);die;

					$sql_pd = "SELECT
									COUNT (1) AS jumlah,
									sum(case when pd.jenis_kelamin = 'L' then 1 else 0 end) as 'pd_laki',
									sum(case when pd.jenis_kelamin = 'P' then 1 else 0 end) as 'pd_perempuan'
								FROM
									registrasi_peserta_didik rpd
								INNER JOIN peserta_didik pd ON rpd.peserta_didik_id = pd.peserta_didik_id
								INNER JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
								INNER JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
								JOIN {$ref}semester sm ON sm.semester_id = rb.semester_id
								WHERE
									rpd.Soft_delete = 0
								AND pd.Soft_delete = 0
								AND (
									(
										rpd.jenis_keluar_id IS NULL
										AND rpd.tanggal_keluar IS NULL
									)
									OR (
										rpd.jenis_keluar_id IS NOT NULL
										AND rpd.tanggal_keluar > sm.tanggal_selesai
									)
									OR (rpd.jenis_keluar_id IS NULL)
								)
								AND rb.semester_id = '20141'
								AND rpd.sekolah_id = '".$d['sekolah_id']."'
								AND rb.soft_delete = 0
								AND ar.soft_delete = 0";

					$data_pd = getDataBySql($sql_pd);

					foreach ($data_pd as $dpd) {
						$pdTotal = $dpd['jumlah'];
						$pdLaki = $dpd['pd_laki'];
						$pdPerempuan = $dpd['pd_perempuan'];
					}

					// return json_encode($data_pd);die;
					
				}

				// return json_encode($data);
				$fileName = "Profil Sekolah - ".spasi_remover($namaSekolah);
				$fileTemplate = "template-profil-sekolah.php";

				break;
			case 'ProfilSekolah2':
				$sekolah_id = $request->get('sekolah_id');
				// return $sekolah_id;die;
				$c = new \Criteria();
				$c->add(SekolahPeer::SOFT_DELETE,0);
				$c->add(SekolahPeer::SEKOLAH_ID,$sekolah_id);
				$c->addJoin(SekolahPeer::BENTUK_PENDIDIKAN_ID,BentukPendidikanPeer::BENTUK_PENDIDIKAN_ID,\Criteria::JOIN);
				$c->addJoin(SekolahPeer::KODE_WILAYAH,MstWilayahPeer::KODE_WILAYAH,\Criteria::JOIN);
				$c->addJoin(SekolahPeer::STATUS_KEPEMILIKAN_ID,StatusKepemilikanPeer::STATUS_KEPEMILIKAN_ID,\Criteria::JOIN);

				$fetchs = SekolahPeer::doSelect($c);

				// return var_dump($fetchs);die;

				foreach ($fetchs as $f) {
					$count++;

					$namaSekolah = $f->getNama();
					$npsn = $f->getNpsn();
					$bentukPendidikan = $f->getBentukPendidikan()->getNama();
					$alamatJalan = $f->getAlamatJalan();
					$rt = $f->getRt();
					$rw = $f->getRw();
					$namaDusun = $f->getNamaDusun();
					$desaKelurahan= $f->getDesaKelurahan();
					$kecamatan = $f->getMstWilayah()->getNama();
					$kodePos = $f->getKodePos();
					$lintang = $f->getLintang();
					$bujur = $f->getBujur();
					$nomorTelepon = $f->getNomorTelepon();
					$nomorFax = $f->getNomorFax();
					$email = $f->getEmail();
					$website = $f->getWebsite();
					$skPendirianSekolah = $f->getSkPendirianSekolah();
					$tanggalSkPendirian = $f->getTanggalSkPendirian();
					$statusKepemilikan = $f->getStatusKepemilikan()->getNama();
					$skIzinOperasional = $f->getSkIzinOperasional();
					$tanggalSkIzinOperasional = $f->getTanggalSkIzinOperasional();
					$nomorRekening = $f->getNoRekening();
					$namaBank = $f->getNamaBank();
					$cabangKcpUnit = $f->getCabangKcpUnit();
					$rekeningAtasNama = $f->getRekeningAtasNama();
					$luasTanahMilik = $f->getLuasTanahMilik();
					$luasTanahBukanMilik = $f->getLuasTanahBukanMilik();

					// $skAkreditasi = $f->getSkAkreditasi();
					// $tanggalSkAkreditasi = $f->getTanggalSkAkreditasi();

					switch ($f->getStatusSekolah()) {
						case 1:
							$status = 'Negeri';
							break;
						case 2:
							$status = 'Swasta';
							break;
					}

					switch ($f->getMbs()) {
						case 1:
							$mbs = 'Ya';
							break;
						case 2:
							$mbs = 'Tidak';
							break;
					}

					$c = new \Criteria();
					$c->add(MstWilayahPeer::KODE_WILAYAH, $f->getMstWilayah()->getMstKodeWilayah());
					$kabs = MstWilayahPeer::doSelect($c);

					foreach ($kabs as $k) {
						$namaKab = $k->getNama();

						$c = new \Criteria();
						$c->add(MstWilayahPeer::KODE_WILAYAH, $k->getMstKodeWilayah());
						$prop = MstWilayahPeer::doSelect($c);

						foreach ($prop as $p) {
							$namaProp = $p->getNama();
						}
					}

					//ngitung PTK
					$c = new \Criteria();
					$c->addJoin(PtkPeer::PTK_ID,PtkTerdaftarPeer::PTK_ID,\Criteria::JOIN);
					$c->add(PtkPeer::SOFT_DELETE,0);
					$c->add(PtkTerdaftarPeer::SOFT_DELETE,0);
					$c->add(PtkTerdaftarPeer::SEKOLAH_ID,$sekolah_id);
					$c->add(PtkTerdaftarPeer::JENIS_KELUAR_ID,NULL,\Criteria::ISNULL);
					$c->add(PtkTerdaftarPeer::TAHUN_AJARAN_ID,TA_BERJALAN);

					//ptk total
					$countPtk = PtkPeer::doCount($c);
					
					$c->add(PtkPeer::JENIS_KELAMIN,'L');

					//ptk laki2
					$countPtkLaki = PtkPeer::doCount($c);

					$c->add(PtkPeer::JENIS_KELAMIN,'P');

					//ptk perempuan
					$countPtkPerempuan = PtkPeer::doCount($c);

					$c->add(PtkPeer::JENIS_KELAMIN, array('L','P'), \Criteria::IN);
					$c->add(PtkPeer::JENIS_PTK_ID, array(3,4,5,6), \Criteria::IN);

					//ptk guru
					$countGuru = PtkPeer::doCount($c);

					$c->add(PtkPeer::JENIS_PTK_ID, array(7,8,9,10), \Criteria::IN);

					//ptk pengawas
					$countPengawas = PtkPeer::doCount($c);

					$c->add(PtkPeer::JENIS_PTK_ID, array(11), \Criteria::IN);

					//ptk administrasi
					$countTA = PtkPeer::doCount($c);

					$c = new \Criteria();
					$c->addJoin(PesertaDidikPeer::PESERTA_DIDIK_ID,RegistrasiPesertaDidikPeer::PESERTA_DIDIK_ID,\Criteria::JOIN);
					$c->addJoin(PesertaDidikPeer::PESERTA_DIDIK_ID,AnggotaRombelPeer::PESERTA_DIDIK_ID,\Criteria::JOIN);
					$c->addJoin(AnggotaRombelPeer::ROMBONGAN_BELAJAR_ID,RombonganBelajarPeer::ROMBONGAN_BELAJAR_ID,\Criteria::JOIN);
					$c->add(PesertaDidikPeer::SOFT_DELETE,0);
					$c->add(RegistrasiPesertaDidikPeer::SOFT_DELETE,0);
					$c->add(RegistrasiPesertaDidikPeer::JENIS_KELUAR_ID,NULL,\Criteria::ISNULL);
					$c->add(RombonganBelajarPeer::SEMESTER_ID,SEMESTER_BERJALAN);
					$c->add(RombonganBelajarPeer::SEKOLAH_ID,$sekolah_id);

					$pdTotal = PesertaDidikPeer::doCount($c);

					$c->add(PesertaDidikPeer::JENIS_KELAMIN,'L');

					$pdLaki = PesertaDidikPeer::doCount($c);

					$c->add(PesertaDidikPeer::JENIS_KELAMIN,'P');

					$pdPerempuan = PesertaDidikPeer::doCount($c);

					//ta
					$c = new \Criteria();
					$c->add(TahunAjaranPeer::TAHUN_AJARAN_ID, TA_BERJALAN);

					$ta = TahunAjaranPeer::doSelect($c);

					foreach ($ta as $ta) {
						$ta_str = $ta->getNama();  
					}
				}

				// return $count;die;

				$fileName = "Profil Sekolah - ".spasi_remover($namaSekolah);
				$fileTemplate = "template-profil-sekolah.php";

				break;
			case 'sekolahStatus':
				// return $session_id_level_wilayah;die;

				$con = Chart::initdb();
				// $bentuk_pendidikan_id = $request->get('bentuk_pendidikan_id');
				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = 's.provinsi';
						$group_wilayah_1 = 's.kode_wilayah_provinsi';
						$params_wilayah ='';
						break;
					case 1:
						$col_wilayah = 's.kabupaten';
						$group_wilayah_1 = 's.kode_wilayah_kabupaten';
						$params_wilayah = ' and s.mst_kode_wilayah_kabupaten = '.$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = 's.kecamatan';
						$group_wilayah_1 = 's.kode_wilayah_kecamatan';
						$params_wilayah = ' and s.mst_kode_wilayah_kecamatan = '.$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} AS desk,
							sum(case when s.status_sekolah = 1 then 1 else 0 end) as negeri,
							sum(case when s.status_sekolah = 2 then 1 else 0 end) as swasta
						FROM
							rekap_sekolah s
						where 
							s.semester_id = {$semester_id}
							{$params_bp}
							{$params_wilayah}
						GROUP BY
							{$group_wilayah_1},
							{$col_wilayah}";

				$stmt = sqlsrv_query( $con, $sql );
				$i = 0;

				// return $sql;die;

				while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					$i++;
					$str .= '<Row>
						    <Cell><Data ss:Type="Number">'.$i.'</Data></Cell>
						    <Cell><Data ss:Type="String">'.$row['desk'].'</Data></Cell>
						    <Cell><Data ss:Type="String">'.$row['negeri'].'</Data></Cell>
						    <Cell><Data ss:Type="String">'.$row['swasta'].'</Data></Cell>
						   </Row>';

					$negeriTotal = $negeriTotal+$row['negeri'];
					$swastaTotal = $swastaTotal+$row['swasta'];
				}

				$str .= '<Row>
						    <Cell><Data ss:Type="String"></Data></Cell>
						    <Cell><Data ss:Type="String"></Data></Cell>
						    <Cell><Data ss:Type="String"></Data></Cell>
						    <Cell><Data ss:Type="String"></Data></Cell>
						   </Row>
						<Row>
						    <Cell><Data ss:Type="String"></Data></Cell>
						    <Cell><Data ss:Type="String">Total</Data></Cell>
						    <Cell><Data ss:Type="String">'.$negeriTotal.'</Data></Cell>
						    <Cell><Data ss:Type="String">'.$swastaTotal.'</Data></Cell>
						   </Row>';

				// return var_dump($data);die;
				
				$fileName = "Analisis Data Pokok Sekolah Berdasarkan Status - ".$today;
				$fileTemplate = "template-rekap-sekolah-status.php";
				break;

			case 'sekolahBentukPendidikan':
				$con = Chart::initdb();

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = 's.provinsi';
						$group_wilayah_1 = 's.kode_wilayah_provinsi';
						$params_wilayah ='';
						break;
					case 1:
						$col_wilayah = 's.kabupaten';
						$group_wilayah_1 = 's.kode_wilayah_kabupaten';
						$params_wilayah = ' and s.mst_kode_wilayah_kabupaten = '.$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = 's.kecamatan';
						$group_wilayah_1 = 's.kode_wilayah_kecamatan';
						$params_wilayah = ' and s.mst_kode_wilayah_kecamatan = '.$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} AS desk,
							sum(case when s.bentuk_pendidikan_id = 13 then 1 else 0 end) as sma,
							sum(case when s.bentuk_pendidikan_id = 14 then 1 else 0 end) as smlb,
							sum(case when s.bentuk_pendidikan_id = 15 then 1 else 0 end) as smk,
							sum(case when s.bentuk_pendidikan_id = 29 then 1 else 0 end) as slb
						FROM
							rekap_sekolah s
						where 
							s.semester_id = {$semester_id}
							{$params_bp}
							{$params_wilayah}
						GROUP BY
							{$group_wilayah_1},
							{$col_wilayah}";

				$stmt = sqlsrv_query( $con, $sql );

				while($d = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){
					$i++;
					$str .= '<Row>
							    <Cell ss:StyleID="s86"><Data ss:Type="Number">'.$i.'</Data></Cell>
							    <Cell ss:StyleID="s82"><Data ss:Type="String">'.$d['desk'].'</Data></Cell>
							    <Cell ss:StyleID="s81"><Data ss:Type="Number">'.$d['sd'].'</Data></Cell>
							    <Cell ss:StyleID="s81"><Data ss:Type="Number">'.$d['smp'].'</Data></Cell>
							    <Cell ss:StyleID="s81"><Data ss:Type="Number">'.$d['sma'].'</Data></Cell>
							    <Cell ss:StyleID="s81"><Data ss:Type="Number">'.$d['smk'].'</Data></Cell>
							    <Cell ss:StyleID="s81"><Data ss:Type="Number">'.$d['sdlb'].'</Data></Cell>
							    <Cell ss:StyleID="s81"><Data ss:Type="Number">'.$d['smplb'].'</Data></Cell>
							    <Cell ss:StyleID="s81"><Data ss:Type="Number">'.$d['smlb'].'</Data></Cell>
							    <Cell ss:StyleID="s81"><Data ss:Type="Number">'.$d['slb'].'</Data></Cell>
							   </Row>';

					$sdTotal = $sdTotal + $d['sd'];
					$smpTotal = $smpTotal + $d['smp'];
					$smaTotal = $smaTotal + $d['sma'];
					$smkTotal = $smkTotal + $d['smk'];
					$sdlbTotal = $sdlbTotal + $d['sdlb'];
					$smplbTotal = $smplbTotal + $d['smplb'];
					$smlbTotal = $smlbTotal + $d['smlb'];
					$slbTotal = $slbTotal + $d['slb'];
				}

				$str .= '<Row>
							    <Cell ss:StyleID="s86"><Data ss:Type="String">-</Data></Cell>
							    <Cell ss:StyleID="s82"><Data ss:Type="String">Total</Data></Cell>
							    <Cell ss:StyleID="s81"><Data ss:Type="Number">'.$sdTotal.'</Data></Cell>
							    <Cell ss:StyleID="s81"><Data ss:Type="Number">'.$smpTotal.'</Data></Cell>
							    <Cell ss:StyleID="s81"><Data ss:Type="Number">'.$smaTotal.'</Data></Cell>
							    <Cell ss:StyleID="s81"><Data ss:Type="Number">'.$smkTotal.'</Data></Cell>
							    <Cell ss:StyleID="s81"><Data ss:Type="Number">'.$sdlbTotal.'</Data></Cell>
							    <Cell ss:StyleID="s81"><Data ss:Type="Number">'.$smplbTotal.'</Data></Cell>
							    <Cell ss:StyleID="s81"><Data ss:Type="Number">'.$smlbTotal.'</Data></Cell>
							    <Cell ss:StyleID="s81"><Data ss:Type="Number">'.$slbTotal.'</Data></Cell>
							   </Row>';
				
				$fileName = "Analisis Data Pokok Sekolah Berdasarkan Bentuk Pendidikan - ".$today;
				$fileTemplate = "template-rekap-sekolah-bentuk.php";

				break;
			case 'ptkJenisPtk':
				// return $bentuk_pendidikan_id;die;

				$params_bp = System::getParamsBp($bentuk_pendidikan_id);

				switch ($session_id_level_wilayah) {
					case 0:
						$col_wilayah = "w3.nama";
						$params_wilayah = " and w3.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 1:
						$col_wilayah = "w2.nama";
						$params_wilayah = " and w2.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					case 2:
						$col_wilayah = "w1.nama";
						$params_wilayah = " and w1.mst_kode_wilayah = ".$session_kode_wilayah;
						break;
					default:
						# code...
						break;
				}

				$sql = "SELECT
							{$col_wilayah} AS desk,
							sum(CASE WHEN ptk.jenis_ptk_id = 3 THEN 1 ELSE 0 END) AS 'guru_kelas',
							sum(CASE WHEN ptk.jenis_ptk_id = 4 THEN 1 ELSE 0 END) AS 'guru_mata_pelajaran',
							sum(CASE WHEN ptk.jenis_ptk_id = 5 THEN 1 ELSE 0 END) AS 'guru_bk',
							sum(CASE WHEN ptk.jenis_ptk_id = 6 THEN 1 ELSE 0 END) AS 'guru_inklusi',
							sum(CASE WHEN ptk.jenis_ptk_id = 7 THEN 1 ELSE 0 END) AS 'pengawas_satuan_pendidikan',
							sum(CASE WHEN ptk.jenis_ptk_id = 8 THEN 1 ELSE 0 END) AS 'pengawas_plb',
							sum(CASE WHEN ptk.jenis_ptk_id = 9 THEN 1 ELSE 0 END) AS 'pengawas_mata_pelajaran',
							sum(CASE WHEN ptk.jenis_ptk_id = 10 THEN 1 ELSE 0 END) AS 'pengawas_bidang',
							sum(CASE WHEN ptk.jenis_ptk_id = 11 THEN 1 ELSE 0 END) AS 'tenaga_administrasi_sekolah',
							sum(CASE WHEN ptk.jenis_ptk_id = 12 THEN 1 ELSE 0 END) AS 'guru_pendamping',
							sum(CASE WHEN ptk.jenis_ptk_id = 13 THEN 1 ELSE 0 END) AS 'guru_magang',
							sum(CASE WHEN ptk.jenis_ptk_id = 14 THEN 1 ELSE 0 END) AS 'guru_tik',
							sum(CASE WHEN ptk.jenis_ptk_id = 30 THEN 1 ELSE 0 END) AS 'laboran',
							sum(CASE WHEN ptk.jenis_ptk_id = 40 THEN 1 ELSE 0 END) AS 'pustakawan',
							sum(CASE WHEN ptk.jenis_ptk_id = 99 THEN 1 ELSE 0 END) AS 'lainnya'
						FROM
							ptk ptk
						JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
						JOIN {$ref}tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
						JOIN sekolah s ON ptkd.sekolah_id = s.sekolah_id
						JOIN {$ref}mst_wilayah w1 ON w1.kode_wilayah = s.kode_wilayah
						JOIN {$ref}mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
						JOIN {$ref}mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
						WHERE
							ptk.soft_delete = 0
						AND ptkd.soft_delete = 0
						AND ptkd.jenis_keluar_id IS NULL
						AND (
							(
								ptkd.tgl_ptk_keluar >= ta.tanggal_mulai
								AND ptkd.tgl_ptk_keluar < ta.tanggal_selesai
							)
							OR ptkd.tgl_ptk_keluar IS NULL
						)
						{$params_bp}
						{$params_wilayah}
						AND ptkd.tahun_ajaran_id = {$tahun_ajaran_id}
						GROUP BY
							{$col_wilayah}
						order by {$col_wilayah} asc";

				// return $sql;die;

				$data = getDataBySql($sql);
				$i = 0;

				foreach ($data as $d) {
					$i++;
					$str .= '<Row>
						    <Cell ss:StyleID="s69"><Data ss:Type="Number">'.$i.'</Data></Cell>
						    <Cell ss:StyleID="s69"><Data ss:Type="String">'.$d['desk'].'</Data></Cell>
						    <Cell ss:StyleID="s82"><Data ss:Type="Number">'.$d['guru_kelas'].'</Data></Cell>
						    <Cell ss:StyleID="s82"><Data ss:Type="Number">'.$d['guru_mata_pelajaran'].'</Data></Cell>
						    <Cell ss:StyleID="s82"><Data ss:Type="Number">'.$d['guru_bk'].'</Data></Cell>
						    <Cell ss:StyleID="s82"><Data ss:Type="Number">'.$d['guru_inklusi'].'</Data></Cell>
						    <Cell ss:StyleID="s82"><Data ss:Type="Number">'.$d['tenaga_administrasi_sekolah'].'</Data></Cell>
						    <Cell ss:StyleID="s82"><Data ss:Type="Number">'.$d['guru_pendamping'].'</Data></Cell>
						    <Cell ss:StyleID="s82"><Data ss:Type="Number">'.$d['guru_magang'].'</Data></Cell>
						    <Cell ss:StyleID="s82"><Data ss:Type="Number">'.$d['guru_tik'].'</Data></Cell>
						    <Cell ss:StyleID="s82"><Data ss:Type="Number">'.$d['laboran'].'</Data></Cell>
						    <Cell ss:StyleID="s82"><Data ss:Type="Number">'.$d['pustakawan'].'</Data></Cell>
						    <Cell ss:StyleID="s82"><Data ss:Type="Number">'.$d['lainnya'].'</Data></Cell>
						   </Row>';
				}

				$fileName = "Analisis Data Pokok PTK Berdasarkan Jenis - ".$today;
				$fileTemplate = "template-rekap-ptk-jenis-ptk.php";

				break;
			
			default:
				# code...
				break;
		}

		ob_start();
			
		header('Content-type: application/vnd.ms-excel');			
		header('Content-Disposition: attachment; filename="'.$fileName.' - '.$today.'.xls"');
		include dirname(__FILE__).D.'Templates'.D.$fileTemplate;
		
		ob_end_flush();
		
		return false;

		// return $keyword.' '.$kec.' '.$bp_id.' '.$ss_id;
	}

}