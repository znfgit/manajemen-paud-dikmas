Ext.define('simdik_batam.model.SekolahRangkuman', {
    extend: 'Ext.data.Model',
    idProperty: 'desk',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'desk', type: 'string'  }, 
        { name: 'pd_kelas_10', type: 'float'  },
        { name: 'pd_kelas_11', type: 'float'  },
        { name: 'pd_kelas_12', type: 'float'  },
        { name: 'pd_kelas_13', type: 'float'  },
        { name: 'ptk_total', type: 'float'  },
        { name: 'pd', type: 'float'  },
        { name: 'ptk', type: 'float'  },
        { name: 'pegawai', type: 'float'  },
        { name: 'rombel', type: 'float'  },
        { name: 'kode_wilayah', type: 'string'  },
        { name: 'id_level_wilayah', type: 'string'  },
        
    ],
    proxy: {
        type: 'rest',
        url : 'chart/column/SekolahRangkuman/cache',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});