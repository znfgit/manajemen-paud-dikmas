Ext.define('simdik_batam.model.AnalisisKebutuhanPrasaranaSMA', {
    extend: 'Ext.data.Model',
    idProperty: 'anggota_rombel_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'kode_wilayah', type: 'string'  },
        { name: 'nama', type: 'string'  },
        { name: 'negeri', type: 'float'  },
        { name: 'swasta', type: 'float'  },
        { name: 'kelas_negeri', type: 'float'  },
        { name: 'kelas_swasta', type: 'float'  },
        { name: 'lab_ipa_negeri', type: 'float'  },
        { name: 'lab_ipa_swasta', type: 'float'  },
        { name: 'lab_komputer_negeri', type: 'float'  },
        { name: 'lab_komputer_swasta', type: 'float'  },
        { name: 'perpustakaan_negeri', type: 'float'  },
        { name: 'perpustakaan_swasta', type: 'float'  },
        { name: 'rdk', type: 'float'  },
        { name: 'rdg', type: 'float'  },
        { name: 'rdj', type: 'float'  },
        { name: 'wc', type: 'float'  },
        { name: 'pr_p', type: 'float'  },
        { name: 'pr_a', type: 'float'  },
        { name: 'pr_k', type: 'float'  },
        { name: 'pr_l', type: 'float'  }
    ],
    proxy: {
        type: 'rest',
        url : 'get/AnalisisKebutuhanPrasarana?bentuk_pendidikan_id=13',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});