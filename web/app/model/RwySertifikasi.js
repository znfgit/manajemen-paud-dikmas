Ext.define('simdik_batam.model.RwySertifikasi', {
    extend: 'Ext.data.Model',
    idProperty: 'riwayat_sertifikasi_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'riwayat_sertifikasi_id', type: 'string'  }, 
        { name: 'ptk_id', type: 'string', useNull: true },
        { name: 'ptk_id_str', type: 'string'  }, 
        { name: 'id_jenis_sertifikasi', type: 'float', useNull: true },
        { name: 'id_jenis_sertifikasi_str', type: 'string'  }, 
        { name: 'nomor_sertifikat', type: 'string'  }, 
        { name: 'tahun_sertifikasi', type: 'float'  }, 
        { name: 'bidang_studi_id', type: 'int', useNull: true },
        { name: 'bidang_studi_id_str', type: 'string'  }, 
        { name: 'nrg', type: 'string'  }, 
        { name: 'nomor_peserta', type: 'string'  }
        
    ],
    proxy: {
        type: 'rest',
        url : 'get/RwySertifikasi',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);                    
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data RwySertifikasi ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});