Ext.define('simdik_batam.model.ProgresData', {
    extend: 'Ext.data.Model',
    idProperty: 'kode_wilayah',
    fields: [
        { name: 'nama', type: 'string'  }, 
        { name: 'kode_wilayah', type: 'string'  }, 
        { name: 'id_level_wilayah', type: 'string'  }, 
        { name: 'mst_kode_wilayah', type: 'string'  }, 
        { name: 'sma', type: 'float'  }, 
        { name: 'smk', type: 'float'  }, 
        { name: 'smlb', type: 'float'  }, 
        { name: 'kirim_sma', type: 'float'  }, 
        { name: 'kirim_smk', type: 'float'  }, 
        { name: 'kirim_smlb', type: 'float'  }, 
        { name: 'persen', type: 'float'  }
        
    ],
    proxy: {
        type: 'rest',
        url : 'chart/column/ProgresData/cache',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);                    
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});