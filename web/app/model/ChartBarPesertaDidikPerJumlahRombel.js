Ext.define('simdik_batam.model.ChartBarPesertaDidikPerJumlahRombel', {
    extend: 'Ext.data.Model',
    idProperty: 'desk',
    clientIdProperty: 'ext_client_id',
    proxy: {
        type: 'rest',
        url : 'chart/column/SebaranPdPerJumlahRombel',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});