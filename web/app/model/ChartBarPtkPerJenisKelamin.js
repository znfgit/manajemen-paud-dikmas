Ext.define('simdik_batam.model.ChartBarPtkPerJenisKelamin', {
    extend: 'Ext.data.Model',
    idProperty: 'desk',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'desk', type: 'string'  }, 
        { name: 'laki-laki', type: 'float'  },
        { name: 'perempuan', type: 'float'  }
        
    ],
    proxy: {
        type: 'rest',
        url : 'chart/column/SebaranPtkJenisKelamin',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});