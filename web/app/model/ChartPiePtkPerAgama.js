Ext.define('simdik_batam.model.ChartPiePtkPerAgama', {
    extend: 'Ext.data.Model',
    idProperty: 'desk',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'desk', type: 'string'  }, 
        { name: 'jumlah', type: 'float'  }
        
    ],
    proxy: {
        type: 'rest',
        url : 'chart/pie/PtkPerAgama',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});