Ext.define('simdik_batam.model.PdTingkatKelasJenisKelaminSp', {
    extend: 'Ext.data.Model',
    idProperty: 'desk',
    // groupField: 'kabupaten',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'sekolah_id', type: 'string'  }, 
        { name: 'nama_sekolah', type: 'string'  }, 
        { name: 'npsn', type: 'string'  }, 
        { name: 'kecamatan', type: 'string'  }, 
        { name: 'kabupaten', type: 'string'  }, 
        { name: 'pd_kelas_10', type: 'float'  },
        { name: 'pd_kelas_10_laki', type: 'float'  },
        { name: 'pd_kelas_10_perempuan', type: 'float'  },
        { name: 'pd_kelas_11', type: 'float'  },
        { name: 'pd_kelas_11_laki', type: 'float'  },
        { name: 'pd_kelas_11_perempuan', type: 'float'  },
        { name: 'pd_kelas_12', type: 'float'  },
        { name: 'pd_kelas_12_laki', type: 'float'  },
        { name: 'pd_kelas_12_perempuan', type: 'float'  },
        { name: 'pd_kelas_13', type: 'float'  },
        { name: 'pd_kelas_13_laki', type: 'float'  },
        { name: 'pd_kelas_13_perempuan', type: 'float'  }

    ],
    proxy: {
        type: 'rest',
        url : 'chart/column/PdTingkatKelasJenisKelaminSp/cache',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});