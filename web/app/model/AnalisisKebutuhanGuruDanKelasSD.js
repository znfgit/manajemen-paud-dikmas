Ext.define('simdik_batam.model.AnalisisKebutuhanGuruDanKelasSD', {
    extend: 'Ext.data.Model',
    idProperty: 'anggota_rombel_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'kode_wilayah', type: 'string'  },
        { name: 'nama', type: 'string'  },
        { name: 'rb_negeri', type: 'float'  },
        { name: 'rb_swasta', type: 'float'  },
        { name: 'pr_negeri', type: 'float'  },
        { name: 'pr_swasta', type: 'float'  },
        { name: 'pd_negeri', type: 'float'  },
        { name: 'pd_swasta', type: 'float'  },
        { name: 'negeri', type: 'float'  },
        { name: 'swasta', type: 'float'  },
        { name: 'guru_p', type: 'float'  },
        { name: 'guru_a', type: 'float'  },
        { name: 'guru_k', type: 'float'  },
        { name: 'guru_l', type: 'float'  },
        { name: 'kelas_p', type: 'float'  },
        { name: 'kelas_a', type: 'float'  },
        { name: 'kelas_k', type: 'float'  },
        { name: 'kelas_l', type: 'float'  }
    ],
    proxy: {
        type: 'rest',
        url : 'get/AnalisisKebutuhanGuruDanKelas?bentuk_pendidikan_id=5',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});