Ext.define('simdik_batam.model.ChartBarPrasaranaJenis', {
    extend: 'Ext.data.Model',
    idProperty: 'desk',
    clientIdProperty: 'ext_client_id',
    proxy: {
        type: 'rest',
        url : 'chart/column/SebaranPrasaranaPerJenis',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});