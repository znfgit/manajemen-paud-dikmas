Ext.define('simdik_batam.model.RekapTotal', {
    extend: 'Ext.data.Model',
    idProperty: 'desk',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'desk', type: 'string'  }, 
        { name: 'total', type: 'string' },
        { name: 'sd', type: 'string' }, 
        { name: 'smp', type: 'string' },
        { name: 'slb', type: 'string'  }
        
    ],
    proxy: {
        type: 'rest',
        url : 'get/RekapTotalCache',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);                    
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data AnggotaRombel ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});