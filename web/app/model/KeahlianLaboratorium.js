Ext.define('simdik_batam.model.KeahlianLaboratorium', {
    extend: 'Ext.data.Model',
    idProperty: 'keahlian_laboratorium_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'keahlian_laboratorium_id', type: 'int'  }, 
        { name: 'nama', type: 'string'  }, 
        { name: 'create_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'}, 
        { name: 'expired_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'}
        
    ],
    proxy: {
        type: 'rest',
        url : 'get/KeahlianLaboratorium',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);                    
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data KeahlianLaboratorium ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});