Ext.define('simdik_batam.model.ChartBarPesertaDidikPerUmur', {
    extend: 'Ext.data.Model',
    idProperty: 'desk',
    clientIdProperty: 'ext_client_id',
    proxy: {
        type: 'rest',
        url : 'chart/column/SebaranPdPerUmur',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});