Ext.define('simdik_batam.model.chartBarPtkMataPelajaran', {
    extend: 'Ext.data.Model',
    idProperty: 'jumlah',
    fields: [
        { name: 'nama', type: 'string'  }, 
        { name: 'guru_matematika', type: 'float', useNull: true },
        { name: 'guru_bahasa_indonesia', type: 'float', useNull: true },
        { name: 'guru_bahasa_inggris', type: 'float', useNull: true },
        { name: 'guru_pkn', type: 'float', useNull: true },
        { name: 'guru_agama_budi_pekerti', type: 'float', useNull: true },
        { name: 'guru_penjaskes', type: 'float', useNull: true },
        { name: 'guru_seni_budaya', type: 'float', useNull: true },
        { name: 'guru_sejarah_indonesia', type: 'float', useNull: true }
    ],
    proxy: {
        type: 'rest',
        url : 'chart/column/SebaranPtkPerMataPelajaran',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);                    
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});