Ext.define('simdik_batam.model.ChartBarPtkPerJenisPtk', {
    extend: 'Ext.data.Model',
    idProperty: 'desk',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'desk', type: 'string'  }, 
        { name: 'guru_mata_pelajaran', type: 'float'  },
        { name: 'guru_kelas', type: 'float'  }
        
    ],
    proxy: {
        type: 'rest',
        url : 'chart/column/SebaranPtkJenisPtk',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});