Ext.define('simdik_batam.model.Prasarana', {
    extend: 'Ext.data.Model',
    idProperty: 'prasarana_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'prasarana_id', type: 'string'  }, 
        { name: 'sekolah_id', type: 'string', useNull: true },
        { name: 'sekolah_id_str', type: 'string'  }, 
        { name: 'jenis_prasarana_id', type: 'int', useNull: true },
        { name: 'jenis_prasarana_id_str', type: 'string'  }, 
        { name: 'kepemilikan_sarpras_id', type: 'float', useNull: true },
        { name: 'kepemilikan_sarpras_id_str', type: 'string'  }, 
        { name: 'nama', type: 'string'  }, 
        { name: 'panjang', type: 'float'  }, 
        { name: 'lebar', type: 'float'  }
        
    ],
    proxy: {
        type: 'rest',
        url : 'get/Prasarana',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);                    
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data Prasarana ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});