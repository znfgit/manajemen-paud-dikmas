Ext.define('simdik_batam.model.Pembelajaran', {
    extend: 'Ext.data.Model',
    idProperty: 'pembelajaran_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'pembelajaran_id', type: 'string'  }, 
        { name: 'rombongan_belajar_id', type: 'string', useNull: true },
        { name: 'rombongan_belajar_id_str', type: 'string'  }, 
        { name: 'semester_id', type: 'string', useNull: true },
        { name: 'semester_id_str', type: 'string'  }, 
        { name: 'mata_pelajaran_id', type: 'int', useNull: true },
        { name: 'mata_pelajaran_id_str', type: 'string'  }, 
        { name: 'nama_mata_pelajaran', type: 'string'  }, 
        { name: 'ptk_terdaftar_id', type: 'string', useNull: true },
        { name: 'ptk_terdaftar_id_str', type: 'string'  }, 
        { name: 'sk_mengajar', type: 'string'  }, 
        { name: 'tanggal_sk_mengajar', type: 'date',  dateFormat: 'Y-m-d'}, 
        { name: 'jam_mengajar_per_minggu', type: 'float'  }, 
        { name: 'status_di_kurikulum', type: 'float'  }
        
    ],
    proxy: {
        type: 'rest',
        url : 'get/Pembelajaran',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);                    
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data Pembelajaran ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});