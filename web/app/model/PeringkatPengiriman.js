Ext.define('simdik_batam.model.PeringkatPengiriman', {
    extend: 'Ext.data.Model',
    idProperty: 'kode_wilayah',
    fields: [
        { name: 'nama', type: 'string'  }, 
        { name: 'kode_wilayah', type: 'string'  }, 
        { name: 'id_level_wilayah', type: 'string'  }, 
        { name: 'mst_kode_wilayah', type: 'string'  }, 
        { name: 'sd', type: 'float'  }, 
        { name: 'smp', type: 'float'  }, 
        { name: 'sdlb', type: 'float'  }, 
        { name: 'smplb', type: 'float'  }, 
        { name: 'slb', type: 'float'  }, 
        { name: 'sma', type: 'float'  }, 
        { name: 'smk', type: 'float'  }, 
        { name: 'smlb', type: 'float'  }, 
        { name: 'kirim_sd', type: 'float'  }, 
        { name: 'kirim_smp', type: 'float'  }, 
        { name: 'kirim_sdlb', type: 'float'  }, 
        { name: 'kirim_smplb', type: 'float'  }, 
        { name: 'kirim_slb', type: 'float'  }, 
        { name: 'kirim_sma', type: 'float'  }, 
        { name: 'kirim_smk', type: 'float'  }, 
        { name: 'kirim_smlb', type: 'float'  }, 
        { name: 'persen', type: 'float'  }
        
    ],
    proxy: {
        type: 'rest',
        url : 'get/PeringkatPengiriman',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);                    
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});