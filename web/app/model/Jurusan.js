Ext.define('simdik_batam.model.Jurusan', {
    extend: 'Ext.data.Model',
    idProperty: 'jurusan_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'jurusan_id', type: 'string'  }, 
        { name: 'nama_jurusan', type: 'string'  }, 
        { name: 'untuk_sma', type: 'float'  }, 
        { name: 'untuk_smk', type: 'float'  }, 
        { name: 'untuk_pt', type: 'float'  }, 
        { name: 'untuk_slb', type: 'float'  }, 
        { name: 'untuk_smklb', type: 'float'  }, 
        { name: 'jurusan_induk', type: 'string', useNull: true },
        { name: 'jurusan_induk_str', type: 'string'  }, 
        { name: 'level_bidang_id', type: 'string', useNull: true },
        { name: 'level_bidang_id_str', type: 'string'  }, 
        { name: 'create_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'}, 
        { name: 'expired_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'}, 
        { name: 'jenjang_pendidikan_id', type: 'float'  }        
    ],
    proxy: {
        type: 'rest',
        url : 'get/Jurusan',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);                    
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data AnggotaRombel ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});