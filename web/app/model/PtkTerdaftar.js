Ext.define('simdik_batam.model.PtkTerdaftar', {
    extend: 'Ext.data.Model',
    idProperty: 'ptk_terdaftar_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'ptk_terdaftar_id', type: 'string'  }, 
        { name: 'ptk_id', type: 'string', useNull: true },
        { name: 'ptk_id_str', type: 'string'  }, 
        { name: 'sekolah_id', type: 'string', useNull: true },
        { name: 'sekolah_id_str', type: 'string'  }, 
        { name: 'tahun_ajaran_id', type: 'float', useNull: true },
        { name: 'tahun_ajaran_id_str', type: 'string'  }, 
        { name: 'nomor_surat_tugas', type: 'string'  }, 
        { name: 'tanggal_surat_tugas', type: 'date',  dateFormat: 'Y-m-d'}, 
        { name: 'tmt_tugas', type: 'date',  dateFormat: 'Y-m-d'}, 
        { name: 'ptk_induk', type: 'float'  }, 
        { name: 'aktif_bulan_07', type: 'float'  }, 
        { name: 'aktif_bulan_08', type: 'float'  }, 
        { name: 'aktif_bulan_09', type: 'float'  }, 
        { name: 'aktif_bulan_10', type: 'float'  }, 
        { name: 'aktif_bulan_11', type: 'float'  }, 
        { name: 'aktif_bulan_12', type: 'float'  }, 
        { name: 'aktif_bulan_01', type: 'float'  }, 
        { name: 'aktif_bulan_02', type: 'float'  }, 
        { name: 'aktif_bulan_03', type: 'float'  }, 
        { name: 'aktif_bulan_04', type: 'float'  }, 
        { name: 'aktif_bulan_05', type: 'float'  }, 
        { name: 'aktif_bulan_06', type: 'float'  }, 
        { name: 'jenis_keluar_id', type: 'string', useNull: true },
        { name: 'jenis_keluar_id_str', type: 'string'  }, 
        { name: 'tgl_ptk_keluar', type: 'date',  dateFormat: 'Y-m-d'}
        
    ],
    proxy: {
        type: 'rest',
        url : 'get/PtkTerdaftar',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);                    
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data PtkTerdaftar ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});