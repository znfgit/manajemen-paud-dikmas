Ext.define('simdik_batam.model.ChartBarSekolahPerWaktuPenyelenggaraan', {
    extend: 'Ext.data.Model',
    idProperty: 'desk',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'desk', type: 'string'  }, 
        { name: 'pagi', type: 'float'  },
        { name: 'siang', type: 'float'  },
        { name: 'kombinasi', type: 'float'  },
        { name: 'sore', type: 'float'  },
        { name: 'malam', type: 'float'  },
        { name: 'sehari_penuh_5', type: 'float' },
        { name: 'sehari_penuh_6', type: 'float' },
        { name: 'lainnya', type: 'float' }
    ],
    proxy: {
        type: 'rest',
        url : 'chart/column/SebaranSekolahPerWaktuPenyelenggaraan',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});