Ext.define('simdik_batam.model.ChartPieSekolahPerStatus', {
    extend: 'Ext.data.Model',
    idProperty: 'desk',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'desk', type: 'string'  }, 
        { name: 'jumlah', type: 'float'  }
        
    ],
    proxy: {
        type: 'rest',
        url : 'chart/pie/SekolahPerStatus/cache',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});