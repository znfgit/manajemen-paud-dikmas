Ext.define('simdik_batam.model.Semester', {
    extend: 'Ext.data.Model',
    idProperty: 'semester_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'semester_id', type: 'string'  }, 
        { name: 'tahun_ajaran_id', type: 'float', useNull: true },
        { name: 'tahun_ajaran_id_str', type: 'string'  }, 
        { name: 'nama', type: 'string'  }, 
        { name: 'semester', type: 'float'  }, 
        { name: 'periode_aktif', type: 'float'  }, 
        { name: 'tanggal_mulai', type: 'date',  dateFormat: 'Y-m-d'}, 
        { name: 'tanggal_selesai', type: 'date',  dateFormat: 'Y-m-d'}, 
        { name: 'create_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'}, 
        { name: 'expired_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'}
        
    ],
    proxy: {
        type: 'rest',
        url : 'get/Semester',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);                    
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data Semester ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});