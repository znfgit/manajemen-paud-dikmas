Ext.define('simdik_batam.model.Sekolah', {
    extend: 'Ext.data.Model',
    idProperty: 'sekolah_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'sekolah_id', type: 'string'  }, 
        { name: 'nama_sekolah', type: 'string'  }, 
        { name: 'nss', type: 'string'  }, 
        { name: 'npsn', type: 'string'  }, 
        { name: 'bentuk_pendidikan_id', type: 'int', useNull: true },
        { name: 'bentuk_pendidikan_id_str', type: 'string'  }, 
        { name: 'status_sekolah', type: 'float'  }, 
        { name: 'alamat_jalan', type: 'string'  }, 
        { name: 'rt', type: 'float'  }, 
        { name: 'rw', type: 'float'  }, 
        { name: 'nama_dusun', type: 'string'  }, 
        { name: 'desa_kelurahan', type: 'string'  }, 
        { name: 'kode_wilayah', type: 'string', useNull: true },
        { name: 'kecamatan', type: 'string'  }, 
        { name: 'kabupaten', type: 'string'  }, 
        { name: 'propinsi', type: 'string'  }, 
        { name: 'kode_pos', type: 'string'  }, 
        { name: 'lintang', type: 'float'  }, 
        { name: 'bujur', type: 'float'  }, 
        { name: 'kebutuhan_khusus_id', type: 'int', useNull: true },
        { name: 'kebutuhan_khusus_id_str', type: 'string'  }, 
        { name: 'sk_pendirian_sekolah', type: 'string'  }, 
        { name: 'tanggal_sk_pendirian', type: 'date',  dateFormat: 'Y-m-d'}, 
        { name: 'status_kepemilikan_id', type: 'float', useNull: true },
        { name: 'status_kepemilikan_id_str', type: 'string'  }, 
        { name: 'yayasan_id', type: 'string', useNull: true },
        { name: 'yayasan_id_str', type: 'string'  }, 
        { name: 'pengguna_id', type: 'string'  }, 
        { name: 'sk_izin_operasional', type: 'string'  }, 
        { name: 'tanggal_sk_izin_operasional', type: 'date',  dateFormat: 'Y-m-d'}, 
        { name: 'sk_akreditasi', type: 'string'  }, 
        { name: 'tanggal_sk_akreditasi', type: 'date',  dateFormat: 'Y-m-d'}, 
        { name: 'no_rekening', type: 'string'  }, 
        { name: 'nama_bank', type: 'string'  }, 
        { name: 'cabang_kcp_unit', type: 'string'  }, 
        { name: 'rekening_atas_nama', type: 'string'  }, 
        { name: 'mbs', type: 'float'  }, 
        { name: 'luas_tanah_milik', type: 'float'  }, 
        { name: 'luas_tanah_bukan_milik', type: 'float'  }, 
        { name: 'nomor_telepon', type: 'string'  }, 
        { name: 'nomor_fax', type: 'string'  }, 
        { name: 'email', type: 'string'  }, 
        { name: 'website', type: 'string'  }, 
        { name: 'kode_registrasi', type: 'string'  }, 
        { name: 'flag', type: 'string'  }
        
    ],
    proxy: {
        type: 'rest',
        url : 'get/Sekolah',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);                    
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data Sekolah ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});