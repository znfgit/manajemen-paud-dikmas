Ext.define('simdik_batam.model.ChartBarSekolahPerBentukPendidikan', {
    extend: 'Ext.data.Model',
    idProperty: 'desk',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'desk', type: 'string'  }, 
        { name: 'sd', type: 'float'  },
        { name: 'smp', type: 'float'  },
        { name: 'slb', type: 'float'  },
        { name: 'sma', type: 'float'  },
        { name: 'smk', type: 'float'  },
        { name: 'smlb', type: 'float' }
    ],
    proxy: {
        type: 'rest',
        url : 'chart/column/SebaranSekolahPerBentukPendidikan/cache',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});