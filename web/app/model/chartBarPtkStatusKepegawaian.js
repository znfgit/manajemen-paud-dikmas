Ext.define('simdik_batam.model.chartBarPtkStatusKepegawaian', {
    extend: 'Ext.data.Model',
    idProperty: 'jumlah',
    fields: [
        { name: 'nama', type: 'string'  }, 
        { name: 'pns', type: 'float', useNull: true },
        { name: 'pns_diperbantukan', type: 'float', useNull: true },
        { name: 'pns_depag', type: 'float', useNull: true },
        { name: 'cpns', type: 'float', useNull: true },
        { name: 'gty_pty', type: 'float', useNull: true },
        { name: 'gtt_ptt_provinsi', type: 'float', useNull: true },
        { name: 'gtt_ptt_kabkota', type: 'float', useNull: true },
        { name: 'guru_bantu_pusat', type: 'float', useNull: true },
        { name: 'guru_honor_sekolah', type: 'float', useNull: true },
        { name: 'tenaga_honor_sekolah', type: 'float', useNull: true },
        { name: 'lainnya', type: 'float', useNull: true }
    ],
    proxy: {
        type: 'rest',
        url : 'chart/column/SebaranPtkStatusKepegawaian',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);                    
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});