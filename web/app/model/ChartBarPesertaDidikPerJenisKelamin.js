Ext.define('simdik_batam.model.ChartBarPesertaDidikPerJenisKelamin', {
    extend: 'Ext.data.Model',
    idProperty: 'desk',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'desk', type: 'string'  }, 
        { name: 'laki-laki', type: 'float'  },
        { name: 'perempuan', type: 'float'  }
        
    ],
    proxy: {
        type: 'rest',
        url : 'chart/column/SebaranPdJenisKelamin/cache',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});