Ext.define('simdik_batam.model.JenisSurat', {
    extend: 'Ext.data.Model',
    idProperty: 'jenis_surat_id',
    clientIdProperty: 'ext_client_id',
    proxy: {
        type: 'rest',
        url : 'get/JenisSurat',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});