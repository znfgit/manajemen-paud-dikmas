Ext.define('simdik_batam.model.JenisPrasarana', {
    extend: 'Ext.data.Model',
    idProperty: 'jenis_prasarana_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'jenis_prasarana_id', type: 'int'  }, 
        { name: 'nama', type: 'string'  }, 
        { name: 'create_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'}, 
        { name: 'expired_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'}
        
    ],
    proxy: {
        type: 'rest',
        url : 'get/JenisPrasarana',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);                    
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data JenisPrasarana ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});