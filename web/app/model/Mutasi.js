Ext.define('simdik_batam.model.Mutasi', {
    extend: 'Ext.data.Model',
    idProperty: 'anggota_rombel_id',
    clientIdProperty: 'ext_client_id',
    proxy: {
        type: 'rest',
        url : 'get/Mutasi',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});