Ext.define('simdik_batam.model.BentukPendidikan', {
    extend: 'Ext.data.Model',
    idProperty: 'bentuk_pendidikan_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'bentuk_pendidikan_id', type: 'string'  }, 
        { name: 'nama', type: 'string'  }
        
    ],
    proxy: {
        type: 'rest',
        url : 'get/BentukPendidikan',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);                    
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data MstWilayah ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});