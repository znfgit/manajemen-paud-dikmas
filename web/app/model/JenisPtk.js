Ext.define('simdik_batam.model.JenisPtk', {
    extend: 'Ext.data.Model',
    idProperty: 'jenis_ptk_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        
    ],
    proxy: {
        type: 'rest',
        url : 'get/JenisPtk',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);                    
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data AnggotaRombel ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});