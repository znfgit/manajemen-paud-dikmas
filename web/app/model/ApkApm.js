Ext.define('simdik_batam.model.ApkApm', {
    extend: 'Ext.data.Model',
    idProperty: 'anggota_rombel_id',
    clientIdProperty: 'ext_client_id',
    proxy: {
        type: 'rest',
        url : 'get/ApkApm',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});