Ext.define('simdik_batam.model.SekolahRangkumanSp', {
    extend: 'Ext.data.Model',
    idProperty: 'desk',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'nama_sekolah', type: 'string'  }, 
        { name: 'npsn', type: 'string'  }, 
        { name: 'kecamatan', type: 'string'  }, 
        { name: 'kabupaten', type: 'string'  }, 
        { name: 'pd_kelas_10', type: 'float'  },
        { name: 'pd_kelas_11', type: 'float'  },
        { name: 'pd_kelas_12', type: 'float'  },
        { name: 'pd_kelas_13', type: 'float'  },
        { name: 'ptk_total', type: 'float'  },
        { name: 'pd', type: 'float'  },
        { name: 'ptk', type: 'float'  },
        { name: 'pegawai', type: 'float'  },
        { name: 'rombel', type: 'float'  },
        { name: 'kode_wilayah', type: 'string'  },
        { name: 'id_level_wilayah', type: 'string'  },
        
    ],
    proxy: {
        type: 'rest',
        url : 'chart/column/SekolahRangkumanSp/cache',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});