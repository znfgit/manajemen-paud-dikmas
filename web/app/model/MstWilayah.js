Ext.define('simdik_batam.model.MstWilayah', {
    extend: 'Ext.data.Model',
    idProperty: 'kode_wilayah',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'wilayah_id', type: 'string'  }, 
        { name: 'kode_wilayah', type: 'string'  }, 
        { name: 'nama', type: 'string'  }, 
        { name: 'id_level_wilayah', type: 'int', useNull: true },
        { name: 'id_level_wilayah_str', type: 'string'  }, 
        { name: 'mst_kode_wilayah', type: 'string', useNull: true },
        { name: 'mst_kode_wilayah_str', type: 'string'  }, 
        { name: 'negara_id', type: 'string', useNull: true },
        { name: 'negara_id_str', type: 'string'  }, 
        { name: 'asal_wilayah', type: 'string'  }, 
        { name: 'kode_bps', type: 'string'  }, 
        { name: 'kode_dagri', type: 'string'  }, 
        { name: 'kode_keu', type: 'string'  }, 
        { name: 'create_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'}, 
        { name: 'expired_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'}
        
    ],
    proxy: {
        type: 'rest',
        url : 'get/MstWilayah',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);                    
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data MstWilayah ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});