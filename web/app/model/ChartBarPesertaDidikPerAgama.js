Ext.define('simdik_batam.model.ChartBarPesertaDidikPerAgama', {
    extend: 'Ext.data.Model',
    idProperty: 'desk',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'desk', type: 'string'  }, 
        { name: 'islam', type: 'float'  },
        { name: 'kristen', type: 'float'  },
        { name: 'katholik', type: 'float'  },
        { name: 'hindu', type: 'float'  },
        { name: 'budha', type: 'float'  },
        { name: 'konghucu', type: 'float'  }
    ],
    proxy: {
        type: 'rest',
        url : 'chart/column/SebaranPdPerAgama',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});