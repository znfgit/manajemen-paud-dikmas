Ext.define('simdik_batam.model.ChartBarSekolahPerStatus', {
    extend: 'Ext.data.Model',
    idProperty: 'desk',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'desk', type: 'string'  }, 
        { name: 'negeri', type: 'float'  },
        { name: 'swasta', type: 'float'  }
    ],
    proxy: {
        type: 'rest',
        url : 'chart/column/SebaranSekolahPerStatus/cache',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        }
    }
});