Ext.define('simdik_batam.model.AnggotaRombel', {
    extend: 'Ext.data.Model',
    idProperty: 'anggota_rombel_id',
    clientIdProperty: 'ext_client_id',
    fields: [
        { name: 'anggota_rombel_id', type: 'string'  }, 
        { name: 'rombongan_belajar_id', type: 'string', useNull: true },
        { name: 'rombongan_belajar_id_str', type: 'string'  }, 
        { name: 'peserta_didik_id', type: 'string', useNull: true },
        { name: 'peserta_didik_id_str', type: 'string'  }, 
        { name: 'jenis_pendaftaran_id', type: 'float', useNull: true },
        { name: 'jenis_pendaftaran_id_str', type: 'string'  },
        { name: 'semester_id', type: 'string'  },
        { name: 'semester_id_str', type: 'string'  },
        { name: 'tahun_ajaran_id', type: 'string'  },
        { name: 'tahun_ajaran_id_str', type: 'string'  },
        { name: 'sekolah_id', type: 'string'  },
        { name: 'sekolah_id_str', type: 'string'  }
        
    ],
    proxy: {
        type: 'rest',
        url : 'get/AnggotaRombel',
        reader: {
            type: 'json',
            rootProperty: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);                    
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data AnggotaRombel ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});