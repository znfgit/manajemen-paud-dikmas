/**
 * The main application class. An instance of this class is created by app.js when it calls
 * Ext.application(). This is the ideal place to handle application launch and initialization
 * details.
 */
Ext.define('simdik_batam.Application', {
    extend: 'Ext.app.Application',

    requires:[
    	'simdik_batam.Viewport',
        'simdik_batam.Login',
    	'Ext.state.CookieProvider',
    	'Ext.tree.Panel',
    	'Ext.form.*',
    	'Ext.grid.*',
    	'Ext.data.TreeStore',
        'Ext.data.proxy.Rest',
        'Ext.ux.TabReorderer',
        'Ext.chart.interactions.ItemHighlight',
        'Ext.chart.series.Pie',
        'Ext.chart.interactions.Rotate',
        'Ext.chart.CartesianChart',
        'Ext.chart.axis.Numeric',
        'Ext.chart.axis.Category',
        'Ext.chart.series.Bar',
        'Ext.chart.PolarChart',
        'Ext.data.reader.Xml'
        // 'Ext.chart.Chart'
    ],
    
    name: 'simdik_batam',

    stores: [
        // TODO: add global / shared stores here
        'Sekolah',
        'MstWilayah',
        'Ptk',
        'PesertaDidik',
        'TahunAjaran',
        'Semester',
        'DataPokokPesertaDidik',
        'DataPokokPtk',
        'KeahlianLaboratorium',
        'PtkTerdaftar',
        'Pembelajaran',
        'AnggotaRombel',
        'Peran',
        'Pengguna',
        'BentukPendidikan',
        'ChartPiePesertaDidikPerJenisKelamin',
        'ChartPiePesertaDidikPerAgama',
        'ChartPiePtkPerAgama',
        'ChartBarPesertaDidikPerAgama',
        'ChartBarPtkPerAgama',
        'ChartBarPesertaDidikPerJenisKelamin',
        'ChartBarPtkPerJenisKelamin',
        'ChartPiePtkPerJenisKelamin',
        'ChartBarSekolahPerStatus',
        'ChartPieSekolahPerStatus',
        'ChartPieSekolahPerBentukPendidikan',
        'ChartBarSekolahPerBentukPendidikan',
        'RekapTotal',
        'ChartPieSekolahPerWaktuPenyelenggaraan',
        'ChartBarSekolahPerWaktuPenyelenggaraan',
        'ChartPiePtkPerJenisPtk',
        'ChartBarPtkPerJenisPtk',
        'chartBarPtkStatusKepegawaian',
        'ChartPiePtkPerStatusKepegawaian',
        'chartBarPtkMataPelajaran',
        'ChartPiePtkPerMataPelajaran',
        'ChartBarPesertaDidikPerUmur',
        'ChartBarPesertaDidikAngkaMengulang',
        'ChartBarPesertaDidikAngkaPutusSekolah',
        'RwySertifikasi',
        'Mutasi',
        'JenisSurat',
        'Prasarana',
        'JenisPrasarana',
        'ChartBarPesertaDidikPerJumlahRombel',
        'ChartBarPrasaranaJenis',
        'ApkApm',
        'AnalisisKebutuhanGuruDanKelasSD',
        'AnalisisKebutuhanGuruDanKelasSMP',
        'AnalisisKebutuhanGuruDanKelasSMA',
        'AnalisisKebutuhanPrasaranaSD',
        'AnalisisKebutuhanPrasaranaSMP',
        'AnalisisKebutuhanPrasaranaSMA',
        'PeringkatPengiriman',
        'ChartBarPesertaDidikAngkaSiswaMiskin',
        'PdTingkatKelasJenisKelamin',
        'SekolahRangkuman',
        'SekolahRangkumanSp',
        'ProgresData',
        'PdTingkatKelasJenisKelaminSp',
        'Jurusan',
        'JenisPtk'
    ],
    
    launch: function () {

        // TODO - Launch the application
        // Ext.setGlyphFontFamily('font-awesome');
        // Ext.tip.QuickTipManager.init();
        // Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider')); 

        // Ext.Ajax.request({
        //     url: 'session',
        //     success: function(response){
        //         var text = response.responseText;
                
        //         var res = Ext.JSON.decode(response.responseText);

        //         if(res.session == true){

        //             var hideMask = function () {
        //                 // Ext.get('loading').remove();
        //                 Ext.fly('loading-mask').animate({
        //                     opacity:0,
        //                     remove:true
        //                 });
        //             };

        //             Ext.defer(hideMask, 1000);
        //             // Ext.create('simdik_batam.Viewport',{
        //             //     record: '1'
        //             // });
                    
        //         }else{

        //             // Ext.getCmp('app-main').destroy();

        //             Ext.create('simdik_batam.Login',{
        //                 record: res.level,
        //                 kabkota: res.kabkota,
        //                 labels: res.labels
        //             });


        //         }
        //     }
        // });
    }
});
