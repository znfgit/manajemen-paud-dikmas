Ext.define('simdik_batam.store.Mutasi', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.Mutasi',
    model: 'simdik_batam.model.Mutasi',
    pageSize: 50,
    autoLoad: false
});