Ext.define('simdik_batam.store.AnalisisKebutuhanPrasaranaSD', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.AnalisisKebutuhanPrasaranaSD',
    model: 'simdik_batam.model.AnalisisKebutuhanPrasaranaSD',
    pageSize: 50,
    autoLoad: false
});