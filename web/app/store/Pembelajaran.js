Ext.define('simdik_batam.store.Pembelajaran', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.Pembelajaran',
    model: 'simdik_batam.model.Pembelajaran',
    pageSize: 50,
    autoLoad: false,
    groupField: 'semester_id_str'
});