Ext.define('simdik_batam.store.ChartPiePtkPerJenisKelamin', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ChartPiePtkPerJenisKelamin',
    model: 'simdik_batam.model.ChartPiePtkPerJenisKelamin',
    pageSize: 50,
    autoLoad: false
});