Ext.define('simdik_batam.store.BentukPendidikan', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.BentukPendidikan',
    model: 'simdik_batam.model.BentukPendidikan',
    pageSize: 50,
    autoLoad: false
});