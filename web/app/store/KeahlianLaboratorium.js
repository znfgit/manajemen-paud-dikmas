Ext.define('simdik_batam.store.KeahlianLaboratorium', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.KeahlianLaboratorium',
    model: 'simdik_batam.model.KeahlianLaboratorium',
    pageSize: 50,
    autoLoad: false    
});