Ext.define('simdik_batam.store.Sekolah', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.Sekolah',
    model: 'simdik_batam.model.Sekolah',
    pageSize: 50,
    autoLoad: false 
});