Ext.define('simdik_batam.store.ChartPiePesertaDidikPerAgama', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ChartPiePesertaDidikPerAgama',
    model: 'simdik_batam.model.ChartPiePesertaDidikPerAgama',
    pageSize: 50,
    autoLoad: false
});