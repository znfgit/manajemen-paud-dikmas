Ext.define('simdik_batam.store.ApkApm', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ApkApm',
    model: 'simdik_batam.model.ApkApm',
    pageSize: 50,
    autoLoad: false
});