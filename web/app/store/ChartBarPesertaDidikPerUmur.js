Ext.define('simdik_batam.store.ChartBarPesertaDidikPerUmur', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ChartBarPesertaDidikPerUmur',
    model: 'simdik_batam.model.ChartBarPesertaDidikPerUmur',
    pageSize: 50,
    autoLoad: false,
    groupField: 'kabupaten'
});