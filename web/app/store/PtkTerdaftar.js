Ext.define('simdik_batam.store.PtkTerdaftar', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.PtkTerdaftar',
    model: 'simdik_batam.model.PtkTerdaftar',
    pageSize: 50,
    autoLoad: false
});