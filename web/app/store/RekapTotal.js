Ext.define('simdik_batam.store.RekapTotal', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.RekapTotal',
    model: 'simdik_batam.model.RekapTotal',
    pageSize: 50,
    autoLoad: false
});