Ext.define('simdik_batam.store.Semester', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.Semester',
    model: 'simdik_batam.model.Semester',
    pageSize: 50,
    autoLoad: false    
});