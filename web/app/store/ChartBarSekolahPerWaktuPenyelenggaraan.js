Ext.define('simdik_batam.store.ChartBarSekolahPerWaktuPenyelenggaraan', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ChartBarSekolahPerWaktuPenyelenggaraan',
    model: 'simdik_batam.model.ChartBarSekolahPerWaktuPenyelenggaraan',
    pageSize: 50,
    autoLoad: false
});