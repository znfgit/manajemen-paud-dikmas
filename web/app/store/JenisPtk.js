Ext.define('simdik_batam.store.JenisPtk', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.JenisPtk',
    model: 'simdik_batam.model.JenisPtk',
    pageSize: 50,
    autoLoad: false
});