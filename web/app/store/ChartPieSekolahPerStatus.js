Ext.define('simdik_batam.store.ChartPieSekolahPerStatus', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ChartPieSekolahPerStatus',
    model: 'simdik_batam.model.ChartPieSekolahPerStatus',
    pageSize: 50,
    autoLoad: false
});