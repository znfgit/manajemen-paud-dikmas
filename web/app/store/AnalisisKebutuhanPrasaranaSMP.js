Ext.define('simdik_batam.store.AnalisisKebutuhanPrasaranaSMP', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.AnalisisKebutuhanPrasaranaSMP',
    model: 'simdik_batam.model.AnalisisKebutuhanPrasaranaSMP',
    pageSize: 50,
    autoLoad: false
});