Ext.define('simdik_batam.store.PdTingkatKelasJenisKelamin', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.PdTingkatKelasJenisKelamin',
    model: 'simdik_batam.model.PdTingkatKelasJenisKelamin',
    pageSize: 50,
    autoLoad: false
});