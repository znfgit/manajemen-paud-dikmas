Ext.define('simdik_batam.store.ChartPiePtkPerJenisPtk', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ChartPiePtkPerJenisPtk',
    model: 'simdik_batam.model.ChartPiePtkPerJenisPtk',
    pageSize: 50,
    autoLoad: false
});