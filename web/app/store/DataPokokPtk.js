Ext.define('simdik_batam.store.DataPokokPtk', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.DataPokokPtk',
    model: 'simdik_batam.model.DataPokokPtk',
    pageSize: 50,
    autoLoad: false
});