Ext.define('simdik_batam.store.ChartBarPesertaDidikPerJenisKelamin', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ChartBarPesertaDidikPerJenisKelamin',
    model: 'simdik_batam.model.ChartBarPesertaDidikPerJenisKelamin',
    pageSize: 50,
    autoLoad: false
});