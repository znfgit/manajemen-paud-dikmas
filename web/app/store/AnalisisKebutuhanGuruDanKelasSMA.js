Ext.define('simdik_batam.store.AnalisisKebutuhanGuruDanKelasSMA', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.AnalisisKebutuhanGuruDanKelasSMA',
    model: 'simdik_batam.model.AnalisisKebutuhanGuruDanKelasSMA',
    pageSize: 50,
    autoLoad: false
});