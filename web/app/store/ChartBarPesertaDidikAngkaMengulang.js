Ext.define('simdik_batam.store.ChartBarPesertaDidikAngkaMengulang', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ChartBarPesertaDidikAngkaMengulang',
    model: 'simdik_batam.model.ChartBarPesertaDidikAngkaMengulang',
    pageSize: 50,
    autoLoad: false
});