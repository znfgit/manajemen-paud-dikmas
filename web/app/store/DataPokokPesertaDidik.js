Ext.define('simdik_batam.store.DataPokokPesertaDidik', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.DataPokokPesertaDidik',
    model: 'simdik_batam.model.DataPokokPesertaDidik',
    pageSize: 50,
    autoLoad: false
});