Ext.define('simdik_batam.store.ChartPieSekolahPerBentukPendidikan', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ChartPieSekolahPerBentukPendidikan',
    model: 'simdik_batam.model.ChartPieSekolahPerBentukPendidikan',
    pageSize: 50,
    autoLoad: false
});