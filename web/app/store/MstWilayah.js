Ext.define('simdik_batam.store.MstWilayah', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.MstWilayah',
    model: 'simdik_batam.model.MstWilayah',
    pageSize: 50,
    autoLoad: false
});