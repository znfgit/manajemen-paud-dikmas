Ext.define('simdik_batam.store.chartBarPtkStatusKepegawaian', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.chartBarPtkStatusKepegawaian',
    model: 'simdik_batam.model.chartBarPtkStatusKepegawaian',
    pageSize: 50,
    autoLoad: false
});