Ext.define('simdik_batam.store.AnggotaRombel', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.AnggotaRombel',
    model: 'simdik_batam.model.AnggotaRombel',
    pageSize: 50,
    autoLoad: false
});