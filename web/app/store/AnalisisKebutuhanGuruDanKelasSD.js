Ext.define('simdik_batam.store.AnalisisKebutuhanGuruDanKelasSD', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.AnalisisKebutuhanGuruDanKelasSD',
    model: 'simdik_batam.model.AnalisisKebutuhanGuruDanKelasSD',
    pageSize: 50,
    autoLoad: false
});