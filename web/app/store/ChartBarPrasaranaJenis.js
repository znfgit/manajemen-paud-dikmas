Ext.define('simdik_batam.store.ChartBarPrasaranaJenis', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ChartBarPrasaranaJenis',
    model: 'simdik_batam.model.ChartBarPrasaranaJenis',
    pageSize: 50,
    autoLoad: false
});