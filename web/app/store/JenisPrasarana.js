Ext.define('simdik_batam.store.JenisPrasarana', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.JenisPrasarana',
    model: 'simdik_batam.model.JenisPrasarana',
    pageSize: 50,
    autoLoad: false});