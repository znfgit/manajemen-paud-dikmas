Ext.define('simdik_batam.store.Pengguna', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.Pengguna',
    model: 'simdik_batam.model.Pengguna',
    pageSize: 50,
    autoLoad: false
});