Ext.define('simdik_batam.store.AnalisisKebutuhanGuruDanKelasSMP', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.AnalisisKebutuhanGuruDanKelasSMP',
    model: 'simdik_batam.model.AnalisisKebutuhanGuruDanKelasSMP',
    pageSize: 50,
    autoLoad: false
});