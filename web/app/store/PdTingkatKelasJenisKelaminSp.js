Ext.define('simdik_batam.store.PdTingkatKelasJenisKelaminSp', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.PdTingkatKelasJenisKelaminSp',
    model: 'simdik_batam.model.PdTingkatKelasJenisKelaminSp',
    pageSize: 50,
    autoLoad: false
});