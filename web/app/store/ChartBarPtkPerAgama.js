Ext.define('simdik_batam.store.ChartBarPtkPerAgama', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ChartBarPtkPerAgama',
    model: 'simdik_batam.model.ChartBarPtkPerAgama',
    pageSize: 50,
    autoLoad: false
});