Ext.define('simdik_batam.store.ChartBarSekolahPerBentukPendidikan', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ChartBarSekolahPerBentukPendidikan',
    model: 'simdik_batam.model.ChartBarSekolahPerBentukPendidikan',
    pageSize: 50,
    autoLoad: false
});