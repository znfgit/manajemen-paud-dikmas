Ext.define('simdik_batam.store.chartBarPtkMataPelajaran', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.chartBarPtkMataPelajaran',
    model: 'simdik_batam.model.chartBarPtkMataPelajaran',
    pageSize: 50,
    autoLoad: false
});