Ext.define('simdik_batam.store.ChartBarPesertaDidikAngkaPutusSekolah', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ChartBarPesertaDidikAngkaPutusSekolah',
    model: 'simdik_batam.model.ChartBarPesertaDidikAngkaPutusSekolah',
    pageSize: 50,
    autoLoad: false
});