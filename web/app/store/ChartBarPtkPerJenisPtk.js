Ext.define('simdik_batam.store.ChartBarPtkPerJenisPtk', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ChartBarPtkPerJenisPtk',
    model: 'simdik_batam.model.ChartBarPtkPerJenisPtk',
    pageSize: 50,
    autoLoad: false
});