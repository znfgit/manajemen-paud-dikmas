Ext.define('simdik_batam.store.Ptk', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.Ptk',
    model: 'simdik_batam.model.Ptk',
    pageSize: 50,
    autoLoad: false
});