Ext.define('simdik_batam.store.ChartPiePesertaDidikPerJenisKelamin', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ChartPiePesertaDidikPerJenisKelamin',
    model: 'simdik_batam.model.ChartPiePesertaDidikPerJenisKelamin',
    pageSize: 50,
    autoLoad: false
});