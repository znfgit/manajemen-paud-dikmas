Ext.define('simdik_batam.store.ChartBarPesertaDidikAngkaSiswaMiskin', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ChartBarPesertaDidikAngkaSiswaMiskin',
    model: 'simdik_batam.model.ChartBarPesertaDidikAngkaSiswaMiskin',
    pageSize: 50,
    autoLoad: false
});