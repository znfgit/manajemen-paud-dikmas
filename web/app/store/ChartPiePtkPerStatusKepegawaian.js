Ext.define('simdik_batam.store.ChartPiePtkPerStatusKepegawaian', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ChartPiePtkPerStatusKepegawaian',
    model: 'simdik_batam.model.ChartPiePtkPerStatusKepegawaian',
    pageSize: 50,
    autoLoad: false
});