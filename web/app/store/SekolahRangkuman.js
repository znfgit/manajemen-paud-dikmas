Ext.define('simdik_batam.store.SekolahRangkuman', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.SekolahRangkuman',
    model: 'simdik_batam.model.SekolahRangkuman',
    pageSize: 50,
    autoLoad: false
});