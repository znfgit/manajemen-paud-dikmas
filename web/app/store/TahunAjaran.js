Ext.define('simdik_batam.store.TahunAjaran', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.TahunAjaran',
    model: 'simdik_batam.model.TahunAjaran',
    pageSize: 50,
    autoLoad: false    
});