Ext.define('simdik_batam.store.ChartPiePtkPerMataPelajaran', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ChartPiePtkPerMataPelajaran',
    model: 'simdik_batam.model.ChartPiePtkPerMataPelajaran',
    pageSize: 50,
    autoLoad: false
});