Ext.define('simdik_batam.store.AnalisisKebutuhanPrasaranaSMA', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.AnalisisKebutuhanPrasaranaSMA',
    model: 'simdik_batam.model.AnalisisKebutuhanPrasaranaSMA',
    pageSize: 50,
    autoLoad: false
});