Ext.define('simdik_batam.store.JenisSurat', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.JenisSurat',
    model: 'simdik_batam.model.JenisSurat',
    pageSize: 50,
    autoLoad: false
});