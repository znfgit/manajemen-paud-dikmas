Ext.define('simdik_batam.store.PesertaDidik', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.PesertaDidik',
    model: 'simdik_batam.model.PesertaDidik',
    pageSize: 50,
    autoLoad: false
});