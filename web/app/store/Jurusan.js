Ext.define('simdik_batam.store.Jurusan', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.Jurusan',
    model: 'simdik_batam.model.Jurusan',
    pageSize: 50,
    autoLoad: false
});