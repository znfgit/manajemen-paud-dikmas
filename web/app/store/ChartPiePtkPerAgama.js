Ext.define('simdik_batam.store.ChartPiePtkPerAgama', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ChartPiePtkPerAgama',
    model: 'simdik_batam.model.ChartPiePtkPerAgama',
    pageSize: 50,
    autoLoad: false
});