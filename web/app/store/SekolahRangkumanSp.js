Ext.define('simdik_batam.store.SekolahRangkumanSp', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.SekolahRangkumanSp',
    model: 'simdik_batam.model.SekolahRangkumanSp',
    pageSize: 50,
    autoLoad: false
});