Ext.define('simdik_batam.store.ChartBarPesertaDidikPerJumlahRombel', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ChartBarPesertaDidikPerJumlahRombel',
    model: 'simdik_batam.model.ChartBarPesertaDidikPerJumlahRombel',
    pageSize: 50,
    autoLoad: false
});