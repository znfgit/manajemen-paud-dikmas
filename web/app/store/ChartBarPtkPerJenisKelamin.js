Ext.define('simdik_batam.store.ChartBarPtkPerJenisKelamin', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ChartBarPtkPerJenisKelamin',
    model: 'simdik_batam.model.ChartBarPtkPerJenisKelamin',
    pageSize: 50,
    autoLoad: false
});