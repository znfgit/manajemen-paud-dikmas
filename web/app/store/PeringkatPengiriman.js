Ext.define('simdik_batam.store.PeringkatPengiriman', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.PeringkatPengiriman',
    model: 'simdik_batam.model.PeringkatPengiriman',
    pageSize: 50,
    autoLoad: false
});