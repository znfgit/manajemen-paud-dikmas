Ext.define('simdik_batam.store.ChartBarSekolahPerStatus', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ChartBarSekolahPerStatus',
    model: 'simdik_batam.model.ChartBarSekolahPerStatus',
    pageSize: 50,
    autoLoad: false
});