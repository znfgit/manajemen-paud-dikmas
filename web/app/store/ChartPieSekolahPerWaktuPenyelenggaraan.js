Ext.define('simdik_batam.store.ChartPieSekolahPerWaktuPenyelenggaraan', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ChartPieSekolahPerWaktuPenyelenggaraan',
    model: 'simdik_batam.model.ChartPieSekolahPerWaktuPenyelenggaraan',
    pageSize: 50,
    autoLoad: false
});