Ext.define('simdik_batam.store.ProgresData', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ProgresData',
    model: 'simdik_batam.model.ProgresData',
    pageSize: 50,
    autoLoad: false
});