Ext.define('simdik_batam.store.ChartBarPesertaDidikPerAgama', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.ChartBarPesertaDidikPerAgama',
    model: 'simdik_batam.model.ChartBarPesertaDidikPerAgama',
    pageSize: 50,
    autoLoad: false
});