Ext.define('simdik_batam.store.Peran', {
    extend: 'Ext.data.Store',
    requires: 'simdik_batam.model.Peran',
    model: 'simdik_batam.model.Peran',
    pageSize: 50,
    autoLoad: false    
});