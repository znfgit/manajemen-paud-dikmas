Ext.define('simdik_batam.Viewport', {
    extend: 'Ext.container.Viewport',
    requires:[
        'Ext.layout.container.Fit',
        'simdik_batam.view.main.Main',
        'simdik_batam.view.main.MainController'
    ],

    layout: {
        type: 'fit'
    },

    controller: 'main',

    items: [{
        xtype: 'app-main'
    }]
});
