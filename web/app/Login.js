Ext.define('simdik_batam.Login', {
    extend: 'Ext.container.Viewport',
    requires:[
        'Ext.layout.container.Fit',
        'simdik_batam.LoginController'
    ],
    controller:'login',
    layout: {
        type: 'fit'
    },
    initComponent:function(){
        var record = this.initialConfig.record ? this.initialConfig.record : false;
        var kabkota = this.initialConfig.kabkota ? this.initialConfig.kabkota : false;
        var labels = this.initialConfig.labels ? this.initialConfig.labels : false;

        if(record == 'dikdas'){
            var splash = 'splash_dikdas.png';
            var title = 'Pendidikan Dasar';
        }else if(record == 'dikmen'){
            var splash = 'manajemen-splash.png';
            var title = 'Pendidikan Menengah';
        }else if(record == 'dikdasmen'){
            var splash = 'splash.png';

            if(labels == 'Simdik'){
                var title = 'Sistem Informasi Pendidikan';
                labels = '';    
            }
            
        }

        var win = new Ext.window.Window({   
            height: 370,
            width: 500,
            modal:false,
            resizable: false,
            title: labels,
            autoScroll: false,
            frame: true,            
            border: true,
            layout: 'border',
            closable: false,
            items: [{
                region:'center',
                layout: 'fit',
                items: [{
                    bodyStyle: 'background-image:url(resources/img/'+splash+');background-size:cover',
                    plain: true,
                    border: false,
                    anchor: '100%'
                }]
            },{
                region:'south',
                layout: 'fit',
                border: false,
                items: [{
                    xtype:'form',
                    frame: false,            
                    border: false,
                    width: 600,
                    bodyPadding: '10 10 10 10',
                    defaultType: 'textfield',
                    defaults: {
                        anchor: '100%'
                    },
                    items: [
                        {
                            allowBlank: false,
                            fieldLabel: 'Username',
                            name: 'username',
                            emptyText: 'Email',
                            vtype: 'email',
                            anchor: '100%',
                            tabIndex:1
                        },{
                            allowBlank: false,
                            fieldLabel: 'Password',
                            name: 'password',
                            emptyText: 'Password',
                            inputType: 'password',
                            anchor: '100%',
                            tabIndex:2
                        }
                    ]
                }]
            }],
            buttons: [{ 
                text: 'Login' ,
                handler: function(btn){

                    // var username = btn.up('window').down('panel[region=south]').down('form').down('textfield[name=username]');
                    // var password = btn.up('window').down('panel[region=south]').down('form').down('textfield[name=password]');
                    var form = btn.up('window').down('panel[region=south]').down('form').getForm();

                    if (form.isValid()) { 
                    
                        Ext.MessageBox.show({
                            msg: 'Mohon Tunggu ...',
                            progressText: 'Mohon Tunggu...',
                            width: 200,
                            wait: true
                        });

                        form.submit({
                            clientValidation: true,
                            url: 'login',
                            submitEmptyText: false,
                            success: function(form, action) {
                                
                                var val = action.result;
                                
                                if(val){
                                    if (val.success) {
                                        Ext.MessageBox.hide();
                                        Ext.MessageBox.alert('Berhasil', 'Login Berhasil<br> Mohon tunggu beberapa saat....');
                                        
                                        setTimeout( function() {
                                            window.location='.';
                                        }, 50);
                                    } else {
                                        Ext.MessageBox.alert('Gagal', val.message);
                                    }
                                }else{
                                    Ext.MessageBox.alert('Gagal', 'Ada kesalahan pada sistem. Mohon coba beberapa saat lagi');
                                }
                            },
                            failure: function(form, action) {
                                //...
                                var val = action.result;
                                Ext.Msg.alert('Gagal', val.message);
                                Ext.MessageBox.hide();
                            }
                        });

                    } else {
                        Ext.Msg.alert('Error', 'Form harap isi dengan benar');
                    }


                }
            }]
        });

        win.show();
    }
});
