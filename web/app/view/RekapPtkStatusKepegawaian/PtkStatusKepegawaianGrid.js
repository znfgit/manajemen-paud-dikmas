Ext.define('simdik_batam.view.RekapPtkStatusKepegawaian.PtkStatusKepegawaianGrid', {
    extend: 'Ext.grid.Panel',
    requires:[
        'simdik_batam.view.RekapPtkStatusKepegawaian.RekapPtkStatusKepegawaianController'
    ],
    alias: 'widget.ptkstatuskepegawaiangrid',
    title: '',
    selType: 'rowmodel',
    controller: 'rekapptkstatuskepegawaian',
    autoScroll: true,
    columnLines:true,
    listeners:{
        afterrender:'setupPtkStatusKepegawaianGrid'
    },
    formatMoney: function(a, c, d, t){
        var n = a, 
            c = isNaN(c = Math.abs(c)) ? 0 : c, 
            d = d == undefined ? "," : d, 
            t = t == undefined ? "." : t, 
            s = n < 0 ? "-" : "", 
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('simdik_batam.store.chartBarPtkStatusKepegawaian');
        
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        this.getSelectionModel().setSelectionMode('SINGLE');
        
        this.columns = [{
            header: 'Wilayah',
            width:200,
            sortable: true,
            dataIndex: 'desk',
			hideable: false,
            hidden: false
        },{
            header: 'PNS',
            width: 150,
            sortable: true,
            dataIndex: 'pns',
			hideable: false,
            align:'right',
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },{
            header: 'PNS Diperbantukan',
            width: 150,
            sortable: true,
            dataIndex: 'pns_diperbantukan',
            hideable: false,
            align:'right',
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },{
            header: 'PNS Depag',
            width: 150,
            sortable: true,
            dataIndex: 'pns_depag',
            hideable: false,
            align:'right',
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },{
            header: 'CNPS',
            width: 150,
            sortable: true,
            dataIndex: 'cpns',
            hideable: false,
            align:'right',
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },{
            header: 'GTY/PTY',
            width: 150,
            sortable: true,
            dataIndex: 'gty_pty',
            hideable: false,
            align:'right',
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },{
            header: 'GTT/PTT Provinsi',
            width: 150,
            sortable: true,
            dataIndex: 'gtt_ptt_provinsi',
            hideable: false,
            align:'right',
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },{
            header: 'GTT/PTT Kab/Kota',
            width: 150,
            sortable: true,
            dataIndex: 'gtt_ptt_kabkota',
            hideable: false,
            align:'right',
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },{
            header: 'Guru Bantu Pusat',
            width: 150,
            sortable: true,
            dataIndex: 'guru_bantu_pusat',
            hideable: false,
            align:'right',
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },{
            header: 'Guru Honor Sekolah',
            width: 150,
            sortable: true,
            dataIndex: 'guru_honor_sekolah',
            hideable: false,
            align:'right',
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },{
            header: 'Tenaga Honor Sekolah',
            width: 150,
            sortable: true,
            dataIndex: 'tenaga_honor_sekolah',
            hideable: false,
            align:'right',
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },{
            header: 'Lainnya',
            width: 150,
            sortable: true,
            dataIndex: 'lainnya',
            align:'right',
			hideable: false,
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        }];

        var pilihBentukStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'nama'],
            data : [
                {"id":"13", "nama":"SMA"},
                {"id":"15", "nama":"SMK"},
                {"id":"14", "nama":"SMLB"},
                {"id":"999", "nama":"Semua"}
            ]
        });


        // this.dockedItems = [{
        //     xtype: 'toolbar',
        //     items: ['-',{
        //         xtype:'combobox',
        //         name: 'pilihbentukcombo',
        //         emptyText:'Pilih Bentuk Pendidikan...',
        //         // itemId: 'pilihbentukcombo',
        //         store: pilihBentukStore,
        //         queryMode: 'local',
        //         displayField: 'nama',
        //         valueField: 'id',
        //         width: 200,
        //         listeners: {
        //             'change': function(field, newValue, oldValue) {
                        
        //                 grid.getStore().reload();

        //                 // this.up('panel').up('panel').up('panel').down('panel').down('panel').down('chartpiepdagama').getStore().reload();
        //                 // this.up('panel').up('panel').up('panel').down('panel').down('panel[itemId=chartbar]').down('chartbarpdagama').getStore().reload();
        //                 // // this.up('panel').up('panel').up('panel').down('panel').down('panel').setTitle('Jumlah ' + field + ' Negeri Swasta');
        //             }
        //         }
        //     }]
        // }];
        
        this.bbar = Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
        
        this.callParent(arguments);

        // grid.store.load();
    }
});