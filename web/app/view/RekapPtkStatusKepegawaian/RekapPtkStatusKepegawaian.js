Ext.define('simdik_batam.view.RekapPtkStatusKepegawaian.RekapPtkStatusKepegawaian', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.ptkstatuskepegawaianpanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

    	this.items = [{
            // xtype: 'ptkstatuskepegawaiangrid',
            xtype:'panel',
            region:'center',
            split:true,
            layout:'border',
            items:[{
                xtype: 'ptkstatuskepegawaiantotalgrid',
                // xtype:'panel',
                region:'north',
                height:300,
                collapsible:true,
                title:'Jumlah PTK Berdasarkan Status Kepegawaian',
                split:true
            },{
                xtype:'ptkstatuskepegawaiangrid',
                region:'center',
                title: 'Sebaran Jumlah PTK Berdasarkan Status Kepegawaian'
            }]
        },{
            xtype: 'panel',
            region:'east',
            flex:1,
            // collapsible:true,
            split:true,
            autoScroll:true,
            layout: {
                type:'vbox',
                padding:'0',
                align:'stretch'
            },
            defaults:{margin:'0 0 10 0'},
            items:[{
                xtype:'panel',
                itemId: 'chartpie',
                split: true,
                layout:'fit',
                border:false,
                title: 'Grafik Jumlah PTK BerdasarkanStatus Kepegawaian',
                items:[{
                    xtype: 'chartpieptkstatuskepegawaian',
                    height:300
                }]
            },{
                xtype:'panel',
                title: 'Grafik Sebaran Jumlah PTK Berdasarkan Status Kepegawaian',
                itemId: 'chartbar',
                split: true,
                layout:'fit',
                border:false,
                items:[{
                    xtype: 'chartbarptkstatuskepegawaian',
                    minHeight:500
                }]
            }]
        }];

    	this.callParent(arguments);
    }
});