Ext.define('simdik_batam.view.RekapPtkStatusKepegawaian.chartBarPtkStatusKepegawaian', {
    extend: 'Ext.Panel',
    xtype: 'chartbarptkstatuskepegawaian',


    width: 650,

    initComponent: function() {
        var me = this;

        this.myDataStore = Ext.create('simdik_batam.store.chartBarPtkStatusKepegawaian');

        me.items = [{
            xtype: 'cartesian',
            width: '100%',
            height: 460,
            legend: {
                docked: 'left'
            },
            interactions: ['itemhighlight'],
            store: this.myDataStore,
            insetPadding: {
                top: 40,
                left: 40,
                right: 40,
                bottom: 40
            },
            // sprites: [{
            //     type: 'text',
            //     text: 'Column Charts - Stacked Columns',
            //     font: '22px Helvetica',
            //     width: 100,
            //     height: 30,
            //     x: 40, // the sprite x position
            //     y: 20  // the sprite y position
            // }, {
            //     type: 'text',
            //     text: 'Data: Browser Stats 2012',
            //     font: '10px Helvetica',
            //     x: 12,
            //     y: 380
            // }, {
            //     type: 'text',
            //     text: 'Source: http://www.w3schools.com/',
            //     font: '10px Helvetica',
            //     x: 12,
            //     y: 390
            // }],
            axes: [{
                type: 'numeric',
                position: 'left',
                adjustByMajorUnit: true,
                grid: true,
                fields: ['pns'],
                minimum: 0
            }, {
                type: 'category',
                position: 'bottom',
                grid: true,
                fields: ['desk'],
                label: {
                    rotate: {
                        degrees: -45
                    }
                }
                
            }],
            series: [{
                type: 'bar',
                axis: 'left',
                title: [ 
                    'PNS', 
                    'PNS Diperbantukan', 
                    'PNS Depag',
                    'CPNS',
                    'GTY/PTY',
                    'GTT/PTT Propinsi',
                    'GTT/PTT Kab/Kota',
                    'Guru Bantu Pusat',
                    'Guru Honor Sekolah',
                    'Tenaga Honor Sekolah',
                    'lainnya'
                ],
                xField: 'desk',
                yField: [ 
                    'pns', 
                    'pns_diperbantukan', 
                    'pns_depag',
                    'cpns',
                    'gty_pty',
                    'gtt_ptt_propinsi',
                    'gtt_ptt_kabkota',
                    'guru_bantu_pusat',
                    'guru_honor_sekolah',
                    'tenaga_honor_sekolah',
                    'lainnya'
                ],
                stacked: true,
                style: {
                    opacity: 0.80
                },
                highlight: {
                    fillStyle: 'yellow'
                },
                tooltip: {
                    style: 'background: #fff',
                    renderer: function(storeItem, item) {
                        var browser = item.series.getTitle()[Ext.Array.indexOf(item.series.getYField(), item.field)];
                        this.setHtml(browser + ' di wilayah ' + storeItem.get('desk') + ': ' + storeItem.get(item.field));
                    }
                }
            }]
        }];

        this.myDataStore.load();

        this.callParent();
    }
});