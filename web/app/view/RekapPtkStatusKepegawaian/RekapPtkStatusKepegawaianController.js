Ext.define('simdik_batam.view.RekapPtkStatusKepegawaian.RekapPtkStatusKepegawaianController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.MessageBox'
    ],

    alias: 'controller.rekapptkstatuskepegawaian',

    setupPtkStatusKepegawaianGrid:function(grid){
    	grid.getStore().on('beforeload', function(store){
            var keyword = grid.up('panel').down('ptkstatuskepegawaiantotalgrid').down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var tahun_ajaran_id = grid.up('panel').down('ptkstatuskepegawaiantotalgrid').down('toolbar').down('tahunajarancombo').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword,
                tahun_ajaran_id: tahun_ajaran_id
            });
        });

    	grid.getStore().load();
    },
    setupPtkStatusKepegawaianTotalGrid:function(grid){
        grid.down('toolbar').add({
            xtype:'tahunajarancombo',
            emptyText:'Pilih Tahun Ajaran...',
            listeners: {
                change: function(combo, newValue, oldValue, eOpts ) {
                    var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();

                    grid.getStore().reload();

                    grid.up('panel').down('ptkstatuskepegawaiangrid').getStore().reload();

                    grid.up('panel').up('panel').down('panel[region=east]').down('chartpieptkstatuskepegawaian').down('polar').getStore().reload({
                        params:{
                            bentuk_pendidikan_id:keyword,
                            tahun_ajaran_id: newValue
                        }
                    });

                    grid.up('panel').up('panel').down('panel[region=east]').down('chartbarptkstatuskepegawaian').down('cartesian').getStore().reload({
                        params:{
                            bentuk_pendidikan_id:keyword,
                            tahun_ajaran_id: newValue
                        }
                    });
                }
            }
        });

    	grid.getStore().on('beforeload', function(store){
            var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var tahun_ajaran_id = grid.down('toolbar').down('tahunajarancombo').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword,
                tahun_ajaran_id: tahun_ajaran_id
            });
        });

    	grid.getStore().load();
    },
    setupChartPiePtkStatusKepegawaian:function(panel){

    },
    exportXls:function(btn){
        var url = btn.urls;

       var bentuk_pendidikan_id = btn.up('toolbar').down('combobox').getValue();

       window.location = url+'/'+bentuk_pendidikan_id;
    },
});