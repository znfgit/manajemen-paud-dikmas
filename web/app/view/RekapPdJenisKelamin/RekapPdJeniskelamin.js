Ext.define('simdik_batam.view.RekapPdJenisKelamin.RekapPdJenisKelamin', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.rekappdjeniskelamin',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

        this.items = [{
            xtype: 'panel',
            region:'east',
            split:true,
            flex: 1,
            autoScroll:true,
            collapsible:true,
            layout: {
                type:'vbox',
                padding:'0',
                align:'stretch'
            },
            defaults:{margin:'0 0 10 0'},
            items:[{
                xtype:'panel',
                title: 'Chart Jumlah PD Berdasarkan Jenis Kelamin',
                itemId: 'chartpie',
                split: true,
                layout:'fit',
                border:false,
                items:[{
                    xtype: 'chartpiepdjeniskelamin',
                    height:300
                }]
            },{
                xtype:'panel',
                title: 'Sebaran Jumlah PD Berdasarkan Jenis Kelamin',
                itemId: 'chartbar',
                split: true,
                layout:'fit',
                border:false,
                items:[{
                    xtype: 'chartbarpdjeniskelamin',
                    minHeight:500
                }]
            }]
        },{
            xtype: 'panel',
            region: 'center',
            layout:'border',
            title:'Jumlah PD Berdasarkan Jenis Kelamin',
            items:[{
                xtype: 'pdjeniskelamintotalgrid',
                region:'center'
            },{
                xtype: 'pdjeniskelamingrid',
                region: 'south',
                height:350,
                title:'Tabel Sebaran Jumlah PD Berdasarkan Jenis Kelamin'
            }]
        }];

        this.callParent(arguments);
    }
});