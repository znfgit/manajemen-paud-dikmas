Ext.define('simdik_batam.view.PeringkatPengiriman.PeringkatPengirimanController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.MessageBox'
    ],

    alias: 'controller.peringkatpengiriman',

    setupPeringkatPengirimanGrid:function(grid){
    	// grid.getStore().on('beforeload', function(store){
     //        // var keyword = grid.up('panel').down('ptkmatapelajarantotalgrid').down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
     //        var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            
     //        Ext.apply(store.proxy.extraParams, {
     //            bentuk_pendidikan_id: keyword
     //        });
     //    });
        Ext.Ajax.request({
            url: 'session',
            success: function(response){
                var text = response.responseText;
                
                var res = Ext.JSON.decode(response.responseText);

                if(res.level == 'dikdas'){
                    grid.columns[17].hide();
                    grid.columns[18].hide();
                    grid.columns[19].hide();

                    grid.columns[20].hide();
                    grid.columns[21].hide();
                    grid.columns[22].hide();

                    grid.columns[23].hide();
                    grid.columns[24].hide();
                    grid.columns[25].hide();
                }else if(res.level == 'dikmen'){
                    grid.columns[2].hide();
                    grid.columns[3].hide();
                    grid.columns[4].hide();

                    grid.columns[5].hide();
                    grid.columns[6].hide();
                    grid.columns[7].hide();

                    grid.columns[8].hide();
                    grid.columns[9].hide();
                    grid.columns[10].hide();

                    grid.columns[11].hide();
                    grid.columns[12].hide();
                    grid.columns[13].hide();

                    grid.columns[14].hide();
                    grid.columns[15].hide();
                    grid.columns[16].hide();
                }

            }
        });

    	grid.getStore().load();
    }
});