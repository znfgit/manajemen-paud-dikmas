Ext.define('simdik_batam.view.PeringkatPengiriman.PeringkatPengiriman', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.peringkatpengirimanpanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    initComponent:function(){

    	this.items = [{
    		xtype: 'peringkatpengirimangrid',
    		region: 'center'	
    	}];

    	this.callParent(arguments);
    }
});