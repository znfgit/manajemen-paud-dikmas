Ext.define('simdik_batam.view.PeringkatPengiriman.PeringkatPengirimanGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.peringkatpengirimangrid',
    // title: 'Peringkat Pengiriman',
    selType: 'rowmodel',
    autoScroll: true,
    requires:[
        'simdik_batam.view.PeringkatPengiriman.PeringkatPengirimanController'
    ],
    controller: 'peringkatpengiriman',
    listeners : {
        afterrender:'setupPeringkatPengirimanGrid',
        itemdblclick: function(dv, record, item, index, e) {

            if(record.data.id_level_wilayah != 3){
                this.store.reload({
                    params:{
                        var1: record.data.kode_wilayah,
                        var2: record.data.id_level_wilayah
                    }
                })
            }

            
        }
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('simdik_batam.store.PeringkatPengiriman');
        
        this.getSelectionModel().setSelectionMode('SINGLE');

        this.columns = [{
            header: 'ID',
            width: 60,
            sortable: true,
            dataIndex: 'kode_wilayah',
			hideable: false,
            hidden: true,
            field: {
                xtype: 'hidden'
            }
        },{
            header: 'Wilayah',
            width: 200,
            sortable: true,
            dataIndex: 'nama',
            locked   : true,
			hideable: false
        },{
                text: 'SD',
                columns: [{
                    header: 'Total',
                    width: 80,
                    sortable: true,
                    align:'right',
                    dataIndex: 'sd',
                    hideable: false
                },{
                    header: 'Terkirim',
                    width: 80,
                    sortable: true,
                    align:'right',
                    dataIndex: 'kirim_sd',
                    hideable: false
                },{
                    header: 'Sisa',
                    width: 80,
                    sortable: true,
                    align:'right',
                    dataIndex: 'kirim_sd',
                    hideable: false,
                    renderer: function(value, metaData, record, row, col, store, gridView){
                        return record.data.sd - value;
                    }
                }]
        },{
                text: 'SDLB',
                columns: [{
                    header: 'Total',
                    width: 80,
                    sortable: true,
                    align:'right',
                    dataIndex: 'sdlb',
                    hideable: false
                },{
                    header: 'Terkirim',
                    width: 80,
                    sortable: true,
                    align:'right',
                    dataIndex: 'kirim_sdlb',
                    hideable: false
                },{
                    header: 'Sisa',
                    width: 80,
                    sortable: true,
                    align:'right',
                    dataIndex: 'kirim_sdlb',
                    hideable: false,
                    renderer: function(value, metaData, record, row, col, store, gridView){
                        return record.data.sdlb - value;
                    }
                }]
        },{
                text: 'SMP',
                columns: [{
                    header: 'Total',
                    width: 80,
                    sortable: true,
                    align:'right',
                    dataIndex: 'smp',
                    hideable: false
                },{
                    header: 'Terkirim',
                    width: 80,
                    sortable: true,
                    align:'right',
                    dataIndex: 'kirim_smp',
                    hideable: false
                },{
                    header: 'Sisa',
                    width: 80,
                    sortable: true,
                    align:'right',
                    dataIndex: 'kirim_smp',
                    hideable: false,
                    renderer: function(value, metaData, record, row, col, store, gridView){
                        return record.data.smp - value;
                    }
                }]
        },{
                text: 'SMPLB',
                columns: [{
                    header: 'Total',
                    width: 80,
                    sortable: true,
                    align:'right',
                    dataIndex: 'smplb',
                    hideable: false
                },{
                    header: 'Terkirim',
                    width: 80,
                    sortable: true,
                    align:'right',
                    dataIndex: 'kirim_smplb',
                    hideable: false
                },{
                    header: 'Sisa',
                    width: 80,
                    sortable: true,
                    align:'right',
                    dataIndex: 'kirim_smplb',
                    hideable: false,
                    renderer: function(value, metaData, record, row, col, store, gridView){
                        return record.data.smplb - value;
                    }
                }]
        },{
                text: 'SLB',
                columns: [{
                    header: 'Total',
                    width: 80,
                    sortable: true,
                    align:'right',
                    dataIndex: 'slb',
                    hideable: false
                },{
                    header: 'Terkirim',
                    width: 80,
                    sortable: true,
                    align:'right',
                    dataIndex: 'kirim_slb',
                    hideable: false
                },{
                    header: 'Sisa',
                    width: 80,
                    sortable: true,
                    align:'right',
                    dataIndex: 'kirim_slb',
                    hideable: false,
                    renderer: function(value, metaData, record, row, col, store, gridView){
                        return record.data.slb - value;
                    }
                }]
        },{
                text: 'SMA',
                columns: [{
                    header: 'Total',
                    width: 80,
                    sortable: true,
                    align:'right',
                    dataIndex: 'sma',
                    hideable: false
                },{
                    header: 'Terkirim',
                    width: 80,
                    sortable: true,
                    align:'right',
                    dataIndex: 'kirim_sma',
                    hideable: false
                },{
                    header: 'Sisa',
                    width: 80,
                    align:'right',
                    sortable: true,
                    dataIndex: 'kirim_sma',
                    hideable: false,
                    renderer: function(value, metaData, record, row, col, store, gridView){
                        return record.data.sma - value;
                    }
                }]
        },{
            text: 'SMK',
            columns: [{
                header: 'Total',
                width: 80,
                sortable: true,
                dataIndex: 'smk',
                align:'right',
                hideable: false
            },{
                header: 'Terkirim',
                width: 80,
                sortable: true,
                dataIndex: 'kirim_smk',
                align:'right',
                hideable: false
            },{
                header: 'Sisa',
                width: 80,
                sortable: true,
                dataIndex: 'kirim_smk',
                align:'right',
                hideable: false,
                renderer: function(value, metaData, record, row, col, store, gridView){
                    return record.data.smk - value;
                }
            }]
        },{
            text: 'SMLB',
            columns: [{
                header: 'Total',
                width: 80,
                sortable: true,
                dataIndex: 'smlb',
                align:'right',
                hideable: false
            },{
                header: 'Terkirim',
                width: 80,
                sortable: true,
                dataIndex: 'kirim_smlb',
                align:'right',
                hideable: false
            },{
                header: 'Sisa',
                width: 80,
                sortable: true,
                dataIndex: 'kirim_smlb',
                align:'right',
                hideable: false,
                renderer: function(value, metaData, record, row, col, store, gridView){
                    return record.data.smlb - value;
                }
            }]
        },{
            header: 'Sekolah Total',
            width: 110,
            sortable: true,
            dataIndex: 'sma',
            align:'right',
            hideable: false,
            renderer: function(value, metaData, record, row, col, store, gridView){
                return (record.data.slb + record.data.smplb + record.data.sdlb + record.data.smp + record.data.sd + record.data.sma + record.data.smk + record.data.smlb);
            }
        },{
            header: 'Terkirim Total',
            width: 100,
            sortable: true,
            dataIndex: 'sma',
            align:'right',
            hideable: false,
            renderer: function(value, metaData, record, row, col, store, gridView){
                return (record.data.kirim_slb + record.data.kirim_smplb + record.data.kirim_sdlb + record.data.kirim_smp + record.data.kirim_sd + record.data.kirim_sma + record.data.kirim_smk + record.data.kirim_smlb);
            }
        },{
            header: 'Sisa Total',
            width: 100,
            sortable: true,
            dataIndex: 'sma',
            align:'right',
            hideable: false,
            renderer: function(value, metaData, record, row, col, store, gridView){
                return (record.data.slb + record.data.smplb + record.data.sdlb + record.data.smp + record.data.sd + record.data.sma + record.data.smk + record.data.smlb) - (record.data.kirim_slb + record.data.kirim_smplb + record.data.kirim_sdlb + record.data.kirim_smp + record.data.kirim_sd + record.data.kirim_sma + record.data.kirim_smk + record.data.kirim_smlb);
            }
        },{
            header: '%',
            width: 100,
            sortable: true,
            dataIndex: 'persen',
            align:'right',
            hideable: false
        }];

        this.bbar = Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });

        // this.dockedItems = [{
        //     xtype:'toolbar',
        //     dock:'top',
        //     items:[{
        //         text: 'Kembali ke Atas',
        //         itemId: 'kembali',
        //         glyph:  61538,
        //         handler:function(btn){
        //             grid.setLoading(true);

        //             var mst_kode_wilayah = grid.store.getAt(0).data.mst_kode_wilayah;
        //             var id_level_wilayah = grid.store.getAt(0).data.id_level_wilayah - 1;

        //             var storeParent = Ext.create('infopendataan.store.MstWilayah');
        
        //             storeParent.load({
        //                 params:{
        //                     kowil: mst_kode_wilayah
        //                 }
        //             });

        //             storeParent.on('load', function(store, records, options) {
        //                 grid.setLoading(false);

        //                 var rec = storeParent.findRecord('kode_wilayah', mst_kode_wilayah);

        //                 console.log(rec.data.mst_kode_wilayah);
        //                 console.log(rec.data.id_level_wilayah);

        //                 grid.store.reload({
        //                     params:{
        //                         var1: rec.data.mst_kode_wilayah,
        //                         var2: rec.data.id_level_wilayah - 1
        //                     }
        //                 })
        //             });

                    

        //             // grid.store.reload({
        //             //     params:{
        //             //         var1: mst_kode_wilayah,
        //             //         var2: id_level_wilayah
        //             //     }
        //             // })
        //         }
        //     }]
        // }];
        
        
        this.callParent(arguments);
    }
});