// Override Kecamatan

Ext.define('model_sekolah', {
    extend: 'Ext.data.Model',
    idProperty: 'sekolah_id',
    proxy: {
        type: 'rest',
        url : 'get/Sekolah',
        reader: {
            type: 'json',
            root: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);                    
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data MstWilayah ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});

Ext.define('store_sekolah', {
    extend: 'Ext.data.Store',
    requires: 'model_sekolah',
    model: 'model_sekolah',
    pageSize: 50,
    autoLoad: false    
});

//Ext.override('DataDikdas.view._components.combokecamatan', {
Ext.define('simdik_batam.view._GlobalComponent.Combo.ComboSekolah', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'remote',
    pageSize: 20,
    valueField: 'sekolah_id',
    displayField: 'nama',
    label: 'Ref.sekolah',    
    alias: 'widget.combosekolah',
    emptyText:'Cari Sekolah',
    //hideTrigger: false,    
    //tpl: '<div class="search-item">{nama} - {nama_kabupaten}</div>',
    listConfig: {
        loadingText: 'Mencari...',
        emptyText: 'Sekolah tidak ditemukan'

		/*
        // Custom rendering template for each item
        getInnerTpl: function() {
            return '<div class="search-item">{nama} - {nama_kabupaten}</div>';
        }
		*/
    },
    listeners: {
        change: function(combo, options) {
            
        }
    },
    initComponent: function() {
        this.store = Ext.create('store_sekolah', {
            model: 'model_sekolah'
        });
        this.callParent(arguments); 
    }
});
