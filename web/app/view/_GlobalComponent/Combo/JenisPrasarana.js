Ext.define('simdik_batam.view._GlobalComponent.Combo.JenisPrasarana', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'remote',
    pageSize: 20,
    valueField: 'jenis_prasarana_id',
    displayField: 'nama',
    label: 'Ref.jenis prasarana',    
    alias: 'widget.jenisprasaranacombo',
    //hideTrigger: false,
    listeners: {
        beforerender: function(combo, options) {
            this.store.load();
        }
    },
    initComponent: function() {
        this.store = Ext.create('simdik_batam.store.JenisPrasarana', {
            model: 'simdik_batam.model.JenisPrasarana',
            sorters: ['jenis_prasarana_id']
        });
        this.callParent(arguments); 
    }
});