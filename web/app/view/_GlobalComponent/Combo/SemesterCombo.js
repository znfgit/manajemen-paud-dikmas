Ext.define('simdik_batam.view._GlobalComponent.Combo.SemesterCombo', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'remote',
    pageSize: 20,
    valueField: 'semester_id',
    displayField: 'nama',
    label: 'Ref.semester',    
    alias: 'widget.semestercombo',
    //hideTrigger: false,
    listeners: {
        beforerender: function(combo, options) {
            this.store.load();
        }
    },
    initComponent: function() {
        this.store = Ext.create('simdik_batam.store.Semester', {
            model: 'simdik_batam.model.Semester',
            sorters: ['semester_id']
        });
        this.callParent(arguments); 
    }
});