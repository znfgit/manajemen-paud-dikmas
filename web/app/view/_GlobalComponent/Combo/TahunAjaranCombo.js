Ext.define('simdik_batam.view._GlobalComponent.Combo.TahunAjaranCombo', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'local',
    alias: 'widget.tahunajarancombo',
    valueField: 'tahun_ajaran_id',
    displayField: 'nama',
    label: 'Ref.tahun ajaran',
	// editable: false,
    initComponent: function() {
        this.store = Ext.create('simdik_batam.store.TahunAjaran', {
            model: 'simdik_batam.model.TahunAjaran',
            sorters: ['tahun_ajaran_id'],
            autoLoad: true
        });
        this.callParent(arguments); 
    }
});