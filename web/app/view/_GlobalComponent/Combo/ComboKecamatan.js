// Override Kecamatan

Ext.define('model_kecamatan', {
    extend: 'Ext.data.Model',
    idProperty: 'kode_wilayah',
    fields: [
        { name: 'kode_wilayah', type: 'string'  }, 
        { name: 'id_level_wilayah', type: 'int', useNull: true },
        { name: 'id_level_wilayah_str', type: 'string'  }, 
        { name: 'negara_id', type: 'string', useNull: true },
        { name: 'negara_id_str', type: 'string'  }, 
        { name: 'kode_wilayah_induk', type: 'string', useNull: true },
        { name: 'kode_wilayah_induk_str', type: 'string'  }, 
        { name: 'nama_kabupaten', type: 'string'  }, 
        { name: 'nama', type: 'string'  }, 
        { name: 'asal_wilayah', type: 'string'  }, 
        { name: 'kode_bps', type: 'string'  }, 
        { name: 'kode_dagri', type: 'string'  }, 
        { name: 'kode_keu', type: 'string'  }, 
        { name: 'create_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'}, 
        { name: 'expired_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'}
    ],
    proxy: {
        type: 'rest',
        url : 'get/MstWilayah',
        reader: {
            type: 'json',
            root: 'rows',
            totalProperty: 'results'
        },
        listeners: {
            exception: function(proxy, response, operation, eOpts) {
                // console.log(response);
                if (response.status == '400') {
                    var json = Ext.decode(response.responseText);                    
                    //Xond.msg('Error', json.message);
                    //Ext.Msg.alert('Error', 'Gagal menyimpan data MstWilayah ('+ errorMsg +')');
                    Ext.Msg.alert('Error', json.message);
                }
            }
        }
    }
});

Ext.define('store_kecamatan', {
    extend: 'Ext.data.Store',
    requires: 'model_kecamatan',
    model: 'model_kecamatan',
    pageSize: 50,
    autoLoad: false    
});

//Ext.override('DataDikdas.view._components.combokecamatan', {
Ext.define('simdik_batam.view._GlobalComponent.Combo.ComboKecamatan', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'remote',
    pageSize: 20,
    valueField: 'kode_wilayah',
    displayField: 'nama_render',
    label: 'Ref.mst wilayah',    
    alias: 'widget.combokecamatan',
    emptyText:'Cari Wilayah',
    //hideTrigger: false,    
    //tpl: '<div class="search-item">{nama} - {nama_kabupaten}</div>',
    listConfig: {
        loadingText: 'Mencari...',
        emptyText: 'Kecamatan tidak ditemukan'

		/*
        // Custom rendering template for each item
        getInnerTpl: function() {
            return '<div class="search-item">{nama} - {nama_kabupaten}</div>';
        }
		*/
    },
    listeners: {
        change: function(combo, options) {
            
        }
    },
    initComponent: function() {
        this.store = Ext.create('store_kecamatan', {
            model: 'model_kecamatan'
        });

        this.callParent(arguments); 
    }
});
