Ext.define('simdik_batam.view._GlobalComponent.combo.KeahlianLaboratoriumCombo', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'remote',
    pageSize: 20,
    valueField: 'keahlian_laboratorium_id',
    displayField: 'nama',
    label: 'Ref.keahlian laboratorium',    
    alias: 'widget.keahlianlaboratoriumcombo',
    //hideTrigger: false,
    listeners: {
        beforerender: function(combo, options) {
            this.store.load();
        }
    },
    initComponent: function() {
        this.store = Ext.create('simdik_batam.store.KeahlianLaboratorium', {
            model: 'simdik_batam.model.KeahlianLaboratorium',
            sorters: ['keahlian_laboratorium_id']
        });
        this.callParent(arguments); 
    }
});