Ext.define('simdik_batam.view._GlobalComponent.Combo.ComboJenisSurat', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'remote',
    pageSize: 20,
    valueField: 'jenis_surat_id',
    displayField: 'nama',
    label: 'Ref.jenis_surat',    
    alias: 'widget.combojenissurat',
    emptyText:'Cari Jenis Surat',
    //hideTrigger: false,    
    //tpl: '<div class="search-item">{nama} - {nama_kabupaten}</div>',
    listConfig: {
        loadingText: 'Mencari...',
        emptyText: 'Jenis Surat tidak ditemukan'

		/*
        // Custom rendering template for each item
        getInnerTpl: function() {
            return '<div class="search-item">{nama} - {nama_kabupaten}</div>';
        }
		*/
    },
    listeners: {
        change: function(combo, options) {
            
        }
    },
    initComponent: function() {
        this.store = Ext.create('simdik_batam.store.JenisSurat');
        this.callParent(arguments); 
    }
});
