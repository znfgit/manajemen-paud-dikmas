Ext.define('simdik_batam.view._GlobalComponent.combo.PeranCombo', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'local',
    alias: 'widget.perancombo',
    valueField: 'peran_id',
    displayField: 'nama',
    label: 'Ref.peran',
	// editable: false,
    initComponent: function() {
        this.store = Ext.create('simdik_batam.store.Peran', {
            model: 'simdik_batam.model.Peran',
            sorters: ['peran_id'],
            autoLoad: true
        });
        this.callParent(arguments); 
    }
});