Ext.define('simdik_batam.view._GlobalComponent.Combo.BentukPendidikanCombo', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'remote',
    pageSize: 20,
    valueField: 'bentuk_pendidikan_id',
    displayField: 'nama',
    label: 'Ref.bentuk_pendidikan',    
    alias: 'widget.bentukpendidikancombo',
    //hideTrigger: false,
    listeners: {
        beforerender: function(combo, options) {
            // this.store.load();
        }
    },
    initComponent: function() {
        this.store = Ext.create('simdik_batam.store.BentukPendidikan', {
            model: 'simdik_batam.model.BentukPendidikan',
            sorters: ['bentuk_pendidikan_id']
        });
        this.store.load();
        
        this.callParent(arguments); 
    }
});