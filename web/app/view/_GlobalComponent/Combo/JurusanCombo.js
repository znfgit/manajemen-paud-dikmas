Ext.define('simdik_batam.view._GlobalComponent.Combo.JurusanCombo', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'remote',
    pageSize: 20,
    valueField: 'jurusan_id',
    displayField: 'nama_jurusan',
    label: 'Ref.jurusan',    
    alias: 'widget.jurusancombo',
    //hideTrigger: false,
    listeners: {
        beforerender: function(combo, options) {
            // this.store.load();
        }
    },
    initComponent: function() {
        this.store = Ext.create('simdik_batam.store.Jurusan', {
            model: 'simdik_batam.model.Jurusan',
            sorters: ['jurusan_id']
        });
        this.store.load();
        
        this.callParent(arguments); 
    }
});