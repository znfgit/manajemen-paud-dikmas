Ext.define('simdik_batam.view._GlobalComponent.Combo.JenisPtkCombo', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'remote',
    pageSize: 20,
    valueField: 'jenis_ptk_id',
    displayField: 'jenis_ptk',
    label: 'Ref.jenis_ptk',    
    alias: 'widget.jenisptkcombo',
    //hideTrigger: false,
    listeners: {
        beforerender: function(combo, options) {
            // this.store.load();
        }
    },
    initComponent: function() {
        this.store = Ext.create('simdik_batam.store.JenisPtk', {
            model: 'simdik_batam.model.JenisPtk',
            sorters: ['jenis_ptk_id']
        });
        this.store.load();
        
        this.callParent(arguments); 
    }
});