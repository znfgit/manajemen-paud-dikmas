Ext.define('simdik_batam.view._GlobalComponent.Combo.MstWilayahCombo', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'remote',
    pageSize: 20,
    valueField: 'kode_wilayah',
    displayField: 'nama_render',
    label: 'Ref.mst wilayah',    
    alias: 'widget.mstwilayahcombo',
    //hideTrigger: false,
    listeners: {
        beforerender: function(combo, options) {
            // this.store.load();
        }
    },
    initComponent: function() {
        this.store = Ext.create('simdik_batam.store.MstWilayah', {
            model: 'simdik_batam.model.MstWilayah',
            sorters: ['mst_wilayah_id']
        });
        // this.store.load();
        
        this.callParent(arguments); 
    }
});