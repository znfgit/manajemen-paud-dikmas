// AnalisisKebutuhanGuruDanKelas
Ext.define('simdik_batam.view.AnalisisKebutuhanGuruDanKelasSMP.AnalisisKebutuhanGuruDanKelasSMP', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.analisiskebutuhangurudankelassmppanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

    	this.items = [{
    		xtype: 'analisiskebutuhangurudankelassmpgrid',
            title: 'Analisis Kebutuhan Guru dan Kelas SMP',
            region:'center'
    	}];
        
    	this.callParent(arguments);
    }
});