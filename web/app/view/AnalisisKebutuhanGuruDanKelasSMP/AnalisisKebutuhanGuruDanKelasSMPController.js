Ext.define('simdik_batam.view.AnalisisKebutuhanGuruDanKelasSMP.AnalisisKebutuhanGuruDanKelasSMPController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.MessageBox'
    ],

    alias: 'controller.analisiskebutuhangurudankelassmp',

    setupAnalisisKebutuhanGuruDanKelasSMPGrid:function(grid){

    	grid.getStore().load();
    }
});