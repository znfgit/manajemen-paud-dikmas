Ext.define('simdik_batam.view.DataPokokPrasarana.DataPokokPrasarana', {
	extend: 'Ext.panel.Panel',
    // requires:[
    //     'simdik_batam.view.DataPokokSekolah.DataPokokSekolahController'
    // ],
	alias: 'widget.datapokokprasaranapanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    // controller: 'main',
    initComponent:function(){

    	this.items = [{
            xtype:'filterprasaranaform',
            region:'west',
            title:'Filter',
            collapsible:true,
            width:220,
            split:true,
            border:true
        },{
    		xtype: 'datapokokprasaranagrid',
            region:'center',
            title: 'Data Pokok Prasarana'
    	}];

    	this.callParent(arguments);
    }
});