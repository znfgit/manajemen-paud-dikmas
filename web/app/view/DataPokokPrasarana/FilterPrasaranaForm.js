Ext.define('simdik_batam.view.DataPokokPrasarana.FilterPrasaranaForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.filterprasaranaform',
    bodyPadding: 10,
    fieldDefaults: {
        anchor: '100%',
        labelWidth: 120
    },
    initComponent: function() {
        
        var record = this.initialConfig.record ? this.initialConfig.record : false;
        if (record){
            this.listeners = {
                afterrender: function(form, options) {
                    form.loadRecord(record);
                }
            };
        }

        /*this.on('beforeadd', function(form, field){
            if (!field.allowBlank)
              field.labelSeparator += '<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>';
        });*/
        var pilihBentukStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'nama'],
            data : [
                {"id":"5", "nama":"SD"},
                {"id":"6", "nama":"SMP"},
                {"id":"13", "nama":"SMA"},
                {"id":"15", "nama":"SMK"},
                {"id":"29", "nama":"SLB"},
                {"id":"14", "nama":"SMLB"},
                {"id":"999", "nama":"Semua"}
            ]
        });

        var pilihStatusStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'nama'],
            data : [
                {"id":"1", "nama":"Negeri"},
                {"id":"2", "nama":"Swasta"},
                {"id":"999", "nama":"Semua"}
            ]
        });
        
        this.items = [{
            xtype: 'textfield'
            ,fieldLabel: 'Kata Kunci'
            ,labelAlign: 'top'
            ,allowBlank: true
            ,maxValue: 99999999
            ,minValue: 0
            ,maxLength: 100
            ,enforceMaxLength: false
            ,name: 'keyword'
            ,emptyText: 'Ketik Kata Kunci Nama Prasarana...'
        },{
            xtype: 'jenisprasaranacombo',
            labelAlign: 'top',
            fieldLabel: 'Jenis Prasarana',
            name: 'jenisprasaranacombo',
            emptyText: 'Pilih Jenis Prasarana'
        },{
            xtype: 'button',
            text: 'Cari',
            glyph: 61452,
            handler: 'cari',
            anchor: '100%',
            scale: 'medium',
            margin: '10 0 0 0'
        }];

        // this.buttons = [];

        this.callParent(arguments);
    }
});