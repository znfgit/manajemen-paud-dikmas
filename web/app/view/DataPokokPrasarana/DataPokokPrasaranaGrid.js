Ext.define('simdik_batam.view.DataPokokPrasarana.DataPokokPrasaranaGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.datapokokprasaranagrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    listeners : {
        afterrender: 'setupDataPokokPrasaranaGrid'
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('simdik_batam.store.Prasarana');
        
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        this.getSelectionModel().setSelectionMode('SINGLE');
        
        this.columns = [{
            header: 'ID',
            width: 60,
            sortable: true,
            dataIndex: 'prasarana_id',
			hideable: false,
            hidden: true,
            field: {
                xtype: 'hidden'
            }
        },{
            header: 'Sekolah',
            width: 200,
            sortable: true,
            dataIndex: 'sekolah_id',
			hideable: false,
            hidden: false,
            renderer: function(v,p,r) {
                return r.data.sekolah_id_str;
            }
        },{
            header: 'Jenis Prasarana',
            width: 350,
            sortable: true,
            dataIndex: 'jenis_prasarana_id',
			hideable: false,
            renderer: function(v,p,r) {
                return r.data.jenis_prasarana_id_str;
            }
        },{
            header: 'Kepemilikan',
            width: 140,
            sortable: true,
            dataIndex: 'kepemilikan_sarpras_id',
			hideable: false,
            renderer: function(v,p,r) {
                return r.data.kepemilikan_sarpras_id_str;                      
            }
        },{
            header: 'Nama',
            width: 200,
            sortable: true,
            dataIndex: 'nama',
			hideable: false
        },{
            header: 'Panjang (m)',
            width: 120,
            sortable: true,
            align:'right',
            dataIndex: 'panjang',
			hideable: false
        },{
            header: 'Lebar (m)',
            width: 120,
            align:'right',
            sortable: true,
            dataIndex: 'lebar',
			hideable: false        
        }];
        
        this.dockedItems = [{
            xtype: 'toolbar',
            items: ['-']
        }];
        
        this.bbar = Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Menampilkan baris {0} - {1} dari {2}',
            emptyMsg: "Tidak ada data"
        });
        
        this.callParent(arguments);
    }
});