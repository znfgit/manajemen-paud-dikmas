Ext.define('simdik_batam.view.ProgresData.ProgresDataGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.progresdatagrid',
    selType: 'rowmodel',
    autoScroll: true,
    listeners : {
        afterrender:'setupProgresDataGrid',
        itemdblclick: function(dv, record, item, index, e) {

            if(record.data.id_level_wilayah != 3){
                this.store.reload({
                    params:{
                        kode_wilayah: record.data.kode_wilayah,
                        id_level_wilayah: record.data.id_level_wilayah
                    }
                })
            }

            
        }
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('simdik_batam.store.ProgresData');
        
        this.getSelectionModel().setSelectionMode('SINGLE');

        this.columns = [{
            header: 'ID',
            width: 60,
            sortable: true,
            dataIndex: 'kode_wilayah',
			hideable: false,
            hidden: true,
            field: {
                xtype: 'hidden'
            }
        },{
            header: 'Wilayah',
            width: 200,
            sortable: true,
            dataIndex: 'nama',
            locked   : true,
			hideable: false,
            renderer:function(v,p,r){
                return '<a href="#menu/RekapSekolahRangkumanSp/'+ r.data.id_level_wilayah +'/'+ r.data.kode_wilayah +'">' + v + '</v>';
            }
        },{
                text: 'SMA',
                columns: [{
                    header: 'Total',
                    width: 80,
                    sortable: true,
                    dataIndex: 'sma',
                    align:'right',
                    hideable: false
                },{
                    header: 'Data Masuk',
                    width: 100,
                    sortable: true,
                    dataIndex: 'kirim_sma',
                    align:'right',
                    hideable: false
                },{
                    header: 'Sisa',
                    width: 80,
                    sortable: true,
                    dataIndex: 'kirim_sma',
                    align:'right',
                    hideable: false,
                    renderer: function(value, metaData, record, row, col, store, gridView){
                        return record.data.sma - value;
                    }
                }]
        },{
            text: 'SMK',
            columns: [{
                header: 'Total',
                width: 80,
                sortable: true,
                dataIndex: 'smk',
                align:'right',
                hideable: false
            },{
                header: 'Data Masuk',
                width: 100,
                sortable: true,
                dataIndex: 'kirim_smk',
                align:'right',
                hideable: false
            },{
                header: 'Sisa',
                width: 80,
                sortable: true,
                dataIndex: 'kirim_smk',
                align:'right',
                hideable: false,
                renderer: function(value, metaData, record, row, col, store, gridView){
                    return record.data.smk - value;
                }
            }]
        },{
            text: 'SMLB',
            columns: [{
                header: 'Total',
                width: 80,
                sortable: true,
                dataIndex: 'smlb',
                align:'right',
                hideable: false
            },{
                header: 'Data Masuk',
                width: 100,
                sortable: true,
                dataIndex: 'kirim_smlb',
                align:'right',
                hideable: false
            },{
                header: 'Sisa',
                width: 80,
                sortable: true,
                dataIndex: 'kirim_smlb',
                align:'right',
                hideable: false,
                renderer: function(value, metaData, record, row, col, store, gridView){
                    return record.data.smlb - value;
                }
            }]
        },{
            header: 'Sekolah Total',
            width: 110,
            sortable: true,
            dataIndex: 'sma',
            align:'right',
            hideable: false,
            renderer: function(value, metaData, record, row, col, store, gridView){
                return (record.data.sma + record.data.smk + record.data.smlb);
            }
        },{
            header: 'Data Masuk Total',
            width: 150,
            sortable: true,
            dataIndex: 'sma',
            align:'right',
            hideable: false,
            renderer: function(value, metaData, record, row, col, store, gridView){
                return (record.data.kirim_sma + record.data.kirim_smk + record.data.kirim_smlb);
            }
        },{
            header: 'Sisa Total',
            width: 100,
            sortable: true,
            dataIndex: 'sma',
            align:'right',
            hideable: false,
            renderer: function(value, metaData, record, row, col, store, gridView){
                return (record.data.sma + record.data.smk + record.data.smlb) - (record.data.kirim_sma + record.data.kirim_smk + record.data.kirim_smlb);
            }
        },{
            header: '%',
            width: 100,
            sortable: true,
            dataIndex: 'persen',
            align:'right',
            hideable: false
        }];

        this.dockedItems = [{
            xtype:'toolbar',
            dock:'top',
            items:[{
                text: 'Kembali',
                itemId: 'kembali',
                glyph:  61536,
                handler:function(btn){
                    grid.setLoading(true);

                    var mst_kode_wilayah = grid.store.getAt(0).data.mst_kode_wilayah;
                    var id_level_wilayah = grid.store.getAt(0).data.id_level_wilayah - 1;

                    var storeParent = Ext.create('simdik_batam.store.MstWilayah');
        
                    storeParent.load({
                        params:{
                            kode_wilayah: mst_kode_wilayah,
                            mode:null
                        }
                    });

                    storeParent.on('load', function(store, records, options) {
                        grid.setLoading(false);

                        var rec = storeParent.findRecord('kode_wilayah', mst_kode_wilayah);

                        console.log(rec.data.mst_kode_wilayah);
                        console.log(rec.data.id_level_wilayah);

                        grid.store.reload({
                            params:{
                                kode_wilayah: rec.data.mst_kode_wilayah,
                                id_level_wilayah: rec.data.id_level_wilayah - 1
                            }
                        })
                    });

                    

                    // grid.store.reload({
                    //     params:{
                    //         var1: mst_kode_wilayah,
                    //         var2: id_level_wilayah
                    //     }
                    // })
                }
            }]
        }];

        this.bbar = ['-'];
        
        
        this.callParent(arguments);
    }
});