Ext.define('simdik_batam.view.ProgresData.ProgresData', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.progresdata',
    border: false,
    bodyBorder: false,
    layout: 'border',
    initComponent:function(){

    	this.items = [{
    		xtype: 'progresdatagrid',
    		region: 'center'	
    	}];

    	this.callParent(arguments);
    }
});