Ext.define('simdik_batam.view.RekapPtkMataPelajaran.RekapPtkMataPelajaran', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.ptkmatapelajaranpanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

    	this.items = [{
            // xtype: 'ptkstatuskepegawaiangrid',
            xtype:'panel',
            region:'center',
            split:true,
            layout:'border',
            items:[/*{
                xtype:'ptkmatapelajarantotalgrid',
                // xtype:'panel',
                region:'north',
                height:220,
                collapsible:true,
                title:'Jumlah PTK Berdasarkan Mata Pelajaran',
                split:true
            },*/{
                xtype:'ptkmatapelajarangrid',
                region:'center',
                title: 'Sebaran Jumlah PTK Berdasarkan Mata Pelajaran'
            }]
        },{
            xtype: 'panel',
            region:'south',
            height:300,
            collapsible:true,
            split:true,
            autoScroll:true,
            title: 'Grafik Sebaran Jumlah PTK Berdasarkan Mata Pelajaran',
            layout: {
                type:'fit',
                padding:'0',
                align:'stretch'
            },
            defaults:{margin:'0 0 10 0'},
            items:[{
                xtype: 'chartbarptkmatapelajaran',
                // title: 'Grafik Sebaran Jumlah PTK Berdasarkan Mata Pelajaran',
                itemId: 'chartbar',
                split: true,
                layout:'fit',
                border:false
            }]
        }];

    	this.callParent(arguments);
    }
});