Ext.define('simdik_batam.view.RekapPtkMataPelajaran.PtkMataPelajaranTotalGrid', {
    extend: 'Ext.grid.Panel',
    requires:[
        'simdik_batam.view.RekapPtkMataPelajaran.RekapPtkMataPelajaranController'
    ],
    controller: 'rekapptkmatapelajaran',
    alias: 'widget.ptkmatapelajarantotalgrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    listeners:{
        afterrender:'setupPtkMataPelajaranTotalGrid'
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('simdik_batam.store.ChartPiePtkPerMataPelajaran');
        
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        this.getSelectionModel().setSelectionMode('SINGLE');
        
        this.columns = [{
            header: ' ',
            flex:1,
            sortable: true,
            dataIndex: 'desk',
			hideable: false,
            hidden: false
        },{
            header: 'Jumlah',
            width: 150,
            sortable: true,
            dataIndex: 'jumlah',
			hideable: false,
            hidden: false
        }];

        var pilihBentukStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'nama'],
            data : [
                {"id":"5", "nama":"SD"},
                {"id":"6", "nama":"SMP"},
                {"id":"29", "nama":"SLB"},
                {"id":"999", "nama":"Semua"}
            ]
        });


        this.dockedItems = [{
            xtype: 'toolbar',
            items: ['-',{
                xtype:'bentukpendidikancombo',
                name: 'pilihbentukcombo',
                emptyText:'Pilih Bentuk Pendidikan...',
                displayField: 'nama',
                width: 200,
                listeners: {
                    'change': function(field, newValue, oldValue) {
                        
                        grid.getStore().reload();

                        grid.up('panel').down('ptkmatapelajarangrid').getStore().reload();

                        grid.up('panel').up('panel').down('panel[region=east]').down('chartpieptkmatapelajaran').down('polar').getStore().reload({
                            params:{
                                bentuk_pendidikan_id:newValue
                            }
                        });

                        grid.up('panel').up('panel').down('panel[region=east]').down('chartbarptkmatapelajaran').down('cartesian').getStore().reload({
                            params:{
                                bentuk_pendidikan_id:newValue
                            }
                        });

                        // this.up('panel').up('panel').up('panel').setTitle('tes');
                        // this.up('panel').up('panel').up('panel').down('panel').down('panel[itemId=chartbar]').down('chartbarptkjeniskelamin').getStore().reload();
                        // this.up('panel').up('panel').up('panel').down('panel').down('panel').setTitle('Jumlah ' + field + ' Negeri Swasta');
                    }
                }
            }]
        }];
        
        // this.bbar = Ext.create('Ext.PagingToolbar', {
        //     store: this.store,
        //     displayInfo: true,
        //     displayMsg: 'Displaying data {0} - {1} of {2}',
        //     emptyMsg: "Tidak ada data"
        // });
        
        this.callParent(arguments);
    }
});