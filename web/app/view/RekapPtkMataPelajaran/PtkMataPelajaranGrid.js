Ext.define('simdik_batam.view.RekapPtkMataPelajaran.PtkMataPelajaranGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.ptkmatapelajarangrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    columnLines:true,
    requires:[
        'simdik_batam.view.RekapPtkMataPelajaran.RekapPtkMataPelajaranController'
    ],
    controller: 'rekapptkmatapelajaran',
    listeners:{
        afterrender:'setupPtkMataPelajaranGrid'
    },
    formatMoney: function(a, c, d, t){
        var n = a, 
            c = isNaN(c = Math.abs(c)) ? 0 : c, 
            d = d == undefined ? "," : d, 
            t = t == undefined ? "." : t, 
            s = n < 0 ? "-" : "", 
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('simdik_batam.store.chartBarPtkMataPelajaran');
        
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        this.getSelectionModel().setSelectionMode('SINGLE');
        
        this.columns = [{
            header: 'Wilayah',
            width:170,
            locked   : true,
            sortable: true,
            dataIndex: 'desk',

			hideable: false,
            hidden: false
        },{
            header: 'Matematika',
            width: 120,
            sortable: true,
            dataIndex: 'matematika',
			hideable: false,
            align:'right',
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },{
            header: 'B. Indonesia',
            width: 150,
            sortable: true,
            dataIndex: 'bahasa_indonesia',
            hideable: false,
            align:'right',
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },{
            header: 'B. Inggris',
            width: 120,
            sortable: true,
            dataIndex: 'bahasa_inggris',
            hideable: false,
            align:'right',
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },{
            header: 'PKn',
            width: 120,
            sortable: true,
            dataIndex: 'pkn',
            hideable: false,
            align:'right',
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },{
            header: 'Pend. Agama',
            width: 120,
            sortable: true,
            dataIndex: 'pendidikan_agama',
            hideable: false,
            align:'right',
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },{
            header: 'PJOK',
            width: 120,
            sortable: true,
            dataIndex: 'pjok',
            hideable: false,
            align:'right',
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },{
            header: 'Seni Budaya',
            width: 120,
            sortable: true,
            dataIndex: 'seni_budaya',
            hideable: false,
            align:'right',
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },{
            header: 'TIK',
            width: 120,
            sortable: true,
            dataIndex: 'tik',
            hideable: false,
            align:'right',
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },{
            header: 'IPA',
            width: 120,
            sortable: true,
            dataIndex: 'ipa',
            hideable: false,
            align:'right',
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },{
            header: 'IPS',
            width: 120,
            sortable: true,
            dataIndex: 'ips',
			hideable: false,
            align:'right',
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        }];

        var pilihBentukStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'nama'],
            data : [
                {"id":"13", "nama":"SMA"},
                {"id":"15", "nama":"SMK"},
                {"id":"14", "nama":"SMLB"},
                {"id":"999", "nama":"Semua"}
            ]
        });


        this.dockedItems = [{
            xtype: 'toolbar',
            items: ['-',{
                xtype:'bentukpendidikancombo',
                name: 'pilihbentukcombo',
                emptyText:'Pilih Bentuk Pendidikan...',
                displayField: 'nama',
                width: 200,
                listeners: {
                    'change': function(field, newValue, oldValue) {
                        
                        grid.getStore().reload();

                        var semester_id = grid.down('toolbar').down('semestercombo').getValue();

                        // grid.up('panel').down('ptkstatuskepegawaiangrid').getStore().reload();

                        grid.up('panel').up('panel').down('panel[region=south]').down('chartbarptkmatapelajaran').down('cartesian').getStore().reload({
                            params:{
                                bentuk_pendidikan_id:newValue,
                                semester_id: semester_id
                            }
                        });
                    }
                }
            },{
                xtype:'button',
                text:'Export Xls',
                glyph: 61474,
                urls: 'excel/SebaranPtkPerMataPelajaran',
                handler:function(btn){
                    var bentuk_pendidikan_id = btn.up('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
                    var semester_id = btn.up('toolbar').down('semestercombo').getValue();

                    window.location = 'excel/SebaranPtkPerMataPelajaran?bentuk_pendidikan_id='+bentuk_pendidikan_id+'&semester_id='+semester_id;
                }
            }]
        }];
        
        // this.bbar = Ext.create('Ext.PagingToolbar', {
        //     store: this.store,
        //     displayInfo: true,
        //     displayMsg: 'Displaying data {0} - {1} of {2}',
        //     emptyMsg: "Tidak ada data"
        // });
        
        this.callParent(arguments);
    }
});