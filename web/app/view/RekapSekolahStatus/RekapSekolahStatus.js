Ext.define('simdik_batam.view.RekapSekolahStatus.RekapSekolahStatus', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.rekapsekolahstatus',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

        this.items = [{
            xtype: 'panel',
            region:'east',
            flex: 1,
            split:true,
            autoScroll:true,
            layout: {
                type:'vbox',
                padding:'0',
                align:'stretch'
            },
            defaults:{margin:'0 0 10 0'},
            items:[{
                xtype:'panel',
                title: 'Grafik Jumlah Sekolah Berdasarkan Status',
                itemId: 'chartpie',
                split: true,
                layout:'fit',
                border:false,
                items:[{
                    xtype: 'chartpiesekolahstatus',
                    height:300
                }]
            },{
                xtype:'panel',
                title: 'Grafik Sebaran Jumlah Sekolah Berdasarkan Status',
                itemId: 'chartbar',
                split: true,
                layout:'fit',
                border:false,
                items:[{
                    xtype: 'chartbarsekolahstatus',
                    minHeight:500
                }]
            }]
        },{
            xtype: 'panel',
            region: 'center',
            layout:'border',
            title:'Jumlah Sekolah Berdasarkan Status',
            items:[{
                xtype: 'sekolahstatustotalgrid',
                region:'center'
            },{
                xtype: 'sekolahstatusgrid',
                region: 'south',
                height:280,
                split:true,
                collapsible:true,
                title:'Tabel Sebaran Jumlah Sekolah Berdasarkan Status'
            }]
        }];

        this.callParent(arguments);
    }
});