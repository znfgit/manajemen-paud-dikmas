Ext.define('simdik_batam.view.RekapPtkAgama.chartPiePtkAgama', {
    extend: 'Ext.Panel',
    alias: 'widget.chartpieptkagama',
    width: 650,

    initComponent: function() {
        var me = this;

        this.myDataStore = Ext.create('simdik_batam.store.ChartPiePtkPerAgama');


        me.items = [{
            xtype: 'polar',
            width: '100%',
            height: 300,
            store: this.myDataStore,
            insetPadding: 5,
            innerPadding: 10,
            legend: {
                docked: 'right'
            },
            interactions: ['rotate', 'itemhighlight'],
            sprites: [{
                type: 'text',
                text: 'Sumber: Dapodik 2014/2015',
                font: '12px Helvetica',
                width: 100,
                height: 30,
                x: 40, // the sprite x position
                y: 20  // the sprite y position
            }],
            series: [{
                type: 'pie',
                angleField: 'jumlah',
                label: {
                    field: 'desk',
                    display: 'inside',
                    calloutLine: {
                        length: 60,
                        width: 3
                        // specifying 'color' is also possible here
                    }
                },
                highlight: true,
                tooltip: {
                    trackMouse: true,
                    renderer: function(storeItem, item) {
                    	if(storeItem.get('desk') == 'L'){
                    		var str = 'Laki-laki';
                    	}else{
                    		var str = 'Perempuan';
                    	}

                        this.setHtml(str + ': ' + storeItem.get('jumlah'));
                    }
                }
            }]
        }];

        this.myDataStore.load();

        this.callParent();
    }
});