Ext.define('simdik_batam.view.RekapPtkAgama.PtkAgamaTotalGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.ptkagamatotalgrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    listeners:{
        afterrender:'setupPtkAgamaTotalGrid'
    },
    formatMoney: function(a, c, d, t){
        var n = a, 
            c = isNaN(c = Math.abs(c)) ? 0 : c, 
            d = d == undefined ? "," : d, 
            t = t == undefined ? "." : t, 
            s = n < 0 ? "-" : "", 
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('simdik_batam.store.ChartPiePtkPerAgama');
        
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        this.getSelectionModel().setSelectionMode('SINGLE');
        
        this.columns = [{
            header: ' ',
            flex:1,
            sortable: true,
            dataIndex: 'desk',
			hideable: false,
            hidden: false
        },{
            header: 'Jumlah',
            width: 150,
            sortable: true,
            dataIndex: 'jumlah',
			hideable: false,
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        }];

        var pilihBentukStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'nama'],
            data : [
                {"id":"5", "nama":"SD"},
                {"id":"6", "nama":"SMP"},
                {"id":"29", "nama":"SLB"},
                {"id":"999", "nama":"Semua"}
            ]
        });


        this.dockedItems = [{
            xtype: 'toolbar',
            items: ['-',{
                xtype:'bentukpendidikancombo',
                name: 'pilihbentukcombo',
                emptyText:'Pilih Bentuk Pendidikan...',
                displayField: 'nama',
                width: 200,
                listeners: {
                    'change': function(field, newValue, oldValue) {
                        
                        grid.getStore().reload();

                        grid.up('panel').down('ptkagamagrid').getStore().reload();

                        grid.up('panel').up('panel').down('panel').down('chartpieptkagama').down('polar').getStore().reload({
                            params:{
                                bentuk_pendidikan_id:newValue
                            }
                        });

                        grid.up('panel').up('panel').down('panel').down('chartbarptkagama').down('cartesian').getStore().reload({
                            params:{
                                bentuk_pendidikan_id:newValue
                            }
                        });

                        // this.up('panel').up('panel').up('panel').setTitle('tes');
                        // this.up('panel').up('panel').up('panel').down('panel').down('panel[itemId=chartbar]').down('chartbarptkjeniskelamin').getStore().reload();
                        // this.up('panel').up('panel').up('panel').down('panel').down('panel').setTitle('Jumlah ' + field + ' Negeri Swasta');
                    }
                }
            },{
                xtype:'button',
                text:'Export Xls',
                glyph: 61474,
                urls: 'excel/SebaranPtkPerAgama',
                handler:function(btn){
                    var bentuk_pendidikan_id = btn.up('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
                    var tahun_ajaran_id = btn.up('toolbar').down('tahunajarancombo').getValue();

                    window.location = 'excel/SebaranPtkPerAgama?bentuk_pendidikan_id='+bentuk_pendidikan_id+'&tahun_ajaran_id='+tahun_ajaran_id;
                }
            }]
        }];
        
        // this.bbar = Ext.create('Ext.PagingToolbar', {
        //     store: this.store,
        //     displayInfo: true,
        //     displayMsg: 'Displaying data {0} - {1} of {2}',
        //     emptyMsg: "Tidak ada data"
        // });
        
        this.callParent(arguments);
    }
});