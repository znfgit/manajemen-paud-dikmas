Ext.define('simdik_batam.view.RekapPtkAgama.RekapPtkAgama', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.rekapptkagama',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

        this.items = [{
            xtype: 'panel',
            region:'east',
            flex: 1,
            split:true,
            autoScroll:true,
            collapsible: true,
            layout: {
                type:'vbox',
                padding:'0',
                align:'stretch'
            },
            defaults:{margin:'0 0 10 0'},
            items:[{
                xtype:'panel',
                title: 'Grafik Jumlah PTK Berdasarkan Agama',
                itemId: 'chartpie',
                split: true,
                layout:'fit',
                border:false,
                items:[{
                    xtype: 'chartpieptkagama',
                    height:300
                }]
            },{
                xtype:'panel',
                title: 'Grafik Sebaran Jumlah PTK Berdasarkan Agama',
                itemId: 'chartbar',
                split: true,
                layout:'fit',
                border:false,
                items:[{
                    xtype: 'chartbarptkagama',
                    minHeight:500
                }]
            }]
        },{
            xtype: 'panel',
            region: 'center',
            layout:'border',
            title:'Jumlah PTK Berdasarkan Agama',
            items:[{
                xtype: 'ptkagamatotalgrid',
                region:'center'
            },{
                xtype: 'ptkagamagrid',
                region: 'south',
                collapsible: true,
                split:true,
                height:280,
                title:'Tabel Sebaran Jumlah PTK Berdasarkan Agama'
            }]
        }];

        this.callParent(arguments);
    }
});