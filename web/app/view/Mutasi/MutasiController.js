Ext.define('simdik_batam.view.Mutasi.MutasiController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.MessageBox'
    ],

    alias: 'controller.mutasi',

    // MutasiPesertaDidik:function(id){
    //     Ext.Msg.alert(id);
    // },

    setupMutasiGrid:function(grid){

    	grid.getStore().load();
    },
    cetak:function(btn){
        var grid = btn.up('gridpanel');

        var rec = grid.getSelectionModel().getSelection()[0];

        if(!rec){
            Ext.Msg.alert('Pilih salah satu baris terlebih dahulu!');
        }else{

            // Ext.MessageBox.show({
            //     msg: 'Memuat Data ...',
            //     progressText: 'Memuat Data ...',
            //     width: 200,
            //     wait: true
            // });

            var urlCetak = 'cetak/Mutasi/'+rec.data.mutasi_id + '/lihat';

            var win = new Ext.window.Window({   
                title: 'Cetak Mutasi Peserta Didik: ' + rec.data.nama,
                width : 700,
                height: 600,
                modal:true,
                layout : 'fit',
                closeAction: 'hide',
                items : [{
                    xtype : "component",
                    autoEl : {
                        tag : "iframe",
                        src : urlCetak
                    }
                }],
                buttons: [{
                    text: 'Cetak PDF',
                    scope: this,
                    handler: function(btn) {
                        // window.location.replace('cetak/Mutasi/'+ rec.data.mutasi_id + '/pdf');
                        window.open('cetak/Mutasi/'+ rec.data.mutasi_id + '/pdf','_blank');
                    }
                }]
            });

            win.show();    

        }
    },
    hapus:function(btn){
        var grid = btn.up('gridpanel');

        var rec = grid.getSelectionModel().getSelection()[0];

        if(!rec){
            Ext.Msg.alert('Pilih salah satu baris terlebih dahulu!');
        }else{
            Ext.MessageBox.show({
                msg: 'Memuat Data ...',
                progressText: 'Memuat Data ...',
                width: 200,
                wait: true
            });

            Ext.Ajax.request({
                url     : 'delete/Mutasi',
                method  : 'POST',
                params  : {
                    mutasi_id: rec.data.mutasi_id
                },
                failure : function(response, options){
                    Ext.Msg.alert('Warning','Ada Kesalahan dalam pengolahan data. Mohon dicoba lagi');
                },
                success : function(response){
                    var json = Ext.JSON.decode(response.responseText);
                    
                    if(json.success){
                        Ext.MessageBox.hide();

                        Ext.Msg.alert('Berhasil', json.message);

                        grid.getStore().load();

                    } else if(json.success == false) {
                        Ext.MessageBox.hide();

                        Ext.Msg.alert('Error',json.message);
                    }
                }
            });      
        }
    },
    ubah:function(btn){
        var grid = btn.up('gridpanel');

        var rec = grid.getSelectionModel().getSelection()[0];

        if(!rec){
            Ext.Msg.alert('Pilih salah satu baris terlebih dahulu!');
        }else{

            var id = rec.data.mutasi_id;

            var tabPanel = Ext.getCmp('ContentPanel');
            var panelId = 'MutasiPesertaDidik_' + id;
            var tab = tabPanel.getComponent(panelId);

            if (!tab) {
                Ext.MessageBox.show({
                    msg: 'Memuat Data ...',
                    progressText: 'Memuat Data ...',
                    width: 200,
                    wait: true
                });

                var store = Ext.create('simdik_batam.store.Mutasi');

                store.load({
                    params:{
                        mutasi_id: id
                    }
                });
                
                store.on('load',function(store, records, options){
        
                    var rec = store.findRecord('mutasi_id', id);

                    if(rec){

                        var judul = 'Edit Mutasi PD: ' + rec.data.nama;

                        var classname = 'simdik_batam.view.Mutasi.MutasiPesertaDidik';

                        tabPanel.add(Ext.create(classname, {
                            title: judul,
                            closable:true,
                            glyph: 61447,
                            id: panelId,
                            record: rec
                        }));

                        tabPanel.setActiveTab(panelId);

                        Ext.MessageBox.hide();

                        win.hide();
                    }else{
                        Ext.Msg.alert('Error','Data Peserta Didik tidak ditemukan atau ada kesalahan dalam sistem. Mohon Coba sekali lagi');
                    }
                
                });
            }else{
                tabPanel.setActiveTab(tab);
            }
        }
    },
    tambah:function(btn){
        var store = Ext.create('simdik_batam.store.DataPokokPesertaDidik');

        var win = new Ext.window.Window({   
            height: 150,
            width: 500,
            modal:true,
            resizable: false,
            title: 'Tambah Mutasi Peserta Didik',
            autoScroll: true,
            frame: false,            
            border: true,
            layout: 'border',
            closable: true,
            items: [{
                xtype:'form',
                region:'center',
                frame: false,            
                border: false,
                width: 600,
                bodyPadding: '10 10 10 10',
                defaultType: 'textfield',
                defaults: {
                    anchor: '100%',
                    labelAlign: 'top'
                },
                items: [
                    {
                        allowBlank: false,
                        xtype:'combobox',
                        fieldLabel: 'Ketikkan Kata Kunci Nama atau NISN Peserta Didik',
                        name: 'keyword',
                        store:store,
                        hideTrigger:true,
                        queryMode: 'remote',
                        emptyText: 'Kata Kunci Nama / NISN',
                        displayField:'nama',
                        valueField:'peserta_didik_id',
                        listConfig: {
                            loadingText: 'Mencari...',
                            emptyText: 'Data Tidak Ditemukan',
                            // Custom rendering template for each item
                            getInnerTpl: function() {
                                return '<b>{nama}</b><br>'+
                                        '<i>{nisn} - {sekolah_id_str}</i>';
                            }
                        },
                        pageSize: 50
                    }
                ],
                buttons:[{
                    text:'Pilih',
                    handler:function(btn){
                        var id = btn.up('form').down('combobox').getValue();
                        console.log(id);

                        var tabPanel = Ext.getCmp('ContentPanel');
                        var panelId = 'MutasiPesertaDidik_' + id;
                        var tab = tabPanel.getComponent(panelId);
                        
                        if (!tab) {
                            Ext.MessageBox.show({
                                msg: 'Memuat Data ...',
                                progressText: 'Memuat Data ...',
                                width: 200,
                                wait: true
                            });

                            var store = Ext.create('simdik_batam.store.DataPokokPesertaDidik');

                            store.load({
                                params:{
                                    peserta_didik_id: id,
                                    modul: 'mutasi',
                                    keyword:null
                                }
                            });
                            
                            store.on('load',function(store, records, options){
                    
                                var rec = store.findRecord('peserta_didik_id', id);

                                if(rec){

                                    var judul = 'Tambah Mutasi PD: ' + rec.data.nama;

                                    var classname = 'simdik_batam.view.Mutasi.MutasiPesertaDidik';

                                    tabPanel.add(Ext.create(classname, {
                                        title: judul,
                                        closable:true,
                                        glyph: 61447,
                                        id: panelId,
                                        record: rec
                                    }));

                                    tabPanel.setActiveTab(panelId);

                                    Ext.MessageBox.hide();

                                    win.hide();
                                }else{
                                    Ext.Msg.alert('Error','Data Peserta Didik tidak ditemukan atau ada kesalahan dalam sistem. Mohon Coba sekali lagi');
                                }
                            
                            });
                        }else{
                            tabPanel.setActiveTab(tab);
                            win.hide();
                        }
                    }
                }]
            }]
        });

        win.show();

    },
    pilihPesertaDidik:function(btn){
        Ext.Msg.alert('tes');
    }
});