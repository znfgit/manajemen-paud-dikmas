Ext.define('simdik_batam.view.Mutasi.MutasiPesertaDidik', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.mutasipesertadidikpanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    // controller: 'main',
    initComponent:function(){

        var record = this.initialConfig.record ? this.initialConfig.record : false;
        if (record){
            this.listeners = {
                afterrender: function(form, options) {
                    // form.loadRecord(record);
                }
            };
        }

    	this.items = [{
            xtype:'mutasipesertadidikform',
            region:'center',
            record: record
        }];

    	this.callParent(arguments);
    }
});