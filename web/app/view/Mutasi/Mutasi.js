Ext.define('simdik_batam.view.Mutasi.Mutasi', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.mutasipanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

    	this.items = [{
    		xtype: 'mutasigrid',
            title: 'Mutasi Peserta Didik',
            region:'center'
    	}];

    	this.callParent(arguments);
    }
});