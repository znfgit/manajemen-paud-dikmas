Ext.define('simdik_batam.view.Mutasi.MutasiPesertaDidikForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.mutasipesertadidikform',
    bodyPadding:20,
    autoScroll:true,
    initComponent: function() {
        
        var record = this.initialConfig.record ? this.initialConfig.record : false;
        if (record){
            this.listeners = {
                afterrender: function(form, options) {
                    form.loadRecord(record);
                }
            };
        }

        if(record.data.mutasi_id){
            this.dockedItems = [{
                xtype:'toolbar',
                dock:'bottom',
                ui:'footer',
                items:['-',{
                    text:'Simpan',
                    handler:'simpanMutasiBaru',
                    scale: 'medium'
                },{
                    text:'Preview Cetak',
                    scale: 'medium',
                    handler:'cetakFromForm'
                },{
                    text:'Cetak PDF',
                    scale: 'medium',
                    handler:function(btn){
                        window.location.replace('cetak/Mutasi/'+ record.data.mutasi_id + '/pdf');
                    }
                }]
            }];
        }else{
            this.dockedItems = [{
                xtype:'toolbar',
                dock:'bottom',
                ui:'footer',
                items:['-',{
                    text:'Simpan',
                    handler:'simpanMutasiBaru',
                    scale: 'medium'
                }]
            }];
        }

        this.items = [{
            xtype: 'hidden'
            ,fieldLabel: 'ID'
            ,labelAlign: 'right'
            ,labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
            ,allowBlank: false
            ,maxValue: 99999999
            ,minValue: 0
            ,name: 'mutasi_id'
        },{
            xtype: 'hidden'
            ,fieldLabel: 'ID'
            ,labelAlign: 'right'
            ,labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
            ,allowBlank: false
            ,maxValue: 99999999
            ,minValue: 0
            ,name: 'peserta_didik_id'
        },{
            xtype: 'fieldset'
            ,title: 'Surat'
            ,collapsible: true
            ,labelAlign: 'right'
            ,defaults: {
                labelWidth: 175
                ,anchor: '100%'
                ,margins: '0 0 0 5',
                renderer:function(v){
                    return '<b>' + v + '</b>';
                }
            }
            ,items: [{
                xtype:'combojenissurat',
                name: 'jenis_surat_id',
                emptyText:'Pilih Jenis Surat...',
                displayField: 'nama',
                valueField: 'jenis_surat_id',
                fieldLabel: 'Jenis Surat',
                labelAlign: 'right',
                allowBlank: false,
                margins: '0 0 0 0',
                width: 200,
                listeners: {
                    'afterrender': function(combo) {
                        combo.getStore().load({
                            params:{
                                jenis_surat_id: record.get('jenis_surat_id')
                            }
                        });
                    }
                }
            },{
                xtype:'combopengguna',
                name: 'pengguna_id',
                emptyText:'Pilih Pejabat Pendandatangan...',
                displayField: 'nama',
                valueField: 'pengguna_id',
                fieldLabel: 'Pejabat Pendandatangan',
                labelAlign: 'right',
                allowBlank: false,
                margins: '0 0 0 0',
                width: 200,
                listeners: {
                    'afterrender': function(combo) {
                        combo.getStore().load({
                            params:{
                                pengguna_id: record.get('pengguna_id')
                            }
                        });
                    }
                }
            },{
                xtype: 'textfield'
                ,fieldLabel: 'ID'
                ,labelAlign: 'right'
                ,allowBlank: false
                ,fieldLabel: 'Nomor Surat'
                ,name: 'nomor_surat'
                ,anchor:'100%'
            },{
                xtype: 'datefield'
                ,fieldLabel: 'Tanggal Surat'
                ,name: 'tanggal_surat'
                ,labelAlign: 'right'
                ,margins: '0 0 0 0'
                ,anchor: '50%'
                ,allowBlank: false
            }]
        },{
            xtype: 'fieldset'
            ,title: 'I. Data Peserta Didik'
            ,collapsible: true
            ,labelAlign: 'right'
            ,defaults: {
                labelWidth: 175
                ,anchor: '100%'
                ,margins: '0 0 0 5',
                renderer:function(v){
                    return '<b>' + v + '</b>';
                }
            }
            ,items: [{
                xtype: 'textfield'
                ,allowBlank: false
                ,minValue: 0
                ,fieldLabel: 'Nama'
                ,maxLength: 80
                ,enforceMaxLength: true
                ,labelAlign: 'right'
                ,margins: '0 0 0 0'
                ,name: 'nama'
            },{
                xtype: 'textfield'
                ,allowBlank: false
                ,minValue: 0
                ,fieldLabel: 'Tempat Lahir'
                ,maxLength: 80
                ,enforceMaxLength: true
                ,labelAlign: 'right'
                ,margins: '0 0 0 0'
                ,name: 'tempat_lahir'
            },{
                xtype: 'datefield'
                ,fieldLabel: 'Tanggal Lahir'
                ,name: 'tanggal_lahir'
                ,labelAlign: 'right'
                ,margins: '0 0 0 0'
                ,anchor: '50%'
                ,allowBlank: false
            },/*{
                xtype: 'textfield'
                ,allowBlank: false
                ,minValue: 0
                ,fieldLabel: 'Asal Sekolah'
                ,maxLength: 80
                ,enforceMaxLength: true
                ,labelAlign: 'right'
                ,margins: '0 0 0 0'
                ,name: 'sekolah_id'
            },*/{
                xtype:'combosekolah',
                name: 'sekolah_id',
                emptyText:'Pilih Sekolah...',
                displayField: 'nama_sekolah',
                valueField: 'sekolah_id',
                fieldLabel: 'Asal Sekolah',
                labelAlign: 'right',
                margins: '0 0 0 0',
                allowBlank: false,
                width: 200,
                listeners: {
                    'afterrender': function(combo) {
                        combo.getStore().load({
                            params:{
                                sekolah_id: record.get('sekolah_id')
                            }
                        });
                    }
                }
            },{
                xtype: 'textfield'
                ,allowBlank: false
                ,minValue: 0
                ,fieldLabel: 'Kelas'
                ,maxLength: 80
                ,enforceMaxLength: true
                ,labelAlign: 'right'
                ,margins: '0 0 0 0'
                ,name: 'kelas'
            },{
                xtype: 'textfield'
                ,allowBlank: false
                ,minValue: 0
                ,fieldLabel: 'NIS'
                ,maxLength: 80
                ,enforceMaxLength: true
                ,labelAlign: 'right'
                ,margins: '0 0 0 0'
                ,name: 'nipd'
            },{
                xtype: 'textfield'
                ,allowBlank: false
                ,minValue: 0
                ,fieldLabel: 'NISN'
                ,maxLength: 80
                ,enforceMaxLength: true
                ,labelAlign: 'right'
                ,margins: '0 0 0 0'
                ,name: 'nisn'
            }]
        },{
            xtype: 'fieldset'
            ,title: 'II. Data Orang Tua'
            ,collapsible: true
            ,labelAlign: 'right'
            ,defaults: {
                labelWidth: 175
                ,anchor: '100%'
                ,margins: '0 0 0 5',
                renderer:function(v){
                    return '<b>' + v + '</b>';
                }
            }
            ,items: [{
                xtype: 'textfield'
                ,allowBlank: false
                ,minValue: 0
                ,fieldLabel: 'Nama Orang Tua / Wali'
                ,maxLength: 80
                ,enforceMaxLength: true
                ,labelAlign: 'right'
                ,margins: '0 0 0 0'
                ,name: 'nama_ayah'
            },{
                xtype: 'textfield'
                ,allowBlank: false
                ,minValue: 0
                ,fieldLabel: 'Pekerjaan'
                ,maxLength: 80
                ,enforceMaxLength: true
                ,labelAlign: 'right'
                ,margins: '0 0 0 0'
                ,name: 'pekerjaan_id_ayah_str'
            },{
                xtype: 'textfield'
                ,allowBlank: false
                ,minValue: 0
                ,fieldLabel: 'Alamat'
                ,maxLength: 80
                ,enforceMaxLength: true
                ,labelAlign: 'right'
                ,margins: '0 0 0 0'
                ,name: 'alamat_jalan'
            }]
        },{
            xtype: 'fieldset'
            ,title: 'III. Detail Mutasi'
            ,collapsible: true
            ,labelAlign: 'right'
            ,defaults: {
                labelWidth: 175
                ,anchor: '100%'
                ,margins: '0 0 0 5',
                renderer:function(v){
                    return '<b>' + v + '</b>';
                }
            }
            ,items: [/*{
                xtype: 'textfield'
                ,allowBlank: false
                ,minValue: 0
                ,fieldLabel: 'Akan Berangkat Ke'
                ,maxLength: 80
                ,enforceMaxLength: true
                ,labelAlign: 'right'
                ,margins: '0 0 0 0'
                ,name: 'kode_wilayah_berangkat_str'
            },*/{
                xtype:'combokecamatan',
                name: 'kode_wilayah_berangkat',
                emptyText:'Pilih Propinsi...',
                displayField: 'nama',
                valueField: 'kode_wilayah',
                fieldLabel: 'Akan Berangkat Ke',
                labelAlign: 'right',
                margins: '0 0 0 0',
                width: 200,
                allowBlank: false,
                listeners: {
                    'afterrender': function(combo) {
                        combo.getStore().load({
                            params:{
                                kode_wilayah: record.get('kode_wilayah_berangkat')
                            }
                        });
                    }
                }
            },{
                xtype: 'textfield'
                ,allowBlank: false
                ,minValue: 0
                ,fieldLabel: 'Maksud dan Tujuan'
                ,maxLength: 80
                ,enforceMaxLength: true
                ,labelAlign: 'right'
                ,margins: '0 0 0 0'
                ,name: 'maksud_tujuan'
            },{
                xtype: 'textfield'
                ,allowBlank: false
                ,minValue: 0
                ,fieldLabel: 'Nama yang Diikuti'
                ,maxLength: 80
                ,enforceMaxLength: true
                ,labelAlign: 'right'
                ,margins: '0 0 0 0'
                ,name: 'nama_yang_diikuti'
            },{
                xtype: 'textfield'
                ,allowBlank: false
                ,minValue: 0
                ,fieldLabel: 'Hubungan Keluarga'
                ,maxLength: 80
                ,enforceMaxLength: true
                ,labelAlign: 'right'
                ,margins: '0 0 0 0'
                ,name: 'hubungan_keluarga'
            },{
                xtype: 'textfield'
                ,allowBlank: false
                ,minValue: 0
                ,fieldLabel: 'Pekerjaan'
                ,maxLength: 80
                ,enforceMaxLength: true
                ,labelAlign: 'right'
                ,margins: '0 0 0 0'
                ,name: 'pekerjaan'
            },{
                xtype: 'textfield'
                ,allowBlank: false
                ,minValue: 0
                ,fieldLabel: 'Alamat / Tempat Tinggal'
                ,maxLength: 80
                ,enforceMaxLength: true
                ,labelAlign: 'right'
                ,margins: '0 0 0 0'
                ,name: 'alamat_yang_diikuti'
            },{
                xtype: 'datefield'
                ,fieldLabel: 'Berangkat pada Tanggal'
                ,name: 'tanggal_berangkat'
                ,labelAlign: 'right'
                ,margins: '0 0 0 0'
                ,anchor: '50%'
                ,allowBlank: false
            },{
                xtype: 'textfield'
                ,allowBlank: false
                ,minValue: 0
                ,fieldLabel: 'Keterangan Kepala Sekolah'
                ,maxLength: 80
                ,enforceMaxLength: true
                ,labelAlign: 'right'
                ,margins: '0 0 0 0'
                ,name: 'keterangan_kepala_sekolah'
            },{
                xtype: 'textfield'
                ,allowBlank: false
                ,minValue: 0
                ,fieldLabel: 'Keterangan Lain-lain'
                ,maxLength: 80
                ,enforceMaxLength: true
                ,labelAlign: 'right'
                ,margins: '0 0 0 0'
                ,name: 'keterangan_lain'
            }]
        }];

        this.callParent(arguments);
    }
});