Ext.define('simdik_batam.view.Mutasi.MutasiGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.mutasigrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    requires:[
        'simdik_batam.view.Mutasi.MutasiController'
    ],
    controller: 'mutasi',
    listeners:{
        afterrender:'setupMutasiGrid'
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('simdik_batam.store.Mutasi');
        
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        this.getSelectionModel().setSelectionMode('SINGLE');
        
        this.columns = [{
            header: 'Nomor Surat',
            width:150,
            sortable: true,
            dataIndex: 'nomor_surat',
            hideable: false,
            hidden: false
        },{
            header: 'Nama',
            width:150,
            sortable: true,
            dataIndex: 'nama',
            hideable: false,
            hidden: false
        },{
            header: 'Sekolah Asal',
            width:150,
            sortable: true,
            dataIndex: 'sekolah_id_str',
            hideable: false,
            hidden: false
        },{
            header: 'NISN',
            width:150,
            sortable: true,
            dataIndex: 'NISN',
            hideable: false,
            hidden: false
        },{
            header: 'NIS',
            width:150,
            sortable: true,
            dataIndex: 'NIS',
            hideable: false,
            hidden: false
        },{
            header: 'Nama Orang Tua',
            width:150,
            sortable: true,
            dataIndex: 'nama_ayah',
            hideable: false,
            hidden: false
        },{
            header: 'Berangkat',
            width:150,
            sortable: true,
            dataIndex: 'kode_wilayah_berangkat',
            hideable: false,
            hidden: false
        },{
            header: 'Maksud dan Tujuan',
            width:150,
            sortable: true,
            dataIndex: 'maksud_tujuan',
            hideable: false,
            hidden: false
        },{
            header: 'Alamat',
            width:150,
            sortable: true,
            dataIndex: 'alamat_jalan',
            hideable: false,
            hidden: false
        },{
            header: 'Tanggal Berangkat',
            width:150,
            sortable: true,
            dataIndex: 'tanggal_berangkat',
			hideable: false,
            hidden: false
        }];

        this.dockedItems = [{
            xtype: 'toolbar',
            items: [{
                text:'Tambah',
                handler:'tambah',
                glyph:61525
            },{
                text:'Ubah',
                handler:'ubah',
                glyph:61504
            },{
                text:'Hapus',
                handler:'hapus',
                glyph:61453
            },{
                text:'Cetak',
                handler:'cetak',
                glyph:61487
            }]
        }];
        
        this.bbar = Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
        
        this.callParent(arguments);
    }
});