Ext.define('simdik_batam.view.DataPokokPesertaDidik.AnggotaRombelGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.anggotarombelgrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    listeners : {
        afterrender: 'setupAnggotaRombelGrid'
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('simdik_batam.store.AnggotaRombel');
        
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        this.getSelectionModel().setSelectionMode('SINGLE');
        
        this.columns = [{
            header: 'ID',
            width: 60,
            sortable: true,
            dataIndex: 'anggota_rombel_id',
			hideable: false,
            hidden: true,
            field: {
                xtype: 'hidden'
            }
        },{
            header: 'Sekolah',
            width: 300,
            sortable: true,
            dataIndex: 'sekolah_id',
            hideable: false,
            renderer: function(v,p,r) {
                return r.data.sekolah_id_str;
            }
        },{
            header: 'Tahun Ajaran',
            width: 200,
            sortable: true,
            dataIndex: 'tahun_ajaran_id',
            hideable: false,
            renderer: function(v,p,r) {
                return r.data.tahun_ajaran_id_str;
            }
        },{
            header: 'Semester',
            width: 200,
            sortable: true,
            dataIndex: 'semester_id',
            hideable: false,
            renderer: function(v,p,r) {
                return r.data.semester_id_str;
            }
        },{
            header: 'Rombel',
            width: 132,
            sortable: true,
            dataIndex: 'rombongan_belajar_id',
			hideable: false,
            renderer: function(v,p,r) {
                return r.data.rombongan_belajar_id_str;
            }
        },{
            header: 'Peserta Didik',
            width: 170,
            sortable: true,
            dataIndex: 'peserta_didik_id',
			hideable: false,
            hidden: true
        },{
            header: 'Jenis Pendaftaran',
            width: 140,
            sortable: true,
            dataIndex: 'jenis_pendaftaran_id_str',
			hideable: false
        }];
        
        
        this.bbar = Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
        
        this.callParent(arguments);
    }
});