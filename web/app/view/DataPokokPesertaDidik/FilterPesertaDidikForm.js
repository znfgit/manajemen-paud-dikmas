Ext.define('simdik_batam.view.DataPokokPesertaDidik.FilterPesertaDidikForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.filterpesertadidikform',
    bodyPadding: 10,
    fieldDefaults: {
        anchor: '100%',
        labelWidth: 120
    },
    listeners : {
        afterrender: 'setupFilterPdForm'
    },
    initComponent: function() {
        var form = this;
        
        var record = this.initialConfig.record ? this.initialConfig.record : false;
        if (record){
            this.listeners = {
                afterrender: function(form, options) {
                    form.loadRecord(record);
                }
            };
        }

        /*this.on('beforeadd', function(form, field){
            if (!field.allowBlank)
              field.labelSeparator += '<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>';
        });*/
        var pilihBentukStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'nama'],
            data : [
                {"id":"5", "nama":"SD"},
                {"id":"6", "nama":"SMP"},
                {"id":"13", "nama":"SMA"},
                {"id":"15", "nama":"SMK"},
                {"id":"29", "nama":"SLB"},
                {"id":"14", "nama":"SMLB"},
                {"id":"999", "nama":"Semua"}
            ]
        });

        var pilihStatusStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'nama'],
            data : [
                {"id":"1", "nama":"Negeri"},
                {"id":"2", "nama":"Swasta"},
                {"id":"999", "nama":"Semua"}
            ]
        });
        
        this.items = [{
            xtype: 'textfield'
            ,fieldLabel: 'Kata Kunci'
            ,labelAlign: 'top'
            ,allowBlank: true
            ,maxValue: 99999999
            ,minValue: 0
            ,width: 200
            ,maxLength: 100
            ,enforceMaxLength: false
            ,name: 'keyword'
            ,emptyText: 'Ketik Nama / NISN...'
        }
        ,{
            xtype: 'mstwilayahcombo',
            labelAlign: 'top',
            anchor: '100%',
            fieldLabel: 'Propinsi',
            name: 'combopropmasterdatapd',
            emptyText: 'Pilih Propinsi',
            listeners: {
                'change': function(field, newValue, oldValue) {
                    var value = newValue;

                    var combokab = form.down('mstwilayahcombo[name=combokabmasterdatapd]');
                    
                    combokab.enable();
                    
                    combokab.setValue('');

                    combokab.getStore().load();  
                }
            }
        },{
            xtype: 'mstwilayahcombo',
            labelAlign: 'top',
            anchor: '100%',
            fieldLabel: 'Kabupaten',
            disabled:true,
            name: 'combokabmasterdatapd',
            emptyText: 'Pilih Kabupaten / Kota',
            listeners: {
                'change': function(field, newValue, oldValue) {
                    var value = newValue;

                    var combokec = form.down('mstwilayahcombo[name=combokecmasterdatapd]');
                    
                    combokec.enable();
                    
                    combokec.setValue('');

                    combokec.getStore().load();  
                }
            }
        },{
            xtype: 'mstwilayahcombo',
            labelAlign: 'top',
            disabled: true,
            fieldLabel: 'Kecamatan',
            name: 'combokecmasterdatapd',
            emptyText: 'Pilih Kecamatan',
            anchor: '100%'
        },{
            xtype:'bentukpendidikancombo',
            name: 'pilihbentukcombo',
            emptyText:'Pilih Bentuk Pendidikan...',
            itemId: 'pilihbentukcombo',
            labelAlign: 'top',
            fieldLabel: 'Bentuk Pendidikan',
            displayField: 'nama',
            anchor: '100%'
        },{
            xtype:'combobox',
            name: 'pilihstatuscombo',
            emptyText:'Pilih Status Sekolah...',
            itemId: 'pilihstatuscombo',
            store: pilihStatusStore,
            labelAlign: 'top',
            fieldLabel: 'Status Sekolah',
            queryMode: 'local',
            displayField: 'nama',
            valueField: 'id',
            anchor: '100%'
        }/*,{
            xtype: 'mstwilayahcombo',
            labelAlign: 'top',
            fieldLabel: 'Kecamatan',
            name: 'combokecmasterdatasekolah',
            emptyText: 'Pilih Kecamatan'
        }*/,{
            xtype: 'button',
            text: 'Cari',
            glyph: 61452,
            handler: 'cari',
            anchor: '100%',
            scale: 'medium',
            margin: '10 0 0 0'
        }];

        // this.buttons = [];

        this.callParent(arguments);
    }
});