Ext.define('simdik_batam.view.DataPokokPesertaDidik.DataPokokPesertaDidik', {
	extend: 'Ext.panel.Panel',
    // requires:[
    //     'simdik_batam.view.DataPokokSekolah.DataPokokSekolahController'
    // ],
	alias: 'widget.datapokokpesertadidikpanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    // controller: 'main',
    initComponent:function(){

    	this.items = [{
            xtype:'filterpesertadidikform',
            region:'west',
            title:'Filter',
            collapsible:true,
            width:220,
            split:true,
            border:true
        },{
    		xtype: 'datapokokpesertadidikgrid',
            region:'center',
            title: 'Data Pokok Peserta Didik'
    	}];

    	this.callParent(arguments);
    }
});