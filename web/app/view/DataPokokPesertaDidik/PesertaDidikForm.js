Ext.define('simdik_batam.view.DataPokokPesertaDidik.PesertaDidikForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.pesertadidikform',
    bodyPadding:20,
    autoScroll:true,
    bodyStyle : 'background:#F3FFDE',
    defaults: {
        border: false,
        xtype: 'panel',
        flex: 1,
        layout: 'anchor',
        bodyStyle : 'background:#F3FFDE',
    }, 
    layout: {
        type:'hbox'
    },
    initComponent: function() {
        
        var record = this.initialConfig.record ? this.initialConfig.record : false;
        if (record){
            this.listeners = {
                afterrender: function(form, options) {
                    form.loadRecord(record);
                }
            };
        }

        /*this.on('beforeadd', function(form, field){
            if (!field.allowBlank)
              field.labelSeparator += '<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>';
        });*/
        this.items = [{
            defaults: {
                anchor: '-5',
            },
            items:[{
                xtype: 'hidden'
                ,fieldLabel: 'ID'
                ,labelAlign: 'right'
                ,maxValue: 99999999
                ,minValue: 0
                ,name: 'peserta_didik_id'
            },{
                xtype: 'fieldset'
                ,title: 'Data Pribadi'
                ,collapsible: true
                ,labelAlign: 'right'
                ,defaults: {
                    labelWidth: 185
                    ,anchor: '100%'
                    ,margins: '0 0 0 5'
                    ,renderer: function(v){
                        return "<b>" + v + "</b>";
                    }
                }
                ,items: [{
                    xtype: 'displayfield'
                    ,fieldLabel: 'Nama'
                    ,labelAlign: 'right'
                    ,margins: '0 0 0 0'
                    ,name: 'nama'
                },{
                    xtype: 'displayfield'
                    ,fieldLabel: 'Jenis Kelamin'
                    ,labelAlign: 'right'
                    ,margins: '0 0 0 0'
                    ,name: 'jenis_kelamin'
                    ,renderer:function(v){
                        switch(v){
                            case 'L': return '<b>Laki-laki</b>'; break;
                            case 'P': return '<b>Perempuan</b>'; break;
                        }
                    }
                },{
                    xtype: 'displayfield'
                    ,fieldLabel: 'NISN'
                    ,labelAlign: 'right'
                    ,margins: '0 0 0 0'
                    ,name: 'nisn'
                },{
                    xtype: 'displayfield'
                    ,fieldLabel: 'NIK'
                    ,labelAlign: 'right'
                    ,margins: '0 0 0 0'
                    ,name: 'nik'
                },{
                    xtype: 'displayfield'
                    ,fieldLabel: 'Tempat Lahir'
                    ,labelAlign: 'right'
                    ,margins: '0 0 0 0'
                    ,name: 'tempat_lahir'
                },{
                    xtype: 'displayfield'
                    ,fieldLabel: 'Tanggal Lahir'
                    ,labelAlign: 'right'
                    ,margins: '0 0 0 0'
                    ,name: 'tanggal_lahir'
                    ,renderer : Ext.util.Format.dateRenderer('d/m/Y')
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Agama'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'agama_id_str'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Kebutuhan Khusus'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'kebutuhan_khusus_id_str'
                },{
                    xtype: 'displayfield'
                    ,allowBlank: false
                    ,minValue: 0
                    ,fieldLabel: 'Alamat Jalan'
                    ,maxLength: 80
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,margins: '0 0 0 0'
                    ,name: 'alamat_jalan'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'RT'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'rt'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'RW'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'rw'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Nama Dusun'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'nama_dusun'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Desa/Kelurahan'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'desa_kelurahan'
                },
                //{
                //     xtype: 'displayfield'
                //     ,minValue: 0
                //     ,fieldLabel: 'Kecamatan'
                //     ,maxLength: 8
                //     ,enforceMaxLength: true
                //     ,labelAlign: 'right'
                //     ,name: 'kecamatan'
                // },
                {
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Kabupaten/Kota'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'kabupaten'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Propinsi'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'propinsi'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Kode Pos'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'kode_pos'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Tempat tinggal'
                    ,maxValue: 6
                    ,labelAlign: 'right'
                    ,name: 'jenis_tinggal_id_str'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Moda transportasi'
                    ,maxValue: 13
                    ,labelAlign: 'right'
                    ,name: 'alat_transportasi_id_str'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Nomor telepon'
                    ,maxLength: 20
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'nomor_telepon_rumah'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Nomor HP'
                    ,maxLength: 20
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'nomor_telepon_seluler'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Email'
                    ,vtype: 'email'
                    ,maxLength: 50
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'email'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Penerima KPS'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'penerima_kps'
                    ,renderer:function(v){
                        if(v == 1){
                            return '<b>Ya</b>';
                        }else if(v == 0){
                            return '<b>Tidak</b>';
                        }
                    }
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'No KPS (apabila menerima)'
                    ,maxLength: 40
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'no_kps'
                
                }]
                
            }]
        },{
            defaults: {
                anchor: '-5',
            },
            items:[{
                xtype: 'fieldset'
                ,title: 'Data Ayah Kandung'
                ,collapsible: true
                ,labelAlign: 'right'
                ,defaults: {
                    labelWidth: 185
                    ,anchor: '100%'
                    ,margins: '0 0 0 5'
                    ,renderer: function(v){
                        return "<b>" + v + "</b>";
                    }
                }
                ,items: [{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Nama'
                    ,maxLength: 50
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,margins: '0 0 0 0'
                    ,name: 'nama_ayah'
                },{
                    xtype: 'displayfield'
                    ,fieldLabel: 'Tahun lahir'
                    ,hideTrigger:true
                    ,minLength: 4
                    ,maxLength: 4
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'tahun_lahir_ayah'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Pendidikan'
                    ,maxValue: 21
                    ,labelAlign: 'right'
                    ,name: 'jenjang_pendidikan_ayah_str'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Pekerjaan'
                    ,maxValue: 15
                    ,labelAlign: 'right'
                    ,name: 'pekerjaan_id_ayah_str'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Penghasilan'
                    ,maxValue: 11
                    ,columns: 1
                    ,labelAlign: 'right'
                    ,name: 'penghasilan_id_ayah_str'
                },{
                    xtype: 'displayfield'
                    ,allowBlank: false
                    ,minValue: 0
                    ,fieldLabel: 'Berkebutuhan khusus'
                    ,maxValue: 36865
                    ,labelAlign: 'right'
                    ,name: 'kebutuhan_khusus_id_ayah_str'
                
                }]
                
            },{
                xtype: 'fieldset'
                ,title: 'Data Ibu Kandung'
                ,collapsible: true
                ,labelAlign: 'right'
                ,defaults: {
                    labelWidth: 185
                    ,anchor: '100%'
                    ,margins: '0 0 0 5'
                    ,renderer: function(v){
                        return "<b>" + v + "</b>";
                    }
                }
                ,items: [{
                    xtype: 'displayfield'
                    ,allowBlank: false
                    ,minValue: 0
                    ,fieldLabel: 'Nama'
                    ,maxLength: 50
                    ,enforceMaxLength: true
                    ,labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
                    ,labelAlign: 'right'
                    ,margins: '0 0 0 0'
                    ,name: 'nama_ibu_kandung'
                },{
                    xtype: 'displayfield'
                    ,fieldLabel: 'Tahun lahir'
                    ,hideTrigger:true
                    ,minLength: 4
                    ,maxLength: 4
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'tahun_lahir_ibu'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Pendidikan'
                    ,maxValue: 21
                    ,labelAlign: 'right'
                    ,name: 'jenjang_pendidikan_ibu_str'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Pekerjaan'
                    ,maxValue: 15
                    ,labelAlign: 'right'
                    ,name: 'pekerjaan_id_ibu_str'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Penghasilan'
                    ,maxValue: 11
                    ,columns: 1
                    ,labelAlign: 'right'
                    ,name: 'penghasilan_id_ibu_str'
                },{
                    xtype: 'displayfield'
                    ,allowBlank: false
                    ,minValue: 0
                    ,fieldLabel: 'Berkebutuhan khusus'
                    ,maxValue: 36865
                    ,labelAlign: 'right'
                    ,name: 'kebutuhan_khusus_id_ibu_str'
                
                }]
                
            },{
                xtype: 'fieldset'
                ,title: 'Data Wali'
                ,collapsible: true
                ,labelAlign: 'right'
                ,defaults: {
                    labelWidth: 185
                    ,anchor: '100%'
                    ,margins: '0 0 0 5'
                    ,renderer: function(v){
                        return "<b>" + v + "</b>";
                    }
                }
                ,items: [{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Nama'
                    ,maxLength: 30
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,margins: '0 0 0 0'
                    ,name: 'nama_wali'
                },{
                    xtype: 'displayfield'
                    ,fieldLabel: 'Tahun lahir'
                    ,hideTrigger:true
                    ,minLength: 4
                    ,maxLength: 4
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'tahun_lahir_wali'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Pendidikan'
                    ,maxValue: 21
                    ,labelAlign: 'right'
                    ,name: 'jenjang_pendidikan_wali_str'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Pekerjaan'
                    ,maxValue: 15
                    ,labelAlign: 'right'
                    ,name: 'pekerjaan_id_wali_str'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Penghasilan'
                    ,maxValue: 11
                    ,columns: 1
                    ,labelAlign: 'right'
                    ,name: 'penghasilan_id_wali_str'            
                }]
            }]
        }];

        this.callParent(arguments);
    }
});