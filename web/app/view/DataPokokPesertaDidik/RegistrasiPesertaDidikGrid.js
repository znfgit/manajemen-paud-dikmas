Ext.define('simdik_batam.view.DataPokokPesertaDidik.RegistrasiPesertaDidikGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.registrasipesertadidikgrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    initComponent: function() {
        
        var grid = this;
        
        // this.store = Ext.create('simdik_batam.store.RegistrasiPesertaDidik');
        
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        this.getSelectionModel().setSelectionMode('SINGLE');
        
        this.columns = [{
            header: 'ID',
            width: 60,
            sortable: true,
            dataIndex: 'registrasi_id',
			hideable: false,
            hidden: true,
            field: {
                xtype: 'hidden'
            }
        },{
            header: 'Jurusan Sp',
            width: 140,
            sortable: true,
            dataIndex: 'jurusan_sp_id',
			hideable: false,
            renderer: function(v,p,r) {
                return r.data.jurusan_sp_id_str;
            }
        },{
            header: 'Sekolah',
            width: 200,
            sortable: true,
            dataIndex: 'sekolah_id',
			hideable: false,
            renderer: function(v,p,r) {
                return r.data.sekolah_id_str;
            }
        },{
            header: 'Jenis Pendaftaran',
            width: 140,
            sortable: true,
            dataIndex: 'jenis_pendaftaran_id_str',
			hideable: false
        },{
            header: 'Tgl Masuk Sekolah',
            width: 100,
            sortable: true,
            dataIndex: 'tanggal_masuk_sekolah',
			hideable: false,
            renderer : Ext.util.Format.dateRenderer('d/m/Y')
        },{
            header: 'No Skhun',
            width: 120,
            sortable: true,
            dataIndex: 'no_skhun',
			hideable: false
        }];
        
        this.bbar = Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
        
        this.callParent(arguments);
    }
});