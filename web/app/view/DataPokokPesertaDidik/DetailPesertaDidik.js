Ext.define('simdik_batam.view.DataPokokPesertaDidik.DetailPesertaDidik', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.detailpesertadidikpanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    // controller: 'main',
    initComponent:function(){

        var record = this.initialConfig.record ? this.initialConfig.record : false;
        if (record){
            this.listeners = {
                afterrender: function(form, options) {
                    // form.loadRecord(record);
                }
            };
        }

    	this.items = [{
            xtype:'tabpanel',
            region:'center',
            // title: 'Detail ' + record.data.nama_sekolah,
            items:[{
                title: 'Profil Peserta Didik',
                layout:'fit',
                items:[{
                    xtype: 'pesertadidikform',
                    record:record,
                    autoScroll: true
                }]
            },{
                title:'Riwayat Pendidikan',
                xtype: 'anggotarombelgrid'
            }]
        }];

    	this.callParent(arguments);
    }
});