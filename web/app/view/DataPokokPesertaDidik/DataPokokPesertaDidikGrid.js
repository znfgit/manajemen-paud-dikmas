Ext.define('simdik_batam.view.DataPokokPesertaDidik.DataPokokPesertaDidikGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.datapokokpesertadidikgrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    listeners : {
        afterrender: 'setupDataPokokPesertaDidikGrid'
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('simdik_batam.store.DataPokokPesertaDidik');
        
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        this.getSelectionModel().setSelectionMode('SINGLE');
        
        this.columns = [{
            header: 'ID',
            width: 60,
            sortable: true,
            dataIndex: 'peserta_didik_id',
			hideable: false,
            hidden: true,
            field: {
                xtype: 'hidden'
            }
        },{
            header: 'Nama',
            width: 210,
            sortable: true,
            dataIndex: 'nama',
			hideable: false
        },{
			
            header: 'JK',
            width: 63,
            sortable: true,
            dataIndex: 'jenis_kelamin',
			hideable: false,
			renderer: function(v,p,r) {
				switch (v) {
					case 'L' : return 'L'; break;
					case 'P' : return 'P'; break;
					default : return '-'; break;
				}
            }
        },{
            header: 'NISN',
            width: 90,
            sortable: true,
            dataIndex: 'nisn',
			hideable: false
        },{
            header: 'NIK',
            width: 108,
            sortable: true,
            dataIndex: 'nik',
			hideable: false
        },{
            header: 'Tempat Lahir',
            width: 120,
            sortable: true,
            dataIndex: 'tempat_lahir',
			hideable: false
        },{
            header: 'Tgl lahir',
            width: 100,
            sortable: true,
            dataIndex: 'tanggal_lahir',
			hideable: false,
            renderer : Ext.util.Format.dateRenderer('d/m/Y')
        },{
            header: 'Agama',
            width: 145,
            sortable: true,
            dataIndex: 'agama_id_str',
			hideable: false
        },{
            header: 'Sekolah',
            width: 200,
            sortable: true,
            dataIndex: 'sekolah_id',
			hideable: false,
            renderer: function(v,p,r) {
                return r.data.sekolah_id_str;
            }
        },{
            header: 'Alamat Jalan',
            width: 300,
            sortable: true,
            dataIndex: 'alamat_jalan',
			hideable: false
        },{
            header: 'Nama Ibu Kandung',
            width: 210,
            sortable: true,
            dataIndex: 'nama_ibu_kandung',
			hideable: false,
            field: {
                xtype: 'textfield'
				,maxLength: 50
				,enforceMaxLength: true
				,minValue: 0
            }
        }];
        
        this.dockedItems = [{
            xtype: 'toolbar',
            items: ['-']
        }];
        
        this.bbar = Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
        
        this.callParent(arguments);
    }
});