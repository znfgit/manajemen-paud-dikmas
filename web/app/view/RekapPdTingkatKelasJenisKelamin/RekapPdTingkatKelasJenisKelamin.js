Ext.define('simdik_batam.view.RekapPdTingkatKelasJenisKelamin.RekapPdTingkatKelasJenisKelamin', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.rekappdtingkatkelasjeniskelamin',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

        this.title = 'Rekap PD Tingkat Kelas dan Jenis kelamin';

    	this.items = [{
    		xtype:'pdtingkatkelasjeniskelamingrid',
            region:'center'
    	}]

    	this.callParent(arguments);
    }
});