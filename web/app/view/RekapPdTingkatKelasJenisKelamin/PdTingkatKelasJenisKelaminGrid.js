Ext.define('simdik_batam.view.RekapPdTingkatKelasJenisKelamin.PdTingkatKelasJenisKelaminGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.pdtingkatkelasjeniskelamingrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    requires:[
        'Ext.grid.feature.Grouping'
    ],
    listeners:{
        afterrender:'setupPdTingkatKelasJenisKelaminGrid'
    },
    // features: [{
    //     ftype: 'grouping',
    //     groupHeaderTpl: '{columnName}: {name} ({rows.length} Sekolah)',
    //     hideGroupedHeader: false,
    //     startCollapsed: true,
    //     id: 'sekolahGrouping'
    // }],
    formatMoney: function(a, c, d, t){
        var n = a, 
            c = isNaN(c = Math.abs(c)) ? 0 : c, 
            d = d == undefined ? "," : d, 
            t = t == undefined ? "." : t, 
            s = n < 0 ? "-" : "", 
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('simdik_batam.store.PdTingkatKelasJenisKelamin');
        
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        this.getSelectionModel().setSelectionMode('SINGLE');

        // this.groupingFeature = grid.view.getFeature('sekolahGrouping');
        
        this.columns = [{
            header: 'Wilayah',
            width:200,
            sortable: true,
            dataIndex: 'desk',
			hideable: false,
            hidden: false,
            locked:true,
            renderer: function(v,p,r){
                return '<a href="#menu/RekapPdTingkatKelasJenisKelaminSp/'+ r.data.id_level_wilayah +'/'+ r.data.kode_wilayah +'">' + v + '</a>';
            }
        },{
            text: 'Kelas 10',
            itemId:'kelas_10_group', 
            columns: [{
                header: 'Laki-laki',
                width: 100,
                sortable: true,
                dataIndex: 'pd_kelas_10_laki',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return grid.formatMoney(Math.abs(v),0);
                }
            },{
                header: 'Perempuan',
                width: 100,
                sortable: true,
                dataIndex: 'pd_kelas_10_perempuan',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return grid.formatMoney(Math.abs(v),0);
                }
            },{
                header: 'Total',
                width: 100,
                sortable: true,
                dataIndex: 'pd_kelas_10',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return grid.formatMoney(Math.abs(v),0);
                }
            }]
        },{
            text: 'Kelas 11',
            itemId:'kelas_11_group', 
            columns: [{
                header: 'Laki-laki',
                width: 100,
                sortable: true,
                dataIndex: 'pd_kelas_11_laki',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return grid.formatMoney(Math.abs(v),0);
                }
            },{
                header: 'Perempuan',
                width: 100,
                sortable: true,
                dataIndex: 'pd_kelas_11_perempuan',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return grid.formatMoney(Math.abs(v),0);
                }
            },{
                header: 'Total',
                width: 100,
                sortable: true,
                dataIndex: 'pd_kelas_11',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return grid.formatMoney(Math.abs(v),0);
                }
            }]
        },{
            text: 'Kelas 12',
            itemId:'kelas_12_group', 
            columns: [{
                header: 'Laki-laki',
                width: 100,
                sortable: true,
                dataIndex: 'pd_kelas_12_laki',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return grid.formatMoney(Math.abs(v),0);
                }
            },{
                header: 'Perempuan',
                width: 100,
                sortable: true,
                dataIndex: 'pd_kelas_12_perempuan',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return grid.formatMoney(Math.abs(v),0);
                }
            },{
                header: 'Total',
                width: 100,
                sortable: true,
                dataIndex: 'pd_kelas_12',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return grid.formatMoney(Math.abs(v),0);
                }
            }]
        },{
            text: 'Kelas 13',
            itemId:'kelas_13_group', 
            columns: [{
                header: 'Laki-laki',
                width: 100,
                sortable: true,
                dataIndex: 'pd_kelas_13_laki',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return grid.formatMoney(Math.abs(v),0);
                }
            },{
                header: 'Perempuan',
                width: 100,
                sortable: true,
                dataIndex: 'pd_kelas_13_perempuan',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return grid.formatMoney(Math.abs(v),0);
                }
            },{
                header: 'Total',
                width: 100,
                sortable: true,
                dataIndex: 'pd_kelas_13',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return grid.formatMoney(Math.abs(v),0);
                }
            }]
        }];

        this.dockedItems = [{
            xtype: 'toolbar',
            items: ['-',{
                xtype:'bentukpendidikancombo',
                name: 'pilihbentukcombo',
                emptyText:'Pilih Bentuk Pendidikan...',
                displayField: 'nama',
                width: 200,
                listeners: {
                    'change': function(field, newValue, oldValue) {
                        
                        grid.getStore().reload();
                    }
                }
            }
            // ,{
            //     xtype:'button',
            //     text:'Export Xls',
            //     glyph: 61474,
            //     urls: 'excel/SebaranPdPerUmur',
            //     handler:function(btn){
            //         var bentuk_pendidikan_id = btn.up('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            //         var semester_id = btn.up('toolbar').down('semestercombo').getValue();

            //         window.location = 'excel/SebaranPdPerUmur?bentuk_pendidikan_id='+bentuk_pendidikan_id+'&semester_id='+semester_id;
            //     }
            // }
            ]
        }];
        
        this.bbar = ['-'];
        // this.bbar = Ext.create('Ext.PagingToolbar', {
        //     store: this.store,
        //     displayInfo: true,
        //     displayMsg: 'Displaying data {0} - {1} of {2}',
        //     emptyMsg: "Tidak ada data"
        // });
        
        this.callParent(arguments);
    }
});