// AnalisisKebutuhanGuruDanKelas
Ext.define('simdik_batam.view.AnalisisKebutuhanPrasaranaSMA.AnalisisKebutuhanPrasaranaSMA', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.analisiskebutuhanprasaranasmapanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

    	this.items = [{
    		xtype: 'analisiskebutuhanprasaranasmagrid',
            title: 'Analisis Kebutuhan Prasarana SMA',
            region:'center'
    	}];

    	this.callParent(arguments);
    }
});