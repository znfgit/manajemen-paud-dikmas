Ext.define('simdik_batam.view.AnalisisKebutuhanPrasaranaSMA.AnalisisKebutuhanPrasaranaSMAController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.MessageBox'
    ],

    alias: 'controller.analisiskebutuhanprasaranasma',

    setupAnalisisKebutuhanPrasaranaSMAGrid:function(grid){

    	grid.getStore().load();
    }
});