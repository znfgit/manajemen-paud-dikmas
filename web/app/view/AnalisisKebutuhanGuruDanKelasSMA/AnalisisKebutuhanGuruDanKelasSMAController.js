Ext.define('simdik_batam.view.AnalisisKebutuhanGuruDanKelasSMA.AnalisisKebutuhanGuruDanKelasSMAController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.MessageBox'
    ],

    alias: 'controller.analisiskebutuhangurudankelassma',

    setupAnalisisKebutuhanGuruDanKelasSMAGrid:function(grid){

    	grid.getStore().load();
    }
});