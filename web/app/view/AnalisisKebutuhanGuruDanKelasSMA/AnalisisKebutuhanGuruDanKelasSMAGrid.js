Ext.define('simdik_batam.view.AnalisisKebutuhanGuruDanKelasSMA.AnalisisKebutuhanGuruDanKelasSMAGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.analisiskebutuhangurudankelassmagrid',
    selType: 'rowmodel',
    autoScroll: true,
    requires:[
        'simdik_batam.view.AnalisisKebutuhanGuruDanKelasSMA.AnalisisKebutuhanGuruDanKelasSMAController'
    ],
    controller: 'analisiskebutuhangurudankelassma',
    listeners:{
        afterrender:'setupAnalisisKebutuhanGuruDanKelasSMAGrid'
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('simdik_batam.store.AnalisisKebutuhanGuruDanKelasSMA');
        
        this.getSelectionModel().setSelectionMode('SINGLE');

        this.columns = [{
            header: 'ID',
            width: 60,
            sortable: true,
            dataIndex: 'kode_wilayah',
            hideable: false,
            hidden: true,
            field: {
                xtype: 'hidden'
            }
        },{
            header: 'Wilayah',
            width: 200,
            sortable: true,
            dataIndex: 'nama',
            locked   : true,
            hideable: false
        },{
                text: 'Sekolah',
                columns: [{
                    header: 'Negeri',
                    width: 80,
                    sortable: true,
                    dataIndex: 'negeri',
                    align:'right',
                    hideable: false
                },{
                    header: 'Swasta',
                    width: 100,
                    sortable: true,
                    dataIndex: 'swasta',
                    align:'right',
                    hideable: false
                },{
                    header: 'Total',
                    width: 80,
                    sortable: true,
                    dataIndex: 'total',
                    align:'right',
                    hideable: false,
                    renderer: function(value, metaData, record, row, col, store, gridView){
                        return record.data.negeri + record.data.swasta;
                    }
                }]
        },{
            text: 'Jumlah',
            columns: [{
                text:'Siswa',
                columns:[{
                    header: 'Negeri',
                    width: 80,
                    sortable: true,
                    dataIndex: 'pd_negeri',
                    align:'right',
                    hideable: false
                },{
                    header: 'Swasta',
                    width: 100,
                    sortable: true,
                    dataIndex: 'pd_swasta',
                    align:'right',
                    hideable: false
                },{
                    header: 'Total',
                    width: 80,
                    sortable: true,
                    dataIndex: 'pd_total',
                    align:'right',
                    hideable: false,
                    renderer: function(value, metaData, record, row, col, store, gridView){
                        return record.data.pd_negeri + record.data.pd_swasta;
                    }
                }]
            },{
                text:'Rombel',
                columns:[{
                    header: 'Negeri',
                    width: 80,
                    sortable: true,
                    dataIndex: 'rb_negeri',
                    align:'right',
                    hideable: false
                },{
                    header: 'Swasta',
                    width: 100,
                    sortable: true,
                    dataIndex: 'rb_swasta',
                    align:'right',
                    hideable: false
                },{
                    header: 'Total',
                    width: 80,
                    sortable: true,
                    dataIndex: 'rb_total',
                    align:'right',
                    hideable: false,
                    renderer: function(value, metaData, record, row, col, store, gridView){
                        return record.data.rb_negeri + record.data.rb_swasta;
                    }
                }]
            },{
                text:'Kelas',
                columns:[{
                    header: 'Negeri',
                    width: 80,
                    sortable: true,
                    dataIndex: 'pr_negeri',
                    align:'right',
                    hideable: false
                },{
                    header: 'Swasta',
                    width: 100,
                    sortable: true,
                    dataIndex: 'pr_swasta',
                    align:'right',
                    hideable: false
                },{
                    header: 'Total',
                    width: 80,
                    sortable: true,
                    dataIndex: 'pr_total',
                    align:'right',
                    hideable: false,
                    renderer: function(value, metaData, record, row, col, store, gridView){
                        return record.data.pr_negeri + record.data.pr_swasta;
                    }
                }]
            }]
        },{
            text: 'Kebutuhan',
            columns: [{
                text:'Guru',
                columns:[{
                    header: 'P',
                    width: 80,
                    sortable: true,
                    dataIndex: 'guru_p',
                    align:'right',
                    hideable: false
                },{
                    header: 'A',
                    width: 100,
                    sortable: true,
                    dataIndex: 'guru_a',
                    align:'right',
                    hideable: false
                },{
                    header: 'K',
                    width: 80,
                    sortable: true,
                    dataIndex: 'guru_k',
                    align:'right',
                    hideable: false,
                    renderer: function(value, metaData, record, row, col, store, gridView){
                        if(value < 0){
                            return 0;
                        }else{
                            return value;
                        }
                    }
                },{
                    header: 'L',
                    width: 80,
                    sortable: true,
                    dataIndex: 'guru_l',
                    align:'right',
                    hideable: false,
                    renderer: function(value, metaData, record, row, col, store, gridView){
                        if(value < 0){
                            return 0;
                        }else{
                            return value;
                        }
                    }
                }]
            },{
                text:'Kelas',
                columns:[{
                    header: 'P',
                    width: 80,
                    sortable: true,
                    dataIndex: 'kelas_p',
                    align:'right',
                    hideable: false
                },{
                    header: 'A',
                    width: 100,
                    sortable: true,
                    dataIndex: 'kelas_a',
                    align:'right',
                    hideable: false
                },{
                    header: 'K',
                    width: 80,
                    sortable: true,
                    dataIndex: 'kelas_k',
                    align:'right',
                    hideable: false,
                    renderer: function(value, metaData, record, row, col, store, gridView){
                        if(value < 0){
                            return 0;
                        }else{
                            return value;
                        }
                    }
                },{
                    header: 'L',
                    width: 80,
                    sortable: true,
                    dataIndex: 'kelas_l',
                    align:'right',
                    hideable: false,
                    renderer: function(value, metaData, record, row, col, store, gridView){
                        if(value < 0){
                            return 0;
                        }else{
                            return value;
                        }
                    }
                }]
            }]
        }];

        this.dockedItems = [{
            xtype:'toolbar',
            dock:'top',
            items:['-',{
                // text:'Muat Ulang',
                glyph: 61470,
                handler: function(btn){
                    grid.store.reload();
                }
            }]
        }];
        
        
        this.callParent(arguments);
    }
});