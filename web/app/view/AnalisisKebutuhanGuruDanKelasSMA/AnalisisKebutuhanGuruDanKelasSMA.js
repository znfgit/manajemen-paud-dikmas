// AnalisisKebutuhanGuruDanKelas
Ext.define('simdik_batam.view.AnalisisKebutuhanGuruDanKelasSMA.AnalisisKebutuhanGuruDanKelasSMA', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.analisiskebutuhangurudankelassmapanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

    	this.items = [{
    		xtype: 'analisiskebutuhangurudankelassmagrid',
            title: 'Analisis Kebutuhan Guru dan Kelas SMA',
            region:'center'
    	}];
        
    	this.callParent(arguments);
    }
});