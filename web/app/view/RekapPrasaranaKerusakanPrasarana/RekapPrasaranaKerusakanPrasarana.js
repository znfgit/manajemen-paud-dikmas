Ext.define('simdik_batam.view.RekapPrasaranaKerusakanPrasarana.RekapPrasaranaKerusakanPrasarana', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.prasaranakerusakanprasaranapanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

        this.items = [{
           xtype:'panel',
            region:'center',
            split:true,
            layout:'border',
            items:[{
                xtype:'panel',
                region:'center',
                title: 'Jumlah Prasarana Berdasarakan Kerusakan'
            }]
        }];

        this.callParent(arguments);
    }
});