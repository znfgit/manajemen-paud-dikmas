// AnalisisKebutuhanGuruDanKelas
Ext.define('simdik_batam.view.AnalisisKebutuhanGuruDanKelasSD.AnalisisKebutuhanGuruDanKelasSD', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.analisiskebutuhangurudankelassdpanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

    	this.items = [{
    		xtype: 'analisiskebutuhangurudankelassdgrid',
            title: 'Analisis Kebutuhan Guru dan Kelas SD',
            region:'center'
    	}];
        
    	this.callParent(arguments);
    }
});