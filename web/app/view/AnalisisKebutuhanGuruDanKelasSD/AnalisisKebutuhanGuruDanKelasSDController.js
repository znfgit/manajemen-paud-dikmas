Ext.define('simdik_batam.view.AnalisisKebutuhanGuruDanKelasSD.AnalisisKebutuhanGuruDanKelasSDController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.MessageBox'
    ],

    alias: 'controller.analisiskebutuhangurudankelassd',

    setupAnalisisKebutuhanGuruDanKelasSDGrid:function(grid){

    	grid.getStore().load();
    }
});