Ext.define('simdik_batam.view.DataPokokSekolah.DetailSekolah', {
	extend: 'Ext.panel.Panel',
    // requires:[
    //     'simdik_batam.view.DataPokokSekolah.DataPokokSekolahController'
    // ],
	alias: 'widget.detailsekolahpanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    // controller: 'main',
    initComponent:function(){

        var record = this.initialConfig.record ? this.initialConfig.record : false;
        if (record){
            this.listeners = {
                afterrender: function(form, options) {
                    // form.loadRecord(record);
                }
            };
        }

    	this.items = [{
            xtype:'tabpanel',
            region:'center',
            title: 'Detail ' + record.data.nama_sekolah,
            items:[{
                title: 'Profil Sekolah',
                layout:'fit',
                glyph: 61687,
                items:[{
                    xtype: 'sekolahform',
                    record:record
                }]
            },{
                title:'PTK',
                glyph: 61447,
                xtype: 'ptkgrid'
            },{
                title: 'Peserta Didik',
                glyph: 61720,
                xtype: 'pesertadidikgrid'
            },{
                title: 'Prasarana',
                glyph: 61687,
                xtype: 'prasaranagrid'
            }]
        }];

    	this.callParent(arguments);
    }
});