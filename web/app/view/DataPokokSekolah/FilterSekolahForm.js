Ext.define('simdik_batam.view.DataPokokSekolah.FilterSekolahForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.filtersekolahform',
    bodyPadding: 10,
    fieldDefaults: {
        labelWidth: 120
    },
    listeners : {
        afterrender: 'setupFilterSekolahForm',
        beforerender: 'setupFilterSekolahFormBefore'
    },
    initComponent: function() {

        var form = this;
        
        var record = this.initialConfig.record ? this.initialConfig.record : false;
        if (record){
            this.listeners = {
                afterrender: function(form, options) {
                    form.loadRecord(record);
                }
            };
        }

        /*this.on('beforeadd', function(form, field){
            if (!field.allowBlank)
              field.labelSeparator += '<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>';
        });*/
        // var pilihBentukStore = Ext.create('Ext.data.Store', {
        //     fields: ['id', 'nama'],
        //     data : [
        //         {"id":"5", "nama":"SD"},
        //         {"id":"6", "nama":"SMP"},
        //         {"id":"13", "nama":"SMA"},
        //         {"id":"15", "nama":"SMK"},
        //         {"id":"29", "nama":"SLB"},
        //         {"id":"14", "nama":"SMLB"},
        //         {"id":"999", "nama":"Semua"}
        //     ]
        // });

        var pilihStatusStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'nama'],
            data : [
                {"id":"1", "nama":"Negeri"},
                {"id":"2", "nama":"Swasta"},
                {"id":"999", "nama":"Semua"}
            ]
        });
        
        this.items = [{
            xtype: 'textfield'
            ,fieldLabel: 'Kata Kunci'
            ,labelAlign: 'top'
            ,allowBlank: true
            ,maxValue: 99999999
            ,minValue: 0
            ,maxLength: 100
            ,anchor: '100%'
            ,enforceMaxLength: false
            ,name: 'keyword'
            ,emptyText: 'Ketik Nama / NPSN...'
        },{
            xtype: 'mstwilayahcombo',
            labelAlign: 'top',
            anchor: '100%',
            fieldLabel: 'Propinsi',
            name: 'combopropmasterdatasekolah',
            emptyText: 'Pilih Propinsi',
            listeners: {
                'change': function(field, newValue, oldValue) {
                    var value = newValue;

                    var combokab = form.down('mstwilayahcombo[name=combokabmasterdatasekolah]');
                    
                    combokab.enable();
                    
                    combokab.setValue('');

                    combokab.getStore().load();  
                }
            }
        },{
            xtype: 'mstwilayahcombo',
            labelAlign: 'top',
            anchor: '100%',
            fieldLabel: 'Kabupaten',
            disabled:true,
            name: 'combokabmasterdatasekolah',
            emptyText: 'Pilih Kabupaten / Kota',
            listeners: {
                'change': function(field, newValue, oldValue) {
                    var value = newValue;

                    var combokec = form.down('mstwilayahcombo[name=combokecmasterdatasekolah]');
                    
                    combokec.enable();
                    
                    combokec.setValue('');

                    combokec.getStore().load();  
                }
            }
        },{
            xtype: 'mstwilayahcombo',
            labelAlign: 'top',
            disabled: true,
            fieldLabel: 'Kecamatan',
            name: 'combokecmasterdatasekolah',
            emptyText: 'Pilih Kecamatan',
            anchor: '100%'
        },{
            xtype:'bentukpendidikancombo',
            name: 'pilihbentukcombo',
            emptyText:'Pilih Bentuk Pendidikan...',
            itemId: 'pilihbentukcombo',
            labelAlign: 'top',
            fieldLabel: 'Bentuk Pendidikan',
            displayField: 'nama',
            anchor: '100%'
        },{
            xtype:'combobox',
            name: 'pilihstatuscombo',
            emptyText:'Pilih Status Sekolah...',
            itemId: 'pilihstatuscombo',
            store: pilihStatusStore,
            labelAlign: 'top',
            fieldLabel: 'Status Sekolah',
            queryMode: 'local',
            displayField: 'nama',
            valueField: 'id',
            anchor: '100%'
        },{
            xtype: 'button',
            text: 'Cari',
            glyph: 61452,
            handler: 'cari',
            anchor: '100%',
            scale: 'medium',
            margin: '10 0 0 0'
        }];

        // this.buttons = [];

        this.callParent(arguments);
    }
});