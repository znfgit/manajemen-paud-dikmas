Ext.define('simdik_batam.view.DataPokokSekolah.KoregWindow', {
	extend: 'Ext.window.Window',
    alias: 'widget.koregwindow',
    requires:[
        'Ext.layout.container.Fit',
        'Ext.layout.container.VBox',
        'Ext.TabPanel'
    ],
    layout: {
        type: 'border'
    },
    title: 'Kode Registrasi',
    width: 500,
    height: 200,
    initComponent: function() {     
        this.items =[{
            xtype: 'panel',
            region: 'center',
            html:'Kode Registrasi'
        }];

        this.tools = [{
            type: 'maximize',
            handler: function(event, target, owner, tool){
                win = tool.up('window');
                win.toggleMaximize();

                var tool = win.down('tool[type=minimize]');

                tool.show();
            }
        },{
            type: 'minimize',
            hidden:true,
            handler: function(event, target, owner, tool){
                win = tool.up('window');
                win.toggleMaximize();

                var tool = win.down('tool[type=minimize]');

                tool.hide();
            }
        }];

        this.buttons = ['-'];
        
        this.callParent(arguments);
    }
});