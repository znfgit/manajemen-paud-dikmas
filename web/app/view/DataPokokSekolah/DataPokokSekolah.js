Ext.define('simdik_batam.view.DataPokokSekolah.DataPokokSekolah', {
	extend: 'Ext.panel.Panel',
    // requires:[
    //     'simdik_batam.view.DataPokokSekolah.DataPokokSekolahController'
    // ],
	alias: 'widget.datapokoksekolahpanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    // controller: 'main',
    initComponent:function(){

    	this.items = [{
            xtype:'filtersekolahform',
            region:'west',
            title:'Filter',
            collapsible:true,
            width:220,
            split:true,
            border:true
        },{
    		xtype: 'datapokoksekolahgrid',
            region:'center',
            title: 'Data Pokok Sekolah'
    	}];

    	this.callParent(arguments);
    }
});