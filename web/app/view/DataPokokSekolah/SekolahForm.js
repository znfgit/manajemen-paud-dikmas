Ext.define('simdik_batam.view.DataPokokSekolah.SekolahForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.sekolahform',
    bodyPadding:20,
    autoScroll:true,
    defaults: {
        border: false,
        xtype: 'panel',
        flex: 1,
        layout: 'anchor'
    }, 
    layout: {
        type:'hbox'
    },
    initComponent: function() {
        
        var record = this.initialConfig.record ? this.initialConfig.record : false;
        if (record){
            this.listeners = {
                afterrender: function(form, options) {
                    form.loadRecord(record);
                }
            };
        }

        this.dockedItems = [{
            xtype:'toolbar',
            dock:'top',
            ui:'footer',
            items:[{
                text:'Export Xls',
                glyph: 61646,
                handler:'exportProfilSekolah'
            }]
        }]

        this.items = [{
            defaults: {
                anchor: '-5'
            },
            items:[{
                xtype: 'hidden'
                ,fieldLabel: 'ID'
                ,labelAlign: 'right'
                ,labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
                ,allowBlank: false
                ,maxValue: 99999999
                ,minValue: 0
                ,name: 'sekolah_id'
            },{
                xtype: 'fieldset'
                ,title: 'Identitas Sekolah'
                ,collapsible: true
                ,labelAlign: 'right'
                ,defaults: {
                    labelWidth: 175
                    ,anchor: '100%'
                    ,margins: '0 0 0 5',
                    renderer:function(v){
                        return '<b>' + v + '</b>';
                    }
                }
                ,items: [{
                    xtype: 'displayfield'
                    ,allowBlank: false
                    ,minValue: 0
                    ,fieldLabel: 'Nama'
                    ,maxLength: 80
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,margins: '0 0 0 0'
                    ,name: 'nama_sekolah'
                },{
                    xtype: 'hidden'
                    ,minValue: 0
                    ,fieldLabel: 'NSS'
                    ,maxLength: 12
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'nss'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'NPSN'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'npsn'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Bentuk Pendidikan'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'bentuk_pendidikan_id_str'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Status Sekolah'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'status_sekolah'
                    ,renderer: function(v){
                        if(v == 1){
                            return '<b>Negeri</b>';
                        }else{
                            return '<b>Swasta</b>';
                        }
                    }
                }]
                
            },{
                xtype: 'fieldset'
                ,title: 'Lokasi Sekolah'
                ,collapsible: true
                ,labelAlign: 'right'
                ,defaults: {
                    labelWidth: 175
                    ,anchor: '100%'
                    ,margins: '0 0 0 5'
                    ,renderer:function(v){
                        return '<b>' + v + '</b>';
                    }
                }
                ,items: [{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Alamat'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'alamat_jalan'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'RT'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'rt'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'RW'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'rw'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Nama Dusun'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'nama_dusun'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Desa/Kelurahan'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'desa_kelurahan'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Kecamatan'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'kecamatan'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Kabupaten/Kota'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'kabupaten'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Propinsi'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'propinsi'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Kode Pos'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'kode_pos'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Lintang'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'lintang'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Bujur'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'bujur'
                }]
                
            }]
        },{
            defaults: {
                anchor: '-5'
            },
            items:[{
                xtype: 'fieldset'
                ,title: 'Kontak Sekolah'
                ,collapsible: true
                ,labelAlign: 'right'
                ,defaults: {
                    labelWidth: 175
                    ,anchor: '100%'
                    ,margins: '0 0 0 5'
                    ,renderer:function(v){
                        return '<b>' + v + '</b>';
                    }
                }
                ,items: [{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Nomor Telepon'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'nomor_telepon'
                    
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Nomor Fax'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'nomor_fax'
                    
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Email'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'email'
                    
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Website'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'website'
                    
                }]
            },{
                xtype: 'fieldset'
                ,title: 'Data Pelengkap'
                ,collapsible: true
                ,labelAlign: 'right'
                ,defaults: {
                    labelWidth: 175
                    ,anchor: '100%'
                    ,margins: '0 0 0 5'
                    ,renderer:function(v){
                        return '<b>' + v + '</b>';
                    }
                }
                ,items: [{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'SK Pendirian Sekolah'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'sk_pendirian_sekolah'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Tanggal SK Pendirian'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'tanggal_sk_pendirian'
                    ,renderer : Ext.util.Format.dateRenderer('d/m/Y')
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Status Kepemilikan'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'status_kepemilikan_id_str'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'SK Izin Operasional'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'sk_izin_operasional'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Tanggal SK izin operasional'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'tanggal_sk_izin_operasional'
                    ,renderer : Ext.util.Format.dateRenderer('d/m/Y')
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'SK Akreditasi'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'sk_akreditasi'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Tanggal SK Akreditasi'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'tanggal_sk_akreditasi'
                    ,renderer : Ext.util.Format.dateRenderer('d/m/Y')
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Nomor Rekening'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'no_rekening'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Nama Bank'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'nama_bank'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Cabang KCP Unit'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'cabang_kcp_unit'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Rekening Atas Nama'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'rekening_atas_nama'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'MBS'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'rekening_atas_nama'
                    ,renderer: function(v){
                        if(v == 1){
                            return '<b>Ya</b>';
                        }else{
                            return '<b>Tidak</b>';
                        }
                    }
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Luas Tanah Milik'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'luas_tanah_milik'
                    
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Luas Tanah Bukan Milik'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'luas_tanah_bukan_milik'
                    
                }]
                
            },{
                xtype: 'hidden'
                ,fieldLabel: 'Kode registrasi'
                ,labelAlign: 'right'
                ,maxValue: 99999999
                ,minValue: 0
                ,maxLength: 100
                ,enforceMaxLength: true
                ,name: 'kode_registrasi'
            },{
                xtype: 'hidden'
                ,fieldLabel: 'Flag'
                ,labelAlign: 'right'
                ,maxValue: 99999999
                ,minValue: 0
                ,maxLength: 1
                ,enforceMaxLength: true
                ,name: 'flag'
            
            }]
        }];

        this.callParent(arguments);
    }
});