Ext.define('simdik_batam.view.DataPokokSekolah.DataPokokSekolahGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.datapokoksekolahgrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    listeners : {
        afterrender: 'setupDataPokokSekolahGrid'
    },
    initComponent: function() {
        
        var grid = this;

        this.store = Ext.create('simdik_batam.store.Sekolah');
        
        this.getSelectionModel().setSelectionMode('SINGLE');

        this.columns = [{
            header: 'ID',
            width: 60,
            sortable: true,
            dataIndex: 'sekolah_id',
            hideable: false,
            hidden: true,
            field: {
                xtype: 'hidden'
            }
        },{
            header: 'Nama',
            width: 300,
            sortable: true,
            dataIndex: 'nama_sekolah',
            hideable: false
        },{
            header: 'Nss',
            width: 96,
            sortable: true,
            dataIndex: 'nss',
            hideable: false
        },{
            header: 'Npsn',
            width: 84,
            sortable: true,
            dataIndex: 'npsn',
            hideable: false
        },{
            header: 'Bentuk Pendidikan',
            width: 170,
            sortable: true,
            dataIndex: 'bentuk_pendidikan_id',
            hideable: false,
            renderer: function(v,p,r) {
                return r.data.bentuk_pendidikan_id_str;
            }
        },{
            header: 'Status Sekolah',
            width: 150,
            sortable: true,
            dataIndex: 'status_sekolah',
            hideable: false,          
            renderer: function(v,p,r) {
                switch (v) {
                    case 1.0 : return 'Negeri'; break;                  
                    case 2.0 : return 'Swasta'; break;                  
                    default : return '-'; break;
                }
            }
        },{
            header: 'Kecamatan',
            width: 170,
            sortable: true,
            dataIndex: 'kecamatan',
            hideable: false
        },{
            header: 'Kab / Kota',
            width: 170,
            sortable: true,
            dataIndex: 'kabupaten',
            hideable: false
        },{
            header: 'Alamat Jalan',
            width: 300,
            sortable: true,
            dataIndex: 'alamat_jalan',
            hideable: false
        },{
            header: 'Nomor Telepon',
            width: 120,
            sortable: true,
            dataIndex: 'nomor_telepon',
            hideable: false
        }];
        
        this.dockedItems = [{
            xtype: 'toolbar',
            items: ['-',{
                text: 'Detail',
                glyph: 61485,
                handler: 'DetailSekolah'
            }]
        }];
        
        this.bbar = Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
        
        this.callParent(arguments);
    }
});