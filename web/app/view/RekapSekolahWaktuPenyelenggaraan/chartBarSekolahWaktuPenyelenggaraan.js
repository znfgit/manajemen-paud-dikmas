Ext.define('simdik_batam.view.RekapSekolahWaktuPenyelenggaraan.chartBarSekolahWaktuPenyelenggaraan', {
    extend: 'Ext.Panel',
    xtype: 'chartbarsekolahwaktupenyelenggaraan',


    width: 650,

    initComponent: function() {
        var me = this;

        this.myDataStore = Ext.create('simdik_batam.store.ChartBarSekolahPerWaktuPenyelenggaraan');

        me.items = [{
            xtype: 'cartesian',
            width: '100%',
            height: 460,
            legend: {
                docked: 'bottom'
            },
            interactions: ['itemhighlight'],
            store: this.myDataStore,
            insetPadding: {
                top: 40,
                left: 40,
                right: 40,
                bottom: 40
            },
            // sprites: [{
            //     type: 'text',
            //     text: 'Column Charts - Stacked Columns',
            //     font: '22px Helvetica',
            //     width: 100,
            //     height: 30,
            //     x: 40, // the sprite x position
            //     y: 20  // the sprite y position
            // }, {
            //     type: 'text',
            //     text: 'Data: Browser Stats 2012',
            //     font: '10px Helvetica',
            //     x: 12,
            //     y: 380
            // }, {
            //     type: 'text',
            //     text: 'Source: http://www.w3schools.com/',
            //     font: '10px Helvetica',
            //     x: 12,
            //     y: 390
            // }],
            axes: [{
                type: 'numeric',
                position: 'left',
                adjustByMajorUnit: true,
                grid: true,
                fields: ['pagi'],
                minimum: 0
            }, {
                type: 'category',
                position: 'bottom',
                grid: true,
                fields: ['desk'],
                label: {
                    rotate: {
                        degrees: -45
                    }
                }
                
            }],
            series: [{
                type: 'bar',
                axis: 'left',
                title: [ 'pagi', 'siang', 'kombinasi', 'sore', 'malam', 'sehari penuh (5 h/m)', 'sehari penuh (6 h/m)', 'lainnya' ],
                xField: 'desk',
                yField: [ 'pagi', 'siang', 'kombinasi', 'sore', 'malam', 'sehari_penuh_5', 'sehari_penuh_6', 'lainnya' ],
                stacked: true,
                style: {
                    opacity: 0.80
                },
                highlight: {
                    fillStyle: 'yellow'
                },
                tooltip: {
                    style: 'background: #fff',
                    renderer: function(storeItem, item) {
                        var browser = item.series.getTitle()[Ext.Array.indexOf(item.series.getYField(), item.field)];
                        this.setHtml(browser + ' di wilayah ' + storeItem.get('desk') + ': ' + storeItem.get(item.field));
                    }
                }
            }]
        }];

        this.myDataStore.load();

        this.callParent();
    }
});