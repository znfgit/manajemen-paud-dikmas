Ext.define('simdik_batam.view.RekapSekolahWaktuPenyelenggaraan.SekolahWaktuPenyelenggaraanGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.sekolahwaktupenyelenggaraangrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    listeners:{
        afterrender:'setupSekolahWaktuPenyelenggaraanGrid'
    },
    formatMoney: function(a, c, d, t){
        var n = a, 
            c = isNaN(c = Math.abs(c)) ? 0 : c, 
            d = d == undefined ? "," : d, 
            t = t == undefined ? "." : t, 
            s = n < 0 ? "-" : "", 
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('simdik_batam.store.ChartBarSekolahPerWaktuPenyelenggaraan');
        
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        this.getSelectionModel().setSelectionMode('SINGLE');
        
        this.columns = [{
            header: 'Wilayah',
            width:120,
            sortable: true,
            dataIndex: 'desk',
			hideable: false,
            hidden: false
        },{
            header: 'Pagi',
            width: 100,
            sortable: true,
            dataIndex: 'pagi',
			hideable: false,
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }

        },{
            header: 'Siang',
            width: 100,
            sortable: true,
            dataIndex: 'siang',
            hideable: false,
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }

        },{
            header: 'Kombinasi',
            width: 100,
            sortable: true,
            dataIndex: 'kombinasi',
            hideable: false,
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }

        },{
            header: 'Sore',
            width: 100,
            sortable: true,
            dataIndex: 'sore',
            hideable: false,
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }

        },{
            header: 'Malam',
            width: 100,
            sortable: true,
            dataIndex: 'malam',
            hideable: false,
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }

        },{
            header: 'Sehari Penuh (5 h/m)',
            width: 180,
            sortable: true,
            dataIndex: 'sehari_penuh_5',
            hideable: false,
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },{
            header: 'Sehari Penuh (6 h/m)',
            width: 180,
            sortable: true,
            dataIndex: 'sehari_penuh_6',
            hideable: false,
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },{
            header: 'Lainnya',
            width: 100,
            sortable: true,
            dataIndex: 'lainnya',
            hideable: false,
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        }];

        // var pilihBentukStore = Ext.create('Ext.data.Store', {
        //     fields: ['id', 'nama'],
        //     data : [
        //         {"id":"5", "nama":"SD"},
        //         {"id":"6", "nama":"SMP"},
        //         {"id":"29", "nama":"SLB"},
        //         {"id":"999", "nama":"Semua"}
        //     ]
        // });


        // this.dockedItems = [{
        //     xtype: 'toolbar',
        //     items: ['-',{
        //         xtype:'combobox',
        //         name: 'pilihbentukcombo',
        //         emptyText:'Pilih Bentuk Pendidikan...',
        //         // itemId: 'pilihbentukcombo',
        //         store: pilihBentukStore,
        //         queryMode: 'local',
        //         displayField: 'nama',
        //         valueField: 'id',
        //         width: 200,
        //         listeners: {
        //             'change': function(field, newValue, oldValue) {
                        
        //                 grid.getStore().reload();

        //                 grid.up('panel').up('panel').down('panel').down('chartpieptkjeniskelamin').down('polar').getStore().reload({
        //                     params:{
        //                         bentuk_pendidikan_id:newValue
        //                     }
        //                 });

        //                 grid.up('panel').up('panel').down('panel').down('chartbarptkjeniskelamin').down('cartesian').getStore().reload({
        //                     params:{
        //                         bentuk_pendidikan_id:newValue
        //                     }
        //                 });

        //                 // this.up('panel').up('panel').up('panel').setTitle('tes');
        //                 // this.up('panel').up('panel').up('panel').down('panel').down('panel[itemId=chartbar]').down('chartbarptkjeniskelamin').getStore().reload();
        //                 // this.up('panel').up('panel').up('panel').down('panel').down('panel').setTitle('Jumlah ' + field + ' Negeri Swasta');
        //             }
        //         }
        //     }]
        // }];
        
        this.bbar = Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
        
        this.callParent(arguments);
    }
});