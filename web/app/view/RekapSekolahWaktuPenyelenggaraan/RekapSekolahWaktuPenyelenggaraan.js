Ext.define('simdik_batam.view.RekapSekolahWaktuPenyelenggaraan.RekapSekolahWaktuPenyelenggaraan', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.rekapsekolahwaktupenyelenggaraan',
    border: false,
    bodyBorder: false,
    layout: 'border',
    initComponent:function(){

        this.items = [{
            xtype: 'panel',
            region:'east',
            flex: 1,
            split:true,
            autoScroll:true,
            layout: {
                type:'vbox',
                padding:'0',
                align:'stretch'
            },
            defaults:{margin:'0 0 10 0'},
            items:[{
                xtype:'panel',
                title: 'Grafik Jumlah Sekolah Berdasarkan Waktu Penyelenggaraan',
                itemId: 'chartpie',
                split: true,
                layout:'fit',
                border:false,
                items:[{
                    xtype: 'chartpiesekolahwaktupenyelenggaraan',
                    height:300
                }]
            },{
                xtype:'panel',
                title: 'Grafik Sebaran Jumlah Sekolah Berdasarkan Waktu Penyelenggaraan',
                itemId: 'chartbar',
                split: true,
                layout:'fit',
                border:false,
                items:[{
                    xtype: 'chartbarsekolahwaktupenyelenggaraan',
                    minHeight:500
                }]
            }]
        },{
            xtype: 'panel',
            region: 'center',
            layout:'border',
            title:'Jumlah Sekolah Berdasarkan Waktu Penyelenggaraan',
            items:[{
                xtype: 'sekolahwaktupenyelenggaraantotalgrid',
                region:'center'
            },{
                xtype: 'sekolahwaktupenyelenggaraangrid',
                region: 'south',
                split:true,
                collapsible:true,
                height:280,
                title:'Tabel Sebaran Jumlah Sekolah Berdasarkan Waktu Penyelenggaraan'
            }]
        }];

        this.callParent(arguments);
    }
});