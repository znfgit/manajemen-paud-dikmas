Ext.define('simdik_batam.view.RekapPdAngkaPutusSekolah.RekapPdAngkaPutusSekolah', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.pdangkaputussekolahpanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

    	this.items = [{
            // xtype: 'ptkstatuskepegawaiangrid',
            xtype:'panel',
            region:'center',
            split:true,
            layout:'border',
            items:[{
                xtype:'pdangkaputussekolahgrid',
                region:'center',
                title: 'Sebaran Angka Putus Sekolah Peserta Didik'
            }]
        },{
            xtype: 'panel',
            region:'south',
            height:300,
            collapsible:true,
            split:true,
            autoScroll:true,
            title: 'Grafik Sebaran Angka Putus Sekolah Peserta Didik',
            layout: {
                type:'fit',
                padding:'0',
                align:'stretch'
            },
            defaults:{margin:'0 0 10 0'},
            items:[{
                xtype: 'chartbarpdangkaputussekolah',
                itemId: 'chartbar',
                split: true,
                layout:'fit',
                border:false
            }]
        }];

    	this.callParent(arguments);
    }
});