Ext.define('simdik_batam.view.RekapPdAngkaPutusSekolah.PdAngkaPutusSekolahGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.pdangkaputussekolahgrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    columnLines:true,
    requires:[
        'simdik_batam.view.RekapPdAngkaPutusSekolah.RekapPdAngkaPutusSekolahController'
    ],
    controller: 'rekappdangkaputussekolah',
    listeners:{
        afterrender:'setupPdAngkaPutusSekolahGrid'
    },
    formatMoney: function(a, c, d, t){
        var n = a, 
            c = isNaN(c = Math.abs(c)) ? 0 : c, 
            d = d == undefined ? "," : d, 
            t = t == undefined ? "." : t, 
            s = n < 0 ? "-" : "", 
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('simdik_batam.store.ChartBarPesertaDidikAngkaPutusSekolah');
        
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        this.getSelectionModel().setSelectionMode('SINGLE');
        
        this.columns = [{
            header: 'Wilayah',
            width:170,
            locked   : true,
            sortable: true,
            dataIndex: 'desk',

			hideable: false,
            hidden: false
        },{
            header: 'Jumlah Angka Putus Sekolah',
            width: 300,
            sortable: true,
            dataIndex: 'jumlah',
			hideable: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        }];

        this.dockedItems = [{
            xtype: 'toolbar',
            items: ['-',{
                xtype:'bentukpendidikancombo',
                name: 'pilihbentukcombo',
                emptyText:'Pilih Bentuk Pendidikan...',
                displayField: 'nama',
                width: 200,
                listeners: {
                    'change': function(field, newValue, oldValue) {
                        
                        grid.getStore().reload();

                        // grid.up('panel').down('ptkstatuskepegawaiangrid').getStore().reload();

                        grid.up('panel').up('panel').down('panel[region=south]').down('chartbarpdangkamengulang').down('cartesian').getStore().reload({
                            params:{
                                bentuk_pendidikan_id:newValue
                            }
                        });
                    }
                }
            },{
                xtype:'button',
                text:'Export Xls',
                glyph: 61474,
                urls: 'excel/SebaranPdAngkaPutusSekolah',
                handler:'exportXls'
            }]
        }];
        
        // this.bbar = Ext.create('Ext.PagingToolbar', {
        //     store: this.store,
        //     displayInfo: true,
        //     displayMsg: 'Displaying data {0} - {1} of {2}',
        //     emptyMsg: "Tidak ada data"
        // });
        
        this.callParent(arguments);
    }
});