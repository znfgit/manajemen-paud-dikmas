Ext.define('simdik_batam.view.RekapPdAngkaPutusSekolah.RekapPdAngkaPutusSekolahController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.MessageBox'
    ],

    alias: 'controller.rekappdangkaputussekolah',

    setupPdAngkaPutusSekolahGrid:function(grid){
    	grid.getStore().on('beforeload', function(store){
            // var keyword = grid.up('panel').down('ptkmatapelajarantotalgrid').down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword
            });
        });

    	grid.getStore().load();
    },
    exportXls:function(btn){
        var url = btn.urls;

       var bentuk_pendidikan_id = btn.up('toolbar').down('combobox').getValue();

       window.location = url+'/'+bentuk_pendidikan_id;
    }
});