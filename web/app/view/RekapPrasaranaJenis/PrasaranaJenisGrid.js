Ext.define('simdik_batam.view.RekapPrasaranaJenis.PrasaranaJenisGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.prasaranajenisgrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    requires:[
        'simdik_batam.view.RekapPrasaranaJenis.RekapPrasaranaJenisController'
    ],
    controller: 'rekapprasaranajenis',
    listeners:{
        afterrender:'setupPrasaranaJenisGrid'
    },
    formatMoney: function(a, c, d, t){
        var n = a, 
            c = isNaN(c = Math.abs(c)) ? 0 : c, 
            d = d == undefined ? "," : d, 
            t = t == undefined ? "." : t, 
            s = n < 0 ? "-" : "", 
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('simdik_batam.store.ChartBarPrasaranaJenis');
        
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        this.getSelectionModel().setSelectionMode('SINGLE');
        
        this.columns = [{
            header: 'Jenis Prasarana',
            width:200,
            sortable: true,
            locked:false,
            dataIndex: 'nama',
			hideable: false,
            hidden: false
        },{
            header: 'Total',
            width: 150,
            sortable: true,
            dataIndex: 'total',
			hideable: false,
            hidden: false,
            align:'right'
        }];

        this.dockedItems = [{
            xtype: 'toolbar',
            items: ['-',{
                xtype:'bentukpendidikancombo',
                name: 'pilihbentukcombo',
                emptyText:'Pilih Bentuk Pendidikan...',
                displayField: 'nama',
                width: 200,
                listeners: {
                    'change': function(field, newValue, oldValue) {
                        
                        grid.getStore().reload();
                    }
                }
            },{
                xtype:'button',
                text:'Export Xls',
                glyph: 61474,
                urls: 'excel/SebaranPrasaranaPerJenis',
                handler:function(btn){
                    var bentuk_pendidikan_id = btn.up('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
                    
                    window.location = 'excel/SebaranPrasaranaPerJenis?bentuk_pendidikan_id='+bentuk_pendidikan_id;
                }
            }]
        }];
        
        this.bbar = Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
        
        this.callParent(arguments);
    }
});