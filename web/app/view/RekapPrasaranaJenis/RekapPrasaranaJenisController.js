Ext.define('simdik_batam.view.RekapPrasaranaJenis.RekapPrasaranaJenisController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.MessageBox'
    ],

    alias: 'controller.rekapprasaranajenis',
    exportXls:function(btn){
        var url = btn.urls;

       var bentuk_pendidikan_id = btn.up('toolbar').down('combobox').getValue();

       window.location = url+'/'+bentuk_pendidikan_id;
    },
    setupPrasaranaJenisGrid:function(grid){
    	grid.getStore().on('beforeload', function(store){
            var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword
            });
        });

        var storeMst = Ext.create('simdik_batam.store.MstWilayah');

        storeMst.on('load', function(store, records, options){

            for (var i = records.length - 1; i >= 0; i--) {
                
                var kolom = Ext.create('Ext.grid.column.Column', {
                    header: records[i].data.nama,
                    width: 150,
                    sortable: true,
                    dataIndex: "col_" + records[i].data.kode_wilayah.replace("  ", ""),
                    hideable: false,
                    align:'right'
                });

                grid.headerCt.insert(2, kolom);

                console.log(kolom);
            };
            
        });  

        storeMst.load({
            params:{
                mode:'kolom'
            }
        });

    	grid.getStore().load();
    }
});