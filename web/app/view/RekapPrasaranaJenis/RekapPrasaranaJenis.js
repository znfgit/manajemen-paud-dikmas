Ext.define('simdik_batam.view.RekapPrasaranaJenis.RekapPrasaranaJenis', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.prasaranajenispanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

        this.items = [{
           xtype:'panel',
            region:'center',
            split:true,
            layout:'border',
            items:[{
                xtype:'prasaranajenisgrid',
                region:'center',
                title: 'Jumlah Prasarana Berdasarakan Jenis Per Wilayah'
            }]
        }];

        this.callParent(arguments);
    }
});