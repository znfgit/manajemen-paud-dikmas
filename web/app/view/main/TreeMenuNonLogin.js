Ext.define('simdik_batam.view.main.TreeMenuNonLogin', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.treemenunonlogin',
    layout: 'fit',
    initComponent: function() {

        var store = Ext.create('Ext.data.TreeStore', {
            root: {
                expanded: true,
                children: [{
                    text: 'Beranda',
                    leaf: true,
                    id: 'Beranda'
                },{
                    text: 'Analisis Data Pokok',
                    expanded: true,
                    children:[
                    {
                        text: "Sekolah",
                        expanded: true,
                        children: [
                        {
                            text: "Status Sekolah",
                            leaf: true,
                            id: 'RekapSekolahStatus'
                        }
                        ,{
                            text: "Bentuk Pendidikan",
                            leaf: true,
                            id: 'RekapSekolahBentukPendidikan'
                        }
                        ,{
                            text: "Waktu Penyelenggaraan",
                            leaf: true,
                            id: 'RekapSekolahWaktuPenyelenggaraan'
                        } 
                        // ,{
                        //     text: "Akreditasi",
                        //     leaf: true,
                        //     id: 'SekolahAkreditasi'
                        // }, {
                        //     text: "Listrik",
                        //     leaf: true,
                        //     id: 'SekolahListrik'
                        // }, {
                        //     text: "Sanitasi",
                        //     leaf: true,
                        //     id: 'SekolahSanitasi'
                        // }, {
                        //     text: "Akses Internet",
                        //     leaf: true,
                        //     id: 'SekolahAksesInternet'
                        // }, {
                        //     text: "MBS",
                        //     leaf: true,
                        //     id: 'SekolahMbs'
                        // }, {
                        //     text: "Wilayah Khusus",
                        //     leaf: true,
                        //     id: 'SekolahWilayahKhusus'
                        // }
                        ]
                    },{
                        text: "PTK",
                        expanded: true,
                        children: [{
                            text: "Jenis Kelamin",
                            leaf: true,
                            id: 'RekapPtkJenisKelamin'
                        },
                        {
                            text: "Agama",
                            leaf: true,
                            id: 'RekapPtkAgama'
                        }, 
                        // ,{
                        //     text: "Usia",
                        //     leaf: true,
                        //     id: 'PtkUsia'
                        // }, 
                        {
                            text: "Status Kepegawaian",
                            leaf: true,
                            id: 'RekapPtkStatusKepegawaian'
                        },
                        {
                            text: "Mata Pelajaran",
                            leaf: true,
                            id: 'RekapPtkMataPelajaran'
                        }, 
                        // {
                        //     text: "Kualifikasi",
                        //     leaf: true,
                        //     id: 'PtkKualifikasi'
                        // }, {
                        //     text: "Sertifikasi",
                        //     leaf: true,
                        //     id: 'PtkSertifikasi'
                        // }, 
                        {
                            text: "Jenis PTK",
                            leaf: true,
                            id: 'RekapPtkJenisPtk'
                        }
                        ]
                    },{
                        text: "Peserta Didik",
                        expanded: true,
                        children: [
                        {
                            text: "Jenis Kelamin",
                            leaf: true,
                            id: 'RekapPdJenisKelamin'
                        }, 
                        // {
                        //     text: "Tingkat",
                        //     leaf: true,
                        //     id: 'PdTingkat'
                        // },
                         {
                            text: "Usia",
                            leaf: true,
                            id: 'RekapPdPerUmur'
                        }, 
                        {
                            text: "Agama",
                            leaf: true,
                            id: 'RekapPdAgama'
                        },
                        {
                            text: "Angka Mengulang",
                            leaf: true,
                            id: 'RekapPdAngkaMengulang'
                        },
                        {
                            text: "Angka Putus Sekolah",
                            leaf: true,
                            id: 'RekapPdAngkaPutusSekolah'
                        }
                        ]
                    },{
                        text: "Prasarana",
                        expanded: true,
                        children: [
                        {
                            text: "Jenis Prasarana",
                            leaf: true,
                            id: 'RekapPrasaranaJenis'
                        }, 
                        // {
                        //     text: "Total Ruang Guru",
                        //     leaf: true,
                        //     id: 'PrasaranaTotalRuangGuru'
                        // }, {
                        //     text: "Total Ruang Kepala Sekolah",
                        //     leaf: true,
                        //     id: 'PrasaranaTotalRuangKepalaSekolah'
                        // }, {
                        //     text: "Total Ruang Perpustakaan",
                        //     leaf: true,
                        //     id: 'PrasaranaTotalRuangPerpustakaan'
                        // }
                        ]
                    }
                    ]
                }]
            }
        });

        this.items = [
            {
                xtype: 'treepanel',
                rootVisible: false,
                // bodyStyle: 'background:#FFFCDE',
                store: store,
                width:200,
                // region: 'center',
                listeners: {
                    itemclick: 'treeMenuItemClick'
                }
            }
        ];

        this.callParent();
    }
});