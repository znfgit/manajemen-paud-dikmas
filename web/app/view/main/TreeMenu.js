Ext.define('simdik_batam.view.main.TreeMenu', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.treemenu',
    layout: 'fit',
    initComponent: function() {

        var store = Ext.create('Ext.data.TreeStore', {
            root: {
                expanded: true,
                children: [{
                    text: 'Beranda',
                    leaf: true,
                    id: 'Beranda'
                },{
                    text: 'Administrasi',
                    expanded: true,
                    id:'Administrasi',
                    children:[
                    {
                        text: 'Manajemen Pengguna',
                        leaf: true,
                        id: 'ManajemenPengguna'
                    },
                    {
                        text: 'Progres Data',
                        leaf: true,
                        id: 'ProgresData'
                    },
                    // {
                    //     text: 'Custom Reporting',
                    //     leaf: true,
                    //     id: 'CustomReporting'
                    // }
                    ]
                },{
                    text: 'Data Pokok',
                    expanded: true,
                    children: [
                    {
                        text: "Sekolah",
                        leaf: true,
                        id: 'DataPokokSekolah'
                    }
                    ,{
                        text: "PTK",
                        leaf: true,
                        id: 'DataPokokPtk'
                    }
                    ,{
                        text: "Peserta Didik",
                        leaf: true,
                        id: 'DataPokokPesertaDidik'
                    }
                    // ,{
                    //     text: "Prasarana",
                    //     leaf: true,
                    //     id: 'DataPokokPrasarana'
                    // }
                    ]
                },{
                    text: 'Rekap Data Pokok',
                    expanded: true,
                    children:[
                    {
                        text: "Sekolah",
                        expanded: true,
                        children: [
                        {
                            text: "Rangkuman",
                            leaf: true,
                            id: 'RekapSekolahRangkuman'
                        },
                        {
                            text: "Status Sekolah",
                            leaf: true,
                            id: 'RekapSekolahStatus'
                        }
                        ,{
                            text: "Bentuk Pendidikan",
                            leaf: true,
                            id: 'RekapSekolahBentukPendidikan'
                        }
                        ,{
                            text: "Waktu Penyelenggaraan",
                            leaf: true,
                            id: 'RekapSekolahWaktuPenyelenggaraan'
                        } 
                        // ,{
                        //     text: "Akreditasi",
                        //     leaf: true,
                        //     id: 'SekolahAkreditasi'
                        // }, {
                        //     text: "Listrik",
                        //     leaf: true,
                        //     id: 'SekolahListrik'
                        // }, {
                        //     text: "Sanitasi",
                        //     leaf: true,
                        //     id: 'SekolahSanitasi'
                        // }, {
                        //     text: "Akses Internet",
                        //     leaf: true,
                        //     id: 'SekolahAksesInternet'
                        // }, {
                        //     text: "MBS",
                        //     leaf: true,
                        //     id: 'SekolahMbs'
                        // }, {
                        //     text: "Wilayah Khusus",
                        //     leaf: true,
                        //     id: 'SekolahWilayahKhusus'
                        // }
                        ]
                    },{
                        text: "PTK",
                        expanded: true,
                        children: [{
                            text: "Jenis Kelamin",
                            leaf: true,
                            id: 'RekapPtkJenisKelamin'
                        },
                        {
                            text: "Agama",
                            leaf: true,
                            id: 'RekapPtkAgama'
                        }, 
                        // ,{
                        //     text: "Usia",
                        //     leaf: true,
                        //     id: 'PtkUsia'
                        // }, 
                        {
                            text: "Status Kepegawaian",
                            leaf: true,
                            id: 'RekapPtkStatusKepegawaian'
                        },
                        {
                            text: "Mata Pelajaran",
                            leaf: true,
                            id: 'RekapPtkMataPelajaran'
                        }, 
                        // {
                        //     text: "Kualifikasi",
                        //     leaf: true,
                        //     id: 'PtkKualifikasi'
                        // }, {
                        //     text: "Sertifikasi",
                        //     leaf: true,
                        //     id: 'PtkSertifikasi'
                        // }, 
                        {
                            text: "Jenis PTK",
                            leaf: true,
                            id: 'RekapPtkJenisPtk'
                        }
                        ]
                    },{
                        text: "Peserta Didik",
                        expanded: true,
                        children: [
                        {
                            text: 'Tingkat Kelas & Jenis Kelamin',
                            leaf: true,
                            id: 'RekapPdTingkatKelasJenisKelamin'
                        },
                        {
                            text: "Jenis Kelamin",
                            leaf: true,
                            id: 'RekapPdJenisKelamin'
                        }, 
                        // {
                        //     text: "Tingkat",
                        //     leaf: true,
                        //     id: 'PdTingkat'
                        // },
                        {
                            text: "Usia",
                            leaf: true,
                            id: 'RekapPdPerUmur'
                        },
                        // {
                        //     text: "Jumlah PD Per Rombel",
                        //     leaf: true,
                        //     id: 'RekapPdPerJumlahRombel'
                        // }, 
                        {
                            text: "Agama",
                            leaf: true,
                            id: 'RekapPdAgama'
                        },
                        {
                            text: "Angka Mengulang",
                            leaf: true,
                            id: 'RekapPdAngkaMengulang'
                        },
                        {
                            text: "Angka Putus Sekolah",
                            leaf: true,
                            id: 'RekapPdAngkaPutusSekolah'
                        },
                        {
                            text: "Angka Siswa Miskin",
                            leaf: true,
                            id: 'RekapPdAngkaSiswaMiskin'
                        }
                        ]
                    },{
                        text: "Prasarana",
                        expanded: true,
                        children: [
                        {
                            text: "Jenis Prasarana",
                            leaf: true,
                            id: 'RekapPrasaranaJenis'
                        }
                        // ,{
                        //     text: "Kerusakan Prasarana",
                        //     leaf: true,
                        //     id: 'RekapPrasaranaKerusakanPrasarana'
                        // }, 
                        // {
                        //     text: "Total Ruang Guru",
                        //     leaf: true,
                        //     id: 'PrasaranaTotalRuangGuru'
                        // }, {
                        //     text: "Total Ruang Kepala Sekolah",
                        //     leaf: true,
                        //     id: 'PrasaranaTotalRuangKepalaSekolah'
                        // }, {
                        //     text: "Total Ruang Perpustakaan",
                        //     leaf: true,
                        //     id: 'PrasaranaTotalRuangPerpustakaan'
                        // }
                        ]
                    }
                    ]
                }
                // ,{
                //     text: 'Analisis Kebutuhan',
                //     expanded: true,
                //     children:[{
                //         text: "Guru dan Kelas",
                //         expanded: true,
                //         children:[{
                //             text: "SD",
                //             leaf: true,
                //             id: 'AnalisisKebutuhanGuruDanKelasSD'
                //         },{
                //             text: "SMP",
                //             leaf: true,
                //             id: 'AnalisisKebutuhanGuruDanKelasSMP'
                //         },{
                //             text: "SMA",
                //             leaf: true,
                //             id: 'AnalisisKebutuhanGuruDanKelasSMA'
                //         }]
                //     },{
                //         text: "Prasarana",
                //         expanded: true,
                //         children:[{
                //             text: "SD",
                //             leaf: true,
                //             id: 'AnalisisKebutuhanPrasaranaSD'
                //         },{
                //             text: "SMP",
                //             leaf: true,
                //             id: 'AnalisisKebutuhanPrasaranaSMP'
                //         }/*,{
                //             text: "SMA",
                //             leaf: true,
                //             id: 'AnalisisKebutuhanPrasaranaSMA'
                //         }*/]
                //     }]
                // }
                ]
            }
        });

        this.items = [
            {
                xtype: 'treepanel',
                rootVisible: false,
                // bodyStyle: 'background:#FFFCDE',
                store: store,
                width:200,
                // region: 'center',
                listeners: {
                    itemclick: 'treeMenuItemClick'
                }
            }
        ];

        this.callParent();
    }
});