/**
 * This class is the main view for the application. It is specified in app.js as the
 * "autoCreateViewport" property. That setting automatically applies the "viewport"
 * plugin to promote that instance of this class to the body element.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('simdik_batam.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.MessageBox'
    ],
    
    routes : {
        'beranda': 'MenuBeranda',
        'menu/:id' : 'MenuRoute',
        'menu/DetailSekolah/:id': 'detailSekolahTab',
        'menu/DetailPtk/:id': 'detailPtkTab',
        'menu/DetailPesertaDidik/:id': 'detailPesertaDidikTab',
        'menu/MutasiPesertaDidik/:id': 'MutasiPesertaDidik',
        'menu/RekapSekolahRangkumanSp/:id_level_wilayah/:kode_wilayah': 'RekapSekolahRangkumanSp',
        'menu/RekapPdTingkatKelasJenisKelaminSp/:id_level_wilayah/:kode_wilayah': 'RekapPdTingkatKelasJenisKelaminSp'
    },

    alias: 'controller.main',

    // init: function() {
    //     this.control({
    //         'datapokoksekolahgrid':{
    //             afterrender: this.setupDataPokokSekolahGrid
    //         },
    //         'ptkgrid':{
    //             afterrender:this.setupPtkGrid
    //         }
    //     });
    //     // this.superclass.init.call(this);
    // },
    init:function(){
        Ext.override(Ext.Ajax, { timeout: 300000000 });
        Ext.override(Ext.data.Connection, { timeout: 300000000 });

        Ext.Ajax.timeout = 120000;

        Ext.setGlyphFontFamily('font-awesome');
        Ext.tip.QuickTipManager.init();
        Ext.state.Manager.setProvider(Ext.create('Ext.state.CookieProvider')); 

        Ext.ns('simdik_batam.global');

        Ext.Ajax.request({
            url: 'getDeskripsi',
            success: function(response){
                var text = response.responseText;
                // Ext.Msg.alert(text);

                var deskripsiStr = Ext.getCmp('deskripsiStr');

                deskripsiStr.update(text);

            }
        });

        Ext.Ajax.request({
            url: 'session',
            success: function(response){
                var text = response.responseText;
                
                var res = Ext.JSON.decode(response.responseText);

                console.log(res.forceLogin);                

                if(res.forceLogin ==  true){

                    if(res.session == true){
                        var headerPanel = Ext.getCmp('app-main').down('box');

                        var treePanel = Ext.getCmp('app-main').down('panel[region=west]').down('treemenu').down('treepanel');
                        
                        var hideMask = function () {
                            // Ext.get('loading').remove();
                            Ext.fly('loading-mask').animate({
                                opacity:0,
                                remove:true
                            });
                        };

                        Ext.defer(hideMask, 100);

                        // console.log(res.peran_id);
                        if(res.peran_id != 1){
                            treePanel.getStore().getNodeById('ManajemenPengguna').remove();    
                        }

                        if(res.theme == 'anegan-yellow'){
                            var bgColor = '#849400, #B6D700';
                            var color = 'white';
                        }else if(res.theme == 'anegan-blue'){
                            var bgColor = '#3892D3 , #74c3ff';
                            var color = 'white';
                        }else if(res.theme == 'anegan-green'){
                            var bgColor = '#e5cd04 , #91e400';
                            var color = '#434343';
                            // var bgColor = '#e6c800 , #ffec6f';
                        }

                        if(res.show_level == true){
                            if(res.level == 'dikmen'){
                                var level = 'Dikmen';
                            }else if(res.level == 'dikdas'){
                                var level = 'Dikdas';
                            }else{
                                var level = res.level;
                            }
                        }else{
                            var level = '';
                        }

                        if(res.labels == 'Profil'){
                            if(res.show_level == false){
                                var pendidikan = 'Pendidikan ';
                            }else{
                                var pendidikan = '';
                            }
                            
                        }else{
                            var pendidikan = '';
                        }

                        headerPanel.update('<div style="background: linear-gradient(to right, '+bgColor+');height:100%">' 
                            + '<div style="float:left;margin-top:5px;margin-left:10px">'
                                +'<img src="resources/img/logo.png?level=nasional" width="45px"/>'
                            +'</div>'
                            +'<div style="color:'+color+';float:left;margin:15px;font-size:35px;font-weight:none" class="ubuntu">'
                                // + 'Profil Dikdas Belitung'
                                + res.labels + ' ' + level + ' ' + res.kabkota
                            +'</div>'
                            +'<div style="color:'+color+';float:left;margin-top:25px;font-size:12px;font-weight:none" class="ubuntu">'
                                +'Versi 3.0.0'
                            +'</div>'
                            + '<div style="float:right;margin-top:5px;margin-right:10px">'
                                // +'<img src="resources/img/logo_dik.png" width="50px"/>'
                                +'<div style="color:white;font-weight:bold;font-size:16px;margin-top:20px">'+res.kode_wilayah_str+'</div>'
                            +'</div>'
                            +'<div style="clear:both"></div>'
                        +'</div>');

                        var welcomeStr = Ext.getCmp('welcomeStr');

                        welcomeStr.update('<div style="margin-top:20px;margin-left:10px;color:white" class="ubuntu">'
                                +'<div style="font-size:17px;">'
                                    +'Selamat Datang di'
                                +'</div>'
                                +'<div style="font-size:28px;margin-top:10px;line-height:30px">'
                                    + res.labels + ' ' + pendidikan + level + ' ' + res.kabkota
                                +'</div>'
                            +'</div>');

                        simdik_batam.global.level = res.level;
                        simdik_batam.global.show_login_button = res.show_login_button;

                        console.log(simdik_batam.global.show_login_button);

                        // if(res.peran_id == '10'){
                        //     Ext.getCmp('app-main').down('panel[region=west]').destroy();

                        // }
                    }else{
                        var hideMask = function () {
                            // Ext.get('loading').remove();
                            Ext.fly('loading-mask').animate({
                                opacity:0,
                                remove:true
                            });
                        };

                        Ext.defer(hideMask, 100);

                        Ext.getCmp('app-main').destroy();

                        Ext.create('simdik_batam.Login',{
                            record: res.level,
                            kabkota: res.kabkota,
                            labels: res.labels
                        });


                    }

                }else{

                    if(res.session == true){
                        var hideMask = function () {
                            // Ext.get('loading').remove();
                            Ext.fly('loading-mask').animate({
                                opacity:0,
                                remove:true
                            });
                        };

                        Ext.defer(hideMask, 100);

                        var headerPanel = Ext.getCmp('app-main').down('box');
                        var treePanel = Ext.getCmp('app-main').down('panel[region=west]').down('treemenu').down('treepanel');
                        // console.log(res.peran_id);
                        if(res.peran_id != 1){
                            treePanel.getStore().getNodeById('ManajemenPengguna').remove();    
                        }

                        if(res.theme == 'anegan-yellow'){
                            var bgColor = '#849400, #B6D700';
                            var color = 'white';
                        }else if(res.theme == 'anegan-blue'){
                            var bgColor = '#3892D3 , #74c3ff';
                            var color = 'white';
                        }else if(res.theme == 'anegan-green'){
                            var bgColor = '#e5cd04 , #91e400';
                            var color = '#434343';
                            // var bgColor = '#e6c800 , #ffec6f';
                        }

                        if(res.show_level == true){
                            if(res.level == 'dikmen'){
                                var level = 'Dikmen';
                            }else if(res.level == 'dikdas'){
                                var level = 'Dikdas';
                            }else{
                                var level = res.level;
                            }
                        }else{
                            var level = '';
                        }

                        if(res.labels == 'Profil'){
                            if(res.show_level == false){
                                var pendidikan = 'Pendidikan ';
                            }else{
                                var pendidikan = '';
                            }
                            
                        }else{
                            var pendidikan = '';
                        }

                        headerPanel.update('<div style="background: linear-gradient(to right, '+bgColor+');height:100%">' 
                            + '<div style="float:left;margin-top:5px;margin-left:10px">'
                                +'<img src="resources/img/logo.png?level=nasional" width="45px"/>'
                            +'</div>'
                            +'<div style="color:'+color+';float:left;margin:15px;font-size:35px;font-weight:none" class="ubuntu">'
                                // + 'Profil Dikdas Belitung'
                                + res.labels + ' ' + level + ' ' + res.kabkota
                            +'</div>'
                            +'<div style="color:'+color+';float:left;margin-top:25px;font-size:12px;font-weight:none" class="ubuntu">'
                                +'Versi 3.0.0'
                            +'</div>'
                            + '<div style="float:right;margin-top:5px;margin-right:10px">'
                                // +'<img src="resources/img/logo_dik.png" width="50px"/>'
                            +'</div>'
                            +'<div style="clear:both"></div>'
                        +'</div>');

                        var welcomeStr = Ext.getCmp('welcomeStr');

                        welcomeStr.update('<div style="margin-top:20px;margin-left:10px;color:white" class="ubuntu">'
                                +'<div style="font-size:17px;">'
                                    +'Selamat Datang di'
                                +'</div>'
                                +'<div style="font-size:28px;margin-top:10px;line-height:30px">'
                                    + res.labels + ' ' + pendidikan + level + ' ' + res.kabkota
                                +'</div>'
                            +'</div>');

                        simdik_batam.global.level = res.level;

                    }else{

                        var hideMask = function () {
                            // Ext.get('loading').remove();
                            Ext.fly('loading-mask').animate({
                                opacity:0,
                                remove:true
                            });
                        };

                        Ext.defer(hideMask, 100);

                        var headerPanel = Ext.getCmp('app-main').down('box');

                        if(res.theme == 'anegan-yellow'){
                            var bgColor = '#849400, #B6D700';
                            var color = 'white';
                        }else if(res.theme == 'anegan-blue'){
                            var bgColor = '#3892D3 , #74c3ff';
                            var color = 'white';
                        }else if(res.theme == 'anegan-green'){
                            var bgColor = '#e5cd04 , #91e400';
                            var color = '#434343';
                            // var bgColor = '#e6c800 , #ffec6f';
                        }

                        if(res.show_level == true){
                            if(res.level == 'dikmen'){
                                var level = 'Dikmen';
                            }else if(res.level == 'dikdas'){
                                var level = 'Dikdas';
                            }else{
                                var level = res.level;
                            }
                        }else{
                            var level = '';
                        }

                        if(res.labels == 'Profil'){
                            if(res.show_level == false){
                                var pendidikan = 'Pendidikan ';
                            }else{
                                var pendidikan = '';
                            }
                            
                        }else{
                            var pendidikan = '';
                        }

                        headerPanel.update('<div style="background: linear-gradient(to right, '+bgColor+');height:100%">' 
                            + '<div style="float:left;margin-top:5px;margin-left:10px">'
                                +'<img src="resources/img/logo.png?level=nasional" width="45px"/>'
                            +'</div>'
                            +'<div style="color:'+color+';float:left;margin:15px;font-size:35px;font-weight:none" class="ubuntu">'
                                // + 'Profil Dikdas Belitung'
                                + res.labels + ' ' + level + ' ' + res.kabkota
                            +'</div>'
                            +'<div style="color:'+color+';float:left;margin-top:25px;font-size:12px;font-weight:none" class="ubuntu">'
                                +'Versi 3.0.0'
                            +'</div>'
                            + '<div style="float:right;margin-top:5px;margin-right:10px">'
                                // +'<img src="resources/img/logo_dik.png" width="50px"/>'
                            +'</div>'
                            +'<div style="clear:both"></div>'
                        +'</div>');

                        var welcomeStr = Ext.getCmp('welcomeStr');

                        welcomeStr.update('<div style="margin-top:20px;margin-left:10px;color:white" class="ubuntu">'
                                +'<div style="font-size:17px;">'
                                    +'Selamat Datang di'
                                +'</div>'
                                +'<div style="font-size:28px;margin-top:10px;line-height:30px">'
                                    + res.labels + ' ' + pendidikan + level + ' ' + res.kabkota
                                +'</div>'
                            +'</div>');

                        simdik_batam.global.level = res.level;

                        var panelWest = Ext.getCmp('app-main').down('panel[region=west]');

                        panelWest.down('treemenu').destroy();

                        panelWest.add({
                            xtype:'treemenunonlogin',
                            region:'center',
                            width:200
                        });

                        var tabPanel = Ext.getCmp('app-main').down('tabpanel[region=center]');

                        var toolbarLogin = tabPanel.down('berandapanel').down('panel[itemId=panelkiri]').down('panel[id=panelatas2]').down('toolbar');

                        toolbarLogin.down('button[itemId=logout]').hide();
                        toolbarLogin.down('button[itemId=profilpengguna]').hide();
                        toolbarLogin.down('button[itemId=GantiPassword]').hide();
                        toolbarLogin.add({
                            text:'Login',
                            glyph: 61447,
                            handler:'NonForceLogin'
                        });

                        if(res.show_login_button == true){

                            tabPanel.down('berandapanel').down('panel[itemId=panelkiri]').down('panel[id=panelatas2]').update('<div style="border:0px solid #ccc;width:70%;float:left;padding:10px 10px 10px 10px">'                 
                                      + '<font size="4pt"><b>Selamat Datang!</b></font><br>'
                                      + 'Silakan Login untuk menggunakan fitur-fitur aplikasi ini dengan lengkap<br>'
                                      + '</div>'
                                      );
                        }else{
                            tabPanel.down('berandapanel').down('panel[itemId=panelkiri]').down('panel[id=panelatas2]').update('<div style="border:0px solid #ccc;width:70%;float:left;padding:10px 10px 10px 10px">'                 
                                      + '<font size="4pt"><b>Selamat Datang!</b></font><br>'
                                      + 'Untuk memilih menu yang tersedia, silakan klik salah satu baris dari menu di sebelah kiri aplikasi<br>'
                                      + '</div>'
                                      );
                            tabPanel.down('berandapanel').down('panel[itemId=panelkanan]').down('panel[id=panelatas2]').down('toolbar').hide();
                        }
                    }                    

                }

                
            }
        });
    },
    detailSekolahTab: function(id){
        
        // Ext.Msg.alert(id);
        var tabPanel = Ext.getCmp('ContentPanel');
        var panelId = 'DetailSekolah_' + id;
        var tab = tabPanel.getComponent(panelId);
        
        if (!tab) {
            Ext.MessageBox.show({
                msg: 'Memuat Data ...',
                progressText: 'Memuat Data ...',
                width: 200,
                wait: true
            });

            var store = Ext.create('simdik_batam.store.Sekolah');

            store.load({
                params:{
                    sekolah_id: id,
                    keyword: null,
                    prop: null,
                    kab: null,
                    kec:null,
                    bp:null,
                    ss:null
                }
            });

            store.on('load',function(store, records, options){
                
                    var rec = store.findRecord('sekolah_id', id);

                    if(rec){

                        // tabPanel.setTitle('tes');

                        var judul = 'Detail ' + rec.data.nama_sekolah;

                        
                        var classname = 'simdik_batam.view.DataPokokSekolah.DetailSekolah';

                        tabPanel.add(Ext.create(classname, {
                            // title: record.parentNode.get('text') + ' - ' + record.get('text'),
                            title: judul,
                            closable:true,
                            glyph: 61687,
                            id: panelId,
                            record: rec
                        }));

                        tabPanel.setActiveTab(panelId);

                        Ext.MessageBox.hide();

                    }
            }); 


        }else{
            tabPanel.setActiveTab(tab);
        }

    },

    detailPtkTab: function(id){
        var tabPanel = Ext.getCmp('ContentPanel');
        var panelId = 'DetailPtk_' + id;
        var tab = tabPanel.getComponent(panelId);
        
        if (!tab) {
            Ext.MessageBox.show({
                msg: 'Memuat Data ...',
                progressText: 'Memuat Data ...',
                width: 200,
                wait: true
            });

            var store = Ext.create('simdik_batam.store.DataPokokPtk');

            store.load({
                params:{
                    ptk_id: id,
                    keyword: null,
                    prop:null,
                    kab:null,
                    kec:null,
                    bp_id:null,
                    ss:null,
                    jurusan_id:null,
                    jenis_ptk_id:null,
                    semester_id:null
                }
            });

            store.on('load',function(store, records, options){
                
                    var rec = store.findRecord('ptk_id', id);

                    if(rec){

                        // tabPanel.setTitle('tes');

                        var judul = 'Detail PTK: ' + rec.data.nama;

                        
                        var classname = 'simdik_batam.view.DataPokokPtk.DetailPtk';

                        tabPanel.add(Ext.create(classname, {
                            // title: record.parentNode.get('text') + ' - ' + record.get('text'),
                            title: judul,
                            closable:true,
                            glyph: 61447,
                            id: panelId,
                            record: rec
                        }));

                        tabPanel.setActiveTab(panelId);

                        Ext.MessageBox.hide();

                    }else{
                        Ext.Msg.alert('Error','Data PTK tidak ditemukan atau ada kesalahan dalam sistem. Mohon Coba sekali lagi');
                    }
            }); 
        }else{
            tabPanel.setActiveTab(tab);
        }
    },
    RekapSekolahRangkumanSp:function(id_level_wilayah, kode_wilayah){
        var tabPanel = Ext.getCmp('ContentPanel');
        var panelId = 'RekapSekolahRangkumanSp_' + id_level_wilayah + '_' + kode_wilayah;
        var tab = tabPanel.getComponent(panelId);

        if (!tab) {
            Ext.MessageBox.show({
                msg: 'Memuat Data ...',
                progressText: 'Memuat Data ...',
                width: 200,
                wait: true
            });

            var storeWil = Ext.create('simdik_batam.store.MstWilayah');

            storeWil.load({
                params:{
                    kode_wilayah: kode_wilayah,
                    mode:null
                }
            });

            storeWil.on('load',function(store, records, options){
    
                // var rec = store.findRecord('kode_wilayah', kode_wilayah);

                var judul = 'Rekap Sekolah di Wilayah ' + store.getAt(0).data.nama_render;

                var classname = 'simdik_batam.view.RekapSekolahRangkumanSp.RekapSekolahRangkumanSp';

                tabPanel.add(Ext.create(classname, {
                    title: judul,
                    closable:true,
                    glyph: 61447,
                    id: panelId,
                    items:[{
                        xtype: 'sekolahrangkumanspgrid',
                        region:'center',
                        kodeWilayah: kode_wilayah,
                        idLevelWilayah: id_level_wilayah
                    }]
                }));

                tabPanel.setActiveTab(panelId);

                Ext.MessageBox.hide();
                
            });

        }else{
            tabPanel.setActiveTab(tab);
        }
        
    },
    RekapPdTingkatKelasJenisKelaminSp:function(id_level_wilayah, kode_wilayah){
        var tabPanel = Ext.getCmp('ContentPanel');
        var panelId = 'RekapPdTingkatKelasJenisKelaminSp_' + id_level_wilayah + '_' + kode_wilayah;
        var tab = tabPanel.getComponent(panelId);

        if (!tab) {
            Ext.MessageBox.show({
                msg: 'Memuat Data ...',
                progressText: 'Memuat Data ...',
                width: 200,
                wait: true
            });

            var storeWil = Ext.create('simdik_batam.store.MstWilayah');

            storeWil.load({
                params:{
                    kode_wilayah: kode_wilayah,
                    mode:null
                }
            });

            storeWil.on('load',function(store, records, options){
    
                // var rec = store.findRecord('kode_wilayah', kode_wilayah);

                var judul = 'Rekap Peserta Didik Per Tingkat di Wilayah ' + store.getAt(0).data.nama_render;

                var classname = 'simdik_batam.view.RekapPdTingkatKelasJenisKelaminSp.RekapPdTingkatKelasJenisKelaminSp';

                tabPanel.add(Ext.create(classname, {
                    title: judul,
                    closable:true,
                    glyph: 61447,
                    id: panelId,
                    items:[{
                        xtype: 'pdtingkatkelasjeniskelaminspgrid',
                        region:'center',
                        kodeWilayah: kode_wilayah,
                        idLevelWilayah: id_level_wilayah
                    }]
                }));

                tabPanel.setActiveTab(panelId);

                Ext.MessageBox.hide();
                
            });

        }else{
            tabPanel.setActiveTab(tab);
        }
        
    },
    MutasiPesertaDidik:function(id){
        var tabPanel = Ext.getCmp('ContentPanel');
        var panelId = 'MutasiPesertaDidik_' + id;
        var tab = tabPanel.getComponent(panelId);
        
        if (!tab) {
            Ext.MessageBox.show({
                msg: 'Memuat Data ...',
                progressText: 'Memuat Data ...',
                width: 200,
                wait: true
            });

            var storeMutasi = Ext.create('simdik_batam.store.Mutasi');

            storeMutasi.load({
                params:{
                    mutasi_id: id
                }
            });

            storeMutasi.on('load',function(store, records, options){
    
                var rec = store.findRecord('mutasi_id', id);

                if(rec){

                    var judul = 'Edit Mutasi PD: ' + rec.data.nama;

                    var classname = 'simdik_batam.view.Mutasi.MutasiPesertaDidik';

                    tabPanel.add(Ext.create(classname, {
                        title: judul,
                        closable:true,
                        glyph: 61447,
                        id: panelId,
                        record: rec
                    }));

                    tabPanel.setActiveTab(panelId);

                    Ext.MessageBox.hide();

                }else{

                    var store = Ext.create('simdik_batam.store.DataPokokPesertaDidik');

                    store.load({
                        params:{
                            peserta_didik_id: id,
                            modul: 'mutasi',
                            keyword:null
                        }
                    });
                    
                    store.on('load',function(store, records, options){
            
                        var rec = store.findRecord('peserta_didik_id', id);

                        if(rec){

                            var judul = 'Tambah Mutasi PD: ' + rec.data.nama;

                            var classname = 'simdik_batam.view.Mutasi.MutasiPesertaDidik';

                            tabPanel.add(Ext.create(classname, {
                                title: judul,
                                closable:true,
                                glyph: 61447,
                                id: panelId,
                                record: rec
                            }));

                            tabPanel.setActiveTab(panelId);

                            Ext.MessageBox.hide();
                        }else{
                            Ext.Msg.alert('Error','Data Peserta Didik tidak ditemukan atau ada kesalahan dalam sistem. Mohon Coba sekali lagi');
                        }
                    
                    });
                }
            });

            
        }else{
            tabPanel.setActiveTab(tab);
        }
    },

    detailPesertaDidikTab: function(id){
        var tabPanel = Ext.getCmp('ContentPanel');
        var panelId = 'DetailPesertaDidik_' + id;
        var tab = tabPanel.getComponent(panelId);
        
        if (!tab) {
            Ext.MessageBox.show({
                msg: 'Memuat Data ...',
                progressText: 'Memuat Data ...',
                width: 200,
                wait: true
            });

            var store = Ext.create('simdik_batam.store.DataPokokPesertaDidik');

            store.load({
                params:{
                    peserta_didik_id: id,
                    modul: 'form',
                    keyword:null,
                    prop:null,
                    kab:null,
                    kec:null,
                    bp_id:null,
                    ss:null
                }
            });

            store.on('load',function(store, records, options){
                
                    var rec = store.findRecord('peserta_didik_id', id);

                    if(rec){

                        // tabPanel.setTitle('tes');

                        var judul = 'Detail PD: ' + rec.data.nama;

                        
                        var classname = 'simdik_batam.view.DataPokokPesertaDidik.DetailPesertaDidik';

                        tabPanel.add(Ext.create(classname, {
                            // title: record.parentNode.get('text') + ' - ' + record.get('text'),
                            title: judul,
                            closable:true,
                            glyph: 61447,
                            id: panelId,
                            record: rec
                        }));

                        tabPanel.setActiveTab(panelId);

                        Ext.MessageBox.hide();

                    }else{
                        Ext.Msg.alert('Error','Data Peserta Didik tidak ditemukan atau ada kesalahan dalam sistem. Mohon Coba sekali lagi');
                    }
            }); 
        }else{
            tabPanel.setActiveTab(tab);
        }
    },

    setupDataPokokPtkGrid:function(grid){
        grid.down('toolbar').add({
            xtype:'semestercombo',
            emptyText:'Pilih Semester...',
            listeners: {
                change: function(combo, newValue, oldValue, eOpts ) {
                    var grid = combo.up('gridpanel');
                    
                    grid.getStore().reload();
                }
            }
        },{
            text: 'Export Xls',
            glyph: 61474,
            handler:function(btn){
                var keyword = grid.up('panel').down('filterptkform').down('textfield[name=keyword]').getValue();
                var prop = grid.up('panel').down('filterptkform').down('mstwilayahcombo[name=combopropmasterdataptk]').getValue();
                var kab = grid.up('panel').down('filterptkform').down('mstwilayahcombo[name=combokabmasterdataptk]').getValue();
                var kec = grid.up('panel').down('filterptkform').down('mstwilayahcombo[name=combokecmasterdataptk]').getValue();
                var bp_id = grid.up('panel').down('filterptkform').down('bentukpendidikancombo').getValue();
                var ss = grid.up('panel').down('filterptkform').down('combobox[name=pilihstatuscombo]').getValue();

                var jurusan_id = grid.up('panel').down('filterptkform').down('fieldset').down('jurusancombo').getValue();
                var jenis_ptk_id = grid.up('panel').down('filterptkform').down('fieldset').down('jenisptkcombo').getValue();

                var semester_id = grid.down('toolbar').down('semestercombo').getValue();

                window.location = 'excel/DataPokokPtk?keyword='+keyword+'&prop='+prop+'&kab='+kab+'&kec='+kec+'&bp_id='+bp_id+'&ss='+ss+'&jurusan_id='+jurusan_id+'&jenis_ptk_id='+jenis_ptk_id+'&semester_id='+semester_id;
            }
        });

        grid.getStore().on('beforeload', function(store){
            var keyword = grid.up('panel').down('filterptkform').down('textfield[name=keyword]').getValue();
            var semester_id = grid.down('toolbar').down('semestercombo').getValue();
            var semester_id_str = grid.down('toolbar').down('semestercombo').getRawValue();

            var prop = grid.up('panel').down('filterptkform').down('mstwilayahcombo[name=combopropmasterdataptk]').getValue();
            var kab = grid.up('panel').down('filterptkform').down('mstwilayahcombo[name=combokabmasterdataptk]').getValue();
            var kec = grid.up('panel').down('filterptkform').down('mstwilayahcombo[name=combokecmasterdataptk]').getValue();
            var bp_id = grid.up('panel').down('filterptkform').down('bentukpendidikancombo').getValue();
            var ss = grid.up('panel').down('filterptkform').down('combobox[name=pilihstatuscombo]').getValue();

            var jurusan_id = grid.up('panel').down('filterptkform').down('fieldset[itemId=filter_tambahan]').down('jurusancombo').getValue();
            var jenis_ptk_id = grid.up('panel').down('filterptkform').down('fieldset[itemId=filter_tambahan]').down('jenisptkcombo').getValue();

            // sekolah_id.setTitle('tes');
            if(!semester_id){
                grid.setTitle('Data Pokok PTK Semester 2014/2015 Genap');
            }else{
                grid.setTitle('Data Pokok PTK Semester ' + semester_id_str);
            }
            
            Ext.apply(store.proxy.extraParams, {
                keyword: keyword,
                semester_id:semester_id,
                prop:prop,
                kab:kab,
                kec:kec,
                bp_id:bp_id,
                ss:ss,
                jurusan_id:jurusan_id,
                jenis_ptk_id:jenis_ptk_id
            });
        });

        // grid.getStore().load();
    },

    setupFilterPtkForm: function(form){
        var comboProp = form.down('mstwilayahcombo[name=combopropmasterdataptk]');
        var comboKab = form.down('mstwilayahcombo[name=combokabmasterdataptk]');
        var comboKec = form.down('mstwilayahcombo[name=combokecmasterdataptk]');

        comboProp.getStore().on('beforeload', function(store){
            Ext.apply(store.proxy.extraParams, {
                mode: 'propinsi'
            });
        });

        comboKab.getStore().on('beforeload', function(store){
            var propValue = comboProp.getValue();

            Ext.apply(store.proxy.extraParams, {
                mode: 'kabupaten',
                kode_wilayah: propValue
            });
        });

        comboKec.getStore().on('beforeload', function(store){
            var kabValue = comboKab.getValue();

            Ext.apply(store.proxy.extraParams, {
                mode: 'kecamatan',
                kode_wilayah: kabValue
            });
        });

        Ext.Ajax.request({
            url: 'session',
            success: function(response){
                var text = response.responseText;
                
                var res = Ext.JSON.decode(response.responseText);

                if(res.id_level_wilayah == 1){
                    comboProp.setValue(res.kode_wilayah);
                    comboProp.hide();
                }else if(res.id_level_wilayah == 2){
                    comboProp.setValue(res.mst_kode_wilayah);
                    comboProp.hide();
                    comboKab.setValue(res.kode_wilayah);
                    comboKab.hide();
                }
            }
        });
    },

    setupFilterPdForm: function(form){
        var comboProp = form.down('mstwilayahcombo[name=combopropmasterdatapd]');
        var comboKab = form.down('mstwilayahcombo[name=combokabmasterdatapd]');
        var comboKec = form.down('mstwilayahcombo[name=combokecmasterdatapd]');

        comboProp.getStore().on('beforeload', function(store){
            Ext.apply(store.proxy.extraParams, {
                mode: 'propinsi'
            });
        });

        comboKab.getStore().on('beforeload', function(store){
            var propValue = comboProp.getValue();

            Ext.apply(store.proxy.extraParams, {
                mode: 'kabupaten',
                kode_wilayah: propValue
            });
        });

        comboKec.getStore().on('beforeload', function(store){
            var kabValue = comboKab.getValue();

            Ext.apply(store.proxy.extraParams, {
                mode: 'kecamatan',
                kode_wilayah: kabValue
            });
        });

        Ext.Ajax.request({
            url: 'session',
            success: function(response){
                var text = response.responseText;
                
                var res = Ext.JSON.decode(response.responseText);

                if(res.id_level_wilayah == 1){
                    comboProp.setValue(res.kode_wilayah);
                    comboProp.hide();
                }else if(res.id_level_wilayah == 2){
                    comboProp.setValue(res.mst_kode_wilayah);
                    comboProp.hide();
                    comboKab.setValue(res.kode_wilayah);
                    comboKab.hide();
                }
            }
        });
    },

    setupDataPokokPesertaDidikGrid:function(grid){
        grid.down('toolbar').add({
            text: 'Detail',                
            glyph: 61485,
            handler: 'DetailPesertaDidik' 
        },{
            xtype:'semestercombo',
            emptyText:'Pilih Semester...',
            listeners: {
                change: function(combo, newValue, oldValue, eOpts ) {
                    var grid = combo.up('gridpanel');
                    
                    grid.getStore().reload();
                }
            }
        });

        grid.getStore().on('beforeload', function(store){
            var keyword = grid.up('panel').down('filterpesertadidikform').down('textfield[name=keyword]').getValue();
            
            var semester_id = grid.down('toolbar').down('semestercombo').getValue();
            var semester_id_str = grid.down('toolbar').down('semestercombo').getRawValue();

            var prop = grid.up('panel').down('filterpesertadidikform').down('mstwilayahcombo[name=combopropmasterdatapd]').getValue();
            var kab = grid.up('panel').down('filterpesertadidikform').down('mstwilayahcombo[name=combokabmasterdatapd]').getValue();
            var kec = grid.up('panel').down('filterpesertadidikform').down('mstwilayahcombo[name=combokecmasterdatapd]').getValue();
            var bp_id = grid.up('panel').down('filterpesertadidikform').down('bentukpendidikancombo').getValue();
            var ss = grid.up('panel').down('filterpesertadidikform').down('combobox[name=pilihstatuscombo]').getValue();

            if(!semester_id){
                grid.setTitle('Data Pokok Peserta Didik Tahun Ajaran 2014/2015 Genap');
            }else{
                grid.setTitle('Data Pokok Peserta Didik Tahun Ajaran ' + semester_id_str);
            }
            
            Ext.apply(store.proxy.extraParams, {
                keyword: keyword,
                semester_id:semester_id,
                prop:prop,
                kab:kab,
                kec:kec,
                bp_id:bp_id,
                ss:ss
            });
        });


        // grid.getStore().load();
    },

    setupPtkGrid:function(grid){
        
        grid.down('toolbar').add({
            xtype:'tahunajarancombo',
            emptyText:'Pilih Tahun Ajaran...',
            listeners: {
                change: function(combo, newValue, oldValue, eOpts ) {
                    var grid = combo.up('gridpanel');
                    
                    grid.getStore().reload();
                }
            }
        },{
            text: 'Export XXls',
            glyph: 61474,
            handler:function(btn){
                var sekolah_id = btn.up('gridpanel').up('panel').down('sekolahform').down('hidden[name=sekolah_id]').getValue();
                var tahun_ajaran_id = btn.up('toolbar').down('tahunajarancombo').getValue();

                window.location = 'excel/SekolahPtk?sekolah_id='+sekolah_id+'&tahun_ajaran_id='+tahun_ajaran_id;
            }
        });

        grid.getStore().on('beforeload', function(store){
            var sekolah_id = grid.up('panel').down('sekolahform').down('hidden[name=sekolah_id]').getValue();
            var tahun_ajaran_id = grid.down('toolbar').down('tahunajarancombo').getValue();

            Ext.apply(store.proxy.extraParams, {
                sekolah_id: sekolah_id,
                tahun_ajaran_id: tahun_ajaran_id
            });
        });

        grid.getStore().load();
    },

    setupPesertaDidikGrid:function (grid) {
        grid.down('toolbar').add({
            text: 'Detail',                
            glyph: 61485,
            handler: 'DetailPesertaDidik' 
        },{
            xtype:'semestercombo',
            emptyText:'Pilih Semester...',
            listeners: {
                change: function(combo, newValue, oldValue, eOpts ) {
                    var grid = combo.up('gridpanel');
                    
                    grid.getStore().reload();
                }
            }
        });

        grid.getStore().on('beforeload', function(store){
            var sekolah_id = grid.up('panel').down('sekolahform').down('hidden[name=sekolah_id]').getValue();
            var semester_id = grid.down('toolbar').down('semestercombo').getValue();

            // sekolah_id.setTitle('tes');

            Ext.apply(store.proxy.extraParams, {
                sekolah_id: sekolah_id,
                semester_id: semester_id
            });
        });

        grid.getStore().load();
    },

    DetailSekolah:function(btn){
        var grid = btn.up('gridpanel');

        var rec = grid.getSelectionModel().getSelection()[0];

        if(!rec){
            Ext.Msg.alert('Pilih salah satu baris terlebih dahulu!');
        }else{
            this.redirectTo('menu/DetailSekolah/'+ rec.data.sekolah_id);
            // this.redirectTo('detail/Sekolah/params');
        }
    },

    DetailPtk:function(btn){
        var grid = btn.up('gridpanel');

        var rec = grid.getSelectionModel().getSelection()[0];

        if(!rec){
            Ext.Msg.alert('Pilih salah satu baris terlebih dahulu!');
        }else{
            this.redirectTo('menu/DetailPtk/'+ rec.data.ptk_id);
            
            // var tabPanel = Ext.getCmp('ContentPanel');

            // // tabPanel.setTitle('tes');

            // var panelId = 'DetailPtk-' + rec.data.ptk_id;

            // var judul = 'Detail PTK: ' + rec.data.nama;

            // var tab = tabPanel.getComponent(panelId);
            // if (!tab) {
            //     var classname = 'simdik_batam.view.DataPokokPtk.DetailPtk';

            //     tabPanel.add(Ext.create(classname, {
            //         // title: record.parentNode.get('text') + ' - ' + record.get('text'),
            //         title: judul,
            //         closable:true,
            //         id: panelId,
            //         record: rec
            //     }));

            //     tabPanel.setActiveTab(panelId);
            // }else{
            //     tabPanel.setActiveTab(tab);
            // }

        }
    },

    DetailPesertaDidik:function(btn){
        var grid = btn.up('gridpanel');

        var rec = grid.getSelectionModel().getSelection()[0];

        if(!rec){
            Ext.Msg.alert('Pilih salah satu baris terlebih dahulu!');
        }else{
            this.redirectTo('menu/DetailPesertaDidik/'+ rec.data.peserta_didik_id);
            // this.redirectTo('detail/Sekolah/params');

            // var tabPanel = Ext.getCmp('ContentPanel');

            // // tabPanel.setTitle('tes');

            // var panelId = 'DetailPesertaDidik-' + rec.data.peserta_didik_id;

            // var judul = 'Detail PD: ' + rec.data.nama;

            // var tab = tabPanel.getComponent(panelId);
            // if (!tab) {
            //     var classname = 'simdik_batam.view.DataPokokPesertaDidik.DetailPesertaDidik';

            //     tabPanel.add(Ext.create(classname, {
            //         // title: record.parentNode.get('text') + ' - ' + record.get('text'),
            //         title: judul,
            //         closable:true,
            //         id: panelId,
            //         record: rec
            //     }));

            //     tabPanel.setActiveTab(panelId);
            // }else{
            //     tabPanel.setActiveTab(tab);
            // }

        }
    },
    setupFilterSekolahFormBefore:function(form){
        var comboProp = form.down('mstwilayahcombo[name=combopropmasterdatasekolah]');
        var comboKab = form.down('mstwilayahcombo[name=combokabmasterdatasekolah]');
        var comboKec = form.down('mstwilayahcombo[name=combokecmasterdatasekolah]');

        Ext.Ajax.request({
            url: 'session',
            success: function(response){
                var text = response.responseText;
                
                var res = Ext.JSON.decode(response.responseText);

                if(res.id_level_wilayah == 1){
                    comboProp.setValue(res.kode_wilayah);
                    comboProp.hide();
                }else if(res.id_level_wilayah == 2){
                    comboProp.setValue(res.mst_kode_wilayah);
                    comboProp.hide();
                    comboKab.setValue(res.kode_wilayah);
                    comboKab.hide();
                }
            }
        });
    },
    setupFilterSekolahForm:function(form){
        var comboProp = form.down('mstwilayahcombo[name=combopropmasterdatasekolah]');
        var comboKab = form.down('mstwilayahcombo[name=combokabmasterdatasekolah]');
        var comboKec = form.down('mstwilayahcombo[name=combokecmasterdatasekolah]');

        comboProp.getStore().on('beforeload', function(store){
            Ext.apply(store.proxy.extraParams, {
                mode: 'propinsi'
            });
        });

        comboKab.getStore().on('beforeload', function(store){
            var propValue = comboProp.getValue();

            Ext.apply(store.proxy.extraParams, {
                mode: 'kabupaten',
                kode_wilayah: propValue
            });
        });

        comboKec.getStore().on('beforeload', function(store){
            var kabValue = comboKab.getValue();

            Ext.apply(store.proxy.extraParams, {
                mode: 'kecamatan',
                kode_wilayah: kabValue
            });
        });
    },
    setupDataPokokSekolahGrid: function(grid){
        console.log(simdik_batam.global.level);

        grid.getStore().on('beforeload', function(store){
            var keyword = grid.up('panel').down('filtersekolahform').down('textfield[name=keyword]').getValue();
            var prop = grid.up('panel').down('filtersekolahform').down('mstwilayahcombo[name=combopropmasterdatasekolah]').getValue();
            var kab = grid.up('panel').down('filtersekolahform').down('mstwilayahcombo[name=combokabmasterdatasekolah]').getValue();
            var kec = grid.up('panel').down('filtersekolahform').down('mstwilayahcombo[name=combokecmasterdatasekolah]').getValue();
            var bp = grid.up('panel').down('filtersekolahform').down('combobox[name="pilihbentukcombo"]').getValue();
            var ss = grid.up('panel').down('filtersekolahform').down('combobox[name="pilihstatuscombo"]').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                keyword: keyword,
                prop: prop,
                kab: kab,
                kec: kec,
                bp: bp,
                ss: ss
            });
        });

        // grid.down('toolbar').add({
        //     text:'Export Xls',
        //     glyph: 61646,
        //     handler: function(btn){
        //         var grid = btn.up('gridpanel');

        //         var rec = grid.getSelectionModel().getSelection()[0];

        //         if(!rec){
        //             Ext.Msg.alert('Peringatan','Silakan Pilih Baris Terlebih Dahulu!');
        //         }else{

        //         }
        //     }
        // });

        Ext.Ajax.request({
            url: 'session',
            success: function(response){
                var text = response.responseText;
                
                var res = Ext.JSON.decode(response.responseText);

                if(res.peran_id == 1){
                    grid.down('toolbar').add({
                        text:'Kode Registrasi',
                        glyph: 61646,
                        handler: function(btn){
                            var grid = btn.up('gridpanel');

                            var rec = grid.getSelectionModel().getSelection()[0];

                            if(!rec){
                                Ext.Msg.alert('Peringatan','Silakan Pilih Baris Terlebih Dahulu!');
                            }else{

                                var window = Ext.create('simdik_batam.view.DataPokokSekolah.KoregWindow',{
                                    title: 'Kode Registrasi ' + rec.data.nama_sekolah,
                                    modal:true
                                });

                                var panel = window.down('panel');

                                panel.update('<div style="width:100%;text-align:center;padding-top:20px">' + 
                                                rec.data.nama_sekolah + '<br>' +
                                                rec.data.npsn + '<br>' +
                                                rec.data.alamat_jalan + '<br>' +
                                                '<div style="font-size:30px;font-weight:bold;padding-top:20px">'+
                                                rec.data.koreggen +
                                                '</div>' +
                                            '</div>');

                                if(rec.data.koreggen == 0){

                                    window.down('toolbar').add({
                                        itemId:'genKoregBtn',
                                        text:'Generate Koreg',
                                        handler: function(btn){

                                            Ext.Ajax.request({
                                                waitMsg : 'Generate Koreg...',
                                                url     : 'genKoreg',
                                                method  : 'POST',
                                                params  : {
                                                    id : rec.data.sekolah_id
                                                },
                                                failure: function(response, options){
                                                    Ext.Msg.alert('Peringatan','Ada Kesalahan dalam sistem. mohon dicoba beberapa saat lagi');
                                                },
                                                success: function(response, options){
                                                    var json = Ext.JSON.decode(response.responseText);
                                                    
                                                    if(json.success == true){
                                                        Ext.Msg.alert('Berhasil','Berhasil generate koreg');

                                                        panel.update('<div style="width:100%;text-align:center;padding-top:20px">' + 
                                                                        rec.data.nama_sekolah + '<br>' +
                                                                        rec.data.npsn + '<br>' +
                                                                        rec.data.alamat_jalan + '<br>' +
                                                                        '<div style="font-size:30px;font-weight:bold;padding-top:20px">'+
                                                                        json.koreg +
                                                                        '</div>' +
                                                                    '</div>');

                                                        rec.data.koreggen = json.koreg;

                                                        window.down('toolbar').down('button[itemId=genKoregBtn]').hide();

                                                    } else if(json.success == false) {

                                                        Ext.Msg.alert('Error','Gagal generate koreg. Silakan coba beberapa saat lagi');
                                                        
                                                    }
                                                }
                                            });

                                        }
                                    });
                                }

                                
                                window.show();
                            }
                        }
                    });
                }
            }
        });

        // grid.getStore().load();
    },
    cari:function(btn){
        var grid = btn.up('form').up('panel').down('gridpanel');

        grid.getStore().reload();
    },

    // DetailTab:function(id,params){
    //     if(id == 'DetailSekolah'){
    //         Ext.Msg.alert('yes');
    //     }
    // },

    MenuBeranda: function(){
        var tabPanel = Ext.getCmp('app-main').down('tabpanel');

        tabPanel.setActiveTab('Beranda');
    },
    
    judulTab: function(id){
        var judul = id.match(/[A-Z]?[a-z]+|[0-9]+/g);
        var strJudul = '';

        for (var i = 0; i <= judul.length - 1 ; i++) {
            strJudul += judul[i] + " ";
        };

        return strJudul;
    },

    MenuRoute: function(id){

        var tabPanel = Ext.getCmp('ContentPanel');

        // tabPanel.setTitle('tes');

        var panelId = id;

        var judul = this.judulTab(id);

        var tab = tabPanel.getComponent(panelId);
        if (!tab) {
            var classname = 'simdik_batam.view.' + id + '.' + id;

            tabPanel.add(Ext.create(classname, {
                // title: record.parentNode.get('text') + ' - ' + record.get('text'),
                title: judul,
                glyph: 61498,
                closable:true,
                id: panelId
            }));

            tabPanel.setActiveTab(panelId);
        }else{
            tabPanel.setActiveTab(tab);
        }
    },

    treeMenuItemClick: function(tree,record) {
        var leaf = record.data.leaf;
        var id = record.data.id;

        if(leaf == true){
            // Ext.Msg.alert('yes!');
            if(id == 'Beranda'){
                this.redirectTo('beranda', true);
            }else{
                this.redirectTo('menu/'+ id, true);    
            }

                            
        }
    },
    onTabChange:function(tabs, newTab, oldTab){
        // Ext.suspendLayouts();

        var id = newTab.id;

        console.log(id);

        if(id != 'Beranda'){
            var route = id.replace("_","/");
            route = route.replace("_","/");
            this.redirectTo('menu/' + route, false);

        }else{
            this.redirectTo('beranda');
        }

        // Ext.resumeLayouts(true);
    },

    // onClickButton: function () {
    //     Ext.Msg.confirm('Confirm', 'Are you sure?', 'onConfirm', this);
    // },

    // onConfirm: function (choice) {
    //     if (choice === 'yes') {
    //         //
    //     }
    // }

    setupPtkTerdaftarGrid: function(grid){
        
        grid.getStore().on('beforeload', function(store){
            var rec = grid.up('tabpanel').down('ptkform').getForm().getRecord();

            Ext.apply(store.proxy.extraParams, {
                ptk_id: rec.data.ptk_id
            });
        });

        grid.getStore().load();
    },
    setupPembelajaranGrid: function(grid){
        grid.getStore().on('beforeload', function(store){
            var rec = grid.up('tabpanel').down('ptkform').getForm().getRecord();

            Ext.apply(store.proxy.extraParams, {
                ptk_id: rec.data.ptk_id
            });
        });
        
        grid.getStore().load();
    },
    setupAnggotaRombelGrid: function(grid){
        grid.getStore().on('beforeload', function(store){
            var rec = grid.up('tabpanel').down('pesertadidikform').getForm().getRecord();

            Ext.apply(store.proxy.extraParams, {
                peserta_didik_id: rec.data.peserta_didik_id
            });
        });

        grid.getStore().load();
    },
    setupPenggunaGrid:function(grid){
        grid.down('toolbar').add({
            xtype: 'button',
            text: 'Tambah',                
            //cls: 'addbutton',
            glyph: 61525,                
            scope: this,
            handler: 'tambahPengguna'
        }, {
            xtype: 'button',
            text: 'Ubah',            
            glyph: 61508,
            //cls: 'editbutton',    
            itemId: 'edit',
            scope: this,
            handler: 'editPengguna'
        }, {    
            xtype: 'button',
            text: 'Hapus',    
            //cls: 'deletebutton',            
            glyph: 61526,
            itemId: 'delete',
            scope: this,
            handler: 'hapusPengguna'
        },'->',{
            xtype: 'textfield',
            emptyText: 'Cari Nama Pengguna ...',
            enableKeyEvents: true,
            width: 300,
            name: 'keyword',
            listeners: {
                'keypress': function (field, e) {
                    var cari_text = field.getValue();
                    
                    //var grid = field.up('gridpanel');
                    var len = cari_text.length;
                    
                    if (e.getKey() == e.ENTER){
                        if (len < 1) {
                            grid.getStore().reload();

                        } else {
                            grid.getStore().reload();

                        }   
                        
                    }
                }
            }
        }
        ,{
            xtype: 'perancombo',
            width: 300,
            emptyText: 'Filter Berdasarkan Peran Pengguna',
            listeners: {
                'change': function(field, newValue, oldValue) {
                    grid.getStore().reload();   
                }
            }
        }
        );

        grid.headerCt.insert(0, {
            header: 'Sts',
            width: 50,
            sortable: true,
            dataIndex: 'aktif',
            hideable: false,
            renderer:function(v,p,r) {

                var aktifkah = r.data.aktif;
                var img = "";
                
                if(aktifkah) {
                    
                    if (aktifkah == 1) {
                        
                        img = "user_go";
                        p.tdAttr = 'data-qtip="Pengguna Berstatus AKTIF"';

                    } else if(aktifkah == 0) {                                    
                        img = "exclamation";
                        p.tdAttr = 'data-qtip="Pengguna Berstatus TIDAK AKTIF"';
                        
                    }                           
                
                } else {                                    
                    img = "exclamation";
                    p.tdAttr = 'data-qtip="Pengguna Berstatus TIDAK AKTIF"';
                    
                }

                var imgStr = '<div style="background: url(resources/icons/'+ img +'.png) no-repeat">&nbsp;</div>';
                //console.log(imgStr);
                
                return imgStr;
            }
        });
        
        grid.getStore().on('beforeload', function(store){
            var val = grid.down('toolbar').down('perancombo').getValue();
            var val_cari = grid.down('toolbar').down('textfield[name=keyword]').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                peran_id: val,
                keyword: val_cari
            });
        });


        grid.getStore().load();
    },
    editPengguna:function(btn){
        var grid = btn.up('gridpanel');

        var rec = grid.getSelectionModel().getSelection()[0];

        if(!rec){
            Ext.Msg.alert('Pilih salah satu baris terlebih dahulu!');
        }else{
            var win = Ext.create('simdik_batam.view.ManajemenPengguna.PenggunaWindow',{
                title: 'Ubah Pengguna: '+rec.data.nama+' ('+rec.data.username+')',
                height:500
            });

            var form = win.down('penggunaform');

            form.down('hidden[name=username]').destroy();
            form.down('hidden[name=password]').destroy();
            form.down('hidden[name=kode_wilayah]').destroy();
            form.down('hidden[name=peran_id]').destroy();

            form.add({
                xtype: 'perancombo',
                allowBlank: false,
                fieldLabel: 'Peran',
                labelAlign: 'right',
                name: 'peran_id',
                listeners: {
                    'change': function(field, newValue, oldValue) {
                        var val = newValue;

                        var combowilayah = this.up('form').down('mstwilayahcombo[name=kode_wilayah]');
                        
                        if(val == 1 || val == 2 || val == 3 || val == 4 || val == 5){
                            combowilayah.setValue('000000 ');
                            combowilayah.disable();

                        }else{
                            // combowilayah.enable();
                    
                            // combowilayah.setValue('');
                            combowilayah.getStore().reload();  
                        }
                         
                    }
                }
            },{
                xtype: 'mstwilayahcombo',
                anchor: '100%',
                fieldLabel: 'Wilayah',
                name: 'kode_wilayah',
                disabled: false,
                hidden:false,
                emptyText: 'Pilih Wilayah (Ketik untuk mencari)',
                labelAlign: 'right',
                labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
            },{
                xtype: 'textfield',
                allowBlank: false,
                disabled: true,
                fieldLabel: 'Username (Email)',
                name: 'username',
                emptyText: 'Username...',
                vtype: 'email',
                labelAlign: 'right',
                labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
            });

            form.down('mstwilayahcombo').getStore().on('beforeload', function(store){
                Ext.apply(store.proxy.extraParams, {
                    mode: 'bebas'
                });
            });

            form.loadRecord(rec);

            // form.down('mstwilayahcombo').setValue(rec.data.kode_wilayah);            
            win.show();
        }

    },
    tambahPengguna:function(btn){
        var grid = btn.up('gridpanel');

        var win = Ext.create('simdik_batam.view.ManajemenPengguna.PenggunaWindow',{
            title: 'Tambah Pengguna'
        });

        var form = win.down('form');

        Ext.apply(Ext.form.VTypes, {
            password: function(val, field) {
                // console.log(val, field);
                if (field.initialPassField) {
                    var pwd = Ext.getCmp(field.initialPassField);
                    return (val == pwd.getValue());
                }
                return true;
            },
            passwordText: 'Password tidak sama!'
        });

        form.down('hidden[name=username]').destroy();
        form.down('hidden[name=password]').destroy();
        form.down('hidden[name=kode_wilayah]').destroy();
        form.down('hidden[name=peran_id]').destroy();

        form.add({
            xtype: 'perancombo',
            allowBlank: false,
            fieldLabel: 'Peran',
            labelAlign: 'right',
            name: 'peran_id',
            listeners: {
                'change': function(field, newValue, oldValue) {
                    var val = newValue;

                    var combowilayah = this.up('form').down('mstwilayahcombo[name=kode_wilayah]');
                    
                    if(val == 1 || val == 2 || val == 3 || val == 4 || val == 5){
                        combowilayah.setValue('000000 ');
                        combowilayah.disable();

                    }else{
                        combowilayah.enable();
                
                        combowilayah.setValue('');
                        combowilayah.getStore().reload();  
                    }
                     
                }
            }
        },{
            xtype: 'mstwilayahcombo',
            anchor: '100%',
            fieldLabel: 'Wilayah',
            name: 'kode_wilayah',
            disabled: true,
            hidden:false,
            emptyText: 'Pilih Wilayah (Ketik untuk mencari)',
            labelAlign: 'right',
            labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
        },{
            xtype: 'textfield',
            allowBlank: false,
            fieldLabel: 'Username (Email)',
            name: 'username',
            emptyText: 'Username...',
            vtype: 'email',
            labelAlign: 'right',
            labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
        },{
            xtype: 'textfield',
            allowBlank: false,
            fieldLabel: 'Password',
            name: 'password',
            labelAlign: 'right',
            id: 'password',
            emptyText: 'Password...',
            inputType: 'password',
            labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
        },{
            xtype: 'textfield',
            allowBlank: false,
            labelAlign: 'right',
            fieldLabel: 'Ulang Password',
            name: 'password_confirm',
            emptyText: 'Konfirmasi password...',
            inputType: 'password',
            vtype: 'password',
            initialPassField: 'password',
            labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
        });

        form.down('mstwilayahcombo').getStore().on('beforeload', function(store){
            Ext.apply(store.proxy.extraParams, {
                mode: 'bebas'
            });
        });

        var recordConfig = {
            sekolah_id : '',
            pengguna_id : '07517B9B-1961-43DA-9A55-925630F2EC9B',
            lembaga_id : '',
            peran_id : '',
            username : '',
            password : '',
            nama : '',
            nip_nim : '',
            jabatan_lembaga : '',
            ym : '',
            skype : '',
            alamat : '',
            kode_wilayah : '',
            no_telepon : '',
            no_hp : '',
            aktif : 1
        };

        store = Ext.create('simdik_batam.store.Pengguna');

        rec = new simdik_batam.model.Pengguna(recordConfig);

        store.insert(0, rec);
                        
        form.loadRecord(rec);

        win.show();
    },
    savePenggunaBaru:function(btn){
        Ext.MessageBox.show({
            msg: 'Memproses Data ...',
            progressText: 'Memproses Data ...',
            width: 200,
            wait: true
        });

        var form = btn.up('penggunaform');
        var win = form.up('window');

        form.down('textfield[name=username]').enable();
        if(form.down('mstwilayahcombo')){
            form.down('mstwilayahcombo').enable();
        }

        var values = form.getForm().getValues();

        console.log(values);

        Ext.Ajax.request({
            url     : 'post/Pengguna',
            method  : 'POST',
            params  : {
                pengguna_id: values.pengguna_id,
                nama: values.nama,
                alamat: values.alamat,
                no_telepon: values.no_telepon,
                no_hp: values.no_hp,
                nip_nim: values.nip_nim,
                jabatan_lembaga: values.jabatan_lembaga,
                username: values.username,
                password: values.password,
                password_confirm: values.password_confirm,
                peran_id: values.peran_id,
                aktif: values.aktif,
                kode_wilayah: values.kode_wilayah
            },
            failure : function(response, options){
                Ext.Msg.alert('Warning','Ada Kesalahan dalam pengisian data. Mohon dicek lagi');
            },
            success : function(response){
                var json = Ext.JSON.decode(response.responseText);
                
                if(json.success){
                    
                    Ext.Msg.alert('Berhasil', json.message);

                    win.destroy();

                } else if(json.success == false) {
                    Ext.Msg.alert('Error',json.message);
                }
            }
        });
    },
    hapusPengguna:function(btn){
        var grid = btn.up('gridpanel');

        var rec = grid.getSelectionModel().getSelection()[0];

        if(!rec){
            Ext.Msg.alert('Pilih salah satu baris terlebih dahulu!');
        }else{
            Ext.Ajax.request({
            url     : 'delete/Pengguna',
            method  : 'POST',
            params  : {
                pengguna_id: rec.data.pengguna_id
            },
            failure : function(response, options){
                Ext.Msg.alert('Warning','Ada Kesalahan dalam pengolahan data. Mohon dicoba lagi');
            },
            success : function(response){
                var json = Ext.JSON.decode(response.responseText);
                
                if(json.success){
                    
                    Ext.Msg.alert('Berhasil', json.message);

                    grid.getStore().load();

                } else if(json.success == false) {
                    Ext.Msg.alert('Error',json.message);
                }
            }
        });      
        }
    },
    setupPtkJenisKelaminGrid:function(grid){
        grid.getStore().on('beforeload', function(store){
            // var keyword = grid.up('panel').down('ptkjeniskelamingrid').down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var keyword = grid.up('panel').down('ptkjeniskelamintotalgrid').down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var tahun_ajaran_id = grid.up('panel').down('ptkjeniskelamintotalgrid').down('toolbar').down('tahunajarancombo').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword,
                tahun_ajaran_id: tahun_ajaran_id
            });
        });

        grid.getStore().load();
    },
    setupPtkJenisKelaminTotalGrid:function(grid){
        grid.down('toolbar').add({
            xtype:'tahunajarancombo',
            emptyText:'Pilih Tahun Ajaran...',
            listeners: {
                change: function(combo, newValue, oldValue, eOpts ) {
                    var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();

                    grid.getStore().reload();

                    grid.up('panel').down('ptkjeniskelamingrid').getStore().reload();

                    grid.up('panel').up('panel').down('panel').down('chartpieptkjeniskelamin').down('polar').getStore().reload({
                        params:{
                            bentuk_pendidikan_id:keyword,
                            tahun_ajaran_id: newValue
                        }
                    });

                    grid.up('panel').up('panel').down('panel').down('chartbarptkjeniskelamin').down('cartesian').getStore().reload({
                        params:{
                            bentuk_pendidikan_id:keyword,
                            tahun_ajaran_id: newValue
                        }
                    });
                }
            }
        });

        grid.getStore().on('beforeload', function(store){
            var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var tahun_ajaran_id = grid.down('toolbar').down('tahunajarancombo').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword,
                tahun_ajaran_id: tahun_ajaran_id
            });
        });

        grid.getStore().load();
    },
    setupPtkAgamaTotalGrid:function(grid){
        grid.down('toolbar').add({
            xtype:'tahunajarancombo',
            emptyText:'Pilih Tahun Ajaran...',
            listeners: {
                change: function(combo, newValue, oldValue, eOpts ) {
                    var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();

                    grid.getStore().reload();

                    grid.up('panel').down('ptkagamagrid').getStore().reload();

                    grid.up('panel').up('panel').down('panel').down('chartpieptkagama').down('polar').getStore().reload({
                        params:{
                            bentuk_pendidikan_id:keyword,
                            tahun_ajaran_id: newValue
                        }
                    });

                    grid.up('panel').up('panel').down('panel').down('chartbarptkagama').down('cartesian').getStore().reload({
                        params:{
                            bentuk_pendidikan_id:keyword,
                            tahun_ajaran_id: newValue
                        }
                    });
                }
            }
        });

        grid.getStore().on('beforeload', function(store){
            var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var tahun_ajaran_id = grid.down('toolbar').down('tahunajarancombo').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword,
                tahun_ajaran_id: tahun_ajaran_id
            });
        });

        grid.getStore().load();
    },
    setupPtkAgamaGrid:function(grid){
        grid.getStore().on('beforeload', function(store){
            // var keyword = grid.up('panel').down('ptkjeniskelamingrid').down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var keyword = grid.up('panel').down('ptkagamatotalgrid').down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var tahun_ajaran_id = grid.up('panel').down('ptkagamatotalgrid').down('toolbar').down('tahunajarancombo').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword,
                tahun_ajaran_id: tahun_ajaran_id
            });
        });

        grid.getStore().load();
    },
    setupPdJenisKelaminGrid:function(grid){
        grid.getStore().on('beforeload', function(store){
            // var keyword = grid.up('panel').down('ptkjeniskelamingrid').down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var keyword = grid.up('panel').down('pdjeniskelamintotalgrid').down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var semester_id = grid.up('panel').down('pdjeniskelamintotalgrid').down('toolbar').down('semestercombo').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword,
                semester_id: semester_id
            });
        });

        grid.getStore().load();
    },
    setupPdJenisKelaminTotalGrid:function(grid){
        grid.down('toolbar').add({
            xtype:'semestercombo',
            emptyText:'Pilih Semester...',
            listeners: {
                change: function(combo, newValue, oldValue, eOpts ) {
                    var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();

                    grid.getStore().reload();

                    grid.up('panel').down('pdjeniskelamingrid').getStore().reload();

                    grid.up('panel').up('panel').down('panel').down('chartpiepdjeniskelamin').down('polar').getStore().reload({
                        params:{
                            bentuk_pendidikan_id:keyword,
                            semester_id: newValue
                        }
                    });

                    grid.up('panel').up('panel').down('panel').down('chartbarpdjeniskelamin').down('cartesian').getStore().reload({
                        params:{
                            bentuk_pendidikan_id:keyword,
                            semester_id: newValue
                        }
                    });
                }
            }
        });

        grid.getStore().on('beforeload', function(store){
            var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var semester_id = grid.down('toolbar').down('semestercombo').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword,
                semester_id: semester_id
            });
        });

        grid.getStore().load();
    },
    setupPdAgamaGrid: function(grid){
        grid.getStore().on('beforeload', function(store){
            // var keyword = grid.up('panel').down('ptkjeniskelamingrid').down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var keyword = grid.up('panel').down('pdagamatotalgrid').down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var semester_id = grid.up('panel').down('pdagamatotalgrid').down('toolbar').down('semestercombo').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword,
                semester_id: semester_id
            });
        });

        // grid.getStore().load({
        //     scope: this,
        //     callback: function(records, operation, success) {
        //         // the operation object
        //         // contains all of the details of the load operation
        //         console.log(success);
        //     }
        // });
    },
    setupPdAgamaTotalGrid:function(grid){
        grid.down('toolbar').add({
            xtype:'semestercombo',
            emptyText:'Pilih Semester...',
            listeners: {
                change: function(combo, newValue, oldValue, eOpts ) {
                    grid.getStore().reload();

                }
            }
        });

        grid.getStore().on('beforeload', function(store){
            var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var semester_id = grid.down('toolbar').down('semestercombo').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword,
                semester_id: semester_id
            });
        });

        grid.getStore().load({
            scope: this,
            callback: function(records, operation, success) {
                // the operation object
                // contains all of the details of the load operation
                console.log(success);
                if(success){
                    grid.up('panel').down('pdagamagrid').getStore().load({
                        scope: this,
                        callback: function(records, operation, success) {
                            // the operation object
                            // contains all of the details of the load operation
                            console.log(success);
                            grid.up('panel').up('panel').down('panel').down('chartpiepdagama').down('polar').getStore().load({
                                scope: this,
                                callback: function(records, operation, success) {
                                    // the operation object
                                    // contains all of the details of the load operation
                                    console.log(success);
                                    grid.up('panel').up('panel').down('panel').down('chartbarpdagama').down('cartesian').getStore().load({});
                                }
                            });
                        }
                    });
                }
            }
        });
    },
    setupSekolahStatusGrid:function(grid){
        grid.getStore().on('beforeload', function(store){
            // var keyword = grid.up('panel').down('ptkjeniskelamingrid').down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var keyword = grid.up('panel').down('sekolahstatustotalgrid').down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var semester_id = grid.up('panel').down('sekolahstatustotalgrid').down('toolbar').down('semestercombo').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword,
                semester_id: semester_id
            });
        });

        grid.getStore().load();
    },
    setupSekolahStatusTotalGrid:function(grid){

        grid.down('toolbar').add({
            xtype:'semestercombo',
            emptyText:'Pilih Semester...',
            listeners: {
                change: function(combo, newValue, oldValue, eOpts ) {
                    var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();

                    grid.getStore().reload();

                    grid.up('panel').down('sekolahstatusgrid').getStore().reload();

                    grid.up('panel').up('panel').down('panel').down('chartpiesekolahstatus').down('polar').getStore().reload({
                        params:{
                            bentuk_pendidikan_id:keyword,
                            semester_id: newValue
                        }
                    });

                    grid.up('panel').up('panel').down('panel').down('chartbarsekolahstatus').down('cartesian').getStore().reload({
                        params:{
                            bentuk_pendidikan_id:keyword,
                            semester_id: newValue
                        }
                    });
                }
            }
        },{
            xtype:'button',
            text:'Export Xls',
            glyph: 61474,
            urls: 'excel/sekolahStatus',
            params: 'semester_id=',
            handler:function(btn){
                var bentuk_pendidikan_id = btn.up('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
                var semester_id = btn.up('toolbar').down('semestercombo').getValue();

                // console.log(url+'/'+bentuk_pendidikan_id+'?'+params);
                window.location = 'excel/sekolahStatus?bentuk_pendidikan_id='+bentuk_pendidikan_id+'&semester_id='+semester_id;
            }
        });

        grid.getStore().on('beforeload', function(store){
            var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var semester_id = grid.down('toolbar').down('semestercombo').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword,
                semester_id: semester_id
            });
        });

        grid.getStore().load();
    },
    setupSekolahBentukPendidikanTotalGrid:function(grid){
        grid.getStore().on('beforeload', function(store){
            var semester_id = grid.down('toolbar').down('semestercombo').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                semester_id: semester_id
            });
        });

        grid.down('toolbar').add({
            xtype:'button',
            text:'Export Xls',
            glyph: 61474,
            url: 'excel/sekolahBentukPendidikan',
            handler:function(btn){
                // var bentuk_pendidikan_id = btn.up('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
                var semester_id = btn.up('toolbar').down('semestercombo').getValue();

                // console.log(url+'/'+bentuk_pendidikan_id+'?'+params);
                window.location = 'excel/sekolahBentukPendidikan?semester_id='+semester_id;
            }
        },{
            xtype:'semestercombo',
            emptyText:'Pilih Semester...',
            listeners: {
                change: function(combo, newValue, oldValue, eOpts ) {
                    // var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();

                    grid.getStore().reload();

                    grid.up('panel').down('sekolahbentukpendidikangrid').getStore().reload();

                    grid.up('panel').up('panel').down('panel').down('chartpiesekolahbentukpendidikan').down('polar').getStore().reload({
                        params:{
                            // bentuk_pendidikan_id:keyword,
                            semester_id: newValue
                        }
                    });

                    grid.up('panel').up('panel').down('panel').down('chartbarsekolahbentukpendidikan').down('cartesian').getStore().reload({
                        params:{
                            // bentuk_pendidikan_id:keyword,
                            semester_id: newValue
                        }
                    });
                }
            }
        });

        grid.getStore().load();
    },
    setupSekolahBentukPendidikanGrid:function(grid){
        grid.getStore().on('beforeload', function(store){

            var semester_id = grid.up('panel').down('sekolahbentukpendidikantotalgrid').down('toolbar').down('semestercombo').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                semester_id: semester_id
            });
        });

        Ext.Ajax.request({
            url: 'session',
            success: function(response){
                var text = response.responseText;
                
                var res = Ext.JSON.decode(response.responseText);

                if(res.level == 'dikmen'){
                    grid.columns[1].setVisible(false);
                    grid.columns[2].setVisible(false);
                    // grid.columns[3].setVisible(false);

                    var kolomSma = Ext.create('Ext.grid.column.Column', {
                        header: 'SMA',
                        width: 80,
                        sortable: true,
                        dataIndex: 'sma',
                        hideable: false,
                        align:'right',
                        renderer:function(v,p,r){
                            return grid.formatMoney(Math.abs(v),0);
                        }
                    });
                    var kolomSmk = Ext.create('Ext.grid.column.Column', {
                        header: 'SMK',
                        width: 80,
                        sortable: true,
                        dataIndex: 'smk',
                        hideable: false,
                        align:'right',
                        renderer:function(v,p,r){
                            return grid.formatMoney(Math.abs(v),0);
                        }
                    });
                    var kolomSmlb = Ext.create('Ext.grid.column.Column', {
                        header: 'SMLB',
                        width: 80,
                        sortable: true,
                        dataIndex: 'smlb',
                        hideable: false,
                        align:'right',
                        renderer:function(v,p,r){
                            return grid.formatMoney(Math.abs(v),0);
                        }
                    });
                    grid.headerCt.insert(1, kolomSma);
                    grid.headerCt.insert(2, kolomSmk);
                    grid.headerCt.insert(3, kolomSmlb);
                    
                }else if(res.level == 'dikdasmen'){
                    var kolomSma = Ext.create('Ext.grid.column.Column', {
                        header: 'SMA',
                        width: 80,
                        sortable: true,
                        dataIndex: 'sma',
                        hideable: false,
                        align:'right',
                        renderer:function(v,p,r){
                            return grid.formatMoney(Math.abs(v),0);
                        }
                    });
                    var kolomSmk = Ext.create('Ext.grid.column.Column', {
                        header: 'SMK',
                        width: 80,
                        sortable: true,
                        dataIndex: 'smk',
                        hideable: false,
                        align:'right',
                        renderer:function(v,p,r){
                            return grid.formatMoney(Math.abs(v),0);
                        }
                    });
                    var kolomSmlb = Ext.create('Ext.grid.column.Column', {
                        header: 'SMLB',
                        width: 80,
                        sortable: true,
                        dataIndex: 'smlb',
                        hideable: false,
                        align:'right',
                        renderer:function(v,p,r){
                            return grid.formatMoney(Math.abs(v),0);
                        }
                    });
                    grid.headerCt.insert(1, kolomSma);
                    grid.headerCt.insert(2, kolomSmk);
                    grid.headerCt.insert(3, kolomSmlb);
                }

                grid.getStore().load();
            }
        });
    },
    ExportDataPokokSekolah:function(btn){
        var grid = btn.up('gridpanel');
        var form = grid.up('panel').down('filtersekolahform');
        var values = form.getForm().getValues();

        // console.log(values);
        // console.log(values.keyword);

        window.location = 'excel/dataPokokSekolah?keyword='+values.keyword+'&bp_id='+values.pilihbentukcombo+'&ss_id='+values.pilihstatuscombo+'&kec='+values.combokecmasterdatasekolah;

    },
    exportProfilSekolah:function(btn){
        // Ext.Msg.alert('tes');
        var form = btn.up('toolbar').up('sekolahform');
        var values = form.getForm().getValues();

        // console.log(values.sekolah_id);
        window.location = 'excel/ProfilSekolah?sekolah_id='+values.sekolah_id;
    },
    setupSekolahWaktuPenyelenggaraanTotalGrid:function(grid){
        grid.down('toolbar').add({
            xtype:'semestercombo',
            emptyText:'Pilih Semester...',
            listeners: {
                change: function(combo, newValue, oldValue, eOpts ) {
                    var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();

                    grid.getStore().reload();

                    grid.up('panel').down('sekolahwaktupenyelenggaraangrid').getStore().reload();

                    grid.up('panel').up('panel').down('panel').down('chartpiesekolahwaktupenyelenggaraan').down('polar').getStore().reload({
                        params:{
                            bentuk_pendidikan_id:keyword,
                            semester_id: newValue
                        }
                    });

                    grid.up('panel').up('panel').down('panel').down('chartbarsekolahwaktupenyelenggaraan').down('cartesian').getStore().reload({
                        params:{
                            bentuk_pendidikan_id:keyword,
                            semester_id: newValue
                        }
                    });
                }
            }
        });

        grid.getStore().on('beforeload', function(store){
            var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var semester_id = grid.down('toolbar').down('semestercombo').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword,
                semester_id: semester_id
            });
        });

        grid.getStore().load();
    },
    setupSekolahWaktuPenyelenggaraanGrid:function(grid){
        grid.getStore().on('beforeload', function(store){
            // var keyword = grid.up('panel').down('ptkjeniskelamingrid').down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var keyword = grid.up('panel').down('sekolahwaktupenyelenggaraantotalgrid').down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var semester_id = grid.up('panel').down('sekolahwaktupenyelenggaraantotalgrid').down('toolbar').down('semestercombo').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword,
                semester_id: semester_id
            });
        });

        grid.getStore().load();
    },
    setupPtkJenisPtkTotalGrid: function(grid){
        var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();

        grid.down('toolbar').add({
            xtype:'button',
            text:'Export Xls',
            glyph: 61474,
            urls: 'excel/ptkJenisPtk',
            handler:function(btn){
                var bentuk_pendidikan_id = btn.up('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
                var tahun_ajaran_id = btn.up('toolbar').down('tahunajarancombo').getValue();

                window.location = 'excel/ptkJenisPtk?bentuk_pendidikan_id='+bentuk_pendidikan_id+'&tahun_ajaran_id='+tahun_ajaran_id;
            }
        },{
            xtype:'tahunajarancombo',
            emptyText:'Pilih Tahun Ajaran...',
            listeners: {
                change: function(combo, newValue, oldValue, eOpts ) {
                    var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();

                    grid.getStore().reload();

                    grid.up('panel').down('ptkjenisptkgrid').getStore().reload();

                    grid.up('panel').up('panel').down('panel').down('chartpieptkjenisptk').down('polar').getStore().reload({
                        params:{
                            bentuk_pendidikan_id:keyword,
                            tahun_ajaran_id: newValue
                        }
                    });

                    grid.up('panel').up('panel').down('panel').down('chartbarptkjenisptk').down('cartesian').getStore().reload({
                        params:{
                            bentuk_pendidikan_id:keyword,
                            tahun_ajaran_id: newValue
                        }
                    });
                }
            }
        });

        grid.getStore().on('beforeload', function(store){
            var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var tahun_ajaran_id = grid.down('toolbar').down('tahunajarancombo').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword,
                tahun_ajaran_id: tahun_ajaran_id
            });
        });

        grid.getStore().load();
    },
    setupPtkJenisPtkGrid:function(grid){
        grid.getStore().on('beforeload', function(store){
            // var keyword = grid.up('panel').down('ptkjeniskelamingrid').down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var keyword = grid.up('panel').down('ptkjenisptktotalgrid').down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var tahun_ajaran_id = grid.up('panel').down('ptkjenisptktotalgrid').down('toolbar').down('tahunajarancombo').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword,
                tahun_ajaran_id: tahun_ajaran_id
            });
        });

        grid.getStore().load();
    },
    exportXls:function(btn){
        var url = btn.urls;
        // var params = '';

        // if(btn.params){
        //     params = btn.params;
        // }
        
       var bentuk_pendidikan_id = btn.up('toolbar').down('combobox').getValue();

       // console.log(url+'/'+bentuk_pendidikan_id+'?'+params);

       window.location = url+'/'+bentuk_pendidikan_id;
    },
    savePassword:function(btn){
        Ext.MessageBox.show({
            msg: 'Menyimpan Data ...',
            progressText: 'Menyimpan Data ...',
            width: 200,
            wait: true
        });

        var form = btn.up('form');
        var window = btn.up('form').up('window');
        // var values = Ext.JSON.encode(form.getValues());
        var values = form.getValues();

        if(form.isValid()){
            // Ext.Msg.alert(values);

            Ext.Ajax.request({
                url     : 'post/GantiPassword',
                method  : 'POST',
                params  : {
                    pengguna_id: values.pengguna_id,
                    password_lama: values.password_lama,
                    password_baru: values.password_baru
                },
                failure : function(response, options){
                    Ext.Msg.alert('Warning','Ada Kesalahan dalam pemrosesan data. Mohon dicek lagi');
                },
                success : function(response){
                    var json = Ext.JSON.decode(response.responseText);
                    
                    if(json.success){
                        
                        Ext.MessageBox.hide();
                        window.destroy();
                        Ext.Msg.alert('Berhasil', json.message);

                    } else if(json.success == false) {
                        Ext.Msg.alert('Error',json.message);
                    }
                }
            });

        }
    },
    simpanMutasiBaru:function(btn){
        Ext.MessageBox.show({
            msg: 'Menyimpan Data ...',
            progressText: 'Menyimpan Data ...',
            width: 200,
            wait: true
        });

        var form = btn.up('form');

        var values = form.getForm().getValues();

        console.log(values);

        if(form.isValid()){

            Ext.Ajax.request({
                url     : 'post/Mutasi',
                method  : 'POST',
                params  : {
                    alamat_jalan: values.alamat_jalan,
                    alamat_yang_diikuti:values.alamat_yang_diikuti,
                    hubungan_keluarga: values.hubungan_keluarga,
                    kelas: values.kelas,
                    kode_wilayah_berangkat: values.kode_wilayah_berangkat,
                    maksud_tujuan: values.maksud_tujuan,
                    nama: values.nama,
                    nama_ayah: values.nama_ayah,
                    nama_yang_diikuti: values.nama_yang_diikuti,
                    nipd: values.nipd,
                    nisn: values.nisn,
                    nomor_surat: values.nomor_surat,
                    pekerjaan: values.pekerjaan,
                    pekerjaan_id_ayah_str: values.pekerjaan_id_ayah_str,
                    peserta_didik_id: values.peserta_didik_id,
                    sekolah_id: values.sekolah_id,
                    tanggal_berangkat: values.tanggal_berangkat,
                    tanggal_lahir:values.tanggal_lahir,
                    tempat_lahir: values.tempat_lahir,
                    sekolah_id: values.sekolah_id,
                    jenis_surat_id: values.jenis_surat_id,
                    tanggal_surat: values.tanggal_surat,
                    pengguna_id: values.pengguna_id,
                    keterangan_kepala_sekolah:values.keterangan_kepala_sekolah,
                    keterangan_lain: values.keterangan_lain,
                    mutasi_id: values.mutasi_id
                },
                failure : function(response, options){
                    Ext.MessageBox.hide();

                    Ext.Msg.alert('Warning','Ada Kesalahan dalam pengisian data. Mohon dicek lagi');
                },
                success : function(response){
                    var json = Ext.JSON.decode(response.responseText);
                    
                    if(json.success){
                        
                        Ext.MessageBox.show({
                            title: 'Berhasil',
                            msg: json.message,
                            buttons: Ext.MessageBox.YESNO,
                            buttonText:{ 
                                yes: "Lanjut Edit", 
                                no: "Cetak" 
                            },
                            fn: function(btn){
                                if (btn == 'yes'){

                                    if(json.perlu_reload){

                                        Ext.MessageBox.show({
                                            msg: 'Memuat Ulang Data Mutasi '+values.nama+'...',
                                            progressText: 'Memuat Ulang Data Mutasi '+values.nama+'...',
                                            width: 200,
                                            wait: true
                                        });

                                        var tabPanel = Ext.getCmp('ContentPanel');
                                        var panelId = 'MutasiPesertaDidik_' + json.mutasi_id;
                                        var tab = tabPanel.getComponent(panelId);
                                        // console.log('yes');
                                        form.up('mutasipesertadidikpanel').close();

                                        var storeCallback = Ext.create('simdik_batam.store.Mutasi');

                                        storeCallback.load({
                                            params:{
                                                mutasi_id: json.mutasi_id
                                            }
                                        });

                                        storeCallback.on('load', function(store, records, options){

                                            var rec = store.findRecord('mutasi_id', json.mutasi_id);

                                            if(rec){
                                                var judul = 'Edit Mutasi PD: ' + rec.data.nama;

                                                var classname = 'simdik_batam.view.Mutasi.MutasiPesertaDidik';

                                                tabPanel.add(Ext.create(classname, {
                                                    title: judul,
                                                    closable:true,
                                                    glyph: 61447,
                                                    id: panelId,
                                                    record: rec
                                                }));

                                                tabPanel.setActiveTab(panelId);

                                                Ext.MessageBox.hide();

                                            }

                                            console.log(rec);

                                        });     

                                    }   

                                }else if(btn == 'no'){
                                    // console.log('no');

                                    var urlCetak = 'cetak/Mutasi/'+json.mutasi_id + '/lihat';

                                    var win = new Ext.window.Window({   
                                        title: 'Cetak Mutasi Peserta Didik',
                                        width : 700,
                                        height: 600,
                                        modal:true,
                                        layout : 'fit',
                                        closeAction: 'hide',
                                        items : [{
                                            xtype : "component",
                                            autoEl : {
                                                tag : "iframe",
                                                src : urlCetak
                                            }
                                        }],
                                        buttons: [{
                                            text: 'Cetak PDF',
                                            scope: this,
                                            handler: function(btn) {
                                                window.location.replace('cetak/Mutasi/'+ json.mutasi_id + '/pdf');
                                            }
                                        }]
                                    });

                                    win.show();


                                }
                            }
                        });

                        // form.up('mutasipesertadidikpanel').close();

                        // Ext.MessageBox.hide();


                    } else if(json.success == false) {
                        Ext.Msg.alert('Error',json.message);

                        // form.up('mutasipesertadidikpanel').close();

                        Ext.MessageBox.hide();

                    }
                }
            });

        }else{
            Ext.Msg.alert('Peringatan', 'Lengkapi seluruh isian form sebelum menyimpan data');
        }
    },
    setupPrasaranaGrid:function (grid) {
        // grid.down('toolbar').add({
        //     xtype:'semestercombo',
        //     emptyText:'Pilih Semester...',
        //     listeners: {
        //         change: function(combo, newValue, oldValue, eOpts ) {
        //             var grid = combo.up('gridpanel');
                    
        //             grid.getStore().reload();
        //         }
        //     }
        // });

        grid.getStore().on('beforeload', function(store){
            var sekolah_id = grid.up('panel').down('sekolahform').down('hidden[name=sekolah_id]').getValue();
            // var semester_id = grid.down('toolbar').down('semestercombo').getValue();

            // sekolah_id.setTitle('tes');

            Ext.apply(store.proxy.extraParams, {
                sekolah_id: sekolah_id,
                keyword: null,
                jenis_prasarana_id: null,
                // semester_id: semester_id
            });
        });

        grid.getStore().load();
    },
    setupDataPokokPrasaranaGrid:function (grid) {

        grid.getStore().on('beforeload', function(store){
            var keyword = grid.up('panel').down('filterprasaranaform').down('textfield[name=keyword]').getValue();
            var jenis_prasarana_id = grid.up('panel').down('filterprasaranaform').down('jenisprasaranacombo').getValue();
                        
            Ext.apply(store.proxy.extraParams, {
                keyword: keyword,
                jenis_prasarana_id: jenis_prasarana_id,
                sekolah_id: null
            });
        });


        grid.getStore().load();
    },
    cetakFromForm:function(btn){
        var form = btn.up('form');

        var values = form.getForm().getValues();

        var urlCetak = 'cetak/Mutasi/'+values.mutasi_id + '/lihat';

        var win = new Ext.window.Window({   
            title: 'Cetak Mutasi Peserta Didik: ' + values.nama,
            width : 700,
            height: 600,
            modal:true,
            layout : 'fit',
            closeAction: 'hide',
            items : [{
                xtype : "component",
                autoEl : {
                    tag : "iframe",
                    src : urlCetak
                }
            }],
            buttons: [{
                text: 'Cetak PDF',
                scope: this,
                handler: function(btn) {
                    window.location.replace('cetak/Mutasi/'+ values.mutasi_id + '/pdf');
                }
            }]
        });

        win.show();
    },
    setupPdTingkatKelasJenisKelaminGrid:function(grid){
        grid.down('toolbar').add({
            xtype:'semestercombo',
            emptyText:'Pilih Semester...',
            listeners: {
                change: function(combo, newValue, oldValue, eOpts ) {
                    grid.getStore().reload();
                }
            }
        },{
            text:'Export Xls',
            glyph: 61646,
            handler: function(btn){
                var semester_id = grid.down('toolbar').down('semestercombo').getValue();
                var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();

                window.location = 'excel/RekapPdTingkatJenisKelamin?bentuk_pendidikan_id='+ keyword + '&semester_id=' + semester_id;
            }
        });

        grid.getStore().on('beforeload', function(store){
            var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var semester_id = grid.down('toolbar').down('semestercombo').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword,
                semester_id: semester_id
            });
        });

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        if(dd<10) {
            dd='0'+dd
        } 

        if(mm<10) {
            mm='0'+mm
        } 

        today = dd+'/'+mm+'/'+yyyy;

        grid.down('toolbar[dock=bottom]').add({
            xtype:'displayfield',
            value: 'Data Rekap Per Tanggal ' + today + ' 00:00:00'
        })
        

        grid.getStore().load();
    },
    setupSekolahRangkumanGrid:function(grid){
        grid.down('toolbar').add({
            xtype:'semestercombo',
            emptyText:'Pilih Semester...',
            listeners: {
                change: function(combo, newValue, oldValue, eOpts ) {
                    grid.getStore().reload();
                }
            }
        },{
            text:'Export Xls',
            glyph: 61646,
            handler: function(btn){
                var semester_id = grid.down('toolbar').down('semestercombo').getValue();
                var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();

                window.location = 'excel/Rangkuman?bentuk_pendidikan_id='+ keyword + '&semester_id=' + semester_id;
            }
        },{
            text:'Export PDF',
            glyph: 61646,
            handler: function(btn){
                var semester_id = grid.down('toolbar').down('semestercombo').getValue();
                var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();

                window.open('pdf/rangkuman?bentuk_pendidikan_id='+ keyword + '&semester_id=' + semester_id, '_blank');
            }
        });

        grid.getStore().on('beforeload', function(store){
            var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var semester_id = grid.down('toolbar').down('semestercombo').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword,
                semester_id: semester_id
            });
        });

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        if(dd<10) {
            dd='0'+dd
        } 

        if(mm<10) {
            mm='0'+mm
        } 

        today = dd+'/'+mm+'/'+yyyy;

        grid.down('toolbar[dock=bottom]').add({
            xtype:'displayfield',
            value: 'Data Rekap Per Tanggal ' + today + ' 00:00:00'
        });
        

        grid.getStore().load();
    },
    setupSekolahRangkumanSpGrid:function(grid){
        grid.down('toolbar').add({
            xtype:'semestercombo',
            emptyText:'Pilih Semester...',
            listeners: {
                change: function(combo, newValue, oldValue, eOpts ) {
                    grid.getStore().reload();
                }
            }
        },{
            text:'Export Xls',
            glyph: 61646,
            handler: function(btn){
                var semester_id = grid.down('toolbar').down('semestercombo').getValue();
                var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
                var hash = window.location.hash;

                var hashArr = hash.split("/");

                // console.log(hashArr[3]);
                var id_level_wilayah = hashArr[2];
                var kode_wilayah = hashArr[3];

                window.location = 'excel/SekolahRangkumanSp2?bentuk_pendidikan_id='+ keyword + '&semester_id=' + semester_id + '&kode_wilayah=' + kode_wilayah + '&id_level_wilayah=' + id_level_wilayah;
            }
        },{
            text:'Export PDF',
            glyph: 61646,
            itemId: 'exportPdfRangkumanSekolahSp',
            handler: function(btn){
                var semester_id = grid.down('toolbar').down('semestercombo').getValue();
                var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
                var hash = window.location.hash;

                var hashArr = hash.split("/");

                // console.log(hashArr[3]);
                var id_level_wilayah = hashArr[2];
                var kode_wilayah = hashArr[3];

                window.open('pdf/sekolah_rangkuman_sp?bentuk_pendidikan_id='+ keyword + '&semester_id=' + semester_id + '&kode_wilayah=' + kode_wilayah + '&id_level_wilayah=' + id_level_wilayah, '_blank');
            }
        });
        
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        if(dd<10) {
            dd='0'+dd
        } 

        if(mm<10) {
            mm='0'+mm
        } 

        today = dd+'/'+mm+'/'+yyyy;

        grid.down('toolbar[dock=bottom]').add({
            xtype:'displayfield',
            value: 'Data Rekap Per Tanggal ' + today + ' 00:00:00'
        });
        
        var hash = window.location.hash;
        var hashArr = hash.split("/");
        var id_level_wilayah = hashArr[2];

        if(id_level_wilayah == 1){
            grid.down('toolbar').down('button[itemId=exportPdfRangkumanSekolahSp]').hide();
        }

        grid.getStore().on('beforeload', function(store){
            var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var semester_id 
             grid.down('toolbar').down('semestercombo').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword,
                semester_id: semester_id
            });
        });

        // grid.getStore().on('load', function(store){
        //     grid.down('toolbar[dock=bottom]').down('dispslayfield').setValue(store.getCount());

        //     console.log(store.getCount());
        // });

        grid.getStore().load();
    },
    setupProgresDataGrid:function(grid){
        grid.down('toolbar').add({
            text:'Export Xls',
            glyph: 61646,
            handler: function(btn){
                var kode_wilayah = grid.getStore().getAt(0).data.mst_kode_wilayah;
                var id_level_wilayah = grid.getStore().getAt(0).data.id_level_wilayah - 1;
                var semester_id = grid.down('toolbar').down('semestercombo').getValue();

                window.location = 'excel/ProgresData?kode_wilayah='+ kode_wilayah +'&id_level_wilayah=' + id_level_wilayah + '&semester_id=' + semester_id;
            }
        },{
            text: 'Export PDF',
            glyph: 61646,
            handler: function(btn){
                var kode_wilayah = grid.getStore().getAt(0).data.mst_kode_wilayah;
                var id_level_wilayah = grid.getStore().getAt(0).data.id_level_wilayah - 1;
                var semester_id = grid.down('toolbar').down('semestercombo').getValue();

                window.open('pdf/progres_data?kode_wilayah='+ kode_wilayah +'&id_level_wilayah=' + id_level_wilayah + '&semester_id=' + semester_id, '_blank');
            }
        },{
            xtype:'semestercombo',
            emptyText:'Pilih Semester...',
            listeners: {
                change: function(combo, newValue, oldValue, eOpts ) {
                    grid.getStore().reload();
                }
            }
        });

        grid.getStore().on('beforeload', function(store){
            var semester_id = grid.down('toolbar').down('semestercombo').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                semester_id: semester_id
            });
        });

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        if(dd<10) {
            dd='0'+dd
        } 

        if(mm<10) {
            mm='0'+mm
        } 

        today = dd+'/'+mm+'/'+yyyy;

        grid.down('toolbar[dock=bottom]').add({
            xtype:'displayfield',
            value: 'Data Rekap Per Tanggal ' + today + ' 00:00:00'
        });
        

        grid.getStore().load();
    },
    setupPdTingkatKelasJenisKelaminSpGrid:function(grid){

        grid.down('toolbar').add({
            xtype:'semestercombo',
            emptyText:'Pilih Semester...',
            listeners: {
                change: function(combo, newValue, oldValue, eOpts ) {
                    grid.getStore().reload();
                }
            }
        },{
            text:'Export Xls',
            glyph: 61646,
            handler: function(btn){
                var semester_id = grid.down('toolbar').down('semestercombo').getValue();
                var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
                var hash = window.location.hash;

                var hashArr = hash.split("/");

                // console.log(hashArr[3]);
                var id_level_wilayah = hashArr[2];
                var kode_wilayah = hashArr[3];

                window.location = 'excel/SekolahRangkumanSp?bentuk_pendidikan_id='+ keyword + '&semester_id=' + semester_id + '&kode_wilayah=' + kode_wilayah + '&id_level_wilayah=' + id_level_wilayah;
            }
        });

        grid.getStore().on('beforeload', function(store){
            var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var semester_id = grid.down('toolbar').down('semestercombo').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword,
                semester_id: semester_id
            });
        });

        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();

        if(dd<10) {
            dd='0'+dd
        } 

        if(mm<10) {
            mm='0'+mm
        } 

        today = dd+'/'+mm+'/'+yyyy;

        grid.down('toolbar[dock=bottom]').add({
            xtype:'displayfield',
            value: 'Data Rekap Per Tanggal ' + today + ' 00:00:00'
        })
        

        grid.getStore().load();
    }
});
