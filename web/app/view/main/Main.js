/**
 * This class is the main view for the application. It is specified in app.js as the
 * "autoCreateViewport" property. That setting automatically applies the "viewport"
 * plugin to promote that instance of this class to the body element.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('simdik_batam.view.main.Main', {
    extend: 'Ext.container.Container',
    requires:[
        'simdik_batam.view.main.TreeMenu',
        'simdik_batam.view.main.TreeMenuNonLogin',
        'simdik_batam.view._GlobalComponent.Combo.MstWilayahCombo',
        'simdik_batam.view._GlobalComponent.Combo.TahunAjaranCombo',
        'simdik_batam.view._GlobalComponent.Combo.SemesterCombo',
        'simdik_batam.view._GlobalComponent.combo.KeahlianLaboratoriumCombo',
        'simdik_batam.view._GlobalComponent.combo.PeranCombo',
        'simdik_batam.view._GlobalComponent.Combo.BentukPendidikanCombo',
        'simdik_batam.view._GlobalComponent.Combo.ComboKecamatan',
        'simdik_batam.view._GlobalComponent.Combo.ComboSekolah',
        'simdik_batam.view._GlobalComponent.Combo.ComboPengguna',
        'simdik_batam.view._GlobalComponent.Combo.ComboJenisSurat',
        'simdik_batam.view._GlobalComponent.Combo.JenisPrasarana',
        'simdik_batam.view._GlobalComponent.Combo.JurusanCombo',
        'simdik_batam.view._GlobalComponent.Combo.JenisPtkCombo',
        'simdik_batam.view.Beranda.Beranda',
        'simdik_batam.view.Beranda.RekapTotalGrid',
        'simdik_batam.view.Beranda.ProfilWindow',
        'simdik_batam.view.Beranda.GantiPasswordForm',
        'simdik_batam.view.Beranda.GantiPasswordWindow',
        'simdik_batam.view.DataPokokSekolah.DataPokokSekolah',
        'simdik_batam.view.DataPokokSekolah.DataPokokSekolahGrid',
        'simdik_batam.view.DataPokokSekolah.FilterSekolahForm',
        'simdik_batam.view.DataPokokSekolah.DetailSekolah',
        'simdik_batam.view.DataPokokSekolah.SekolahForm',
        'simdik_batam.view.DataPokokSekolah.PtkGrid',
        'simdik_batam.view.DataPokokSekolah.PrasaranaGrid',
        'simdik_batam.view.DataPokokSekolah.PesertaDidikGrid',
        'simdik_batam.view.DataPokokSekolah.KoregWindow',
        'simdik_batam.view.DataPokokPesertaDidik.DataPokokPesertaDidik',
        'simdik_batam.view.DataPokokPesertaDidik.DataPokokPesertaDidikGrid',
        'simdik_batam.view.DataPokokPesertaDidik.FilterPesertaDidikForm',
        'simdik_batam.view.DataPokokPesertaDidik.DetailPesertaDidik',
        'simdik_batam.view.DataPokokPesertaDidik.PesertaDidikForm',
        'simdik_batam.view.DataPokokPesertaDidik.AnggotaRombelGrid',
        'simdik_batam.view.DataPokokPtk.DataPokokPtk',
        'simdik_batam.view.DataPokokPtk.DataPokokPtkGrid',
        'simdik_batam.view.DataPokokPtk.FilterPtkForm',
        'simdik_batam.view.DataPokokPtk.DetailPtk',
        'simdik_batam.view.DataPokokPtk.PtkForm',
        'simdik_batam.view.DataPokokPtk.PtkTerdaftarGrid',
        'simdik_batam.view.DataPokokPtk.PembelajaranGrid',
        'simdik_batam.view.DataPokokPtk.RiwayatSertifikasiGrid',
        'simdik_batam.view.ManajemenPengguna.ManajemenPengguna',
        'simdik_batam.view.ManajemenPengguna.PenggunaGrid',
        'simdik_batam.view.ManajemenPengguna.PenggunaForm',
        'simdik_batam.view.ManajemenPengguna.PenggunaWindow',
        'simdik_batam.view.RekapPdJenisKelamin.RekapPdJenisKelamin',
        'simdik_batam.view.RekapPdJenisKelamin.chartPiePdJenisKelamin',
        'simdik_batam.view.RekapPdJenisKelamin.chartBarPdJenisKelamin',
        'simdik_batam.view.RekapPdJenisKelamin.PdJenisKelaminGrid',
        'simdik_batam.view.RekapPdJenisKelamin.PdJenisKelaminTotalGrid',
        // 'simdik_batam.view.RekapPdJenisKelamin.chartBarPdJenisKelamin'
        'simdik_batam.view.RekapPdAgama.RekapPdAgama',
        'simdik_batam.view.RekapPdAgama.chartPiePdAgama',
        'simdik_batam.view.RekapPdAgama.chartBarPdAgama',
        'simdik_batam.view.RekapPdAgama.PdAgamaGrid',
        'simdik_batam.view.RekapPdAgama.PdAgamaTotalGrid',
        'simdik_batam.view.RekapPtkJenisKelamin.RekapPtkJenisKelamin',
        'simdik_batam.view.RekapPtkJenisKelamin.chartPiePtkJenisKelamin',
        'simdik_batam.view.RekapPtkJenisKelamin.chartBarPtkJenisKelamin',
        'simdik_batam.view.RekapPtkJenisKelamin.PtkJenisKelaminGrid',
        'simdik_batam.view.RekapPtkJenisKelamin.PtkJenisKelaminTotalGrid',
        'simdik_batam.view.RekapPtkAgama.RekapPtkAgama',
        'simdik_batam.view.RekapPtkAgama.chartPiePtkAgama',
        'simdik_batam.view.RekapPtkAgama.chartBarPtkAgama',
        'simdik_batam.view.RekapPtkAgama.PtkAgamaGrid',
        'simdik_batam.view.RekapPtkAgama.PtkAgamaTotalGrid',
        'simdik_batam.view.RekapPtkStatusKepegawaian.RekapPtkStatusKepegawaian',
        'simdik_batam.view.RekapPtkStatusKepegawaian.PtkStatusKepegawaianGrid',
        'simdik_batam.view.RekapPtkStatusKepegawaian.PtkStatusKepegawaianTotalGrid',
        'simdik_batam.view.RekapPtkStatusKepegawaian.chartPiePtkStatusKepegawaian',
        'simdik_batam.view.RekapPtkStatusKepegawaian.chartBarPtkStatusKepegawaian',
        'simdik_batam.view.Mutasi.Mutasi',
        'simdik_batam.view.Mutasi.MutasiGrid',
        'simdik_batam.view.Mutasi.MutasiPesertaDidik',
        'simdik_batam.view.Mutasi.MutasiPesertaDidikForm',
        'simdik_batam.view.RekapSekolahStatus.RekapSekolahStatus',
        'simdik_batam.view.RekapSekolahStatus.chartPieSekolahStatus',
        'simdik_batam.view.RekapSekolahStatus.chartBarSekolahStatus',
        'simdik_batam.view.RekapSekolahStatus.SekolahStatusGrid',
        'simdik_batam.view.RekapSekolahStatus.SekolahStatusTotalGrid',
        'simdik_batam.view.RekapSekolahBentukPendidikan.RekapSekolahBentukPendidikan',
        'simdik_batam.view.RekapSekolahBentukPendidikan.chartPieSekolahBentukPendidikan',
        'simdik_batam.view.RekapSekolahBentukPendidikan.chartBarSekolahBentukPendidikan',
        'simdik_batam.view.RekapSekolahBentukPendidikan.SekolahBentukPendidikanTotalGrid',
        'simdik_batam.view.RekapSekolahBentukPendidikan.SekolahBentukPendidikanGrid',
        'simdik_batam.view.RekapPrasaranaJenis.RekapPrasaranaJenis',
        'simdik_batam.view.RekapPrasaranaJenis.PrasaranaJenisGrid',
        'simdik_batam.view.DataPokokPrasarana.DataPokokPrasarana',
        'simdik_batam.view.DataPokokPrasarana.FilterPrasaranaForm',
        'simdik_batam.view.DataPokokPrasarana.DataPokokPrasaranaGrid',
        'simdik_batam.view.RekapSekolahWaktuPenyelenggaraan.RekapSekolahWaktuPenyelenggaraan',
        'simdik_batam.view.RekapSekolahWaktuPenyelenggaraan.SekolahWaktuPenyelenggaraanTotalGrid',
        'simdik_batam.view.RekapSekolahWaktuPenyelenggaraan.SekolahWaktuPenyelenggaraanGrid',
        'simdik_batam.view.RekapSekolahWaktuPenyelenggaraan.chartPieSekolahWaktuPenyelenggaraan',
        'simdik_batam.view.RekapSekolahWaktuPenyelenggaraan.chartBarSekolahWaktuPenyelenggaraan',
        'simdik_batam.view.RekapPtkJenisPtk.RekapPtkJenisPtk',
        'simdik_batam.view.RekapPtkJenisPtk.PtkJenisPtkTotalGrid',
        'simdik_batam.view.RekapPtkJenisPtk.PtkJenisPtkGrid',
        'simdik_batam.view.RekapPtkJenisPtk.chartPiePtkJenisPtk',
        'simdik_batam.view.RekapPtkJenisPtk.chartBarPtkJenisPtk',
        'simdik_batam.view.RekapPtkMataPelajaran.RekapPtkMataPelajaran',
        'simdik_batam.view.RekapPtkMataPelajaran.PtkMataPelajaranTotalGrid',
        'simdik_batam.view.RekapPtkMataPelajaran.PtkMataPelajaranGrid',
        'simdik_batam.view.RekapPtkMataPelajaran.chartBarPtkMataPelajaran',
        'simdik_batam.view.RekapPdPerUmur.RekapPdPerUmur',
        'simdik_batam.view.RekapPdPerUmur.PdUmurGrid',
        'simdik_batam.view.RekapPdAngkaMengulang.RekapPdAngkaMengulang',
        'simdik_batam.view.RekapPdAngkaMengulang.PdAngkaMengulangGrid',
        'simdik_batam.view.RekapPdAngkaMengulang.chartBarPdAngkaMengulang',
        'simdik_batam.view.RekapPdAngkaPutusSekolah.RekapPdAngkaPutusSekolah',
        'simdik_batam.view.RekapPdAngkaPutusSekolah.PdAngkaPutusSekolahGrid',
        'simdik_batam.view.RekapPdAngkaPutusSekolah.chartBarPdAngkaPutusSekolah',
        'simdik_batam.view.RekapPdPerJumlahRombel.RekapPdPerJumlahRombel',
        'simdik_batam.view.RekapPdPerJumlahRombel.PdJumlahRombelGrid',
        'simdik_batam.view.ApkApm.ApkApm',
        'simdik_batam.view.ApkApm.ApkApmForm',
        'simdik_batam.view.AnalisisKebutuhanGuruDanKelasSD.AnalisisKebutuhanGuruDanKelasSD',
        'simdik_batam.view.AnalisisKebutuhanGuruDanKelasSD.AnalisisKebutuhanGuruDanKelasSDGrid',
        'simdik_batam.view.AnalisisKebutuhanGuruDanKelasSMP.AnalisisKebutuhanGuruDanKelasSMP',
        'simdik_batam.view.AnalisisKebutuhanGuruDanKelasSMP.AnalisisKebutuhanGuruDanKelasSMPGrid',
        'simdik_batam.view.AnalisisKebutuhanGuruDanKelasSMA.AnalisisKebutuhanGuruDanKelasSMA',
        'simdik_batam.view.AnalisisKebutuhanGuruDanKelasSMA.AnalisisKebutuhanGuruDanKelasSMAGrid',
        'simdik_batam.view.AnalisisKebutuhanPrasaranaSD.AnalisisKebutuhanPrasaranaSD',
        'simdik_batam.view.AnalisisKebutuhanPrasaranaSD.AnalisisKebutuhanPrasaranaSDGrid',
        'simdik_batam.view.AnalisisKebutuhanPrasaranaSMP.AnalisisKebutuhanPrasaranaSMP',
        'simdik_batam.view.AnalisisKebutuhanPrasaranaSMP.AnalisisKebutuhanPrasaranaSMPGrid',
        'simdik_batam.view.AnalisisKebutuhanPrasaranaSMA.AnalisisKebutuhanPrasaranaSMA',
        'simdik_batam.view.AnalisisKebutuhanPrasaranaSMA.AnalisisKebutuhanPrasaranaSMAGrid',
        'simdik_batam.view.RekapPdAngkaSiswaMiskin.RekapPdAngkaSiswaMiskin',
        'simdik_batam.view.RekapPdAngkaSiswaMiskin.chartBarPdAngkaSiswaMiskin',
        'simdik_batam.view.RekapPdAngkaSiswaMiskin.PdAngkaSiswaMiskinGrid',
        'simdik_batam.view.PeringkatPengiriman.PeringkatPengiriman',
        'simdik_batam.view.PeringkatPengiriman.PeringkatPengirimanGrid',
        'simdik_batam.view.RekapPdTingkatKelasJenisKelamin.RekapPdTingkatKelasJenisKelamin',
        'simdik_batam.view.RekapPdTingkatKelasJenisKelamin.PdTingkatKelasJenisKelaminGrid',
        'simdik_batam.view.RekapSekolahRangkuman.RekapSekolahRangkuman',
        'simdik_batam.view.RekapSekolahRangkuman.SekolahRangkumanGrid',
        'simdik_batam.view.RekapSekolahRangkumanSp.RekapSekolahRangkumanSp',
        'simdik_batam.view.RekapSekolahRangkumanSp.SekolahRangkumanSpGrid',
        'simdik_batam.view.ProgresData.ProgresData',
        'simdik_batam.view.ProgresData.ProgresDataGrid',
        'simdik_batam.view.RekapPdTingkatKelasJenisKelaminSp.RekapPdTingkatKelasJenisKelaminSp',
        'simdik_batam.view.RekapPdTingkatKelasJenisKelaminSp.PdTingkatKelasJenisKelaminSpGrid',
        'simdik_batam.view.Beranda.BeritaGrid',
        'simdik_batam.view.CustomReporting.CustomReporting',
        'simdik_batam.view.CustomReporting.CustomCriteria',
        'simdik_batam.view.CustomReporting.CustomTable',
        'simdik_batam.view.CustomReporting.TabelCombo',
        'simdik_batam.view.CustomReporting.CriteriaSekolahCombo',
        'simdik_batam.view.CustomReporting.ValueSekolahStatusSekolahCombo',
        'simdik_batam.view.CustomReporting.ValueSekolahBentukPendidikanCombo',
        'simdik_batam.view.CustomReporting.ValueSekolahStatusKepemilikanCombo',
        'simdik_batam.view.CustomReporting.ResultGrid',
        'simdik_batam.view.CustomReporting.GroupCombo',
        'simdik_batam.view.CustomReporting.ValueBooleanCombo',
        'simdik_batam.view.CustomReporting.QueryForm'
    ],
    xtype: 'app-main',

    id: 'app-main',
    
    viewModel: {
        type: 'main'
    },

    layout: {
        type: 'border'
    },

    items: [{
        id: 'app-header',
        xtype: 'box',
        region: 'north',
        height: 55,
        html: '<div style="background: linear-gradient(to right, #023257 , #74c3ff);height:100%">' 
                + '<div style="float:left;margin-top:5px;margin-left:10px">'
                    +'<img src="resources/img/logo.png" width="35px"/>'
                +'</div>'
                +'<div style="color:white;float:left;margin:15px;font-size:35px;font-weight:none" class="ubuntu">'
                    // + 'Profil Dikdas Belitung'
                    + '---'
                +'</div>'
                +'<div style="color:white;float:left;margin-top:25px;font-size:12px;font-weight:none" class="ubuntu">'
                    +'Versi 0.1.0'
                +'</div>'
                + '<div style="float:right;margin-top:5px;margin-right:10px">'
                +'</div>'
                +'<div style="clear:both"></div>'
            +'</div>'

    },{
        xtype:'panel',
        bind: {
            title: '{item}'
        },
        region: 'west',
        width: 280,
        split: true,
        itemId:'MenuPanel',
        border:true,
        collapsible:true,
        layout:'border',
        items:[{
            xtype: 'treemenu',
            region:'center',
            width:200
        }]
    },{
        region: 'center',
        xtype: 'tabpanel',
        itemId: 'ContentPanel',
        id:'ContentPanel',
        plugins: Ext.create('Ext.ux.TabReorderer'),
        items:[{
            title: 'Beranda',
            id:'Beranda',
            xtype:'berandapanel',
            reorderable: false
        }],
        listeners: {
            tabchange: 'onTabChange'
        }
    }]
});
