/**
 * This class is the view model for the Main view of the application.
 */
Ext.define('simdik_batam.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',

    alias: 'viewmodel.main',

    data: {
        name: 'Simdik Batam',
        item: 'Menu'
    }

    //TODO - add data, formulas and/or methods to support your view
});