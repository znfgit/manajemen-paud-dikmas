
var criteriaSekolahStore = Ext.create('Ext.data.Store', {
    fields: ['criteria_sekolah_id', 'nama'],
    data : [
        {"criteria_sekolah_id":"status_sekolah", "nama":"Status Sekolah"},
        {"criteria_sekolah_id":"bentuk_pendidikan_id", "nama":"Bentuk Pendidikan"},
        {"criteria_sekolah_id":"status_kepemilikan_id", "nama":"Status Kepemilikan"},
        {"criteria_sekolah_id":"wilayah_terpencil", "nama":"Wilayah Terpencil"},
        {"criteria_sekolah_id":"wilayah_perbatasan", "nama":"Wilayah Perbatasan"},
        {"criteria_sekolah_id":"wilayah_transmigrasi", "nama":"Wilayah Transmigrasi"},
        {"criteria_sekolah_id":"wilayah_adat_terpencil", "nama":"Wilayah Adat Terpencil"},
        {"criteria_sekolah_id":"wilayah_bencana_alam", "nama":"Wilayah Bencana Alam"},
    ]
});

Ext.define('simdik_batam.view.CustomReporting.CriteriaSekolahCombo', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'local',
    valueField: 'criteria_sekolah_id',
    displayField: 'nama',
    label: 'Ref.criteria_sekolah_combo',    
    alias: 'widget.criteriasekolahcombo',
    listeners: {
        'change': function(field, newValue, oldValue) {

            var seqName = this.name + "_nilai";
            var kolomNilai = this.up('container');

            console.log(seqName);
            if(kolomNilai.down('combobox[name='+seqName+']')){
                kolomNilai.down('combobox[name='+seqName+']').destroy();
            }
            
            if(newValue == 'status_sekolah'){

                kolomNilai.add({
                    xtype:'valuesekolahstatussekolahcombo',
                    fieldLabel: 'Nilai',
                    labelAlign:'right',
                    name:seqName
                    // itemId: varStatus;
                });

            }else if(newValue == 'bentuk_pendidikan_id'){
                kolomNilai.add({
                    xtype:'valuesekolahbentukpendidikancombo',
                    fieldLabel: 'Nilai',
                    labelAlign:'right',
                    name:seqName
                    // itemId: varStatus;
                });
            }else if(newValue == 'status_kepemilikan_id'){
                kolomNilai.add({
                    xtype:'valuesekolahstatuskepemilikancombo',
                    fieldLabel: 'Nilai',
                    labelAlign:'right',
                    name:seqName
                    // itemId: varStatus;
                });
            }else if(newValue == 'wilayah_terpencil' 
                || newValue == 'wilayah_transmigrasi' 
                || newValue == 'wilayah_perbatasan' 
                || newValue == 'wilayah_adat_terpencil'
                || newValue == 'wilayah_bencana_alam'){
                kolomNilai.add({
                    xtype:'valuebooleancombo',
                    fieldLabel: 'Nilai',
                    labelAlign:'right',
                    name:seqName
                    // itemId: varStatus;
                });
            }

        }
    },
    initComponent: function() {
        this.store = criteriaSekolahStore;
        
        this.store.load();
        
        this.callParent(arguments); 
    }
});