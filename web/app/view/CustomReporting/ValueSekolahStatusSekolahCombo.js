
var ValueSekolahStatusSekolahComboStore = Ext.create('Ext.data.Store', {
    fields: ['id', 'nama'],
    data : [
        {"id":"1", "nama":"Negeri"},
        {"id":"2", "nama":"Swasta"},
        {"id":"3", "nama":"Semua"},
    ]
});

Ext.define('simdik_batam.view.CustomReporting.ValueSekolahStatusSekolahCombo', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'local',
    valueField: 'id',
    displayField: 'nama',
    label: 'Ref.value_sekolah_status_sekolah_combo',    
    alias: 'widget.valuesekolahstatussekolahcombo',
    listeners: {
        'change': function(field, newValue, oldValue) {
            
        }
    },
    initComponent: function() {
        this.store = ValueSekolahStatusSekolahComboStore;
        
        this.store.load();
        
        this.callParent(arguments); 
    }
});