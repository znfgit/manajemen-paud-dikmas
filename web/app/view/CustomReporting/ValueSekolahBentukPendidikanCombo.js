
var ValueSekolahBentukPendidikanComboStore = Ext.create('Ext.data.Store', {
    fields: ['id', 'nama'],
    data : [
        {"id":"13", "nama":"SMA"},
        {"id":"14", "nama":"SMLB"},
        {"id":"15", "nama":"SMK"},
        {"id":"29", "nama":"SLB"},
        {"id":"999", "nama":"Semua"}
    ]
});

Ext.define('simdik_batam.view.CustomReporting.ValueSekolahBentukPendidikanCombo', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'local',
    valueField: 'id',
    displayField: 'nama',
    label: 'Ref.value_sekolah_bentuk_pendidikan_combo',    
    alias: 'widget.valuesekolahbentukpendidikancombo',
    listeners: {
        'change': function(field, newValue, oldValue) {
            
        }
    },
    initComponent: function() {
        this.store = ValueSekolahBentukPendidikanComboStore;
        
        this.store.load();
        
        this.callParent(arguments); 
    }
});