
var tabelStore = Ext.create('Ext.data.Store', {
    fields: ['id', 'nama'],
    data : [
        {"id":"1", "nama":"Sekolah"},
        {"id":"2", "nama":"PTK"}
    ]
});

Ext.define('simdik_batam.view.CustomReporting.TabelCombo', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'local',
    valueField: 'id',
    displayField: 'nama',
    label: 'Ref.tabel_combo',    
    alias: 'widget.tabelcombo',
    listeners: {
        'change': function(field, newValue, oldValue) {

            this.up('form').up('panel').down('customcriteriaform').removeAll();
            this.up('form').up('panel').down('customcriteriaform').add({
                xtype:'hidden',
                name:'sequenceContainer',
                value:0
            });
            // this.up('form').up('panel').down('customcriteriaform').down('container[itemId=kolomNilai]').removeAll();
            // this.up('form').up('panel').down('customcriteriaform').down('container[itemId=kolomChecklist]').removeAll();

            var seqCon = this.up('form').up('panel').down('customcriteriaform').down('hidden[name=sequenceContainer]');

            seqCon.setValue(0);

            var sequence = seqCon.getValue();

            var raw = this.getRawValue();

            this.up('form').up('panel').down('customcriteriaform').setTitle('Kriteria ' + raw);

            console.log(sequence);

            if(newValue == 1){
                this.up('form').up('panel').down('customcriteriaform').add({
                    name: sequence + '_container',
                    xtype:'container',
                    layout: {
                        type: 'hbox',
                        padding:'5',
                        align:'top'
                    },
                    defaults:{margin:'0 10 0 0'}
                });

                this.up('form').up('panel').down('customcriteriaform').down('container[name='+sequence+'_container]').add({
                    xtype:'checkboxfield',
                    name: sequence + '_checkbox',
                    checked:true
                });

                this.up('form').up('panel').down('customcriteriaform').down('container[name='+sequence+'_container]').add({
                    xtype:'criteriasekolahcombo',
                    fieldLabel: 'Kriteria',
                    name: sequence + '_combo',
                    labelAlign:'right'
                }); 
                // console.log(this.up('form').up('panel').down('customcriteriaform').down('container[itemId=kolomKriteria]').down('criteriasekolahcombo'));
            }

            seqCon.setValue(parseInt(sequence)+1);
            console.log(seqCon.getValue());
        }
    },
    initComponent: function() {
        this.store = tabelStore;
        
        this.store.load();
        
        this.callParent(arguments); 
    }
});