
var ValueSekolahStatusKepemilikanComboStore = Ext.create('Ext.data.Store', {
    fields: ['id', 'nama'],
    data : [
        {"id":"1", "nama":"Pemerintah Pusat"},
        {"id":"2", "nama":"Pemerintah Daerah"},
        {"id":"3", "nama":"Yayasan"},
        {"id":"4", "nama":"Lainnya"}
    ]
});

Ext.define('simdik_batam.view.CustomReporting.ValueSekolahStatusKepemilikanCombo', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'local',
    valueField: 'id',
    displayField: 'nama',
    label: 'Ref.value_sekolah_status_kepemilikan_combo',    
    alias: 'widget.valuesekolahstatuskepemilikancombo',
    listeners: {
        'change': function(field, newValue, oldValue) {
            
        }
    },
    initComponent: function() {
        this.store = ValueSekolahStatusKepemilikanComboStore;
        
        this.store.load();
        
        this.callParent(arguments); 
    }
});