Ext.define('simdik_batam.view.CustomReporting.CustomCriteria', {
	extend: 'Ext.form.Panel',
	alias: 'widget.customcriteriaform',
    border: false,
    bodyBorder: false,
    layout: {
        type:'vbox',
        padding:'5',
        align:'left'
    },
    // defaults: {
    //     layout: 'form',
    //     xtype: 'container',
    //     defaultType: 'textfield',
    //     style: 'width: 30%'
    // },
    
    initComponent:function(){

        var form = this;

    	this.items = [{
            xtype:'hidden',
            name:'sequenceContainer',
            value:0
        }
        // ,{
        //     itemId: 'kolomKriteria'
        // },{
        //     itemId: 'kolomNilai'
        // },{
        //     itemId: 'kolomChecklist'
        // }
        ];

        this.dockedItems = [{
            xtype:'toolbar',
            dock:'bottom',
            items:[{
                text: 'Tambah Kriteria',
                handler:function(btn){
                    var sequence = btn.up('panel').down('hidden[name=sequenceContainer]').getValue();
                    console.log(sequence);

                    var tabel = btn.up('panel').up('panel').down('customtableform').down('tabelcombo').getValue();
                    console.log(tabel);

                    if(tabel == 1){
                        btn.up('form').add({
                            name: sequence + '_container',
                            xtype:'container',
                            layout: {
                                type: 'hbox',
                                padding:'5',
                                align:'top'
                            },
                            defaults:{margin:'0 10 0 0'}
                            // defaults: {
                            //     layout: 'form',
                            //     xtype: 'container',
                            //     defaultType: 'textfield',
                            //     style: 'width: 30%',
                            //     border:true
                            // },
                        });

                        btn.up('form').down('container[name='+sequence+'_container]').add({
                            xtype:'checkboxfield',
                            name: sequence + '_checkbox',
                            checked:true
                        });

                        btn.up('form').down('container[name='+sequence+'_container]').add({
                            xtype:'criteriasekolahcombo',
                            fieldLabel: 'Kriteria',
                            name: sequence + '_combo',
                            labelAlign:'right'
                        });
                        // console.log(this.up('form').up('panel').down('customcriteriaform').down('container[itemId=kolomKriteria]').down('criteriasekolahcombo'));
                    }

                    btn.up('panel').down('hidden[name=sequenceContainer]').setValue(parseInt(sequence)+1);
                    console.log(btn.up('panel').down('hidden[name=sequenceContainer]').getValue());
                }
            },{
                text: 'Tampilkan',
                handler:function(btn){
                    Ext.MessageBox.show({
                        msg: 'Mengolah Data ...',
                        progressText: 'Mengolah Data ...',
                        width: 200,
                        wait: true
                    });

                    var sequence = form.down('hidden[name=sequenceContainer]').getValue();
                    var str = '';

                    var tabel = form.up('panel').down('customtableform').down('tabelcombo').getValue();
                    var checkboxAgregat = form.up('panel').down('customtableform').down('checkboxfield').getValue();
                    var kriteriaAgregat = form.up('panel').down('customtableform').down('groupcombo').getValue() ? form.up('panel').down('customtableform').down('groupcombo').getValue() : 0;

                    if(tabel == 1){
                        str += 'tabel=sekolah';
                    }else if(tabel == 2){
                        str += 'tabel=peserta_didik';
                    }

                    if(checkboxAgregat){
                        str += '&agregat=true';
                        str += '&kriteria_agregat='+kriteriaAgregat;
                    }else{
                        str += '&agregat=false';
                    }



                    for (var i = 0; i <= sequence - 1; i++) {

                        if(form.down('container[name='+ i +'_container]').down('checkboxfield[name='+ i +'_checkbox]').getValue()){
                            str += '&' + form.down('container[name='+ i +'_container]').down('combobox[name='+ i +'_combo]').getValue() + '='+ form.down('container[name='+ i +'_container]').down('combobox[name='+ i +'_combo_nilai]').getValue();
                        }
                        // arr[form.down('container[itemId=kolomKriteria]').down('combobox[name='+ i +'_combo]').getValue()] = form.down('container[itemId=kolomNilai]').down('combobox[name='+ i +'_combo]').getValue();

                    };

                    console.log(str);

                    Ext.Ajax.request({
                        url: 'get/customReporting?' + str,
                        success: function(response){
                            var text = response.responseText;
                            var __columns = [];
                
                            var res = Ext.JSON.decode(response.responseText);

                            var resultGrid = form.up('panel').up('tabpanel').up('panel').down('resultgrid');

                            // resultGrid.setTitle('testing');

                            for (var i = 0; i <= res.kolom.length - 1; i++) {
                                if(res.kolom[i] == 'nama'){
                                    var lock = true;
                                }else{
                                    var lock = false;
                                }

                                if(res.kolom[i] == 'jumlah'){
                                    var align_str = 'right';
                                }else{
                                    var align_str = 'left';
                                }
                                

                                var kolom = Ext.create('Ext.grid.column.Column', {
                                    header: res.kolom[i],
                                    width: 150,
                                    sortable: true,
                                    locked: lock,
                                    align: align_str,
                                    dataIndex: res.kolom[i],
                                    hideable: false,
                                    align:'left',
                                    // renderer:function(v,p,r){
                                    //     return grid.formatMoney(Math.abs(v),0);
                                    // }
                                });
                                // resultGrid.headerCt.add(kolom);
                                __columns.push(kolom);

                            };

                            var gridStore = Ext.create('Ext.data.Store', {
                                fields: res.kolom,
                                data : res.rows
                            });

                            // console.log(text);
                            resultGrid.reconfigure(gridStore, __columns);
                            // resultGrid.down('toolbar').down('pagingtoolbar').destroy();
                            // resultGrid.down('toolbar').add({
                            //     xtype: 'pagingtoolbar',
                            //     displayInfo: true,
                            //     displayMsg: 'Displaying data {0} - {1} of {2}',
                            //     emptyMsg: "Tidak ada data"
                            // })

                            Ext.MessageBox.hide();
                        },
                        failure : function(response, options){
                            Ext.MessageBox.hide();
                            Ext.Msg.alert('Warning','Ada kesalahan dalam pemrosesan data');
                        }
                    });
                }
            }]
        }];

    	this.callParent(arguments);
    }
});