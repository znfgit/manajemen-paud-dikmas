
var criteriaSekolahStore = Ext.create('Ext.data.Store', {
    fields: ['criteria_sekolah_id', 'nama'],
    data : [
        {"criteria_sekolah_id":"1", "nama":"Status Sekolah"}
    ]
});

Ext.define('simdik_batam.view.CustomReporting.CriteriaSekolahCombo', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'local',
    valueField: 'criteria_sekolah_id',
    displayField: 'nama',
    label: 'Ref.criteria_sekolah_combo',    
    alias: 'widget.criteriasekolahcombo',
    listeners: {
        'change': function(field, newValue, oldValue) {
            
        }
    },
    initComponent: function() {
        this.store = criteriaSekolahStore;
        
        this.store.load();
        
        this.callParent(arguments); 
    }
});