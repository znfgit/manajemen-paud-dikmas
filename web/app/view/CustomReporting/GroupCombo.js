
var GroupStore = Ext.create('Ext.data.Store', {
    fields: ['id', 'nama'],
    data : [
        {"id":"0", "nama":"Propinsi"},
        {"id":"1", "nama":"Kabupaten"},
        {"id":"2", "nama":"Kecamatan"}
    ]
});

Ext.define('simdik_batam.view.CustomReporting.GroupCombo', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'local',
    valueField: 'id',
    displayField: 'nama',
    label: 'Ref.group_combo',    
    alias: 'widget.groupcombo',
    emptyText:'Pilihan kriteria agregasi...',
    allowBlank:true,
    initComponent: function() {
        this.store = GroupStore;
        
        this.store.load();
        
        this.callParent(arguments); 
    }
});