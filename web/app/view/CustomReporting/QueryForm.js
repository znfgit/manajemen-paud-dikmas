Ext.define('simdik_batam.view.CustomReporting.QueryForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.queryform',
    bodyPadding: 10,
    autoScroll:true,
    fieldDefaults: {
        anchor: '100%',
        labelWidth: 120
    },
    initComponent: function() {
        
        
        this.items = [{
            xtype: 'textareafield'
            ,fieldLabel: 'Query'
            ,labelAlign: 'top'
            ,allowBlank: true
            ,name: 'query'
            ,emptyText: 'Ketik Query...'
            ,height:150,
            fieldStyle: {
                'fontFamily'   : 'courier new',
                'fontSize'     : '13px'
            }
        }];

        this.buttons = [{
            text: 'Proses Query',
            handler: function(btn){
                Ext.MessageBox.show({
                    msg: 'Mengolah Data ...',
                    progressText: 'Mengolah Data ...',
                    width: 200,
                    wait: true
                });

                var form = btn.up('form');
                var values = form.getForm().getValues();
                var str = '';
                // console.log(values.query);

                str += 'tabel=query';
                str += '&query=' +  values.query;

                Ext.Ajax.request({
                    url: 'get/customReporting?' + str,
                    success: function(response){
                        var text = response.responseText;
                        var __columns = [];
            
                        var res = Ext.JSON.decode(response.responseText);

                        var resultGrid = form.up('tabpanel').up('panel').down('resultgrid');

                        // resultGrid.setTitle('testing');

                        for (var i = 0; i <= res.kolom.length - 1; i++) {
                            if(res.kolom[i] == 'nama'){
                                var lock = true;
                            }else{
                                var lock = false;
                            }

                            if(res.kolom[i] == 'jumlah'){
                                var align_str = 'right';
                            }else{
                                var align_str = 'left';
                            }
                            

                            var kolom = Ext.create('Ext.grid.column.Column', {
                                header: res.kolom[i],
                                width: 150,
                                sortable: true,
                                locked: lock,
                                align: align_str,
                                dataIndex: res.kolom[i],
                                hideable: false,
                                align:'left',
                                // renderer:function(v,p,r){
                                //     return grid.formatMoney(Math.abs(v),0);
                                // }
                            });
                            // resultGrid.headerCt.add(kolom);
                            __columns.push(kolom);

                        };

                        var gridStore = Ext.create('Ext.data.Store', {
                            fields: res.kolom,
                            data : res.rows
                        });

                        // console.log(text);
                        resultGrid.reconfigure(gridStore, __columns);
                        // resultGrid.down('toolbar').down('pagingtoolbar').destroy();
                        // resultGrid.down('toolbar').add({
                        //     xtype: 'pagingtoolbar',
                        //     displayInfo: true,
                        //     displayMsg: 'Displaying data {0} - {1} of {2}',
                        //     emptyMsg: "Tidak ada data"
                        // })

                        Ext.MessageBox.hide();
                    },
                    failure : function(response, options){
                        Ext.MessageBox.hide();
                        Ext.Msg.alert('Warning','Ada kesalahan dalam pemrosesan data');
                    }
                });
            }
        }];

        this.callParent(arguments);
    }
});