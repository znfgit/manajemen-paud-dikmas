Ext.define('simdik_batam.view.CustomReporting.CustomTable', {
    extend: 'Ext.form.Panel',
    alias: 'widget.customtableform',
    bodyPadding: 10,
    fieldDefaults: {
        anchor: '100%',
        labelWidth: 120
    },
    initComponent: function() {
        
        this.items = [{
            xtype: 'tabelcombo'
            ,labelAlign: 'top'
            ,allowBlank: false
            ,width: 200
            ,name: 'customTableCombo'
        },{
            xtype:'checkboxfield',
            name: 'checkbox_group',
            checked:false,
            boxLabel: 'Tampilkan dalam bentuk agregat',
            listeners: {
                'change': function(field, newValue, oldValue){
                    if(newValue == true){
                        this.up('form').down('groupcombo').enable();
                    }else{
                        this.up('form').down('groupcombo').disable();
                    }
                }
            }
        },{
            xtype: 'groupcombo'
            ,labelAlign: 'top'
            ,allowBlank: true
            ,disabled:true
            ,width: 200
            ,name: 'customGroupCombo'
        }];

        this.callParent(arguments);
    }
});