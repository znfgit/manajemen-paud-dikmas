Ext.define('simdik_batam.view.CustomReporting.CustomReporting', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.customreporting',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

    	this.items = [{
            xtype: 'tabpanel',
            region:'north',
            collapsible:false,
            split:true,
            border:true,
            flex: 1,
            items:[{
                xtype: 'panel',
                title: 'Custom Reporting',
                split:true,
                border:true,
                layout:'border',
                items:[{
                    xtype: 'customcriteriaform',
                    region:'center',
                    title:'Kriteria',
                    autoScroll:true
                },{
                    xtype:'customtableform',
                    region:'west',
                    width:250,
                    split:true,
                    collapsible:true,
                    border:true,
                    title: 'Tabel'
                }]
            },{
                xtype:'queryform',
                title: 'Custom Query'
            }]
        },{
            xtype: 'resultgrid',
            title: 'Hasil',
            region:'center'
        }];

    	this.callParent(arguments);
    }
});