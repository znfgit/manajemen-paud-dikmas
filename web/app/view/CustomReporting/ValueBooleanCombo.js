
var Store = Ext.create('Ext.data.Store', {
    fields: ['id', 'nama'],
    data : [
        {"id":"0", "nama":"Tidak"},
        {"id":"1", "nama":"Ya"}
    ]
});

Ext.define('simdik_batam.view.CustomReporting.ValueBooleanCombo', {
    extend: 'Ext.form.field.ComboBox',
    queryMode: 'local',
    valueField: 'id',
    displayField: 'nama',
    label: 'Ref.value_boolean_combo',    
    alias: 'widget.valuebooleancombo',
    listeners: {
        'change': function(field, newValue, oldValue) {
            
        }
    },
    initComponent: function() {
        this.store = Store;
        
        this.store.load();
        
        this.callParent(arguments); 
    }
});