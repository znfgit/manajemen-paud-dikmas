Ext.define('simdik_batam.view.RekapPtkJenisPtk.RekapPtkJenisPtk', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.rekapptkjenisptk',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

        this.items = [{
            xtype: 'panel',
            region:'east',
            flex: 1,
            split:true,
            collapsible:true,
            autoScroll:true,
            layout: {
                type:'vbox',
                padding:'0',
                align:'stretch'
            },
            defaults:{margin:'0 0 10 0'},
            items:[{
                xtype:'panel',
                title: 'Grafik Jumlah PTK Berdasarkan Jenis PTK',
                itemId: 'chartpie',
                split: true,
                layout:'fit',
                border:false,
                items:[{
                    xtype: 'chartpieptkjenisptk',
                    height:500
                }]
            },{
                xtype:'panel',
                title: 'Grafik Sebaran Jumlah PTK Berdasarkan Jenis PTK',
                itemId: 'chartbar',
                split: true,
                layout:'fit',
                border:false,
                items:[{
                    xtype: 'chartbarptkjenisptk',
                    minHeight:500
                }]
            }]
        },{
            xtype: 'panel',
            region: 'center',
            layout:'border',
            title:'Jumlah PTK Berdasarkan Jenis PTK',
            items:[{
                xtype: 'ptkjenisptktotalgrid',
                region:'center'
            },{
                xtype: 'ptkjenisptkgrid',
                region: 'south',
                height:250,
                title:'Tabel Sebaran Jumlah PTK Berdasarkan Jenis PTK'
            }]
        }];

        this.callParent(arguments);
    }
});