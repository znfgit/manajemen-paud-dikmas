Ext.define('simdik_batam.view.RekapPtkJenisPtk.chartPiePtkJenisPtk', {
    extend: 'Ext.Panel',
    alias: 'widget.chartpieptkjenisptk',
    width: 750,

    initComponent: function() {
        var me = this;

        this.myDataStore = Ext.create('simdik_batam.store.ChartPiePtkPerJenisPtk');


        me.items = [{
            xtype: 'polar',
            width: '100%',
            height: 500,
            store: this.myDataStore,
            insetPadding: 5,
            innerPadding: 30,
            legend: {
                docked: 'bottom'
            },
            interactions: ['rotate', 'itemhighlight'],
            sprites: [{
                type: 'text',
                text: 'Sumber: Dapodik 2014/2015',
                font: '12px Helvetica',
                width: 100,
                height: 30,
                x: 40, // the sprite x position
                y: 20  // the sprite y position
            }],
            series: [{
                type: 'pie',
                angleField: 'jumlah',
                label: {
                    enabled:false,
                    field: 'desk',
                    display: 'inside',
                    calloutLine: {
                        length: 30,
                        width: 2
                        // specifying 'color' is also possible here
                    }
                },
                highlight: true,
                tooltip: {
                    trackMouse: true,
                    renderer: function(storeItem, item) {

                        this.setHtml(storeItem.get('desk') + ': ' + storeItem.get('jumlah'));
                    }
                }
            }]
        }];

        this.myDataStore.load();

        this.callParent();
    }
});