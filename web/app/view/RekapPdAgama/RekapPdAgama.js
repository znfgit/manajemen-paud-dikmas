Ext.define('simdik_batam.view.RekapPdAgama.RekapPdAgama', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.rekappdagama',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

        this.items = [{
            xtype: 'panel',
            region:'east',
            split:true,
            flex:1,
            collapsible:true,
            autoScroll:true,
            layout: {
                type:'vbox',
                padding:'0',
                align:'stretch'
            },
            defaults:{margin:'0 0 10 0'},
            items:[{
                xtype:'panel',
                title: 'Grafik Jumlah PD Berdasarkan Agama',
                itemId: 'chartpie',
                split: true,
                layout:'fit',
                border:false,
                items:[{
                    xtype: 'chartpiepdagama',
                    height:300
                }]
            },{
                xtype:'panel',
                title: 'Grafik Sebaran Jumlah PD Berdasarkan Agama',
                itemId: 'chartbar',
                split: true,
                layout:'fit',
                border:false,
                items:[{
                    xtype: 'chartbarpdagama',
                    minHeight:500
                }]
            }]
        },{
            xtype: 'panel',
            region: 'center',
            layout:'border',
            title:'Jumlah PD Berdasarkan Agama',
            items:[{
                xtype: 'pdagamatotalgrid',
                region:'center'
            },{
                xtype: 'pdagamagrid',
                region: 'south',
                height:280,
                title:'Tabel Sebaran Jumlah PD Berdasarkan Agama'
            }]
        }];

        this.callParent(arguments);
    }
});