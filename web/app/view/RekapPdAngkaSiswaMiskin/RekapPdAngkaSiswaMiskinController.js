Ext.define('simdik_batam.view.RekapPdAngkaSiswaMiskin.RekapPdAngkaSiswaMiskinController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.MessageBox'
    ],

    alias: 'controller.rekappdangkasiswamiskin',

    setupPdAngkaSiswaMiskinGrid:function(grid){
    	grid.getStore().on('beforeload', function(store){
            // var keyword = grid.up('panel').down('ptkmatapelajarantotalgrid').down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword
            });
        });

    	grid.getStore().load({
            scope:this,
            callback:function(records, operation, success) {
                 // the operation object
                 // contains all of the details of the load operation
                console.log(success);
                grid.up('panel').up('panel').down('panel[region=south]').down('chartbarpdangkasiswamiskin').down('cartesian').getStore().reload();
            }
        });
    }
});