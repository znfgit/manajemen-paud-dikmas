Ext.define('simdik_batam.view.RekapPdAngkaSiswaMiskin.RekapPdAngkaSiswaMiskin', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.pdangkasiswamiskinpanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

    	this.items = [{
            // xtype: 'ptkstatuskepegawaiangrid',
            xtype:'panel',
            region:'center',
            split:true,
            layout:'border',
            items:[{
                xtype:'pdangkasiswamiskingrid',
                region:'center',
                title: 'Sebaran Angka Siswa Miskin'
            }]
        },{
            xtype: 'panel',
            region:'south',
            height:300,
            collapsible:true,
            split:true,
            autoScroll:true,
            title: 'Grafik Sebaran Angka Siswa Miskin',
            layout: {
                type:'fit',
                padding:'0',
                align:'stretch'
            },
            defaults:{margin:'0 0 10 0'},
            items:[{
                xtype: 'chartbarpdangkasiswamiskin',
                itemId: 'chartbar',
                split: true,
                layout:'fit',
                border:false
            }]
        }];

    	this.callParent(arguments);
    }
});