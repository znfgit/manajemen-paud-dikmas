Ext.define('simdik_batam.view.RekapPdAngkaSiswaMiskin.chartBarPdAngkaSiswaMiskin', {
    extend: 'Ext.Panel',
    xtype: 'chartbarpdangkasiswamiskin',
    autoScroll:true,

    // width: 650,

    initComponent: function() {
        var me = this;

        this.myDataStore = Ext.create('simdik_batam.store.ChartBarPesertaDidikAngkaSiswaMiskin');

        me.items = [{
            xtype: 'cartesian',
            width: '100%',
            height: 250,
            legend: {
                docked: 'left'
            },
            interactions: ['itemhighlight'],
            store: this.myDataStore,
            insetPadding: {
                top: 40,
                left: 40,
                right: 40,
                bottom: 40
            },
            // sprites: [{
            //     type: 'text',
            //     text: 'Column Charts - Stacked Columns',
            //     font: '22px Helvetica',
            //     width: 100,
            //     height: 30,
            //     x: 40, // the sprite x position
            //     y: 20  // the sprite y position
            // }, {
            //     type: 'text',
            //     text: 'Data: Browser Stats 2012',
            //     font: '10px Helvetica',
            //     x: 12,
            //     y: 380
            // }, {
            //     type: 'text',
            //     text: 'Source: http://www.w3schools.com/',
            //     font: '10px Helvetica',
            //     x: 12,
            //     y: 390
            // }],
            axes: [{
                type: 'numeric',
                position: 'left',
                adjustByMajorUnit: true,
                grid: true,
                fields: ['jumlah'],
                minimum: 0
            }, {
                type: 'category',
                position: 'bottom',
                grid: true,
                fields: ['desk'],
                label: {
                    rotate: {
                        degrees: 0
                    }
                }
                
            }],
            series: [{
                type: 'bar',
                axis: 'left',
                title: [ 
                    'Jumlah'
                ],
                xField: 'desk',
                yField: [ 
                    'jumlah'
                ],
                stacked: true,
                style: {
                    opacity: 0.80
                },
                highlight: {
                    fillStyle: 'yellow'
                },
                tooltip: {
                    style: 'background: #fff',
                    renderer: function(storeItem, item) {
                        var browser = item.series.getTitle()[Ext.Array.indexOf(item.series.getYField(), item.field)];
                        this.setHtml(browser + ' di wilayah ' + storeItem.get('desk') + ': ' + storeItem.get(item.field));
                    }
                }
            }]
        }];

        // this.myDataStore.load();

        this.callParent();
    }
});