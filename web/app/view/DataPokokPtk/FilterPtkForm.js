Ext.define('simdik_batam.view.DataPokokPtk.FilterPtkForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.filterptkform',
    bodyPadding: 10,
    autoScroll:true,
    fieldDefaults: {
        anchor: '100%',
        labelWidth: 120
    },
    listeners : {
        afterrender: 'setupFilterPtkForm'
    },
    initComponent: function() {

        var form = this;
        
        var record = this.initialConfig.record ? this.initialConfig.record : false;
        if (record){
            this.listeners = {
                afterrender: function(form, options) {
                    form.loadRecord(record);
                }
            };
        }

        var pilihStatusStore = Ext.create('Ext.data.Store', {
            fields: ['id', 'nama'],
            data : [
                {"id":"1", "nama":"Negeri"},
                {"id":"2", "nama":"Swasta"},
                {"id":"999", "nama":"Semua"}
            ]
        });

        /*this.on('beforeadd', function(form, field){
            if (!field.allowBlank)
              field.labelSeparator += '<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>';
        });*/
        
        this.items = [{
            xtype: 'textfield'
            ,fieldLabel: 'Kata Kunci'
            ,labelAlign: 'top'
            ,allowBlank: true
            ,maxValue: 99999999
            ,minValue: 0
            ,width: 200
            ,maxLength: 100
            ,enforceMaxLength: false
            ,name: 'keyword'
            ,emptyText: 'Ketik Nama / NUPTK...'
        }
        ,{
            xtype: 'mstwilayahcombo',
            labelAlign: 'top',
            anchor: '100%',
            fieldLabel: 'Propinsi',
            name: 'combopropmasterdataptk',
            emptyText: 'Pilih Propinsi',
            listeners: {
                'change': function(field, newValue, oldValue) {
                    var value = newValue;

                    var combokab = form.down('mstwilayahcombo[name=combokabmasterdataptk]');
                    
                    combokab.enable();
                    
                    combokab.setValue('');

                    combokab.getStore().load();  
                }
            }
        },{
            xtype: 'mstwilayahcombo',
            labelAlign: 'top',
            anchor: '100%',
            fieldLabel: 'Kabupaten',
            disabled:true,
            name: 'combokabmasterdataptk',
            emptyText: 'Pilih Kabupaten / Kota',
            listeners: {
                'change': function(field, newValue, oldValue) {
                    var value = newValue;

                    var combokec = form.down('mstwilayahcombo[name=combokecmasterdataptk]');
                    
                    combokec.enable();
                    
                    combokec.setValue('');

                    combokec.getStore().load();  
                }
            }
        },{
            xtype: 'mstwilayahcombo',
            labelAlign: 'top',
            disabled: true,
            fieldLabel: 'Kecamatan',
            name: 'combokecmasterdataptk',
            emptyText: 'Pilih Kecamatan',
            anchor: '100%'
        },{
            xtype:'bentukpendidikancombo',
            name: 'pilihbentukcombo',
            emptyText:'Pilih Bentuk Pendidikan...',
            itemId: 'pilihbentukcombo',
            labelAlign: 'top',
            fieldLabel: 'Bentuk Pendidikan',
            displayField: 'nama',
            anchor: '100%',
            listeners: {
                'change': function(field, newValue, oldValue) {
                    var value = newValue;

                    var combojur = form.down('fieldset[itemId=filter_tambahan]').down('jurusancombo');
                    
                    // combokec.enable();
                    
                    combojur.setValue('');

                    if(value != 999 || value != 993){
                        combojur.getStore().load();

                        if(value == 13){
                            combojur.setFieldLabel('Program Pengajaran');

                        }else if(value == 14 || value == 29){
                            combojur.setFieldLabel('Jenis Ketunaan');

                        }else if(value == 15){
                            combojur.setFieldLabel('Kompetensi Keahlian');
                        }

                    }else{
                        combojur.getStore().load();
                    }  
                }
            }
        },{
            xtype:'combobox',
            name: 'pilihstatuscombo',
            emptyText:'Pilih Status Sekolah...',
            itemId: 'pilihstatuscombo',
            store: pilihStatusStore,
            labelAlign: 'top',
            fieldLabel: 'Status Sekolah',
            queryMode: 'local',
            displayField: 'nama',
            valueField: 'id',
            anchor: '100%'
        },{
            xtype:'displayfield',
            value: ' '
        },
        ,{
            xtype:'fieldset',
            itemId: 'filter_tambahan',
            title:'Filter Tambahan',
            collapsible:true,
            collapsed: false,
            items:[{
                xtype:'jurusancombo',
                emptyText:'Pilih Program Pengajaran / Kompetensi Keahlian...',
                labelAlign: 'top',
                fieldLabel: 'Prg. Pengajaran / Kompetensi Keahlian',
                anchor: '100%',
                listeners: {
                    'afterrender': function(combo){

                        combo.getStore().on('beforeload', function(store){
                            var combobp = form.down('bentukpendidikancombo').getValue();

                            Ext.apply(store.proxy.extraParams, {
                                bp_id: combobp
                            });
                        });
                    }
                }
            },{
                xtype:'jenisptkcombo',
                emptyText: 'Pilih Jenis PTK...',
                labelAlign: 'top',
                fieldLabel: 'Jenis PTK',
                anchor: '100%'
            }]
        }
        ];

        this.buttons = [{
            xtype: 'button',
            text: 'Cari',
            glyph: 61452,
            handler: 'cari'
        }];

        this.callParent(arguments);
    }
});