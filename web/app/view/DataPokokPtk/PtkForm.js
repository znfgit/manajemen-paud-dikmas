Ext.define('simdik_batam.view.DataPokokPtk.PtkForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.ptkform',
    bodyPadding:20,
    autoScroll:true,
    bodyStyle : 'background:#F3FFDE',
    defaults: {
        border: false,
        xtype: 'panel',
        flex: 1,
        layout: 'anchor',
        bodyStyle : 'background:#F3FFDE',
    }, 
    layout: {
        type:'hbox'
    },
    initComponent: function() {
        
        var record = this.initialConfig.record ? this.initialConfig.record : false;
        if (record){
            this.listeners = {
                afterrender: function(form, options) {
                    form.loadRecord(record);
                }
            };
        }

        this.items = [{
            defaults: {
                anchor: '-5',
            },
            items:[{
                xtype: 'fieldset'
                ,title: 'Identitas'
                ,collapsible: true
                ,labelAlign: 'right'
                ,defaults: {
                    labelWidth: 185
                    ,anchor: '100%'
                    ,margins: '0 0 0 5'
                    ,renderer: function(v){
                        return '<b>' + v + '</b>';
                    }
                }
                ,items: [{
                    xtype: 'displayfield'
                    ,allowBlank: false
                    ,minValue: 0
                    ,fieldLabel: 'Nama'
                    ,maxLength: 80
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,margins: '0 0 0 0'
                    ,name: 'nama'
                },{
                    xtype: 'displayfield'
                    ,allowBlank: false
                    ,minValue: 0
                    ,fieldLabel: 'NIK'
                    ,maxLength: 80
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,margins: '0 0 0 0'
                    ,name: 'nik'
                },{
                    xtype: 'displayfield'
                    ,allowBlank: false
                    ,minValue: 0
                    ,fieldLabel: 'Jenis Kelamin'
                    ,maxLength: 80
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,margins: '0 0 0 0'
                    ,name: 'jenis_kelamin'
                    ,renderer:function(v){
                        switch(v){
                            case 'L': return '<b>Laki-laki</b>'; break;
                            case 'P': return '<b>Perempuan</b>'; break;
                        }
                    }
                },{
                    xtype: 'displayfield'
                    ,allowBlank: false
                    ,minValue: 0
                    ,fieldLabel: 'Tempat Lahir'
                    ,maxLength: 80
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,margins: '0 0 0 0'
                    ,name: 'tempat_lahir'
                },{
                    xtype: 'displayfield'
                    ,allowBlank: false
                    ,minValue: 0
                    ,fieldLabel: 'Tanggal Lahir'
                    ,maxLength: 80
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,margins: '0 0 0 0'
                    ,name: 'tanggal_lahir'
                    ,renderer : Ext.util.Format.dateRenderer('d/m/Y')
                },{
                    xtype: 'displayfield'
                    ,allowBlank: false
                    ,minValue: 0
                    ,fieldLabel: 'Nama Ibu Kandung'
                    ,maxLength: 80
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,margins: '0 0 0 0'
                    ,name: 'nama_ibu_kandung'
                }]
                
            },{
                xtype: 'fieldset'
                ,title: 'Data Pribadi'
                ,collapsible: true
                ,labelAlign: 'right'
                ,defaults: {
                    labelWidth: 185
                    ,anchor: '100%'
                    ,margins: '0 0 0 5'
                    ,renderer: function(v){
                        return '<b>' + v + '</b>';
                    }
                }
                ,items: [{
                    xtype: 'displayfield'
                    ,allowBlank: false
                    ,minValue: 0
                    ,fieldLabel: 'Alamat Jalan'
                    ,maxLength: 80
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,margins: '0 0 0 0'
                    ,name: 'alamat_jalan'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'RT'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'rt'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'RW'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'rw'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Nama Dusun'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'nama_dusun'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Desa/Kelurahan'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'desa_kelurahan'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Kecamatan'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'kecamatan'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Kabupaten/Kota'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'kabupaten'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Propinsi'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'propinsi'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Kode Pos'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'kode_pos'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Agama'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'agama_id_str'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Status Perkawinan'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'status_perkawinan'
                    ,renderer:function(v){
                        if(v == 1){
                            return '<b>Kawin</b>';
                        }else if(v == 2){
                            return '<b>Belum Kawin</b>';
                        }else{
                            return '<b>Janda / Duda</b>';
                        }
                    }
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Nama Suami / Istri'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'nama_suami_istri'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'NIP Suami / Istri'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'nip_suami_istri'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Pekerjaan Suami / Istri'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'pekerjaan_suami_istri_str'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'NPWP'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'npwp'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Kewarganegaraan'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'kewarganegaraan'
                    ,renderer:function(v){
                        if(v == 'ID'){
                            return '<b>Indonesia</b>';
                        }else if(v == 'A'){
                            return '<b>Asing</b>';
                        }
                    }
                }]
                
            }]
        },{
            defaults: {
                anchor: '-5'
            },
            items:[{
                xtype: 'fieldset'
                ,title: 'Kepegawaian'
                ,collapsible: true
                ,labelAlign: 'right'
                ,defaults: {
                    labelWidth: 185
                    ,anchor: '100%'
                    ,margins: '0 0 0 5'
                    ,renderer: function(v){
                        return '<b>' + v + '</b>';
                    }
                }
                ,items: [{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Status Kepegawaian'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'status_kepegawaian_id_str'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'NIP'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'nip'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'NIY/NIGK'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'niy_nigk'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'NUPTK'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'nuptk'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Jenis PTK'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'jenis_ptk_id_str'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'SK Pengangkatan'
                    ,maxLength: 40
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'sk_pengangkatan'
                },{
                    xtype: 'displayfield'
                    ,fieldLabel: 'TMT Pengangkatan'
                    ,format: 'd/m/Y'
                    ,maxValue: new Date()
                    ,labelAlign: 'right'
                    ,name: 'tmt_pengangkatan'
                    ,renderer : Ext.util.Format.dateRenderer('d/m/Y')
                },{
                    xtype: 'displayfield'
                    ,fieldLabel: 'Lembaga Pengangkat'
                    ,labelAlign: 'right'
                    ,name: 'lembaga_pengangkat_id_str'
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'SK CPNS'
                    ,maxLength: 40
                    ,enforceMaxLength: true
                    ,labelAlign: 'right'
                    ,name: 'sk_cpns'
                },{
                    xtype: 'displayfield'
                    ,fieldLabel: 'TMT CPNS'
                    ,format: 'd/m/Y'
                    ,renderer : Ext.util.Format.dateRenderer('d/m/Y')
                    ,labelAlign: 'right'
                    ,name: 'tgl_cpns'
                },{
                    xtype: 'displayfield'
                    ,fieldLabel: 'TMT PNS'
                    ,renderer : Ext.util.Format.dateRenderer('d/m/Y')
                    ,labelAlign: 'right'
                    ,name: 'tmt_pns'
                },{
                    xtype: 'displayfield'
                    ,fieldLabel: 'Pangkat Golongan'
                    ,labelAlign: 'right'
                    ,name: 'pangkat_golongan_id_str'
                },{
                    xtype: 'displayfield'
                    ,fieldLabel: 'Sumber Gaji'
                    ,labelAlign: 'right'
                    ,name: 'sumber_gaji_id_str'
                }]
                
            },{
                xtype: 'fieldset'
                ,title: 'Kompetensi Khusus'
                ,collapsible: true
                ,labelAlign: 'right'
                ,defaults: {
                    labelWidth: 185
                    ,anchor: '100%'
                    ,margins: '0 0 0 5'
                    ,renderer: function(v){
                        return '<b>' + v + '</b>';
                    }
                }
                ,items: [{
                    xtype: 'displayfield'
                    ,fieldLabel: 'Punya Lisensi Kepala Sekolah'
                    ,labelAlign: 'right'
                    ,name: 'sudah_lisensi_kepala_sekolah'
                    ,renderer:function(v){
                        if(v == 1){
                            return '<b>Ya</b>';
                        }else if(v == 0){
                            return '<b>Tidak</b>';
                        }
                    }
                },{
                    xtype: 'displayfield'
                    ,fieldLabel: 'Keahlian Laboratorium'
                    ,labelAlign: 'right'
                    ,name: 'keahlian_laboratorium_id_str'
                },{
                    xtype: 'displayfield'
                    ,fieldLabel: 'Keahlian Braille'
                    ,labelAlign: 'right'
                    ,name: 'keahlian_braille'
                    ,renderer:function(v){
                        if(v == 1){
                            return '<b>Ya</b>';
                        }else if(v == 0){
                            return '<b>Tidak</b>';
                        }
                    }
                },{
                    xtype: 'displayfield'
                    ,fieldLabel: 'Keahlian Bahasa Isyarat'
                    ,labelAlign: 'right'
                    ,name: 'keahlian_bhs_isyarat'
                    ,renderer:function(v){
                        if(v == 1){
                            return '<b>Ya<b>';
                        }else if(v == 0){
                            return '<b>Tidak</b>';
                        }
                    }
                }]
                
            },{
                xtype: 'fieldset'
                ,title: 'Kontak'
                ,collapsible: true
                ,labelAlign: 'right'
                ,defaults: {
                    labelWidth: 185
                    ,anchor: '100%'
                    ,margins: '0 0 0 5'
                    ,renderer: function(v){
                        return '<b>' + v + '</b>';
                    }
                }
                ,items: [{
                    xtype: 'displayfield'
                    ,fieldLabel: 'Nomor Telepon Rumah'
                    ,labelAlign: 'right'
                    ,name: 'no_telepon_rumah'
                },{
                    xtype: 'displayfield'
                    ,fieldLabel: 'Nomor HP'
                    ,labelAlign: 'right'
                    ,name: 'no_hp'
                },{
                    xtype: 'displayfield'
                    ,fieldLabel: 'Email'
                    ,labelAlign: 'right'
                    ,name: 'email'
                }] 
            }]
        }];

        this.callParent(arguments);
    }
});