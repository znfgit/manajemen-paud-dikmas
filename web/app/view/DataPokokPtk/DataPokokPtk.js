Ext.define('simdik_batam.view.DataPokokPtk.DataPokokPtk', {
	extend: 'Ext.panel.Panel',
    // requires:[
    //     'simdik_batam.view.DataPokokSekolah.DataPokokSekolahController'
    // ],
	alias: 'widget.datapokokptkpanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    // controller: 'main',
    initComponent:function(){

        this.title = 'Data Pokok PTK';

    	this.items = [{
            xtype:'filterptkform',
            region:'west',
            title:'Filter',
            collapsible:true,
            width:300,
            split:true,
            border:true
        },{
    		xtype: 'datapokokptkgrid',
            region:'center',
            title: 'Data Pokok PTK'
    	}];

    	this.callParent(arguments);
    }
});