Ext.define('simdik_batam.view.DataPokokPtk.PembelajaranGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.pembelajarangrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    listeners : {
        afterrender: 'setupPembelajaranGrid'
    },
    requires: [
        'Ext.grid.feature.Grouping'
    ],
    features: [{
        ftype: 'grouping',
        groupHeaderTpl: 'Semester: {name} ({rows.length} daftar)',
        hideGroupedHeader: false,
        startCollapsed: false,
        id: 'semesterGrouping'
    }],
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('simdik_batam.store.Pembelajaran');
        
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        this.getSelectionModel().setSelectionMode('SINGLE');
        
        this.columns = [{
            header: 'ID',
            width: 60,
            sortable: true,
            dataIndex: 'pembelajaran_id',
			hideable: false,
            hidden: true,
            field: {
                xtype: 'hidden'
            }
        },{
            header: 'Sekolah',
            width: 300,
            sortable: true,
            dataIndex: 'sekolah_id_str',
            hideable: false,
            hidden: false
        },{
            header: 'Rombongan Belajar',
            width: 180,
            sortable: true,
            dataIndex: 'rombongan_belajar_id',
			hideable: false,
            hidden: false,
            renderer: function(v,p,r) {
                return r.data.rombongan_belajar_id_str;
            }
        },{
            header: 'Tahun Ajaran',
            width: 140,
            sortable: true,
            dataIndex: 'tahun_ajaran_id_str',
            hideable: false,
            hidden: false
        },{
            header: 'Semester',
            width: 140,
            sortable: true,
            dataIndex: 'semester_id',
			hideable: false,
            hidden: false,
            renderer: function(v,p,r) {
                return r.data.semester_id_str;
            }
        },{
            header: 'Mata Pelajaran',
            width: 170,
            sortable: true,
            dataIndex: 'mata_pelajaran_id',
			hideable: false,
            renderer: function(v,p,r) {
                return r.data.mata_pelajaran_id_str;
            }
        },{
            header: 'Nama Mata Pelajaran',
            width: 210,
            sortable: true,
            dataIndex: 'nama_mata_pelajaran',
			hideable: false
        },{
            header: 'PTK',
            width: 140,
            sortable: true,
            hidden:true,
            dataIndex: 'ptk_terdaftar_id',
			hideable: false,
            renderer: function(v,p,r) {
                return r.data.ptk_id_str;
            }
        },{
            header: 'SK Mengajar',
            width: 180,
            sortable: true,
            dataIndex: 'sk_mengajar',
			hideable: false
        },{
            header: 'Tgl SK',
            width: 100,
            sortable: true,
            dataIndex: 'tanggal_sk_mengajar',
			hideable: false,
            renderer : Ext.util.Format.dateRenderer('d/m/Y')
        },{
            header: 'Jam',
            width: 100,
            sortable: true,
            dataIndex: 'jam_mengajar_per_minggu',
			hideable: false
        },{
            header: 'Status Di Kurikulum',
            width: 360,
            sortable: true,
            dataIndex: 'status_di_kurikulum',
			hideable: false,
            hidden: true
        
        }];
        
        this.bbar = Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
        
        this.callParent(arguments);
    }
});