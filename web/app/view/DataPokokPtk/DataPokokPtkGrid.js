Ext.define('simdik_batam.view.DataPokokPtk.DataPokokPtkGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.datapokokptkgrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    listeners : {
        afterrender: 'setupDataPokokPtkGrid'
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('simdik_batam.store.DataPokokPtk');
        
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        this.getSelectionModel().setSelectionMode('SINGLE');
        
        this.columns = [{
            header: 'Nama',
            width: 210,
            sortable: true,
            dataIndex: 'nama',
			hideable: false
        },{
            header: 'NIK',
            width: 130,
            sortable: true,
            dataIndex: 'nik',
			hideable: false
        },{
			
            header: 'JK',
            width: 63,
            sortable: true,
            dataIndex: 'jenis_kelamin',
			hideable: false,
			renderer: function(v,p,r) {
				switch (v) {
					case 'L' : return 'L'; break;
					case 'P' : return 'P'; break;
					default : return '-'; break;
				}
            }
        },{
            header: 'Tmp.Lahir',
            width: 120,
            sortable: true,
            dataIndex: 'tempat_lahir',
			hideable: false
        },{
            header: 'Tgl Lahir',
            width: 100,
            sortable: true,
            dataIndex: 'tanggal_lahir',
			hideable: false,
            renderer : Ext.util.Format.dateRenderer('d/m/Y')
        },{
            header: 'Nama Ibu Kandung',
            width: 210,
            sortable: true,
            dataIndex: 'nama_ibu_kandung',
			hideable: false
        },{
            header: 'Alamat Jalan',
            width: 300,
            sortable: true,
            dataIndex: 'alamat_jalan',
			hideable: false
        }/*,{
			
            header: 'Kewarganegaraan',
            width: 63,
            sortable: true,
            dataIndex: 'kewarganegaraan',
			hideable: false,
			renderer: function(v,p,r) {
				switch (v) {
					case 'I' : return 'Indonesia'; break;
					case 'A' : return 'Asing'; break;
					default : return '-'; break;
				}
            }
        }*/,{
            header: 'Status Kepegawaian',
            width: 150,
            sortable: true,
            dataIndex: 'status_kepegawaian_id_str',
			hideable: false
        },{
            header: 'NIP',
            width: 114,
            sortable: true,
            dataIndex: 'nip',
			hideable: false
        }/*,{
            header: 'Niy Nigk',
            width: 150,
            sortable: true,
            dataIndex: 'niy_nigk',
			hideable: false
        },{
            header: 'NUPTK',
            width: 108,
            sortable: true,
            dataIndex: 'nuptk',
			hideable: false
        },{
            header: 'Jenis Ptk',
            width: 140,
            sortable: true,
            dataIndex: 'jenis_ptk_id_str',
			hideable: false
        },{
            header: 'Status Keaktifan',
            width: 150,
            sortable: true,
            dataIndex: 'status_keaktifan_id_str',
			hideable: false
        },{
            header: 'Sk Pengangkatan',
            width: 180,
            sortable: true,
            dataIndex: 'sk_pengangkatan',
			hideable: false
        },{
            header: 'Tmt Pengangkatan',
            width: 100,
            sortable: true,
            dataIndex: 'tmt_pengangkatan',
			hideable: false,
            renderer : Ext.util.Format.dateRenderer('d/m/Y')
        },{
            header: 'Lembaga Pengangkat',
            width: 200,
            sortable: true,
            dataIndex: 'lembaga_pengangkat_id_str',
			hideable: false
        },{
            header: 'Sk Cpns',
            width: 180,
            sortable: true,
            dataIndex: 'sk_cpns',
			hideable: false
        },{
            header: 'Tgl Cpns',
            width: 100,
            sortable: true,
            dataIndex: 'tgl_cpns',
			hideable: false,
            renderer : Ext.util.Format.dateRenderer('d/m/Y')
        },{
            header: 'Tmt Pns',
            width: 100,
            sortable: true,
            dataIndex: 'tmt_pns',
			hideable: false,
            renderer : Ext.util.Format.dateRenderer('d/m/Y')
        },{
            header: 'Pangkat Golongan',
            width: 140,
            sortable: true,
            dataIndex: 'pangkat_golongan_id_str',
			hideable: false
        },{
            header: 'Sumber Gaji',
            width: 160,
            sortable: true,
            dataIndex: 'sumber_gaji_id_str',
			hideable: false
        },{
			
            header: 'Sudah Lisensi Kepala Sekolah',
            width: 360,
            sortable: true,
            dataIndex: 'sudah_lisensi_kepala_sekolah',
			hideable: false,
			renderer: function(v,p,r) {
				switch (v) {
					case 1 : return 'Ya'; break;					
					case 0 : return 'Tidak'; break;					
					default : return '-'; break;
				}
            }
        },{
            header: 'Keahlian Laboratorium',
            width: 170,
            sortable: true,
            dataIndex: 'keahlian_laboratorium_id',
			hideable: false,
            renderer: function(v,p,r) {
                return r.data.keahlian_laboratorium_id_str;
            }
        },{
            header: 'Mampu Handle Kk',
            width: 140,
            sortable: true,
            dataIndex: 'mampu_handle_kk',
			hideable: false,
            renderer: function(v,p,r) {
                return r.data.kebutuhan_khusus_id_str;
            }
        },{
			
            header: 'Keahlian Braille',
            width: 360,
            sortable: true,
            dataIndex: 'keahlian_braille',
			hideable: false,
			renderer: function(v,p,r) {
				switch (v) {
					case 1 : return 'Ya'; break;					
					case 0 : return 'Tidak'; break;					
					default : return '-'; break;
				}
            }
        },{
			
            header: 'Keahlian Bhs Isyarat',
            width: 360,
            sortable: true,
            dataIndex: 'keahlian_bhs_isyarat',
			hideable: false,
			renderer: function(v,p,r) {
				switch (v) {
					case 1 : return 'Ya'; break;					
					case 0 : return 'Tidak'; break;					
					default : return '-'; break;
				}
            }
        },{
            header: 'No Telepon Rumah',
            width: 120,
            sortable: true,
            dataIndex: 'no_telepon_rumah',
			hideable: false
        },{
            header: 'No Hp',
            width: 120,
            sortable: true,
            dataIndex: 'no_hp',
			hideable: false
        },{
            header: 'Email',
            width: 210,
            sortable: true,
            dataIndex: 'email',
			hideable: false
        },{
            header: 'ID',
            width: 60,
            sortable: true,
            dataIndex: 'ptk_id',
			hideable: false,
            hidden: true
        },{
            header: 'Pengawas Bidang Studi',
            width: 140,
            sortable: true,
            dataIndex: 'pengawas_bidang_studi_id',
			hideable: false,
            renderer: function(v,p,r) {
                return r.data.bidang_studi_id_str;
            }
        },{
            header: 'Entry Sekolah',
            width: 200,
            sortable: true,
            dataIndex: 'entry_sekolah_id',
			hideable: false,
            renderer: function(v,p,r) {
                return r.data.sekolah_id_str;
            }
        },{
            header: 'Jumlah Sekolah Binaan',
            width: 360,
            sortable: true,
            dataIndex: 'jumlah_sekolah_binaan',
			hideable: false
        },{
            header: 'Pernah Diklat Kepengawasan',
            width: 360,
            sortable: true,
            dataIndex: 'pernah_diklat_kepengawasan',
			hideable: false
        }*/];
        
        this.dockedItems = [{
            xtype: 'toolbar',
            items: [{
                xtype: 'button',
                text: 'Detail',                
                //cls: 'addbutton',
                glyph: 61485,
                handler: 'DetailPtk'
            }]
        }];
        
        this.bbar = Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
        
        this.callParent(arguments);
    }
});