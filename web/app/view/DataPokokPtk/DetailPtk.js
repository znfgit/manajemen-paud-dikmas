Ext.define('simdik_batam.view.DataPokokPtk.DetailPtk', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.detailptkpanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    // controller: 'main',
    initComponent:function(){

        var record = this.initialConfig.record ? this.initialConfig.record : false;
        if (record){
            this.listeners = {
                afterrender: function(form, options) {
                    // form.loadRecord(record);
                }
            };
        }

    	this.items = [{
            xtype:'tabpanel',
            region:'center',
            title: 'Detail PTK: ' + record.data.nama,
            items:[{
                title: 'Profil PTK',
                layout:'fit',
                items:[{
                    xtype: 'ptkform',
                    record:record
                }]
            },{
                title:'Riwayat Penugasan',
                xtype: 'ptkterdaftargrid'
            },{
                title: 'Riwayat Mengajar',
                xtype: 'pembelajarangrid'
            },{
                title: 'Riwayat Sertifikasi',
                xtype: 'riwayatsertifikasigrid'
            }]
        }];

    	this.callParent(arguments);
    }
});