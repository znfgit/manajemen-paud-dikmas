Ext.define('simdik_batam.view.DataPokokPtk.RiwayatSertifikasiGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.riwayatsertifikasigrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    /*listeners : {
        afterrender: 'setupRiwayatSertifikasiGrid'
    },*/
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('simdik_batam.store.RwySertifikasi');
        
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        this.getSelectionModel().setSelectionMode('SINGLE');
        
        this.columns = [{
            header: 'ID',
            width: 60,
            sortable: true,
            dataIndex: 'riwayat_sertifikasi_id',
			hideable: false,
            hidden: true,
            field: {
                xtype: 'hidden'
            }
        },{
            header: 'Bidang Studi',
            width: 300,
            sortable: true,
            dataIndex: 'bidang_studi_id_str',
            hideable: false,
            hidden: false
        },{
            header: 'Jenis Sertifikasi',
            width: 180,
            sortable: true,
            dataIndex: 'id_jenis_sertifikasi_str',
			hideable: false,
            hidden: false
        },{
            header: 'Tahun Sertifikasi',
            width: 140,
            sortable: true,
            dataIndex: 'tahun_sertifikasi',
            hideable: false,
            hidden: false
        },{
            header: 'Nomor Sertifikat',
            width: 140,
            sortable: true,
            dataIndex: 'nomor_sertifikat',
			hideable: false,
            hidden: false
        },{
            header: 'NRG',
            width: 170,
            sortable: true,
            dataIndex: 'nrg',
			hideable: false
        },{
            header: 'Nomor Peserta',
            width: 210,
            sortable: true,
            dataIndex: 'nomor_peserta',
			hideable: false
        }];
        
        this.bbar = Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
        
        this.callParent(arguments);
    }
});