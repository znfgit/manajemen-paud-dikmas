Ext.define('simdik_batam.view.DataPokokPtk.PtkTerdaftarGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.ptkterdaftargrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    listeners : {
        afterrender: 'setupPtkTerdaftarGrid'
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('simdik_batam.store.PtkTerdaftar');
        
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        this.getSelectionModel().setSelectionMode('SINGLE');
        
        this.columns = [{
            header: 'ID',
            width: 60,
            sortable: true,
            dataIndex: 'ptk_terdaftar_id',
			hideable: false,
            hidden: true,
            field: {
                xtype: 'hidden'
            }
        },{
            header: 'Ptk',
            width: 170,
            sortable: true,
            dataIndex: 'ptk_id',
            hidden:true,
			hideable: false,
            renderer: function(v,p,r) {
                return r.data.ptk_id_str;
            }
        },{
            header: 'Sekolah',
            width: 300,
            sortable: true,
            dataIndex: 'sekolah_id',
			hideable: false,
            renderer: function(v,p,r) {
                return r.data.sekolah_id_str;
            }
        },{
            header: 'Tahun Ajaran',
            width: 130,
            sortable: true,
            dataIndex: 'tahun_ajaran_id_str',
			hideable: false
        },{
            header: 'Nomor Surat Tugas',
            width: 180,
            sortable: true,
            dataIndex: 'nomor_surat_tugas',
			hideable: false
        },{
            header: 'Tanggal Surat Tugas',
            width: 180,
            sortable: true,
            dataIndex: 'tanggal_surat_tugas',
			hideable: false,
            renderer : Ext.util.Format.dateRenderer('d/m/Y')
        },{
            header: 'Tmt Tugas',
            width: 150,
            sortable: true,
            dataIndex: 'tmt_tugas',
			hideable: false,
            renderer : Ext.util.Format.dateRenderer('d/m/Y')
        },{
			
            header: 'Ptk Induk',
            width: 100,
            sortable: true,
            dataIndex: 'ptk_induk',
			hideable: false,
			renderer: function(v,p,r) {
				switch (v) {
					case 1 : return 'Ya'; break;					
					case 0 : return 'Tidak'; break;					
					default : return '-'; break;
				}
            }
        },{
            header: 'Jenis Keluar',
            width: 160,
            sortable: true,
            hidden:true,
            dataIndex: 'jenis_keluar_id_str',
			hideable: false
        },{
            header: 'Tgl Ptk Keluar',
            width: 150,
            hidden: true,
            sortable: true,
            dataIndex: 'tgl_ptk_keluar',
			hideable: false,
            renderer : Ext.util.Format.dateRenderer('d/m/Y')
        
        }];
        
        this.bbar = Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
        
        this.callParent(arguments);
    }
});