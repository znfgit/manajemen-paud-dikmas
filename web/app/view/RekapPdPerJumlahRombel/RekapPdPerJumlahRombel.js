Ext.define('simdik_batam.view.RekapPdPerJumlahRombel.RekapPdPerJumlahRombel', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.pdperjumlahrombelpanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

    	this.items = [{
           xtype:'panel',
            region:'center',
            split:true,
            layout:'border',
            items:[{
                xtype:'pdjumlahrombelgrid',
                region:'center',
                title: 'Jumlah Rombel Berdasarkan Jumlah Siswa Per Rombel'
            }]
        }];

    	this.callParent(arguments);
    }
});