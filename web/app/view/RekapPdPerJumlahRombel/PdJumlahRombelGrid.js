Ext.define('simdik_batam.view.RekapPdPerJumlahRombel.PdJumlahRombelGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.pdjumlahrombelgrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    requires:[
        'simdik_batam.view.RekapPdPerJumlahRombel.RekapPdPerJumlahRombelController'
    ],
    controller: 'rekappdperjumlahrombel',
    listeners:{
        afterrender:'setupPdJumlahRombelGrid'
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('simdik_batam.store.ChartBarPesertaDidikPerJumlahRombel');
        
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        this.getSelectionModel().setSelectionMode('SINGLE');
        
        this.columns = [{
            header: 'Satuan Pendidikan',
            width:200,
            sortable: true,
            locked:true,
            dataIndex: 'nama',
			hideable: false,
            hidden: false
        },{
            header: 'Total',
            width: 150,
            sortable: true,
            dataIndex: 'total',
			hideable: false,
            hidden: false,
            align:'right'
        },{
            text: 'Jumlah Anggota Rombel',
            itemId:'jumlahanggotarombel',
            columns: [{
                header: '<10',
                width: 100,
                sortable: true,
                dataIndex: 'kurang_10',
                hideable: false,
                align:'right'
            },{
                header: '10-20',
                width: 100,
                sortable: true,
                dataIndex: 'sepuluh_duapuluh',
                hideable: false,
                align:'right'
            },{
                header: '21-32',
                width: 100,
                sortable: true,
                dataIndex: 'duapuluhsatu_tigapuluhdua',
                hideable: false,
                align:'right'
            },{
                header: '>32',
                width: 100,
                sortable: true,
                dataIndex: 'lebih_32',
                hideable: false,
                align:'right'
            }]
        }];

        this.dockedItems = [{
            xtype: 'toolbar',
            items: ['-',{
                xtype:'bentukpendidikancombo',
                name: 'pilihbentukcombo',
                emptyText:'Pilih Bentuk Pendidikan...',
                displayField: 'nama',
                width: 200,
                listeners: {
                    'change': function(field, newValue, oldValue) {
                        
                        grid.getStore().reload();
                    }
                }
            }]
        }];
        
        this.bbar = Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
        
        this.callParent(arguments);
    }
});