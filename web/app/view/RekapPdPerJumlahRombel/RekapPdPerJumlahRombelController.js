Ext.define('simdik_batam.view.RekapPdPerJumlahRombel.RekapPdPerJumlahRombelController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.MessageBox'
    ],

    alias: 'controller.rekappdperjumlahrombel',

    setupPdJumlahRombelGrid:function(grid){
    	grid.getStore().on('beforeload', function(store){
            var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword
            });
        });

    	grid.getStore().load();
    }
});