Ext.define('simdik_batam.view.Beranda.BeritaGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.beritagrid',
    uses: [
        'Ext.data.ArrayStore',
		'Ext.data.Store'
    ],
    height: 300,
    frame:true,
    loadFeed: function(url){
        var store = this.store;
        store.getProxy().extraParams.feed = url;
        store.load();
    },
    onProxyException: function(proxy, response, operation) {
        // Ext.Msg.alert("Error with data from server", operation.error);
        this.view.el.update('');
        
        // Update the detail view with a dummy empty record
        this.fireEvent('select', this, {data:{}});
    },
    onLoad: function(store, records, success) {
        if (this.getStore().getCount()) {
            this.getSelectionModel().select(0);
        }
    },
    initComponent: function(){

        Ext.define('FeedItem', {
            extend: 'Ext.data.Model',
            fields: ['title', 'author','link', 'url', {name:'pubDate', type:'date'}, 'guid','description']
        });

        Ext.apply(this, {
            cls: 'feed-grid',
            store: Ext.create('Ext.data.Store', {
                model: 'FeedItem',
                sortInfo: {
                    property: 'title',
                    direction: 'DESC'
                },
                proxy: {
                    type: 'ajax',
                    url: 'rss',
                    reader: {
                        type: 'xml',
                        record: 'item'
                    },
                    listeners: {
                        exception: this.onProxyException,
                        scope: this
                    }
                },
                listeners: {
                    load: this.onLoad,
                    scope: this
                }
            }),
            columns: [{
                text: 'Judul',
                dataIndex: 'title',
                flex: 1,
                renderer: function(v,p,r) {
                    return '<a style="text-decoration:none" target="_blank" href="'+ r.data.link +'"><b style="font-size:13px;color:#434343;">'+ v + '</b><br><div style="color:#434343;padding-bottom:5px;padding-top:5px">'+ r.data.description +'</div></a>';
                }
            }, {
                text: 'Penulis',
                dataIndex: 'author',
                hidden: true,
                width: 200
            }, {
                text: 'Tanggal',
                dataIndex: 'guid',
                width: 150,
                hidden:true
            }]
        });

        this.store.load();

    	this.callParent(arguments);
    }
});
