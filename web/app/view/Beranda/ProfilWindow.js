Ext.define('simdik_batam.view.Beranda.ProfilWindow', {
	extend: 'Ext.window.Window',
    alias: 'widget.profilwindow',
    requires:[
        'Ext.layout.container.Fit',
        'Ext.layout.container.VBox',
        'Ext.TabPanel'
    ],
    layout: {
        type: 'border'
    },
    title: 'Profil Pengguna',
    width: 500,
    height: 400,
    modal: true,
    initComponent: function() { 

        this.items = [{
            xtype: 'penggunaform',
            region: 'center'
        }];

        this.callParent(arguments);
    }
});