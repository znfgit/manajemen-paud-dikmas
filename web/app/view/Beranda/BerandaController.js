Ext.define('simdik_batam.view.Beranda.BerandaController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.MessageBox'
    ],

    alias: 'controller.beranda',

    GantiPassword:function(btn){
        Ext.MessageBox.show({
            msg: 'Membuka Data ...',
            progressText: 'Membuka Data ...',
            width: 200,
            wait: true
        });

        var win = Ext.create('simdik_batam.view.Beranda.GantiPasswordWindow');

        // win.show();
        Ext.Ajax.request({
            url: 'session',
            success: function(response){
                var text = response.responseText;
                
                var res = Ext.JSON.decode(response.responseText);

                // console.log(res.pengguna_id);

                // var store = Ext.create('simdik_batam.store.Pengguna');

                // store.load({
                //     params:{
                //         pengguna_id: res.pengguna_id
                //     }
                // });

                // store.on('load', function(store){
                
                Ext.apply(Ext.form.VTypes, {
                    password: function(val, field) {
                        console.log(val, field);
                        if (field.initialPassField) {
                            var pwd = Ext.getCmp(field.initialPassField);
                            return (val == pwd.getValue());
                        }
                        return true;
                    },
                    passwordText: 'Password tidak sama!'
                });

                // var record = store.findRecord('pengguna_id', res.pengguna_id);

                var form = win.down('gantipasswordform');

                // form.loadRecord(record);

                form.down('hidden[name=pengguna_id]').setValue(res.pengguna_id);
                Ext.MessageBox.hide();
                win.show();

                // });
            },
            failure : function(response, options){
                Ext.MessageBox.hide();
                Ext.Msg.alert('Warning','Ada kesalahan dalam pemrosesan data');
            },
        });

    },

    profilPenggunaWindow:function(btn){
        Ext.MessageBox.show({
            msg: 'Membuka Data ...',
            progressText: 'Membuka Data ...',
            width: 200,
            wait: true
        });

        var win = Ext.create('simdik_batam.view.Beranda.ProfilWindow');

        Ext.Ajax.request({
            url: 'session',
            success: function(response){
                var text = response.responseText;
                
                var res = Ext.JSON.decode(response.responseText);

                console.log(res.pengguna_id);

                var store = Ext.create('simdik_batam.store.Pengguna');

                store.load({
                    params:{
                        pengguna_id: res.pengguna_id
                    }
                });

                store.on('load', function(store){
                    var record = store.findRecord('pengguna_id', res.pengguna_id);

                    var form = win.down('penggunaform');

                    form.add({
                        xtype:'textfield',
                        disabled:true,
                        name: 'username',
                        fieldLabel:'Username',
                        labelAlign:'right',
                        value: res.username
                    });

                    form.loadRecord(record);
                    Ext.MessageBox.hide();
                    win.show();
                });
            },

        });
    },

    NonForceLogin:function(btn){
        Ext.Ajax.request({
            url: 'session',
            success: function(response){
                var text = response.responseText;
                
                var res = Ext.JSON.decode(response.responseText);
                
                Ext.create('simdik_batam.Login',{
                    record: res.level,
                    kabkota: res.kabkota,
                    labels: res.labels
                });
            }
        });
        
    },

    profilPengguna:function(panel){
    	Ext.Ajax.request({
            url: 'session',
            success: function(response){
                var text = response.responseText;
                
                var res = Ext.JSON.decode(response.responseText);

                if(res.session == true){

                	panel.update('<div style="border:0px solid #ccc;width:70%;float:left;padding:10px 10px 10px 10px">'				  
							      + '<font size="4pt"><b>' + res.nama + '</b></font><br>'
							      + 'username : <b>' + res.username + '</b><br>'
                                  + 'Peran : <b>' + res.peran_id_str + '</b><br>'
                                  + 'Wilayah : <b>' + res.kode_wilayah_str + '</b><br>'
                                  + 'Level Wilayah : <b>' + res.id_level_wilayah_str + '</b><br>'
							      + 'Semester Berjalan : <b>' + res.semester_id_str + '</b><br>'
							      + '</div>'
							      );

                }
            }
        });
    },
    logout:function(){
        Ext.Msg.confirm('Konfirmasi','Yakin akan Logout?', function(btn){
            if (btn == "yes") {
                window.location = 'dologout';
            }
        });
    },
    setupRekapTotalGrid:function(grid){
        Ext.Ajax.request({
            url: 'session',
            success: function(response){
                var text = response.responseText;
                
                var res = Ext.JSON.decode(response.responseText);

                grid.setTitle('Rekap Total Pendidikan di Wilayah ' + res.kode_wilayah_str);

                if(res.level == 'dikmen'){
                    
                    // grid.columns[1].setVisible(false);
                    // grid.columns[2].setVisible(false);        

                    // var kolomSma = Ext.create('Ext.grid.column.Column', {
                    //     header: 'SMA',
                    //     width: 80,
                    //     sortable: true,
                    //     dataIndex: 'sma',
                    //     hideable: false,
                    //     align:'right',
                    //     renderer:function(v,p,r){
                    //         return grid.formatMoney(Math.abs(v),0);
                    //     }
                    // });
                    // var kolomSmk = Ext.create('Ext.grid.column.Column', {
                    //     header: 'SMK',
                    //     width: 80,
                    //     sortable: true,
                    //     dataIndex: 'smk',
                    //     hideable: false,
                    //     align:'right',
                    //     renderer:function(v,p,r){
                    //         return grid.formatMoney(Math.abs(v),0);
                    //     }
                    // });
                    // var kolomSmlb = Ext.create('Ext.grid.column.Column', {
                    //     header: 'SMLB',
                    //     width: 80,
                    //     sortable: true,
                    //     dataIndex: 'smlb',
                    //     hideable: false,
                    //     align:'right',
                    //     renderer:function(v,p,r){
                    //         return grid.formatMoney(Math.abs(v),0);
                    //     }
                    // });
                    
                    // grid.headerCt.insert(1, kolomSma);
                    // grid.headerCt.insert(2, kolomSmk);
                    // grid.headerCt.insert(3, kolomSmlb);

                }else if(res.level == 'dikdasmen'){
                    var kolomSma = Ext.create('Ext.grid.column.Column', {
                        header: 'SMA',
                        width: 80,
                        sortable: true,
                        dataIndex: 'sma',
                        hideable: false,
                        align:'right',
                        renderer:function(v,p,r){
                            return grid.formatMoney(Math.abs(v),0);
                        }   
                    });
                    var kolomSmk = Ext.create('Ext.grid.column.Column', {
                        header: 'SMK',
                        width: 80,
                        sortable: true,
                        dataIndex: 'smk',
                        hideable: false,
                        align:'right',
                        renderer:function(v,p,r){
                            return grid.formatMoney(Math.abs(v),0);
                        }   
                    });
                    var kolomSmlb = Ext.create('Ext.grid.column.Column', {
                        header: 'SMLB',
                        width: 80,
                        sortable: true,
                        dataIndex: 'smlb',
                        hideable: false,
                        align:'right',
                        renderer:function(v,p,r){
                            return grid.formatMoney(Math.abs(v),0);
                        }   
                    });
                    grid.headerCt.insert(4, kolomSma);
                    grid.headerCt.insert(5, kolomSmk);
                    grid.headerCt.insert(6, kolomSmlb);
                }

                if(res.include_depag){
                    var kolomMi = Ext.create('Ext.grid.column.Column', {
                        header: 'MI',
                        width: 80,
                        sortable: true,
                        dataIndex: 'mi',
                        hideable: false,
                        align:'right'   
                    });
                    var kolomMts = Ext.create('Ext.grid.column.Column', {
                        header: 'MTs',
                        width: 80,
                        sortable: true,
                        dataIndex: 'mts',
                        hideable: false,
                        align:'right'   
                    });
                    var kolomMa = Ext.create('Ext.grid.column.Column', {
                        header: 'MA',
                        width: 80,
                        sortable: true,
                        dataIndex: 'ma',
                        hideable: false,
                        align:'right'   
                    });
                    var kolomMak = Ext.create('Ext.grid.column.Column', {
                        header: 'MAK',
                        width: 80,
                        sortable: true,
                        dataIndex: 'mak',
                        hideable: false,
                        align:'right'   
                    });
                    grid.headerCt.insert(7, kolomMi);
                    grid.headerCt.insert(8, kolomMts);
                    grid.headerCt.insert(9, kolomMa);
                    grid.headerCt.insert(10, kolomMak);
                }

                grid.down('toolbar').add({
                    xtype:'semestercombo',
                    emptyText:'Pilih Semester...',
                    listeners: {
                        change: function(combo, newValue, oldValue, eOpts ) {
                           grid.getStore().reload();
                        }
                    }
                });

                grid.getStore().on('beforeload', function(store){
                    var semester_id = grid.down('toolbar').down('semestercombo').getValue();
                    
                    Ext.apply(store.proxy.extraParams, {
                        id_level_wilayah: res.id_level_wilayah,
                        kode_wilayah: res.kode_wilayah,
                        semester_id: semester_id
                    });
                });

                grid.getStore().load();
            }
        });

        

    }
});