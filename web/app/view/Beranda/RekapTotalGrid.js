Ext.define('simdik_batam.view.Beranda.RekapTotalGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.rekaptotalgrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    listeners:{
        afterrender:'setupRekapTotalGrid'
    },
    formatMoney: function(a, c, d, t){
        var n = a, 
            c = isNaN(c = Math.abs(c)) ? 0 : c, 
            d = d == undefined ? "," : d, 
            t = t == undefined ? "." : t, 
            s = n < 0 ? "-" : "", 
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('simdik_batam.store.RekapTotal');
        
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        this.getSelectionModel().setSelectionMode('SINGLE');
        
        this.columns = [{
            header: ' ',
            width:100,
            sortable: true,
            dataIndex: 'desk',
			hideable: false,
            hidden: false
        },
        {
            header: 'SMA',
            width: 80,
            sortable: true,
            dataIndex: 'sma',
            hideable: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },
        {
            header: 'SMK',
            width: 80,
            sortable: true,
            dataIndex: 'smk',
            hideable: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },
        {
            header: 'SMLB',
            width: 80,
            sortable: true,
            dataIndex: 'smlb',
            hideable: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },
   //      {
   //          header: 'SD',
   //          width: 80,
   //          sortable: true,
   //          dataIndex: 'sd',
			// hideable: false,
   //          hidden: false,
   //          align:'right',
   //          renderer:function(v,p,r){
   //              return formatMoney(Math.abs(v),0);
   //          }
   //      },{
   //          header: 'SMP',
   //          width: 80,
   //          sortable: true,
   //          dataIndex: 'smp',
   //          hideable: false,
   //          hidden: false,
   //          align:'right',
   //          renderer:function(v,p,r){
   //              return grid.formatMoney(Math.abs(v),0);
   //          }
   //      },
        {
            header: 'SLB',
            width: 70,
            sortable: true,
            dataIndex: 'slb',
            hideable: false,
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },{
            header: 'Total',
            width: 80,
            sortable: true,
            dataIndex: 'total',
            hideable: false,
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        }];

        this.dockedItems = [{
            xtype: 'toolbar',
            items: ['-']
        }];
        
        // this.bbar = Ext.create('Ext.PagingToolbar', {
        //     store: this.store,
        //     displayInfo: true,
        //     displayMsg: 'Displaying data {0} - {1} of {2}',
        //     emptyMsg: "Tidak ada data"
        // });
        
        this.callParent(arguments);
    }
});