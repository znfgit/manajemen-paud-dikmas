Ext.define('simdik_batam.view.Beranda.Beranda2', {
	extend: 'Ext.panel.Panel',
    requires:[
        'simdik_batam.view.Beranda.BerandaController'
    ],
	alias: 'widget.beranda2panel',
    layout: 'border',
    controller: 'beranda',
    border: true,
    bodyPadding:20,
    bodyBorder: false,
    layout: {
        type:'hbox',
        padding:'5',
        align:'top'
    },
    autoScroll: true,
    defaults:{margin:'0 0 5 0'},
    initComponent:function(){
        Ext.Ajax.request({
            url: '/session',
            success: function(response){
                var text = response.responseText;
                
                var res = Ext.JSON.decode(response.responseText);

                var level = res.level;
            }
        });

        var welcomeStr = '';

    	this.items = [{
                xtype:'panel',
                flex:1,
                border:false,
                // margin:'10 5 0 0',
                itemId:'panelkiri',
                split:false,
                frame:false,
                collapsible: false,
                // autoScroll: true,
                layout: {
                    type:'vbox',
                    padding:'5',
                    align:'stretch'
                },
                items:[{
                    xtype: 'panel',
                    flex: 1,
                    height: 510,
                    // title:' ',
                    id:'panelatas',
                    split: true,
                    border:true,
                    // collapsible: true,
                    frame:false,
                    layout:'border',
                    items:[{
                        xtype:'panel',
                        region:'north',
                        split:false,
                        height:120,
                        id:'welcomeStr',
                        html: welcomeStr,
                        bodyStyle:'background:url(resources/img/welcome-scene.png);background-position:bottom'
                    },{
                        xtype:'panel',
                        region:'center',
                        border:false,
                        split:false,
                        html: 'Aplikasi Profil... adalah ...',
                        bodyStyle:'padding:10px' 

                    }]
                }]
            },{
                xtype:'panel',
                flex: 1,
                border: false,
                frame: false,
                split:false,
                itemId:'panelkanan',
                // margin:'0 20 5 0',
                collapsible:false,
                // autoScroll: true,
                layout: {
                    type:'vbox',
                    padding:'5',
                    align:'stretch'
                },
                items:[{
                    xtype: 'panel',
                    height:200,
                    title:'Pengguna',
                    id:'panelatas2',
                    split: true,
                    border:true,
                    // collapsible: true,
                    frame:true,
                    layout:'fit',
                    listeners : {
                        afterrender: 'profilPengguna'
                    },
                    dockedItems:[{
                        xtype: 'toolbar',
                        dock: 'bottom',
                        ui: 'footer',
                        items: ['->',{
                            text: 'Profil Pengguna',
                            itemId:'profilpengguna',
                            // glyph: 61447,
                            action: 'comeback'
                        },{
                            text: 'Logout',
                            itemId:'logout',
                            // glyph: 61579,
                            handler: 'logout'
                        }]
                    }]
                },{
                    xtype: 'rekaptotalgrid',
                    height:300,
                    id:'panelbawah2',
                    title:'Rekap Total',
                    split: true,
                    border:true,
                    // collapsible: true,
                    frame:true
                }]
            }];

    	this.callParent(arguments);
    },
    getInfoStr: function() {

        return 'keterangan pengguna'
                  ;
    },
    getChangelog: function(){
        return 'changelog'
        ;
    }
});