Ext.define('simdik_batam.view.Beranda.Beranda', {
	extend: 'Ext.panel.Panel',
    requires:[
        'simdik_batam.view.Beranda.BerandaController'
    ],
	alias: 'widget.berandapanel',
    layout: 'border',
    controller: 'beranda',
    border: true,
    bodyPadding:20,
    bodyBorder: false,
    layout: {
        type:'vbox',
        padding:'2',
        align:'stretch'
    },
    autoScroll: true,
    defaults:{margin:'0 0 5 0'},
    initComponent:function(){
        Ext.Ajax.request({
            url: 'session',
            success: function(response){
                var text = response.responseText;
                
                var res = Ext.JSON.decode(response.responseText);

                var level = res.level;
            }
        });

        var welcomeStr = '';

    	this.items = [{
                xtype:'panel',
                height:250,
                border:false,
                // margin:'10 5 0 0',
                itemId:'panelkiri',
                split:false,
                frame:false,
                collapsible: false,
                // autoScroll: true,
                layout: {
                    type:'hbox',
                    padding:'5',
                    align:'stretch'
                },
                items:[{
                    xtype: 'panel',
                    flex: 1,
                    height: 510,
                    // title:' ',
                    id:'panelatas',
                    split: true,
                    border:true,
                    // collapsible: true,
                    frame:true,
                    layout:'border',
                    items:[{
                        xtype:'panel',
                        region:'north',
                        split:false,
                        height:120,
                        id:'welcomeStr',
                        html: welcomeStr,
                        bodyStyle:'background:url(resources/img/welcome-bg.png);background-position:bottom'
                    },{
                        xtype:'panel',
                        region:'center',
                        border:false,
                        id:'deskripsiStr',
                        split:false,
                        html: 'Manajemen Dapodikmen',
                        bodyStyle:'padding:10px' 

                    }]
                },{
                    xtype: 'panel',
                    flex:1,
                    title:'Pengguna',
                    id:'panelatas2',
                    split: true,
                    border:true,
                    // collapsible: true,
                    frame:true,
                    layout:'fit',
                    listeners : {
                        afterrender: 'profilPengguna'
                    },
                    dockedItems:[{
                        xtype: 'toolbar',
                        dock: 'bottom',
                        ui: 'footer',
                        items: ['->',{
                            text: 'Profil Pengguna',
                            itemId:'profilpengguna',
                            // glyph: 61447,
                            handler: 'profilPenggunaWindow'
                        },{
                            text:'Ubah Password',
                            itemId: 'GantiPassword',
                            // glyph: 61572,
                            handler:'GantiPassword'
                        },{
                            text: 'Logout',
                            itemId:'logout',
                            // glyph: 61579,
                            handler: 'logout'
                        }]
                    }]
                }]
            },{
                xtype:'panel',
                height: 280,
                border: false,
                frame: false,
                split:false,
                itemId:'panelkanan',
                // collapsible:false,
                layout: {
                    type:'hbox',
                    padding:'5',
                    align:'stretch'
                },
                items:[{
                    xtype: 'beritagrid',
                    flex:1,
                    id:'panelbawah2',
                    title:'Berita',
                    split: true,
                    border:true,
                    // collapsible: true,
                    frame:true
                },{
                    xtype: 'rekaptotalgrid',
                    flex:1,
                    id:'panelbawah3',
                    title:'Rekap Total',
                    split: true,
                    border:true,
                    // collapsible: true,
                    frame:true
                }]
            }];

    	this.callParent(arguments);
    },
    getInfoStr: function() {

        return 'keterangan pengguna'
                  ;
    },
    getChangelog: function(){
        return 'changelog'
        ;
    }
});