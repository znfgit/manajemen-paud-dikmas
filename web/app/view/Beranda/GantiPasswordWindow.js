Ext.define('simdik_batam.view.Beranda.GantiPasswordWindow', {
	extend: 'Ext.window.Window',
    alias: 'widget.gantipasswordwindow',
    requires:[
        'Ext.layout.container.Fit',
        'Ext.layout.container.VBox',
        'Ext.TabPanel'
    ],
    layout: {
        type: 'border'
    },
    title: 'Profil Pengguna',
    width: 500,
    height: 200,
    modal: true,
    initComponent: function() { 

        this.items = [{
            xtype: 'gantipasswordform',
            region: 'center'
        }];

        this.callParent(arguments);
    }
});