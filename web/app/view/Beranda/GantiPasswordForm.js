Ext.define('simdik_batam.view.Beranda.GantiPasswordForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.gantipasswordform',
    bodyPadding: 10,
    fieldDefaults: {
        anchor: '100%',
        labelWidth: 120
    },
    initComponent: function() {

    	var record = this.initialConfig.record ? this.initialConfig.record : false;
        if (record){
            this.listeners = {
                afterrender: function(form, options) {
                    form.loadRecord(record);
                }
            };
        }

        this.items = [{
            xtype: 'hidden'
            ,fieldLabel: 'ID'
            ,labelAlign: 'right'
            ,labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
            ,allowBlank: false
            ,maxValue: 99999999
            ,minValue: 0
            ,name: 'pengguna_id'
        },{
            xtype: 'textfield',
            allowBlank: false,
            fieldLabel: 'Password Lama',
            name: 'password_lama',
            labelAlign: 'right',
            id: 'password_lama',
            emptyText: 'Password Lama...',
            inputType: 'password',
            labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
        },{
            xtype: 'textfield',
            allowBlank: false,
            fieldLabel: 'Password Baru',
            name: 'password_baru',
            labelAlign: 'right',
            id: 'password',
            emptyText: 'Password Baru...',
            inputType: 'password',
            labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
        },{
            xtype: 'textfield',
            allowBlank: false,
            labelAlign: 'right',
            fieldLabel: 'Ulang Password Baru',
            name: 'password_baru_confirm',
            emptyText: 'Konfirmasi password Baru...',
            inputType: 'password',
            vtype: 'password',
            initialPassField: 'password',
            labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
        }];

        this.buttons = [{
            text: 'Save',
            glyph: 61639,
            handler: 'savePassword'
        }];

        this.callParent(arguments);
    }
});