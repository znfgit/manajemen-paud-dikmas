Ext.define('simdik_batam.view.AnalisisKebutuhanPrasaranaSMP.AnalisisKebutuhanPrasaranaSMPGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.analisiskebutuhanprasaranasmpgrid',
    selType: 'rowmodel',
    autoScroll: true,
    requires:[
        'simdik_batam.view.AnalisisKebutuhanPrasaranaSMP.AnalisisKebutuhanPrasaranaSMPController'
    ],
    controller: 'analisiskebutuhanprasaranasmp',
    listeners:{
        afterrender:'setupAnalisisKebutuhanPrasaranaSMPGrid'
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('simdik_batam.store.AnalisisKebutuhanPrasaranaSMP');
        
        this.getSelectionModel().setSelectionMode('SINGLE');

        this.columns = [{
            header: 'ID',
            width: 60,
            sortable: true,
            dataIndex: 'kode_wilayah',
            hideable: false,
            hidden: true,
            field: {
                xtype: 'hidden'
            }
        },{
            header: 'Wilayah',
            width: 200,
            sortable: true,
            dataIndex: 'nama',
            locked   : true,
            hideable: false
        },{
                text: 'Sekolah',
                columns: [{
                    header: 'Negeri',
                    width: 80,
                    sortable: true,
                    dataIndex: 'negeri',
                    align:'right',
                    hideable: false
                },{
                    header: 'Swasta',
                    width: 100,
                    sortable: true,
                    dataIndex: 'swasta',
                    align:'right',
                    hideable: false
                },{
                    header: 'Total',
                    width: 80,
                    sortable: true,
                    dataIndex: 'total',
                    align:'right',
                    hideable: false,
                    renderer: function(value, metaData, record, row, col, store, gridView){
                        return record.data.negeri + record.data.swasta;
                    }
                }]
        },{
            text: 'Jumlah',
            columns: [{
                text:'Ruang Kelas',
                columns:[{
                    header: 'Negeri',
                    width: 80,
                    sortable: true,
                    dataIndex: 'kelas_negeri',
                    align:'right',
                    hideable: false
                },{
                    header: 'Swasta',
                    width: 100,
                    sortable: true,
                    dataIndex: 'kelas_swasta',
                    align:'right',
                    hideable: false
                },{
                    header: 'Total',
                    width: 80,
                    sortable: true,
                    dataIndex: 'kelas_total',
                    align:'right',
                    hideable: false,
                    renderer: function(value, metaData, record, row, col, store, gridView){
                        return record.data.kelas_negeri + record.data.kelas_swasta;
                    }
                }]
            },{
                text:'Lab IPA',
                columns:[{
                    header: 'Negeri',
                    width: 80,
                    sortable: true,
                    dataIndex: 'lab_ipa_negeri',
                    align:'right',
                    hideable: false
                },{
                    header: 'Swasta',
                    width: 100,
                    sortable: true,
                    dataIndex: 'lab_ipa_swasta',
                    align:'right',
                    hideable: false
                },{
                    header: 'Total',
                    width: 80,
                    sortable: true,
                    dataIndex: 'lab_ipa_total',
                    align:'right',
                    hideable: false,
                    renderer: function(value, metaData, record, row, col, store, gridView){
                        return record.data.lab_ipa_negeri + record.data.lab_ipa_swasta;
                    }
                }]
            },{
                text:'Lab Komputer',
                columns:[{
                    header: 'Negeri',
                    width: 80,
                    sortable: true,
                    dataIndex: 'lab_komputer_negeri',
                    align:'right',
                    hideable: false
                },{
                    header: 'Swasta',
                    width: 100,
                    sortable: true,
                    dataIndex: 'lab_komputer_swasta',
                    align:'right',
                    hideable: false
                },{
                    header: 'Total',
                    width: 80,
                    sortable: true,
                    dataIndex: 'lab_komputer_total',
                    align:'right',
                    hideable: false,
                    renderer: function(value, metaData, record, row, col, store, gridView){
                        return record.data.lab_komputer_negeri + record.data.lab_komputer_swasta;
                    }
                }]
            },{
                text:'Perpustakaan',
                columns:[{
                    header: 'Negeri',
                    width: 80,
                    sortable: true,
                    dataIndex: 'perpustakaan_negeri',
                    align:'right',
                    hideable: false
                },{
                    header: 'Swasta',
                    width: 100,
                    sortable: true,
                    dataIndex: 'perpustakaan_swasta',
                    align:'right',
                    hideable: false
                },{
                    header: 'Total',
                    width: 80,
                    sortable: true,
                    dataIndex: 'perpustakaan_total',
                    align:'right',
                    hideable: false,
                    renderer: function(value, metaData, record, row, col, store, gridView){
                        return record.data.perpustakaan_negeri + record.data.perpustakaan_swasta;
                    }
                }]
            }]
        },{
            text: 'Sarana Penunjang / Khusus Hinterland',
            columns: [{
                header: 'RDK',
                width: 80,
                sortable: true,
                dataIndex: 'rdk',
                align:'right',
                hideable: false
            },{
                header: 'RDG',
                width: 80,
                sortable: true,
                dataIndex: 'rdg',
                align:'right',
                hideable: false
            },{
                header: 'RDJ',
                width: 80,
                sortable: true,
                dataIndex: 'rdj',
                align:'right',
                hideable: false
            },{
                header: 'WC',
                width: 80,
                sortable: true,
                dataIndex: 'wc',
                align:'right',
                hideable: false
            },{
                header: 'P',
                width: 80,
                sortable: true,
                dataIndex: 'pr_p',
                align:'right',
                hideable: false
            },{
                header: 'A',
                width: 100,
                sortable: true,
                dataIndex: 'pr_a',
                align:'right',
                hideable: false
            },{
                header: 'K',
                width: 80,
                sortable: true,
                dataIndex: 'pr_k',
                align:'right',
                hideable: false,
                renderer: function(value, metaData, record, row, col, store, gridView){
                    if(value < 0){
                        return 0;
                    }else{
                        return value;
                    }
                }
            },{
                header: 'L',
                width: 80,
                sortable: true,
                dataIndex: 'pr_l',
                align:'right',
                hideable: false,
                renderer: function(value, metaData, record, row, col, store, gridView){
                    if(value < 0){
                        return 0;
                    }else{
                        return value;
                    }
                }
            }]
        }];

        this.dockedItems = [{
            xtype:'toolbar',
            dock:'top',
            items:['-',{
                // text:'Muat Ulang',
                glyph: 61470,
                handler: function(btn){
                    grid.store.reload();
                }
            }]
        }];
        
        
        this.callParent(arguments);
    }
});