Ext.define('simdik_batam.view.AnalisisKebutuhanPrasaranaSMP.AnalisisKebutuhanPrasaranaSMPController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.MessageBox'
    ],

    alias: 'controller.analisiskebutuhanprasaranasmp',

    setupAnalisisKebutuhanPrasaranaSMPGrid:function(grid){

    	grid.getStore().load();
    }
});