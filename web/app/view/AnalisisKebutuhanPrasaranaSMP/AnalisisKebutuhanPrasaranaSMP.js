// AnalisisKebutuhanGuruDanKelas
Ext.define('simdik_batam.view.AnalisisKebutuhanPrasaranaSMP.AnalisisKebutuhanPrasaranaSMP', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.analisiskebutuhanprasaranasmppanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

    	this.items = [{
    		xtype: 'analisiskebutuhanprasaranasmpgrid',
            title: 'Analisis Kebutuhan Prasarana SMP',
            region:'center'
    	}];

    	this.callParent(arguments);
    }
});