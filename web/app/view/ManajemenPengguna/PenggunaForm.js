Ext.define('simdik_batam.view.ManajemenPengguna.PenggunaForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.penggunaform',
    bodyPadding: 10,
    fieldDefaults: {
        anchor: '100%',
        labelWidth: 120
    },
    initComponent: function() {
        
        var record = this.initialConfig.record ? this.initialConfig.record : false;
        if (record){
            this.listeners = {
                afterrender: function(form, options) {
                    form.loadRecord(record);
                }
            };
        }

        /*this.on('beforeadd', function(form, field){
            if (!field.allowBlank)
              field.labelSeparator += '<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>';
        });*/
        
        this.items = [{
            xtype: 'hidden'
            ,fieldLabel: 'ID'
            ,labelAlign: 'right'
            ,labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
			,allowBlank: false
			,maxValue: 99999999
			,minValue: 0
            ,name: 'pengguna_id'
        },{
            xtype: 'hidden'
            ,fieldLabel: 'Sekolah'
            ,labelAlign: 'right'
			,maxValue: 99999999
			,minValue: 0
            ,name: 'sekolah_id'
        },{
            xtype: 'hidden'
            ,fieldLabel: 'Lembaga'
            ,labelAlign: 'right'
			,maxValue: 99999999
			,minValue: 0
            ,name: 'lembaga_id'
        },{
            xtype: 'hidden'
            ,fieldLabel: 'Peran'
            ,labelAlign: 'right'
            ,labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
			,allowBlank: false
			,maxValue: 11
			,minValue: 0
            ,maxLength: 100
            ,enforceMaxLength: true
            ,name: 'peran_id'
        },{
            xtype: 'hidden'
            ,fieldLabel: 'Username'
            ,labelAlign: 'right'
            ,labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
			,allowBlank: false
			,maxValue: 99999999
			,minValue: 0
            ,maxLength: 50
            ,enforceMaxLength: true
            ,name: 'username'
        },{
            xtype: 'hidden'
            ,fieldLabel: 'Password'
            ,labelAlign: 'right'
            ,labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
			,allowBlank: false
			,maxValue: 99999999
			,minValue: 0
            ,maxLength: 50
            ,enforceMaxLength: true
            ,name: 'password'
        },{
            xtype: 'textfield'
            ,fieldLabel: 'Nama'
            ,labelAlign: 'right'
            ,labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
			,allowBlank: false
			,maxValue: 99999999
			,minValue: 0
            ,maxLength: 50
            ,enforceMaxLength: true
            ,name: 'nama'
        },{
            xtype: 'textfield'
            ,fieldLabel: 'Nip nim'
            ,labelAlign: 'right'
			,maxValue: 99999999
			,minValue: 0
            ,maxLength: 9
            ,enforceMaxLength: true
            ,name: 'nip_nim'
        },{
            xtype: 'textareafield'
            ,fieldLabel: 'Jabatan lembaga'
            ,labelAlign: 'right'
			// ,maxValue: 99999999
			,minValue: 0
            // ,maxLength: 25
            // ,enforceMaxLength: true
            ,name: 'jabatan_lembaga'
        },{
            xtype: 'textfield'
            ,fieldLabel: 'Ym'
            ,labelAlign: 'right'
			,maxValue: 99999999
			,minValue: 0
            ,maxLength: 20
            ,hidden: true
            ,enforceMaxLength: true
            ,name: 'ym'
        },{
            xtype: 'textfield'
            ,fieldLabel: 'Skype'
            ,labelAlign: 'right'
			,maxValue: 99999999
			,minValue: 0
            ,maxLength: 20
            ,hidden: true
            ,enforceMaxLength: true
            ,name: 'skype'
        },{
            xtype: 'textfield'
            ,fieldLabel: 'Alamat'
            ,labelAlign: 'right'
			,maxValue: 99999999
			,minValue: 0
            ,maxLength: 80
            ,enforceMaxLength: true
            ,name: 'alamat'
        },{
            xtype: 'hidden'
            ,fieldLabel: 'Kode wilayah'
            ,labelAlign: 'right'
            ,labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
			,allowBlank: false
			,maxValue: 99999999
			,minValue: 0
            ,maxLength: 7
            ,enforceMaxLength: true
            ,name: 'kode_wilayah'
        },{
            xtype: 'textfield'
            ,fieldLabel: 'No telepon'
            ,labelAlign: 'right'
			,maxValue: 99999999
			,minValue: 0
            ,maxLength: 20
            ,enforceMaxLength: true
            ,name: 'no_telepon'
        },{
            xtype: 'textfield'
            ,fieldLabel: 'No hp'
            ,labelAlign: 'right'
			,maxValue: 99999999
			,minValue: 0
            ,maxLength: 20
            ,enforceMaxLength: true
            ,name: 'no_hp'
        },{
            xtype: 'hidden'
            ,fieldLabel: 'Aktif'
            ,labelAlign: 'right'
            ,labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
			,allowBlank: false
			,maxValue: 99999999
			,minValue: 0
            ,maxLength: 100
            ,enforceMaxLength: true
            ,name: 'aktif'
        
        }];

        this.buttons = [{
            text: 'Save',
            glyph: 61639,
            handler: 'savePenggunaBaru'
        }];

        this.callParent(arguments);
    }
});