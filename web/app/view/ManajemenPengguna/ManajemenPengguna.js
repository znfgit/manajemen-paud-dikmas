Ext.define('simdik_batam.view.ManajemenPengguna.ManajemenPengguna', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.manajemenpenggunapanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

    	this.items = [{
    		xtype: 'penggunagrid',
            title: 'Manajemen Pengguna',
            region:'center'
    	}];

    	this.callParent(arguments);
    }
});