Ext.define('simdik_batam.view.ManajemenPengguna.PenggunaWindow', {
	extend: 'Ext.window.Window',
    alias: 'widget.penggunawindow',
    requires:[
        'Ext.layout.container.Fit',
        'Ext.layout.container.VBox',
        'Ext.TabPanel'
    ],
    layout: {
        type: 'border'
    },
    title: 'Tambah Pengguna',
    width: 600,
    height: 450,
    modal: true,
    initComponent: function() {                    

        this.items =[{
            xtype: 'penggunaform',
            region: 'center'
        }];

        this.tools = [{
            type: 'maximize',
            handler: function(event, target, owner, tool){
                win = tool.up('window');
                win.toggleMaximize();

                var tool = win.down('tool[type=minimize]');

                tool.show();
            }
        },{
            type: 'minimize',
            hidden:true,
            handler: function(event, target, owner, tool){
                win = tool.up('window');
                win.toggleMaximize();

                var tool = win.down('tool[type=minimize]');

                tool.hide();
            }
        }];
        
        this.callParent(arguments);
    }
});