Ext.define('simdik_batam.view.ManajemenPengguna.PenggunaGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.penggunagrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    listeners : {
        afterrender: 'setupPenggunaGrid'
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('simdik_batam.store.Pengguna');
        
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        this.getSelectionModel().setSelectionMode('SINGLE');
        
        this.columns = [{
            header: 'ID',
            width: 60,
            sortable: true,
            dataIndex: 'pengguna_id',
			hideable: false,
            hidden: true
        },{
            header: 'Sekolah',
            width: 200,
            sortable: true,
            dataIndex: 'sekolah_id',
			hideable: false,
            hidden: true,
            renderer: function(v,p,r) {
                return r.data.sekolah_id_str;
            }
        },{
            header: 'Lembaga',
            width: 200,
            sortable: true,
            dataIndex: 'lembaga_id',
			hideable: false,
            hidden: true,
            renderer: function(v,p,r) {
                return r.data.lembaga_id_str;
            }
        },{
            header: 'Peran',
            width: 170,
            sortable: true,
            dataIndex: 'peran_id',
			hideable: false,
            hidden: true
        },{
            header: 'Username',
            width: 210,
            sortable: true,
            dataIndex: 'username',
			hideable: false
        },{
            header: 'Password',
            width: 210,
            sortable: true,
            dataIndex: 'password',
			hideable: false,
            hidden: true
        },{
            header: 'Nama',
            width: 210,
            sortable: true,
            dataIndex: 'nama',
            hideable: false
        },{
            header: 'Wilayah',
            width: 210,
            sortable: true,
            dataIndex: 'kode_wilayah_str',
			hideable: false
        },{
            header: 'Nip Nim',
            width: 87,
            sortable: true,
            dataIndex: 'nip_nim',
			hideable: false,
            hidden: false
        },{
            header: 'Jabatan Lembaga',
            width: 150,
            sortable: true,
            dataIndex: 'jabatan_lembaga',
			hideable: false,
            hidden: false
        },{
            header: 'Ym',
            width: 120,
            sortable: true,
            dataIndex: 'ym',
			hideable: false,
            hidden: true
        },{
            header: 'Skype',
            width: 120,
            sortable: true,
            dataIndex: 'skype',
            hideable: false,
            hidden: true
        },{
            header: 'Peran',
            width: 120,
            sortable: true,
            dataIndex: 'peran_id_str',
			hideable: false,
            hidden: false
        },{
            header: 'Alamat',
            width: 250,
            sortable: true,
            dataIndex: 'alamat',
			hideable: false
        },{
            header: 'Kode Wilayah',
            width: 150,
            sortable: true,
            dataIndex: 'kode_wilayah',
			hideable: false,
            hidden: true,
            renderer: function(v,p,r) {
                return r.data.kode_wilayah_str;
            }
        },{
            header: 'No Telepon',
            width: 120,
            sortable: true,
            dataIndex: 'no_telepon',
			hideable: false
        },{
            header: 'No Hp',
            width: 120,
            sortable: true,
            dataIndex: 'no_hp',
			hideable: false
        },{
            header: 'Aktif',
            width: 360,
            sortable: true,
            dataIndex: 'aktif',
			hideable: false,
            hidden: true
        
        }];
        
        this.dockedItems = [{
            xtype: 'toolbar',
            items: ['-']
        }];
        
        this.bbar = Ext.create('Ext.PagingToolbar', {
            store: this.store,
            displayInfo: true,
            displayMsg: 'Displaying data {0} - {1} of {2}',
            emptyMsg: "Tidak ada data"
        });
        
        this.callParent(arguments);
    }
});