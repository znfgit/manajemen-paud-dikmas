// AnalisisKebutuhanGuruDanKelas
Ext.define('simdik_batam.view.AnalisisKebutuhanPrasaranaSD.AnalisisKebutuhanPrasaranaSD', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.analisiskebutuhanprasaranasdpanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

    	this.items = [{
    		xtype: 'analisiskebutuhanprasaranasdgrid',
            title: 'Analisis Kebutuhan Prasarana SD',
            region:'center'
    	}];

    	this.callParent(arguments);
    }
});