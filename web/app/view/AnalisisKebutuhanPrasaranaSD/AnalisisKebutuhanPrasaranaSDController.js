Ext.define('simdik_batam.view.AnalisisKebutuhanPrasaranaSD.AnalisisKebutuhanPrasaranaSDController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.MessageBox'
    ],

    alias: 'controller.analisiskebutuhanprasaranasd',

    setupAnalisisKebutuhanPrasaranaSDGrid:function(grid){

    	grid.getStore().load();
    }
});