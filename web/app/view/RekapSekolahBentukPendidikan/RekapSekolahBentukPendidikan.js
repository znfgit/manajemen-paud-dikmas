Ext.define('simdik_batam.view.RekapSekolahBentukPendidikan.RekapSekolahBentukPendidikan', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.rekapsekolahbentukpendidikan',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

        this.items = [{
            xtype: 'panel',
            region:'east',
            flex: 1,
            split:true,
            autoScroll:true,
            layout: {
                type:'vbox',
                padding:'0',
                align:'stretch'
            },
            defaults:{margin:'0 0 10 0'},
            items:[{
                xtype:'panel',
                title: 'Grafik Jumlah Sekolah Berdasarkan Bentuk Pendidikan',
                itemId: 'chartpie',
                split: true,
                layout:'fit',
                border:false,
                items:[{
                    xtype: 'chartpiesekolahbentukpendidikan',
                    height:300
                }]
            },{
                xtype:'panel',
                title: 'Grafik Sebaran Jumlah Sekolah Berdasarkan Bentuk Pendidikan',
                itemId: 'chartbar',
                split: true,
                layout:'fit',
                border:false,
                items:[{
                    xtype: 'chartbarsekolahbentukpendidikan',
                    minHeight:500
                }]
            }]
        },{
            xtype: 'panel',
            region: 'center',
            layout:'border',
            title:'Jumlah Sekolah Berdasarkan Bentuk Pendidikan',
            items:[{
                xtype: 'sekolahbentukpendidikantotalgrid',
                region:'center'
            },{
                xtype: 'sekolahbentukpendidikangrid',
                region: 'south',
                height:280,
                split:true,
                collapsible:true,
                title:'Tabel Sebaran Jumlah Sekolah Berdasarkan Bentuk Pendidikan'
            }]
        }];

        this.callParent(arguments);
    }
});