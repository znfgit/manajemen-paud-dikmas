Ext.define('simdik_batam.view.RekapPdAngkaMengulang.RekapPdAngkaMengulangController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.MessageBox'
    ],

    alias: 'controller.rekappdangkamengulang',

    setupPdAngkaMengulangGrid:function(grid){
        grid.down('toolbar').add({
            xtype:'semestercombo',
            emptyText:'Pilih Semester...',
            listeners: {
                change: function(combo, newValue, oldValue, eOpts ) {
                    var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();

                    grid.getStore().reload();

                        // grid.up('panel').down('ptkstatuskepegawaiangrid').getStore().reload();

                    grid.up('panel').up('panel').down('panel[region=south]').down('chartbarpdangkamengulang').down('cartesian').getStore().reload({
                        params:{
                            semester_id:newValue,
                            bentuk_pendidikan_id: keyword
                        }
                    });
                }
            }
        });
        
    	grid.getStore().on('beforeload', function(store){
            // var keyword = grid.up('panel').down('ptkmatapelajarantotalgrid').down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var semester_id = grid.down('toolbar').down('semestercombo').getValue();
            
            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword,
                semester_id: semester_id
            });
        });

    	grid.getStore().load();
    },
    exportXls:function(btn){
        var url = btn.urls;

       var bentuk_pendidikan_id = btn.up('toolbar').down('combobox').getValue();

       window.location = url+'/'+bentuk_pendidikan_id;
    },
});