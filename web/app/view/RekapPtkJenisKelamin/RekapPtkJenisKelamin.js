Ext.define('simdik_batam.view.RekapPtkJenisKelamin.RekapPtkJenisKelamin', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.rekapptkjeniskelamin',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

        this.items = [{
            xtype: 'panel',
            region:'east',
            flex: 1,
            split:true,
            autoScroll:true,
            layout: {
                type:'vbox',
                padding:'0',
                align:'stretch'
            },
            defaults:{margin:'0 0 10 0'},
            items:[{
                xtype:'panel',
                title: 'Grafik Jumlah PTK Berdasarkan Jenis Kelamin',
                itemId: 'chartpie',
                split: true,
                layout:'fit',
                border:false,
                items:[{
                    xtype: 'chartpieptkjeniskelamin',
                    height:300
                }]
            },{
                xtype:'panel',
                title: 'Grafik Sebaran Jumlah PTK Berdasarkan Jenis Kelamin',
                itemId: 'chartbar',
                split: true,
                layout:'fit',
                border:false,
                items:[{
                    xtype: 'chartbarptkjeniskelamin',
                    minHeight:500
                }]
            }]
        },{
            xtype: 'panel',
            region: 'center',
            layout:'border',
            title:'Jumlah PTK Berdasarkan Jenis Kelamin',
            collapsible:true,
            items:[{
                xtype: 'ptkjeniskelamintotalgrid',
                region:'center'
            },{
                xtype: 'ptkjeniskelamingrid',
                region: 'south',
                height:400,
                title:'Tabel Sebaran Jumlah PTK Berdasarkan Jenis Kelamin'
            }]
        }];

        this.callParent(arguments);
    }
});