Ext.define('simdik_batam.view.ApkApm.ApkApm', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.apkapmpanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

    	this.items = [{
    		xtype: 'apkapmform',
            title: 'APK APM',
            region:'center'
    	}];

    	this.callParent(arguments);
    }
});