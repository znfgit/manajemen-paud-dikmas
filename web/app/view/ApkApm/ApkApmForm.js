Ext.define('simdik_batam.view.ApkApm.ApkApmForm', {
    extend: 'Ext.form.Panel',
    alias: 'widget.apkapmform',
    bodyPadding:20,
    autoScroll:true,
    defaults: {
        border: false,
        xtype: 'panel',
        flex: 1,
        layout: 'anchor'
    }, 
    layout: {
        type:'hbox'
    },
    requires:[
        'simdik_batam.view.ApkApm.ApkApmController'
    ],
    controller: 'apkapm',
    listeners:{
        afterrender:'setupApkApmForm'
    },
    initComponent: function() {
        
        var record = this.initialConfig.record ? this.initialConfig.record : false;
        if (record){
            this.listeners = {
                afterrender: function(form, options) {
                    form.loadRecord(record);
                }
            };
        }

        this.dockedItems = [{
            xtype:'toolbar',
            dock:'top',
            ui:'footer',
            items:['-',{
                text:'Simpan',
                handler:'simpanApkApm'
            }]
        }];

        this.items = [{
            defaults: {
                anchor: '-5'
            },
            items:[{
                xtype: 'hidden'
                ,fieldLabel: 'ID'
                ,labelAlign: 'left'
                ,labelSeparator: ':<span style="color: rgb(255, 0, 0); padding-left: 2px;">*</span>'
                ,allowBlank: false
                ,maxValue: 99999999
                ,minValue: 0
                ,name: 'apk_apm_id'
            },{
                xtype: 'fieldset'
                ,title: 'Input Jumlah Penduduk'
                ,collapsible: true
                ,labelAlign: 'left'
                ,defaults: {
                    labelWidth: 250
                    ,anchor: '100%'
                    ,margins: '0 0 0 5',
                    renderer:function(v){
                        return '<b>' + v + '</b>';
                    }
                }
                ,items: [{
                    xtype: 'textfield'
                    ,allowBlank: false
                    ,minValue: 0
                    ,fieldLabel: 'Jumlah Penduduk 6-12 Tahun'
                    ,maxLength: 80
                    ,enforceMaxLength: true
                    ,labelAlign: 'left'
                    ,margins: '0 0 0 0'
                    ,name: 'jumlah_penduduk_6_12'
                },{
                    xtype: 'textfield'
                    ,allowBlank: false
                    ,minValue: 0
                    ,fieldLabel: 'Jumlah Penduduk 13-15 Tahun'
                    ,maxLength: 80
                    ,enforceMaxLength: true
                    ,labelAlign: 'left'
                    ,margins: '0 0 0 0'
                    ,name: 'jumlah_penduduk_13_15'
                },{
                    xtype: 'textfield'
                    ,allowBlank: false
                    ,minValue: 0
                    ,fieldLabel: 'Jumlah Penduduk 16-19 Tahun'
                    ,maxLength: 80
                    ,enforceMaxLength: true
                    ,labelAlign: 'left'
                    ,margins: '0 0 0 0'
                    ,name: 'jumlah_penduduk_16_19'
                }]
                
            },{
                xtype: 'fieldset'
                ,title: 'Hasil Penghitungan Jumlah Peserta Didik'
                ,collapsible: true
                ,labelAlign: 'left'
                ,defaults: {
                    labelWidth: 250
                    ,anchor: '100%'
                    ,margins: '0 0 0 5'
                    ,renderer:function(v){
                        return '<b>' + v + '</b>';
                    }
                }
                ,items: [{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Jumlah Siswa SD'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'left'
                    ,name: 'jumlah_siswa_sd'
                    
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Jumlah Siswa SD 6-12 Tahun'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'left'
                    ,name: 'jumlah_siswa_sd_6_12'
                    
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Jumlah Siswa SMP'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'left'
                    ,name: 'jumlah_siswa_smp'
                    
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Jumlah Siswa SMP 13-15 Tahun'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'left'
                    ,name: 'jumlah_siswa_smp_13_15'
                    
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Jumlah Siswa SMA'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'left'
                    ,name: 'jumlah_siswa_sma'
                    
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Jumlah Siswa SMA/SMK 16-19 Tahun'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'left'
                    ,name: 'jumlah_siswa_sma_16_19'
                    
                }]
            }]
        },{
            defaults: {
                anchor: '-5'
            },
            items:[{
                xtype: 'fieldset'
                ,title: 'Hasil Penghitungan APK APM'
                ,collapsible: true
                ,labelAlign: 'left'
                ,defaults: {
                    labelWidth: 250
                    ,anchor: '100%'
                    ,margins: '0 0 0 5'
                    ,renderer:function(v){
                        return '<b>' + v + '</b>';
                    }
                }
                ,items: [{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'APK SD'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'left'
                    ,name: 'apk_sd'
                    
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'APM SD'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'left'
                    ,name: 'apm_sd'
                    
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'APK SMP'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'left'
                    ,name: 'apk_smp'
                    
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'APM SMP'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'left'
                    ,name: 'apm_smp'
                    
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'APK SMA'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'left'
                    ,name: 'apk_sma'
                    
                },{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'APM SMA'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'left'
                    ,name: 'apm_sma'
                    
                }]
            },{
                xtype: 'fieldset'
                ,title: 'Pengubahan Terakhir'
                ,collapsible: true
                ,labelAlign: 'left'
                ,defaults: {
                    labelWidth: 250
                    ,anchor: '100%'
                    ,margins: '0 0 0 5'
                    ,renderer:function(v){
                        return '<b>' + v + '</b>';
                    }
                }
                ,items: [{
                    xtype: 'displayfield'
                    ,minValue: 0
                    ,fieldLabel: 'Diubah Terakhir pada Tanggal'
                    ,maxLength: 8
                    ,enforceMaxLength: true
                    ,labelAlign: 'left'
                    ,name: 'tanggal'
                    
                }]
            }]
        }];

        this.callParent(arguments);
    }
});