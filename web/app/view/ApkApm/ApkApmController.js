Ext.define('simdik_batam.view.ApkApm.ApkApmController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.MessageBox'
    ],

    alias: 'controller.apkapm',

    setupApkApmForm:function(form){

        Ext.MessageBox.show({
            msg: 'Memuat Data ...',
            progressText: 'Memuat Data ...',
            width: 200,
            wait: true
        });

    	var store = Ext.create('simdik_batam.store.ApkApm');

        store.on('load',function(store){
            // Ext.Msg.alert('tes');

            var rec = store.getAt(0);

            if(rec){
                form.loadRecord(rec);

                Ext.MessageBox.hide();
            }

        });

        store.load();
    },
    simpanApkApm: function(btn){
        var form  = btn.up('apkapmform');
        var values = form.getForm().getValues();

        // console.log(values);
        var data = Ext.JSON.encode(values);

        Ext.MessageBox.show({
            msg: 'Menyimpan Data ...',
            progressText: 'Menyimpan Data ...',
            width: 200,
            wait: true
        });

        Ext.Ajax.request({
            url     : 'post/ApkApm',
            method  : 'POST',
            params  : {
                data: data
            },
            failure : function(response, options){
                Ext.Msg.alert('Warning','Ada Kesalahan dalam pengolahan data. Mohon dicoba lagi');
            },
            success : function(response){
                var json = Ext.JSON.decode(response.responseText);
                
                if(json.success){
                    Ext.MessageBox.hide();

                    Ext.Msg.alert('Berhasil', json.message);

                    var storeBaru = Ext.create('simdik_batam.store.ApkApm');

                    storeBaru.on('load',function(store){
                        // Ext.Msg.alert('tes');

                        var rec = store.getAt(0);

                        if(rec){
                            form.loadRecord(rec);
                        }

                    });

                    storeBaru.load();

                } else if(json.success == false) {
                    Ext.MessageBox.hide();

                    Ext.Msg.alert('Error',json.message);
                }
            }
        });     
    }
});