Ext.define('simdik_batam.view.RekapPdPerUmur.PdUmurGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.pdumurgrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    requires:[
        'simdik_batam.view.RekapPdPerUmur.RekapPdPerUmurController',
        'Ext.grid.feature.Grouping'
    ],
    controller: 'rekappdperumur',
    listeners:{
        afterrender:'setupPdUmurGrid'
    },
    features: [{
        ftype: 'grouping',
        groupHeaderTpl: '{columnName}: {name} ({rows.length} Sekolah)',
        hideGroupedHeader: false,
        startCollapsed: true,
        enableGroupingMenu: false,
        id: 'sekolahGrouping'
    }],
    formatMoney: function(a, c, d, t){
        var n = a, 
            c = isNaN(c = Math.abs(c)) ? 0 : c, 
            d = d == undefined ? "," : d, 
            t = t == undefined ? "." : t, 
            s = n < 0 ? "-" : "", 
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('simdik_batam.store.ChartBarPesertaDidikPerUmur');
        
        if (this.initialConfig.baseParams) {
            
            var baseParams = this.initialConfig.baseParams;
            this.store.on('beforeload', function(store, options) {
                Ext.apply(store.proxy.extraParams, baseParams);
            });
            
        }
        
        this.getSelectionModel().setSelectionMode('SINGLE');

        // this.groupingFeature = grid.view.getFeature('sekolahGrouping');
        
        this.columns = [{
            header: 'Satuan Pendidikan',
            width:200,
            sortable: true,
            dataIndex: 'nama',
			hideable: false,
            hidden: false
        },{
            header: 'Kecamatan',
            width: 150,
            sortable: true,
            dataIndex: 'kecamatan',
            hideable: true,
            hidden: false,
            align:'left'
        },{
            header: 'Kabupaten',
            width: 150,
            sortable: true,
            dataIndex: 'kabupaten',
            hideable: true,
            hidden: false,
            align:'left'
        },{
            header: 'Total PD',
            width: 150,
            sortable: true,
            dataIndex: 'total',
			hideable: false,
            hidden: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        },{
            text: 'Kelompok Usia (Tahun) SMA/SMK/SMLB/SLB',
            itemId:'sma', 
            columns: [{
                header: '<15',
                width: 100,
                sortable: true,
                dataIndex: 'kurang_limabelas',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return grid.formatMoney(Math.abs(v),0);
                }
            },{
                header: '15-18',
                width: 100,
                sortable: true,
                dataIndex: 'limabelas_delapanbelas',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return grid.formatMoney(Math.abs(v),0);
                }
            },{
                header: '>18',
                width: 100,
                sortable: true,
                dataIndex: 'lebih_delapanbelas',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return grid.formatMoney(Math.abs(v),0);
                }
            }]
        }];

        this.dockedItems = [{
            xtype: 'toolbar',
            items: ['-',{
                xtype:'bentukpendidikancombo',
                name: 'pilihbentukcombo',
                emptyText:'Pilih Bentuk Pendidikan...',
                displayField: 'nama',
                width: 200,
                listeners: {
                    'change': function(field, newValue, oldValue) {
                        
                        grid.getStore().reload();
                    }
                }
            },{
                xtype:'button',
                text:'Export Xls',
                glyph: 61474,
                urls: 'excel/SebaranPdPerUmur',
                handler:function(btn){
                    var bentuk_pendidikan_id = btn.up('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
                    var semester_id = btn.up('toolbar').down('semestercombo').getValue();

                    window.location = 'excel/SebaranPdPerUmur?bentuk_pendidikan_id='+bentuk_pendidikan_id+'&semester_id='+semester_id;
                }
            }]
        }];
        
        // this.bbar = Ext.create('Ext.PagingToolbar', {
        //     store: this.store,
        //     displayInfo: true,
        //     displayMsg: 'Displaying data {0} - {1} of {2}',
        //     emptyMsg: "Tidak ada data"
        // });
        
        this.callParent(arguments);
    }
});