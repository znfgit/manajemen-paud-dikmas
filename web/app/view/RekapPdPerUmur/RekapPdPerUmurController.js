Ext.define('simdik_batam.view.RekapPdPerUmur.RekapPdPerUmurController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.MessageBox'
    ],

    alias: 'controller.rekappdperumur',
    exportXls:function(btn){
        var url = btn.urls;

       var bentuk_pendidikan_id = btn.up('toolbar').down('combobox').getValue();

       window.location = url+'/'+bentuk_pendidikan_id;
    },
    setupPdUmurGrid:function(grid){
        grid.down('toolbar').add({
            xtype:'semestercombo',
            emptyText:'Pilih Semester...',
            listeners: {
                change: function(combo, newValue, oldValue, eOpts ) {
                    grid.getStore().reload();
                }
            }
        });

    	grid.getStore().on('beforeload', function(store){
            var keyword = grid.down('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            var semester_id = grid.down('toolbar').down('semestercombo').getValue();

            Ext.apply(store.proxy.extraParams, {
                bentuk_pendidikan_id: keyword,
                semester_id: semester_id
            });
        });

        // grid.down('toolbar').down('combobox[name=pilihbentukcombo]').on('change',function(field, newValue, oldValue){
        //     if(newValue == 5 || newValue == 7 || newValue == 9 || newValue == 991){
                
        //         grid.columns[2].show();
        //         grid.columns[3].show();
        //         grid.columns[4].show();

        //         grid.columns[5].hide();
        //         grid.columns[6].hide();
        //         grid.columns[7].hide();

        //         grid.columns[8].hide();
        //         grid.columns[9].hide();
        //         grid.columns[10].hide();
            
        //     }else if(newValue == 6 || newValue == 8 || newValue == 10 || newValue == 992){
                
        //         grid.columns[2].hide();
        //         grid.columns[3].hide();
        //         grid.columns[4].hide();

        //         grid.columns[5].show();
        //         grid.columns[6].show();
        //         grid.columns[7].show();

        //         grid.columns[8].hide();
        //         grid.columns[9].hide();
        //         grid.columns[10].hide();
            
        //     }else if(newValue == 15 || newValue == 13 || newValue == 14 || newValue == 16 || newValue == 17 || newValue == 993){

        //         grid.columns[2].hide();
        //         grid.columns[3].hide();
        //         grid.columns[4].hide();

        //         grid.columns[5].hide();
        //         grid.columns[6].hide();
        //         grid.columns[7].hide();

        //         grid.columns[8].show();
        //         grid.columns[9].show();
        //         grid.columns[10].show();

        //     }else if(newValue == 999 || newValue == ''){

        //         grid.columns[2].show();
        //         grid.columns[3].show();
        //         grid.columns[4].show();

        //         grid.columns[5].show();
        //         grid.columns[6].show();
        //         grid.columns[7].show();

        //         grid.columns[8].show();
        //         grid.columns[9].show();
        //         grid.columns[10].show();
        //     }
        // });

        // grid.columns[2].hide();
        // grid.columns[3].hide();
        // grid.columns[4].hide();

        // grid.columns[5].hide();
        // grid.columns[6].hide();
        // grid.columns[7].hide();

        // grid.columns[8].hide();
        // grid.columns[9].hide();
        // grid.columns[10].hide();

    	grid.getStore().load();
    }
});