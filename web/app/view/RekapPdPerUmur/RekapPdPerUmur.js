Ext.define('simdik_batam.view.RekapPdPerUmur.RekapPdPerUmur', {
	extend: 'Ext.panel.Panel',
	alias: 'widget.pdperumurpanel',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

    	this.items = [{
           xtype:'panel',
            region:'center',
            split:true,
            layout:'border',
            items:[{
                xtype:'pdumurgrid',
                region:'center',
                title: 'Jumlah Peserta Didik Berdasarkan Usia Per Satuan Pendidikan'
            }]
        }];

    	this.callParent(arguments);
    }
});