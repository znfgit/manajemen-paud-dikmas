Ext.define('simdik_batam.view.RekapSekolahRangkuman.RekapSekolahRangkuman', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.rekapsekolahrangkuman',
    border: false,
    bodyBorder: false,
    layout: 'border',
    
    initComponent:function(){

        this.title = 'Rekap Rangkuman Sekolah Per Wilayah';

    	this.items = [{
    		xtype:'sekolahrangkumangrid',
            region:'center'
    	}]

    	this.callParent(arguments);
    }
});