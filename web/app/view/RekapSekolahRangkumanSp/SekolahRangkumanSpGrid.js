Ext.define('simdik_batam.view.RekapSekolahRangkumanSp.SekolahRangkumanSpGrid', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.sekolahrangkumanspgrid',
    title: '',
    selType: 'rowmodel',
    autoScroll: true,
    requires:[
        'Ext.grid.feature.Grouping'
    ],
    listeners:{
        afterrender:'setupSekolahRangkumanSpGrid'
    },
    // features: [{
    //     ftype: 'grouping',
    //     groupHeaderTpl: '{columnName}: {name} ({rows.length} Sekolah)',
    //     hideGroupedHeader: false,
    //     startCollapsed: true,
    //     id: 'sekolahGrouping'
    // }],
    formatMoney: function(a, c, d, t){
        var n = a, 
            c = isNaN(c = Math.abs(c)) ? 0 : c, 
            d = d == undefined ? "," : d, 
            t = t == undefined ? "." : t, 
            s = n < 0 ? "-" : "", 
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
    },
    initComponent: function() {
        
        var grid = this;
        
        this.store = Ext.create('simdik_batam.store.SekolahRangkumanSp');
        
        var id_level_wilayah = this.initialConfig.idLevelWilayah;
        var kode_wilayah = this.initialConfig.kodeWilayah;

        this.store.on('beforeload', function(store, options) {
            Ext.apply(store.proxy.extraParams, {
                id_level_wilayah: id_level_wilayah,
                kode_wilayah: kode_wilayah
            });
        });
     
        
        this.getSelectionModel().setSelectionMode('SINGLE');

        // this.groupingFeature = grid.view.getFeature('sekolahGrouping');
        
        this.columns = [{
            header: 'Satuan Pendidikan',
            width:200,
            sortable: true,
            dataIndex: 'desk',
            locked:true,
			hideable: false,
            hidden: false,
            renderer:function(v,p,r){
                return '<a href="#menu/DetailSekolah/'+r.data.sekolah_id+'">' + v + '</a>';
            }
        },{
            header: 'NPSN',
            width:150,
            sortable: true,
            dataIndex: 'npsn',
            hideable: false,
            hidden: false
        },{
            header: 'Kecamatan',
            width:150,
            sortable: true,
            dataIndex: 'kecamatan',
            hideable: false,
            hidden: false
        },{
            header: 'Kabupaten',
            width:150,
            sortable: true,
            dataIndex: 'kabupaten',
            hideable: false,
            hidden: false
        },{
            text: 'PTK',
            itemId:'ptk_group', 
            columns: [{
                header: 'Guru',
                width: 100,
                sortable: true,
                dataIndex: 'ptk',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return grid.formatMoney(Math.abs(v),0);
                }
            },{
                header: 'Pegawai',
                width: 100,
                sortable: true,
                dataIndex: 'pegawai',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return grid.formatMoney(Math.abs(v),0);
                }
            },{
                header: 'Total',
                width: 100,
                sortable: true,
                dataIndex: 'ptk_total',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return grid.formatMoney(Math.abs(v),0);
                }
            }]
        },{
            text: 'Peserta Didik',
            itemId:'pd_group', 
            columns: [{
                header: 'Kelas 10',
                width: 100,
                sortable: true,
                dataIndex: 'pd_kelas_10',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return grid.formatMoney(Math.abs(v),0);
                }
            },{
                header: 'Kelas 11',
                width: 100,
                sortable: true,
                dataIndex: 'pd_kelas_11',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return grid.formatMoney(Math.abs(v),0);
                }
            },{
                header: 'Kelas 12',
                width: 100,
                sortable: true,
                dataIndex: 'pd_kelas_12',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return grid.formatMoney(Math.abs(v),0);
                }
            },{
                header: 'Kelas 13',
                width: 100,
                sortable: true,
                dataIndex: 'pd_kelas_13',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return grid.formatMoney(Math.abs(v),0);
                }
            },{
                header: 'Total',
                width: 100,
                sortable: true,
                dataIndex: 'pd',
                hideable: false,
                align:'right',
                renderer:function(v,p,r){
                    return grid.formatMoney(Math.abs(v),0);
                }
            }]
        },{
            header: 'Rombel',
            width: 100,
            sortable: true,
            dataIndex: 'rombel',
            hideable: false,
            align:'right',
            renderer:function(v,p,r){
                return grid.formatMoney(Math.abs(v),0);
            }
        }];

        this.dockedItems = [{
            xtype: 'toolbar',
            items: ['-',{
                xtype:'bentukpendidikancombo',
                name: 'pilihbentukcombo',
                emptyText:'Pilih Bentuk Pendidikan...',
                displayField: 'nama',
                width: 200,
                listeners: {
                    'change': function(field, newValue, oldValue) {
                        
                        grid.getStore().reload();
                    }
                }
            }
            // ,{
            //     xtype:'button',
            //     text:'Export Xls',
            //     glyph: 61474,
            //     urls: 'excel/SebaranPdPerUmur',
            //     handler:function(btn){
            //         var bentuk_pendidikan_id = btn.up('toolbar').down('combobox[name=pilihbentukcombo]').getValue();
            //         var semester_id = btn.up('toolbar').down('semestercombo').getValue();

            //         window.location = 'excel/SebaranPdPerUmur?bentuk_pendidikan_id='+bentuk_pendidikan_id+'&semester_id='+semester_id;
            //     }
            // }
            ]
        }];
        
        this.bbar = ['-'];
        
        this.callParent(arguments);
    }
});