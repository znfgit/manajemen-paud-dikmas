Ext.application({
    name: 'DataDikdasLogin',
    launch: function() {
                
        Ext.define('DataDikdas.model.Semester', {
            extend: 'Ext.data.Model',
            idProperty: 'semester_id',
            fields: [
                { name: 'semester_id', type: 'string'  }, 
                { name: 'tahun_ajaran_id', type: 'float', useNull: true },
                { name: 'tahun_ajaran_id_str', type: 'string'  }, 
                { name: 'semester', type: 'int'  }, 
                { name: 'periode_aktif', type: 'int'  }, 
                { name: 'tanggal_mulai', type: 'date',  dateFormat: 'Y-m-d H:i:s'}, 
                { name: 'tanggal_selesai', type: 'date',  dateFormat: 'Y-m-d H:i:s'}, 
                { name: 'create_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'}, 
                { name: 'expired_date', type: 'date',  dateFormat: 'Y-m-d H:i:s'}
                
            ],
            proxy: {
                type: 'rest',
                url : 'rest/Semester',
                reader: {
                    type: 'json',
                    root: 'rows',
                    totalProperty: 'results'
                },
                listeners: {
                    exception: function(proxy, response, operation, eOpts) {
                        // console.log(response);
                        if (response.status == '400') {
                            var json = Ext.decode(response.responseText);                    
                            Xond.msg('Error', json.message);
                        }
                    }
                }
            }
        });

        Ext.define('DataDikdas.store.Semester', {
            extend: 'Ext.data.Store',
            requires: 'DataDikdas.model.Semester',
            model: 'DataDikdas.model.Semester',
            pageSize: 50,
            autoLoad: false    
        });
        

        Ext.define('DataDikdas.view._components.combo.Semester', {
            extend: 'Ext.form.field.ComboBox',
            queryMode: 'remote',
            pageSize: 20,
            valueField: 'semester_id',
            displayField: 'semester_id',
            label: 'Ref.semester',    
            alias: 'widget.semestercombo',
            //hideTrigger: false,
            //xtype: 'semestercombo',
            listeners: {
                beforerender: function(combo, options) {
                    this.store.load();
                }
            },
            initComponent: function() {
                this.store = Ext.create('DataDikdas.store.Semester', {
                    model: 'DataDikdas.model.Semester',
                    sorters: ['semester_id']
                });
                this.callParent(arguments); 
            }
        });

        Ext.define('KitchenSink.view.form.LoginForm', {
            extend: 'Ext.form.Panel',
            bodyStyle: 'background-color: #EEEEEE; padding-left:20; padding-right:20, color: white',                
            xtype: 'login-form',
            frame: false,            
            border: false,
            width: 600,
            bodyPadding: '10 10 10 10',
            defaultType: 'textfield',

            defaults: {
                anchor: '100%'
            },
            items: [
                {
                    allowBlank: false,
                    fieldLabel: 'Username',
                    name: 'username',
                    emptyText: 'Email',
					vtype: 'email'
                },{
                    allowBlank: false,
                    fieldLabel: 'Password',
                    name: 'password',
                    emptyText: 'Password',
                    inputType: 'password'
                }
				/*,{
					xtype: 'semestercombo',
                    fieldLabel: 'Periode',
                    tpl: Ext.create('Ext.XTemplate',
                        '<tpl for=".">',
                            '<div class="x-boundlist-item"><tpl if="semester==1">Ganjil<tpl else>Genap</tpl> / {tahun_ajaran_id}</div>',
                        '</tpl>'
                    ),
                    displayTpl: Ext.create('Ext.XTemplate',
                        '<tpl for=".">',
                            '<tpl if="semester==1">Ganjil<tpl else>Genap</tpl> / {tahun_ajaran_id}',
                        '</tpl>'
                    )
				}
				*/
                ,{
                    xtype: 'semestercombo',
                    fieldLabel: 'Periode',
                    name: 'semester_id',
                    hiddenName: 'semester_id',
                    //tpl: '<tpl for="."><div class="x-combo-list-item">Smt {semester}/{tahun_ajaran_id}</div></tpl>',
                    tpl: Ext.create('Ext.XTemplate',
                        '<tpl for=".">',
                            '<div class="x-boundlist-item"><tpl if="semester==1">Ganjil<tpl else>Genap</tpl> / {tahun_ajaran_id}-{tahun_ajaran_id+1}</div>',
                        '</tpl>'
                    ),
                    // template for the content inside text field
                    displayTpl: Ext.create('Ext.XTemplate',
                        '<tpl for=".">',
                            '<tpl if="semester==1">Ganjil<tpl else>Genap</tpl> / {tahun_ajaran_id}-{tahun_ajaran_id+1}',
                        '</tpl>'
                    ),
                    listeners: {
                        select: function(combo) {
                            
                            var store = combo.getStore()
                            var rec = store.getById(combo.getValue());                            
                            var semester = rec.get('semester') == 1 ? 'Ganjil' : 'Genap';
                            var tahun = rec.get('tahun_ajaran_id');
                            
                            DataDikdas.global.semester = combo.getValue();
                            
                            Xond.msg('Info', 'Periode terpilih: Semester ' + semester + ' / ' + tahun );
                            //console.log(portal.up('viewport'));
                        }
                    },
                    pageSize: 0,
                    //labelWidth: 60,
                    hideTrigger: false
                }
            ]
        });

        var win = new Ext.window.Window({   
            height: 362,
            width: 412,
            resizable: false,
           
            title: 'Aplikasi Dapodikmen',
            autoScroll: false,
            frame: true,            
            border: true,
            layout: 'vbox',
			closable: false,
            items: [{
                width: 412,
                height: 210,
                layout: 'fit',
                items: [{
                    bodyStyle: 'background-image:url(/resources/images/splash-dapodikmen.png)',
                    plain: true,
                    border: false,
                    anchor: '100%'
                }]
            },{
                layout: 'fit',
                border: false,
                items: [{
                    xtype: 'login-form'
                }]
            }],
            buttons: [{
                text: 'Registrasi' ,
                handler: function() {
                    window.location='/registrasi.html';
                }
            },{ 
                text: 'Login' ,
                handler: function() {
                    var form = this.up('window').down('login-form').getForm();

                    if (form.isValid()) { 
                    
                        Ext.MessageBox.show({
                            msg: 'Please wait...',
                            progressText: 'Please wait...',
                            width: 200,
                            wait: true
                        });
                    
                        form.submit({
                            clientValidation: true,
                            url: '/login',
                            submitEmptyText: false,
                            success: function(form, action) {
                                
                                var val = action.result;
                                if (val.success) {
                                    Ext.MessageBox.hide();
                                    Ext.MessageBox.alert('Berhasil', 'Login Berhasil<br> Mohon tunggu beberapa saat....');
                                    
                                    setTimeout( function() {
                                        window.location='/';
                                    }, 50);
                                } else {
                                    Ext.MessageBox.alert('Gagal', val.message);
                                }
                            },
                            failure: function(form, action) {
                                //...
                                var val = action.result;
                                Ext.MessageBox.alert('Gagal', val.message);
                            }
                        });
                    } else {
                        Ext.Msg.alert('Error', 'Form harap isi dengan benar');
                    }
                }
            }]
        });
        
        var smt = win.down('semestercombo');
        smt.getStore().on('load', function(store){
            //store.filter('periode_aktif', 1);
            smt.setValue('20131');
            //smt.setRawValue('Ganjil / 2013'); 
        });
        
        smt.getStore().on('beforeload', function(store, options) {
            Ext.apply(store.proxy.extraParams, {
                periode_aktif: 1
            });
        });

        smt.getStore().load();

        win.show();
    }

}); 