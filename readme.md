# Profil Pendidikan #


### List Item ###

- Jumlah Peserta Didik Berdasarkan Jenis Kelamin (pie)
- Jumlah Peserta Didik Berdasarkan Jenis Kelamin per Kecamatan (Bar)
- Jumlah Peserta Didik Berdasarkan Jenis Kelamin per Kecamatan (Table)
- Jumlah Peserta Didik Berdasarkan Agama (pie)
- Jumlah Peserta Didik Berdasarkan Agama per Kecamatan (Bar)
- Jumlah Peserta Didik Berdasarkan Agama per Kecamatan (Table)
- Jumlah PTK Berdasarkan Jenis Kelamin (pie)
- Jumlah PTK Berdasarkan Jenis Kelamin per Kecamatan (Bar)
- Jumlah PTK Berdasarkan Jenis Kelamin per Kecamatan (Table)
- Jumlah Sekolah Berdasarkan Status (pie)
- Jumlah Sekolah Berdasarkan Status per kecamatan (Bar)
- Jumlah Sekolah Berdasarkan Status per kecamatan (Table)
- Jumlah Sekolah Berdasarkan Bentuk Pendidikan (pie)
- Jumlah Sekolah Berdasarkan Bentuk Pendidikan per kecamatan (Bar)
- Jumlah Sekolah Berdasarkan Bentuk Pendidikan per kecamatan (Table)
- Jumlah Prasarana Berdasarkan Jenis Prasarana (Table)
- Jumlah Prasarana Berdasarkan Jenis Prasarana (Column)


### Data Pokok ###

**Sekolah**

- Nama atau NPSN
- Kecamatan
- Bentuk Pendidikan
- Status Sekolah

**PTK**

- Nama atau NUPTK

**Peserta Didik**

- Nama atau NISN

**Rombongan Belajar**