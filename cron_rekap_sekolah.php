<?php

$today = date('Y-m-d');

echo "Inisiasi rekap sekolah tanggal ".$today."<br>";

function initDapodikmenDb(){
	$serverName = "172.17.3.18";
	$connectionInfo = array( "Database"=>"Dapodikmen", "UID"=>"UserRpt", "PWD"=>"Reportd1km3n",'ReturnDatesAsStrings'=>true);
	$con_dapodikmen = sqlsrv_connect( $serverName, $connectionInfo);

	return $con_dapodikmen;
}

function initRekapDb(){
	$serverName = "(local)";
	$connectionInfo = array( "Database"=>"rekap_dapodikmen", "UID"=>"report", "PWD"=>"ReportDikmen",'ReturnDatesAsStrings'=>true);
	/* Connect using Windows Authentication. */
	$con_rekap = sqlsrv_connect( $serverName, $connectionInfo);

	return $con_rekap;
}


// pre requisite delete
$con_rekap = initRekapDb();

$sql_delete = "delete from rekap_sekolah";
$stmt_delete = sqlsrv_query( $con_rekap, $sql_delete);

if($stmt_delete){
	echo "Berhasil menghapus data masa lalu ...<br>";
}else{
	echo "Gagal menghapus data masa lalu ... <br>";
}

// end of pre requisite delete

// start of ngambil data dari dapodikmen pusat
$con_dapodikmen = initDapodikmenDb();

if($con_dapodikmen){
	echo "Berhasil konek ke database ...<br>";
}else{
	echo "Gagal";
}

$sql_semester = "select * from ref.semester where periode_aktif = 1 and tahun_ajaran_id = 2014 and expired_date is null order by semester_id desc";

$stmt_semester = sqlsrv_query( $con_dapodikmen, $sql_semester);
while($row_semester = sqlsrv_fetch_array( $stmt_semester, SQLSRV_FETCH_ASSOC)){
	echo "Memproses data untuk semester ".$row_semester['nama']."<br>";

	$sql = "SELECT
				GETDATE() as tanggal,
				(
					SELECT
						COUNT (1)
					FROM
						ptk ptk
					JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
					JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
					WHERE
						ptkd.sekolah_id = s.sekolah_id
					AND ptk.Soft_delete = 0
					AND ptkd.Soft_delete = 0
					AND ptkd.tahun_ajaran_id = ".$row_semester['tahun_ajaran_id']."
					AND ptk.jenis_ptk_id in (3,4,5,6,12,13,14)
					AND (
						ptkd.tgl_ptk_keluar > ta.tanggal_selesai
						OR ptkd.jenis_keluar_id IS NULL
					)
				) AS ptk,
				(
					SELECT
						COUNT (1)
					FROM
						ptk ptk
					JOIN ptk_terdaftar ptkd ON ptk.ptk_id = ptkd.ptk_id
					JOIN ref.tahun_ajaran ta ON ta.tahun_ajaran_id = ptkd.tahun_ajaran_id
					WHERE
						ptkd.sekolah_id = s.sekolah_id
					AND ptk.Soft_delete = 0
					AND ptkd.Soft_delete = 0
					AND ptkd.tahun_ajaran_id = ".$row_semester['tahun_ajaran_id']."
					AND ptk.jenis_ptk_id in (11,99,30,40)
					AND (
						ptkd.tgl_ptk_keluar > ta.tanggal_selesai
						OR ptkd.jenis_keluar_id IS NULL
					)
				) AS pegawai,
				(
					SELECT
						COUNT (1) AS jumlah
					FROM
						registrasi_peserta_didik rpd
					JOIN peserta_didik pd ON rpd.peserta_didik_id = pd.peserta_didik_id
					JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
					JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
					JOIN ref.semester sm ON sm.semester_id = rb.semester_id
					WHERE
						rpd.Soft_delete = 0
					AND pd.Soft_delete = 0
					AND (
						(
							rpd.jenis_keluar_id IS NULL
							AND rpd.tanggal_keluar IS NULL
						)
						OR (
							rpd.jenis_keluar_id IS NOT NULL
							AND rpd.tanggal_keluar > sm.tanggal_selesai
						)
						OR (rpd.jenis_keluar_id IS NULL)
					)
					AND rb.semester_id = '".$row_semester['semester_id']."'
					AND s.sekolah_id = rpd.sekolah_id
					AND rb.soft_delete = 0
					AND ar.soft_delete = 0
					AND rb.jenis_rombel = 1
				) AS pd,
				(
					SELECT
						COUNT (1) AS jumlah
					FROM
						rombongan_belajar
					WHERE
						sekolah_id = s.sekolah_id
					AND soft_delete = 0
					AND semester_id = '".$row_semester['semester_id']."'
				) AS rombel,
				(
					select count (1) from dbo.sync_log where sekolah_id = s.sekolah_id and sync_media=1 and alamat_ip NOT LIKE 'prefil%' and begin_sync >= '".$row_semester['tanggal_mulai']."' and begin_sync <= '".$row_semester['tanggal_selesai']."' 
				) as jumlah_kirim,
				w1.kode_wilayah AS kode_wilayah_kecamatan,
				w1.nama AS kecamatan,
				w1.mst_kode_wilayah as mst_kode_wilayah_kecamatan,
				w1.id_level_wilayah as id_level_wilayah_kecamatan,
				w2.kode_wilayah AS kode_wilayah_kabupaten,
				w2.nama AS kabupaten,
				w2.mst_kode_wilayah as mst_kode_wilayah_kabupaten,
				w2.id_level_wilayah as id_level_wilayah_kabupaten,
				w3.kode_wilayah AS kode_wilayah_provinsi,
				w3.nama AS provinsi,
				w3.mst_kode_wilayah as mst_kode_wilayah_provinsi,
				w3.id_level_wilayah as id_level_wilayah_provinsi,
				'".$row_semester['semester_id']."' as semester,
				s.*
			FROM
				sekolah s
			JOIN ref.mst_wilayah w1 ON w1.kode_wilayah = s.kode_wilayah
			JOIN ref.mst_wilayah w2 ON w2.kode_wilayah = w1.mst_kode_wilayah
			JOIN ref.mst_wilayah w3 ON w3.kode_wilayah = w2.mst_kode_wilayah
			WHERE
				soft_delete = 0;";

	$stmt = sqlsrv_query( $con_dapodikmen, $sql);

	if($stmt){
		echo "Berhasil ambil data...<br>";
	}else{
		echo "gagal ambil data ...". $sql ."<br>";
	}

	while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)){

		$sql_kelas = "
					SELECT
						sum(case when rb.tingkat_pendidikan_id = 10 and pd.jenis_kelamin = 'L' then 1 else 0 end ) as pd_kelas_10_laki,
						sum(case when rb.tingkat_pendidikan_id = 10 and pd.jenis_kelamin = 'P' then 1 else 0 end ) as pd_kelas_10_perempuan,
						sum(case when rb.tingkat_pendidikan_id = 11 and pd.jenis_kelamin = 'L' then 1 else 0 end ) as pd_kelas_11_laki,
						sum(case when rb.tingkat_pendidikan_id = 11 and pd.jenis_kelamin = 'P' then 1 else 0 end ) as pd_kelas_11_perempuan,
						sum(case when rb.tingkat_pendidikan_id = 12 and pd.jenis_kelamin = 'L' then 1 else 0 end ) as pd_kelas_12_laki,
						sum(case when rb.tingkat_pendidikan_id = 12 and pd.jenis_kelamin = 'P' then 1 else 0 end ) as pd_kelas_12_perempuan,
						sum(case when rb.tingkat_pendidikan_id = 13 and pd.jenis_kelamin = 'L' then 1 else 0 end ) as pd_kelas_13_laki,
						sum(case when rb.tingkat_pendidikan_id = 13 and pd.jenis_kelamin = 'P' then 1 else 0 end ) as pd_kelas_13_perempuan
					FROM
						registrasi_peserta_didik rpd
					JOIN peserta_didik pd ON rpd.peserta_didik_id = pd.peserta_didik_id
					JOIN anggota_rombel ar ON ar.peserta_didik_id = pd.peserta_didik_id
					JOIN rombongan_belajar rb ON rb.rombongan_belajar_id = ar.rombongan_belajar_id
					JOIN ref.semester sm ON sm.semester_id = rb.semester_id
					WHERE
						rpd.Soft_delete = 0
					AND pd.Soft_delete = 0
					AND (
						(
							rpd.jenis_keluar_id IS NULL
							AND rpd.tanggal_keluar IS NULL
						)
						OR (
							rpd.jenis_keluar_id IS NOT NULL
							AND rpd.tanggal_keluar > sm.tanggal_selesai
						)
						OR (rpd.jenis_keluar_id IS NULL)
					)
					AND rb.semester_id = '".$row_semester['semester_id']."'
					AND rpd.sekolah_id = '".$row['sekolah_id']."'
					AND rb.soft_delete = 0
					AND ar.soft_delete = 0
					AND rb.jenis_rombel = 1
				";
		$stmt_kelas = sqlsrv_query( $con_dapodikmen, $sql_kelas);
		$row_kelas = sqlsrv_fetch_array( $stmt_kelas, SQLSRV_FETCH_ASSOC);

		$con_rekap = initRekapDb();

		if($con_rekap){
			echo "Berhasil konek ke database lokal ...<br>";
		}else{
			echo "Gagal konek ke database lokal ...<br>";
		}

		$insert = "insert into rekap_sekolah 
					(
						rekap_sekolah_id,
						sekolah_id,
						tanggal,
						ptk,
						pegawai,
						pd,
						rombel,
						kode_wilayah_kecamatan,
						kecamatan,
						kode_wilayah_kabupaten,
						kabupaten,
						kode_wilayah_provinsi,
						provinsi,
						mst_kode_wilayah_kecamatan,
						mst_kode_wilayah_kabupaten,
						mst_kode_wilayah_provinsi,
						id_level_wilayah_kecamatan,
						id_level_wilayah_kabupaten,
						id_level_wilayah_provinsi,
						nama,
						npsn,
						bentuk_pendidikan_id,
						status_sekolah,
						semester_id,
						tahun_ajaran_id,
						pd_kelas_10,
						pd_kelas_10_laki,
						pd_kelas_10_perempuan,
						pd_kelas_11,
						pd_kelas_11_laki,
						pd_kelas_11_perempuan,
						pd_kelas_12,
						pd_kelas_12_laki,
						pd_kelas_12_perempuan,
						pd_kelas_13,
						pd_kelas_13_laki,
						pd_kelas_13_perempuan,
						jumlah_kirim
					) values (
						NEWID(),
						'".$row['sekolah_id']."',
						'".$today."',
						".$row['ptk'].",
						".$row['pegawai'].",
						".$row['pd'].",
						".$row['rombel'].",
						'".$row['kode_wilayah_kecamatan']."',
						'".$row['kecamatan']."',
						'".$row['kode_wilayah_kabupaten']."',
						'".$row['kabupaten']."',
						'".$row['kode_wilayah_provinsi']."',
						'".$row['provinsi']."',
						'".$row['mst_kode_wilayah_kecamatan']."',
						'".$row['mst_kode_wilayah_kabupaten']."',
						'".$row['mst_kode_wilayah_provinsi']."',
						'".$row['id_level_wilayah_kecamatan']."',
						'".$row['id_level_wilayah_kabupaten']."',
						'".$row['id_level_wilayah_provinsi']."',
						'".$row['nama']."',
						'".$row['npsn']."',
						'".$row['bentuk_pendidikan_id']."',
						'".$row['status_sekolah']."',
						'".$row_semester['semester_id']."',
						'".$row_semester['tahun_ajaran_id']."',
						'".($row_kelas['pd_kelas_10_laki'] + $row_kelas['pd_kelas_10_perempuan'])."',
						'".$row_kelas['pd_kelas_10_laki']."',
						'".$row_kelas['pd_kelas_10_perempuan']."',
						'".($row_kelas['pd_kelas_11_laki'] + $row_kelas['pd_kelas_11_perempuan'])."',
						'".$row_kelas['pd_kelas_11_laki']."',
						'".$row_kelas['pd_kelas_11_perempuan']."',
						'".($row_kelas['pd_kelas_12_laki'] + $row_kelas['pd_kelas_12_perempuan'])."',
						'".$row_kelas['pd_kelas_12_laki']."',
						'".$row_kelas['pd_kelas_12_perempuan']."',
						'".($row_kelas['pd_kelas_13_laki'] + $row_kelas['pd_kelas_13_perempuan'])."',
						'".$row_kelas['pd_kelas_13_laki']."',
						'".$row_kelas['pd_kelas_13_perempuan']."',
						'".$row['jumlah_kirim']."'
					)";

		$stmtInsert = sqlsrv_query( $con_rekap, $insert);

		if($stmtInsert){
			echo "berhasil merekap ".$row['nama']."<br>";
		}else{
			echo 'gagal merekap '.$row['nama'].'<br>';
		}

		// sqlsrv_close ( resource $con_rekap );

	}


}


// sqlsrv_close ( resource $con_dapodikmen );

?>
