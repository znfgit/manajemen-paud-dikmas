<?php

define ('ROOT', dirname(__FILE__).'/../');
define ('D', DIRECTORY_SEPARATOR);
define ('P', PATH_SEPARATOR);
define ('SYSDIR', ROOT.D.'system'.D);
define ('APPNAME', 'simdik_batam');
define ('DBNAME', 'dapodikdasmen_batam');
define ('WORK_OFFLINE', false);
define ('TA_BERJALAN', 2014);
define ('SEMESTER_BERJALAN', 20142);
define ('KODE_WILAYAH', 000000);

define ('LEVEL', 'dikmen');
define ('SHOW_LEVEL', false);
define ('KABKOTA', '');
define ('LABEL', 'Manajemen Dapodikmen');
// define ('KABKOTA', '-');
// define ('LABEL', '-');
define ('URL', 'http://dapo.dikmen.kemdikbud.go.id');

define ('THEME', 'anegan-blue');
// define ('THEME', 'anegan-green');
define ('FORCE_LOGIN', true);
define ('INCLUDE_DEPAG', false);
define ('DBMS', 'mssql'); //  mysql, mssql, postgres
define ('SHOW_LOGIN_BUTTON', true);
define ('BERANDA_TYPE', 1);
?>