<?php
	require_once 'functions.php';
	require_once 'config.php';
	error_reporting(E_ERROR);

	// Prepare Class Environment
	$loader = require __DIR__.'/../vendor/autoload.php';
	$loader->add('simdik_batam', __DIR__.'/../src/');
	//usable functions
	// require_once '../src/functions.php';

	// Initialize the App
	$app = new Silex\Application();
	$app['debug'] = true;

	if(DBMS != 'mysql'){
		$dbms = '_'.DBMS;
	}else{
		$dbms = '';
	}

	//  Propel
	$app['propel.config_file'] = __DIR__.'/config'.$dbms.'/conf/'.DBNAME.'-conf.php';
	$app['propel.model_path'] = __DIR__.'/../src/';
	$app->register(new Propel\Silex\PropelServiceProvider());

	// Register Twig
	$app->register(new Silex\Provider\TwigServiceProvider(), array(
		'twig.path' => __DIR__.'/../web',
	));

	// Register Session
	$app->register(new Silex\Provider\SessionServiceProvider());

	// Register for auth too
	$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

	$app->register(new Silex\Provider\SecurityServiceProvider(), array(
		'admin' => array(
			'pattern' => '^/admin',
			'http' => true,
			'users' => array(
				// raw password is foo
				'admin' => array('ROLE_ADMIN', '5FZ2Z8QIkA7UTZ4BYkoC+GsReLf569mSKDsfods6LYQ8t+a8EW9oaircfMpmaLbPBh4FOBiiFyLfuZmTSUwzZg=='),
			),
		),
		'security.firewalls' => array(
			'foo' => array('pattern' => '^/foo'), // Example of an url available as anonymous user
			'default' => array(
				'pattern' => '^.*$',
				'anonymous' => true, // Needed as the login path is under the secured area
				'form' => array('login_path' => '/', 'check_path' => 'login_check'),
				'logout' => array('logout_path' => '/logout')
			),
		)
	));
	
	// Set TIMEZONE
	date_default_timezone_set('Asia/Jakarta');

	return $app;
