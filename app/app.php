<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\Debug\Debug;

require_once __DIR__.'/bootstrap.php';

/**
 * Enable Silex Error
 */
// $app->error(function (\Exception $e, $code) use ($app){
//     switch ($code) {
//         case 404:
//             $message = 'The requested page could not be found.';
        	
//             break;
//         default:
//             $message = 'We are sorry, but something went terribly wrong.';
            
//     }

//     //return new Response($message);
//     return $app['twig']->render('err.twig');
// });

$app->get('/', function() use ($app){
	$array = array();

	if($app['session']->get('username')){
		if(SHOW_LEVEL == true){
			$array['title'] = LABEL.' '.LEVEL.' '.KABKOTA;
		}else{
			$array['title'] = LABEL.' '.KABKOTA;
		}

		$array['theme'] = THEME;

	    return $app['twig']->render('index.html', $array);
	}else{
		return $app['twig']->render('login.html');
	}

	
});

$app->get('session', 'simdik_batam\Auth::session');
$app->post('login', 'simdik_batam\Auth::login');
$app->get('dologout', 'simdik_batam\Auth::logout');

$app->get('get/{model}', 'simdik_batam\System::get');
$app->post('post/{model}', 'simdik_batam\System::post');
$app->post('delete/{model}', 'simdik_batam\System::delete');

// import excel
$app->get('excel/{model}', 'simdik_batam\Excel::get');
$app->get('excel/{model}/{bentuk_pendidikan_id}', 'simdik_batam\Excel::get');
$app->get('excel/{model}/{bentuk_pendidikan_id}/{tahun_ajaran_id}', 'simdik_batam\Excel::get');
$app->get('excel/{model}/{bentuk_pendidikan_id}/{tahun_ajaran_id}/{semester_id}', 'simdik_batam\Excel::get');

$app->get('cetak/{model}/{id}/{tipe}', 'simdik_batam\Cetak::get');

//chart
$app->get('chart/{chart}/{model}', 'simdik_batam\Chart::get');
$app->get('chart/{chart}/{model}/cache', 'simdik_batam\Chart::getCache');

$app->get('testing', 'simdik_batam\System::testing');
$app->get('tesCross', 'simdik_batam\Auth::tesCross');
$app->get('getDeskripsi', 'simdik_batam\System::getDeskripsi');
$app->post('genKoreg', 'simdik_batam\System::genKoreg');

$app->get('rss', 'simdik_batam\System::getRss');

$app->get('tesparams', function() use ($app){
	$str = simdik_batam\System::getParamsBp(993);

	return $str;
});


$app->get('tes', function() use ($app){
	return 'oke konek';
});

$app->get('pdf/{nama}', 'simdik_batam\Pdf::get');

// Login
// $app->post('/login', 'simdik_batam\System::getLogin');

// Logout
// $app->get('/destauth', function(Request $request) use ($app) {
// 	$app['session']->clear();
// 	// return "Session destroy";
// 	return $app->redirect('/');
// });

return $app;
?>