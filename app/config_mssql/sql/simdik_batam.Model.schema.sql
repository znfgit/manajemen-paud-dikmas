
-----------------------------------------------------------------------
-- ref.status_anak
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.status_anak')
BEGIN
    DECLARE @reftable_1 nvarchar(60), @constraintname_1 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.status_anak'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_1, @constraintname_1
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_1+' drop constraint '+@constraintname_1)
        FETCH NEXT from refcursor into @reftable_1, @constraintname_1
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[status_anak]
END

CREATE TABLE [ref].[status_anak]
(
    [status_anak_id] NUMERIC(3,0) NOT NULL,
    [nama] VARCHAR(20) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [status_anak_PK] PRIMARY KEY ([status_anak_id])
);

CREATE INDEX [PK__status_a__8DA4E974F1A5A133] ON [ref].[status_anak] ([status_anak_id]);

-----------------------------------------------------------------------
-- buku_alat
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__buku_alat__prasa__0307610B')
    ALTER TABLE [buku_alat] DROP CONSTRAINT [FK__buku_alat__prasa__0307610B];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__buku_alat__prasa__67C95AEA')
    ALTER TABLE [buku_alat] DROP CONSTRAINT [FK__buku_alat__prasa__67C95AEA];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__buku_alat__sekol__011F1899')
    ALTER TABLE [buku_alat] DROP CONSTRAINT [FK__buku_alat__sekol__011F1899];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__buku_alat__sekol__65E11278')
    ALTER TABLE [buku_alat] DROP CONSTRAINT [FK__buku_alat__sekol__65E11278];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__buku_alat__jenis__63F8CA06')
    ALTER TABLE [buku_alat] DROP CONSTRAINT [FK__buku_alat__jenis__63F8CA06];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__buku_alat__jenis__7F36D027')
    ALTER TABLE [buku_alat] DROP CONSTRAINT [FK__buku_alat__jenis__7F36D027];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__buku_alat__mata___002AF460')
    ALTER TABLE [buku_alat] DROP CONSTRAINT [FK__buku_alat__mata___002AF460];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__buku_alat__mata___64ECEE3F')
    ALTER TABLE [buku_alat] DROP CONSTRAINT [FK__buku_alat__mata___64ECEE3F];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__buku_alat__tingk__02133CD2')
    ALTER TABLE [buku_alat] DROP CONSTRAINT [FK__buku_alat__tingk__02133CD2];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__buku_alat__tingk__66D536B1')
    ALTER TABLE [buku_alat] DROP CONSTRAINT [FK__buku_alat__tingk__66D536B1];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'buku_alat')
BEGIN
    DECLARE @reftable_2 nvarchar(60), @constraintname_2 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'buku_alat'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_2, @constraintname_2
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_2+' drop constraint '+@constraintname_2)
        FETCH NEXT from refcursor into @reftable_2, @constraintname_2
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [buku_alat]
END

CREATE TABLE [buku_alat]
(
    [buku_alat_id] CHAR(16) NOT NULL,
    [mata_pelajaran_id] INT NOT NULL,
    [prasarana_id] CHAR(16) NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [tingkat_pendidikan_id] NUMERIC(4,0) NULL,
    [jenis_buku_alat_id] NUMERIC(8,0) NOT NULL,
    [buku_alat] VARCHAR(60) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [buku_alat_PK] PRIMARY KEY ([buku_alat_id])
);

CREATE INDEX [BUKU_ALAT_JENIS_FK] ON [buku_alat] ([jenis_buku_alat_id]);

CREATE INDEX [BUKU_ALAT_LU] ON [buku_alat] ([Last_update]);

CREATE INDEX [BUKU_ALAT_MATA_PELAJARAN_FK] ON [buku_alat] ([mata_pelajaran_id]);

CREATE INDEX [BUKU_SEKOLAH_FK] ON [buku_alat] ([sekolah_id]);

CREATE INDEX [BUKU_TINGKAT_FK] ON [buku_alat] ([tingkat_pendidikan_id]);

CREATE INDEX [LETAK_BUKU_ALAT_FK] ON [buku_alat] ([prasarana_id]);

CREATE INDEX [PK__buku_ala__6D73766008DE7261] ON [buku_alat] ([buku_alat_id]);

BEGIN
ALTER TABLE [buku_alat] ADD CONSTRAINT [FK__buku_alat__prasa__0307610B] FOREIGN KEY ([prasarana_id]) REFERENCES [prasarana] ([prasarana_id])
END
;

BEGIN
ALTER TABLE [buku_alat] ADD CONSTRAINT [FK__buku_alat__prasa__67C95AEA] FOREIGN KEY ([prasarana_id]) REFERENCES [prasarana] ([prasarana_id])
END
;

BEGIN
ALTER TABLE [buku_alat] ADD CONSTRAINT [FK__buku_alat__sekol__011F1899] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [buku_alat] ADD CONSTRAINT [FK__buku_alat__sekol__65E11278] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [buku_alat] ADD CONSTRAINT [FK__buku_alat__jenis__63F8CA06] FOREIGN KEY ([jenis_buku_alat_id]) REFERENCES [ref].[jenis_buku_alat] ([jenis_buku_alat_id])
END
;

BEGIN
ALTER TABLE [buku_alat] ADD CONSTRAINT [FK__buku_alat__jenis__7F36D027] FOREIGN KEY ([jenis_buku_alat_id]) REFERENCES [ref].[jenis_buku_alat] ([jenis_buku_alat_id])
END
;

BEGIN
ALTER TABLE [buku_alat] ADD CONSTRAINT [FK__buku_alat__mata___002AF460] FOREIGN KEY ([mata_pelajaran_id]) REFERENCES [ref].[mata_pelajaran] ([mata_pelajaran_id])
END
;

BEGIN
ALTER TABLE [buku_alat] ADD CONSTRAINT [FK__buku_alat__mata___64ECEE3F] FOREIGN KEY ([mata_pelajaran_id]) REFERENCES [ref].[mata_pelajaran] ([mata_pelajaran_id])
END
;

BEGIN
ALTER TABLE [buku_alat] ADD CONSTRAINT [FK__buku_alat__tingk__02133CD2] FOREIGN KEY ([tingkat_pendidikan_id]) REFERENCES [ref].[tingkat_pendidikan] ([tingkat_pendidikan_id])
END
;

BEGIN
ALTER TABLE [buku_alat] ADD CONSTRAINT [FK__buku_alat__tingk__66D536B1] FOREIGN KEY ([tingkat_pendidikan_id]) REFERENCES [ref].[tingkat_pendidikan] ([tingkat_pendidikan_id])
END
;

-----------------------------------------------------------------------
-- ref.jenis_tunjangan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.jenis_tunjangan')
BEGIN
    DECLARE @reftable_3 nvarchar(60), @constraintname_3 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.jenis_tunjangan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_3, @constraintname_3
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_3+' drop constraint '+@constraintname_3)
        FETCH NEXT from refcursor into @reftable_3, @constraintname_3
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[jenis_tunjangan]
END

CREATE TABLE [ref].[jenis_tunjangan]
(
    [jenis_tunjangan_id] INT NOT NULL,
    [nama] VARCHAR(50) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [jenis_tunjangan_PK] PRIMARY KEY ([jenis_tunjangan_id])
);

CREATE INDEX [PK__jenis_tu__0B0A72E9F67EBBAD] ON [ref].[jenis_tunjangan] ([jenis_tunjangan_id]);

-----------------------------------------------------------------------
-- vld_rwy_kepangkatan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_rwy_k__riway__4356F04A')
    ALTER TABLE [vld_rwy_kepangkatan] DROP CONSTRAINT [FK__vld_rwy_k__riway__4356F04A];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_rwy_k__riway__5E94F66B')
    ALTER TABLE [vld_rwy_kepangkatan] DROP CONSTRAINT [FK__vld_rwy_k__riway__5E94F66B];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_rwy_k__idtyp__5F891AA4')
    ALTER TABLE [vld_rwy_kepangkatan] DROP CONSTRAINT [FK__vld_rwy_k__idtyp__5F891AA4];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_rwy_k__idtyp__444B1483')
    ALTER TABLE [vld_rwy_kepangkatan] DROP CONSTRAINT [FK__vld_rwy_k__idtyp__444B1483];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_rwy_kepangkatan')
BEGIN
    DECLARE @reftable_4 nvarchar(60), @constraintname_4 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_rwy_kepangkatan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_4, @constraintname_4
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_4+' drop constraint '+@constraintname_4)
        FETCH NEXT from refcursor into @reftable_4, @constraintname_4
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_rwy_kepangkatan]
END

CREATE TABLE [vld_rwy_kepangkatan]
(
    [riwayat_kepangkatan_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_rwy_kepangkatan_PK] PRIMARY KEY ([riwayat_kepangkatan_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_rwy_kepangkatan] ([idtype]);

CREATE INDEX [VLD_RWY_PANG_LU] ON [vld_rwy_kepangkatan] ([last_update]);

CREATE INDEX [PK__vld_rwy___5220C7CE34675D13] ON [vld_rwy_kepangkatan] ([riwayat_kepangkatan_id],[logid]);

BEGIN
ALTER TABLE [vld_rwy_kepangkatan] ADD CONSTRAINT [FK__vld_rwy_k__riway__4356F04A] FOREIGN KEY ([riwayat_kepangkatan_id]) REFERENCES [rwy_kepangkatan] ([riwayat_kepangkatan_id])
END
;

BEGIN
ALTER TABLE [vld_rwy_kepangkatan] ADD CONSTRAINT [FK__vld_rwy_k__riway__5E94F66B] FOREIGN KEY ([riwayat_kepangkatan_id]) REFERENCES [rwy_kepangkatan] ([riwayat_kepangkatan_id])
END
;

BEGIN
ALTER TABLE [vld_rwy_kepangkatan] ADD CONSTRAINT [FK__vld_rwy_k__idtyp__5F891AA4] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_rwy_kepangkatan] ADD CONSTRAINT [FK__vld_rwy_k__idtyp__444B1483] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- ref.jenis_penghargaan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.jenis_penghargaan')
BEGIN
    DECLARE @reftable_5 nvarchar(60), @constraintname_5 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.jenis_penghargaan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_5, @constraintname_5
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_5+' drop constraint '+@constraintname_5)
        FETCH NEXT from refcursor into @reftable_5, @constraintname_5
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[jenis_penghargaan]
END

CREATE TABLE [ref].[jenis_penghargaan]
(
    [jenis_penghargaan_id] INT NOT NULL,
    [nama] VARCHAR(50) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [jenis_penghargaan_PK] PRIMARY KEY ([jenis_penghargaan_id])
);

CREATE INDEX [PK__jenis_pe__785B4B193B224228] ON [ref].[jenis_penghargaan] ([jenis_penghargaan_id]);

-----------------------------------------------------------------------
-- jurusan_sp
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__jurusan_s__sekol__03FB8544')
    ALTER TABLE [jurusan_sp] DROP CONSTRAINT [FK__jurusan_s__sekol__03FB8544];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__jurusan_s__sekol__68BD7F23')
    ALTER TABLE [jurusan_sp] DROP CONSTRAINT [FK__jurusan_s__sekol__68BD7F23];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__jurusan_s__jurus__04EFA97D')
    ALTER TABLE [jurusan_sp] DROP CONSTRAINT [FK__jurusan_s__jurus__04EFA97D];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__jurusan_s__jurus__69B1A35C')
    ALTER TABLE [jurusan_sp] DROP CONSTRAINT [FK__jurusan_s__jurus__69B1A35C];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__jurusan_s__kebut__05E3CDB6')
    ALTER TABLE [jurusan_sp] DROP CONSTRAINT [FK__jurusan_s__kebut__05E3CDB6];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__jurusan_s__kebut__6AA5C795')
    ALTER TABLE [jurusan_sp] DROP CONSTRAINT [FK__jurusan_s__kebut__6AA5C795];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'jurusan_sp')
BEGIN
    DECLARE @reftable_6 nvarchar(60), @constraintname_6 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'jurusan_sp'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_6, @constraintname_6
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_6+' drop constraint '+@constraintname_6)
        FETCH NEXT from refcursor into @reftable_6, @constraintname_6
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [jurusan_sp]
END

CREATE TABLE [jurusan_sp]
(
    [jurusan_sp_id] CHAR(16) NOT NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [kebutuhan_khusus_id] INT NOT NULL,
    [jurusan_id] VARCHAR(25) NOT NULL,
    [nama_jurusan_sp] VARCHAR(60) NOT NULL,
    [sk_izin] VARCHAR(40) NULL,
    [tanggal_sk_izin] VARCHAR(20) NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [jurusan_sp_PK] PRIMARY KEY ([jurusan_sp_id])
);

CREATE INDEX [JUR_SEKOLAH_FK] ON [jurusan_sp] ([sekolah_id]);

CREATE INDEX [JURSEK_JUR_FK] ON [jurusan_sp] ([jurusan_id]);

CREATE INDEX [JURSP_KK_FK] ON [jurusan_sp] ([kebutuhan_khusus_id]);

CREATE INDEX [JURSP_LU] ON [jurusan_sp] ([Last_update]);

CREATE INDEX [PK__jurusan___48A0B48ECF63C78D] ON [jurusan_sp] ([jurusan_sp_id]);

BEGIN
ALTER TABLE [jurusan_sp] ADD CONSTRAINT [FK__jurusan_s__sekol__03FB8544] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [jurusan_sp] ADD CONSTRAINT [FK__jurusan_s__sekol__68BD7F23] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [jurusan_sp] ADD CONSTRAINT [FK__jurusan_s__jurus__04EFA97D] FOREIGN KEY ([jurusan_id]) REFERENCES [ref].[jurusan] ([jurusan_id])
END
;

BEGIN
ALTER TABLE [jurusan_sp] ADD CONSTRAINT [FK__jurusan_s__jurus__69B1A35C] FOREIGN KEY ([jurusan_id]) REFERENCES [ref].[jurusan] ([jurusan_id])
END
;

BEGIN
ALTER TABLE [jurusan_sp] ADD CONSTRAINT [FK__jurusan_s__kebut__05E3CDB6] FOREIGN KEY ([kebutuhan_khusus_id]) REFERENCES [ref].[kebutuhan_khusus] ([kebutuhan_khusus_id])
END
;

BEGIN
ALTER TABLE [jurusan_sp] ADD CONSTRAINT [FK__jurusan_s__kebut__6AA5C795] FOREIGN KEY ([kebutuhan_khusus_id]) REFERENCES [ref].[kebutuhan_khusus] ([kebutuhan_khusus_id])
END
;

-----------------------------------------------------------------------
-- ref.jenis_ptk
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.jenis_ptk')
BEGIN
    DECLARE @reftable_7 nvarchar(60), @constraintname_7 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.jenis_ptk'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_7, @constraintname_7
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_7+' drop constraint '+@constraintname_7)
        FETCH NEXT from refcursor into @reftable_7, @constraintname_7
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[jenis_ptk]
END

CREATE TABLE [ref].[jenis_ptk]
(
    [jenis_ptk_id] NUMERIC(4,0) NOT NULL,
    [jenis_ptk] VARCHAR(30) NOT NULL,
    [guru_kelas] NUMERIC(3,0) NOT NULL,
    [guru_matpel] NUMERIC(3,0) NOT NULL,
    [guru_bk] NUMERIC(3,0) NOT NULL,
    [guru_inklusi] NUMERIC(3,0) NOT NULL,
    [pengawas_satdik] NUMERIC(3,0) NOT NULL,
    [pengawas_plb] NUMERIC(3,0) NOT NULL,
    [pengawas_matpel] NUMERIC(3,0) NOT NULL,
    [pengawas_bidang] NUMERIC(3,0) NOT NULL,
    [tas] NUMERIC(3,0) NOT NULL,
    [formal] NUMERIC(3,0) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [jenis_ptk_PK] PRIMARY KEY ([jenis_ptk_id])
);

CREATE INDEX [PK__jenis_pt__8E737E22F880A1B9] ON [ref].[jenis_ptk] ([jenis_ptk_id]);

-----------------------------------------------------------------------
-- vld_rwy_fungsional
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_rwy_f__riway__453F38BC')
    ALTER TABLE [vld_rwy_fungsional] DROP CONSTRAINT [FK__vld_rwy_f__riway__453F38BC];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_rwy_f__riway__607D3EDD')
    ALTER TABLE [vld_rwy_fungsional] DROP CONSTRAINT [FK__vld_rwy_f__riway__607D3EDD];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_rwy_f__idtyp__61716316')
    ALTER TABLE [vld_rwy_fungsional] DROP CONSTRAINT [FK__vld_rwy_f__idtyp__61716316];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_rwy_f__idtyp__46335CF5')
    ALTER TABLE [vld_rwy_fungsional] DROP CONSTRAINT [FK__vld_rwy_f__idtyp__46335CF5];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_rwy_fungsional')
BEGIN
    DECLARE @reftable_8 nvarchar(60), @constraintname_8 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_rwy_fungsional'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_8, @constraintname_8
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_8+' drop constraint '+@constraintname_8)
        FETCH NEXT from refcursor into @reftable_8, @constraintname_8
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_rwy_fungsional]
END

CREATE TABLE [vld_rwy_fungsional]
(
    [riwayat_fungsional_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_rwy_fungsional_PK] PRIMARY KEY ([riwayat_fungsional_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_rwy_fungsional] ([idtype]);

CREATE INDEX [VLD_RWY_FUNG_LU] ON [vld_rwy_fungsional] ([last_update]);

CREATE INDEX [PK__vld_rwy___0C8B660689581408] ON [vld_rwy_fungsional] ([riwayat_fungsional_id],[logid]);

BEGIN
ALTER TABLE [vld_rwy_fungsional] ADD CONSTRAINT [FK__vld_rwy_f__riway__453F38BC] FOREIGN KEY ([riwayat_fungsional_id]) REFERENCES [rwy_fungsional] ([riwayat_fungsional_id])
END
;

BEGIN
ALTER TABLE [vld_rwy_fungsional] ADD CONSTRAINT [FK__vld_rwy_f__riway__607D3EDD] FOREIGN KEY ([riwayat_fungsional_id]) REFERENCES [rwy_fungsional] ([riwayat_fungsional_id])
END
;

BEGIN
ALTER TABLE [vld_rwy_fungsional] ADD CONSTRAINT [FK__vld_rwy_f__idtyp__61716316] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_rwy_fungsional] ADD CONSTRAINT [FK__vld_rwy_f__idtyp__46335CF5] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- kesejahteraan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__kesejahte__ptk_i__06D7F1EF')
    ALTER TABLE [kesejahteraan] DROP CONSTRAINT [FK__kesejahte__ptk_i__06D7F1EF];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__kesejahte__ptk_i__6B99EBCE')
    ALTER TABLE [kesejahteraan] DROP CONSTRAINT [FK__kesejahte__ptk_i__6B99EBCE];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__kesejahte__jenis__07CC1628')
    ALTER TABLE [kesejahteraan] DROP CONSTRAINT [FK__kesejahte__jenis__07CC1628];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__kesejahte__jenis__6C8E1007')
    ALTER TABLE [kesejahteraan] DROP CONSTRAINT [FK__kesejahte__jenis__6C8E1007];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'kesejahteraan')
BEGIN
    DECLARE @reftable_9 nvarchar(60), @constraintname_9 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'kesejahteraan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_9, @constraintname_9
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_9+' drop constraint '+@constraintname_9)
        FETCH NEXT from refcursor into @reftable_9, @constraintname_9
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [kesejahteraan]
END

CREATE TABLE [kesejahteraan]
(
    [kesejahteraan_id] CHAR(16) NOT NULL,
    [ptk_id] CHAR(16) NOT NULL,
    [jenis_kesejahteraan_id] INT NOT NULL,
    [nama] VARCHAR(25) NOT NULL,
    [penyelenggara] VARCHAR(80) NOT NULL,
    [dari_tahun] NUMERIC(6,0) NOT NULL,
    [sampai_tahun] NUMERIC(6,0) NULL,
    [status] INT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [kesejahteraan_PK] PRIMARY KEY ([kesejahteraan_id])
);

CREATE INDEX [KESEJAHTERAAN_JENIS_FK] ON [kesejahteraan] ([jenis_kesejahteraan_id]);

CREATE INDEX [KESEJAHTERAAN_LU] ON [kesejahteraan] ([Last_update]);

CREATE INDEX [KESEJAHTERAAN_PTK_FK] ON [kesejahteraan] ([ptk_id]);

CREATE INDEX [PK__kesejaht__CF366811586BA8C7] ON [kesejahteraan] ([kesejahteraan_id]);

BEGIN
ALTER TABLE [kesejahteraan] ADD CONSTRAINT [FK__kesejahte__ptk_i__06D7F1EF] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [kesejahteraan] ADD CONSTRAINT [FK__kesejahte__ptk_i__6B99EBCE] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [kesejahteraan] ADD CONSTRAINT [FK__kesejahte__jenis__07CC1628] FOREIGN KEY ([jenis_kesejahteraan_id]) REFERENCES [ref].[jenis_kesejahteraan] ([jenis_kesejahteraan_id])
END
;

BEGIN
ALTER TABLE [kesejahteraan] ADD CONSTRAINT [FK__kesejahte__jenis__6C8E1007] FOREIGN KEY ([jenis_kesejahteraan_id]) REFERENCES [ref].[jenis_kesejahteraan] ([jenis_kesejahteraan_id])
END
;

-----------------------------------------------------------------------
-- ref.lembaga_akreditasi
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.lembaga_akreditasi')
BEGIN
    DECLARE @reftable_10 nvarchar(60), @constraintname_10 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.lembaga_akreditasi'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_10, @constraintname_10
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_10+' drop constraint '+@constraintname_10)
        FETCH NEXT from refcursor into @reftable_10, @constraintname_10
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[lembaga_akreditasi]
END

CREATE TABLE [ref].[lembaga_akreditasi]
(
    [la_id] CHAR(5) NOT NULL,
    [nama] VARCHAR(80) NOT NULL,
    [la_tgl_mulai] VARCHAR(20) NOT NULL,
    [la_ket] VARCHAR(250) NULL,
    [alamat_jalan] VARCHAR(80) NOT NULL,
    [rt] NUMERIC(4,0) NULL,
    [rw] NUMERIC(4,0) NULL,
    [nama_dusun] VARCHAR(40) NULL,
    [desa_kelurahan] VARCHAR(40) NOT NULL,
    [kode_wilayah] CHAR(8) NOT NULL,
    [kode_pos] CHAR(5) NULL,
    [lintang] NUMERIC(12,6) NULL,
    [bujur] NUMERIC(12,6) NULL,
    [nomor_telepon] VARCHAR(20) NULL,
    [nomor_fax] VARCHAR(20) NULL,
    [email] VARCHAR(50) NULL,
    [website] VARCHAR(100) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [lembaga_akreditasi_PK] PRIMARY KEY ([la_id])
);

CREATE INDEX [LEMBAGA_KECAMATAN_FK] ON [ref].[lembaga_akreditasi] ([kode_wilayah]);

CREATE INDEX [PK__lembaga___48E3E3AE2D577426] ON [ref].[lembaga_akreditasi] ([la_id]);

-----------------------------------------------------------------------
-- ref.jenis_lembaga
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.jenis_lembaga')
BEGIN
    DECLARE @reftable_11 nvarchar(60), @constraintname_11 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.jenis_lembaga'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_11, @constraintname_11
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_11+' drop constraint '+@constraintname_11)
        FETCH NEXT from refcursor into @reftable_11, @constraintname_11
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[jenis_lembaga]
END

CREATE TABLE [ref].[jenis_lembaga]
(
    [jenis_lembaga_id] NUMERIC(7,0) NOT NULL,
    [nama] VARCHAR(80) NOT NULL,
    [tempat_pengawas] NUMERIC(3,0) NOT NULL,
    [simpul_pendataan] NUMERIC(3,0) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [jenis_lembaga_PK] PRIMARY KEY ([jenis_lembaga_id])
);

CREATE INDEX [PK__jenis_le__378B192EFD0C451D] ON [ref].[jenis_lembaga] ([jenis_lembaga_id]);

-----------------------------------------------------------------------
-- vld_rombel
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_rombe__rombo__481BA567')
    ALTER TABLE [vld_rombel] DROP CONSTRAINT [FK__vld_rombe__rombo__481BA567];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_rombe__rombo__6359AB88')
    ALTER TABLE [vld_rombel] DROP CONSTRAINT [FK__vld_rombe__rombo__6359AB88];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_rombe__idtyp__6265874F')
    ALTER TABLE [vld_rombel] DROP CONSTRAINT [FK__vld_rombe__idtyp__6265874F];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_rombe__idtyp__4727812E')
    ALTER TABLE [vld_rombel] DROP CONSTRAINT [FK__vld_rombe__idtyp__4727812E];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_rombel')
BEGIN
    DECLARE @reftable_12 nvarchar(60), @constraintname_12 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_rombel'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_12, @constraintname_12
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_12+' drop constraint '+@constraintname_12)
        FETCH NEXT from refcursor into @reftable_12, @constraintname_12
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_rombel]
END

CREATE TABLE [vld_rombel]
(
    [rombongan_belajar_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_rombel_PK] PRIMARY KEY ([rombongan_belajar_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_rombel] ([idtype]);

CREATE INDEX [VLD_ROMBEL_LU] ON [vld_rombel] ([last_update]);

CREATE INDEX [PK__vld_romb__14A250B37C6C61EB] ON [vld_rombel] ([rombongan_belajar_id],[logid]);

BEGIN
ALTER TABLE [vld_rombel] ADD CONSTRAINT [FK__vld_rombe__rombo__481BA567] FOREIGN KEY ([rombongan_belajar_id]) REFERENCES [rombongan_belajar] ([rombongan_belajar_id])
END
;

BEGIN
ALTER TABLE [vld_rombel] ADD CONSTRAINT [FK__vld_rombe__rombo__6359AB88] FOREIGN KEY ([rombongan_belajar_id]) REFERENCES [rombongan_belajar] ([rombongan_belajar_id])
END
;

BEGIN
ALTER TABLE [vld_rombel] ADD CONSTRAINT [FK__vld_rombe__idtyp__6265874F] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_rombel] ADD CONSTRAINT [FK__vld_rombe__idtyp__4727812E] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- sekolah_longitudinal
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sekolah_l__sekol__0AA882D3')
    ALTER TABLE [sekolah_longitudinal] DROP CONSTRAINT [FK__sekolah_l__sekol__0AA882D3];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sekolah_l__sekol__6F6A7CB2')
    ALTER TABLE [sekolah_longitudinal] DROP CONSTRAINT [FK__sekolah_l__sekol__6F6A7CB2];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sekolah_l__akses__0E7913B7')
    ALTER TABLE [sekolah_longitudinal] DROP CONSTRAINT [FK__sekolah_l__akses__0E7913B7];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sekolah_l__akses__0F6D37F0')
    ALTER TABLE [sekolah_longitudinal] DROP CONSTRAINT [FK__sekolah_l__akses__0F6D37F0];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sekolah_l__akses__733B0D96')
    ALTER TABLE [sekolah_longitudinal] DROP CONSTRAINT [FK__sekolah_l__akses__733B0D96];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sekolah_l__akses__742F31CF')
    ALTER TABLE [sekolah_longitudinal] DROP CONSTRAINT [FK__sekolah_l__akses__742F31CF];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sekolah_l__semes__0B9CA70C')
    ALTER TABLE [sekolah_longitudinal] DROP CONSTRAINT [FK__sekolah_l__semes__0B9CA70C];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sekolah_l__semes__705EA0EB')
    ALTER TABLE [sekolah_longitudinal] DROP CONSTRAINT [FK__sekolah_l__semes__705EA0EB];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sekolah_l__serti__09B45E9A')
    ALTER TABLE [sekolah_longitudinal] DROP CONSTRAINT [FK__sekolah_l__serti__09B45E9A];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sekolah_l__serti__6E765879')
    ALTER TABLE [sekolah_longitudinal] DROP CONSTRAINT [FK__sekolah_l__serti__6E765879];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sekolah_l__sumbe__0C90CB45')
    ALTER TABLE [sekolah_longitudinal] DROP CONSTRAINT [FK__sekolah_l__sumbe__0C90CB45];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sekolah_l__sumbe__7152C524')
    ALTER TABLE [sekolah_longitudinal] DROP CONSTRAINT [FK__sekolah_l__sumbe__7152C524];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sekolah_l__waktu__0D84EF7E')
    ALTER TABLE [sekolah_longitudinal] DROP CONSTRAINT [FK__sekolah_l__waktu__0D84EF7E];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sekolah_l__waktu__7246E95D')
    ALTER TABLE [sekolah_longitudinal] DROP CONSTRAINT [FK__sekolah_l__waktu__7246E95D];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'sekolah_longitudinal')
BEGIN
    DECLARE @reftable_13 nvarchar(60), @constraintname_13 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'sekolah_longitudinal'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_13, @constraintname_13
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_13+' drop constraint '+@constraintname_13)
        FETCH NEXT from refcursor into @reftable_13, @constraintname_13
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [sekolah_longitudinal]
END

CREATE TABLE [sekolah_longitudinal]
(
    [sekolah_id] CHAR(16) NOT NULL,
    [semester_id] CHAR(5) NOT NULL,
    [daya_listrik] NUMERIC(8,0) NOT NULL,
    [wilayah_terpencil] NUMERIC(3,0) NOT NULL,
    [wilayah_perbatasan] NUMERIC(3,0) NOT NULL,
    [wilayah_transmigrasi] NUMERIC(3,0) NOT NULL,
    [wilayah_adat_terpencil] NUMERIC(3,0) NOT NULL,
    [wilayah_bencana_alam] NUMERIC(3,0) NOT NULL,
    [wilayah_bencana_sosial] NUMERIC(3,0) NOT NULL,
    [partisipasi_bos] NUMERIC(3,0) DEFAULT ((1)) NOT NULL,
    [waktu_penyelenggaraan_id] NUMERIC(3,0) NOT NULL,
    [sumber_listrik_id] NUMERIC(4,0) NOT NULL,
    [sertifikasi_iso_id] SMALLINT(2,0) NOT NULL,
    [akses_internet_id] SMALLINT(2,0) NULL,
    [akses_internet_2_id] SMALLINT(2,0) NOT NULL,
    [blob_id] CHAR(16) NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [sekolah_longitudinal_PK] PRIMARY KEY ([sekolah_id],[semester_id])
);

CREATE INDEX [SEKOLAH_FOTO_FK] ON [sekolah_longitudinal] ([blob_id]);

CREATE INDEX [SEKOLAH_INTERNET_2_FK] ON [sekolah_longitudinal] ([akses_internet_id]);

CREATE INDEX [SEKOLAH_INTERNET_FK] ON [sekolah_longitudinal] ([akses_internet_2_id]);

CREATE INDEX [SEKOLAH_ISO_FK] ON [sekolah_longitudinal] ([sertifikasi_iso_id]);

CREATE INDEX [SEKOLAH_LISTRIK_FK] ON [sekolah_longitudinal] ([sumber_listrik_id]);

CREATE INDEX [SEKOLAH_LONG_LU] ON [sekolah_longitudinal] ([Last_update]);

CREATE INDEX [SEKOLAH_LONG_SEM_FK] ON [sekolah_longitudinal] ([semester_id]);

CREATE INDEX [SEKOLAH_WAKTU_FK] ON [sekolah_longitudinal] ([waktu_penyelenggaraan_id]);

CREATE INDEX [PK__sekolah___F53A286FA230B8A8] ON [sekolah_longitudinal] ([sekolah_id],[semester_id]);

BEGIN
ALTER TABLE [sekolah_longitudinal] ADD CONSTRAINT [FK__sekolah_l__sekol__0AA882D3] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [sekolah_longitudinal] ADD CONSTRAINT [FK__sekolah_l__sekol__6F6A7CB2] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [sekolah_longitudinal] ADD CONSTRAINT [FK__sekolah_l__akses__0E7913B7] FOREIGN KEY ([akses_internet_id]) REFERENCES [ref].[akses_internet] ([akses_internet_id])
END
;

BEGIN
ALTER TABLE [sekolah_longitudinal] ADD CONSTRAINT [FK__sekolah_l__akses__0F6D37F0] FOREIGN KEY ([akses_internet_2_id]) REFERENCES [ref].[akses_internet] ([akses_internet_id])
END
;

BEGIN
ALTER TABLE [sekolah_longitudinal] ADD CONSTRAINT [FK__sekolah_l__akses__733B0D96] FOREIGN KEY ([akses_internet_id]) REFERENCES [ref].[akses_internet] ([akses_internet_id])
END
;

BEGIN
ALTER TABLE [sekolah_longitudinal] ADD CONSTRAINT [FK__sekolah_l__akses__742F31CF] FOREIGN KEY ([akses_internet_2_id]) REFERENCES [ref].[akses_internet] ([akses_internet_id])
END
;

BEGIN
ALTER TABLE [sekolah_longitudinal] ADD CONSTRAINT [FK__sekolah_l__semes__0B9CA70C] FOREIGN KEY ([semester_id]) REFERENCES [ref].[semester] ([semester_id])
END
;

BEGIN
ALTER TABLE [sekolah_longitudinal] ADD CONSTRAINT [FK__sekolah_l__semes__705EA0EB] FOREIGN KEY ([semester_id]) REFERENCES [ref].[semester] ([semester_id])
END
;

BEGIN
ALTER TABLE [sekolah_longitudinal] ADD CONSTRAINT [FK__sekolah_l__serti__09B45E9A] FOREIGN KEY ([sertifikasi_iso_id]) REFERENCES [ref].[sertifikasi_iso] ([sertifikasi_iso_id])
END
;

BEGIN
ALTER TABLE [sekolah_longitudinal] ADD CONSTRAINT [FK__sekolah_l__serti__6E765879] FOREIGN KEY ([sertifikasi_iso_id]) REFERENCES [ref].[sertifikasi_iso] ([sertifikasi_iso_id])
END
;

BEGIN
ALTER TABLE [sekolah_longitudinal] ADD CONSTRAINT [FK__sekolah_l__sumbe__0C90CB45] FOREIGN KEY ([sumber_listrik_id]) REFERENCES [ref].[sumber_listrik] ([sumber_listrik_id])
END
;

BEGIN
ALTER TABLE [sekolah_longitudinal] ADD CONSTRAINT [FK__sekolah_l__sumbe__7152C524] FOREIGN KEY ([sumber_listrik_id]) REFERENCES [ref].[sumber_listrik] ([sumber_listrik_id])
END
;

BEGIN
ALTER TABLE [sekolah_longitudinal] ADD CONSTRAINT [FK__sekolah_l__waktu__0D84EF7E] FOREIGN KEY ([waktu_penyelenggaraan_id]) REFERENCES [ref].[waktu_penyelenggaraan] ([waktu_penyelenggaraan_id])
END
;

BEGIN
ALTER TABLE [sekolah_longitudinal] ADD CONSTRAINT [FK__sekolah_l__waktu__7246E95D] FOREIGN KEY ([waktu_penyelenggaraan_id]) REFERENCES [ref].[waktu_penyelenggaraan] ([waktu_penyelenggaraan_id])
END
;

-----------------------------------------------------------------------
-- ref.tetangga_kabkota
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__tetangga___kode___7EC1CEDB')
    ALTER TABLE [ref].[tetangga_kabkota] DROP CONSTRAINT [FK__tetangga___kode___7EC1CEDB];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__tetangga___mst_k__7FB5F314')
    ALTER TABLE [ref].[tetangga_kabkota] DROP CONSTRAINT [FK__tetangga___mst_k__7FB5F314];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.tetangga_kabkota')
BEGIN
    DECLARE @reftable_14 nvarchar(60), @constraintname_14 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.tetangga_kabkota'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_14, @constraintname_14
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_14+' drop constraint '+@constraintname_14)
        FETCH NEXT from refcursor into @reftable_14, @constraintname_14
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[tetangga_kabkota]
END

CREATE TABLE [ref].[tetangga_kabkota]
(
    [kode_wilayah] CHAR(8) NOT NULL,
    [mst_kode_wilayah] CHAR(8) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [tetangga_kabkota_PK] PRIMARY KEY ([kode_wilayah],[mst_kode_wilayah])
);

CREATE INDEX [KABKOTA_2_FK] ON [ref].[tetangga_kabkota] ([mst_kode_wilayah]);

CREATE INDEX [TETANGGA_LU] ON [ref].[tetangga_kabkota] ([last_update]);

CREATE INDEX [PK__tetangga__C485F3DE580FF5C6] ON [ref].[tetangga_kabkota] ([kode_wilayah],[mst_kode_wilayah]);

BEGIN
ALTER TABLE [ref].[tetangga_kabkota] ADD CONSTRAINT [FK__tetangga___kode___7EC1CEDB] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

BEGIN
ALTER TABLE [ref].[tetangga_kabkota] ADD CONSTRAINT [FK__tetangga___mst_k__7FB5F314] FOREIGN KEY ([mst_kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

-----------------------------------------------------------------------
-- ref.tingkat_pendidikan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__tingkat_p__jenja__7BE56230')
    ALTER TABLE [ref].[tingkat_pendidikan] DROP CONSTRAINT [FK__tingkat_p__jenja__7BE56230];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.tingkat_pendidikan')
BEGIN
    DECLARE @reftable_15 nvarchar(60), @constraintname_15 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.tingkat_pendidikan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_15, @constraintname_15
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_15+' drop constraint '+@constraintname_15)
        FETCH NEXT from refcursor into @reftable_15, @constraintname_15
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[tingkat_pendidikan]
END

CREATE TABLE [ref].[tingkat_pendidikan]
(
    [tingkat_pendidikan_id] NUMERIC(4,0) NOT NULL,
    [jenjang_pendidikan_id] NUMERIC(4,0) NOT NULL,
    [kode] VARCHAR(5) NOT NULL,
    [nama] VARCHAR(20) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [tingkat_pendidikan_PK] PRIMARY KEY ([tingkat_pendidikan_id])
);

CREATE INDEX [TINGKAT_JENJANG_FK] ON [ref].[tingkat_pendidikan] ([jenjang_pendidikan_id]);

CREATE INDEX [PK__tingkat___0BBA31228E7EA297] ON [ref].[tingkat_pendidikan] ([tingkat_pendidikan_id]);

BEGIN
ALTER TABLE [ref].[tingkat_pendidikan] ADD CONSTRAINT [FK__tingkat_p__jenja__7BE56230] FOREIGN KEY ([jenjang_pendidikan_id]) REFERENCES [ref].[jenjang_pendidikan] ([jenjang_pendidikan_id])
END
;

-----------------------------------------------------------------------
-- vld_ptk
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_ptk__ptk_id__6541F3FA')
    ALTER TABLE [vld_ptk] DROP CONSTRAINT [FK__vld_ptk__ptk_id__6541F3FA];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_ptk__ptk_id__4A03EDD9')
    ALTER TABLE [vld_ptk] DROP CONSTRAINT [FK__vld_ptk__ptk_id__4A03EDD9];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_ptk__idtype__644DCFC1')
    ALTER TABLE [vld_ptk] DROP CONSTRAINT [FK__vld_ptk__idtype__644DCFC1];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_ptk__idtype__490FC9A0')
    ALTER TABLE [vld_ptk] DROP CONSTRAINT [FK__vld_ptk__idtype__490FC9A0];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_ptk')
BEGIN
    DECLARE @reftable_16 nvarchar(60), @constraintname_16 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_ptk'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_16, @constraintname_16
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_16+' drop constraint '+@constraintname_16)
        FETCH NEXT from refcursor into @reftable_16, @constraintname_16
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_ptk]
END

CREATE TABLE [vld_ptk]
(
    [ptk_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_ptk_PK] PRIMARY KEY ([ptk_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_ptk] ([idtype]);

CREATE INDEX [VLD_PTK_LU] ON [vld_ptk] ([last_update]);

CREATE INDEX [PK__vld_ptk__A48B08C0186921E3] ON [vld_ptk] ([ptk_id],[logid]);

BEGIN
ALTER TABLE [vld_ptk] ADD CONSTRAINT [FK__vld_ptk__ptk_id__6541F3FA] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [vld_ptk] ADD CONSTRAINT [FK__vld_ptk__ptk_id__4A03EDD9] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [vld_ptk] ADD CONSTRAINT [FK__vld_ptk__idtype__644DCFC1] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_ptk] ADD CONSTRAINT [FK__vld_ptk__idtype__490FC9A0] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- ref.kebutuhan_khusus
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.kebutuhan_khusus')
BEGIN
    DECLARE @reftable_17 nvarchar(60), @constraintname_17 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.kebutuhan_khusus'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_17, @constraintname_17
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_17+' drop constraint '+@constraintname_17)
        FETCH NEXT from refcursor into @reftable_17, @constraintname_17
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[kebutuhan_khusus]
END

CREATE TABLE [ref].[kebutuhan_khusus]
(
    [kebutuhan_khusus_id] INT NOT NULL,
    [kebutuhan_khusus] VARCHAR(40) NOT NULL,
    [kk_a] NUMERIC(3,0) NOT NULL,
    [kk_b] NUMERIC(3,0) NOT NULL,
    [kk_c] NUMERIC(3,0) NOT NULL,
    [kk_c1] NUMERIC(3,0) NOT NULL,
    [kk_d] NUMERIC(3,0) NOT NULL,
    [kk_d1] NUMERIC(3,0) NOT NULL,
    [kk_e] NUMERIC(3,0) NOT NULL,
    [kk_f] NUMERIC(3,0) NOT NULL,
    [kk_h] NUMERIC(3,0) NOT NULL,
    [kk_i] NUMERIC(3,0) NOT NULL,
    [kk_j] NUMERIC(3,0) NOT NULL,
    [kk_k] NUMERIC(3,0) NOT NULL,
    [kk_n] NUMERIC(3,0) NOT NULL,
    [kk_o] NUMERIC(3,0) NOT NULL,
    [kk_p] NUMERIC(3,0) NOT NULL,
    [kk_q] NUMERIC(3,0) NOT NULL,
    [untuk_lembaga] NUMERIC(3,0) DEFAULT ((1)) NOT NULL,
    [untuk_ptk] NUMERIC(3,0) DEFAULT ((1)) NOT NULL,
    [untuk_pd] NUMERIC(3,0) DEFAULT ((1)) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [kebutuhan_khusus_PK] PRIMARY KEY ([kebutuhan_khusus_id])
);

CREATE INDEX [PK__kebutuha__5E7AA4CEB1C54FE0] ON [ref].[kebutuhan_khusus] ([kebutuhan_khusus_id]);

-----------------------------------------------------------------------
-- prestasi
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__prestasi__pesert__1249A49B')
    ALTER TABLE [prestasi] DROP CONSTRAINT [FK__prestasi__pesert__1249A49B];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__prestasi__pesert__770B9E7A')
    ALTER TABLE [prestasi] DROP CONSTRAINT [FK__prestasi__pesert__770B9E7A];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__prestasi__jenis___10615C29')
    ALTER TABLE [prestasi] DROP CONSTRAINT [FK__prestasi__jenis___10615C29];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__prestasi__jenis___75235608')
    ALTER TABLE [prestasi] DROP CONSTRAINT [FK__prestasi__jenis___75235608];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__prestasi__tingka__11558062')
    ALTER TABLE [prestasi] DROP CONSTRAINT [FK__prestasi__tingka__11558062];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__prestasi__tingka__76177A41')
    ALTER TABLE [prestasi] DROP CONSTRAINT [FK__prestasi__tingka__76177A41];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'prestasi')
BEGIN
    DECLARE @reftable_18 nvarchar(60), @constraintname_18 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'prestasi'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_18, @constraintname_18
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_18+' drop constraint '+@constraintname_18)
        FETCH NEXT from refcursor into @reftable_18, @constraintname_18
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [prestasi]
END

CREATE TABLE [prestasi]
(
    [prestasi_id] CHAR(16) NOT NULL,
    [jenis_prestasi_id] INT NOT NULL,
    [tingkat_prestasi_id] INT NOT NULL,
    [peserta_didik_id] CHAR(16) NOT NULL,
    [nama] VARCHAR(30) NOT NULL,
    [tahun_prestasi] NUMERIC(6,0) NOT NULL,
    [penyelenggara] VARCHAR(80) NULL,
    [peringkat] INT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [prestasi_PK] PRIMARY KEY ([prestasi_id])
);

CREATE INDEX [PRESTASI_JENIS_FK] ON [prestasi] ([jenis_prestasi_id]);

CREATE INDEX [PRESTASI_LU] ON [prestasi] ([Last_update]);

CREATE INDEX [PRESTASI_PESERTA_DIDIK_FK] ON [prestasi] ([peserta_didik_id]);

CREATE INDEX [PRESTASI_TINGKAT_FK] ON [prestasi] ([tingkat_prestasi_id]);

CREATE INDEX [PK__prestasi__C092B292FD971D77] ON [prestasi] ([prestasi_id]);

BEGIN
ALTER TABLE [prestasi] ADD CONSTRAINT [FK__prestasi__pesert__1249A49B] FOREIGN KEY ([peserta_didik_id]) REFERENCES [peserta_didik] ([peserta_didik_id])
END
;

BEGIN
ALTER TABLE [prestasi] ADD CONSTRAINT [FK__prestasi__pesert__770B9E7A] FOREIGN KEY ([peserta_didik_id]) REFERENCES [peserta_didik] ([peserta_didik_id])
END
;

BEGIN
ALTER TABLE [prestasi] ADD CONSTRAINT [FK__prestasi__jenis___10615C29] FOREIGN KEY ([jenis_prestasi_id]) REFERENCES [ref].[jenis_prestasi] ([jenis_prestasi_id])
END
;

BEGIN
ALTER TABLE [prestasi] ADD CONSTRAINT [FK__prestasi__jenis___75235608] FOREIGN KEY ([jenis_prestasi_id]) REFERENCES [ref].[jenis_prestasi] ([jenis_prestasi_id])
END
;

BEGIN
ALTER TABLE [prestasi] ADD CONSTRAINT [FK__prestasi__tingka__11558062] FOREIGN KEY ([tingkat_prestasi_id]) REFERENCES [ref].[tingkat_prestasi] ([tingkat_prestasi_id])
END
;

BEGIN
ALTER TABLE [prestasi] ADD CONSTRAINT [FK__prestasi__tingka__76177A41] FOREIGN KEY ([tingkat_prestasi_id]) REFERENCES [ref].[tingkat_prestasi] ([tingkat_prestasi_id])
END
;

-----------------------------------------------------------------------
-- ref.template_un
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__template___jenja__00AA174D')
    ALTER TABLE [ref].[template_un] DROP CONSTRAINT [FK__template___jenja__00AA174D];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__template___jurus__019E3B86')
    ALTER TABLE [ref].[template_un] DROP CONSTRAINT [FK__template___jurus__019E3B86];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__template___mp3_i__02925FBF')
    ALTER TABLE [ref].[template_un] DROP CONSTRAINT [FK__template___mp3_i__02925FBF];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__template___mp4_i__038683F8')
    ALTER TABLE [ref].[template_un] DROP CONSTRAINT [FK__template___mp4_i__038683F8];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__template___mp7_i__047AA831')
    ALTER TABLE [ref].[template_un] DROP CONSTRAINT [FK__template___mp7_i__047AA831];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__template___mp5_i__056ECC6A')
    ALTER TABLE [ref].[template_un] DROP CONSTRAINT [FK__template___mp5_i__056ECC6A];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__template___mp1_i__0662F0A3')
    ALTER TABLE [ref].[template_un] DROP CONSTRAINT [FK__template___mp1_i__0662F0A3];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__template___mp2_i__075714DC')
    ALTER TABLE [ref].[template_un] DROP CONSTRAINT [FK__template___mp2_i__075714DC];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__template___mp6_i__084B3915')
    ALTER TABLE [ref].[template_un] DROP CONSTRAINT [FK__template___mp6_i__084B3915];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__template___tahun__093F5D4E')
    ALTER TABLE [ref].[template_un] DROP CONSTRAINT [FK__template___tahun__093F5D4E];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.template_un')
BEGIN
    DECLARE @reftable_19 nvarchar(60), @constraintname_19 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.template_un'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_19, @constraintname_19
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_19+' drop constraint '+@constraintname_19)
        FETCH NEXT from refcursor into @reftable_19, @constraintname_19
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[template_un]
END

CREATE TABLE [ref].[template_un]
(
    [template_id] CHAR(16) NOT NULL,
    [jenjang_pendidikan_id] NUMERIC(4,0) NOT NULL,
    [tahun_ajaran_id] NUMERIC(6,0) NOT NULL,
    [jurusan_id] VARCHAR(25) NULL,
    [template_ket] VARCHAR(250) NULL,
    [mp1_id] INT NULL,
    [mp2_id] INT NULL,
    [mp3_id] INT NULL,
    [mp4_id] INT NULL,
    [mp5_id] INT NULL,
    [mp6_id] INT NULL,
    [mp7_id] INT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [template_un_PK] PRIMARY KEY ([template_id])
);

CREATE INDEX [TUN_JENJANG_FK] ON [ref].[template_un] ([jenjang_pendidikan_id]);

CREATE INDEX [TUN_JURUSAN_FK] ON [ref].[template_un] ([jurusan_id]);

CREATE INDEX [TUN_MTP1_FK] ON [ref].[template_un] ([mp3_id]);

CREATE INDEX [TUN_MTP2_FK] ON [ref].[template_un] ([mp4_id]);

CREATE INDEX [TUN_MTP3_FK] ON [ref].[template_un] ([mp7_id]);

CREATE INDEX [TUN_MTP4_FK] ON [ref].[template_un] ([mp5_id]);

CREATE INDEX [TUN_MTP5_FK] ON [ref].[template_un] ([mp1_id]);

CREATE INDEX [TUN_MTP6_FK] ON [ref].[template_un] ([mp2_id]);

CREATE INDEX [TUN_MTP7_FK] ON [ref].[template_un] ([mp6_id]);

CREATE INDEX [TUN_TAHUN_FK] ON [ref].[template_un] ([tahun_ajaran_id]);

CREATE INDEX [PK__template__BE44E0781C5610DC] ON [ref].[template_un] ([template_id]);

BEGIN
ALTER TABLE [ref].[template_un] ADD CONSTRAINT [FK__template___jenja__00AA174D] FOREIGN KEY ([jenjang_pendidikan_id]) REFERENCES [ref].[jenjang_pendidikan] ([jenjang_pendidikan_id])
END
;

BEGIN
ALTER TABLE [ref].[template_un] ADD CONSTRAINT [FK__template___jurus__019E3B86] FOREIGN KEY ([jurusan_id]) REFERENCES [ref].[jurusan] ([jurusan_id])
END
;

BEGIN
ALTER TABLE [ref].[template_un] ADD CONSTRAINT [FK__template___mp3_i__02925FBF] FOREIGN KEY ([mp3_id]) REFERENCES [ref].[mata_pelajaran] ([mata_pelajaran_id])
END
;

BEGIN
ALTER TABLE [ref].[template_un] ADD CONSTRAINT [FK__template___mp4_i__038683F8] FOREIGN KEY ([mp4_id]) REFERENCES [ref].[mata_pelajaran] ([mata_pelajaran_id])
END
;

BEGIN
ALTER TABLE [ref].[template_un] ADD CONSTRAINT [FK__template___mp7_i__047AA831] FOREIGN KEY ([mp7_id]) REFERENCES [ref].[mata_pelajaran] ([mata_pelajaran_id])
END
;

BEGIN
ALTER TABLE [ref].[template_un] ADD CONSTRAINT [FK__template___mp5_i__056ECC6A] FOREIGN KEY ([mp5_id]) REFERENCES [ref].[mata_pelajaran] ([mata_pelajaran_id])
END
;

BEGIN
ALTER TABLE [ref].[template_un] ADD CONSTRAINT [FK__template___mp1_i__0662F0A3] FOREIGN KEY ([mp1_id]) REFERENCES [ref].[mata_pelajaran] ([mata_pelajaran_id])
END
;

BEGIN
ALTER TABLE [ref].[template_un] ADD CONSTRAINT [FK__template___mp2_i__075714DC] FOREIGN KEY ([mp2_id]) REFERENCES [ref].[mata_pelajaran] ([mata_pelajaran_id])
END
;

BEGIN
ALTER TABLE [ref].[template_un] ADD CONSTRAINT [FK__template___mp6_i__084B3915] FOREIGN KEY ([mp6_id]) REFERENCES [ref].[mata_pelajaran] ([mata_pelajaran_id])
END
;

BEGIN
ALTER TABLE [ref].[template_un] ADD CONSTRAINT [FK__template___tahun__093F5D4E] FOREIGN KEY ([tahun_ajaran_id]) REFERENCES [ref].[tahun_ajaran] ([tahun_ajaran_id])
END
;

-----------------------------------------------------------------------
-- anggota_rombel
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__anggota_r__peser__192BAC54')
    ALTER TABLE [anggota_rombel] DROP CONSTRAINT [FK__anggota_r__peser__192BAC54];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__anggota_r__peser__7DEDA633')
    ALTER TABLE [anggota_rombel] DROP CONSTRAINT [FK__anggota_r__peser__7DEDA633];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__anggota_r__rombo__1A1FD08D')
    ALTER TABLE [anggota_rombel] DROP CONSTRAINT [FK__anggota_r__rombo__1A1FD08D];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__anggota_r__rombo__7EE1CA6C')
    ALTER TABLE [anggota_rombel] DROP CONSTRAINT [FK__anggota_r__rombo__7EE1CA6C];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__anggota_r__jenis__1B13F4C6')
    ALTER TABLE [anggota_rombel] DROP CONSTRAINT [FK__anggota_r__jenis__1B13F4C6];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__anggota_r__jenis__7FD5EEA5')
    ALTER TABLE [anggota_rombel] DROP CONSTRAINT [FK__anggota_r__jenis__7FD5EEA5];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'anggota_rombel')
BEGIN
    DECLARE @reftable_20 nvarchar(60), @constraintname_20 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'anggota_rombel'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_20, @constraintname_20
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_20+' drop constraint '+@constraintname_20)
        FETCH NEXT from refcursor into @reftable_20, @constraintname_20
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [anggota_rombel]
END

CREATE TABLE [anggota_rombel]
(
    [anggota_rombel_id] CHAR(16) NOT NULL,
    [rombongan_belajar_id] CHAR(16) NOT NULL,
    [peserta_didik_id] CHAR(16) NOT NULL,
    [jenis_pendaftaran_id] NUMERIC(3,0) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [anggota_rombel_PK] PRIMARY KEY ([anggota_rombel_id])
);

CREATE INDEX [ANGG_ROMBEL_JENIS_DAFTAR_FK] ON [anggota_rombel] ([jenis_pendaftaran_id]);

CREATE INDEX [ANGGOTA_ROMBEL_DEL] ON [anggota_rombel] ([rombongan_belajar_id],[Soft_delete]);

CREATE INDEX [ANGGOTA_ROMBEL_LU] ON [anggota_rombel] ([Last_update]);

CREATE INDEX [PK__anggota___C37454997616C47D] ON [anggota_rombel] ([anggota_rombel_id]);

CREATE INDEX [ROMBEL_PD_UNIQUE] ON [anggota_rombel] ([peserta_didik_id],[rombongan_belajar_id]);

BEGIN
ALTER TABLE [anggota_rombel] ADD CONSTRAINT [FK__anggota_r__peser__192BAC54] FOREIGN KEY ([peserta_didik_id]) REFERENCES [peserta_didik] ([peserta_didik_id])
END
;

BEGIN
ALTER TABLE [anggota_rombel] ADD CONSTRAINT [FK__anggota_r__peser__7DEDA633] FOREIGN KEY ([peserta_didik_id]) REFERENCES [peserta_didik] ([peserta_didik_id])
END
;

BEGIN
ALTER TABLE [anggota_rombel] ADD CONSTRAINT [FK__anggota_r__rombo__1A1FD08D] FOREIGN KEY ([rombongan_belajar_id]) REFERENCES [rombongan_belajar] ([rombongan_belajar_id])
END
;

BEGIN
ALTER TABLE [anggota_rombel] ADD CONSTRAINT [FK__anggota_r__rombo__7EE1CA6C] FOREIGN KEY ([rombongan_belajar_id]) REFERENCES [rombongan_belajar] ([rombongan_belajar_id])
END
;

BEGIN
ALTER TABLE [anggota_rombel] ADD CONSTRAINT [FK__anggota_r__jenis__1B13F4C6] FOREIGN KEY ([jenis_pendaftaran_id]) REFERENCES [ref].[jenis_pendaftaran] ([jenis_pendaftaran_id])
END
;

BEGIN
ALTER TABLE [anggota_rombel] ADD CONSTRAINT [FK__anggota_r__jenis__7FD5EEA5] FOREIGN KEY ([jenis_pendaftaran_id]) REFERENCES [ref].[jenis_pendaftaran] ([jenis_pendaftaran_id])
END
;

-----------------------------------------------------------------------
-- penghargaan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengharga__ptk_i__133DC8D4')
    ALTER TABLE [penghargaan] DROP CONSTRAINT [FK__pengharga__ptk_i__133DC8D4];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengharga__ptk_i__77FFC2B3')
    ALTER TABLE [penghargaan] DROP CONSTRAINT [FK__pengharga__ptk_i__77FFC2B3];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengharga__jenis__1431ED0D')
    ALTER TABLE [penghargaan] DROP CONSTRAINT [FK__pengharga__jenis__1431ED0D];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengharga__jenis__78F3E6EC')
    ALTER TABLE [penghargaan] DROP CONSTRAINT [FK__pengharga__jenis__78F3E6EC];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengharga__tingk__15261146')
    ALTER TABLE [penghargaan] DROP CONSTRAINT [FK__pengharga__tingk__15261146];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengharga__tingk__79E80B25')
    ALTER TABLE [penghargaan] DROP CONSTRAINT [FK__pengharga__tingk__79E80B25];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'penghargaan')
BEGIN
    DECLARE @reftable_21 nvarchar(60), @constraintname_21 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'penghargaan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_21, @constraintname_21
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_21+' drop constraint '+@constraintname_21)
        FETCH NEXT from refcursor into @reftable_21, @constraintname_21
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [penghargaan]
END

CREATE TABLE [penghargaan]
(
    [penghargaan_id] CHAR(16) NOT NULL,
    [tingkat_penghargaan_id] INT NOT NULL,
    [ptk_id] CHAR(16) NOT NULL,
    [jenis_penghargaan_id] INT NOT NULL,
    [nama] VARCHAR(50) NOT NULL,
    [tahun] NUMERIC(6,0) NOT NULL,
    [instansi] VARCHAR(80) NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [penghargaan_PK] PRIMARY KEY ([penghargaan_id])
);

CREATE INDEX [PENGHARGAAN_JENIS_FK] ON [penghargaan] ([jenis_penghargaan_id]);

CREATE INDEX [PENGHARGAAN_LU] ON [penghargaan] ([Last_update]);

CREATE INDEX [PENGHARGAAN_PTK_FK] ON [penghargaan] ([ptk_id]);

CREATE INDEX [PENGHARGAAN_TINGKAT_FK] ON [penghargaan] ([tingkat_penghargaan_id]);

CREATE INDEX [PK__pengharg__FF319750F49A7748] ON [penghargaan] ([penghargaan_id]);

BEGIN
ALTER TABLE [penghargaan] ADD CONSTRAINT [FK__pengharga__ptk_i__133DC8D4] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [penghargaan] ADD CONSTRAINT [FK__pengharga__ptk_i__77FFC2B3] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [penghargaan] ADD CONSTRAINT [FK__pengharga__jenis__1431ED0D] FOREIGN KEY ([jenis_penghargaan_id]) REFERENCES [ref].[jenis_penghargaan] ([jenis_penghargaan_id])
END
;

BEGIN
ALTER TABLE [penghargaan] ADD CONSTRAINT [FK__pengharga__jenis__78F3E6EC] FOREIGN KEY ([jenis_penghargaan_id]) REFERENCES [ref].[jenis_penghargaan] ([jenis_penghargaan_id])
END
;

BEGIN
ALTER TABLE [penghargaan] ADD CONSTRAINT [FK__pengharga__tingk__15261146] FOREIGN KEY ([tingkat_penghargaan_id]) REFERENCES [ref].[tingkat_penghargaan] ([tingkat_penghargaan_id])
END
;

BEGIN
ALTER TABLE [penghargaan] ADD CONSTRAINT [FK__pengharga__tingk__79E80B25] FOREIGN KEY ([tingkat_penghargaan_id]) REFERENCES [ref].[tingkat_penghargaan] ([tingkat_penghargaan_id])
END
;

-----------------------------------------------------------------------
-- ref.jenis_diklat
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.jenis_diklat')
BEGIN
    DECLARE @reftable_22 nvarchar(60), @constraintname_22 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.jenis_diklat'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_22, @constraintname_22
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_22+' drop constraint '+@constraintname_22)
        FETCH NEXT from refcursor into @reftable_22, @constraintname_22
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[jenis_diklat]
END

CREATE TABLE [ref].[jenis_diklat]
(
    [jenis_diklat_id] INT NOT NULL,
    [nama] VARCHAR(50) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [jenis_diklat_PK] PRIMARY KEY ([jenis_diklat_id])
);

CREATE INDEX [PK__jenis_di__316E4A7F69DF0317] ON [ref].[jenis_diklat] ([jenis_diklat_id]);

-----------------------------------------------------------------------
-- vld_prestasi
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_prest__prest__4BEC364B')
    ALTER TABLE [vld_prestasi] DROP CONSTRAINT [FK__vld_prest__prest__4BEC364B];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_prest__prest__672A3C6C')
    ALTER TABLE [vld_prestasi] DROP CONSTRAINT [FK__vld_prest__prest__672A3C6C];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_prest__idtyp__66361833')
    ALTER TABLE [vld_prestasi] DROP CONSTRAINT [FK__vld_prest__idtyp__66361833];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_prest__idtyp__4AF81212')
    ALTER TABLE [vld_prestasi] DROP CONSTRAINT [FK__vld_prest__idtyp__4AF81212];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_prestasi')
BEGIN
    DECLARE @reftable_23 nvarchar(60), @constraintname_23 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_prestasi'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_23, @constraintname_23
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_23+' drop constraint '+@constraintname_23)
        FETCH NEXT from refcursor into @reftable_23, @constraintname_23
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_prestasi]
END

CREATE TABLE [vld_prestasi]
(
    [prestasi_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_prestasi_PK] PRIMARY KEY ([prestasi_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_prestasi] ([idtype]);

CREATE INDEX [VLD_PRESTASI_LU] ON [vld_prestasi] ([last_update]);

CREATE INDEX [PK__vld_pres__87113DB523C4AA1B] ON [vld_prestasi] ([prestasi_id],[logid]);

BEGIN
ALTER TABLE [vld_prestasi] ADD CONSTRAINT [FK__vld_prest__prest__4BEC364B] FOREIGN KEY ([prestasi_id]) REFERENCES [prestasi] ([prestasi_id])
END
;

BEGIN
ALTER TABLE [vld_prestasi] ADD CONSTRAINT [FK__vld_prest__prest__672A3C6C] FOREIGN KEY ([prestasi_id]) REFERENCES [prestasi] ([prestasi_id])
END
;

BEGIN
ALTER TABLE [vld_prestasi] ADD CONSTRAINT [FK__vld_prest__idtyp__66361833] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_prestasi] ADD CONSTRAINT [FK__vld_prest__idtyp__4AF81212] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- peserta_didik_longitudinal
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__peser__00CA12DE')
    ALTER TABLE [peserta_didik_longitudinal] DROP CONSTRAINT [FK__peserta_d__peser__00CA12DE];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__peser__1C0818FF')
    ALTER TABLE [peserta_didik_longitudinal] DROP CONSTRAINT [FK__peserta_d__peser__1C0818FF];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__semes__01BE3717')
    ALTER TABLE [peserta_didik_longitudinal] DROP CONSTRAINT [FK__peserta_d__semes__01BE3717];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__semes__1CFC3D38')
    ALTER TABLE [peserta_didik_longitudinal] DROP CONSTRAINT [FK__peserta_d__semes__1CFC3D38];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'peserta_didik_longitudinal')
BEGIN
    DECLARE @reftable_24 nvarchar(60), @constraintname_24 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'peserta_didik_longitudinal'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_24, @constraintname_24
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_24+' drop constraint '+@constraintname_24)
        FETCH NEXT from refcursor into @reftable_24, @constraintname_24
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [peserta_didik_longitudinal]
END

CREATE TABLE [peserta_didik_longitudinal]
(
    [peserta_didik_id] CHAR(16) NOT NULL,
    [semester_id] CHAR(5) NOT NULL,
    [tinggi_badan] NUMERIC(5,0) NOT NULL,
    [berat_badan] NUMERIC(5,0) NOT NULL,
    [jarak_rumah_ke_sekolah] NUMERIC(3,0) NOT NULL,
    [jarak_rumah_ke_sekolah_km] NUMERIC(5,0) NULL,
    [waktu_tempuh_ke_sekolah] NUMERIC(3,0) NOT NULL,
    [menit_tempuh_ke_sekolah] NUMERIC(5,0) NULL,
    [jumlah_saudara_kandung] NUMERIC(4,0) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [peserta_didik_longitudinal_PK] PRIMARY KEY ([peserta_didik_id],[semester_id])
);

CREATE INDEX [PD_LONG_LU] ON [peserta_didik_longitudinal] ([Last_update]);

CREATE INDEX [PD_LONG_SEM_FK] ON [peserta_didik_longitudinal] ([semester_id]);

CREATE INDEX [PK__peserta___FFA9CA7D53CDA4EE] ON [peserta_didik_longitudinal] ([peserta_didik_id],[semester_id]);

BEGIN
ALTER TABLE [peserta_didik_longitudinal] ADD CONSTRAINT [FK__peserta_d__peser__00CA12DE] FOREIGN KEY ([peserta_didik_id]) REFERENCES [peserta_didik] ([peserta_didik_id])
END
;

BEGIN
ALTER TABLE [peserta_didik_longitudinal] ADD CONSTRAINT [FK__peserta_d__peser__1C0818FF] FOREIGN KEY ([peserta_didik_id]) REFERENCES [peserta_didik] ([peserta_didik_id])
END
;

BEGIN
ALTER TABLE [peserta_didik_longitudinal] ADD CONSTRAINT [FK__peserta_d__semes__01BE3717] FOREIGN KEY ([semester_id]) REFERENCES [ref].[semester] ([semester_id])
END
;

BEGIN
ALTER TABLE [peserta_didik_longitudinal] ADD CONSTRAINT [FK__peserta_d__semes__1CFC3D38] FOREIGN KEY ([semester_id]) REFERENCES [ref].[semester] ([semester_id])
END
;

-----------------------------------------------------------------------
-- ref.template_rapor
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__template___mata___0A338187')
    ALTER TABLE [ref].[template_rapor] DROP CONSTRAINT [FK__template___mata___0A338187];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__template___templ__0B27A5C0')
    ALTER TABLE [ref].[template_rapor] DROP CONSTRAINT [FK__template___templ__0B27A5C0];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.template_rapor')
BEGIN
    DECLARE @reftable_25 nvarchar(60), @constraintname_25 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.template_rapor'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_25, @constraintname_25
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_25+' drop constraint '+@constraintname_25)
        FETCH NEXT from refcursor into @reftable_25, @constraintname_25
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[template_rapor]
END

CREATE TABLE [ref].[template_rapor]
(
    [template_id] CHAR(16) NOT NULL,
    [mata_pelajaran_id] INT NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [template_rapor_PK] PRIMARY KEY ([template_id],[mata_pelajaran_id])
);

CREATE INDEX [TRAPOR_MTP_FK] ON [ref].[template_rapor] ([mata_pelajaran_id]);

CREATE INDEX [PK__template__3E882BDB2A7E6472] ON [ref].[template_rapor] ([template_id],[mata_pelajaran_id]);

BEGIN
ALTER TABLE [ref].[template_rapor] ADD CONSTRAINT [FK__template___mata___0A338187] FOREIGN KEY ([mata_pelajaran_id]) REFERENCES [ref].[mata_pelajaran] ([mata_pelajaran_id])
END
;

BEGIN
ALTER TABLE [ref].[template_rapor] ADD CONSTRAINT [FK__template___templ__0B27A5C0] FOREIGN KEY ([template_id]) REFERENCES [ref].[template_un] ([template_id])
END
;

-----------------------------------------------------------------------
-- beasiswa_peserta_didik
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__beasiswa___peser__161A357F')
    ALTER TABLE [beasiswa_peserta_didik] DROP CONSTRAINT [FK__beasiswa___peser__161A357F];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__beasiswa___peser__7ADC2F5E')
    ALTER TABLE [beasiswa_peserta_didik] DROP CONSTRAINT [FK__beasiswa___peser__7ADC2F5E];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__beasiswa___jenis__18F6A22A')
    ALTER TABLE [beasiswa_peserta_didik] DROP CONSTRAINT [FK__beasiswa___jenis__18F6A22A];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__beasiswa___jenis__7DB89C09')
    ALTER TABLE [beasiswa_peserta_didik] DROP CONSTRAINT [FK__beasiswa___jenis__7DB89C09];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__beasiswa___tahun__170E59B8')
    ALTER TABLE [beasiswa_peserta_didik] DROP CONSTRAINT [FK__beasiswa___tahun__170E59B8];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__beasiswa___tahun__18027DF1')
    ALTER TABLE [beasiswa_peserta_didik] DROP CONSTRAINT [FK__beasiswa___tahun__18027DF1];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__beasiswa___tahun__7BD05397')
    ALTER TABLE [beasiswa_peserta_didik] DROP CONSTRAINT [FK__beasiswa___tahun__7BD05397];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__beasiswa___tahun__7CC477D0')
    ALTER TABLE [beasiswa_peserta_didik] DROP CONSTRAINT [FK__beasiswa___tahun__7CC477D0];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'beasiswa_peserta_didik')
BEGIN
    DECLARE @reftable_26 nvarchar(60), @constraintname_26 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'beasiswa_peserta_didik'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_26, @constraintname_26
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_26+' drop constraint '+@constraintname_26)
        FETCH NEXT from refcursor into @reftable_26, @constraintname_26
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [beasiswa_peserta_didik]
END

CREATE TABLE [beasiswa_peserta_didik]
(
    [beasiswa_peserta_didik_id] CHAR(16) NOT NULL,
    [peserta_didik_id] CHAR(16) NOT NULL,
    [jenis_beasiswa_id] INT NOT NULL,
    [keterangan] VARCHAR(80) NOT NULL,
    [tahun_mulai] NUMERIC(6,0) NOT NULL,
    [tahun_selesai] NUMERIC(6,0) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [beasiswa_peserta_didik_PK] PRIMARY KEY ([beasiswa_peserta_didik_id])
);

CREATE INDEX [BEA_PD_LU] ON [beasiswa_peserta_didik] ([Last_update]);

CREATE INDEX [BEASISWA_PD_BEASISWA_FK] ON [beasiswa_peserta_didik] ([jenis_beasiswa_id]);

CREATE INDEX [BEASISWA_PD_FK] ON [beasiswa_peserta_didik] ([peserta_didik_id]);

CREATE INDEX [BEASISWA_PD_MULAI_FK] ON [beasiswa_peserta_didik] ([tahun_mulai]);

CREATE INDEX [BEASISWA_PD_SELESAI_FK] ON [beasiswa_peserta_didik] ([tahun_selesai]);

CREATE INDEX [PK__beasiswa__1D83E75529A7437A] ON [beasiswa_peserta_didik] ([beasiswa_peserta_didik_id]);

BEGIN
ALTER TABLE [beasiswa_peserta_didik] ADD CONSTRAINT [FK__beasiswa___peser__161A357F] FOREIGN KEY ([peserta_didik_id]) REFERENCES [peserta_didik] ([peserta_didik_id])
END
;

BEGIN
ALTER TABLE [beasiswa_peserta_didik] ADD CONSTRAINT [FK__beasiswa___peser__7ADC2F5E] FOREIGN KEY ([peserta_didik_id]) REFERENCES [peserta_didik] ([peserta_didik_id])
END
;

BEGIN
ALTER TABLE [beasiswa_peserta_didik] ADD CONSTRAINT [FK__beasiswa___jenis__18F6A22A] FOREIGN KEY ([jenis_beasiswa_id]) REFERENCES [ref].[jenis_beasiswa] ([jenis_beasiswa_id])
END
;

BEGIN
ALTER TABLE [beasiswa_peserta_didik] ADD CONSTRAINT [FK__beasiswa___jenis__7DB89C09] FOREIGN KEY ([jenis_beasiswa_id]) REFERENCES [ref].[jenis_beasiswa] ([jenis_beasiswa_id])
END
;

BEGIN
ALTER TABLE [beasiswa_peserta_didik] ADD CONSTRAINT [FK__beasiswa___tahun__170E59B8] FOREIGN KEY ([tahun_selesai]) REFERENCES [ref].[tahun_ajaran] ([tahun_ajaran_id])
END
;

BEGIN
ALTER TABLE [beasiswa_peserta_didik] ADD CONSTRAINT [FK__beasiswa___tahun__18027DF1] FOREIGN KEY ([tahun_mulai]) REFERENCES [ref].[tahun_ajaran] ([tahun_ajaran_id])
END
;

BEGIN
ALTER TABLE [beasiswa_peserta_didik] ADD CONSTRAINT [FK__beasiswa___tahun__7BD05397] FOREIGN KEY ([tahun_selesai]) REFERENCES [ref].[tahun_ajaran] ([tahun_ajaran_id])
END
;

BEGIN
ALTER TABLE [beasiswa_peserta_didik] ADD CONSTRAINT [FK__beasiswa___tahun__7CC477D0] FOREIGN KEY ([tahun_mulai]) REFERENCES [ref].[tahun_ajaran] ([tahun_ajaran_id])
END
;

-----------------------------------------------------------------------
-- ref.mata_pelajaran_kurikulum
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__mata_pela__gmp_i__6E8B6712')
    ALTER TABLE [ref].[mata_pelajaran_kurikulum] DROP CONSTRAINT [FK__mata_pela__gmp_i__6E8B6712];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__mata_pela__kurik__6F7F8B4B')
    ALTER TABLE [ref].[mata_pelajaran_kurikulum] DROP CONSTRAINT [FK__mata_pela__kurik__6F7F8B4B];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__mata_pela__mata___7073AF84')
    ALTER TABLE [ref].[mata_pelajaran_kurikulum] DROP CONSTRAINT [FK__mata_pela__mata___7073AF84];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__mata_pela__tingk__7167D3BD')
    ALTER TABLE [ref].[mata_pelajaran_kurikulum] DROP CONSTRAINT [FK__mata_pela__tingk__7167D3BD];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.mata_pelajaran_kurikulum')
BEGIN
    DECLARE @reftable_27 nvarchar(60), @constraintname_27 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.mata_pelajaran_kurikulum'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_27, @constraintname_27
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_27+' drop constraint '+@constraintname_27)
        FETCH NEXT from refcursor into @reftable_27, @constraintname_27
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[mata_pelajaran_kurikulum]
END

CREATE TABLE [ref].[mata_pelajaran_kurikulum]
(
    [kurikulum_id] SMALLINT(2,0) NOT NULL,
    [mata_pelajaran_id] INT NOT NULL,
    [tingkat_pendidikan_id] NUMERIC(4,0) NOT NULL,
    [gmp_id] CHAR(16) NULL,
    [jumlah_jam] NUMERIC(4,0) NOT NULL,
    [jumlah_jam_maksimum] NUMERIC(4,0) NOT NULL,
    [wajib] NUMERIC(3,0) NOT NULL,
    [sks] NUMERIC(4,0) DEFAULT ((0)) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [mata_pelajaran_kurikulum_PK] PRIMARY KEY ([kurikulum_id],[mata_pelajaran_id],[tingkat_pendidikan_id])
);

CREATE INDEX [MATPEL_GROUP_FK] ON [ref].[mata_pelajaran_kurikulum] ([gmp_id]);

CREATE INDEX [MATPELKUR_LU] ON [ref].[mata_pelajaran_kurikulum] ([last_update]);

CREATE INDEX [MATPELKUR_MATPEL_FK] ON [ref].[mata_pelajaran_kurikulum] ([mata_pelajaran_id]);

CREATE INDEX [MATPELKUR_TINGKAT_FK] ON [ref].[mata_pelajaran_kurikulum] ([tingkat_pendidikan_id]);

CREATE INDEX [PK__mata_pel__908FD25C7D29DDA2] ON [ref].[mata_pelajaran_kurikulum] ([kurikulum_id],[mata_pelajaran_id],[tingkat_pendidikan_id]);

BEGIN
ALTER TABLE [ref].[mata_pelajaran_kurikulum] ADD CONSTRAINT [FK__mata_pela__gmp_i__6E8B6712] FOREIGN KEY ([gmp_id]) REFERENCES [ref].[group_matpel] ([gmp_id])
END
;

BEGIN
ALTER TABLE [ref].[mata_pelajaran_kurikulum] ADD CONSTRAINT [FK__mata_pela__kurik__6F7F8B4B] FOREIGN KEY ([kurikulum_id]) REFERENCES [ref].[kurikulum] ([kurikulum_id])
END
;

BEGIN
ALTER TABLE [ref].[mata_pelajaran_kurikulum] ADD CONSTRAINT [FK__mata_pela__mata___7073AF84] FOREIGN KEY ([mata_pelajaran_id]) REFERENCES [ref].[mata_pelajaran] ([mata_pelajaran_id])
END
;

BEGIN
ALTER TABLE [ref].[mata_pelajaran_kurikulum] ADD CONSTRAINT [FK__mata_pela__tingk__7167D3BD] FOREIGN KEY ([tingkat_pendidikan_id]) REFERENCES [ref].[tingkat_pendidikan] ([tingkat_pendidikan_id])
END
;

-----------------------------------------------------------------------
-- ref.alat_transportasi
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.alat_transportasi')
BEGIN
    DECLARE @reftable_28 nvarchar(60), @constraintname_28 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.alat_transportasi'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_28, @constraintname_28
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_28+' drop constraint '+@constraintname_28)
        FETCH NEXT from refcursor into @reftable_28, @constraintname_28
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[alat_transportasi]
END

CREATE TABLE [ref].[alat_transportasi]
(
    [alat_transportasi_id] NUMERIC(4,0) NOT NULL,
    [nama] VARCHAR(40) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [alat_transportasi_PK] PRIMARY KEY ([alat_transportasi_id])
);

CREATE INDEX [PK__alat_tra__82A6FC612261391B] ON [ref].[alat_transportasi] ([alat_transportasi_id]);

-----------------------------------------------------------------------
-- vld_prasarana
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_prasa__prasa__4DD47EBD')
    ALTER TABLE [vld_prasarana] DROP CONSTRAINT [FK__vld_prasa__prasa__4DD47EBD];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_prasa__prasa__691284DE')
    ALTER TABLE [vld_prasarana] DROP CONSTRAINT [FK__vld_prasa__prasa__691284DE];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_prasa__idtyp__681E60A5')
    ALTER TABLE [vld_prasarana] DROP CONSTRAINT [FK__vld_prasa__idtyp__681E60A5];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_prasa__idtyp__4CE05A84')
    ALTER TABLE [vld_prasarana] DROP CONSTRAINT [FK__vld_prasa__idtyp__4CE05A84];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_prasarana')
BEGIN
    DECLARE @reftable_29 nvarchar(60), @constraintname_29 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_prasarana'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_29, @constraintname_29
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_29+' drop constraint '+@constraintname_29)
        FETCH NEXT from refcursor into @reftable_29, @constraintname_29
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_prasarana]
END

CREATE TABLE [vld_prasarana]
(
    [prasarana_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_prasarana_PK] PRIMARY KEY ([prasarana_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_prasarana] ([idtype]);

CREATE INDEX [VLD_PRAS_LU] ON [vld_prasarana] ([last_update]);

CREATE INDEX [PK__vld_pras__F893A0713EB37F66] ON [vld_prasarana] ([prasarana_id],[logid]);

BEGIN
ALTER TABLE [vld_prasarana] ADD CONSTRAINT [FK__vld_prasa__prasa__4DD47EBD] FOREIGN KEY ([prasarana_id]) REFERENCES [prasarana] ([prasarana_id])
END
;

BEGIN
ALTER TABLE [vld_prasarana] ADD CONSTRAINT [FK__vld_prasa__prasa__691284DE] FOREIGN KEY ([prasarana_id]) REFERENCES [prasarana] ([prasarana_id])
END
;

BEGIN
ALTER TABLE [vld_prasarana] ADD CONSTRAINT [FK__vld_prasa__idtyp__681E60A5] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_prasarana] ADD CONSTRAINT [FK__vld_prasa__idtyp__4CE05A84] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- peserta_didik
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__sekol__0B47A151')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__sekol__0B47A151];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__sekol__2685A772')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__sekol__2685A772];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__agama__0777106D')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__agama__0777106D];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__agama__22B5168E')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__agama__22B5168E];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__alat___0A537D18')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__alat___0A537D18];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__alat___25918339')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__alat___25918339];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__jenis__095F58DF')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__jenis__095F58DF];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__jenis__249D5F00')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__jenis__249D5F00];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__jenja__0D2FE9C3')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__jenja__0D2FE9C3];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__jenja__100C566E')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__jenja__100C566E];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__jenja__12E8C319')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__jenja__12E8C319];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__jenja__286DEFE4')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__jenja__286DEFE4];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__jenja__2B4A5C8F')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__jenja__2B4A5C8F];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__jenja__2E26C93A')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__jenja__2E26C93A];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__kebut__02B25B50')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__kebut__02B25B50];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__kebut__03A67F89')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__kebut__03A67F89];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__kebut__0682EC34')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__kebut__0682EC34];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__kebut__1DF06171')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__kebut__1DF06171];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__kebut__1EE485AA')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__kebut__1EE485AA];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__kebut__21C0F255')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__kebut__21C0F255];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__kode___058EC7FB')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__kode___058EC7FB];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__kode___20CCCE1C')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__kode___20CCCE1C];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__kewar__049AA3C2')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__kewar__049AA3C2];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__kewar__1FD8A9E3')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__kewar__1FD8A9E3];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__peker__0C3BC58A')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__peker__0C3BC58A];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__peker__0F183235')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__peker__0F183235];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__peker__11F49EE0')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__peker__11F49EE0];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__peker__2779CBAB')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__peker__2779CBAB];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__peker__2A563856')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__peker__2A563856];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__peker__2D32A501')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__peker__2D32A501];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__pengh__086B34A6')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__pengh__086B34A6];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__pengh__0E240DFC')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__pengh__0E240DFC];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__pengh__11007AA7')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__pengh__11007AA7];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__pengh__23A93AC7')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__pengh__23A93AC7];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__pengh__2962141D')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__pengh__2962141D];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__pengh__2C3E80C8')
    ALTER TABLE [peserta_didik] DROP CONSTRAINT [FK__peserta_d__pengh__2C3E80C8];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'peserta_didik')
BEGIN
    DECLARE @reftable_30 nvarchar(60), @constraintname_30 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'peserta_didik'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_30, @constraintname_30
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_30+' drop constraint '+@constraintname_30)
        FETCH NEXT from refcursor into @reftable_30, @constraintname_30
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [peserta_didik]
END

CREATE TABLE [peserta_didik]
(
    [peserta_didik_id] CHAR(16) NOT NULL,
    [nama] VARCHAR(50) NULL,
    [jenis_kelamin] CHAR(1) NOT NULL,
    [nisn] CHAR(10) NULL,
    [nik] CHAR(16) NULL,
    [tempat_lahir] VARCHAR(20) NULL,
    [tanggal_lahir] VARCHAR(20) NOT NULL,
    [agama_id] SMALLINT(2,0) NOT NULL,
    [kewarganegaraan] CHAR(2) NOT NULL,
    [kebutuhan_khusus_id] INT NOT NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [alamat_jalan] VARCHAR(80) NOT NULL,
    [rt] NUMERIC(4,0) NULL,
    [rw] NUMERIC(4,0) NULL,
    [nama_dusun] VARCHAR(40) NULL,
    [desa_kelurahan] VARCHAR(40) NOT NULL,
    [kode_wilayah] CHAR(8) NOT NULL,
    [kode_pos] CHAR(5) NULL,
    [jenis_tinggal_id] NUMERIC(4,0) NULL,
    [alat_transportasi_id] NUMERIC(4,0) NULL,
    [nomor_telepon_rumah] VARCHAR(20) NULL,
    [nomor_telepon_seluler] VARCHAR(20) NULL,
    [email] VARCHAR(50) NULL,
    [penerima_KPS] NUMERIC(3,0) NOT NULL,
    [no_KPS] VARCHAR(40) NULL,
    [status_data] INT NULL,
    [nama_ayah] VARCHAR(50) NULL,
    [tahun_lahir_ayah] NUMERIC(6,0) NULL,
    [jenjang_pendidikan_ayah] NUMERIC(4,0) NULL,
    [pekerjaan_id_ayah] INT NULL,
    [penghasilan_id_ayah] INT NULL,
    [kebutuhan_khusus_id_ayah] INT NOT NULL,
    [nama_ibu_kandung] VARCHAR(50) NOT NULL,
    [tahun_lahir_ibu] NUMERIC(6,0) NULL,
    [jenjang_pendidikan_ibu] NUMERIC(4,0) NULL,
    [penghasilan_id_ibu] INT NULL,
    [pekerjaan_id_ibu] INT NULL,
    [kebutuhan_khusus_id_ibu] INT NOT NULL,
    [nama_wali] VARCHAR(30) NULL,
    [tahun_lahir_wali] NUMERIC(6,0) NULL,
    [jenjang_pendidikan_wali] NUMERIC(4,0) NULL,
    [pekerjaan_id_wali] INT NULL,
    [penghasilan_id_wali] INT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [peserta_didik_PK] PRIMARY KEY ([peserta_didik_id])
);

CREATE INDEX [AYAH_KK_FK] ON [peserta_didik] ([kebutuhan_khusus_id_ayah]);

CREATE INDEX [IBU_KK_FK] ON [peserta_didik] ([kebutuhan_khusus_id_ibu]);

CREATE INDEX [KEWARGANEGARAAN_PD_FK] ON [peserta_didik] ([kewarganegaraan]);

CREATE INDEX [PD_DEL] ON [peserta_didik] ([Soft_delete]);

CREATE INDEX [PD_ENTRY_FK] ON [peserta_didik] ([sekolah_id]);

CREATE INDEX [PD_JENIS_TINGGAL_FK] ON [peserta_didik] ([jenis_tinggal_id]);

CREATE INDEX [PD_KECAMATAN_FK] ON [peserta_didik] ([kode_wilayah]);

CREATE INDEX [PD_KK_FK] ON [peserta_didik] ([kebutuhan_khusus_id]);

CREATE INDEX [PD_LU] ON [peserta_didik] ([Last_update]);

CREATE INDEX [PD_TGL_NAMA] ON [peserta_didik] ([tanggal_lahir],[nama]);

CREATE INDEX [PD_TRANSPORT_FK] ON [peserta_didik] ([alat_transportasi_id]);

CREATE INDEX [PEKERJAAN_AYAH_FK] ON [peserta_didik] ([pekerjaan_id_wali]);

CREATE INDEX [PEKERJAAN_IBU_FK] ON [peserta_didik] ([pekerjaan_id_ayah]);

CREATE INDEX [PEKERJAAN_WALI_FK] ON [peserta_didik] ([pekerjaan_id_ibu]);

CREATE INDEX [PENDIDIKAN_AYAH_FK] ON [peserta_didik] ([jenjang_pendidikan_wali]);

CREATE INDEX [PENDIDIKAN_IBU_FK] ON [peserta_didik] ([jenjang_pendidikan_ayah]);

CREATE INDEX [PENDIDIKAN_WALI_FK] ON [peserta_didik] ([jenjang_pendidikan_ibu]);

CREATE INDEX [PENGHASILAN_AYAH_FK] ON [peserta_didik] ([penghasilan_id_wali]);

CREATE INDEX [PENGHASILAN_IBU_FK] ON [peserta_didik] ([penghasilan_id_ibu]);

CREATE INDEX [PENGHASILAN_WALI_FK] ON [peserta_didik] ([penghasilan_id_ayah]);

CREATE INDEX [PESERTA_DIDIK_AGAMA_FK] ON [peserta_didik] ([agama_id]);

CREATE INDEX [PK__peserta___F3154BCDBDDC5476] ON [peserta_didik] ([peserta_didik_id]);

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__sekol__0B47A151] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__sekol__2685A772] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__agama__0777106D] FOREIGN KEY ([agama_id]) REFERENCES [ref].[agama] ([agama_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__agama__22B5168E] FOREIGN KEY ([agama_id]) REFERENCES [ref].[agama] ([agama_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__alat___0A537D18] FOREIGN KEY ([alat_transportasi_id]) REFERENCES [ref].[alat_transportasi] ([alat_transportasi_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__alat___25918339] FOREIGN KEY ([alat_transportasi_id]) REFERENCES [ref].[alat_transportasi] ([alat_transportasi_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__jenis__095F58DF] FOREIGN KEY ([jenis_tinggal_id]) REFERENCES [ref].[jenis_tinggal] ([jenis_tinggal_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__jenis__249D5F00] FOREIGN KEY ([jenis_tinggal_id]) REFERENCES [ref].[jenis_tinggal] ([jenis_tinggal_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__jenja__0D2FE9C3] FOREIGN KEY ([jenjang_pendidikan_ibu]) REFERENCES [ref].[jenjang_pendidikan] ([jenjang_pendidikan_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__jenja__100C566E] FOREIGN KEY ([jenjang_pendidikan_ayah]) REFERENCES [ref].[jenjang_pendidikan] ([jenjang_pendidikan_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__jenja__12E8C319] FOREIGN KEY ([jenjang_pendidikan_wali]) REFERENCES [ref].[jenjang_pendidikan] ([jenjang_pendidikan_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__jenja__286DEFE4] FOREIGN KEY ([jenjang_pendidikan_ibu]) REFERENCES [ref].[jenjang_pendidikan] ([jenjang_pendidikan_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__jenja__2B4A5C8F] FOREIGN KEY ([jenjang_pendidikan_ayah]) REFERENCES [ref].[jenjang_pendidikan] ([jenjang_pendidikan_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__jenja__2E26C93A] FOREIGN KEY ([jenjang_pendidikan_wali]) REFERENCES [ref].[jenjang_pendidikan] ([jenjang_pendidikan_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__kebut__02B25B50] FOREIGN KEY ([kebutuhan_khusus_id_ayah]) REFERENCES [ref].[kebutuhan_khusus] ([kebutuhan_khusus_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__kebut__03A67F89] FOREIGN KEY ([kebutuhan_khusus_id_ibu]) REFERENCES [ref].[kebutuhan_khusus] ([kebutuhan_khusus_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__kebut__0682EC34] FOREIGN KEY ([kebutuhan_khusus_id]) REFERENCES [ref].[kebutuhan_khusus] ([kebutuhan_khusus_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__kebut__1DF06171] FOREIGN KEY ([kebutuhan_khusus_id_ayah]) REFERENCES [ref].[kebutuhan_khusus] ([kebutuhan_khusus_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__kebut__1EE485AA] FOREIGN KEY ([kebutuhan_khusus_id_ibu]) REFERENCES [ref].[kebutuhan_khusus] ([kebutuhan_khusus_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__kebut__21C0F255] FOREIGN KEY ([kebutuhan_khusus_id]) REFERENCES [ref].[kebutuhan_khusus] ([kebutuhan_khusus_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__kode___058EC7FB] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__kode___20CCCE1C] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__kewar__049AA3C2] FOREIGN KEY ([kewarganegaraan]) REFERENCES [ref].[negara] ([negara_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__kewar__1FD8A9E3] FOREIGN KEY ([kewarganegaraan]) REFERENCES [ref].[negara] ([negara_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__peker__0C3BC58A] FOREIGN KEY ([pekerjaan_id_ayah]) REFERENCES [ref].[pekerjaan] ([pekerjaan_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__peker__0F183235] FOREIGN KEY ([pekerjaan_id_ibu]) REFERENCES [ref].[pekerjaan] ([pekerjaan_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__peker__11F49EE0] FOREIGN KEY ([pekerjaan_id_wali]) REFERENCES [ref].[pekerjaan] ([pekerjaan_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__peker__2779CBAB] FOREIGN KEY ([pekerjaan_id_ayah]) REFERENCES [ref].[pekerjaan] ([pekerjaan_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__peker__2A563856] FOREIGN KEY ([pekerjaan_id_ibu]) REFERENCES [ref].[pekerjaan] ([pekerjaan_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__peker__2D32A501] FOREIGN KEY ([pekerjaan_id_wali]) REFERENCES [ref].[pekerjaan] ([pekerjaan_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__pengh__086B34A6] FOREIGN KEY ([penghasilan_id_ayah]) REFERENCES [ref].[penghasilan_orangtua_wali] ([penghasilan_orangtua_wali_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__pengh__0E240DFC] FOREIGN KEY ([penghasilan_id_wali]) REFERENCES [ref].[penghasilan_orangtua_wali] ([penghasilan_orangtua_wali_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__pengh__11007AA7] FOREIGN KEY ([penghasilan_id_ibu]) REFERENCES [ref].[penghasilan_orangtua_wali] ([penghasilan_orangtua_wali_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__pengh__23A93AC7] FOREIGN KEY ([penghasilan_id_ayah]) REFERENCES [ref].[penghasilan_orangtua_wali] ([penghasilan_orangtua_wali_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__pengh__2962141D] FOREIGN KEY ([penghasilan_id_wali]) REFERENCES [ref].[penghasilan_orangtua_wali] ([penghasilan_orangtua_wali_id])
END
;

BEGIN
ALTER TABLE [peserta_didik] ADD CONSTRAINT [FK__peserta_d__pengh__2C3E80C8] FOREIGN KEY ([penghasilan_id_ibu]) REFERENCES [ref].[penghasilan_orangtua_wali] ([penghasilan_orangtua_wali_id])
END
;

-----------------------------------------------------------------------
-- sanitasi
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sanitasi__sekola__7EACC042')
    ALTER TABLE [sanitasi] DROP CONSTRAINT [FK__sanitasi__sekola__7EACC042];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sanitasi__sekola__19EAC663')
    ALTER TABLE [sanitasi] DROP CONSTRAINT [FK__sanitasi__sekola__19EAC663];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sanitasi__semest__1ADEEA9C')
    ALTER TABLE [sanitasi] DROP CONSTRAINT [FK__sanitasi__semest__1ADEEA9C];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sanitasi__semest__7FA0E47B')
    ALTER TABLE [sanitasi] DROP CONSTRAINT [FK__sanitasi__semest__7FA0E47B];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sanitasi__sumber__009508B4')
    ALTER TABLE [sanitasi] DROP CONSTRAINT [FK__sanitasi__sumber__009508B4];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sanitasi__sumber__1BD30ED5')
    ALTER TABLE [sanitasi] DROP CONSTRAINT [FK__sanitasi__sumber__1BD30ED5];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'sanitasi')
BEGIN
    DECLARE @reftable_31 nvarchar(60), @constraintname_31 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'sanitasi'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_31, @constraintname_31
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_31+' drop constraint '+@constraintname_31)
        FETCH NEXT from refcursor into @reftable_31, @constraintname_31
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [sanitasi]
END

CREATE TABLE [sanitasi]
(
    [sekolah_id] CHAR(16) NOT NULL,
    [semester_id] CHAR(5) NOT NULL,
    [sumber_air_id] NUMERIC(4,0) NOT NULL,
    [ketersediaan_air] NUMERIC(3,0) NOT NULL,
    [kecukupan_air] NUMERIC(3,0) NOT NULL,
    [minum_siswa] NUMERIC(3,0) NOT NULL,
    [memproses_air] NUMERIC(3,0) NOT NULL,
    [siswa_bawa_air] NUMERIC(3,0) NOT NULL,
    [toilet_siswa_laki] NUMERIC(4,0) NOT NULL,
    [toilet_siswa_perempuan] NUMERIC(4,0) NOT NULL,
    [toilet_siswa_kk] NUMERIC(4,0) NOT NULL,
    [toilet_siswa_kecil] NUMERIC(3,0) NOT NULL,
    [tempat_cuci_tangan] NUMERIC(4,0) DEFAULT ((0)) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [sanitasi_PK] PRIMARY KEY ([sekolah_id],[semester_id])
);

CREATE INDEX [SANITASI_LU] ON [sanitasi] ([Last_update]);

CREATE INDEX [SANITASI_SEM_FK] ON [sanitasi] ([semester_id]);

CREATE INDEX [SANITASI_SUMBER_FK] ON [sanitasi] ([sumber_air_id]);

CREATE INDEX [PK__sanitasi__F53A286E1A528AA5] ON [sanitasi] ([sekolah_id],[semester_id]);

BEGIN
ALTER TABLE [sanitasi] ADD CONSTRAINT [FK__sanitasi__sekola__7EACC042] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [sanitasi] ADD CONSTRAINT [FK__sanitasi__sekola__19EAC663] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [sanitasi] ADD CONSTRAINT [FK__sanitasi__semest__1ADEEA9C] FOREIGN KEY ([semester_id]) REFERENCES [ref].[semester] ([semester_id])
END
;

BEGIN
ALTER TABLE [sanitasi] ADD CONSTRAINT [FK__sanitasi__semest__7FA0E47B] FOREIGN KEY ([semester_id]) REFERENCES [ref].[semester] ([semester_id])
END
;

BEGIN
ALTER TABLE [sanitasi] ADD CONSTRAINT [FK__sanitasi__sumber__009508B4] FOREIGN KEY ([sumber_air_id]) REFERENCES [ref].[sumber_air] ([sumber_air_id])
END
;

BEGIN
ALTER TABLE [sanitasi] ADD CONSTRAINT [FK__sanitasi__sumber__1BD30ED5] FOREIGN KEY ([sumber_air_id]) REFERENCES [ref].[sumber_air] ([sumber_air_id])
END
;

-----------------------------------------------------------------------
-- ref.map_bidang_mata_pelajaran
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__map_bidan__bidan__0C1BC9F9')
    ALTER TABLE [ref].[map_bidang_mata_pelajaran] DROP CONSTRAINT [FK__map_bidan__bidan__0C1BC9F9];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__map_bidan__mata___0D0FEE32')
    ALTER TABLE [ref].[map_bidang_mata_pelajaran] DROP CONSTRAINT [FK__map_bidan__mata___0D0FEE32];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.map_bidang_mata_pelajaran')
BEGIN
    DECLARE @reftable_32 nvarchar(60), @constraintname_32 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.map_bidang_mata_pelajaran'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_32, @constraintname_32
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_32+' drop constraint '+@constraintname_32)
        FETCH NEXT from refcursor into @reftable_32, @constraintname_32
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[map_bidang_mata_pelajaran]
END

CREATE TABLE [ref].[map_bidang_mata_pelajaran]
(
    [mata_pelajaran_id] INT NOT NULL,
    [bidang_studi_id] INT NOT NULL,
    [tahun_sertifikasi] NUMERIC(6,0) NOT NULL,
    [sesuai] NUMERIC(3,0) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [map_bidang_mata_pelajaran_PK] PRIMARY KEY ([mata_pelajaran_id],[bidang_studi_id],[tahun_sertifikasi])
);

CREATE INDEX [MAP_BIDANG_MATPEL_LU] ON [ref].[map_bidang_mata_pelajaran] ([last_update]);

CREATE INDEX [MAP_BIDANG_STUDI_FK] ON [ref].[map_bidang_mata_pelajaran] ([bidang_studi_id]);

CREATE INDEX [PK__map_bida__49807ECFED8171AC] ON [ref].[map_bidang_mata_pelajaran] ([mata_pelajaran_id],[bidang_studi_id],[tahun_sertifikasi]);

BEGIN
ALTER TABLE [ref].[map_bidang_mata_pelajaran] ADD CONSTRAINT [FK__map_bidan__bidan__0C1BC9F9] FOREIGN KEY ([bidang_studi_id]) REFERENCES [ref].[bidang_studi] ([bidang_studi_id])
END
;

BEGIN
ALTER TABLE [ref].[map_bidang_mata_pelajaran] ADD CONSTRAINT [FK__map_bidan__mata___0D0FEE32] FOREIGN KEY ([mata_pelajaran_id]) REFERENCES [ref].[mata_pelajaran] ([mata_pelajaran_id])
END
;

-----------------------------------------------------------------------
-- registrasi_peserta_didik
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__registras__jurus__13DCE752')
    ALTER TABLE [registrasi_peserta_didik] DROP CONSTRAINT [FK__registras__jurus__13DCE752];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__registras__jurus__2F1AED73')
    ALTER TABLE [registrasi_peserta_didik] DROP CONSTRAINT [FK__registras__jurus__2F1AED73];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__registras__peser__14D10B8B')
    ALTER TABLE [registrasi_peserta_didik] DROP CONSTRAINT [FK__registras__peser__14D10B8B];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__registras__peser__300F11AC')
    ALTER TABLE [registrasi_peserta_didik] DROP CONSTRAINT [FK__registras__peser__300F11AC];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__registras__sekol__310335E5')
    ALTER TABLE [registrasi_peserta_didik] DROP CONSTRAINT [FK__registras__sekol__310335E5];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__registras__sekol__15C52FC4')
    ALTER TABLE [registrasi_peserta_didik] DROP CONSTRAINT [FK__registras__sekol__15C52FC4];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__registras__jenis__17AD7836')
    ALTER TABLE [registrasi_peserta_didik] DROP CONSTRAINT [FK__registras__jenis__17AD7836];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__registras__jenis__32EB7E57')
    ALTER TABLE [registrasi_peserta_didik] DROP CONSTRAINT [FK__registras__jenis__32EB7E57];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__registras__jenis__16B953FD')
    ALTER TABLE [registrasi_peserta_didik] DROP CONSTRAINT [FK__registras__jenis__16B953FD];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__registras__jenis__31F75A1E')
    ALTER TABLE [registrasi_peserta_didik] DROP CONSTRAINT [FK__registras__jenis__31F75A1E];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'registrasi_peserta_didik')
BEGIN
    DECLARE @reftable_33 nvarchar(60), @constraintname_33 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'registrasi_peserta_didik'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_33, @constraintname_33
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_33+' drop constraint '+@constraintname_33)
        FETCH NEXT from refcursor into @reftable_33, @constraintname_33
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [registrasi_peserta_didik]
END

CREATE TABLE [registrasi_peserta_didik]
(
    [registrasi_id] CHAR(16) NOT NULL,
    [jurusan_sp_id] CHAR(16) NULL,
    [peserta_didik_id] CHAR(16) NOT NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [jenis_pendaftaran_id] NUMERIC(3,0) NOT NULL,
    [nipd] VARCHAR(18) NULL,
    [tanggal_masuk_sekolah] VARCHAR(20) NOT NULL,
    [jenis_keluar_id] CHAR(1) NULL,
    [tanggal_keluar] VARCHAR(20) NULL,
    [keterangan] VARCHAR(128) NULL,
    [no_SKHUN] CHAR(20) NULL,
    [a_pernah_paud] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [a_pernah_tk] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [registrasi_peserta_didik_PK] PRIMARY KEY ([registrasi_id])
);

CREATE INDEX [PD_JURUSAN_FK] ON [registrasi_peserta_didik] ([jurusan_sp_id]);

CREATE INDEX [REG_PD_DEL] ON [registrasi_peserta_didik] ([Soft_delete]);

CREATE INDEX [REG_PD_JENIS_DAFTAR_FK] ON [registrasi_peserta_didik] ([jenis_pendaftaran_id]);

CREATE INDEX [REG_PD_LU] ON [registrasi_peserta_didik] ([Last_update]);

CREATE INDEX [REG_PD_SP_LU] ON [registrasi_peserta_didik] ([sekolah_id],[Last_update]);

CREATE INDEX [REGIS_KELUAR_FK] ON [registrasi_peserta_didik] ([jenis_keluar_id]);

CREATE INDEX [PD_SEKOLAH_UNIQUE] ON [registrasi_peserta_didik] ([peserta_didik_id],[sekolah_id]);

CREATE INDEX [PK__registra__6D5894B52200442E] ON [registrasi_peserta_didik] ([registrasi_id]);

BEGIN
ALTER TABLE [registrasi_peserta_didik] ADD CONSTRAINT [FK__registras__jurus__13DCE752] FOREIGN KEY ([jurusan_sp_id]) REFERENCES [jurusan_sp] ([jurusan_sp_id])
END
;

BEGIN
ALTER TABLE [registrasi_peserta_didik] ADD CONSTRAINT [FK__registras__jurus__2F1AED73] FOREIGN KEY ([jurusan_sp_id]) REFERENCES [jurusan_sp] ([jurusan_sp_id])
END
;

BEGIN
ALTER TABLE [registrasi_peserta_didik] ADD CONSTRAINT [FK__registras__peser__14D10B8B] FOREIGN KEY ([peserta_didik_id]) REFERENCES [peserta_didik] ([peserta_didik_id])
END
;

BEGIN
ALTER TABLE [registrasi_peserta_didik] ADD CONSTRAINT [FK__registras__peser__300F11AC] FOREIGN KEY ([peserta_didik_id]) REFERENCES [peserta_didik] ([peserta_didik_id])
END
;

BEGIN
ALTER TABLE [registrasi_peserta_didik] ADD CONSTRAINT [FK__registras__sekol__310335E5] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [registrasi_peserta_didik] ADD CONSTRAINT [FK__registras__sekol__15C52FC4] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [registrasi_peserta_didik] ADD CONSTRAINT [FK__registras__jenis__17AD7836] FOREIGN KEY ([jenis_keluar_id]) REFERENCES [ref].[jenis_keluar] ([jenis_keluar_id])
END
;

BEGIN
ALTER TABLE [registrasi_peserta_didik] ADD CONSTRAINT [FK__registras__jenis__32EB7E57] FOREIGN KEY ([jenis_keluar_id]) REFERENCES [ref].[jenis_keluar] ([jenis_keluar_id])
END
;

BEGIN
ALTER TABLE [registrasi_peserta_didik] ADD CONSTRAINT [FK__registras__jenis__16B953FD] FOREIGN KEY ([jenis_pendaftaran_id]) REFERENCES [ref].[jenis_pendaftaran] ([jenis_pendaftaran_id])
END
;

BEGIN
ALTER TABLE [registrasi_peserta_didik] ADD CONSTRAINT [FK__registras__jenis__31F75A1E] FOREIGN KEY ([jenis_pendaftaran_id]) REFERENCES [ref].[jenis_pendaftaran] ([jenis_pendaftaran_id])
END
;

-----------------------------------------------------------------------
-- ref.status_kepegawaian
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.status_kepegawaian')
BEGIN
    DECLARE @reftable_34 nvarchar(60), @constraintname_34 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.status_kepegawaian'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_34, @constraintname_34
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_34+' drop constraint '+@constraintname_34)
        FETCH NEXT from refcursor into @reftable_34, @constraintname_34
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[status_kepegawaian]
END

CREATE TABLE [ref].[status_kepegawaian]
(
    [status_kepegawaian_id] SMALLINT(2,0) NOT NULL,
    [nama] VARCHAR(30) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [status_kepegawaian_PK] PRIMARY KEY ([status_kepegawaian_id])
);

CREATE INDEX [PK__status_k__56CEB7AC154A9DA2] ON [ref].[status_kepegawaian] ([status_kepegawaian_id]);

-----------------------------------------------------------------------
-- vld_peserta_didik
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_peser__peser__4FBCC72F')
    ALTER TABLE [vld_peserta_didik] DROP CONSTRAINT [FK__vld_peser__peser__4FBCC72F];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_peser__peser__6AFACD50')
    ALTER TABLE [vld_peserta_didik] DROP CONSTRAINT [FK__vld_peser__peser__6AFACD50];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_peser__idtyp__6A06A917')
    ALTER TABLE [vld_peserta_didik] DROP CONSTRAINT [FK__vld_peser__idtyp__6A06A917];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_peser__idtyp__4EC8A2F6')
    ALTER TABLE [vld_peserta_didik] DROP CONSTRAINT [FK__vld_peser__idtyp__4EC8A2F6];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_peserta_didik')
BEGIN
    DECLARE @reftable_35 nvarchar(60), @constraintname_35 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_peserta_didik'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_35, @constraintname_35
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_35+' drop constraint '+@constraintname_35)
        FETCH NEXT from refcursor into @reftable_35, @constraintname_35
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_peserta_didik]
END

CREATE TABLE [vld_peserta_didik]
(
    [peserta_didik_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_peserta_didik_PK] PRIMARY KEY ([peserta_didik_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_peserta_didik] ([idtype]);

CREATE INDEX [VLD_PD_LU] ON [vld_peserta_didik] ([last_update]);

CREATE INDEX [PK__vld_pese__B496C4EA1514FB0A] ON [vld_peserta_didik] ([peserta_didik_id],[logid]);

BEGIN
ALTER TABLE [vld_peserta_didik] ADD CONSTRAINT [FK__vld_peser__peser__4FBCC72F] FOREIGN KEY ([peserta_didik_id]) REFERENCES [peserta_didik] ([peserta_didik_id])
END
;

BEGIN
ALTER TABLE [vld_peserta_didik] ADD CONSTRAINT [FK__vld_peser__peser__6AFACD50] FOREIGN KEY ([peserta_didik_id]) REFERENCES [peserta_didik] ([peserta_didik_id])
END
;

BEGIN
ALTER TABLE [vld_peserta_didik] ADD CONSTRAINT [FK__vld_peser__idtyp__6A06A917] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_peserta_didik] ADD CONSTRAINT [FK__vld_peser__idtyp__4EC8A2F6] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- ref.mst_wilayah
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__mst_wilay__id_le__725BF7F6')
    ALTER TABLE [ref].[mst_wilayah] DROP CONSTRAINT [FK__mst_wilay__id_le__725BF7F6];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__mst_wilay__mst_k__73501C2F')
    ALTER TABLE [ref].[mst_wilayah] DROP CONSTRAINT [FK__mst_wilay__mst_k__73501C2F];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__mst_wilay__negar__74444068')
    ALTER TABLE [ref].[mst_wilayah] DROP CONSTRAINT [FK__mst_wilay__negar__74444068];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.mst_wilayah')
BEGIN
    DECLARE @reftable_36 nvarchar(60), @constraintname_36 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.mst_wilayah'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_36, @constraintname_36
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_36+' drop constraint '+@constraintname_36)
        FETCH NEXT from refcursor into @reftable_36, @constraintname_36
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[mst_wilayah]
END

CREATE TABLE [ref].[mst_wilayah]
(
    [kode_wilayah] CHAR(8) NOT NULL,
    [nama] VARCHAR(40) NOT NULL,
    [id_level_wilayah] SMALLINT(2,0) NOT NULL,
    [mst_kode_wilayah] CHAR(8) NULL,
    [negara_id] CHAR(2) NOT NULL,
    [asal_wilayah] CHAR(8) NULL,
    [kode_bps] CHAR(7) NULL,
    [kode_dagri] CHAR(7) NULL,
    [kode_keu] VARCHAR(10) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [mst_wilayah_PK] PRIMARY KEY ([kode_wilayah])
);

CREATE INDEX [MST_WILAYAH_LU] ON [ref].[mst_wilayah] ([last_update]);

CREATE INDEX [PARENT_WILAYAH_FK] ON [ref].[mst_wilayah] ([mst_kode_wilayah]);

CREATE INDEX [PROPINSI_NEGARA_FK] ON [ref].[mst_wilayah] ([negara_id]);

CREATE INDEX [SYNC_ORDER] ON [ref].[mst_wilayah] ([id_level_wilayah],[kode_wilayah]);

CREATE INDEX [PK__mst_wila__29E23AA1D43B5B97] ON [ref].[mst_wilayah] ([kode_wilayah]);

BEGIN
ALTER TABLE [ref].[mst_wilayah] ADD CONSTRAINT [FK__mst_wilay__id_le__725BF7F6] FOREIGN KEY ([id_level_wilayah]) REFERENCES [ref].[level_wilayah] ([id_level_wilayah])
END
;

BEGIN
ALTER TABLE [ref].[mst_wilayah] ADD CONSTRAINT [FK__mst_wilay__mst_k__73501C2F] FOREIGN KEY ([mst_kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

BEGIN
ALTER TABLE [ref].[mst_wilayah] ADD CONSTRAINT [FK__mst_wilay__negar__74444068] FOREIGN KEY ([negara_id]) REFERENCES [ref].[negara] ([negara_id])
END
;

-----------------------------------------------------------------------
-- pengguna
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengguna__lembag__0371755F')
    ALTER TABLE [pengguna] DROP CONSTRAINT [FK__pengguna__lembag__0371755F];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengguna__lembag__1EAF7B80')
    ALTER TABLE [pengguna] DROP CONSTRAINT [FK__pengguna__lembag__1EAF7B80];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengguna__sekola__01892CED')
    ALTER TABLE [pengguna] DROP CONSTRAINT [FK__pengguna__sekola__01892CED];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengguna__sekola__1CC7330E')
    ALTER TABLE [pengguna] DROP CONSTRAINT [FK__pengguna__sekola__1CC7330E];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengguna__kode_w__027D5126')
    ALTER TABLE [pengguna] DROP CONSTRAINT [FK__pengguna__kode_w__027D5126];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengguna__kode_w__1DBB5747')
    ALTER TABLE [pengguna] DROP CONSTRAINT [FK__pengguna__kode_w__1DBB5747];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengguna__peran___04659998')
    ALTER TABLE [pengguna] DROP CONSTRAINT [FK__pengguna__peran___04659998];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengguna__peran___1FA39FB9')
    ALTER TABLE [pengguna] DROP CONSTRAINT [FK__pengguna__peran___1FA39FB9];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'pengguna')
BEGIN
    DECLARE @reftable_37 nvarchar(60), @constraintname_37 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'pengguna'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_37, @constraintname_37
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_37+' drop constraint '+@constraintname_37)
        FETCH NEXT from refcursor into @reftable_37, @constraintname_37
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [pengguna]
END

CREATE TABLE [pengguna]
(
    [pengguna_id] CHAR(16) NOT NULL,
    [sekolah_id] CHAR(16) NULL,
    [lembaga_id] CHAR(16) NULL,
    [yayasan_id] CHAR(16) NULL,
    [la_id] CHAR(5) NULL,
    [peran_id] INT NOT NULL,
    [username] VARCHAR(50) NOT NULL,
    [password] VARCHAR(50) NOT NULL,
    [nama] VARCHAR(50) NOT NULL,
    [nip_nim] VARCHAR(9) NULL,
    [jabatan_lembaga] VARCHAR(25) NULL,
    [ym] VARCHAR(20) NULL,
    [skype] VARCHAR(20) NULL,
    [alamat] VARCHAR(80) NULL,
    [kode_wilayah] CHAR(8) NOT NULL,
    [no_telepon] VARCHAR(20) NULL,
    [no_hp] VARCHAR(20) NULL,
    [aktif] NUMERIC(3,0) DEFAULT ((1)) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [pengguna_PK] PRIMARY KEY ([pengguna_id])
);

CREATE INDEX [PENGGUNA_KABKOTA_FK] ON [pengguna] ([kode_wilayah]);

CREATE INDEX [PENGGUNA_LEMBAGA_FK] ON [pengguna] ([sekolah_id]);

CREATE INDEX [PENGGUNA_LEMBAGA_FK2] ON [pengguna] ([lembaga_id]);

CREATE INDEX [PENGGUNA_PERAN_FK] ON [pengguna] ([peran_id]);

CREATE INDEX [PK__pengguna__043010D1B0D950B5] ON [pengguna] ([pengguna_id]);

BEGIN
ALTER TABLE [pengguna] ADD CONSTRAINT [FK__pengguna__lembag__0371755F] FOREIGN KEY ([lembaga_id]) REFERENCES [lembaga_non_sekolah] ([lembaga_id])
END
;

BEGIN
ALTER TABLE [pengguna] ADD CONSTRAINT [FK__pengguna__lembag__1EAF7B80] FOREIGN KEY ([lembaga_id]) REFERENCES [lembaga_non_sekolah] ([lembaga_id])
END
;

BEGIN
ALTER TABLE [pengguna] ADD CONSTRAINT [FK__pengguna__sekola__01892CED] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [pengguna] ADD CONSTRAINT [FK__pengguna__sekola__1CC7330E] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [pengguna] ADD CONSTRAINT [FK__pengguna__kode_w__027D5126] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

BEGIN
ALTER TABLE [pengguna] ADD CONSTRAINT [FK__pengguna__kode_w__1DBB5747] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

BEGIN
ALTER TABLE [pengguna] ADD CONSTRAINT [FK__pengguna__peran___04659998] FOREIGN KEY ([peran_id]) REFERENCES [ref].[peran] ([peran_id])
END
;

BEGIN
ALTER TABLE [pengguna] ADD CONSTRAINT [FK__pengguna__peran___1FA39FB9] FOREIGN KEY ([peran_id]) REFERENCES [ref].[peran] ([peran_id])
END
;

-----------------------------------------------------------------------
-- ref.group_matpel
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__group_mat__kurik__0E04126B')
    ALTER TABLE [ref].[group_matpel] DROP CONSTRAINT [FK__group_mat__kurik__0E04126B];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__group_mat__tingk__0EF836A4')
    ALTER TABLE [ref].[group_matpel] DROP CONSTRAINT [FK__group_mat__tingk__0EF836A4];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.group_matpel')
BEGIN
    DECLARE @reftable_38 nvarchar(60), @constraintname_38 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.group_matpel'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_38, @constraintname_38
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_38+' drop constraint '+@constraintname_38)
        FETCH NEXT from refcursor into @reftable_38, @constraintname_38
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[group_matpel]
END

CREATE TABLE [ref].[group_matpel]
(
    [gmp_id] CHAR(16) NOT NULL,
    [kurikulum_id] SMALLINT(2,0) NOT NULL,
    [tingkat_pendidikan_id] NUMERIC(4,0) NOT NULL,
    [nama_group] VARCHAR(40) NOT NULL,
    [jumlah_jam_maksimum] NUMERIC(4,0) NOT NULL,
    [jumlah_sks_maksimum] NUMERIC(4,0) DEFAULT ((0)) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [group_matpel_PK] PRIMARY KEY ([gmp_id])
);

CREATE INDEX [GMP_KURIKULUM_FK] ON [ref].[group_matpel] ([kurikulum_id]);

CREATE INDEX [GMP_TINGKAT_FK] ON [ref].[group_matpel] ([tingkat_pendidikan_id]);

CREATE INDEX [PK__group_ma__2D5EE9E04DA8F9FF] ON [ref].[group_matpel] ([gmp_id]);

BEGIN
ALTER TABLE [ref].[group_matpel] ADD CONSTRAINT [FK__group_mat__kurik__0E04126B] FOREIGN KEY ([kurikulum_id]) REFERENCES [ref].[kurikulum] ([kurikulum_id])
END
;

BEGIN
ALTER TABLE [ref].[group_matpel] ADD CONSTRAINT [FK__group_mat__tingk__0EF836A4] FOREIGN KEY ([tingkat_pendidikan_id]) REFERENCES [ref].[tingkat_pendidikan] ([tingkat_pendidikan_id])
END
;

-----------------------------------------------------------------------
-- ref.penghasilan_orangtua_wali
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.penghasilan_orangtua_wali')
BEGIN
    DECLARE @reftable_39 nvarchar(60), @constraintname_39 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.penghasilan_orangtua_wali'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_39, @constraintname_39
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_39+' drop constraint '+@constraintname_39)
        FETCH NEXT from refcursor into @reftable_39, @constraintname_39
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[penghasilan_orangtua_wali]
END

CREATE TABLE [ref].[penghasilan_orangtua_wali]
(
    [penghasilan_orangtua_wali_id] INT NOT NULL,
    [nama] VARCHAR(40) NOT NULL,
    [batas_bawah] INT NOT NULL,
    [batas_atas] INT NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [penghasilan_orangtua_wali_PK] PRIMARY KEY ([penghasilan_orangtua_wali_id])
);

CREATE INDEX [PK__penghasi__AB3650D53CE056C3] ON [ref].[penghasilan_orangtua_wali] ([penghasilan_orangtua_wali_id]);

-----------------------------------------------------------------------
-- vld_penghargaan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_pengh__pengh__51A50FA1')
    ALTER TABLE [vld_penghargaan] DROP CONSTRAINT [FK__vld_pengh__pengh__51A50FA1];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_pengh__pengh__6CE315C2')
    ALTER TABLE [vld_penghargaan] DROP CONSTRAINT [FK__vld_pengh__pengh__6CE315C2];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_pengh__idtyp__6BEEF189')
    ALTER TABLE [vld_penghargaan] DROP CONSTRAINT [FK__vld_pengh__idtyp__6BEEF189];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_pengh__idtyp__50B0EB68')
    ALTER TABLE [vld_penghargaan] DROP CONSTRAINT [FK__vld_pengh__idtyp__50B0EB68];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_penghargaan')
BEGIN
    DECLARE @reftable_40 nvarchar(60), @constraintname_40 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_penghargaan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_40, @constraintname_40
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_40+' drop constraint '+@constraintname_40)
        FETCH NEXT from refcursor into @reftable_40, @constraintname_40
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_penghargaan]
END

CREATE TABLE [vld_penghargaan]
(
    [penghargaan_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_penghargaan_PK] PRIMARY KEY ([penghargaan_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_penghargaan] ([idtype]);

CREATE INDEX [VLD_PENGHARG_LU] ON [vld_penghargaan] ([last_update]);

CREATE INDEX [PK__vld_peng__B8B2187799FC3C45] ON [vld_penghargaan] ([penghargaan_id],[logid]);

BEGIN
ALTER TABLE [vld_penghargaan] ADD CONSTRAINT [FK__vld_pengh__pengh__51A50FA1] FOREIGN KEY ([penghargaan_id]) REFERENCES [penghargaan] ([penghargaan_id])
END
;

BEGIN
ALTER TABLE [vld_penghargaan] ADD CONSTRAINT [FK__vld_pengh__pengh__6CE315C2] FOREIGN KEY ([penghargaan_id]) REFERENCES [penghargaan] ([penghargaan_id])
END
;

BEGIN
ALTER TABLE [vld_penghargaan] ADD CONSTRAINT [FK__vld_pengh__idtyp__6BEEF189] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_penghargaan] ADD CONSTRAINT [FK__vld_pengh__idtyp__50B0EB68] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- ref.jenis_sarana
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.jenis_sarana')
BEGIN
    DECLARE @reftable_41 nvarchar(60), @constraintname_41 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.jenis_sarana'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_41, @constraintname_41
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_41+' drop constraint '+@constraintname_41)
        FETCH NEXT from refcursor into @reftable_41, @constraintname_41
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[jenis_sarana]
END

CREATE TABLE [ref].[jenis_sarana]
(
    [jenis_sarana_id] INT NOT NULL,
    [nama] VARCHAR(30) NOT NULL,
    [kelompok] VARCHAR(50) NULL,
    [perlu_penempatan] NUMERIC(3,0) NOT NULL,
    [keterangan] VARCHAR(128) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [jenis_sarana_PK] PRIMARY KEY ([jenis_sarana_id])
);

CREATE INDEX [PK__jenis_sa__8180C3C292C9DCF8] ON [ref].[jenis_sarana] ([jenis_sarana_id]);

-----------------------------------------------------------------------
-- sarana_longitudinal
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sarana_lo__saran__18A19C6F')
    ALTER TABLE [sarana_longitudinal] DROP CONSTRAINT [FK__sarana_lo__saran__18A19C6F];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sarana_lo__saran__33DFA290')
    ALTER TABLE [sarana_longitudinal] DROP CONSTRAINT [FK__sarana_lo__saran__33DFA290];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sarana_lo__semes__1995C0A8')
    ALTER TABLE [sarana_longitudinal] DROP CONSTRAINT [FK__sarana_lo__semes__1995C0A8];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sarana_lo__semes__34D3C6C9')
    ALTER TABLE [sarana_longitudinal] DROP CONSTRAINT [FK__sarana_lo__semes__34D3C6C9];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'sarana_longitudinal')
BEGIN
    DECLARE @reftable_42 nvarchar(60), @constraintname_42 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'sarana_longitudinal'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_42, @constraintname_42
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_42+' drop constraint '+@constraintname_42)
        FETCH NEXT from refcursor into @reftable_42, @constraintname_42
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [sarana_longitudinal]
END

CREATE TABLE [sarana_longitudinal]
(
    [sarana_id] CHAR(16) NOT NULL,
    [semester_id] CHAR(5) NOT NULL,
    [jumlah] INT NOT NULL,
    [status_kelaikan] NUMERIC(3,0) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [sarana_longitudinal_PK] PRIMARY KEY ([sarana_id],[semester_id])
);

CREATE INDEX [SARANA_SEM_FK] ON [sarana_longitudinal] ([semester_id]);

CREATE INDEX [SARLONG_LU] ON [sarana_longitudinal] ([Last_update]);

CREATE INDEX [PK__sarana_l__A865419F23DA56A3] ON [sarana_longitudinal] ([sarana_id],[semester_id]);

BEGIN
ALTER TABLE [sarana_longitudinal] ADD CONSTRAINT [FK__sarana_lo__saran__18A19C6F] FOREIGN KEY ([sarana_id]) REFERENCES [sarana] ([sarana_id])
END
;

BEGIN
ALTER TABLE [sarana_longitudinal] ADD CONSTRAINT [FK__sarana_lo__saran__33DFA290] FOREIGN KEY ([sarana_id]) REFERENCES [sarana] ([sarana_id])
END
;

BEGIN
ALTER TABLE [sarana_longitudinal] ADD CONSTRAINT [FK__sarana_lo__semes__1995C0A8] FOREIGN KEY ([semester_id]) REFERENCES [ref].[semester] ([semester_id])
END
;

BEGIN
ALTER TABLE [sarana_longitudinal] ADD CONSTRAINT [FK__sarana_lo__semes__34D3C6C9] FOREIGN KEY ([semester_id]) REFERENCES [ref].[semester] ([semester_id])
END
;

-----------------------------------------------------------------------
-- sekolah
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sekolah__bentuk___07420643')
    ALTER TABLE [sekolah] DROP CONSTRAINT [FK__sekolah__bentuk___07420643];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sekolah__bentuk___22800C64')
    ALTER TABLE [sekolah] DROP CONSTRAINT [FK__sekolah__bentuk___22800C64];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sekolah__kebutuh__064DE20A')
    ALTER TABLE [sekolah] DROP CONSTRAINT [FK__sekolah__kebutuh__064DE20A];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sekolah__kebutuh__218BE82B')
    ALTER TABLE [sekolah] DROP CONSTRAINT [FK__sekolah__kebutuh__218BE82B];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sekolah__kode_wi__0559BDD1')
    ALTER TABLE [sekolah] DROP CONSTRAINT [FK__sekolah__kode_wi__0559BDD1];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sekolah__kode_wi__2097C3F2')
    ALTER TABLE [sekolah] DROP CONSTRAINT [FK__sekolah__kode_wi__2097C3F2];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sekolah__status___092A4EB5')
    ALTER TABLE [sekolah] DROP CONSTRAINT [FK__sekolah__status___092A4EB5];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sekolah__status___246854D6')
    ALTER TABLE [sekolah] DROP CONSTRAINT [FK__sekolah__status___246854D6];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'sekolah')
BEGIN
    DECLARE @reftable_43 nvarchar(60), @constraintname_43 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'sekolah'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_43, @constraintname_43
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_43+' drop constraint '+@constraintname_43)
        FETCH NEXT from refcursor into @reftable_43, @constraintname_43
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [sekolah]
END

CREATE TABLE [sekolah]
(
    [sekolah_id] CHAR(16) NOT NULL,
    [nama] VARCHAR(80) NOT NULL,
    [nama_nomenklatur] VARCHAR(80) NULL,
    [nss] CHAR(12) NULL,
    [npsn] CHAR(8) NULL,
    [bentuk_pendidikan_id] SMALLINT(2,0) NOT NULL,
    [alamat_jalan] VARCHAR(80) NOT NULL,
    [rt] NUMERIC(4,0) NULL,
    [rw] NUMERIC(4,0) NULL,
    [nama_dusun] VARCHAR(40) NULL,
    [desa_kelurahan] VARCHAR(40) NOT NULL,
    [kode_wilayah] CHAR(8) NOT NULL,
    [kode_pos] CHAR(5) NULL,
    [lintang] NUMERIC(12,6) NULL,
    [bujur] NUMERIC(12,6) NULL,
    [nomor_telepon] VARCHAR(20) NULL,
    [nomor_fax] VARCHAR(20) NULL,
    [email] VARCHAR(50) NULL,
    [website] VARCHAR(100) NULL,
    [kebutuhan_khusus_id] INT NOT NULL,
    [status_sekolah] NUMERIC(3,0) NOT NULL,
    [sk_pendirian_sekolah] VARCHAR(40) NULL,
    [tanggal_sk_pendirian] VARCHAR(20) NULL,
    [status_kepemilikan_id] NUMERIC(3,0) NOT NULL,
    [yayasan_id] CHAR(16) NULL,
    [sk_izin_operasional] VARCHAR(40) NULL,
    [tanggal_sk_izin_operasional] VARCHAR(20) NULL,
    [no_rekening] VARCHAR(20) NULL,
    [nama_bank] VARCHAR(20) NULL,
    [cabang_kcp_unit] VARCHAR(40) NULL,
    [rekening_atas_nama] VARCHAR(50) NULL,
    [mbs] NUMERIC(3,0) NOT NULL,
    [luas_tanah_milik] NUMERIC(9,0) NOT NULL,
    [luas_tanah_bukan_milik] NUMERIC(9,0) NOT NULL,
    [kode_registrasi] BIGINT(8,0) NULL,
    [flag] CHAR(1) NULL,
    [pic_id] CHAR(16) NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [sekolah_PK] PRIMARY KEY ([sekolah_id])
);

CREATE INDEX [LEMBAGA_KECAMATAN_FK] ON [sekolah] ([kode_wilayah]);

CREATE INDEX [SEKOLAH_BENTUK_FK] ON [sekolah] ([bentuk_pendidikan_id]);

CREATE INDEX [SEKOLAH_DEL] ON [sekolah] ([Soft_delete]);

CREATE INDEX [SEKOLAH_LAYANAN_KK_FK] ON [sekolah] ([kebutuhan_khusus_id]);

CREATE INDEX [SEKOLAH_LU] ON [sekolah] ([Last_update]);

CREATE INDEX [SEKOLAH_ST_KEPEMILIKAN_FK] ON [sekolah] ([status_kepemilikan_id]);

CREATE INDEX [SP_PIC_FK] ON [sekolah] ([pic_id]);

CREATE INDEX [SP_YAYASAN_FK] ON [sekolah] ([yayasan_id]);

CREATE INDEX [PK__sekolah__F986A9DFC4AA01DE] ON [sekolah] ([sekolah_id]);

BEGIN
ALTER TABLE [sekolah] ADD CONSTRAINT [FK__sekolah__bentuk___07420643] FOREIGN KEY ([bentuk_pendidikan_id]) REFERENCES [ref].[bentuk_pendidikan] ([bentuk_pendidikan_id])
END
;

BEGIN
ALTER TABLE [sekolah] ADD CONSTRAINT [FK__sekolah__bentuk___22800C64] FOREIGN KEY ([bentuk_pendidikan_id]) REFERENCES [ref].[bentuk_pendidikan] ([bentuk_pendidikan_id])
END
;

BEGIN
ALTER TABLE [sekolah] ADD CONSTRAINT [FK__sekolah__kebutuh__064DE20A] FOREIGN KEY ([kebutuhan_khusus_id]) REFERENCES [ref].[kebutuhan_khusus] ([kebutuhan_khusus_id])
END
;

BEGIN
ALTER TABLE [sekolah] ADD CONSTRAINT [FK__sekolah__kebutuh__218BE82B] FOREIGN KEY ([kebutuhan_khusus_id]) REFERENCES [ref].[kebutuhan_khusus] ([kebutuhan_khusus_id])
END
;

BEGIN
ALTER TABLE [sekolah] ADD CONSTRAINT [FK__sekolah__kode_wi__0559BDD1] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

BEGIN
ALTER TABLE [sekolah] ADD CONSTRAINT [FK__sekolah__kode_wi__2097C3F2] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

BEGIN
ALTER TABLE [sekolah] ADD CONSTRAINT [FK__sekolah__status___092A4EB5] FOREIGN KEY ([status_kepemilikan_id]) REFERENCES [ref].[status_kepemilikan] ([status_kepemilikan_id])
END
;

BEGIN
ALTER TABLE [sekolah] ADD CONSTRAINT [FK__sekolah__status___246854D6] FOREIGN KEY ([status_kepemilikan_id]) REFERENCES [ref].[status_kepemilikan] ([status_kepemilikan_id])
END
;

-----------------------------------------------------------------------
-- ref.errortype
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.errortype')
BEGIN
    DECLARE @reftable_44 nvarchar(60), @constraintname_44 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.errortype'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_44, @constraintname_44
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_44+' drop constraint '+@constraintname_44)
        FETCH NEXT from refcursor into @reftable_44, @constraintname_44
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[errortype]
END

CREATE TABLE [ref].[errortype]
(
    [idtype] INT NOT NULL,
    [kategori_error] INT NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [errortype_PK] PRIMARY KEY ([idtype])
);

CREATE INDEX [ERRORTYPE_LU] ON [ref].[errortype] ([last_update]);

CREATE INDEX [PK__errortyp__4218320246C2B669] ON [ref].[errortype] ([idtype]);

-----------------------------------------------------------------------
-- peserta_didik_baru
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__peser__1A89E4E1')
    ALTER TABLE [peserta_didik_baru] DROP CONSTRAINT [FK__peserta_d__peser__1A89E4E1];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__peser__35C7EB02')
    ALTER TABLE [peserta_didik_baru] DROP CONSTRAINT [FK__peserta_d__peser__35C7EB02];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__sekol__37B03374')
    ALTER TABLE [peserta_didik_baru] DROP CONSTRAINT [FK__peserta_d__sekol__37B03374];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__sekol__1C722D53')
    ALTER TABLE [peserta_didik_baru] DROP CONSTRAINT [FK__peserta_d__sekol__1C722D53];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__jenis__1B7E091A')
    ALTER TABLE [peserta_didik_baru] DROP CONSTRAINT [FK__peserta_d__jenis__1B7E091A];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__jenis__36BC0F3B')
    ALTER TABLE [peserta_didik_baru] DROP CONSTRAINT [FK__peserta_d__jenis__36BC0F3B];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__tahun__1D66518C')
    ALTER TABLE [peserta_didik_baru] DROP CONSTRAINT [FK__peserta_d__tahun__1D66518C];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__peserta_d__tahun__38A457AD')
    ALTER TABLE [peserta_didik_baru] DROP CONSTRAINT [FK__peserta_d__tahun__38A457AD];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'peserta_didik_baru')
BEGIN
    DECLARE @reftable_45 nvarchar(60), @constraintname_45 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'peserta_didik_baru'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_45, @constraintname_45
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_45+' drop constraint '+@constraintname_45)
        FETCH NEXT from refcursor into @reftable_45, @constraintname_45
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [peserta_didik_baru]
END

CREATE TABLE [peserta_didik_baru]
(
    [pdb_id] CHAR(16) NOT NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [tahun_ajaran_id] NUMERIC(6,0) NOT NULL,
    [nama_pd] VARCHAR(50) NOT NULL,
    [jenis_kelamin] CHAR(1) NOT NULL,
    [nisn] CHAR(10) NULL,
    [nik] CHAR(16) NULL,
    [tempat_lahir] VARCHAR(20) NULL,
    [tanggal_lahir] VARCHAR(20) NOT NULL,
    [nama_ibu_kandung] VARCHAR(50) NOT NULL,
    [jenis_pendaftaran_id] NUMERIC(3,0) NOT NULL,
    [sudah_diproses] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [berhasil_diproses] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [peserta_didik_id] CHAR(16) NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [peserta_didik_baru_PK] PRIMARY KEY ([pdb_id])
);

CREATE INDEX [PD_BARU_PD_FK] ON [peserta_didik_baru] ([peserta_didik_id]);

CREATE INDEX [PDB_JENISDAFTAR_FK] ON [peserta_didik_baru] ([jenis_pendaftaran_id]);

CREATE INDEX [PDB_LU] ON [peserta_didik_baru] ([Last_update]);

CREATE INDEX [PDB_SEKOLAH_FK] ON [peserta_didik_baru] ([sekolah_id]);

CREATE INDEX [PDB_TAHUNAJARAN_FK] ON [peserta_didik_baru] ([tahun_ajaran_id]);

CREATE INDEX [PK__peserta___3BA79A4572004CD1] ON [peserta_didik_baru] ([pdb_id]);

BEGIN
ALTER TABLE [peserta_didik_baru] ADD CONSTRAINT [FK__peserta_d__peser__1A89E4E1] FOREIGN KEY ([peserta_didik_id]) REFERENCES [peserta_didik] ([peserta_didik_id])
END
;

BEGIN
ALTER TABLE [peserta_didik_baru] ADD CONSTRAINT [FK__peserta_d__peser__35C7EB02] FOREIGN KEY ([peserta_didik_id]) REFERENCES [peserta_didik] ([peserta_didik_id])
END
;

BEGIN
ALTER TABLE [peserta_didik_baru] ADD CONSTRAINT [FK__peserta_d__sekol__37B03374] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [peserta_didik_baru] ADD CONSTRAINT [FK__peserta_d__sekol__1C722D53] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [peserta_didik_baru] ADD CONSTRAINT [FK__peserta_d__jenis__1B7E091A] FOREIGN KEY ([jenis_pendaftaran_id]) REFERENCES [ref].[jenis_pendaftaran] ([jenis_pendaftaran_id])
END
;

BEGIN
ALTER TABLE [peserta_didik_baru] ADD CONSTRAINT [FK__peserta_d__jenis__36BC0F3B] FOREIGN KEY ([jenis_pendaftaran_id]) REFERENCES [ref].[jenis_pendaftaran] ([jenis_pendaftaran_id])
END
;

BEGIN
ALTER TABLE [peserta_didik_baru] ADD CONSTRAINT [FK__peserta_d__tahun__1D66518C] FOREIGN KEY ([tahun_ajaran_id]) REFERENCES [ref].[tahun_ajaran] ([tahun_ajaran_id])
END
;

BEGIN
ALTER TABLE [peserta_didik_baru] ADD CONSTRAINT [FK__peserta_d__tahun__38A457AD] FOREIGN KEY ([tahun_ajaran_id]) REFERENCES [ref].[tahun_ajaran] ([tahun_ajaran_id])
END
;

-----------------------------------------------------------------------
-- ref.jenis_tinggal
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.jenis_tinggal')
BEGIN
    DECLARE @reftable_46 nvarchar(60), @constraintname_46 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.jenis_tinggal'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_46, @constraintname_46
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_46+' drop constraint '+@constraintname_46)
        FETCH NEXT from refcursor into @reftable_46, @constraintname_46
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[jenis_tinggal]
END

CREATE TABLE [ref].[jenis_tinggal]
(
    [jenis_tinggal_id] NUMERIC(4,0) NOT NULL,
    [nama] VARCHAR(30) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [jenis_tinggal_PK] PRIMARY KEY ([jenis_tinggal_id])
);

CREATE INDEX [PK__jenis_ti__5F9419F8752199A0] ON [ref].[jenis_tinggal] ([jenis_tinggal_id]);

-----------------------------------------------------------------------
-- vld_pembelajaran
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_pembe__pembe__538D5813')
    ALTER TABLE [vld_pembelajaran] DROP CONSTRAINT [FK__vld_pembe__pembe__538D5813];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_pembe__pembe__6ECB5E34')
    ALTER TABLE [vld_pembelajaran] DROP CONSTRAINT [FK__vld_pembe__pembe__6ECB5E34];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_pembe__idtyp__6DD739FB')
    ALTER TABLE [vld_pembelajaran] DROP CONSTRAINT [FK__vld_pembe__idtyp__6DD739FB];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_pembe__idtyp__529933DA')
    ALTER TABLE [vld_pembelajaran] DROP CONSTRAINT [FK__vld_pembe__idtyp__529933DA];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_pembelajaran')
BEGIN
    DECLARE @reftable_47 nvarchar(60), @constraintname_47 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_pembelajaran'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_47, @constraintname_47
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_47+' drop constraint '+@constraintname_47)
        FETCH NEXT from refcursor into @reftable_47, @constraintname_47
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_pembelajaran]
END

CREATE TABLE [vld_pembelajaran]
(
    [pembelajaran_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_pembelajaran_PK] PRIMARY KEY ([pembelajaran_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_pembelajaran] ([idtype]);

CREATE INDEX [PK__vld_pemb__D58C2DCDBD530C53] ON [vld_pembelajaran] ([pembelajaran_id],[logid]);

BEGIN
ALTER TABLE [vld_pembelajaran] ADD CONSTRAINT [FK__vld_pembe__pembe__538D5813] FOREIGN KEY ([pembelajaran_id]) REFERENCES [pembelajaran] ([pembelajaran_id])
END
;

BEGIN
ALTER TABLE [vld_pembelajaran] ADD CONSTRAINT [FK__vld_pembe__pembe__6ECB5E34] FOREIGN KEY ([pembelajaran_id]) REFERENCES [pembelajaran] ([pembelajaran_id])
END
;

BEGIN
ALTER TABLE [vld_pembelajaran] ADD CONSTRAINT [FK__vld_pembe__idtyp__6DD739FB] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_pembelajaran] ADD CONSTRAINT [FK__vld_pembe__idtyp__529933DA] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- ref.bidang_studi
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__bidang_st__kelom__753864A1')
    ALTER TABLE [ref].[bidang_studi] DROP CONSTRAINT [FK__bidang_st__kelom__753864A1];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.bidang_studi')
BEGIN
    DECLARE @reftable_48 nvarchar(60), @constraintname_48 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.bidang_studi'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_48, @constraintname_48
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_48+' drop constraint '+@constraintname_48)
        FETCH NEXT from refcursor into @reftable_48, @constraintname_48
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[bidang_studi]
END

CREATE TABLE [ref].[bidang_studi]
(
    [bidang_studi_id] INT NOT NULL,
    [kelompok_bidang_studi_id] INT NULL,
    [kode] CHAR(3) NULL,
    [bidang_studi] VARCHAR(40) NOT NULL,
    [kelompok] NUMERIC(3,0) NOT NULL,
    [jenjang_paud] NUMERIC(3,0) NOT NULL,
    [jenjang_tk] NUMERIC(3,0) NOT NULL,
    [jenjang_sd] NUMERIC(3,0) NOT NULL,
    [jenjang_smp] NUMERIC(3,0) NOT NULL,
    [jenjang_sma] NUMERIC(3,0) NOT NULL,
    [jenjang_tinggi] NUMERIC(3,0) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [bidang_studi_PK] PRIMARY KEY ([bidang_studi_id])
);

CREATE INDEX [KELOMPOK_FK] ON [ref].[bidang_studi] ([kelompok_bidang_studi_id]);

CREATE INDEX [PK__bidang_s__433420596A1E9C73] ON [ref].[bidang_studi] ([bidang_studi_id]);

BEGIN
ALTER TABLE [ref].[bidang_studi] ADD CONSTRAINT [FK__bidang_st__kelom__753864A1] FOREIGN KEY ([kelompok_bidang_studi_id]) REFERENCES [ref].[bidang_studi] ([bidang_studi_id])
END
;

-----------------------------------------------------------------------
-- inpassing
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__inpassing__ptk_i__0B129727')
    ALTER TABLE [inpassing] DROP CONSTRAINT [FK__inpassing__ptk_i__0B129727];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__inpassing__ptk_i__26509D48')
    ALTER TABLE [inpassing] DROP CONSTRAINT [FK__inpassing__ptk_i__26509D48];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__inpassing__pangk__0A1E72EE')
    ALTER TABLE [inpassing] DROP CONSTRAINT [FK__inpassing__pangk__0A1E72EE];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__inpassing__pangk__255C790F')
    ALTER TABLE [inpassing] DROP CONSTRAINT [FK__inpassing__pangk__255C790F];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'inpassing')
BEGIN
    DECLARE @reftable_49 nvarchar(60), @constraintname_49 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'inpassing'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_49, @constraintname_49
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_49+' drop constraint '+@constraintname_49)
        FETCH NEXT from refcursor into @reftable_49, @constraintname_49
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [inpassing]
END

CREATE TABLE [inpassing]
(
    [inpassing_id] CHAR(16) NOT NULL,
    [pangkat_golongan_id] NUMERIC(4,0) NOT NULL,
    [ptk_id] CHAR(16) NULL,
    [no_sk_inpassing] VARCHAR(40) NOT NULL,
    [tmt_inpassing] VARCHAR(20) NOT NULL,
    [angka_kredit] NUMERIC(5,0) DEFAULT ((0)) NOT NULL,
    [masa_kerja_tahun] NUMERIC(4,0) DEFAULT ((0)) NOT NULL,
    [masa_kerja_bulan] NUMERIC(4,0) DEFAULT ((0)) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [inpassing_PK] PRIMARY KEY ([inpassing_id])
);

CREATE INDEX [INPASSING_LU] ON [inpassing] ([Last_update]);

CREATE INDEX [INPASSING_PTK_FK] ON [inpassing] ([ptk_id]);

CREATE INDEX [INPASSING_SETARA_FK] ON [inpassing] ([pangkat_golongan_id]);

CREATE INDEX [PK__inpassin__F4ECA9CE68927AFF] ON [inpassing] ([inpassing_id]);

BEGIN
ALTER TABLE [inpassing] ADD CONSTRAINT [FK__inpassing__ptk_i__0B129727] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [inpassing] ADD CONSTRAINT [FK__inpassing__ptk_i__26509D48] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [inpassing] ADD CONSTRAINT [FK__inpassing__pangk__0A1E72EE] FOREIGN KEY ([pangkat_golongan_id]) REFERENCES [ref].[pangkat_golongan] ([pangkat_golongan_id])
END
;

BEGIN
ALTER TABLE [inpassing] ADD CONSTRAINT [FK__inpassing__pangk__255C790F] FOREIGN KEY ([pangkat_golongan_id]) REFERENCES [ref].[pangkat_golongan] ([pangkat_golongan_id])
END
;

-----------------------------------------------------------------------
-- ref.batas_waktu_rapor
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__batas_wak__semes__0FEC5ADD')
    ALTER TABLE [ref].[batas_waktu_rapor] DROP CONSTRAINT [FK__batas_wak__semes__0FEC5ADD];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.batas_waktu_rapor')
BEGIN
    DECLARE @reftable_50 nvarchar(60), @constraintname_50 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.batas_waktu_rapor'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_50, @constraintname_50
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_50+' drop constraint '+@constraintname_50)
        FETCH NEXT from refcursor into @reftable_50, @constraintname_50
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[batas_waktu_rapor]
END

CREATE TABLE [ref].[batas_waktu_rapor]
(
    [semester_id] CHAR(5) NOT NULL,
    [tgl_rapor_mulai] VARCHAR(20) NOT NULL,
    [tgl_rapor_selesai] VARCHAR(20) NOT NULL,
    [tgl_usm_mulai] VARCHAR(20) NULL,
    [tgl_usm_selesai] VARCHAR(20) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [batas_waktu_rapor_PK] PRIMARY KEY ([semester_id])
);

CREATE INDEX [PK__batas_wa__CBC81B015A5CDEC8] ON [ref].[batas_waktu_rapor] ([semester_id]);

BEGIN
ALTER TABLE [ref].[batas_waktu_rapor] ADD CONSTRAINT [FK__batas_wak__semes__0FEC5ADD] FOREIGN KEY ([semester_id]) REFERENCES [ref].[semester] ([semester_id])
END
;

-----------------------------------------------------------------------
-- ref.jenis_keluar
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.jenis_keluar')
BEGIN
    DECLARE @reftable_51 nvarchar(60), @constraintname_51 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.jenis_keluar'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_51, @constraintname_51
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_51+' drop constraint '+@constraintname_51)
        FETCH NEXT from refcursor into @reftable_51, @constraintname_51
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[jenis_keluar]
END

CREATE TABLE [ref].[jenis_keluar]
(
    [jenis_keluar_id] CHAR(1) NOT NULL,
    [ket_keluar] VARCHAR(40) NOT NULL,
    [keluar_pd] NUMERIC(3,0) NOT NULL,
    [keluar_ptk] NUMERIC(3,0) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [jenis_keluar_PK] PRIMARY KEY ([jenis_keluar_id])
);

CREATE INDEX [PK__jenis_ke__3FB3E8957E19DAF2] ON [ref].[jenis_keluar] ([jenis_keluar_id]);

-----------------------------------------------------------------------
-- vld_pd_long
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_pd_long__5575A085')
    ALTER TABLE [vld_pd_long] DROP CONSTRAINT [FK__vld_pd_long__5575A085];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_pd_long__70B3A6A6')
    ALTER TABLE [vld_pd_long] DROP CONSTRAINT [FK__vld_pd_long__70B3A6A6];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_pd_lo__idtyp__6FBF826D')
    ALTER TABLE [vld_pd_long] DROP CONSTRAINT [FK__vld_pd_lo__idtyp__6FBF826D];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_pd_lo__idtyp__54817C4C')
    ALTER TABLE [vld_pd_long] DROP CONSTRAINT [FK__vld_pd_lo__idtyp__54817C4C];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_pd_long')
BEGIN
    DECLARE @reftable_52 nvarchar(60), @constraintname_52 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_pd_long'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_52, @constraintname_52
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_52+' drop constraint '+@constraintname_52)
        FETCH NEXT from refcursor into @reftable_52, @constraintname_52
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_pd_long]
END

CREATE TABLE [vld_pd_long]
(
    [peserta_didik_id] CHAR(16) NOT NULL,
    [semester_id] CHAR(5) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_pd_long_PK] PRIMARY KEY ([peserta_didik_id],[semester_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_pd_long] ([idtype]);

CREATE INDEX [VLD_PD_LONG_LU] ON [vld_pd_long] ([last_update]);

CREATE INDEX [PK__vld_pd_l__9BD1F28ED65362A6] ON [vld_pd_long] ([peserta_didik_id],[semester_id],[logid]);

BEGIN
ALTER TABLE [vld_pd_long] ADD CONSTRAINT [FK__vld_pd_long__5575A085] FOREIGN KEY ([peserta_didik_id],[semester_id],[peserta_didik_id],[semester_id]) REFERENCES [peserta_didik_longitudinal] ([peserta_didik_id],[peserta_didik_id],[semester_id],[semester_id])
END
;

BEGIN
ALTER TABLE [vld_pd_long] ADD CONSTRAINT [FK__vld_pd_long__70B3A6A6] FOREIGN KEY ([peserta_didik_id],[semester_id],[peserta_didik_id],[semester_id]) REFERENCES [peserta_didik_longitudinal] ([peserta_didik_id],[peserta_didik_id],[semester_id],[semester_id])
END
;

BEGIN
ALTER TABLE [vld_pd_long] ADD CONSTRAINT [FK__vld_pd_lo__idtyp__6FBF826D] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_pd_long] ADD CONSTRAINT [FK__vld_pd_lo__idtyp__54817C4C] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- ref.mata_pelajaran
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.mata_pelajaran')
BEGIN
    DECLARE @reftable_53 nvarchar(60), @constraintname_53 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.mata_pelajaran'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_53, @constraintname_53
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_53+' drop constraint '+@constraintname_53)
        FETCH NEXT from refcursor into @reftable_53, @constraintname_53
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[mata_pelajaran]
END

CREATE TABLE [ref].[mata_pelajaran]
(
    [mata_pelajaran_id] INT NOT NULL,
    [nama] VARCHAR(50) NOT NULL,
    [pilihan_sekolah] NUMERIC(3,0) NOT NULL,
    [pilihan_buku] NUMERIC(3,0) NOT NULL,
    [pilihan_kepengawasan] NUMERIC(3,0) NOT NULL,
    [pilihan_evaluasi] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [mata_pelajaran_PK] PRIMARY KEY ([mata_pelajaran_id])
);

CREATE INDEX [MATPEL_LU] ON [ref].[mata_pelajaran] ([last_update]);

CREATE INDEX [PK__mata_pel__0CCCBA28753F8A04] ON [ref].[mata_pelajaran] ([mata_pelajaran_id]);

-----------------------------------------------------------------------
-- sarana
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sarana__prasaran__2136E270')
    ALTER TABLE [sarana] DROP CONSTRAINT [FK__sarana__prasaran__2136E270];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sarana__prasaran__3C74E891')
    ALTER TABLE [sarana] DROP CONSTRAINT [FK__sarana__prasaran__3C74E891];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sarana__sekolah___3A8CA01F')
    ALTER TABLE [sarana] DROP CONSTRAINT [FK__sarana__sekolah___3A8CA01F];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sarana__sekolah___1F4E99FE')
    ALTER TABLE [sarana] DROP CONSTRAINT [FK__sarana__sekolah___1F4E99FE];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sarana__jenis_sa__2042BE37')
    ALTER TABLE [sarana] DROP CONSTRAINT [FK__sarana__jenis_sa__2042BE37];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sarana__jenis_sa__3B80C458')
    ALTER TABLE [sarana] DROP CONSTRAINT [FK__sarana__jenis_sa__3B80C458];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sarana__kepemili__1E5A75C5')
    ALTER TABLE [sarana] DROP CONSTRAINT [FK__sarana__kepemili__1E5A75C5];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sarana__kepemili__39987BE6')
    ALTER TABLE [sarana] DROP CONSTRAINT [FK__sarana__kepemili__39987BE6];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'sarana')
BEGIN
    DECLARE @reftable_54 nvarchar(60), @constraintname_54 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'sarana'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_54, @constraintname_54
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_54+' drop constraint '+@constraintname_54)
        FETCH NEXT from refcursor into @reftable_54, @constraintname_54
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [sarana]
END

CREATE TABLE [sarana]
(
    [sarana_id] CHAR(16) NOT NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [jenis_sarana_id] INT NOT NULL,
    [prasarana_id] CHAR(16) NULL,
    [kepemilikan_sarpras_id] NUMERIC(3,0) NOT NULL,
    [nama] VARCHAR(50) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [sarana_PK] PRIMARY KEY ([sarana_id])
);

CREATE INDEX [LETAK_SARANA_FK] ON [sarana] ([prasarana_id]);

CREATE INDEX [SARANA_DEL] ON [sarana] ([Soft_delete]);

CREATE INDEX [SARANA_JENIS_FK] ON [sarana] ([jenis_sarana_id]);

CREATE INDEX [SARANA_LU] ON [sarana] ([Last_update]);

CREATE INDEX [SARANA_MILIK_FK] ON [sarana] ([kepemilikan_sarpras_id]);

CREATE INDEX [SARANA_SEKOLAH_FK] ON [sarana] ([sekolah_id]);

CREATE INDEX [PK__sarana__A4D9C02E696A4359] ON [sarana] ([sarana_id]);

BEGIN
ALTER TABLE [sarana] ADD CONSTRAINT [FK__sarana__prasaran__2136E270] FOREIGN KEY ([prasarana_id]) REFERENCES [prasarana] ([prasarana_id])
END
;

BEGIN
ALTER TABLE [sarana] ADD CONSTRAINT [FK__sarana__prasaran__3C74E891] FOREIGN KEY ([prasarana_id]) REFERENCES [prasarana] ([prasarana_id])
END
;

BEGIN
ALTER TABLE [sarana] ADD CONSTRAINT [FK__sarana__sekolah___3A8CA01F] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [sarana] ADD CONSTRAINT [FK__sarana__sekolah___1F4E99FE] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [sarana] ADD CONSTRAINT [FK__sarana__jenis_sa__2042BE37] FOREIGN KEY ([jenis_sarana_id]) REFERENCES [ref].[jenis_sarana] ([jenis_sarana_id])
END
;

BEGIN
ALTER TABLE [sarana] ADD CONSTRAINT [FK__sarana__jenis_sa__3B80C458] FOREIGN KEY ([jenis_sarana_id]) REFERENCES [ref].[jenis_sarana] ([jenis_sarana_id])
END
;

BEGIN
ALTER TABLE [sarana] ADD CONSTRAINT [FK__sarana__kepemili__1E5A75C5] FOREIGN KEY ([kepemilikan_sarpras_id]) REFERENCES [ref].[status_kepemilikan_sarpras] ([kepemilikan_sarpras_id])
END
;

BEGIN
ALTER TABLE [sarana] ADD CONSTRAINT [FK__sarana__kepemili__39987BE6] FOREIGN KEY ([kepemilikan_sarpras_id]) REFERENCES [ref].[status_kepemilikan_sarpras] ([kepemilikan_sarpras_id])
END
;

-----------------------------------------------------------------------
-- akreditasi_sp
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__akreditas__sekol__292D09F3')
    ALTER TABLE [akreditasi_sp] DROP CONSTRAINT [FK__akreditas__sekol__292D09F3];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__akreditas__sekol__0DEF03D2')
    ALTER TABLE [akreditasi_sp] DROP CONSTRAINT [FK__akreditas__sekol__0DEF03D2];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__akreditas__akred__0C06BB60')
    ALTER TABLE [akreditasi_sp] DROP CONSTRAINT [FK__akreditas__akred__0C06BB60];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__akreditas__akred__2744C181')
    ALTER TABLE [akreditasi_sp] DROP CONSTRAINT [FK__akreditas__akred__2744C181];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__akreditas__la_id__0CFADF99')
    ALTER TABLE [akreditasi_sp] DROP CONSTRAINT [FK__akreditas__la_id__0CFADF99];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__akreditas__la_id__2838E5BA')
    ALTER TABLE [akreditasi_sp] DROP CONSTRAINT [FK__akreditas__la_id__2838E5BA];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'akreditasi_sp')
BEGIN
    DECLARE @reftable_55 nvarchar(60), @constraintname_55 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'akreditasi_sp'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_55, @constraintname_55
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_55+' drop constraint '+@constraintname_55)
        FETCH NEXT from refcursor into @reftable_55, @constraintname_55
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [akreditasi_sp]
END

CREATE TABLE [akreditasi_sp]
(
    [akred_sp_id] CHAR(16) NOT NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [akred_sp_sk] VARCHAR(40) NOT NULL,
    [akred_sp_tmt] VARCHAR(20) NOT NULL,
    [akred_sp_tst] VARCHAR(20) NOT NULL,
    [akreditasi_id] NUMERIC(3,0) NOT NULL,
    [la_id] CHAR(5) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [akreditasi_sp_PK] PRIMARY KEY ([akred_sp_id])
);

CREATE INDEX [AKRED_SP_LA_FK] ON [akreditasi_sp] ([la_id]);

CREATE INDEX [AKRED_SP_NILAI_FK] ON [akreditasi_sp] ([akreditasi_id]);

CREATE INDEX [SP_AKRED_FK] ON [akreditasi_sp] ([sekolah_id]);

CREATE INDEX [PK__akredita__43BF883FD60374A3] ON [akreditasi_sp] ([akred_sp_id]);

BEGIN
ALTER TABLE [akreditasi_sp] ADD CONSTRAINT [FK__akreditas__sekol__292D09F3] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [akreditasi_sp] ADD CONSTRAINT [FK__akreditas__sekol__0DEF03D2] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [akreditasi_sp] ADD CONSTRAINT [FK__akreditas__akred__0C06BB60] FOREIGN KEY ([akreditasi_id]) REFERENCES [ref].[akreditasi] ([akreditasi_id])
END
;

BEGIN
ALTER TABLE [akreditasi_sp] ADD CONSTRAINT [FK__akreditas__akred__2744C181] FOREIGN KEY ([akreditasi_id]) REFERENCES [ref].[akreditasi] ([akreditasi_id])
END
;

BEGIN
ALTER TABLE [akreditasi_sp] ADD CONSTRAINT [FK__akreditas__la_id__0CFADF99] FOREIGN KEY ([la_id]) REFERENCES [ref].[lembaga_akreditasi] ([la_id])
END
;

BEGIN
ALTER TABLE [akreditasi_sp] ADD CONSTRAINT [FK__akreditas__la_id__2838E5BA] FOREIGN KEY ([la_id]) REFERENCES [ref].[lembaga_akreditasi] ([la_id])
END
;

-----------------------------------------------------------------------
-- pembelajaran
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pembelaja__ptk_t__222B06A9')
    ALTER TABLE [pembelajaran] DROP CONSTRAINT [FK__pembelaja__ptk_t__222B06A9];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pembelaja__ptk_t__3D690CCA')
    ALTER TABLE [pembelajaran] DROP CONSTRAINT [FK__pembelaja__ptk_t__3D690CCA];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pembelaja__rombo__24134F1B')
    ALTER TABLE [pembelajaran] DROP CONSTRAINT [FK__pembelaja__rombo__24134F1B];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pembelaja__rombo__3F51553C')
    ALTER TABLE [pembelajaran] DROP CONSTRAINT [FK__pembelaja__rombo__3F51553C];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pembelaja__mata___40457975')
    ALTER TABLE [pembelajaran] DROP CONSTRAINT [FK__pembelaja__mata___40457975];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pembelaja__mata___25077354')
    ALTER TABLE [pembelajaran] DROP CONSTRAINT [FK__pembelaja__mata___25077354];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pembelaja__semes__231F2AE2')
    ALTER TABLE [pembelajaran] DROP CONSTRAINT [FK__pembelaja__semes__231F2AE2];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pembelaja__semes__3E5D3103')
    ALTER TABLE [pembelajaran] DROP CONSTRAINT [FK__pembelaja__semes__3E5D3103];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'pembelajaran')
BEGIN
    DECLARE @reftable_56 nvarchar(60), @constraintname_56 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'pembelajaran'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_56, @constraintname_56
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_56+' drop constraint '+@constraintname_56)
        FETCH NEXT from refcursor into @reftable_56, @constraintname_56
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [pembelajaran]
END

CREATE TABLE [pembelajaran]
(
    [pembelajaran_id] CHAR(16) NOT NULL,
    [rombongan_belajar_id] CHAR(16) NOT NULL,
    [semester_id] CHAR(5) NOT NULL,
    [mata_pelajaran_id] INT NOT NULL,
    [ptk_terdaftar_id] CHAR(16) NOT NULL,
    [sk_mengajar] VARCHAR(40) NOT NULL,
    [tanggal_sk_mengajar] VARCHAR(20) NOT NULL,
    [jam_mengajar_per_minggu] NUMERIC(4,0) NOT NULL,
    [status_di_kurikulum] NUMERIC(3,0) NOT NULL,
    [nama_mata_pelajaran] VARCHAR(50) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [pembelajaran_PK] PRIMARY KEY ([pembelajaran_id])
);

CREATE INDEX [PEMBELAJARAN_DEL] ON [pembelajaran] ([Soft_delete],[rombongan_belajar_id]);

CREATE INDEX [PEMBELAJARAN_GURU_FK] ON [pembelajaran] ([ptk_terdaftar_id]);

CREATE INDEX [PEMBELAJARAN_LU] ON [pembelajaran] ([Last_update]);

CREATE INDEX [PEMBELAJARAN_MATPEL_FK] ON [pembelajaran] ([mata_pelajaran_id]);

CREATE INDEX [PEMBELAJARAN_ROMBEL_FK] ON [pembelajaran] ([rombongan_belajar_id]);

CREATE INDEX [PEMBELAJARAN_SEMESTER_FK] ON [pembelajaran] ([semester_id]);

CREATE INDEX [PK__pembelaj__920FA2EA1191B140] ON [pembelajaran] ([pembelajaran_id]);

BEGIN
ALTER TABLE [pembelajaran] ADD CONSTRAINT [FK__pembelaja__ptk_t__222B06A9] FOREIGN KEY ([ptk_terdaftar_id]) REFERENCES [ptk_terdaftar] ([ptk_terdaftar_id])
END
;

BEGIN
ALTER TABLE [pembelajaran] ADD CONSTRAINT [FK__pembelaja__ptk_t__3D690CCA] FOREIGN KEY ([ptk_terdaftar_id]) REFERENCES [ptk_terdaftar] ([ptk_terdaftar_id])
END
;

BEGIN
ALTER TABLE [pembelajaran] ADD CONSTRAINT [FK__pembelaja__rombo__24134F1B] FOREIGN KEY ([rombongan_belajar_id]) REFERENCES [rombongan_belajar] ([rombongan_belajar_id])
END
;

BEGIN
ALTER TABLE [pembelajaran] ADD CONSTRAINT [FK__pembelaja__rombo__3F51553C] FOREIGN KEY ([rombongan_belajar_id]) REFERENCES [rombongan_belajar] ([rombongan_belajar_id])
END
;

BEGIN
ALTER TABLE [pembelajaran] ADD CONSTRAINT [FK__pembelaja__mata___40457975] FOREIGN KEY ([mata_pelajaran_id]) REFERENCES [ref].[mata_pelajaran] ([mata_pelajaran_id])
END
;

BEGIN
ALTER TABLE [pembelajaran] ADD CONSTRAINT [FK__pembelaja__mata___25077354] FOREIGN KEY ([mata_pelajaran_id]) REFERENCES [ref].[mata_pelajaran] ([mata_pelajaran_id])
END
;

BEGIN
ALTER TABLE [pembelajaran] ADD CONSTRAINT [FK__pembelaja__semes__231F2AE2] FOREIGN KEY ([semester_id]) REFERENCES [ref].[semester] ([semester_id])
END
;

BEGIN
ALTER TABLE [pembelajaran] ADD CONSTRAINT [FK__pembelaja__semes__3E5D3103] FOREIGN KEY ([semester_id]) REFERENCES [ref].[semester] ([semester_id])
END
;

-----------------------------------------------------------------------
-- ref.lembaga_pengangkat
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.lembaga_pengangkat')
BEGIN
    DECLARE @reftable_57 nvarchar(60), @constraintname_57 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.lembaga_pengangkat'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_57, @constraintname_57
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_57+' drop constraint '+@constraintname_57)
        FETCH NEXT from refcursor into @reftable_57, @constraintname_57
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[lembaga_pengangkat]
END

CREATE TABLE [ref].[lembaga_pengangkat]
(
    [lembaga_pengangkat_id] NUMERIC(4,0) NOT NULL,
    [nama] VARCHAR(80) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [lembaga_pengangkat_PK] PRIMARY KEY ([lembaga_pengangkat_id])
);

CREATE INDEX [PK__lembaga___56611014849814C6] ON [ref].[lembaga_pengangkat] ([lembaga_pengangkat_id]);

-----------------------------------------------------------------------
-- vld_nonsekolah
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_nonse__lemba__575DE8F7')
    ALTER TABLE [vld_nonsekolah] DROP CONSTRAINT [FK__vld_nonse__lemba__575DE8F7];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_nonse__lemba__729BEF18')
    ALTER TABLE [vld_nonsekolah] DROP CONSTRAINT [FK__vld_nonse__lemba__729BEF18];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_nonse__idtyp__71A7CADF')
    ALTER TABLE [vld_nonsekolah] DROP CONSTRAINT [FK__vld_nonse__idtyp__71A7CADF];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_nonse__idtyp__5669C4BE')
    ALTER TABLE [vld_nonsekolah] DROP CONSTRAINT [FK__vld_nonse__idtyp__5669C4BE];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_nonsekolah')
BEGIN
    DECLARE @reftable_58 nvarchar(60), @constraintname_58 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_nonsekolah'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_58, @constraintname_58
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_58+' drop constraint '+@constraintname_58)
        FETCH NEXT from refcursor into @reftable_58, @constraintname_58
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_nonsekolah]
END

CREATE TABLE [vld_nonsekolah]
(
    [lembaga_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_nonsekolah_PK] PRIMARY KEY ([lembaga_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_nonsekolah] ([idtype]);

CREATE INDEX [VLD_NONSEKOLAH_LU] ON [vld_nonsekolah] ([last_update]);

CREATE INDEX [PK__vld_nons__BA690653A9EDB865] ON [vld_nonsekolah] ([lembaga_id],[logid]);

BEGIN
ALTER TABLE [vld_nonsekolah] ADD CONSTRAINT [FK__vld_nonse__lemba__575DE8F7] FOREIGN KEY ([lembaga_id]) REFERENCES [lembaga_non_sekolah] ([lembaga_id])
END
;

BEGIN
ALTER TABLE [vld_nonsekolah] ADD CONSTRAINT [FK__vld_nonse__lemba__729BEF18] FOREIGN KEY ([lembaga_id]) REFERENCES [lembaga_non_sekolah] ([lembaga_id])
END
;

BEGIN
ALTER TABLE [vld_nonsekolah] ADD CONSTRAINT [FK__vld_nonse__idtyp__71A7CADF] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_nonsekolah] ADD CONSTRAINT [FK__vld_nonse__idtyp__5669C4BE] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- ref.jurusan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__jurusan__jurusan__762C88DA')
    ALTER TABLE [ref].[jurusan] DROP CONSTRAINT [FK__jurusan__jurusan__762C88DA];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__jurusan__level_b__7720AD13')
    ALTER TABLE [ref].[jurusan] DROP CONSTRAINT [FK__jurusan__level_b__7720AD13];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.jurusan')
BEGIN
    DECLARE @reftable_59 nvarchar(60), @constraintname_59 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.jurusan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_59, @constraintname_59
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_59+' drop constraint '+@constraintname_59)
        FETCH NEXT from refcursor into @reftable_59, @constraintname_59
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[jurusan]
END

CREATE TABLE [ref].[jurusan]
(
    [jurusan_id] VARCHAR(25) NOT NULL,
    [nama_jurusan] VARCHAR(60) NOT NULL,
    [untuk_sma] NUMERIC(3,0) NOT NULL,
    [untuk_smk] NUMERIC(3,0) NOT NULL,
    [untuk_pt] NUMERIC(3,0) NOT NULL,
    [untuk_slb] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [untuk_smklb] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [jurusan_induk] VARCHAR(25) NULL,
    [level_bidang_id] VARCHAR(5) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [jurusan_PK] PRIMARY KEY ([jurusan_id])
);

CREATE INDEX [INDUK_PROGRAM_FK] ON [ref].[jurusan] ([jurusan_induk]);

CREATE INDEX [JUR_LU] ON [ref].[jurusan] ([last_update]);

CREATE INDEX [JURUSAN_KEL_BID_FK] ON [ref].[jurusan] ([level_bidang_id]);

CREATE INDEX [PK__jurusan__0F53CE7038A3F643] ON [ref].[jurusan] ([jurusan_id]);

BEGIN
ALTER TABLE [ref].[jurusan] ADD CONSTRAINT [FK__jurusan__jurusan__762C88DA] FOREIGN KEY ([jurusan_induk]) REFERENCES [ref].[jurusan] ([jurusan_id])
END
;

BEGIN
ALTER TABLE [ref].[jurusan] ADD CONSTRAINT [FK__jurusan__level_b__7720AD13] FOREIGN KEY ([level_bidang_id]) REFERENCES [ref].[kelompok_bidang] ([level_bidang_id])
END
;

-----------------------------------------------------------------------
-- gugus_sekolah
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__gugus_sek__sekol__2B155265')
    ALTER TABLE [gugus_sekolah] DROP CONSTRAINT [FK__gugus_sek__sekol__2B155265];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__gugus_sek__sekol__0FD74C44')
    ALTER TABLE [gugus_sekolah] DROP CONSTRAINT [FK__gugus_sek__sekol__0FD74C44];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__gugus_sek__jenis__0EE3280B')
    ALTER TABLE [gugus_sekolah] DROP CONSTRAINT [FK__gugus_sek__jenis__0EE3280B];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__gugus_sek__jenis__2A212E2C')
    ALTER TABLE [gugus_sekolah] DROP CONSTRAINT [FK__gugus_sek__jenis__2A212E2C];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'gugus_sekolah')
BEGIN
    DECLARE @reftable_60 nvarchar(60), @constraintname_60 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'gugus_sekolah'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_60, @constraintname_60
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_60+' drop constraint '+@constraintname_60)
        FETCH NEXT from refcursor into @reftable_60, @constraintname_60
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [gugus_sekolah]
END

CREATE TABLE [gugus_sekolah]
(
    [gugus_id] CHAR(16) NOT NULL,
    [sekolah_id] CHAR(16) NULL,
    [jenis_gugus_id] NUMERIC(5,0) NOT NULL,
    [nama] VARCHAR(50) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [gugus_sekolah_PK] PRIMARY KEY ([gugus_id])
);

CREATE INDEX [GUGUS_JENIS_FK] ON [gugus_sekolah] ([jenis_gugus_id]);

CREATE INDEX [GUGUS_SEK_LU] ON [gugus_sekolah] ([Last_update]);

CREATE INDEX [SEKOLAH_INTI_FK] ON [gugus_sekolah] ([sekolah_id]);

CREATE INDEX [PK__gugus_se__56C6CE1F5C416948] ON [gugus_sekolah] ([gugus_id]);

BEGIN
ALTER TABLE [gugus_sekolah] ADD CONSTRAINT [FK__gugus_sek__sekol__2B155265] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [gugus_sekolah] ADD CONSTRAINT [FK__gugus_sek__sekol__0FD74C44] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [gugus_sekolah] ADD CONSTRAINT [FK__gugus_sek__jenis__0EE3280B] FOREIGN KEY ([jenis_gugus_id]) REFERENCES [ref].[jenis_gugus] ([jenis_gugus_id])
END
;

BEGIN
ALTER TABLE [gugus_sekolah] ADD CONSTRAINT [FK__gugus_sek__jenis__2A212E2C] FOREIGN KEY ([jenis_gugus_id]) REFERENCES [ref].[jenis_gugus] ([jenis_gugus_id])
END
;

-----------------------------------------------------------------------
-- ptk_terdaftar
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk_terda__ptk_i__422DC1E7')
    ALTER TABLE [ptk_terdaftar] DROP CONSTRAINT [FK__ptk_terda__ptk_i__422DC1E7];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk_terda__ptk_i__26EFBBC6')
    ALTER TABLE [ptk_terdaftar] DROP CONSTRAINT [FK__ptk_terda__ptk_i__26EFBBC6];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk_terda__sekol__44160A59')
    ALTER TABLE [ptk_terdaftar] DROP CONSTRAINT [FK__ptk_terda__sekol__44160A59];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk_terda__sekol__28D80438')
    ALTER TABLE [ptk_terdaftar] DROP CONSTRAINT [FK__ptk_terda__sekol__28D80438];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk_terda__jenis__25FB978D')
    ALTER TABLE [ptk_terdaftar] DROP CONSTRAINT [FK__ptk_terda__jenis__25FB978D];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk_terda__jenis__41399DAE')
    ALTER TABLE [ptk_terdaftar] DROP CONSTRAINT [FK__ptk_terda__jenis__41399DAE];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk_terda__tahun__27E3DFFF')
    ALTER TABLE [ptk_terdaftar] DROP CONSTRAINT [FK__ptk_terda__tahun__27E3DFFF];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk_terda__tahun__4321E620')
    ALTER TABLE [ptk_terdaftar] DROP CONSTRAINT [FK__ptk_terda__tahun__4321E620];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ptk_terdaftar')
BEGIN
    DECLARE @reftable_61 nvarchar(60), @constraintname_61 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ptk_terdaftar'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_61, @constraintname_61
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_61+' drop constraint '+@constraintname_61)
        FETCH NEXT from refcursor into @reftable_61, @constraintname_61
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ptk_terdaftar]
END

CREATE TABLE [ptk_terdaftar]
(
    [ptk_terdaftar_id] CHAR(16) NOT NULL,
    [ptk_id] CHAR(16) NOT NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [tahun_ajaran_id] NUMERIC(6,0) NOT NULL,
    [nomor_surat_tugas] VARCHAR(40) NOT NULL,
    [tanggal_surat_tugas] VARCHAR(20) NOT NULL,
    [tmt_tugas] VARCHAR(20) NOT NULL,
    [ptk_induk] NUMERIC(3,0) NOT NULL,
    [aktif_bulan_01] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [aktif_bulan_02] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [aktif_bulan_03] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [aktif_bulan_04] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [aktif_bulan_05] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [aktif_bulan_06] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [aktif_bulan_07] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [aktif_bulan_08] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [aktif_bulan_09] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [aktif_bulan_10] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [aktif_bulan_11] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [aktif_bulan_12] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [jenis_keluar_id] CHAR(1) NULL,
    [tgl_ptk_keluar] VARCHAR(20) NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [ptk_terdaftar_PK] PRIMARY KEY ([ptk_terdaftar_id])
);

CREATE INDEX [PTK_KELUAR_FK] ON [ptk_terdaftar] ([jenis_keluar_id]);

CREATE INDEX [PTK_TERDAF_DEL] ON [ptk_terdaftar] ([Soft_delete]);

CREATE INDEX [PTK_TERDAF_LU] ON [ptk_terdaftar] ([Last_update]);

CREATE INDEX [PTK_TERDAF_SP_LU] ON [ptk_terdaftar] ([sekolah_id],[Last_update]);

CREATE INDEX [PTK_TERDAFTAR_MULAI_FK] ON [ptk_terdaftar] ([tahun_ajaran_id]);

CREATE INDEX [PK__ptk_terd__D213755C91A7B3EB] ON [ptk_terdaftar] ([ptk_terdaftar_id]);

CREATE INDEX [PTK_SEKOLAH_UNIQUE] ON [ptk_terdaftar] ([ptk_id],[sekolah_id],[tahun_ajaran_id]);

BEGIN
ALTER TABLE [ptk_terdaftar] ADD CONSTRAINT [FK__ptk_terda__ptk_i__422DC1E7] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [ptk_terdaftar] ADD CONSTRAINT [FK__ptk_terda__ptk_i__26EFBBC6] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [ptk_terdaftar] ADD CONSTRAINT [FK__ptk_terda__sekol__44160A59] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [ptk_terdaftar] ADD CONSTRAINT [FK__ptk_terda__sekol__28D80438] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [ptk_terdaftar] ADD CONSTRAINT [FK__ptk_terda__jenis__25FB978D] FOREIGN KEY ([jenis_keluar_id]) REFERENCES [ref].[jenis_keluar] ([jenis_keluar_id])
END
;

BEGIN
ALTER TABLE [ptk_terdaftar] ADD CONSTRAINT [FK__ptk_terda__jenis__41399DAE] FOREIGN KEY ([jenis_keluar_id]) REFERENCES [ref].[jenis_keluar] ([jenis_keluar_id])
END
;

BEGIN
ALTER TABLE [ptk_terdaftar] ADD CONSTRAINT [FK__ptk_terda__tahun__27E3DFFF] FOREIGN KEY ([tahun_ajaran_id]) REFERENCES [ref].[tahun_ajaran] ([tahun_ajaran_id])
END
;

BEGIN
ALTER TABLE [ptk_terdaftar] ADD CONSTRAINT [FK__ptk_terda__tahun__4321E620] FOREIGN KEY ([tahun_ajaran_id]) REFERENCES [ref].[tahun_ajaran] ([tahun_ajaran_id])
END
;

-----------------------------------------------------------------------
-- ref.waktu_penyelenggaraan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.waktu_penyelenggaraan')
BEGIN
    DECLARE @reftable_62 nvarchar(60), @constraintname_62 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.waktu_penyelenggaraan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_62, @constraintname_62
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_62+' drop constraint '+@constraintname_62)
        FETCH NEXT from refcursor into @reftable_62, @constraintname_62
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[waktu_penyelenggaraan]
END

CREATE TABLE [ref].[waktu_penyelenggaraan]
(
    [waktu_penyelenggaraan_id] NUMERIC(3,0) NOT NULL,
    [nama] VARCHAR(20) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [waktu_penyelenggaraan_PK] PRIMARY KEY ([waktu_penyelenggaraan_id])
);

CREATE INDEX [PK__waktu_pe__9162E11AA2472AC9] ON [ref].[waktu_penyelenggaraan] ([waktu_penyelenggaraan_id]);

-----------------------------------------------------------------------
-- vld_nilai_test
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_nilai__nilai__58520D30')
    ALTER TABLE [vld_nilai_test] DROP CONSTRAINT [FK__vld_nilai__nilai__58520D30];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_nilai__nilai__73901351')
    ALTER TABLE [vld_nilai_test] DROP CONSTRAINT [FK__vld_nilai__nilai__73901351];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_nilai__idtyp__7484378A')
    ALTER TABLE [vld_nilai_test] DROP CONSTRAINT [FK__vld_nilai__idtyp__7484378A];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_nilai__idtyp__59463169')
    ALTER TABLE [vld_nilai_test] DROP CONSTRAINT [FK__vld_nilai__idtyp__59463169];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_nilai_test')
BEGIN
    DECLARE @reftable_63 nvarchar(60), @constraintname_63 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_nilai_test'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_63, @constraintname_63
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_63+' drop constraint '+@constraintname_63)
        FETCH NEXT from refcursor into @reftable_63, @constraintname_63
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_nilai_test]
END

CREATE TABLE [vld_nilai_test]
(
    [nilai_test_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_nilai_test_PK] PRIMARY KEY ([nilai_test_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_nilai_test] ([idtype]);

CREATE INDEX [VLD_NILAI_TEST_LU] ON [vld_nilai_test] ([last_update]);

CREATE INDEX [PK__vld_nila__7BFFB413A81F13D4] ON [vld_nilai_test] ([nilai_test_id],[logid]);

BEGIN
ALTER TABLE [vld_nilai_test] ADD CONSTRAINT [FK__vld_nilai__nilai__58520D30] FOREIGN KEY ([nilai_test_id]) REFERENCES [nilai_test] ([nilai_test_id])
END
;

BEGIN
ALTER TABLE [vld_nilai_test] ADD CONSTRAINT [FK__vld_nilai__nilai__73901351] FOREIGN KEY ([nilai_test_id]) REFERENCES [nilai_test] ([nilai_test_id])
END
;

BEGIN
ALTER TABLE [vld_nilai_test] ADD CONSTRAINT [FK__vld_nilai__idtyp__7484378A] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_nilai_test] ADD CONSTRAINT [FK__vld_nilai__idtyp__59463169] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- blockgrant
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__blockgran__sekol__2C09769E')
    ALTER TABLE [blockgrant] DROP CONSTRAINT [FK__blockgran__sekol__2C09769E];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__blockgran__sekol__10CB707D')
    ALTER TABLE [blockgrant] DROP CONSTRAINT [FK__blockgran__sekol__10CB707D];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__blockgran__jenis__11BF94B6')
    ALTER TABLE [blockgrant] DROP CONSTRAINT [FK__blockgran__jenis__11BF94B6];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__blockgran__jenis__2CFD9AD7')
    ALTER TABLE [blockgrant] DROP CONSTRAINT [FK__blockgran__jenis__2CFD9AD7];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__blockgran__sumbe__12B3B8EF')
    ALTER TABLE [blockgrant] DROP CONSTRAINT [FK__blockgran__sumbe__12B3B8EF];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__blockgran__sumbe__2DF1BF10')
    ALTER TABLE [blockgrant] DROP CONSTRAINT [FK__blockgran__sumbe__2DF1BF10];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'blockgrant')
BEGIN
    DECLARE @reftable_64 nvarchar(60), @constraintname_64 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'blockgrant'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_64, @constraintname_64
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_64+' drop constraint '+@constraintname_64)
        FETCH NEXT from refcursor into @reftable_64, @constraintname_64
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [blockgrant]
END

CREATE TABLE [blockgrant]
(
    [blockgrant_id] CHAR(16) NOT NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [nama] VARCHAR(50) NOT NULL,
    [tahun] NUMERIC(6,0) NOT NULL,
    [jenis_bantuan_id] INT NOT NULL,
    [sumber_dana_id] NUMERIC(5,0) NOT NULL,
    [besar_bantuan] NUMERIC(17,0) NOT NULL,
    [dana_pendamping] NUMERIC(17,0) NOT NULL,
    [peruntukan_dana] VARCHAR(50) NULL,
    [asal_data] CHAR(1) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [blockgrant_PK] PRIMARY KEY ([blockgrant_id])
);

CREATE INDEX [BLOCK_JENIS_BANTUAN_FK] ON [blockgrant] ([jenis_bantuan_id]);

CREATE INDEX [BLOCK_SUMBER_FK] ON [blockgrant] ([sumber_dana_id]);

CREATE INDEX [BLOCKGRANT_LU] ON [blockgrant] ([Last_update]);

CREATE INDEX [BLOCKGRANT_SEKOLAH_FK] ON [blockgrant] ([sekolah_id]);

CREATE INDEX [PK__blockgra__B9985298B4377384] ON [blockgrant] ([blockgrant_id]);

BEGIN
ALTER TABLE [blockgrant] ADD CONSTRAINT [FK__blockgran__sekol__2C09769E] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [blockgrant] ADD CONSTRAINT [FK__blockgran__sekol__10CB707D] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [blockgrant] ADD CONSTRAINT [FK__blockgran__jenis__11BF94B6] FOREIGN KEY ([jenis_bantuan_id]) REFERENCES [ref].[jenis_bantuan] ([jenis_bantuan_id])
END
;

BEGIN
ALTER TABLE [blockgrant] ADD CONSTRAINT [FK__blockgran__jenis__2CFD9AD7] FOREIGN KEY ([jenis_bantuan_id]) REFERENCES [ref].[jenis_bantuan] ([jenis_bantuan_id])
END
;

BEGIN
ALTER TABLE [blockgrant] ADD CONSTRAINT [FK__blockgran__sumbe__12B3B8EF] FOREIGN KEY ([sumber_dana_id]) REFERENCES [ref].[sumber_dana] ([sumber_dana_id])
END
;

BEGIN
ALTER TABLE [blockgrant] ADD CONSTRAINT [FK__blockgran__sumbe__2DF1BF10] FOREIGN KEY ([sumber_dana_id]) REFERENCES [ref].[sumber_dana] ([sumber_dana_id])
END
;

-----------------------------------------------------------------------
-- ref.negara
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.negara')
BEGIN
    DECLARE @reftable_65 nvarchar(60), @constraintname_65 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.negara'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_65, @constraintname_65
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_65+' drop constraint '+@constraintname_65)
        FETCH NEXT from refcursor into @reftable_65, @constraintname_65
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[negara]
END

CREATE TABLE [ref].[negara]
(
    [negara_id] CHAR(2) NOT NULL,
    [nama] VARCHAR(45) NOT NULL,
    [luar_negeri] NUMERIC(3,0) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [negara_PK] PRIMARY KEY ([negara_id])
);

CREATE INDEX [NEGARA_LU] ON [ref].[negara] ([last_update]);

CREATE INDEX [PK__negara__81C52847A96A865C] ON [ref].[negara] ([negara_id]);

-----------------------------------------------------------------------
-- karya_tulis
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__karya_tul__ptk_i__13A7DD28')
    ALTER TABLE [karya_tulis] DROP CONSTRAINT [FK__karya_tul__ptk_i__13A7DD28];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__karya_tul__ptk_i__2EE5E349')
    ALTER TABLE [karya_tulis] DROP CONSTRAINT [FK__karya_tul__ptk_i__2EE5E349];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'karya_tulis')
BEGIN
    DECLARE @reftable_66 nvarchar(60), @constraintname_66 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'karya_tulis'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_66, @constraintname_66
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_66+' drop constraint '+@constraintname_66)
        FETCH NEXT from refcursor into @reftable_66, @constraintname_66
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [karya_tulis]
END

CREATE TABLE [karya_tulis]
(
    [karya_tulis_id] CHAR(16) NOT NULL,
    [ptk_id] CHAR(16) NOT NULL,
    [judul] VARCHAR(50) NOT NULL,
    [tahun_pembuatan] NUMERIC(6,0) NOT NULL,
    [publikasi] VARCHAR(30) NULL,
    [keterangan] VARCHAR(160) NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [karya_tulis_PK] PRIMARY KEY ([karya_tulis_id])
);

CREATE INDEX [KARYA_TULIS_LU] ON [karya_tulis] ([Last_update]);

CREATE INDEX [KARYA_TULIS_PTK_FK] ON [karya_tulis] ([ptk_id]);

CREATE INDEX [PK__karya_tu__5F5B88F4BBDA96D1] ON [karya_tulis] ([karya_tulis_id]);

BEGIN
ALTER TABLE [karya_tulis] ADD CONSTRAINT [FK__karya_tul__ptk_i__13A7DD28] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [karya_tulis] ADD CONSTRAINT [FK__karya_tul__ptk_i__2EE5E349] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

-----------------------------------------------------------------------
-- ref.sumber_air
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.sumber_air')
BEGIN
    DECLARE @reftable_67 nvarchar(60), @constraintname_67 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.sumber_air'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_67, @constraintname_67
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_67+' drop constraint '+@constraintname_67)
        FETCH NEXT from refcursor into @reftable_67, @constraintname_67
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[sumber_air]
END

CREATE TABLE [ref].[sumber_air]
(
    [sumber_air_id] NUMERIC(4,0) NOT NULL,
    [nama] VARCHAR(25) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [sumber_air_PK] PRIMARY KEY ([sumber_air_id])
);

CREATE INDEX [PK__sumber_a__E1B42FE7E54CCE70] ON [ref].[sumber_air] ([sumber_air_id]);

-----------------------------------------------------------------------
-- vld_nilai_rapor
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_nilai__idtyp__766C7FFC')
    ALTER TABLE [vld_nilai_rapor] DROP CONSTRAINT [FK__vld_nilai__idtyp__766C7FFC];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_nilai__idtyp__5B2E79DB')
    ALTER TABLE [vld_nilai_rapor] DROP CONSTRAINT [FK__vld_nilai__idtyp__5B2E79DB];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_nilai_rapor')
BEGIN
    DECLARE @reftable_68 nvarchar(60), @constraintname_68 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_nilai_rapor'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_68, @constraintname_68
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_68+' drop constraint '+@constraintname_68)
        FETCH NEXT from refcursor into @reftable_68, @constraintname_68
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_nilai_rapor]
END

CREATE TABLE [vld_nilai_rapor]
(
    [nilai_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [app_username] VARCHAR(50) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [vld_nilai_rapor_PK] PRIMARY KEY ([nilai_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_nilai_rapor] ([idtype]);

CREATE INDEX [PK__vld_nila__4EAA764F35C577A2] ON [vld_nilai_rapor] ([nilai_id],[logid]);

BEGIN
ALTER TABLE [vld_nilai_rapor] ADD CONSTRAINT [FK__vld_nilai__idtyp__766C7FFC] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_nilai_rapor] ADD CONSTRAINT [FK__vld_nilai__idtyp__5B2E79DB] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- yayasan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__yayasan__kode_wi__149C0161')
    ALTER TABLE [yayasan] DROP CONSTRAINT [FK__yayasan__kode_wi__149C0161];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__yayasan__kode_wi__2FDA0782')
    ALTER TABLE [yayasan] DROP CONSTRAINT [FK__yayasan__kode_wi__2FDA0782];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'yayasan')
BEGIN
    DECLARE @reftable_69 nvarchar(60), @constraintname_69 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'yayasan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_69, @constraintname_69
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_69+' drop constraint '+@constraintname_69)
        FETCH NEXT from refcursor into @reftable_69, @constraintname_69
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [yayasan]
END

CREATE TABLE [yayasan]
(
    [yayasan_id] CHAR(16) NOT NULL,
    [nama] VARCHAR(80) NOT NULL,
    [alamat_jalan] VARCHAR(80) NOT NULL,
    [rt] NUMERIC(4,0) NULL,
    [rw] NUMERIC(4,0) NULL,
    [nama_dusun] VARCHAR(40) NULL,
    [desa_kelurahan] VARCHAR(40) NOT NULL,
    [kode_wilayah] CHAR(8) NOT NULL,
    [kode_pos] CHAR(5) NULL,
    [lintang] NUMERIC(12,6) NULL,
    [bujur] NUMERIC(12,6) NULL,
    [nomor_telepon] VARCHAR(20) NULL,
    [nomor_fax] VARCHAR(20) NULL,
    [email] VARCHAR(50) NULL,
    [website] VARCHAR(100) NULL,
    [nama_pimpinan_yayasan] VARCHAR(50) NOT NULL,
    [no_pendirian_yayasan] VARCHAR(40) NULL,
    [tanggal_pendirian_yayasan] VARCHAR(20) NULL,
    [nomor_pengesahan_pn_ln] VARCHAR(30) NULL,
    [nomor_sk_bn] VARCHAR(255) NULL,
    [tanggal_sk_bn] VARCHAR(20) NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [yayasan_PK] PRIMARY KEY ([yayasan_id])
);

CREATE INDEX [LEMBAGA_KECAMATAN_FK] ON [yayasan] ([kode_wilayah]);

CREATE INDEX [YAYASAN_LU] ON [yayasan] ([Last_update]);

CREATE INDEX [PK__yayasan__7EA57D3C524BBA45] ON [yayasan] ([yayasan_id]);

BEGIN
ALTER TABLE [yayasan] ADD CONSTRAINT [FK__yayasan__kode_wi__149C0161] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

BEGIN
ALTER TABLE [yayasan] ADD CONSTRAINT [FK__yayasan__kode_wi__2FDA0782] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

-----------------------------------------------------------------------
-- ref.gelar_akademik
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.gelar_akademik')
BEGIN
    DECLARE @reftable_70 nvarchar(60), @constraintname_70 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.gelar_akademik'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_70, @constraintname_70
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_70+' drop constraint '+@constraintname_70)
        FETCH NEXT from refcursor into @reftable_70, @constraintname_70
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[gelar_akademik]
END

CREATE TABLE [ref].[gelar_akademik]
(
    [gelar_akademik_id] INT NOT NULL,
    [kode] VARCHAR(10) NOT NULL,
    [nama] VARCHAR(40) NOT NULL,
    [posisi_gelar] NUMERIC(3,0) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [gelar_akademik_PK] PRIMARY KEY ([gelar_akademik_id])
);

CREATE INDEX [GELAR_LU] ON [ref].[gelar_akademik] ([last_update]);

CREATE INDEX [PK__gelar_ak__C89FB63D2A41FC5E] ON [ref].[gelar_akademik] ([gelar_akademik_id]);

-----------------------------------------------------------------------
-- ref.jenjang_kepengawasan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.jenjang_kepengawasan')
BEGIN
    DECLARE @reftable_71 nvarchar(60), @constraintname_71 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.jenjang_kepengawasan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_71, @constraintname_71
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_71+' drop constraint '+@constraintname_71)
        FETCH NEXT from refcursor into @reftable_71, @constraintname_71
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[jenjang_kepengawasan]
END

CREATE TABLE [ref].[jenjang_kepengawasan]
(
    [jenjang_kepengawasan_id] NUMERIC(4,0) NOT NULL,
    [nama] VARCHAR(50) NOT NULL,
    [jenjang_kepengawasan_tk] NUMERIC(3,0) NOT NULL,
    [jenjang_kepengawasan_sd] NUMERIC(3,0) NOT NULL,
    [jenjang_kepengawasan_smp] NUMERIC(3,0) NOT NULL,
    [jenjang_kepengawasan_sma] NUMERIC(3,0) NOT NULL,
    [jenjang_kepengawasan_smk] NUMERIC(3,0) NOT NULL,
    [jenjang_kepengawasan_slb] NUMERIC(3,0) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [jenjang_kepengawasan_PK] PRIMARY KEY ([jenjang_kepengawasan_id])
);

CREATE INDEX [PK__jenjang___8A7D96724F78C3C3] ON [ref].[jenjang_kepengawasan] ([jenjang_kepengawasan_id]);

-----------------------------------------------------------------------
-- vld_mou
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_mou__mou_id__5D16C24D')
    ALTER TABLE [vld_mou] DROP CONSTRAINT [FK__vld_mou__mou_id__5D16C24D];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_mou__mou_id__7854C86E')
    ALTER TABLE [vld_mou] DROP CONSTRAINT [FK__vld_mou__mou_id__7854C86E];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_mou__idtype__7760A435')
    ALTER TABLE [vld_mou] DROP CONSTRAINT [FK__vld_mou__idtype__7760A435];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_mou__idtype__5C229E14')
    ALTER TABLE [vld_mou] DROP CONSTRAINT [FK__vld_mou__idtype__5C229E14];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_mou')
BEGIN
    DECLARE @reftable_72 nvarchar(60), @constraintname_72 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_mou'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_72, @constraintname_72
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_72+' drop constraint '+@constraintname_72)
        FETCH NEXT from refcursor into @reftable_72, @constraintname_72
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_mou]
END

CREATE TABLE [vld_mou]
(
    [mou_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_mou_PK] PRIMARY KEY ([mou_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_mou] ([idtype]);

CREATE INDEX [PK__vld_mou__C1D297A478140CF7] ON [vld_mou] ([mou_id],[logid]);

BEGIN
ALTER TABLE [vld_mou] ADD CONSTRAINT [FK__vld_mou__mou_id__5D16C24D] FOREIGN KEY ([mou_id]) REFERENCES [mou] ([mou_id])
END
;

BEGIN
ALTER TABLE [vld_mou] ADD CONSTRAINT [FK__vld_mou__mou_id__7854C86E] FOREIGN KEY ([mou_id]) REFERENCES [mou] ([mou_id])
END
;

BEGIN
ALTER TABLE [vld_mou] ADD CONSTRAINT [FK__vld_mou__idtype__7760A435] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_mou] ADD CONSTRAINT [FK__vld_mou__idtype__5C229E14] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- nilai_test
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__nilai_tes__ptk_i__168449D3')
    ALTER TABLE [nilai_test] DROP CONSTRAINT [FK__nilai_tes__ptk_i__168449D3];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__nilai_tes__ptk_i__31C24FF4')
    ALTER TABLE [nilai_test] DROP CONSTRAINT [FK__nilai_tes__ptk_i__31C24FF4];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__nilai_tes__jenis__1590259A')
    ALTER TABLE [nilai_test] DROP CONSTRAINT [FK__nilai_tes__jenis__1590259A];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__nilai_tes__jenis__30CE2BBB')
    ALTER TABLE [nilai_test] DROP CONSTRAINT [FK__nilai_tes__jenis__30CE2BBB];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'nilai_test')
BEGIN
    DECLARE @reftable_73 nvarchar(60), @constraintname_73 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'nilai_test'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_73, @constraintname_73
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_73+' drop constraint '+@constraintname_73)
        FETCH NEXT from refcursor into @reftable_73, @constraintname_73
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [nilai_test]
END

CREATE TABLE [nilai_test]
(
    [nilai_test_id] CHAR(16) NOT NULL,
    [jenis_test_id] NUMERIC(5,0) NOT NULL,
    [ptk_id] CHAR(16) NOT NULL,
    [nama] VARCHAR(50) NOT NULL,
    [penyelenggara] VARCHAR(80) NOT NULL,
    [tahun] NUMERIC(6,0) NOT NULL,
    [skor] NUMERIC(8,2) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [nilai_test_PK] PRIMARY KEY ([nilai_test_id])
);

CREATE INDEX [NILAI_TEST_LU] ON [nilai_test] ([Last_update]);

CREATE INDEX [TEST_JENIS_FK] ON [nilai_test] ([jenis_test_id]);

CREATE INDEX [TEST_KEBAHASAAN_PTK_FK] ON [nilai_test] ([ptk_id]);

CREATE INDEX [PK__nilai_te__3C7C3B34C0955FB0] ON [nilai_test] ([nilai_test_id]);

BEGIN
ALTER TABLE [nilai_test] ADD CONSTRAINT [FK__nilai_tes__ptk_i__168449D3] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [nilai_test] ADD CONSTRAINT [FK__nilai_tes__ptk_i__31C24FF4] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [nilai_test] ADD CONSTRAINT [FK__nilai_tes__jenis__1590259A] FOREIGN KEY ([jenis_test_id]) REFERENCES [ref].[jenis_test] ([jenis_test_id])
END
;

BEGIN
ALTER TABLE [nilai_test] ADD CONSTRAINT [FK__nilai_tes__jenis__30CE2BBB] FOREIGN KEY ([jenis_test_id]) REFERENCES [ref].[jenis_test] ([jenis_test_id])
END
;

-----------------------------------------------------------------------
-- ref.kurikulum
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__kurikulum__jenja__7814D14C')
    ALTER TABLE [ref].[kurikulum] DROP CONSTRAINT [FK__kurikulum__jenja__7814D14C];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__kurikulum__jurus__7908F585')
    ALTER TABLE [ref].[kurikulum] DROP CONSTRAINT [FK__kurikulum__jurus__7908F585];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.kurikulum')
BEGIN
    DECLARE @reftable_74 nvarchar(60), @constraintname_74 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.kurikulum'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_74, @constraintname_74
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_74+' drop constraint '+@constraintname_74)
        FETCH NEXT from refcursor into @reftable_74, @constraintname_74
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[kurikulum]
END

CREATE TABLE [ref].[kurikulum]
(
    [kurikulum_id] SMALLINT(2,0) NOT NULL,
    [jurusan_id] VARCHAR(25) NULL,
    [jenjang_pendidikan_id] NUMERIC(4,0) NOT NULL,
    [nama_kurikulum] VARCHAR(30) NOT NULL,
    [mulai_berlaku] VARCHAR(20) NOT NULL,
    [sistem_sks] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [total_sks] NUMERIC(5,0) DEFAULT ((0)) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [kurikulum_PK] PRIMARY KEY ([kurikulum_id])
);

CREATE INDEX [KURIKULUM_JENJANG_FK] ON [ref].[kurikulum] ([jenjang_pendidikan_id]);

CREATE INDEX [KURIKULUM_JURUSAN_FK] ON [ref].[kurikulum] ([jurusan_id]);

CREATE INDEX [PK__kurikulu__3248A3CF0AA7F012] ON [ref].[kurikulum] ([kurikulum_id]);

BEGIN
ALTER TABLE [ref].[kurikulum] ADD CONSTRAINT [FK__kurikulum__jenja__7814D14C] FOREIGN KEY ([jenjang_pendidikan_id]) REFERENCES [ref].[jenjang_pendidikan] ([jenjang_pendidikan_id])
END
;

BEGIN
ALTER TABLE [ref].[kurikulum] ADD CONSTRAINT [FK__kurikulum__jurus__7908F585] FOREIGN KEY ([jurusan_id]) REFERENCES [ref].[jurusan] ([jurusan_id])
END
;

-----------------------------------------------------------------------
-- ref.jenis_pendaftaran
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.jenis_pendaftaran')
BEGIN
    DECLARE @reftable_75 nvarchar(60), @constraintname_75 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.jenis_pendaftaran'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_75, @constraintname_75
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_75+' drop constraint '+@constraintname_75)
        FETCH NEXT from refcursor into @reftable_75, @constraintname_75
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[jenis_pendaftaran]
END

CREATE TABLE [ref].[jenis_pendaftaran]
(
    [jenis_pendaftaran_id] NUMERIC(3,0) NOT NULL,
    [nama] VARCHAR(20) NOT NULL,
    [daftar_sekolah] NUMERIC(3,0) NOT NULL,
    [daftar_rombel] NUMERIC(3,0) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [jenis_pendaftaran_PK] PRIMARY KEY ([jenis_pendaftaran_id])
);

CREATE INDEX [PK__jenis_pe__5C1826F4D91F4966] ON [ref].[jenis_pendaftaran] ([jenis_pendaftaran_id]);

-----------------------------------------------------------------------
-- vld_kesejahteraan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_kesej__kesej__5EFF0ABF')
    ALTER TABLE [vld_kesejahteraan] DROP CONSTRAINT [FK__vld_kesej__kesej__5EFF0ABF];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_kesej__kesej__7A3D10E0')
    ALTER TABLE [vld_kesejahteraan] DROP CONSTRAINT [FK__vld_kesej__kesej__7A3D10E0];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_kesej__idtyp__7948ECA7')
    ALTER TABLE [vld_kesejahteraan] DROP CONSTRAINT [FK__vld_kesej__idtyp__7948ECA7];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_kesej__idtyp__5E0AE686')
    ALTER TABLE [vld_kesejahteraan] DROP CONSTRAINT [FK__vld_kesej__idtyp__5E0AE686];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_kesejahteraan')
BEGIN
    DECLARE @reftable_76 nvarchar(60), @constraintname_76 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_kesejahteraan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_76, @constraintname_76
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_76+' drop constraint '+@constraintname_76)
        FETCH NEXT from refcursor into @reftable_76, @constraintname_76
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_kesejahteraan]
END

CREATE TABLE [vld_kesejahteraan]
(
    [kesejahteraan_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_kesejahteraan_PK] PRIMARY KEY ([kesejahteraan_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_kesejahteraan] ([idtype]);

CREATE INDEX [VLD_SEJAHTERA_LU] ON [vld_kesejahteraan] ([last_update]);

CREATE INDEX [PK__vld_kese__88B5E7367A007791] ON [vld_kesejahteraan] ([kesejahteraan_id],[logid]);

BEGIN
ALTER TABLE [vld_kesejahteraan] ADD CONSTRAINT [FK__vld_kesej__kesej__5EFF0ABF] FOREIGN KEY ([kesejahteraan_id]) REFERENCES [kesejahteraan] ([kesejahteraan_id])
END
;

BEGIN
ALTER TABLE [vld_kesejahteraan] ADD CONSTRAINT [FK__vld_kesej__kesej__7A3D10E0] FOREIGN KEY ([kesejahteraan_id]) REFERENCES [kesejahteraan] ([kesejahteraan_id])
END
;

BEGIN
ALTER TABLE [vld_kesejahteraan] ADD CONSTRAINT [FK__vld_kesej__idtyp__7948ECA7] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_kesejahteraan] ADD CONSTRAINT [FK__vld_kesej__idtyp__5E0AE686] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- buku_ptk
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__buku_ptk__ptk_id__32B6742D')
    ALTER TABLE [buku_ptk] DROP CONSTRAINT [FK__buku_ptk__ptk_id__32B6742D];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__buku_ptk__ptk_id__17786E0C')
    ALTER TABLE [buku_ptk] DROP CONSTRAINT [FK__buku_ptk__ptk_id__17786E0C];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'buku_ptk')
BEGIN
    DECLARE @reftable_77 nvarchar(60), @constraintname_77 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'buku_ptk'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_77, @constraintname_77
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_77+' drop constraint '+@constraintname_77)
        FETCH NEXT from refcursor into @reftable_77, @constraintname_77
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [buku_ptk]
END

CREATE TABLE [buku_ptk]
(
    [buku_id] CHAR(16) NOT NULL,
    [ptk_id] CHAR(16) NOT NULL,
    [judul_buku] VARCHAR(50) NOT NULL,
    [tahun] NUMERIC(6,0) NOT NULL,
    [penerbit] VARCHAR(30) NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [buku_ptk_PK] PRIMARY KEY ([buku_id])
);

CREATE INDEX [BUKU_PTK_FK] ON [buku_ptk] ([ptk_id]);

CREATE INDEX [BUKU_PTK_LU] ON [buku_ptk] ([Last_update]);

CREATE INDEX [PK__buku_ptk__AF740E08BE40ADCF] ON [buku_ptk] ([buku_id]);

BEGIN
ALTER TABLE [buku_ptk] ADD CONSTRAINT [FK__buku_ptk__ptk_id__32B6742D] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [buku_ptk] ADD CONSTRAINT [FK__buku_ptk__ptk_id__17786E0C] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

-----------------------------------------------------------------------
-- prasarana_longitudinal
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__prasarana__prasa__29CC2871')
    ALTER TABLE [prasarana_longitudinal] DROP CONSTRAINT [FK__prasarana__prasa__29CC2871];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__prasarana__prasa__450A2E92')
    ALTER TABLE [prasarana_longitudinal] DROP CONSTRAINT [FK__prasarana__prasa__450A2E92];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__prasarana__semes__2BB470E3')
    ALTER TABLE [prasarana_longitudinal] DROP CONSTRAINT [FK__prasarana__semes__2BB470E3];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__prasarana__semes__46F27704')
    ALTER TABLE [prasarana_longitudinal] DROP CONSTRAINT [FK__prasarana__semes__46F27704];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'prasarana_longitudinal')
BEGIN
    DECLARE @reftable_78 nvarchar(60), @constraintname_78 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'prasarana_longitudinal'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_78, @constraintname_78
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_78+' drop constraint '+@constraintname_78)
        FETCH NEXT from refcursor into @reftable_78, @constraintname_78
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [prasarana_longitudinal]
END

CREATE TABLE [prasarana_longitudinal]
(
    [prasarana_id] CHAR(16) NOT NULL,
    [semester_id] CHAR(5) NOT NULL,
    [rusak_penutup_atap] NUMERIC(8,2) DEFAULT ((0)) NOT NULL,
    [rusak_rangka_atap] NUMERIC(8,2) DEFAULT ((0)) NOT NULL,
    [rusak_lisplang_talang] NUMERIC(8,2) DEFAULT ((0)) NOT NULL,
    [rusak_rangka_plafon] NUMERIC(8,2) DEFAULT ((0)) NOT NULL,
    [rusak_penutup_listplafon] NUMERIC(8,2) DEFAULT ((0)) NOT NULL,
    [rusak_cat_plafon] NUMERIC(8,2) DEFAULT ((0)) NOT NULL,
    [rusak_kolom_ringbalok] NUMERIC(8,2) DEFAULT ((0)) NOT NULL,
    [rusak_bata_dindingpengisi] NUMERIC(8,2) DEFAULT ((0)) NOT NULL,
    [rusak_cat_dinding] NUMERIC(8,2) DEFAULT ((0)) NOT NULL,
    [rusak_kusen] NUMERIC(8,2) DEFAULT ((0)) NOT NULL,
    [rusak_daun_pintu] NUMERIC(8,2) DEFAULT ((0)) NOT NULL,
    [rusak_daun_jendela] NUMERIC(8,2) DEFAULT ((0)) NOT NULL,
    [rusak_struktur_bawah] NUMERIC(8,2) DEFAULT ((0)) NOT NULL,
    [rusak_penutup_lantai] NUMERIC(8,2) DEFAULT ((0)) NOT NULL,
    [rusak_pondasi] NUMERIC(8,2) DEFAULT ((0)) NOT NULL,
    [rusak_sloof] NUMERIC(8,2) DEFAULT ((0)) NOT NULL,
    [rusak_listrik] NUMERIC(8,2) DEFAULT ((0)) NOT NULL,
    [rusak_airhujan_rabatan] NUMERIC(8,2) DEFAULT ((0)) NOT NULL,
    [blob_id] CHAR(16) NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [prasarana_longitudinal_PK] PRIMARY KEY ([prasarana_id],[semester_id])
);

CREATE INDEX [PRASARANA_FOTO_FK] ON [prasarana_longitudinal] ([blob_id]);

CREATE INDEX [PRASARANA_SEM_FK] ON [prasarana_longitudinal] ([semester_id]);

CREATE INDEX [PRASLONG_LU] ON [prasarana_longitudinal] ([Last_update]);

CREATE INDEX [PK__prasaran__B3ACAEE7FAF042C7] ON [prasarana_longitudinal] ([prasarana_id],[semester_id]);

BEGIN
ALTER TABLE [prasarana_longitudinal] ADD CONSTRAINT [FK__prasarana__prasa__29CC2871] FOREIGN KEY ([prasarana_id]) REFERENCES [prasarana] ([prasarana_id])
END
;

BEGIN
ALTER TABLE [prasarana_longitudinal] ADD CONSTRAINT [FK__prasarana__prasa__450A2E92] FOREIGN KEY ([prasarana_id]) REFERENCES [prasarana] ([prasarana_id])
END
;

BEGIN
ALTER TABLE [prasarana_longitudinal] ADD CONSTRAINT [FK__prasarana__semes__2BB470E3] FOREIGN KEY ([semester_id]) REFERENCES [ref].[semester] ([semester_id])
END
;

BEGIN
ALTER TABLE [prasarana_longitudinal] ADD CONSTRAINT [FK__prasarana__semes__46F27704] FOREIGN KEY ([semester_id]) REFERENCES [ref].[semester] ([semester_id])
END
;

-----------------------------------------------------------------------
-- beasiswa_ptk
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__beasiswa___ptk_i__33AA9866')
    ALTER TABLE [beasiswa_ptk] DROP CONSTRAINT [FK__beasiswa___ptk_i__33AA9866];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__beasiswa___ptk_i__186C9245')
    ALTER TABLE [beasiswa_ptk] DROP CONSTRAINT [FK__beasiswa___ptk_i__186C9245];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__beasiswa___jenis__1960B67E')
    ALTER TABLE [beasiswa_ptk] DROP CONSTRAINT [FK__beasiswa___jenis__1960B67E];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__beasiswa___jenis__349EBC9F')
    ALTER TABLE [beasiswa_ptk] DROP CONSTRAINT [FK__beasiswa___jenis__349EBC9F];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'beasiswa_ptk')
BEGIN
    DECLARE @reftable_79 nvarchar(60), @constraintname_79 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'beasiswa_ptk'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_79, @constraintname_79
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_79+' drop constraint '+@constraintname_79)
        FETCH NEXT from refcursor into @reftable_79, @constraintname_79
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [beasiswa_ptk]
END

CREATE TABLE [beasiswa_ptk]
(
    [beasiswa_ptk_id] CHAR(16) NOT NULL,
    [jenis_beasiswa_id] INT NOT NULL,
    [ptk_id] CHAR(16) NOT NULL,
    [keterangan] VARCHAR(80) NOT NULL,
    [tahun_mulai] NUMERIC(6,0) NOT NULL,
    [tahun_akhir] NUMERIC(6,0) NULL,
    [masih_menerima] NUMERIC(3,0) DEFAULT ((1)) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [beasiswa_ptk_PK] PRIMARY KEY ([beasiswa_ptk_id])
);

CREATE INDEX [BEA_PTK_LU] ON [beasiswa_ptk] ([Last_update]);

CREATE INDEX [BEASISWA_PTK_JENIS_FK] ON [beasiswa_ptk] ([jenis_beasiswa_id]);

CREATE INDEX [BEASISWA_PTK_PTK_FK] ON [beasiswa_ptk] ([ptk_id]);

CREATE INDEX [PK__beasiswa__9D69D564D98ABBAC] ON [beasiswa_ptk] ([beasiswa_ptk_id]);

BEGIN
ALTER TABLE [beasiswa_ptk] ADD CONSTRAINT [FK__beasiswa___ptk_i__33AA9866] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [beasiswa_ptk] ADD CONSTRAINT [FK__beasiswa___ptk_i__186C9245] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [beasiswa_ptk] ADD CONSTRAINT [FK__beasiswa___jenis__1960B67E] FOREIGN KEY ([jenis_beasiswa_id]) REFERENCES [ref].[jenis_beasiswa] ([jenis_beasiswa_id])
END
;

BEGIN
ALTER TABLE [beasiswa_ptk] ADD CONSTRAINT [FK__beasiswa___jenis__349EBC9F] FOREIGN KEY ([jenis_beasiswa_id]) REFERENCES [ref].[jenis_beasiswa] ([jenis_beasiswa_id])
END
;

-----------------------------------------------------------------------
-- ref.agama
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.agama')
BEGIN
    DECLARE @reftable_80 nvarchar(60), @constraintname_80 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.agama'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_80, @constraintname_80
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_80+' drop constraint '+@constraintname_80)
        FETCH NEXT from refcursor into @reftable_80, @constraintname_80
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[agama]
END

CREATE TABLE [ref].[agama]
(
    [agama_id] SMALLINT(2,0) NOT NULL,
    [nama] VARCHAR(25) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [agama_PK] PRIMARY KEY ([agama_id])
);

CREATE INDEX [PK__agama__88601DA128E4EA64] ON [ref].[agama] ([agama_id]);

-----------------------------------------------------------------------
-- vld_karya_tulis
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_karya__karya__60E75331')
    ALTER TABLE [vld_karya_tulis] DROP CONSTRAINT [FK__vld_karya__karya__60E75331];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_karya__karya__7C255952')
    ALTER TABLE [vld_karya_tulis] DROP CONSTRAINT [FK__vld_karya__karya__7C255952];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_karya__idtyp__7B313519')
    ALTER TABLE [vld_karya_tulis] DROP CONSTRAINT [FK__vld_karya__idtyp__7B313519];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_karya__idtyp__5FF32EF8')
    ALTER TABLE [vld_karya_tulis] DROP CONSTRAINT [FK__vld_karya__idtyp__5FF32EF8];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_karya_tulis')
BEGIN
    DECLARE @reftable_81 nvarchar(60), @constraintname_81 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_karya_tulis'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_81, @constraintname_81
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_81+' drop constraint '+@constraintname_81)
        FETCH NEXT from refcursor into @reftable_81, @constraintname_81
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_karya_tulis]
END

CREATE TABLE [vld_karya_tulis]
(
    [karya_tulis_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_karya_tulis_PK] PRIMARY KEY ([karya_tulis_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_karya_tulis] ([idtype]);

CREATE INDEX [VLD_KARYA_TULIS_LU] ON [vld_karya_tulis] ([last_update]);

CREATE INDEX [PK__vld_kary__18D807D35360A2A1] ON [vld_karya_tulis] ([karya_tulis_id],[logid]);

BEGIN
ALTER TABLE [vld_karya_tulis] ADD CONSTRAINT [FK__vld_karya__karya__60E75331] FOREIGN KEY ([karya_tulis_id]) REFERENCES [karya_tulis] ([karya_tulis_id])
END
;

BEGIN
ALTER TABLE [vld_karya_tulis] ADD CONSTRAINT [FK__vld_karya__karya__7C255952] FOREIGN KEY ([karya_tulis_id]) REFERENCES [karya_tulis] ([karya_tulis_id])
END
;

BEGIN
ALTER TABLE [vld_karya_tulis] ADD CONSTRAINT [FK__vld_karya__idtyp__7B313519] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_karya_tulis] ADD CONSTRAINT [FK__vld_karya__idtyp__5FF32EF8] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- ref.table_sync
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.table_sync')
BEGIN
    DECLARE @reftable_82 nvarchar(60), @constraintname_82 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.table_sync'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_82, @constraintname_82
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_82+' drop constraint '+@constraintname_82)
        FETCH NEXT from refcursor into @reftable_82, @constraintname_82
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[table_sync]
END

CREATE TABLE [ref].[table_sync]
(
    [table_name] VARCHAR(30) NOT NULL,
    [sync_type] CHAR(1) NOT NULL,
    [sync_seq] NUMERIC(6,0) NOT NULL,
    CONSTRAINT [table_sync_PK] PRIMARY KEY ([table_name])
);

CREATE INDEX [PK__table_sy__B228A5BF336793D5] ON [ref].[table_sync] ([table_name]);

-----------------------------------------------------------------------
-- ref.bidang_usaha
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.bidang_usaha')
BEGIN
    DECLARE @reftable_83 nvarchar(60), @constraintname_83 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.bidang_usaha'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_83, @constraintname_83
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_83+' drop constraint '+@constraintname_83)
        FETCH NEXT from refcursor into @reftable_83, @constraintname_83
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[bidang_usaha]
END

CREATE TABLE [ref].[bidang_usaha]
(
    [bidang_usaha_id] CHAR(10) NOT NULL,
    [nama_bidang_usaha] VARCHAR(40) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [bidang_usaha_PK] PRIMARY KEY ([bidang_usaha_id])
);

CREATE INDEX [PK__bidang_u__A12CAC82D211B356] ON [ref].[bidang_usaha] ([bidang_usaha_id]);

-----------------------------------------------------------------------
-- dudi
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__dudi__bidang_usa__1A54DAB7')
    ALTER TABLE [dudi] DROP CONSTRAINT [FK__dudi__bidang_usa__1A54DAB7];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__dudi__bidang_usa__3592E0D8')
    ALTER TABLE [dudi] DROP CONSTRAINT [FK__dudi__bidang_usa__3592E0D8];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'dudi')
BEGIN
    DECLARE @reftable_84 nvarchar(60), @constraintname_84 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'dudi'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_84, @constraintname_84
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_84+' drop constraint '+@constraintname_84)
        FETCH NEXT from refcursor into @reftable_84, @constraintname_84
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [dudi]
END

CREATE TABLE [dudi]
(
    [dudi_id] CHAR(16) NOT NULL,
    [bidang_usaha_id] CHAR(10) NOT NULL,
    [nama_dudi] VARCHAR(80) NOT NULL,
    [npwp] CHAR(15) NULL,
    [telp_kantor] VARCHAR(20) NULL,
    [fax] VARCHAR(20) NULL,
    [contact_person] VARCHAR(50) NULL,
    [telp_cp] VARCHAR(20) NULL,
    [jabatan_cp] VARCHAR(40) NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [dudi_PK] PRIMARY KEY ([dudi_id])
);

CREATE INDEX [DUDI_BIDANG_USAHA_FK] ON [dudi] ([bidang_usaha_id]);

CREATE INDEX [PK__dudi__78C21BDB8BC77C82] ON [dudi] ([dudi_id]);

BEGIN
ALTER TABLE [dudi] ADD CONSTRAINT [FK__dudi__bidang_usa__1A54DAB7] FOREIGN KEY ([bidang_usaha_id]) REFERENCES [ref].[bidang_usaha] ([bidang_usaha_id])
END
;

BEGIN
ALTER TABLE [dudi] ADD CONSTRAINT [FK__dudi__bidang_usa__3592E0D8] FOREIGN KEY ([bidang_usaha_id]) REFERENCES [ref].[bidang_usaha] ([bidang_usaha_id])
END
;

-----------------------------------------------------------------------
-- ref.tingkat_prestasi
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.tingkat_prestasi')
BEGIN
    DECLARE @reftable_85 nvarchar(60), @constraintname_85 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.tingkat_prestasi'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_85, @constraintname_85
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_85+' drop constraint '+@constraintname_85)
        FETCH NEXT from refcursor into @reftable_85, @constraintname_85
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[tingkat_prestasi]
END

CREATE TABLE [ref].[tingkat_prestasi]
(
    [tingkat_prestasi_id] INT NOT NULL,
    [nama] VARCHAR(50) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [tingkat_prestasi_PK] PRIMARY KEY ([tingkat_prestasi_id])
);

CREATE INDEX [PK__tingkat___684E74C9FD4151E4] ON [ref].[tingkat_prestasi] ([tingkat_prestasi_id]);

-----------------------------------------------------------------------
-- vld_jurusan_sp
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_jurus__jurus__62CF9BA3')
    ALTER TABLE [vld_jurusan_sp] DROP CONSTRAINT [FK__vld_jurus__jurus__62CF9BA3];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_jurus__jurus__7E0DA1C4')
    ALTER TABLE [vld_jurusan_sp] DROP CONSTRAINT [FK__vld_jurus__jurus__7E0DA1C4];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_jurus__idtyp__7D197D8B')
    ALTER TABLE [vld_jurusan_sp] DROP CONSTRAINT [FK__vld_jurus__idtyp__7D197D8B];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_jurus__idtyp__61DB776A')
    ALTER TABLE [vld_jurusan_sp] DROP CONSTRAINT [FK__vld_jurus__idtyp__61DB776A];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_jurusan_sp')
BEGIN
    DECLARE @reftable_86 nvarchar(60), @constraintname_86 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_jurusan_sp'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_86, @constraintname_86
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_86+' drop constraint '+@constraintname_86)
        FETCH NEXT from refcursor into @reftable_86, @constraintname_86
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_jurusan_sp]
END

CREATE TABLE [vld_jurusan_sp]
(
    [jurusan_sp_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_jurusan_sp_PK] PRIMARY KEY ([jurusan_sp_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_jurusan_sp] ([idtype]);

CREATE INDEX [PK__vld_juru__0F233BA9E9C94985] ON [vld_jurusan_sp] ([jurusan_sp_id],[logid]);

BEGIN
ALTER TABLE [vld_jurusan_sp] ADD CONSTRAINT [FK__vld_jurus__jurus__62CF9BA3] FOREIGN KEY ([jurusan_sp_id]) REFERENCES [jurusan_sp] ([jurusan_sp_id])
END
;

BEGIN
ALTER TABLE [vld_jurusan_sp] ADD CONSTRAINT [FK__vld_jurus__jurus__7E0DA1C4] FOREIGN KEY ([jurusan_sp_id]) REFERENCES [jurusan_sp] ([jurusan_sp_id])
END
;

BEGIN
ALTER TABLE [vld_jurusan_sp] ADD CONSTRAINT [FK__vld_jurus__idtyp__7D197D8B] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_jurusan_sp] ADD CONSTRAINT [FK__vld_jurus__idtyp__61DB776A] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- ref.kelompok_bidang
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__kelompok___level__79FD19BE')
    ALTER TABLE [ref].[kelompok_bidang] DROP CONSTRAINT [FK__kelompok___level__79FD19BE];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.kelompok_bidang')
BEGIN
    DECLARE @reftable_87 nvarchar(60), @constraintname_87 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.kelompok_bidang'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_87, @constraintname_87
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_87+' drop constraint '+@constraintname_87)
        FETCH NEXT from refcursor into @reftable_87, @constraintname_87
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[kelompok_bidang]
END

CREATE TABLE [ref].[kelompok_bidang]
(
    [level_bidang_id] VARCHAR(5) NOT NULL,
    [nama_level_bidang] VARCHAR(25) NOT NULL,
    [untuk_sma] NUMERIC(3,0) NOT NULL,
    [untuk_smk] NUMERIC(3,0) NOT NULL,
    [untuk_pt] NUMERIC(3,0) NOT NULL,
    [untuk_slb] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [untuk_smklb] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [level_bidang_induk] VARCHAR(5) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [kelompok_bidang_PK] PRIMARY KEY ([level_bidang_id])
);

CREATE INDEX [INDUK_KELOMPOK_FK] ON [ref].[kelompok_bidang] ([level_bidang_induk]);

CREATE INDEX [PK__kelompok__8471B7A0439DF23A] ON [ref].[kelompok_bidang] ([level_bidang_id]);

BEGIN
ALTER TABLE [ref].[kelompok_bidang] ADD CONSTRAINT [FK__kelompok___level__79FD19BE] FOREIGN KEY ([level_bidang_induk]) REFERENCES [ref].[kelompok_bidang] ([level_bidang_id])
END
;

-----------------------------------------------------------------------
-- mou
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__mou__dudi_id__1B48FEF0')
    ALTER TABLE [mou] DROP CONSTRAINT [FK__mou__dudi_id__1B48FEF0];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__mou__dudi_id__36870511')
    ALTER TABLE [mou] DROP CONSTRAINT [FK__mou__dudi_id__36870511];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__mou__sekolah_id__377B294A')
    ALTER TABLE [mou] DROP CONSTRAINT [FK__mou__sekolah_id__377B294A];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__mou__sekolah_id__1C3D2329')
    ALTER TABLE [mou] DROP CONSTRAINT [FK__mou__sekolah_id__1C3D2329];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'mou')
BEGIN
    DECLARE @reftable_88 nvarchar(60), @constraintname_88 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'mou'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_88, @constraintname_88
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_88+' drop constraint '+@constraintname_88)
        FETCH NEXT from refcursor into @reftable_88, @constraintname_88
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [mou]
END

CREATE TABLE [mou]
(
    [mou_id] CHAR(16) NOT NULL,
    [dudi_id] CHAR(16) NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [nomor_mou] VARCHAR(40) NOT NULL,
    [judul_mou] VARCHAR(80) NOT NULL,
    [tanggal_mulai] VARCHAR(20) NOT NULL,
    [tanggal_selesai] VARCHAR(20) NOT NULL,
    [nama_dudi] VARCHAR(80) NOT NULL,
    [npwp_dudi] CHAR(15) NULL,
    [nama_bidang_usaha] VARCHAR(40) NOT NULL,
    [telp_kantor] VARCHAR(20) NULL,
    [fax] VARCHAR(20) NULL,
    [contact_person] VARCHAR(50) NULL,
    [telp_cp] VARCHAR(20) NULL,
    [jabatan_cp] VARCHAR(40) NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [mou_PK] PRIMARY KEY ([mou_id])
);

CREATE INDEX [MOU_DUDI_FK] ON [mou] ([dudi_id]);

CREATE INDEX [MOU_LU] ON [mou] ([Last_update]);

CREATE INDEX [MOU_SEKOLAH_FK] ON [mou] ([sekolah_id]);

CREATE INDEX [PK__mou__8651188328B4D479] ON [mou] ([mou_id]);

BEGIN
ALTER TABLE [mou] ADD CONSTRAINT [FK__mou__dudi_id__1B48FEF0] FOREIGN KEY ([dudi_id]) REFERENCES [dudi] ([dudi_id])
END
;

BEGIN
ALTER TABLE [mou] ADD CONSTRAINT [FK__mou__dudi_id__36870511] FOREIGN KEY ([dudi_id]) REFERENCES [dudi] ([dudi_id])
END
;

BEGIN
ALTER TABLE [mou] ADD CONSTRAINT [FK__mou__sekolah_id__377B294A] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [mou] ADD CONSTRAINT [FK__mou__sekolah_id__1C3D2329] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

-----------------------------------------------------------------------
-- ref.tingkat_penghargaan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.tingkat_penghargaan')
BEGIN
    DECLARE @reftable_89 nvarchar(60), @constraintname_89 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.tingkat_penghargaan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_89, @constraintname_89
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_89+' drop constraint '+@constraintname_89)
        FETCH NEXT from refcursor into @reftable_89, @constraintname_89
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[tingkat_penghargaan]
END

CREATE TABLE [ref].[tingkat_penghargaan]
(
    [tingkat_penghargaan_id] INT NOT NULL,
    [nama] VARCHAR(50) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [tingkat_penghargaan_PK] PRIMARY KEY ([tingkat_penghargaan_id])
);

CREATE INDEX [PK__tingkat___49BFBE336DE77D05] ON [ref].[tingkat_penghargaan] ([tingkat_penghargaan_id]);

-----------------------------------------------------------------------
-- vld_inpassing
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_inpas__inpas__64B7E415')
    ALTER TABLE [vld_inpassing] DROP CONSTRAINT [FK__vld_inpas__inpas__64B7E415];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_inpas__inpas__7FF5EA36')
    ALTER TABLE [vld_inpassing] DROP CONSTRAINT [FK__vld_inpas__inpas__7FF5EA36];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_inpas__idtyp__7F01C5FD')
    ALTER TABLE [vld_inpassing] DROP CONSTRAINT [FK__vld_inpas__idtyp__7F01C5FD];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_inpas__idtyp__63C3BFDC')
    ALTER TABLE [vld_inpassing] DROP CONSTRAINT [FK__vld_inpas__idtyp__63C3BFDC];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_inpassing')
BEGIN
    DECLARE @reftable_90 nvarchar(60), @constraintname_90 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_inpassing'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_90, @constraintname_90
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_90+' drop constraint '+@constraintname_90)
        FETCH NEXT from refcursor into @reftable_90, @constraintname_90
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_inpassing]
END

CREATE TABLE [vld_inpassing]
(
    [inpassing_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_inpassing_PK] PRIMARY KEY ([inpassing_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_inpassing] ([idtype]);

CREATE INDEX [VLD_INPASSING_LU] ON [vld_inpassing] ([last_update]);

CREATE INDEX [PK__vld_inpa__B36F26E91ADC7CAC] ON [vld_inpassing] ([inpassing_id],[logid]);

BEGIN
ALTER TABLE [vld_inpassing] ADD CONSTRAINT [FK__vld_inpas__inpas__64B7E415] FOREIGN KEY ([inpassing_id]) REFERENCES [inpassing] ([inpassing_id])
END
;

BEGIN
ALTER TABLE [vld_inpassing] ADD CONSTRAINT [FK__vld_inpas__inpas__7FF5EA36] FOREIGN KEY ([inpassing_id]) REFERENCES [inpassing] ([inpassing_id])
END
;

BEGIN
ALTER TABLE [vld_inpassing] ADD CONSTRAINT [FK__vld_inpas__idtyp__7F01C5FD] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_inpassing] ADD CONSTRAINT [FK__vld_inpas__idtyp__63C3BFDC] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- unit_usaha
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__unit_usah__sekol__396371BC')
    ALTER TABLE [unit_usaha] DROP CONSTRAINT [FK__unit_usah__sekol__396371BC];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__unit_usah__sekol__1E256B9B')
    ALTER TABLE [unit_usaha] DROP CONSTRAINT [FK__unit_usah__sekol__1E256B9B];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__unit_usah__kelom__1D314762')
    ALTER TABLE [unit_usaha] DROP CONSTRAINT [FK__unit_usah__kelom__1D314762];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__unit_usah__kelom__386F4D83')
    ALTER TABLE [unit_usaha] DROP CONSTRAINT [FK__unit_usah__kelom__386F4D83];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'unit_usaha')
BEGIN
    DECLARE @reftable_91 nvarchar(60), @constraintname_91 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'unit_usaha'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_91, @constraintname_91
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_91+' drop constraint '+@constraintname_91)
        FETCH NEXT from refcursor into @reftable_91, @constraintname_91
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [unit_usaha]
END

CREATE TABLE [unit_usaha]
(
    [unit_usaha_id] CHAR(16) NOT NULL,
    [kelompok_usaha_id] CHAR(8) NOT NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [nama_unit_usaha] VARCHAR(80) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [unit_usaha_PK] PRIMARY KEY ([unit_usaha_id])
);

CREATE INDEX [UNIT_USAHA_KELOMPOK_FK] ON [unit_usaha] ([kelompok_usaha_id]);

CREATE INDEX [UNIT_USAHA_LU] ON [unit_usaha] ([Last_update]);

CREATE INDEX [UNIT_USAHA_SEKOLAH_FK] ON [unit_usaha] ([sekolah_id]);

CREATE INDEX [PK__unit_usa__77913B50F3ACFDDB] ON [unit_usaha] ([unit_usaha_id]);

BEGIN
ALTER TABLE [unit_usaha] ADD CONSTRAINT [FK__unit_usah__sekol__396371BC] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [unit_usaha] ADD CONSTRAINT [FK__unit_usah__sekol__1E256B9B] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [unit_usaha] ADD CONSTRAINT [FK__unit_usah__kelom__1D314762] FOREIGN KEY ([kelompok_usaha_id]) REFERENCES [ref].[kelompok_usaha] ([kelompok_usaha_id])
END
;

BEGIN
ALTER TABLE [unit_usaha] ADD CONSTRAINT [FK__unit_usah__kelom__386F4D83] FOREIGN KEY ([kelompok_usaha_id]) REFERENCES [ref].[kelompok_usaha] ([kelompok_usaha_id])
END
;

-----------------------------------------------------------------------
-- ref.sumber_gaji
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.sumber_gaji')
BEGIN
    DECLARE @reftable_92 nvarchar(60), @constraintname_92 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.sumber_gaji'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_92, @constraintname_92
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_92+' drop constraint '+@constraintname_92)
        FETCH NEXT from refcursor into @reftable_92, @constraintname_92
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[sumber_gaji]
END

CREATE TABLE [ref].[sumber_gaji]
(
    [sumber_gaji_id] NUMERIC(4,0) NOT NULL,
    [nama] VARCHAR(40) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [sumber_gaji_PK] PRIMARY KEY ([sumber_gaji_id])
);

CREATE INDEX [PK__sumber_g__168ABD2403CADFDF] ON [ref].[sumber_gaji] ([sumber_gaji_id]);

-----------------------------------------------------------------------
-- vld_demografi
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_demog__demog__01DE32A8')
    ALTER TABLE [vld_demografi] DROP CONSTRAINT [FK__vld_demog__demog__01DE32A8];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_demog__demog__66A02C87')
    ALTER TABLE [vld_demografi] DROP CONSTRAINT [FK__vld_demog__demog__66A02C87];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_demog__idtyp__00EA0E6F')
    ALTER TABLE [vld_demografi] DROP CONSTRAINT [FK__vld_demog__idtyp__00EA0E6F];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_demog__idtyp__65AC084E')
    ALTER TABLE [vld_demografi] DROP CONSTRAINT [FK__vld_demog__idtyp__65AC084E];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_demografi')
BEGIN
    DECLARE @reftable_93 nvarchar(60), @constraintname_93 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_demografi'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_93, @constraintname_93
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_93+' drop constraint '+@constraintname_93)
        FETCH NEXT from refcursor into @reftable_93, @constraintname_93
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_demografi]
END

CREATE TABLE [vld_demografi]
(
    [demografi_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_demografi_PK] PRIMARY KEY ([demografi_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_demografi] ([idtype]);

CREATE INDEX [VLD_DEMO_LU] ON [vld_demografi] ([last_update]);

CREATE INDEX [PK__vld_demo__AD91965FBAFC7A38] ON [vld_demografi] ([demografi_id],[logid]);

BEGIN
ALTER TABLE [vld_demografi] ADD CONSTRAINT [FK__vld_demog__demog__01DE32A8] FOREIGN KEY ([demografi_id]) REFERENCES [demografi] ([demografi_id])
END
;

BEGIN
ALTER TABLE [vld_demografi] ADD CONSTRAINT [FK__vld_demog__demog__66A02C87] FOREIGN KEY ([demografi_id]) REFERENCES [demografi] ([demografi_id])
END
;

BEGIN
ALTER TABLE [vld_demografi] ADD CONSTRAINT [FK__vld_demog__idtyp__00EA0E6F] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_demografi] ADD CONSTRAINT [FK__vld_demog__idtyp__65AC084E] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- ref.jenis_bantuan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.jenis_bantuan')
BEGIN
    DECLARE @reftable_94 nvarchar(60), @constraintname_94 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.jenis_bantuan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_94, @constraintname_94
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_94+' drop constraint '+@constraintname_94)
        FETCH NEXT from refcursor into @reftable_94, @constraintname_94
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[jenis_bantuan]
END

CREATE TABLE [ref].[jenis_bantuan]
(
    [jenis_bantuan_id] INT NOT NULL,
    [nama] VARCHAR(50) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [jenis_bantuan_PK] PRIMARY KEY ([jenis_bantuan_id])
);

CREATE INDEX [PK__jenis_ba__CB425BB83A212664] ON [ref].[jenis_bantuan] ([jenis_bantuan_id]);

-----------------------------------------------------------------------
-- jurusan_kerjasama
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__jurusan_k__jurus__200DB40D')
    ALTER TABLE [jurusan_kerjasama] DROP CONSTRAINT [FK__jurusan_k__jurus__200DB40D];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__jurusan_k__jurus__3B4BBA2E')
    ALTER TABLE [jurusan_kerjasama] DROP CONSTRAINT [FK__jurusan_k__jurus__3B4BBA2E];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__jurusan_k__mou_i__1F198FD4')
    ALTER TABLE [jurusan_kerjasama] DROP CONSTRAINT [FK__jurusan_k__mou_i__1F198FD4];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__jurusan_k__mou_i__3A5795F5')
    ALTER TABLE [jurusan_kerjasama] DROP CONSTRAINT [FK__jurusan_k__mou_i__3A5795F5];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'jurusan_kerjasama')
BEGIN
    DECLARE @reftable_95 nvarchar(60), @constraintname_95 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'jurusan_kerjasama'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_95, @constraintname_95
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_95+' drop constraint '+@constraintname_95)
        FETCH NEXT from refcursor into @reftable_95, @constraintname_95
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [jurusan_kerjasama]
END

CREATE TABLE [jurusan_kerjasama]
(
    [mou_id] CHAR(16) NOT NULL,
    [jurusan_sp_id] CHAR(16) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [jurusan_kerjasama_PK] PRIMARY KEY ([mou_id],[jurusan_sp_id])
);

CREATE INDEX [JUR_KS_LU] ON [jurusan_kerjasama] ([Last_update]);

CREATE INDEX [KERJASAMA_JURUSAN_FK] ON [jurusan_kerjasama] ([jurusan_sp_id]);

CREATE INDEX [PK__jurusan___62DB13CA4B01A910] ON [jurusan_kerjasama] ([mou_id],[jurusan_sp_id]);

BEGIN
ALTER TABLE [jurusan_kerjasama] ADD CONSTRAINT [FK__jurusan_k__jurus__200DB40D] FOREIGN KEY ([jurusan_sp_id]) REFERENCES [jurusan_sp] ([jurusan_sp_id])
END
;

BEGIN
ALTER TABLE [jurusan_kerjasama] ADD CONSTRAINT [FK__jurusan_k__jurus__3B4BBA2E] FOREIGN KEY ([jurusan_sp_id]) REFERENCES [jurusan_sp] ([jurusan_sp_id])
END
;

BEGIN
ALTER TABLE [jurusan_kerjasama] ADD CONSTRAINT [FK__jurusan_k__mou_i__1F198FD4] FOREIGN KEY ([mou_id]) REFERENCES [mou] ([mou_id])
END
;

BEGIN
ALTER TABLE [jurusan_kerjasama] ADD CONSTRAINT [FK__jurusan_k__mou_i__3A5795F5] FOREIGN KEY ([mou_id]) REFERENCES [mou] ([mou_id])
END
;

-----------------------------------------------------------------------
-- unit_usaha_kerjasama
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__unit_usah__mou_i__21F5FC7F')
    ALTER TABLE [unit_usaha_kerjasama] DROP CONSTRAINT [FK__unit_usah__mou_i__21F5FC7F];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__unit_usah__mou_i__3D3402A0')
    ALTER TABLE [unit_usaha_kerjasama] DROP CONSTRAINT [FK__unit_usah__mou_i__3D3402A0];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__unit_usah__unit___2101D846')
    ALTER TABLE [unit_usaha_kerjasama] DROP CONSTRAINT [FK__unit_usah__unit___2101D846];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__unit_usah__unit___3C3FDE67')
    ALTER TABLE [unit_usaha_kerjasama] DROP CONSTRAINT [FK__unit_usah__unit___3C3FDE67];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__unit_usah__sumbe__22EA20B8')
    ALTER TABLE [unit_usaha_kerjasama] DROP CONSTRAINT [FK__unit_usah__sumbe__22EA20B8];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__unit_usah__sumbe__3E2826D9')
    ALTER TABLE [unit_usaha_kerjasama] DROP CONSTRAINT [FK__unit_usah__sumbe__3E2826D9];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'unit_usaha_kerjasama')
BEGIN
    DECLARE @reftable_96 nvarchar(60), @constraintname_96 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'unit_usaha_kerjasama'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_96, @constraintname_96
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_96+' drop constraint '+@constraintname_96)
        FETCH NEXT from refcursor into @reftable_96, @constraintname_96
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [unit_usaha_kerjasama]
END

CREATE TABLE [unit_usaha_kerjasama]
(
    [mou_id] CHAR(16) NOT NULL,
    [unit_usaha_id] CHAR(16) NOT NULL,
    [sumber_dana_id] NUMERIC(5,0) NOT NULL,
    [produk_barang_dihasilkan] VARCHAR(200) NULL,
    [jasa_produksi_dihasilkan] VARCHAR(200) NULL,
    [omzet_barang_perbulan] NUMERIC(20,0) NULL,
    [omzet_jasa_perbulan] NUMERIC(20,0) NULL,
    [prestasi_penghargaan] VARCHAR(200) NULL,
    [pangsa_pasar_produk] VARCHAR(200) NULL,
    [pangsa_pasar_jasa] VARCHAR(200) NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [unit_usaha_kerjasama_PK] PRIMARY KEY ([mou_id],[unit_usaha_id])
);

CREATE INDEX [KERJASAMA_UNIT_USAHA_FK] ON [unit_usaha_kerjasama] ([unit_usaha_id]);

CREATE INDEX [UNIT_KS_LU] ON [unit_usaha_kerjasama] ([Last_update]);

CREATE INDEX [UNIT_USAHA_SUMBER_DANA_FK] ON [unit_usaha_kerjasama] ([sumber_dana_id]);

CREATE INDEX [PK__unit_usa__81280B37EC6A6A1D] ON [unit_usaha_kerjasama] ([mou_id],[unit_usaha_id]);

BEGIN
ALTER TABLE [unit_usaha_kerjasama] ADD CONSTRAINT [FK__unit_usah__mou_i__21F5FC7F] FOREIGN KEY ([mou_id]) REFERENCES [mou] ([mou_id])
END
;

BEGIN
ALTER TABLE [unit_usaha_kerjasama] ADD CONSTRAINT [FK__unit_usah__mou_i__3D3402A0] FOREIGN KEY ([mou_id]) REFERENCES [mou] ([mou_id])
END
;

BEGIN
ALTER TABLE [unit_usaha_kerjasama] ADD CONSTRAINT [FK__unit_usah__unit___2101D846] FOREIGN KEY ([unit_usaha_id]) REFERENCES [unit_usaha] ([unit_usaha_id])
END
;

BEGIN
ALTER TABLE [unit_usaha_kerjasama] ADD CONSTRAINT [FK__unit_usah__unit___3C3FDE67] FOREIGN KEY ([unit_usaha_id]) REFERENCES [unit_usaha] ([unit_usaha_id])
END
;

BEGIN
ALTER TABLE [unit_usaha_kerjasama] ADD CONSTRAINT [FK__unit_usah__sumbe__22EA20B8] FOREIGN KEY ([sumber_dana_id]) REFERENCES [ref].[sumber_dana] ([sumber_dana_id])
END
;

BEGIN
ALTER TABLE [unit_usaha_kerjasama] ADD CONSTRAINT [FK__unit_usah__sumbe__3E2826D9] FOREIGN KEY ([sumber_dana_id]) REFERENCES [ref].[sumber_dana] ([sumber_dana_id])
END
;

-----------------------------------------------------------------------
-- ref.kelompok_usaha
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.kelompok_usaha')
BEGIN
    DECLARE @reftable_97 nvarchar(60), @constraintname_97 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.kelompok_usaha'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_97, @constraintname_97
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_97+' drop constraint '+@constraintname_97)
        FETCH NEXT from refcursor into @reftable_97, @constraintname_97
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[kelompok_usaha]
END

CREATE TABLE [ref].[kelompok_usaha]
(
    [kelompok_usaha_id] CHAR(8) NOT NULL,
    [nama_kelompok_usaha] VARCHAR(60) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [kelompok_usaha_PK] PRIMARY KEY ([kelompok_usaha_id])
);

CREATE INDEX [PK__kelompok__C27FCC5A722E77B3] ON [ref].[kelompok_usaha] ([kelompok_usaha_id]);

-----------------------------------------------------------------------
-- vld_buku_ptk
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_buku___buku___03C67B1A')
    ALTER TABLE [vld_buku_ptk] DROP CONSTRAINT [FK__vld_buku___buku___03C67B1A];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_buku___buku___688874F9')
    ALTER TABLE [vld_buku_ptk] DROP CONSTRAINT [FK__vld_buku___buku___688874F9];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_buku___idtyp__02D256E1')
    ALTER TABLE [vld_buku_ptk] DROP CONSTRAINT [FK__vld_buku___idtyp__02D256E1];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_buku___idtyp__679450C0')
    ALTER TABLE [vld_buku_ptk] DROP CONSTRAINT [FK__vld_buku___idtyp__679450C0];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_buku_ptk')
BEGIN
    DECLARE @reftable_98 nvarchar(60), @constraintname_98 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_buku_ptk'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_98, @constraintname_98
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_98+' drop constraint '+@constraintname_98)
        FETCH NEXT from refcursor into @reftable_98, @constraintname_98
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_buku_ptk]
END

CREATE TABLE [vld_buku_ptk]
(
    [buku_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_buku_ptk_PK] PRIMARY KEY ([buku_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_buku_ptk] ([idtype]);

CREATE INDEX [VLD_BUKU_PTK_LU] ON [vld_buku_ptk] ([last_update]);

CREATE INDEX [PK__vld_buku__E8F7812FB22291B4] ON [vld_buku_ptk] ([buku_id],[logid]);

BEGIN
ALTER TABLE [vld_buku_ptk] ADD CONSTRAINT [FK__vld_buku___buku___03C67B1A] FOREIGN KEY ([buku_id]) REFERENCES [buku_ptk] ([buku_id])
END
;

BEGIN
ALTER TABLE [vld_buku_ptk] ADD CONSTRAINT [FK__vld_buku___buku___688874F9] FOREIGN KEY ([buku_id]) REFERENCES [buku_ptk] ([buku_id])
END
;

BEGIN
ALTER TABLE [vld_buku_ptk] ADD CONSTRAINT [FK__vld_buku___idtyp__02D256E1] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_buku_ptk] ADD CONSTRAINT [FK__vld_buku___idtyp__679450C0] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- ref.jenis_buku_alat
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.jenis_buku_alat')
BEGIN
    DECLARE @reftable_99 nvarchar(60), @constraintname_99 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.jenis_buku_alat'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_99, @constraintname_99
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_99+' drop constraint '+@constraintname_99)
        FETCH NEXT from refcursor into @reftable_99, @constraintname_99
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[jenis_buku_alat]
END

CREATE TABLE [ref].[jenis_buku_alat]
(
    [jenis_buku_alat_id] NUMERIC(8,0) NOT NULL,
    [jenis_buku_alat] VARCHAR(60) NOT NULL,
    [spm_qty_min_per_siswa] NUMERIC(5,1) DEFAULT ((-1)) NOT NULL,
    [spm_qty_min_per_sekolah] NUMERIC(6,0) DEFAULT ((-1)) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [jenis_buku_alat_PK] PRIMARY KEY ([jenis_buku_alat_id])
);

CREATE INDEX [PK__jenis_bu__D098BAA103625FF6] ON [ref].[jenis_buku_alat] ([jenis_buku_alat_id]);

-----------------------------------------------------------------------
-- sync_session
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sync_sess__sekol__3F1C4B12')
    ALTER TABLE [sync_session] DROP CONSTRAINT [FK__sync_sess__sekol__3F1C4B12];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sync_sess__sekol__23DE44F1')
    ALTER TABLE [sync_session] DROP CONSTRAINT [FK__sync_sess__sekol__23DE44F1];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'sync_session')
BEGIN
    DECLARE @reftable_100 nvarchar(60), @constraintname_100 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'sync_session'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_100, @constraintname_100
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_100+' drop constraint '+@constraintname_100)
        FETCH NEXT from refcursor into @reftable_100, @constraintname_100
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [sync_session]
END

CREATE TABLE [sync_session]
(
    [token] CHAR(16) NOT NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [pengguna_id] CHAR(16) NOT NULL,
    [create_time] DATETIME(16,3) NOT NULL,
    [last_activity] DATETIME(16,3) NULL,
    CONSTRAINT [sync_session_PK] PRIMARY KEY ([token])
);

CREATE INDEX [CREATE_TIME] ON [sync_session] ([create_time]);

CREATE INDEX [SYNCSESSION_SP_FK] ON [sync_session] ([sekolah_id]);

CREATE INDEX [PK__sync_ses__CA90DA7AB7334C79] ON [sync_session] ([token]);

BEGIN
ALTER TABLE [sync_session] ADD CONSTRAINT [FK__sync_sess__sekol__3F1C4B12] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [sync_session] ADD CONSTRAINT [FK__sync_sess__sekol__23DE44F1] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

-----------------------------------------------------------------------
-- rwy_kepangkatan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_kepan__ptk_i__48DABF76')
    ALTER TABLE [rwy_kepangkatan] DROP CONSTRAINT [FK__rwy_kepan__ptk_i__48DABF76];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_kepan__ptk_i__2D9CB955')
    ALTER TABLE [rwy_kepangkatan] DROP CONSTRAINT [FK__rwy_kepan__ptk_i__2D9CB955];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_kepan__pangk__2CA8951C')
    ALTER TABLE [rwy_kepangkatan] DROP CONSTRAINT [FK__rwy_kepan__pangk__2CA8951C];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_kepan__pangk__47E69B3D')
    ALTER TABLE [rwy_kepangkatan] DROP CONSTRAINT [FK__rwy_kepan__pangk__47E69B3D];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'rwy_kepangkatan')
BEGIN
    DECLARE @reftable_101 nvarchar(60), @constraintname_101 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'rwy_kepangkatan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_101, @constraintname_101
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_101+' drop constraint '+@constraintname_101)
        FETCH NEXT from refcursor into @reftable_101, @constraintname_101
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [rwy_kepangkatan]
END

CREATE TABLE [rwy_kepangkatan]
(
    [riwayat_kepangkatan_id] CHAR(16) NOT NULL,
    [ptk_id] CHAR(16) NOT NULL,
    [pangkat_golongan_id] NUMERIC(4,0) NOT NULL,
    [nomor_sk] VARCHAR(40) NOT NULL,
    [tanggal_sk] VARCHAR(20) NOT NULL,
    [tmt_pangkat] VARCHAR(20) NOT NULL,
    [masa_kerja_gol_tahun] NUMERIC(4,0) NOT NULL,
    [masa_kerja_gol_bulan] NUMERIC(4,0) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [rwy_kepangkatan_PK] PRIMARY KEY ([riwayat_kepangkatan_id])
);

CREATE INDEX [RIWAYAT_PANG_GOL_FK] ON [rwy_kepangkatan] ([pangkat_golongan_id]);

CREATE INDEX [RIWAYAT_PANGKAT_PTK_FK] ON [rwy_kepangkatan] ([ptk_id]);

CREATE INDEX [RWY_PANG_LU] ON [rwy_kepangkatan] ([Last_update]);

CREATE INDEX [PK__rwy_kepa__15A348E96DB14AA1] ON [rwy_kepangkatan] ([riwayat_kepangkatan_id]);

BEGIN
ALTER TABLE [rwy_kepangkatan] ADD CONSTRAINT [FK__rwy_kepan__ptk_i__48DABF76] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [rwy_kepangkatan] ADD CONSTRAINT [FK__rwy_kepan__ptk_i__2D9CB955] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [rwy_kepangkatan] ADD CONSTRAINT [FK__rwy_kepan__pangk__2CA8951C] FOREIGN KEY ([pangkat_golongan_id]) REFERENCES [ref].[pangkat_golongan] ([pangkat_golongan_id])
END
;

BEGIN
ALTER TABLE [rwy_kepangkatan] ADD CONSTRAINT [FK__rwy_kepan__pangk__47E69B3D] FOREIGN KEY ([pangkat_golongan_id]) REFERENCES [ref].[pangkat_golongan] ([pangkat_golongan_id])
END
;

-----------------------------------------------------------------------
-- ref.jenis_sertifikasi
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__jenis_ser__kebut__7CD98669')
    ALTER TABLE [ref].[jenis_sertifikasi] DROP CONSTRAINT [FK__jenis_ser__kebut__7CD98669];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.jenis_sertifikasi')
BEGIN
    DECLARE @reftable_102 nvarchar(60), @constraintname_102 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.jenis_sertifikasi'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_102, @constraintname_102
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_102+' drop constraint '+@constraintname_102)
        FETCH NEXT from refcursor into @reftable_102, @constraintname_102
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[jenis_sertifikasi]
END

CREATE TABLE [ref].[jenis_sertifikasi]
(
    [id_jenis_sertifikasi] NUMERIC(5,0) NOT NULL,
    [jenis_sertifikasi] VARCHAR(30) NOT NULL,
    [prof_guru] NUMERIC(3,0) NOT NULL,
    [kepala_sekolah] NUMERIC(3,0) NOT NULL,
    [laboran] NUMERIC(3,0) NOT NULL,
    [kebutuhan_khusus_id] INT NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [jenis_sertifikasi_PK] PRIMARY KEY ([id_jenis_sertifikasi])
);

CREATE INDEX [SERTIFIKASI_KK_FK] ON [ref].[jenis_sertifikasi] ([kebutuhan_khusus_id]);

CREATE INDEX [PK__jenis_se__895F19A5F66090A9] ON [ref].[jenis_sertifikasi] ([id_jenis_sertifikasi]);

BEGIN
ALTER TABLE [ref].[jenis_sertifikasi] ADD CONSTRAINT [FK__jenis_ser__kebut__7CD98669] FOREIGN KEY ([kebutuhan_khusus_id]) REFERENCES [ref].[kebutuhan_khusus] ([kebutuhan_khusus_id])
END
;

-----------------------------------------------------------------------
-- vld_bea_ptk
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_bea_p__beasi__05AEC38C')
    ALTER TABLE [vld_bea_ptk] DROP CONSTRAINT [FK__vld_bea_p__beasi__05AEC38C];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_bea_p__beasi__6A70BD6B')
    ALTER TABLE [vld_bea_ptk] DROP CONSTRAINT [FK__vld_bea_p__beasi__6A70BD6B];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_bea_p__idtyp__04BA9F53')
    ALTER TABLE [vld_bea_ptk] DROP CONSTRAINT [FK__vld_bea_p__idtyp__04BA9F53];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_bea_p__idtyp__697C9932')
    ALTER TABLE [vld_bea_ptk] DROP CONSTRAINT [FK__vld_bea_p__idtyp__697C9932];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_bea_ptk')
BEGIN
    DECLARE @reftable_103 nvarchar(60), @constraintname_103 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_bea_ptk'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_103, @constraintname_103
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_103+' drop constraint '+@constraintname_103)
        FETCH NEXT from refcursor into @reftable_103, @constraintname_103
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_bea_ptk]
END

CREATE TABLE [vld_bea_ptk]
(
    [beasiswa_ptk_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_bea_ptk_PK] PRIMARY KEY ([beasiswa_ptk_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_bea_ptk] ([idtype]);

CREATE INDEX [VLD_BEA_PTK_LU] ON [vld_bea_ptk] ([last_update]);

CREATE INDEX [PK__vld_bea___DAEA5A43DE5EDF9E] ON [vld_bea_ptk] ([beasiswa_ptk_id],[logid]);

BEGIN
ALTER TABLE [vld_bea_ptk] ADD CONSTRAINT [FK__vld_bea_p__beasi__05AEC38C] FOREIGN KEY ([beasiswa_ptk_id]) REFERENCES [beasiswa_ptk] ([beasiswa_ptk_id])
END
;

BEGIN
ALTER TABLE [vld_bea_ptk] ADD CONSTRAINT [FK__vld_bea_p__beasi__6A70BD6B] FOREIGN KEY ([beasiswa_ptk_id]) REFERENCES [beasiswa_ptk] ([beasiswa_ptk_id])
END
;

BEGIN
ALTER TABLE [vld_bea_ptk] ADD CONSTRAINT [FK__vld_bea_p__idtyp__04BA9F53] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_bea_ptk] ADD CONSTRAINT [FK__vld_bea_p__idtyp__697C9932] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- ptk
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__entry_sekol__4AC307E8')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__entry_sekol__4AC307E8];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__entry_sekol__2F8501C7')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__entry_sekol__2F8501C7];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__agama_id__3A02903A')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__agama_id__3A02903A];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__agama_id__5540965B')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__agama_id__5540965B];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__pengawas_bi__3726238F')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__pengawas_bi__3726238F];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__pengawas_bi__526429B0')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__pengawas_bi__526429B0];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__jenis_ptk_i__30792600')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__jenis_ptk_i__30792600];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__jenis_ptk_i__4BB72C21')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__jenis_ptk_i__4BB72C21];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__keahlian_la__381A47C8')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__keahlian_la__381A47C8];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__keahlian_la__53584DE9')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__keahlian_la__53584DE9];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__mampu_handl__4D9F7493')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__mampu_handl__4D9F7493];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__mampu_handl__32616E72')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__mampu_handl__32616E72];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__lembaga_pen__335592AB')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__lembaga_pen__335592AB];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__lembaga_pen__4E9398CC')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__lembaga_pen__4E9398CC];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__kode_wilaya__4CAB505A')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__kode_wilaya__4CAB505A];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__kode_wilaya__316D4A39')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__kode_wilaya__316D4A39];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__kewarganega__2E90DD8E')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__kewarganega__2E90DD8E];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__kewarganega__49CEE3AF')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__kewarganega__49CEE3AF];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__pangkat_gol__3631FF56')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__pangkat_gol__3631FF56];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__pangkat_gol__51700577')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__pangkat_gol__51700577];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__pekerjaan_s__390E6C01')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__pekerjaan_s__390E6C01];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__pekerjaan_s__544C7222')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__pekerjaan_s__544C7222];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__status_kepe__3AF6B473')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__status_kepe__3AF6B473];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__status_kepe__5634BA94')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__status_kepe__5634BA94];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__status_keak__3449B6E4')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__status_keak__3449B6E4];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__status_keak__4F87BD05')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__status_keak__4F87BD05];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__sumber_gaji__353DDB1D')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__sumber_gaji__353DDB1D];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk__sumber_gaji__507BE13E')
    ALTER TABLE [ptk] DROP CONSTRAINT [FK__ptk__sumber_gaji__507BE13E];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ptk')
BEGIN
    DECLARE @reftable_104 nvarchar(60), @constraintname_104 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ptk'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_104, @constraintname_104
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_104+' drop constraint '+@constraintname_104)
        FETCH NEXT from refcursor into @reftable_104, @constraintname_104
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ptk]
END

CREATE TABLE [ptk]
(
    [ptk_id] CHAR(16) NOT NULL,
    [nama] VARCHAR(50) NOT NULL,
    [nip] VARCHAR(18) NULL,
    [jenis_kelamin] CHAR(1) NOT NULL,
    [tempat_lahir] VARCHAR(20) NOT NULL,
    [tanggal_lahir] VARCHAR(20) NOT NULL,
    [nik] CHAR(16) NOT NULL,
    [niy_nigk] VARCHAR(30) NULL,
    [nuptk] CHAR(16) NULL,
    [status_kepegawaian_id] SMALLINT(2,0) NOT NULL,
    [jenis_ptk_id] NUMERIC(4,0) NOT NULL,
    [pengawas_bidang_studi_id] INT NULL,
    [agama_id] SMALLINT(2,0) NOT NULL,
    [kewarganegaraan] CHAR(2) NOT NULL,
    [alamat_jalan] VARCHAR(80) NOT NULL,
    [rt] NUMERIC(4,0) NULL,
    [rw] NUMERIC(4,0) NULL,
    [nama_dusun] VARCHAR(40) NULL,
    [desa_kelurahan] VARCHAR(40) NOT NULL,
    [kode_wilayah] CHAR(8) NOT NULL,
    [kode_pos] CHAR(5) NULL,
    [no_telepon_rumah] VARCHAR(20) NULL,
    [no_hp] VARCHAR(20) NULL,
    [email] VARCHAR(50) NULL,
    [entry_sekolah_id] CHAR(16) NOT NULL,
    [status_keaktifan_id] NUMERIC(4,0) NOT NULL,
    [sk_cpns] VARCHAR(40) NULL,
    [tgl_cpns] VARCHAR(20) NULL,
    [sk_pengangkatan] VARCHAR(40) NULL,
    [tmt_pengangkatan] VARCHAR(20) NULL,
    [lembaga_pengangkat_id] NUMERIC(4,0) NOT NULL,
    [pangkat_golongan_id] NUMERIC(4,0) NULL,
    [keahlian_laboratorium_id] SMALLINT(2,0) NULL,
    [sumber_gaji_id] NUMERIC(4,0) NOT NULL,
    [nama_ibu_kandung] VARCHAR(50) NOT NULL,
    [status_perkawinan] NUMERIC(3,0) NOT NULL,
    [nama_suami_istri] VARCHAR(50) NULL,
    [nip_suami_istri] CHAR(18) NULL,
    [pekerjaan_suami_istri] INT NOT NULL,
    [tmt_pns] VARCHAR(20) NULL,
    [sudah_lisensi_kepala_sekolah] NUMERIC(3,0) NOT NULL,
    [jumlah_sekolah_binaan] SMALLINT(2,0) NULL,
    [pernah_diklat_kepengawasan] NUMERIC(3,0) NOT NULL,
    [status_data] INT NULL,
    [mampu_handle_kk] INT NOT NULL,
    [keahlian_braille] NUMERIC(3,0) DEFAULT ((0)) NULL,
    [keahlian_bhs_isyarat] NUMERIC(3,0) DEFAULT ((0)) NULL,
    [npwp] CHAR(15) NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [ptk_PK] PRIMARY KEY ([ptk_id])
);

CREATE INDEX [GURU_BIDANGSTUDI_FK] ON [ptk] ([pengawas_bidang_studi_id]);

CREATE INDEX [KEWARGANEGARAAN_PTK_FK] ON [ptk] ([kewarganegaraan]);

CREATE INDEX [PEKERJAAN_SUAMI_ISTRI_FK] ON [ptk] ([pekerjaan_suami_istri]);

CREATE INDEX [PROG_KEAHLIAN_LABORAN_FK] ON [ptk] ([keahlian_laboratorium_id]);

CREATE INDEX [PTK_AGAMA_FK] ON [ptk] ([agama_id]);

CREATE INDEX [PTK_DEL] ON [ptk] ([Soft_delete]);

CREATE INDEX [PTK_ENTRY_FK] ON [ptk] ([entry_sekolah_id]);

CREATE INDEX [PTK_JENIS_FK] ON [ptk] ([jenis_ptk_id]);

CREATE INDEX [PTK_KECAMATAN_FK] ON [ptk] ([kode_wilayah]);

CREATE INDEX [PTK_LU] ON [ptk] ([Last_update]);

CREATE INDEX [PTK_MAMPUHANDLE_KK_FK] ON [ptk] ([mampu_handle_kk]);

CREATE INDEX [PTK_PANG_GOL_TERAKHIR_FK] ON [ptk] ([pangkat_golongan_id]);

CREATE INDEX [PTK_PENGANGKAT_FK] ON [ptk] ([lembaga_pengangkat_id]);

CREATE INDEX [PTK_STATUS_AKTIF_FK] ON [ptk] ([status_keaktifan_id]);

CREATE INDEX [PTK_STATUSPEGAWAI_FK] ON [ptk] ([status_kepegawaian_id]);

CREATE INDEX [PTK_TGL_NAMA] ON [ptk] ([tanggal_lahir],[nama]);

CREATE INDEX [SUMBER_GAJI_PTK_FK] ON [ptk] ([sumber_gaji_id]);

CREATE INDEX [PK__ptk__E30887E7D2131799] ON [ptk] ([ptk_id]);

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__entry_sekol__4AC307E8] FOREIGN KEY ([entry_sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__entry_sekol__2F8501C7] FOREIGN KEY ([entry_sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__agama_id__3A02903A] FOREIGN KEY ([agama_id]) REFERENCES [ref].[agama] ([agama_id])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__agama_id__5540965B] FOREIGN KEY ([agama_id]) REFERENCES [ref].[agama] ([agama_id])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__pengawas_bi__3726238F] FOREIGN KEY ([pengawas_bidang_studi_id]) REFERENCES [ref].[bidang_studi] ([bidang_studi_id])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__pengawas_bi__526429B0] FOREIGN KEY ([pengawas_bidang_studi_id]) REFERENCES [ref].[bidang_studi] ([bidang_studi_id])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__jenis_ptk_i__30792600] FOREIGN KEY ([jenis_ptk_id]) REFERENCES [ref].[jenis_ptk] ([jenis_ptk_id])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__jenis_ptk_i__4BB72C21] FOREIGN KEY ([jenis_ptk_id]) REFERENCES [ref].[jenis_ptk] ([jenis_ptk_id])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__keahlian_la__381A47C8] FOREIGN KEY ([keahlian_laboratorium_id]) REFERENCES [ref].[keahlian_laboratorium] ([keahlian_laboratorium_id])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__keahlian_la__53584DE9] FOREIGN KEY ([keahlian_laboratorium_id]) REFERENCES [ref].[keahlian_laboratorium] ([keahlian_laboratorium_id])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__mampu_handl__4D9F7493] FOREIGN KEY ([mampu_handle_kk]) REFERENCES [ref].[kebutuhan_khusus] ([kebutuhan_khusus_id])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__mampu_handl__32616E72] FOREIGN KEY ([mampu_handle_kk]) REFERENCES [ref].[kebutuhan_khusus] ([kebutuhan_khusus_id])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__lembaga_pen__335592AB] FOREIGN KEY ([lembaga_pengangkat_id]) REFERENCES [ref].[lembaga_pengangkat] ([lembaga_pengangkat_id])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__lembaga_pen__4E9398CC] FOREIGN KEY ([lembaga_pengangkat_id]) REFERENCES [ref].[lembaga_pengangkat] ([lembaga_pengangkat_id])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__kode_wilaya__4CAB505A] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__kode_wilaya__316D4A39] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__kewarganega__2E90DD8E] FOREIGN KEY ([kewarganegaraan]) REFERENCES [ref].[negara] ([negara_id])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__kewarganega__49CEE3AF] FOREIGN KEY ([kewarganegaraan]) REFERENCES [ref].[negara] ([negara_id])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__pangkat_gol__3631FF56] FOREIGN KEY ([pangkat_golongan_id]) REFERENCES [ref].[pangkat_golongan] ([pangkat_golongan_id])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__pangkat_gol__51700577] FOREIGN KEY ([pangkat_golongan_id]) REFERENCES [ref].[pangkat_golongan] ([pangkat_golongan_id])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__pekerjaan_s__390E6C01] FOREIGN KEY ([pekerjaan_suami_istri]) REFERENCES [ref].[pekerjaan] ([pekerjaan_id])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__pekerjaan_s__544C7222] FOREIGN KEY ([pekerjaan_suami_istri]) REFERENCES [ref].[pekerjaan] ([pekerjaan_id])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__status_kepe__3AF6B473] FOREIGN KEY ([status_kepegawaian_id]) REFERENCES [ref].[status_kepegawaian] ([status_kepegawaian_id])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__status_kepe__5634BA94] FOREIGN KEY ([status_kepegawaian_id]) REFERENCES [ref].[status_kepegawaian] ([status_kepegawaian_id])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__status_keak__3449B6E4] FOREIGN KEY ([status_keaktifan_id]) REFERENCES [ref].[status_keaktifan_pegawai] ([status_keaktifan_id])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__status_keak__4F87BD05] FOREIGN KEY ([status_keaktifan_id]) REFERENCES [ref].[status_keaktifan_pegawai] ([status_keaktifan_id])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__sumber_gaji__353DDB1D] FOREIGN KEY ([sumber_gaji_id]) REFERENCES [ref].[sumber_gaji] ([sumber_gaji_id])
END
;

BEGIN
ALTER TABLE [ptk] ADD CONSTRAINT [FK__ptk__sumber_gaji__507BE13E] FOREIGN KEY ([sumber_gaji_id]) REFERENCES [ref].[sumber_gaji] ([sumber_gaji_id])
END
;

-----------------------------------------------------------------------
-- ref.jenis_prasarana
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.jenis_prasarana')
BEGIN
    DECLARE @reftable_105 nvarchar(60), @constraintname_105 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.jenis_prasarana'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_105, @constraintname_105
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_105+' drop constraint '+@constraintname_105)
        FETCH NEXT from refcursor into @reftable_105, @constraintname_105
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[jenis_prasarana]
END

CREATE TABLE [ref].[jenis_prasarana]
(
    [jenis_prasarana_id] INT NOT NULL,
    [nama] VARCHAR(60) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [jenis_prasarana_PK] PRIMARY KEY ([jenis_prasarana_id])
);

CREATE INDEX [PK__jenis_pr__29F25C95BFF70626] ON [ref].[jenis_prasarana] ([jenis_prasarana_id]);

-----------------------------------------------------------------------
-- log_pengguna
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__log_pengg__pengg__24D2692A')
    ALTER TABLE [log_pengguna] DROP CONSTRAINT [FK__log_pengg__pengg__24D2692A];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__log_pengg__pengg__40106F4B')
    ALTER TABLE [log_pengguna] DROP CONSTRAINT [FK__log_pengg__pengg__40106F4B];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'log_pengguna')
BEGIN
    DECLARE @reftable_106 nvarchar(60), @constraintname_106 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'log_pengguna'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_106, @constraintname_106
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_106+' drop constraint '+@constraintname_106)
        FETCH NEXT from refcursor into @reftable_106, @constraintname_106
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [log_pengguna]
END

CREATE TABLE [log_pengguna]
(
    [waktu_login] DATETIME(16,3) NOT NULL,
    [pengguna_id] CHAR(16) NOT NULL,
    [alamat_ip] VARCHAR(50) NOT NULL,
    [keterangan] VARCHAR(128) NULL,
    [waktu_logout] DATETIME(16,3) NULL,
    CONSTRAINT [log_pengguna_PK] PRIMARY KEY ([waktu_login])
);

CREATE INDEX [LOG_USER_FK] ON [log_pengguna] ([pengguna_id]);

CREATE INDEX [PK__log_peng__35665CB0070A5D4D] ON [log_pengguna] ([waktu_login]);

BEGIN
ALTER TABLE [log_pengguna] ADD CONSTRAINT [FK__log_pengg__pengg__24D2692A] FOREIGN KEY ([pengguna_id]) REFERENCES [pengguna] ([pengguna_id])
END
;

BEGIN
ALTER TABLE [log_pengguna] ADD CONSTRAINT [FK__log_pengg__pengg__40106F4B] FOREIGN KEY ([pengguna_id]) REFERENCES [pengguna] ([pengguna_id])
END
;

-----------------------------------------------------------------------
-- versi_db
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'versi_db')
BEGIN
    DECLARE @reftable_107 nvarchar(60), @constraintname_107 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'versi_db'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_107, @constraintname_107
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_107+' drop constraint '+@constraintname_107)
        FETCH NEXT from refcursor into @reftable_107, @constraintname_107
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [versi_db]
END

CREATE TABLE [versi_db]
(
    [versi_id] NUMERIC(3,0) DEFAULT ((1)) NOT NULL,
    [versi] VARCHAR(20) DEFAULT '(''2.42'')' NOT NULL,
    [tanggal_update] DATETIME(16,3) NOT NULL,
    CONSTRAINT [versi_db_PK] PRIMARY KEY ([versi_id])
);

CREATE INDEX [PK__versi_db__8EE4C0365582A905] ON [versi_db] ([versi_id]);

-----------------------------------------------------------------------
-- ref.jenis_gugus
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.jenis_gugus')
BEGIN
    DECLARE @reftable_108 nvarchar(60), @constraintname_108 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.jenis_gugus'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_108, @constraintname_108
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_108+' drop constraint '+@constraintname_108)
        FETCH NEXT from refcursor into @reftable_108, @constraintname_108
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[jenis_gugus]
END

CREATE TABLE [ref].[jenis_gugus]
(
    [jenis_gugus_id] NUMERIC(5,0) NOT NULL,
    [jenis_gugus] VARCHAR(30) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [jenis_gugus_PK] PRIMARY KEY ([jenis_gugus_id])
);

CREATE INDEX [PK__jenis_gu__70398C7CC6F660BD] ON [ref].[jenis_gugus] ([jenis_gugus_id]);

-----------------------------------------------------------------------
-- vld_bea_pd
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_bea_p__beasi__07970BFE')
    ALTER TABLE [vld_bea_pd] DROP CONSTRAINT [FK__vld_bea_p__beasi__07970BFE];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_bea_p__beasi__6C5905DD')
    ALTER TABLE [vld_bea_pd] DROP CONSTRAINT [FK__vld_bea_p__beasi__6C5905DD];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_bea_p__idtyp__06A2E7C5')
    ALTER TABLE [vld_bea_pd] DROP CONSTRAINT [FK__vld_bea_p__idtyp__06A2E7C5];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_bea_p__idtyp__6B64E1A4')
    ALTER TABLE [vld_bea_pd] DROP CONSTRAINT [FK__vld_bea_p__idtyp__6B64E1A4];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_bea_pd')
BEGIN
    DECLARE @reftable_109 nvarchar(60), @constraintname_109 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_bea_pd'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_109, @constraintname_109
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_109+' drop constraint '+@constraintname_109)
        FETCH NEXT from refcursor into @reftable_109, @constraintname_109
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_bea_pd]
END

CREATE TABLE [vld_bea_pd]
(
    [beasiswa_peserta_didik_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_bea_pd_PK] PRIMARY KEY ([beasiswa_peserta_didik_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_bea_pd] ([idtype]);

CREATE INDEX [VLD_BEA_PD_LU] ON [vld_bea_pd] ([last_update]);

CREATE INDEX [PK__vld_bea___5A0068720D304504] ON [vld_bea_pd] ([beasiswa_peserta_didik_id],[logid]);

BEGIN
ALTER TABLE [vld_bea_pd] ADD CONSTRAINT [FK__vld_bea_p__beasi__07970BFE] FOREIGN KEY ([beasiswa_peserta_didik_id]) REFERENCES [beasiswa_peserta_didik] ([beasiswa_peserta_didik_id])
END
;

BEGIN
ALTER TABLE [vld_bea_pd] ADD CONSTRAINT [FK__vld_bea_p__beasi__6C5905DD] FOREIGN KEY ([beasiswa_peserta_didik_id]) REFERENCES [beasiswa_peserta_didik] ([beasiswa_peserta_didik_id])
END
;

BEGIN
ALTER TABLE [vld_bea_pd] ADD CONSTRAINT [FK__vld_bea_p__idtyp__06A2E7C5] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_bea_pd] ADD CONSTRAINT [FK__vld_bea_p__idtyp__6B64E1A4] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- ref.keahlian_laboratorium
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.keahlian_laboratorium')
BEGIN
    DECLARE @reftable_110 nvarchar(60), @constraintname_110 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.keahlian_laboratorium'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_110, @constraintname_110
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_110+' drop constraint '+@constraintname_110)
        FETCH NEXT from refcursor into @reftable_110, @constraintname_110
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[keahlian_laboratorium]
END

CREATE TABLE [ref].[keahlian_laboratorium]
(
    [keahlian_laboratorium_id] SMALLINT(2,0) NOT NULL,
    [nama] VARCHAR(50) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [keahlian_laboratorium_PK] PRIMARY KEY ([keahlian_laboratorium_id])
);

CREATE INDEX [PK__keahlian__281171360FA90B9B] ON [ref].[keahlian_laboratorium] ([keahlian_laboratorium_id]);

-----------------------------------------------------------------------
-- wt_src_sync_log
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__wt_src_sy__sekol__41049384')
    ALTER TABLE [wt_src_sync_log] DROP CONSTRAINT [FK__wt_src_sy__sekol__41049384];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__wt_src_sy__sekol__25C68D63')
    ALTER TABLE [wt_src_sync_log] DROP CONSTRAINT [FK__wt_src_sy__sekol__25C68D63];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__wt_src_sy__kode___41F8B7BD')
    ALTER TABLE [wt_src_sync_log] DROP CONSTRAINT [FK__wt_src_sy__kode___41F8B7BD];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__wt_src_sy__kode___26BAB19C')
    ALTER TABLE [wt_src_sync_log] DROP CONSTRAINT [FK__wt_src_sy__kode___26BAB19C];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__wt_src_sy__table__27AED5D5')
    ALTER TABLE [wt_src_sync_log] DROP CONSTRAINT [FK__wt_src_sy__table__27AED5D5];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__wt_src_sy__table__42ECDBF6')
    ALTER TABLE [wt_src_sync_log] DROP CONSTRAINT [FK__wt_src_sy__table__42ECDBF6];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'wt_src_sync_log')
BEGIN
    DECLARE @reftable_111 nvarchar(60), @constraintname_111 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'wt_src_sync_log'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_111, @constraintname_111
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_111+' drop constraint '+@constraintname_111)
        FETCH NEXT from refcursor into @reftable_111, @constraintname_111
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [wt_src_sync_log]
END

CREATE TABLE [wt_src_sync_log]
(
    [kode_wilayah] CHAR(8) NOT NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [table_name] VARCHAR(30) NOT NULL,
    [begin_sync] DATETIME(16,3) NOT NULL,
    [end_sync] DATETIME(16,3) NULL,
    [sync_media] CHAR(1) DEFAULT '(''1'')' NOT NULL,
    [is_success] NUMERIC(3,0) NOT NULL,
    [n_create] INT NOT NULL,
    [n_update] INT NOT NULL,
    [n_hapus] INT NOT NULL,
    [n_konflik] INT NOT NULL,
    [selisih_waktu_server] BIGINT(8,0) NOT NULL,
    [alamat_ip] VARCHAR(50) NOT NULL,
    [pengguna_id] CHAR(16) NOT NULL,
    CONSTRAINT [wt_src_sync_log_PK] PRIMARY KEY ([kode_wilayah],[sekolah_id],[table_name])
);

CREATE INDEX [SYNCLOG_SP_FK] ON [wt_src_sync_log] ([sekolah_id]);

CREATE INDEX [WSYNCLOG_TABLE_FK] ON [wt_src_sync_log] ([table_name]);

CREATE INDEX [PK__wt_src_s__0D3936528218FF4E] ON [wt_src_sync_log] ([kode_wilayah],[table_name],[sekolah_id]);

BEGIN
ALTER TABLE [wt_src_sync_log] ADD CONSTRAINT [FK__wt_src_sy__sekol__41049384] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [wt_src_sync_log] ADD CONSTRAINT [FK__wt_src_sy__sekol__25C68D63] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [wt_src_sync_log] ADD CONSTRAINT [FK__wt_src_sy__kode___41F8B7BD] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

BEGIN
ALTER TABLE [wt_src_sync_log] ADD CONSTRAINT [FK__wt_src_sy__kode___26BAB19C] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

BEGIN
ALTER TABLE [wt_src_sync_log] ADD CONSTRAINT [FK__wt_src_sy__table__27AED5D5] FOREIGN KEY ([table_name]) REFERENCES [ref].[table_sync] ([table_name])
END
;

BEGIN
ALTER TABLE [wt_src_sync_log] ADD CONSTRAINT [FK__wt_src_sy__table__42ECDBF6] FOREIGN KEY ([table_name]) REFERENCES [ref].[table_sync] ([table_name])
END
;

-----------------------------------------------------------------------
-- table_sync_log
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__table_syn__sekol__5728DECD')
    ALTER TABLE [table_sync_log] DROP CONSTRAINT [FK__table_syn__sekol__5728DECD];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__table_syn__sekol__3BEAD8AC')
    ALTER TABLE [table_sync_log] DROP CONSTRAINT [FK__table_syn__sekol__3BEAD8AC];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__table_syn__table__3CDEFCE5')
    ALTER TABLE [table_sync_log] DROP CONSTRAINT [FK__table_syn__table__3CDEFCE5];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__table_syn__table__581D0306')
    ALTER TABLE [table_sync_log] DROP CONSTRAINT [FK__table_syn__table__581D0306];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'table_sync_log')
BEGIN
    DECLARE @reftable_112 nvarchar(60), @constraintname_112 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'table_sync_log'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_112, @constraintname_112
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_112+' drop constraint '+@constraintname_112)
        FETCH NEXT from refcursor into @reftable_112, @constraintname_112
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [table_sync_log]
END

CREATE TABLE [table_sync_log]
(
    [table_name] VARCHAR(30) NOT NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [begin_sync] DATETIME(16,3) NOT NULL,
    [end_sync] DATETIME(16,3) NULL,
    [sync_media] CHAR(1) DEFAULT '(''1'')' NOT NULL,
    [is_success] NUMERIC(3,0) NOT NULL,
    [selisih_waktu_server] BIGINT(8,0) NOT NULL,
    [n_create] INT NOT NULL,
    [n_update] INT NOT NULL,
    [n_hapus] INT NOT NULL,
    [n_konflik] INT NOT NULL,
    [alamat_ip] VARCHAR(50) NOT NULL,
    [pengguna_id] CHAR(16) NOT NULL,
    CONSTRAINT [table_sync_log_PK] PRIMARY KEY ([table_name],[sekolah_id])
);

CREATE INDEX [ALAMAT_IP] ON [table_sync_log] ([alamat_ip]);

CREATE INDEX [SYNCLOG_SP_FK] ON [table_sync_log] ([sekolah_id]);

CREATE INDEX [PK__table_sy__4DB0CF2250ACF2BF] ON [table_sync_log] ([table_name],[sekolah_id]);

BEGIN
ALTER TABLE [table_sync_log] ADD CONSTRAINT [FK__table_syn__sekol__5728DECD] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [table_sync_log] ADD CONSTRAINT [FK__table_syn__sekol__3BEAD8AC] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [table_sync_log] ADD CONSTRAINT [FK__table_syn__table__3CDEFCE5] FOREIGN KEY ([table_name]) REFERENCES [ref].[table_sync] ([table_name])
END
;

BEGIN
ALTER TABLE [table_sync_log] ADD CONSTRAINT [FK__table_syn__table__581D0306] FOREIGN KEY ([table_name]) REFERENCES [ref].[table_sync] ([table_name])
END
;

-----------------------------------------------------------------------
-- ref.jenis_beasiswa
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__jenis_bea__sumbe__7DCDAAA2')
    ALTER TABLE [ref].[jenis_beasiswa] DROP CONSTRAINT [FK__jenis_bea__sumbe__7DCDAAA2];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.jenis_beasiswa')
BEGIN
    DECLARE @reftable_113 nvarchar(60), @constraintname_113 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.jenis_beasiswa'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_113, @constraintname_113
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_113+' drop constraint '+@constraintname_113)
        FETCH NEXT from refcursor into @reftable_113, @constraintname_113
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[jenis_beasiswa]
END

CREATE TABLE [ref].[jenis_beasiswa]
(
    [jenis_beasiswa_id] INT NOT NULL,
    [sumber_dana_id] NUMERIC(5,0) NOT NULL,
    [nama] VARCHAR(50) NOT NULL,
    [untuk_pd] NUMERIC(3,0) NOT NULL,
    [untuk_ptk] NUMERIC(3,0) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [jenis_beasiswa_PK] PRIMARY KEY ([jenis_beasiswa_id])
);

CREATE INDEX [SUMBER_BEASISWA_FK] ON [ref].[jenis_beasiswa] ([sumber_dana_id]);

CREATE INDEX [PK__jenis_be__4F39AF211D2DBAD4] ON [ref].[jenis_beasiswa] ([jenis_beasiswa_id]);

BEGIN
ALTER TABLE [ref].[jenis_beasiswa] ADD CONSTRAINT [FK__jenis_bea__sumbe__7DCDAAA2] FOREIGN KEY ([sumber_dana_id]) REFERENCES [ref].[sumber_dana] ([sumber_dana_id])
END
;

-----------------------------------------------------------------------
-- vld_anak
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_anak__anak_i__097F5470')
    ALTER TABLE [vld_anak] DROP CONSTRAINT [FK__vld_anak__anak_i__097F5470];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_anak__anak_i__6E414E4F')
    ALTER TABLE [vld_anak] DROP CONSTRAINT [FK__vld_anak__anak_i__6E414E4F];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_anak__idtype__088B3037')
    ALTER TABLE [vld_anak] DROP CONSTRAINT [FK__vld_anak__idtype__088B3037];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_anak__idtype__6D4D2A16')
    ALTER TABLE [vld_anak] DROP CONSTRAINT [FK__vld_anak__idtype__6D4D2A16];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_anak')
BEGIN
    DECLARE @reftable_114 nvarchar(60), @constraintname_114 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_anak'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_114, @constraintname_114
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_114+' drop constraint '+@constraintname_114)
        FETCH NEXT from refcursor into @reftable_114, @constraintname_114
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_anak]
END

CREATE TABLE [vld_anak]
(
    [anak_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_anak_PK] PRIMARY KEY ([anak_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_anak] ([idtype]);

CREATE INDEX [VLD_ANAK_LU] ON [vld_anak] ([last_update]);

CREATE INDEX [PK__vld_anak__32E28F08EBBAB098] ON [vld_anak] ([anak_id],[logid]);

BEGIN
ALTER TABLE [vld_anak] ADD CONSTRAINT [FK__vld_anak__anak_i__097F5470] FOREIGN KEY ([anak_id]) REFERENCES [anak] ([anak_id])
END
;

BEGIN
ALTER TABLE [vld_anak] ADD CONSTRAINT [FK__vld_anak__anak_i__6E414E4F] FOREIGN KEY ([anak_id]) REFERENCES [anak] ([anak_id])
END
;

BEGIN
ALTER TABLE [vld_anak] ADD CONSTRAINT [FK__vld_anak__idtype__088B3037] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_anak] ADD CONSTRAINT [FK__vld_anak__idtype__6D4D2A16] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- wt_dst_sync_log
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__wt_dst_sy__sekol__43E1002F')
    ALTER TABLE [wt_dst_sync_log] DROP CONSTRAINT [FK__wt_dst_sy__sekol__43E1002F];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__wt_dst_sy__sekol__28A2FA0E')
    ALTER TABLE [wt_dst_sync_log] DROP CONSTRAINT [FK__wt_dst_sy__sekol__28A2FA0E];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__wt_dst_sy__kode___44D52468')
    ALTER TABLE [wt_dst_sync_log] DROP CONSTRAINT [FK__wt_dst_sy__kode___44D52468];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__wt_dst_sy__kode___29971E47')
    ALTER TABLE [wt_dst_sync_log] DROP CONSTRAINT [FK__wt_dst_sy__kode___29971E47];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__wt_dst_sy__table__2A8B4280')
    ALTER TABLE [wt_dst_sync_log] DROP CONSTRAINT [FK__wt_dst_sy__table__2A8B4280];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__wt_dst_sy__table__45C948A1')
    ALTER TABLE [wt_dst_sync_log] DROP CONSTRAINT [FK__wt_dst_sy__table__45C948A1];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'wt_dst_sync_log')
BEGIN
    DECLARE @reftable_115 nvarchar(60), @constraintname_115 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'wt_dst_sync_log'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_115, @constraintname_115
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_115+' drop constraint '+@constraintname_115)
        FETCH NEXT from refcursor into @reftable_115, @constraintname_115
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [wt_dst_sync_log]
END

CREATE TABLE [wt_dst_sync_log]
(
    [kode_wilayah] CHAR(8) NOT NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [table_name] VARCHAR(30) NOT NULL,
    [begin_sync] DATETIME(16,3) NOT NULL,
    [end_sync] DATETIME(16,3) NULL,
    [sync_media] CHAR(1) DEFAULT '(''1'')' NOT NULL,
    [is_success] NUMERIC(3,0) NOT NULL,
    [n_create] INT NOT NULL,
    [n_update] INT NOT NULL,
    [n_hapus] INT NOT NULL,
    [n_konflik] INT NOT NULL,
    [selisih_waktu_server] BIGINT(8,0) NOT NULL,
    [alamat_ip] VARCHAR(50) NOT NULL,
    [pengguna_id] CHAR(16) NOT NULL,
    CONSTRAINT [wt_dst_sync_log_PK] PRIMARY KEY ([kode_wilayah],[sekolah_id],[table_name])
);

CREATE INDEX [SYNCLOG_SP_FK] ON [wt_dst_sync_log] ([sekolah_id]);

CREATE INDEX [WSYNCLOG_TABLE_FK] ON [wt_dst_sync_log] ([table_name]);

CREATE INDEX [PK__wt_dst_s__0D393652452380A6] ON [wt_dst_sync_log] ([kode_wilayah],[table_name],[sekolah_id]);

BEGIN
ALTER TABLE [wt_dst_sync_log] ADD CONSTRAINT [FK__wt_dst_sy__sekol__43E1002F] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [wt_dst_sync_log] ADD CONSTRAINT [FK__wt_dst_sy__sekol__28A2FA0E] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [wt_dst_sync_log] ADD CONSTRAINT [FK__wt_dst_sy__kode___44D52468] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

BEGIN
ALTER TABLE [wt_dst_sync_log] ADD CONSTRAINT [FK__wt_dst_sy__kode___29971E47] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

BEGIN
ALTER TABLE [wt_dst_sync_log] ADD CONSTRAINT [FK__wt_dst_sy__table__2A8B4280] FOREIGN KEY ([table_name]) REFERENCES [ref].[table_sync] ([table_name])
END
;

BEGIN
ALTER TABLE [wt_dst_sync_log] ADD CONSTRAINT [FK__wt_dst_sy__table__45C948A1] FOREIGN KEY ([table_name]) REFERENCES [ref].[table_sync] ([table_name])
END
;

-----------------------------------------------------------------------
-- prasarana
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__prasarana__sekol__5AF96FB1')
    ALTER TABLE [prasarana] DROP CONSTRAINT [FK__prasarana__sekol__5AF96FB1];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__prasarana__sekol__3FBB6990')
    ALTER TABLE [prasarana] DROP CONSTRAINT [FK__prasarana__sekol__3FBB6990];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__prasarana__jenis__3EC74557')
    ALTER TABLE [prasarana] DROP CONSTRAINT [FK__prasarana__jenis__3EC74557];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__prasarana__jenis__5A054B78')
    ALTER TABLE [prasarana] DROP CONSTRAINT [FK__prasarana__jenis__5A054B78];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__prasarana__kepem__3DD3211E')
    ALTER TABLE [prasarana] DROP CONSTRAINT [FK__prasarana__kepem__3DD3211E];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__prasarana__kepem__5911273F')
    ALTER TABLE [prasarana] DROP CONSTRAINT [FK__prasarana__kepem__5911273F];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'prasarana')
BEGIN
    DECLARE @reftable_116 nvarchar(60), @constraintname_116 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'prasarana'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_116, @constraintname_116
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_116+' drop constraint '+@constraintname_116)
        FETCH NEXT from refcursor into @reftable_116, @constraintname_116
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [prasarana]
END

CREATE TABLE [prasarana]
(
    [prasarana_id] CHAR(16) NOT NULL,
    [jenis_prasarana_id] INT NOT NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [kepemilikan_sarpras_id] NUMERIC(3,0) NOT NULL,
    [nama] VARCHAR(30) NOT NULL,
    [panjang] FLOAT(8) NULL,
    [lebar] FLOAT(8) NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [prasarana_PK] PRIMARY KEY ([prasarana_id])
);

CREATE INDEX [PRASARANA_DEL] ON [prasarana] ([Soft_delete]);

CREATE INDEX [PRASARANA_JENIS_FK] ON [prasarana] ([jenis_prasarana_id]);

CREATE INDEX [PRASARANA_LU] ON [prasarana] ([Last_update]);

CREATE INDEX [PRASARANA_MILIK_FK] ON [prasarana] ([kepemilikan_sarpras_id]);

CREATE INDEX [PRASARANA_SEKOLAH_FK] ON [prasarana] ([sekolah_id]);

CREATE INDEX [PK__prasaran__BF102F5670351BEC] ON [prasarana] ([prasarana_id]);

BEGIN
ALTER TABLE [prasarana] ADD CONSTRAINT [FK__prasarana__sekol__5AF96FB1] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [prasarana] ADD CONSTRAINT [FK__prasarana__sekol__3FBB6990] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [prasarana] ADD CONSTRAINT [FK__prasarana__jenis__3EC74557] FOREIGN KEY ([jenis_prasarana_id]) REFERENCES [ref].[jenis_prasarana] ([jenis_prasarana_id])
END
;

BEGIN
ALTER TABLE [prasarana] ADD CONSTRAINT [FK__prasarana__jenis__5A054B78] FOREIGN KEY ([jenis_prasarana_id]) REFERENCES [ref].[jenis_prasarana] ([jenis_prasarana_id])
END
;

BEGIN
ALTER TABLE [prasarana] ADD CONSTRAINT [FK__prasarana__kepem__3DD3211E] FOREIGN KEY ([kepemilikan_sarpras_id]) REFERENCES [ref].[status_kepemilikan_sarpras] ([kepemilikan_sarpras_id])
END
;

BEGIN
ALTER TABLE [prasarana] ADD CONSTRAINT [FK__prasarana__kepem__5911273F] FOREIGN KEY ([kepemilikan_sarpras_id]) REFERENCES [ref].[status_kepemilikan_sarpras] ([kepemilikan_sarpras_id])
END
;

-----------------------------------------------------------------------
-- ref.semester
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__semester__tahun___7AF13DF7')
    ALTER TABLE [ref].[semester] DROP CONSTRAINT [FK__semester__tahun___7AF13DF7];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.semester')
BEGIN
    DECLARE @reftable_117 nvarchar(60), @constraintname_117 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.semester'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_117, @constraintname_117
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_117+' drop constraint '+@constraintname_117)
        FETCH NEXT from refcursor into @reftable_117, @constraintname_117
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[semester]
END

CREATE TABLE [ref].[semester]
(
    [semester_id] CHAR(5) NOT NULL,
    [tahun_ajaran_id] NUMERIC(6,0) NOT NULL,
    [nama] VARCHAR(20) NOT NULL,
    [semester] NUMERIC(3,0) NOT NULL,
    [periode_aktif] NUMERIC(3,0) NOT NULL,
    [tanggal_mulai] VARCHAR(20) NOT NULL,
    [tanggal_selesai] VARCHAR(20) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [semester_PK] PRIMARY KEY ([semester_id])
);

CREATE INDEX [SEMESTER_TAHUN_AJARAN_FK] ON [ref].[semester] ([tahun_ajaran_id]);

CREATE INDEX [PK__semester__CBC81B0072BB2615] ON [ref].[semester] ([semester_id]);

BEGIN
ALTER TABLE [ref].[semester] ADD CONSTRAINT [FK__semester__tahun___7AF13DF7] FOREIGN KEY ([tahun_ajaran_id]) REFERENCES [ref].[tahun_ajaran] ([tahun_ajaran_id])
END
;

-----------------------------------------------------------------------
-- wsync_session
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__wsync_ses__kode___46BD6CDA')
    ALTER TABLE [wsync_session] DROP CONSTRAINT [FK__wsync_ses__kode___46BD6CDA];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__wsync_ses__kode___2B7F66B9')
    ALTER TABLE [wsync_session] DROP CONSTRAINT [FK__wsync_ses__kode___2B7F66B9];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'wsync_session')
BEGIN
    DECLARE @reftable_118 nvarchar(60), @constraintname_118 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'wsync_session'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_118, @constraintname_118
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_118+' drop constraint '+@constraintname_118)
        FETCH NEXT from refcursor into @reftable_118, @constraintname_118
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [wsync_session]
END

CREATE TABLE [wsync_session]
(
    [token] CHAR(16) NOT NULL,
    [kode_wilayah] CHAR(8) NOT NULL,
    [pengguna_id] CHAR(16) NOT NULL,
    [create_time] DATETIME(16,3) NOT NULL,
    [last_activity] DATETIME(16,3) NULL,
    CONSTRAINT [wsync_session_PK] PRIMARY KEY ([token])
);

CREATE INDEX [WSYNC_SESSION_WIL_FK] ON [wsync_session] ([kode_wilayah]);

CREATE INDEX [PK__wsync_se__CA90DA7A915DA20A] ON [wsync_session] ([token]);

BEGIN
ALTER TABLE [wsync_session] ADD CONSTRAINT [FK__wsync_ses__kode___46BD6CDA] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

BEGIN
ALTER TABLE [wsync_session] ADD CONSTRAINT [FK__wsync_ses__kode___2B7F66B9] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

-----------------------------------------------------------------------
-- rombongan_belajar
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rombongan__jurus__40AF8DC9')
    ALTER TABLE [rombongan_belajar] DROP CONSTRAINT [FK__rombongan__jurus__40AF8DC9];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rombongan__jurus__5BED93EA')
    ALTER TABLE [rombongan_belajar] DROP CONSTRAINT [FK__rombongan__jurus__5BED93EA];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rombongan__prasa__4668671F')
    ALTER TABLE [rombongan_belajar] DROP CONSTRAINT [FK__rombongan__prasa__4668671F];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rombongan__prasa__61A66D40')
    ALTER TABLE [rombongan_belajar] DROP CONSTRAINT [FK__rombongan__prasa__61A66D40];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rombongan__ptk_i__60B24907')
    ALTER TABLE [rombongan_belajar] DROP CONSTRAINT [FK__rombongan__ptk_i__60B24907];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rombongan__ptk_i__457442E6')
    ALTER TABLE [rombongan_belajar] DROP CONSTRAINT [FK__rombongan__ptk_i__457442E6];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rombongan__sekol__629A9179')
    ALTER TABLE [rombongan_belajar] DROP CONSTRAINT [FK__rombongan__sekol__629A9179];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rombongan__sekol__475C8B58')
    ALTER TABLE [rombongan_belajar] DROP CONSTRAINT [FK__rombongan__sekol__475C8B58];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rombongan__kebut__5CE1B823')
    ALTER TABLE [rombongan_belajar] DROP CONSTRAINT [FK__rombongan__kebut__5CE1B823];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rombongan__kebut__41A3B202')
    ALTER TABLE [rombongan_belajar] DROP CONSTRAINT [FK__rombongan__kebut__41A3B202];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rombongan__kurik__4297D63B')
    ALTER TABLE [rombongan_belajar] DROP CONSTRAINT [FK__rombongan__kurik__4297D63B];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rombongan__kurik__5DD5DC5C')
    ALTER TABLE [rombongan_belajar] DROP CONSTRAINT [FK__rombongan__kurik__5DD5DC5C];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rombongan__semes__5ECA0095')
    ALTER TABLE [rombongan_belajar] DROP CONSTRAINT [FK__rombongan__semes__5ECA0095];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rombongan__semes__438BFA74')
    ALTER TABLE [rombongan_belajar] DROP CONSTRAINT [FK__rombongan__semes__438BFA74];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rombongan__tingk__44801EAD')
    ALTER TABLE [rombongan_belajar] DROP CONSTRAINT [FK__rombongan__tingk__44801EAD];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rombongan__tingk__5FBE24CE')
    ALTER TABLE [rombongan_belajar] DROP CONSTRAINT [FK__rombongan__tingk__5FBE24CE];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'rombongan_belajar')
BEGIN
    DECLARE @reftable_119 nvarchar(60), @constraintname_119 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'rombongan_belajar'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_119, @constraintname_119
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_119+' drop constraint '+@constraintname_119)
        FETCH NEXT from refcursor into @reftable_119, @constraintname_119
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [rombongan_belajar]
END

CREATE TABLE [rombongan_belajar]
(
    [rombongan_belajar_id] CHAR(16) NOT NULL,
    [semester_id] CHAR(5) NOT NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [tingkat_pendidikan_id] NUMERIC(4,0) NOT NULL,
    [jurusan_sp_id] CHAR(16) NULL,
    [kurikulum_id] SMALLINT(2,0) NOT NULL,
    [nama] VARCHAR(12) NOT NULL,
    [ptk_id] CHAR(16) NOT NULL,
    [prasarana_id] CHAR(16) NOT NULL,
    [moving_class] NUMERIC(3,0) NOT NULL,
    [jenis_rombel] NUMERIC(3,0) DEFAULT ((1)) NOT NULL,
    [sks] NUMERIC(4,0) DEFAULT ((0)) NOT NULL,
    [kebutuhan_khusus_id] INT NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [rombongan_belajar_PK] PRIMARY KEY ([rombongan_belajar_id])
);

CREATE INDEX [ROMBEL_DEL_SP] ON [rombongan_belajar] ([Soft_delete],[sekolah_id]);

CREATE INDEX [ROMBEL_JURUSAN_FK] ON [rombongan_belajar] ([jurusan_sp_id]);

CREATE INDEX [ROMBEL_KK_FK] ON [rombongan_belajar] ([kebutuhan_khusus_id]);

CREATE INDEX [ROMBEL_KUR_FK] ON [rombongan_belajar] ([kurikulum_id]);

CREATE INDEX [ROMBEL_LU] ON [rombongan_belajar] ([Last_update]);

CREATE INDEX [ROMBEL_PRASARANA_FK] ON [rombongan_belajar] ([prasarana_id]);

CREATE INDEX [ROMBEL_SEMESTER_FK] ON [rombongan_belajar] ([semester_id]);

CREATE INDEX [ROMBEL_SP_LU] ON [rombongan_belajar] ([sekolah_id],[Last_update]);

CREATE INDEX [ROMBEL_TINGKAT_FK] ON [rombongan_belajar] ([tingkat_pendidikan_id]);

CREATE INDEX [WALI_KELAS_FK] ON [rombongan_belajar] ([ptk_id]);

CREATE INDEX [PK__rombonga__5321DF94F1B73587] ON [rombongan_belajar] ([rombongan_belajar_id]);

BEGIN
ALTER TABLE [rombongan_belajar] ADD CONSTRAINT [FK__rombongan__jurus__40AF8DC9] FOREIGN KEY ([jurusan_sp_id]) REFERENCES [jurusan_sp] ([jurusan_sp_id])
END
;

BEGIN
ALTER TABLE [rombongan_belajar] ADD CONSTRAINT [FK__rombongan__jurus__5BED93EA] FOREIGN KEY ([jurusan_sp_id]) REFERENCES [jurusan_sp] ([jurusan_sp_id])
END
;

BEGIN
ALTER TABLE [rombongan_belajar] ADD CONSTRAINT [FK__rombongan__prasa__4668671F] FOREIGN KEY ([prasarana_id]) REFERENCES [prasarana] ([prasarana_id])
END
;

BEGIN
ALTER TABLE [rombongan_belajar] ADD CONSTRAINT [FK__rombongan__prasa__61A66D40] FOREIGN KEY ([prasarana_id]) REFERENCES [prasarana] ([prasarana_id])
END
;

BEGIN
ALTER TABLE [rombongan_belajar] ADD CONSTRAINT [FK__rombongan__ptk_i__60B24907] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [rombongan_belajar] ADD CONSTRAINT [FK__rombongan__ptk_i__457442E6] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [rombongan_belajar] ADD CONSTRAINT [FK__rombongan__sekol__629A9179] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [rombongan_belajar] ADD CONSTRAINT [FK__rombongan__sekol__475C8B58] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [rombongan_belajar] ADD CONSTRAINT [FK__rombongan__kebut__5CE1B823] FOREIGN KEY ([kebutuhan_khusus_id]) REFERENCES [ref].[kebutuhan_khusus] ([kebutuhan_khusus_id])
END
;

BEGIN
ALTER TABLE [rombongan_belajar] ADD CONSTRAINT [FK__rombongan__kebut__41A3B202] FOREIGN KEY ([kebutuhan_khusus_id]) REFERENCES [ref].[kebutuhan_khusus] ([kebutuhan_khusus_id])
END
;

BEGIN
ALTER TABLE [rombongan_belajar] ADD CONSTRAINT [FK__rombongan__kurik__4297D63B] FOREIGN KEY ([kurikulum_id]) REFERENCES [ref].[kurikulum] ([kurikulum_id])
END
;

BEGIN
ALTER TABLE [rombongan_belajar] ADD CONSTRAINT [FK__rombongan__kurik__5DD5DC5C] FOREIGN KEY ([kurikulum_id]) REFERENCES [ref].[kurikulum] ([kurikulum_id])
END
;

BEGIN
ALTER TABLE [rombongan_belajar] ADD CONSTRAINT [FK__rombongan__semes__5ECA0095] FOREIGN KEY ([semester_id]) REFERENCES [ref].[semester] ([semester_id])
END
;

BEGIN
ALTER TABLE [rombongan_belajar] ADD CONSTRAINT [FK__rombongan__semes__438BFA74] FOREIGN KEY ([semester_id]) REFERENCES [ref].[semester] ([semester_id])
END
;

BEGIN
ALTER TABLE [rombongan_belajar] ADD CONSTRAINT [FK__rombongan__tingk__44801EAD] FOREIGN KEY ([tingkat_pendidikan_id]) REFERENCES [ref].[tingkat_pendidikan] ([tingkat_pendidikan_id])
END
;

BEGIN
ALTER TABLE [rombongan_belajar] ADD CONSTRAINT [FK__rombongan__tingk__5FBE24CE] FOREIGN KEY ([tingkat_pendidikan_id]) REFERENCES [ref].[tingkat_pendidikan] ([tingkat_pendidikan_id])
END
;

-----------------------------------------------------------------------
-- ref.sumber_listrik
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.sumber_listrik')
BEGIN
    DECLARE @reftable_120 nvarchar(60), @constraintname_120 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.sumber_listrik'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_120, @constraintname_120
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_120+' drop constraint '+@constraintname_120)
        FETCH NEXT from refcursor into @reftable_120, @constraintname_120
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[sumber_listrik]
END

CREATE TABLE [ref].[sumber_listrik]
(
    [sumber_listrik_id] NUMERIC(4,0) NOT NULL,
    [nama] VARCHAR(50) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [sumber_listrik_PK] PRIMARY KEY ([sumber_listrik_id])
);

CREATE INDEX [PK__sumber_l__7A67ADDC778131EA] ON [ref].[sumber_listrik] ([sumber_listrik_id]);

-----------------------------------------------------------------------
-- sysdiagrams
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'sysdiagrams')
BEGIN
    DECLARE @reftable_121 nvarchar(60), @constraintname_121 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'sysdiagrams'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_121, @constraintname_121
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_121+' drop constraint '+@constraintname_121)
        FETCH NEXT from refcursor into @reftable_121, @constraintname_121
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [sysdiagrams]
END

CREATE TABLE [sysdiagrams]
(
    [name] VARCHAR(256) NOT NULL,
    [principal_id] INT NOT NULL,
    [diagram_id] INT NOT NULL IDENTITY,
    [version] INT NULL,
    [definition] VARBINARY(MAX)(2147483647) NULL,
    CONSTRAINT [sysdiagrams_PK] PRIMARY KEY ([diagram_id])
);

CREATE INDEX [PK__sysdiagr__C2B05B614C417A1A] ON [sysdiagrams] ([diagram_id]);

CREATE INDEX [UQ__sysdiagr__532EC154319BBB6C] ON [sysdiagrams] ([principal_id],[name]);

CREATE INDEX [UQ__sysdiagr__532EC154FE0AFEA9] ON [sysdiagrams] ([principal_id],[name]);

-----------------------------------------------------------------------
-- sasaran_survey
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sasaran_s__pengg__0A7378A9')
    ALTER TABLE [sasaran_survey] DROP CONSTRAINT [FK__sasaran_s__pengg__0A7378A9];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sasaran_s__pengg__6F357288')
    ALTER TABLE [sasaran_survey] DROP CONSTRAINT [FK__sasaran_s__pengg__6F357288];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sasaran_s__sekol__702996C1')
    ALTER TABLE [sasaran_survey] DROP CONSTRAINT [FK__sasaran_s__sekol__702996C1];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sasaran_s__sekol__0B679CE2')
    ALTER TABLE [sasaran_survey] DROP CONSTRAINT [FK__sasaran_s__sekol__0B679CE2];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'sasaran_survey')
BEGIN
    DECLARE @reftable_122 nvarchar(60), @constraintname_122 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'sasaran_survey'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_122, @constraintname_122
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_122+' drop constraint '+@constraintname_122)
        FETCH NEXT from refcursor into @reftable_122, @constraintname_122
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [sasaran_survey]
END

CREATE TABLE [sasaran_survey]
(
    [pengguna_id] CHAR(16) NOT NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [nomor_urut] INT NOT NULL,
    [tanggal_rencana] VARCHAR(20) NULL,
    [tanggal_pelaksanaan] VARCHAR(20) NULL,
    [waktu_berangkat] DATETIME(16,3) NULL,
    [waktu_sampai] DATETIME(16,3) NULL,
    [waktu_mulai_survey] DATETIME(16,3) NULL,
    [waktu_selesai] DATETIME(16,3) NULL,
    [waktu_isi_form_cetak] INT NULL,
    [waktu_isi_form_elektronik] INT NULL,
    CONSTRAINT [sasaran_survey_PK] PRIMARY KEY ([pengguna_id],[sekolah_id])
);

CREATE INDEX [SASARAN_SURVEY_SEKOLAH_FK] ON [sasaran_survey] ([sekolah_id]);

CREATE INDEX [PK__sasaran___FBA87A4CE52947F1] ON [sasaran_survey] ([pengguna_id],[sekolah_id]);

BEGIN
ALTER TABLE [sasaran_survey] ADD CONSTRAINT [FK__sasaran_s__pengg__0A7378A9] FOREIGN KEY ([pengguna_id]) REFERENCES [pengguna] ([pengguna_id])
END
;

BEGIN
ALTER TABLE [sasaran_survey] ADD CONSTRAINT [FK__sasaran_s__pengg__6F357288] FOREIGN KEY ([pengguna_id]) REFERENCES [pengguna] ([pengguna_id])
END
;

BEGIN
ALTER TABLE [sasaran_survey] ADD CONSTRAINT [FK__sasaran_s__sekol__702996C1] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [sasaran_survey] ADD CONSTRAINT [FK__sasaran_s__sekol__0B679CE2] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

-----------------------------------------------------------------------
-- wsrc_sync_log
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__wsrc_sync__sekol__47B19113')
    ALTER TABLE [wsrc_sync_log] DROP CONSTRAINT [FK__wsrc_sync__sekol__47B19113];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__wsrc_sync__sekol__2C738AF2')
    ALTER TABLE [wsrc_sync_log] DROP CONSTRAINT [FK__wsrc_sync__sekol__2C738AF2];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__wsrc_sync__kode___48A5B54C')
    ALTER TABLE [wsrc_sync_log] DROP CONSTRAINT [FK__wsrc_sync__kode___48A5B54C];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__wsrc_sync__kode___2D67AF2B')
    ALTER TABLE [wsrc_sync_log] DROP CONSTRAINT [FK__wsrc_sync__kode___2D67AF2B];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'wsrc_sync_log')
BEGIN
    DECLARE @reftable_123 nvarchar(60), @constraintname_123 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'wsrc_sync_log'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_123, @constraintname_123
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_123+' drop constraint '+@constraintname_123)
        FETCH NEXT from refcursor into @reftable_123, @constraintname_123
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [wsrc_sync_log]
END

CREATE TABLE [wsrc_sync_log]
(
    [kode_wilayah] CHAR(8) NOT NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [begin_sync] DATETIME(16,3) NOT NULL,
    [end_sync] DATETIME(16,3) NULL,
    [sync_media] CHAR(1) DEFAULT '(''1'')' NOT NULL,
    [is_success] NUMERIC(3,0) NOT NULL,
    [selisih_waktu_server] BIGINT(8,0) NOT NULL,
    [alamat_ip] VARCHAR(50) NOT NULL,
    [pengguna_id] CHAR(16) NOT NULL,
    CONSTRAINT [wsrc_sync_log_PK] PRIMARY KEY ([kode_wilayah],[sekolah_id],[begin_sync])
);

CREATE INDEX [SYNCLOG_SP_FK] ON [wsrc_sync_log] ([sekolah_id]);

CREATE INDEX [PK__wsrc_syn__14A211FD1D558156] ON [wsrc_sync_log] ([kode_wilayah],[sekolah_id],[begin_sync]);

BEGIN
ALTER TABLE [wsrc_sync_log] ADD CONSTRAINT [FK__wsrc_sync__sekol__47B19113] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [wsrc_sync_log] ADD CONSTRAINT [FK__wsrc_sync__sekol__2C738AF2] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [wsrc_sync_log] ADD CONSTRAINT [FK__wsrc_sync__kode___48A5B54C] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

BEGIN
ALTER TABLE [wsrc_sync_log] ADD CONSTRAINT [FK__wsrc_sync__kode___2D67AF2B] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

-----------------------------------------------------------------------
-- sasaran_pengawasan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sasaran_p__penga__0C5BC11B')
    ALTER TABLE [sasaran_pengawasan] DROP CONSTRAINT [FK__sasaran_p__penga__0C5BC11B];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sasaran_p__penga__711DBAFA')
    ALTER TABLE [sasaran_pengawasan] DROP CONSTRAINT [FK__sasaran_p__penga__711DBAFA];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sasaran_p__sekol__7211DF33')
    ALTER TABLE [sasaran_pengawasan] DROP CONSTRAINT [FK__sasaran_p__sekol__7211DF33];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sasaran_p__sekol__0D4FE554')
    ALTER TABLE [sasaran_pengawasan] DROP CONSTRAINT [FK__sasaran_p__sekol__0D4FE554];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'sasaran_pengawasan')
BEGIN
    DECLARE @reftable_124 nvarchar(60), @constraintname_124 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'sasaran_pengawasan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_124, @constraintname_124
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_124+' drop constraint '+@constraintname_124)
        FETCH NEXT from refcursor into @reftable_124, @constraintname_124
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [sasaran_pengawasan]
END

CREATE TABLE [sasaran_pengawasan]
(
    [pengawas_terdaftar_id] CHAR(16) NOT NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [sasaran_pengawasan_PK] PRIMARY KEY ([pengawas_terdaftar_id],[sekolah_id])
);

CREATE INDEX [SASARAN_SEKOLAH_FK] ON [sasaran_pengawasan] ([sekolah_id]);

CREATE INDEX [SASWAS_LU] ON [sasaran_pengawasan] ([Last_update]);

CREATE INDEX [PK__sasaran___632F2C73EE1EEBAB] ON [sasaran_pengawasan] ([pengawas_terdaftar_id],[sekolah_id]);

BEGIN
ALTER TABLE [sasaran_pengawasan] ADD CONSTRAINT [FK__sasaran_p__penga__0C5BC11B] FOREIGN KEY ([pengawas_terdaftar_id]) REFERENCES [pengawas_terdaftar] ([pengawas_terdaftar_id])
END
;

BEGIN
ALTER TABLE [sasaran_pengawasan] ADD CONSTRAINT [FK__sasaran_p__penga__711DBAFA] FOREIGN KEY ([pengawas_terdaftar_id]) REFERENCES [pengawas_terdaftar] ([pengawas_terdaftar_id])
END
;

BEGIN
ALTER TABLE [sasaran_pengawasan] ADD CONSTRAINT [FK__sasaran_p__sekol__7211DF33] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [sasaran_pengawasan] ADD CONSTRAINT [FK__sasaran_p__sekol__0D4FE554] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

-----------------------------------------------------------------------
-- ref.jabatan_tugas_ptk
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.jabatan_tugas_ptk')
BEGIN
    DECLARE @reftable_125 nvarchar(60), @constraintname_125 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.jabatan_tugas_ptk'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_125, @constraintname_125
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_125+' drop constraint '+@constraintname_125)
        FETCH NEXT from refcursor into @reftable_125, @constraintname_125
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[jabatan_tugas_ptk]
END

CREATE TABLE [ref].[jabatan_tugas_ptk]
(
    [jabatan_ptk_id] NUMERIC(5,0) NOT NULL,
    [nama] VARCHAR(40) NOT NULL,
    [jabatan_utama] NUMERIC(3,0) NOT NULL,
    [tugas_tambahan_guru] NUMERIC(3,0) NOT NULL,
    [jumlah_jam_diakui] NUMERIC(4,0) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [jabatan_tugas_ptk_PK] PRIMARY KEY ([jabatan_ptk_id])
);

CREATE INDEX [PK__jabatan___52C0C67264307F2D] ON [ref].[jabatan_tugas_ptk] ([jabatan_ptk_id]);

-----------------------------------------------------------------------
-- wdst_sync_log
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__wdst_sync__sekol__4999D985')
    ALTER TABLE [wdst_sync_log] DROP CONSTRAINT [FK__wdst_sync__sekol__4999D985];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__wdst_sync__sekol__2E5BD364')
    ALTER TABLE [wdst_sync_log] DROP CONSTRAINT [FK__wdst_sync__sekol__2E5BD364];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__wdst_sync__kode___4A8DFDBE')
    ALTER TABLE [wdst_sync_log] DROP CONSTRAINT [FK__wdst_sync__kode___4A8DFDBE];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__wdst_sync__kode___2F4FF79D')
    ALTER TABLE [wdst_sync_log] DROP CONSTRAINT [FK__wdst_sync__kode___2F4FF79D];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'wdst_sync_log')
BEGIN
    DECLARE @reftable_126 nvarchar(60), @constraintname_126 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'wdst_sync_log'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_126, @constraintname_126
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_126+' drop constraint '+@constraintname_126)
        FETCH NEXT from refcursor into @reftable_126, @constraintname_126
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [wdst_sync_log]
END

CREATE TABLE [wdst_sync_log]
(
    [kode_wilayah] CHAR(8) NOT NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [begin_sync] DATETIME(16,3) NOT NULL,
    [end_sync] DATETIME(16,3) NULL,
    [sync_media] CHAR(1) DEFAULT '(''1'')' NOT NULL,
    [is_success] NUMERIC(3,0) NOT NULL,
    [selisih_waktu_server] BIGINT(8,0) NOT NULL,
    [alamat_ip] VARCHAR(50) NOT NULL,
    [pengguna_id] CHAR(16) NOT NULL,
    CONSTRAINT [wdst_sync_log_PK] PRIMARY KEY ([kode_wilayah],[sekolah_id],[begin_sync])
);

CREATE INDEX [SYNCLOG_SP_FK] ON [wdst_sync_log] ([sekolah_id]);

CREATE INDEX [PK__wdst_syn__14A211FD22C04E09] ON [wdst_sync_log] ([kode_wilayah],[sekolah_id],[begin_sync]);

BEGIN
ALTER TABLE [wdst_sync_log] ADD CONSTRAINT [FK__wdst_sync__sekol__4999D985] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [wdst_sync_log] ADD CONSTRAINT [FK__wdst_sync__sekol__2E5BD364] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [wdst_sync_log] ADD CONSTRAINT [FK__wdst_sync__kode___4A8DFDBE] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

BEGIN
ALTER TABLE [wdst_sync_log] ADD CONSTRAINT [FK__wdst_sync__kode___2F4FF79D] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

-----------------------------------------------------------------------
-- ref.jenis_kesejahteraan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.jenis_kesejahteraan')
BEGIN
    DECLARE @reftable_127 nvarchar(60), @constraintname_127 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.jenis_kesejahteraan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_127, @constraintname_127
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_127+' drop constraint '+@constraintname_127)
        FETCH NEXT from refcursor into @reftable_127, @constraintname_127
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[jenis_kesejahteraan]
END

CREATE TABLE [ref].[jenis_kesejahteraan]
(
    [jenis_kesejahteraan_id] INT NOT NULL,
    [nama] VARCHAR(30) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [jenis_kesejahteraan_PK] PRIMARY KEY ([jenis_kesejahteraan_id])
);

CREATE INDEX [PK__jenis_ke__CAD9493EB43377A8] ON [ref].[jenis_kesejahteraan] ([jenis_kesejahteraan_id]);

-----------------------------------------------------------------------
-- sync_log
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sync_log__sekola__638EB5B2')
    ALTER TABLE [sync_log] DROP CONSTRAINT [FK__sync_log__sekola__638EB5B2];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__sync_log__sekola__4850AF91')
    ALTER TABLE [sync_log] DROP CONSTRAINT [FK__sync_log__sekola__4850AF91];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'sync_log')
BEGIN
    DECLARE @reftable_128 nvarchar(60), @constraintname_128 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'sync_log'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_128, @constraintname_128
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_128+' drop constraint '+@constraintname_128)
        FETCH NEXT from refcursor into @reftable_128, @constraintname_128
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [sync_log]
END

CREATE TABLE [sync_log]
(
    [sekolah_id] CHAR(16) NOT NULL,
    [begin_sync] DATETIME(16,3) NOT NULL,
    [end_sync] DATETIME(16,3) NULL,
    [sync_media] CHAR(1) DEFAULT '(''1'')' NOT NULL,
    [is_success] NUMERIC(3,0) NOT NULL,
    [selisih_waktu_server] BIGINT(8,0) NOT NULL,
    [alamat_ip] VARCHAR(50) NOT NULL,
    [pengguna_id] CHAR(16) NOT NULL,
    CONSTRAINT [sync_log_PK] PRIMARY KEY ([sekolah_id],[begin_sync])
);

CREATE INDEX [PK__sync_log__D402B5D3B4A74187] ON [sync_log] ([sekolah_id],[begin_sync]);

BEGIN
ALTER TABLE [sync_log] ADD CONSTRAINT [FK__sync_log__sekola__638EB5B2] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [sync_log] ADD CONSTRAINT [FK__sync_log__sekola__4850AF91] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

-----------------------------------------------------------------------
-- w_pending_job
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__w_pending__sekol__4B8221F7')
    ALTER TABLE [w_pending_job] DROP CONSTRAINT [FK__w_pending__sekol__4B8221F7];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__w_pending__sekol__30441BD6')
    ALTER TABLE [w_pending_job] DROP CONSTRAINT [FK__w_pending__sekol__30441BD6];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__w_pending__kode___4C764630')
    ALTER TABLE [w_pending_job] DROP CONSTRAINT [FK__w_pending__kode___4C764630];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__w_pending__kode___3138400F')
    ALTER TABLE [w_pending_job] DROP CONSTRAINT [FK__w_pending__kode___3138400F];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'w_pending_job')
BEGIN
    DECLARE @reftable_129 nvarchar(60), @constraintname_129 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'w_pending_job'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_129, @constraintname_129
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_129+' drop constraint '+@constraintname_129)
        FETCH NEXT from refcursor into @reftable_129, @constraintname_129
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [w_pending_job]
END

CREATE TABLE [w_pending_job]
(
    [kode_wilayah] CHAR(8) NOT NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [begin_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [w_pending_job_PK] PRIMARY KEY ([kode_wilayah],[sekolah_id],[begin_sync])
);

CREATE INDEX [PENDING_SP_FK] ON [w_pending_job] ([sekolah_id]);

CREATE INDEX [PK__w_pendin__14A211FC39CB7F2E] ON [w_pending_job] ([kode_wilayah],[sekolah_id],[begin_sync]);

BEGIN
ALTER TABLE [w_pending_job] ADD CONSTRAINT [FK__w_pending__sekol__4B8221F7] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [w_pending_job] ADD CONSTRAINT [FK__w_pending__sekol__30441BD6] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [w_pending_job] ADD CONSTRAINT [FK__w_pending__kode___4C764630] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

BEGIN
ALTER TABLE [w_pending_job] ADD CONSTRAINT [FK__w_pending__kode___3138400F] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

-----------------------------------------------------------------------
-- program_inklusi
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__program_i__sekol__7306036C')
    ALTER TABLE [program_inklusi] DROP CONSTRAINT [FK__program_i__sekol__7306036C];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__program_i__sekol__0E44098D')
    ALTER TABLE [program_inklusi] DROP CONSTRAINT [FK__program_i__sekol__0E44098D];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__program_i__kebut__0F382DC6')
    ALTER TABLE [program_inklusi] DROP CONSTRAINT [FK__program_i__kebut__0F382DC6];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__program_i__kebut__73FA27A5')
    ALTER TABLE [program_inklusi] DROP CONSTRAINT [FK__program_i__kebut__73FA27A5];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'program_inklusi')
BEGIN
    DECLARE @reftable_130 nvarchar(60), @constraintname_130 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'program_inklusi'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_130, @constraintname_130
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_130+' drop constraint '+@constraintname_130)
        FETCH NEXT from refcursor into @reftable_130, @constraintname_130
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [program_inklusi]
END

CREATE TABLE [program_inklusi]
(
    [id_pi] CHAR(16) NOT NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [kebutuhan_khusus_id] INT NOT NULL,
    [sk_pi] VARCHAR(40) NOT NULL,
    [tmt_pi] VARCHAR(20) NOT NULL,
    [tst_pi] VARCHAR(20) NULL,
    [ket_pi] VARCHAR(200) NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [program_inklusi_PK] PRIMARY KEY ([id_pi])
);

CREATE INDEX [INKLUSI_SP_FK] ON [program_inklusi] ([sekolah_id]);

CREATE INDEX [PROG_INKLUSI_KK_FK] ON [program_inklusi] ([kebutuhan_khusus_id]);

CREATE INDEX [PK__program___0148A3463D72B36D] ON [program_inklusi] ([id_pi]);

BEGIN
ALTER TABLE [program_inklusi] ADD CONSTRAINT [FK__program_i__sekol__7306036C] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [program_inklusi] ADD CONSTRAINT [FK__program_i__sekol__0E44098D] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [program_inklusi] ADD CONSTRAINT [FK__program_i__kebut__0F382DC6] FOREIGN KEY ([kebutuhan_khusus_id]) REFERENCES [ref].[kebutuhan_khusus] ([kebutuhan_khusus_id])
END
;

BEGIN
ALTER TABLE [program_inklusi] ADD CONSTRAINT [FK__program_i__kebut__73FA27A5] FOREIGN KEY ([kebutuhan_khusus_id]) REFERENCES [ref].[kebutuhan_khusus] ([kebutuhan_khusus_id])
END
;

-----------------------------------------------------------------------
-- ref.bentuk_pendidikan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.bentuk_pendidikan')
BEGIN
    DECLARE @reftable_131 nvarchar(60), @constraintname_131 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.bentuk_pendidikan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_131, @constraintname_131
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_131+' drop constraint '+@constraintname_131)
        FETCH NEXT from refcursor into @reftable_131, @constraintname_131
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[bentuk_pendidikan]
END

CREATE TABLE [ref].[bentuk_pendidikan]
(
    [bentuk_pendidikan_id] SMALLINT(2,0) NOT NULL,
    [nama] VARCHAR(50) NOT NULL,
    [jenjang_paud] NUMERIC(3,0) NOT NULL,
    [jenjang_tk] NUMERIC(3,0) NOT NULL,
    [jenjang_sd] NUMERIC(3,0) NOT NULL,
    [jenjang_smp] NUMERIC(3,0) NOT NULL,
    [jenjang_sma] NUMERIC(3,0) NOT NULL,
    [jenjang_tinggi] NUMERIC(3,0) NOT NULL,
    [direktorat_pembinaan] VARCHAR(40) NULL,
    [aktif] NUMERIC(3,0) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [bentuk_pendidikan_PK] PRIMARY KEY ([bentuk_pendidikan_id])
);

CREATE INDEX [PK__bentuk_p__D346205030D238F3] ON [ref].[bentuk_pendidikan] ([bentuk_pendidikan_id]);

-----------------------------------------------------------------------
-- vld_yayasan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_yayas__yayas__33208881')
    ALTER TABLE [vld_yayasan] DROP CONSTRAINT [FK__vld_yayas__yayas__33208881];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_yayas__yayas__4E5E8EA2')
    ALTER TABLE [vld_yayasan] DROP CONSTRAINT [FK__vld_yayas__yayas__4E5E8EA2];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_yayas__idtyp__4D6A6A69')
    ALTER TABLE [vld_yayasan] DROP CONSTRAINT [FK__vld_yayas__idtyp__4D6A6A69];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_yayas__idtyp__322C6448')
    ALTER TABLE [vld_yayasan] DROP CONSTRAINT [FK__vld_yayas__idtyp__322C6448];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_yayasan')
BEGIN
    DECLARE @reftable_132 nvarchar(60), @constraintname_132 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_yayasan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_132, @constraintname_132
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_132+' drop constraint '+@constraintname_132)
        FETCH NEXT from refcursor into @reftable_132, @constraintname_132
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_yayasan]
END

CREATE TABLE [vld_yayasan]
(
    [yayasan_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_yayasan_PK] PRIMARY KEY ([yayasan_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_yayasan] ([idtype]);

CREATE INDEX [PK__vld_yaya__3926F21B4A60641C] ON [vld_yayasan] ([yayasan_id],[logid]);

BEGIN
ALTER TABLE [vld_yayasan] ADD CONSTRAINT [FK__vld_yayas__yayas__33208881] FOREIGN KEY ([yayasan_id]) REFERENCES [yayasan] ([yayasan_id])
END
;

BEGIN
ALTER TABLE [vld_yayasan] ADD CONSTRAINT [FK__vld_yayas__yayas__4E5E8EA2] FOREIGN KEY ([yayasan_id]) REFERENCES [yayasan] ([yayasan_id])
END
;

BEGIN
ALTER TABLE [vld_yayasan] ADD CONSTRAINT [FK__vld_yayas__idtyp__4D6A6A69] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_yayasan] ADD CONSTRAINT [FK__vld_yayas__idtyp__322C6448] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- riwayat_gaji_berkala
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__riwayat_g__ptk_i__6482D9EB')
    ALTER TABLE [riwayat_gaji_berkala] DROP CONSTRAINT [FK__riwayat_g__ptk_i__6482D9EB];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__riwayat_g__ptk_i__4944D3CA')
    ALTER TABLE [riwayat_gaji_berkala] DROP CONSTRAINT [FK__riwayat_g__ptk_i__4944D3CA];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__riwayat_g__pangk__4A38F803')
    ALTER TABLE [riwayat_gaji_berkala] DROP CONSTRAINT [FK__riwayat_g__pangk__4A38F803];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__riwayat_g__pangk__6576FE24')
    ALTER TABLE [riwayat_gaji_berkala] DROP CONSTRAINT [FK__riwayat_g__pangk__6576FE24];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'riwayat_gaji_berkala')
BEGIN
    DECLARE @reftable_133 nvarchar(60), @constraintname_133 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'riwayat_gaji_berkala'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_133, @constraintname_133
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_133+' drop constraint '+@constraintname_133)
        FETCH NEXT from refcursor into @reftable_133, @constraintname_133
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [riwayat_gaji_berkala]
END

CREATE TABLE [riwayat_gaji_berkala]
(
    [riwayat_gaji_berkala_id] CHAR(16) NOT NULL,
    [ptk_id] CHAR(16) NOT NULL,
    [pangkat_golongan_id] NUMERIC(4,0) NOT NULL,
    [nomor_sk] VARCHAR(40) NOT NULL,
    [tanggal_sk] VARCHAR(20) NOT NULL,
    [tmt_kgb] VARCHAR(20) NOT NULL,
    [masa_kerja_tahun] NUMERIC(4,0) NOT NULL,
    [masa_kerja_bulan] NUMERIC(4,0) NOT NULL,
    [gaji_pokok] NUMERIC(11,0) NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [riwayat_gaji_berkala_PK] PRIMARY KEY ([riwayat_gaji_berkala_id])
);

CREATE INDEX [RIWAYAT_GAJI_PTK_FK] ON [riwayat_gaji_berkala] ([ptk_id]);

CREATE INDEX [RWY_GAJI_LU] ON [riwayat_gaji_berkala] ([Last_update]);

CREATE INDEX [RWYT_KGB_PANG_GOL_FK] ON [riwayat_gaji_berkala] ([pangkat_golongan_id]);

CREATE INDEX [PK__riwayat___E22B2DCB8E31E020] ON [riwayat_gaji_berkala] ([riwayat_gaji_berkala_id]);

BEGIN
ALTER TABLE [riwayat_gaji_berkala] ADD CONSTRAINT [FK__riwayat_g__ptk_i__6482D9EB] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [riwayat_gaji_berkala] ADD CONSTRAINT [FK__riwayat_g__ptk_i__4944D3CA] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [riwayat_gaji_berkala] ADD CONSTRAINT [FK__riwayat_g__pangk__4A38F803] FOREIGN KEY ([pangkat_golongan_id]) REFERENCES [ref].[pangkat_golongan] ([pangkat_golongan_id])
END
;

BEGIN
ALTER TABLE [riwayat_gaji_berkala] ADD CONSTRAINT [FK__riwayat_g__pangk__6576FE24] FOREIGN KEY ([pangkat_golongan_id]) REFERENCES [ref].[pangkat_golongan] ([pangkat_golongan_id])
END
;

-----------------------------------------------------------------------
-- ref.status_kepemilikan_sarpras
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.status_kepemilikan_sarpras')
BEGIN
    DECLARE @reftable_134 nvarchar(60), @constraintname_134 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.status_kepemilikan_sarpras'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_134, @constraintname_134
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_134+' drop constraint '+@constraintname_134)
        FETCH NEXT from refcursor into @reftable_134, @constraintname_134
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[status_kepemilikan_sarpras]
END

CREATE TABLE [ref].[status_kepemilikan_sarpras]
(
    [kepemilikan_sarpras_id] NUMERIC(3,0) NOT NULL,
    [nama] VARCHAR(20) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [status_kepemilikan_sarpras_PK] PRIMARY KEY ([kepemilikan_sarpras_id])
);

CREATE INDEX [PK__status_k__92D3F803657348E0] ON [ref].[status_kepemilikan_sarpras] ([kepemilikan_sarpras_id]);

-----------------------------------------------------------------------
-- pengawas_terdaftar
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengawas___lemba__12149A71')
    ALTER TABLE [pengawas_terdaftar] DROP CONSTRAINT [FK__pengawas___lemba__12149A71];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengawas___lemba__76D69450')
    ALTER TABLE [pengawas_terdaftar] DROP CONSTRAINT [FK__pengawas___lemba__76D69450];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengawas___ptk_i__13FCE2E3')
    ALTER TABLE [pengawas_terdaftar] DROP CONSTRAINT [FK__pengawas___ptk_i__13FCE2E3];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengawas___ptk_i__78BEDCC2')
    ALTER TABLE [pengawas_terdaftar] DROP CONSTRAINT [FK__pengawas___ptk_i__78BEDCC2];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengawas___bidan__102C51FF')
    ALTER TABLE [pengawas_terdaftar] DROP CONSTRAINT [FK__pengawas___bidan__102C51FF];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengawas___bidan__74EE4BDE')
    ALTER TABLE [pengawas_terdaftar] DROP CONSTRAINT [FK__pengawas___bidan__74EE4BDE];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengawas___jenis__11207638')
    ALTER TABLE [pengawas_terdaftar] DROP CONSTRAINT [FK__pengawas___jenis__11207638];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengawas___jenis__75E27017')
    ALTER TABLE [pengawas_terdaftar] DROP CONSTRAINT [FK__pengawas___jenis__75E27017];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengawas___jenja__15E52B55')
    ALTER TABLE [pengawas_terdaftar] DROP CONSTRAINT [FK__pengawas___jenja__15E52B55];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengawas___jenja__7AA72534')
    ALTER TABLE [pengawas_terdaftar] DROP CONSTRAINT [FK__pengawas___jenja__7AA72534];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengawas___mata___77CAB889')
    ALTER TABLE [pengawas_terdaftar] DROP CONSTRAINT [FK__pengawas___mata___77CAB889];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengawas___mata___1308BEAA')
    ALTER TABLE [pengawas_terdaftar] DROP CONSTRAINT [FK__pengawas___mata___1308BEAA];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengawas___tahun__14F1071C')
    ALTER TABLE [pengawas_terdaftar] DROP CONSTRAINT [FK__pengawas___tahun__14F1071C];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__pengawas___tahun__79B300FB')
    ALTER TABLE [pengawas_terdaftar] DROP CONSTRAINT [FK__pengawas___tahun__79B300FB];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'pengawas_terdaftar')
BEGIN
    DECLARE @reftable_135 nvarchar(60), @constraintname_135 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'pengawas_terdaftar'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_135, @constraintname_135
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_135+' drop constraint '+@constraintname_135)
        FETCH NEXT from refcursor into @reftable_135, @constraintname_135
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [pengawas_terdaftar]
END

CREATE TABLE [pengawas_terdaftar]
(
    [pengawas_terdaftar_id] CHAR(16) NOT NULL,
    [ptk_id] CHAR(16) NOT NULL,
    [lembaga_id] CHAR(16) NOT NULL,
    [tahun_ajaran_id] NUMERIC(6,0) NOT NULL,
    [nomor_surat_tugas] VARCHAR(40) NOT NULL,
    [tanggal_surat_tugas] VARCHAR(20) NOT NULL,
    [tmt_tugas] VARCHAR(20) NOT NULL,
    [mata_pelajaran_id] INT NULL,
    [bidang_studi_id] INT NULL,
    [jenjang_kepengawasan_id] NUMERIC(4,0) NOT NULL,
    [aktif_bulan_01] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [aktif_bulan_02] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [aktif_bulan_03] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [aktif_bulan_04] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [aktif_bulan_05] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [aktif_bulan_06] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [aktif_bulan_07] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [aktif_bulan_08] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [aktif_bulan_09] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [aktif_bulan_10] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [aktif_bulan_11] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [aktif_bulan_12] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [jenis_keluar_id] CHAR(1) NULL,
    [tgl_pengawas_keluar] VARCHAR(20) NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [pengawas_terdaftar_PK] PRIMARY KEY ([pengawas_terdaftar_id])
);

CREATE INDEX [PENGAWAS_BIDANG_RUMPUN_FK] ON [pengawas_terdaftar] ([bidang_studi_id]);

CREATE INDEX [PENGAWAS_KELUAR_FK] ON [pengawas_terdaftar] ([jenis_keluar_id]);

CREATE INDEX [PENGAWAS_LEMBAGA_FK] ON [pengawas_terdaftar] ([lembaga_id]);

CREATE INDEX [PENGAWAS_MAPEL_FK] ON [pengawas_terdaftar] ([mata_pelajaran_id]);

CREATE INDEX [PENGAWAS_PTK_FK] ON [pengawas_terdaftar] ([ptk_id]);

CREATE INDEX [PENGAWAS_TAHUN_AJARAN_FK] ON [pengawas_terdaftar] ([tahun_ajaran_id]);

CREATE INDEX [PENGAWAS_TERDAF_LU] ON [pengawas_terdaftar] ([Last_update]);

CREATE INDEX [PTK_JENJANGKEPENGAWASAN_FK] ON [pengawas_terdaftar] ([jenjang_kepengawasan_id]);

CREATE INDEX [PK__pengawas__9CB746EFD3EDE1D7] ON [pengawas_terdaftar] ([pengawas_terdaftar_id]);

BEGIN
ALTER TABLE [pengawas_terdaftar] ADD CONSTRAINT [FK__pengawas___lemba__12149A71] FOREIGN KEY ([lembaga_id]) REFERENCES [lembaga_non_sekolah] ([lembaga_id])
END
;

BEGIN
ALTER TABLE [pengawas_terdaftar] ADD CONSTRAINT [FK__pengawas___lemba__76D69450] FOREIGN KEY ([lembaga_id]) REFERENCES [lembaga_non_sekolah] ([lembaga_id])
END
;

BEGIN
ALTER TABLE [pengawas_terdaftar] ADD CONSTRAINT [FK__pengawas___ptk_i__13FCE2E3] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [pengawas_terdaftar] ADD CONSTRAINT [FK__pengawas___ptk_i__78BEDCC2] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [pengawas_terdaftar] ADD CONSTRAINT [FK__pengawas___bidan__102C51FF] FOREIGN KEY ([bidang_studi_id]) REFERENCES [ref].[bidang_studi] ([bidang_studi_id])
END
;

BEGIN
ALTER TABLE [pengawas_terdaftar] ADD CONSTRAINT [FK__pengawas___bidan__74EE4BDE] FOREIGN KEY ([bidang_studi_id]) REFERENCES [ref].[bidang_studi] ([bidang_studi_id])
END
;

BEGIN
ALTER TABLE [pengawas_terdaftar] ADD CONSTRAINT [FK__pengawas___jenis__11207638] FOREIGN KEY ([jenis_keluar_id]) REFERENCES [ref].[jenis_keluar] ([jenis_keluar_id])
END
;

BEGIN
ALTER TABLE [pengawas_terdaftar] ADD CONSTRAINT [FK__pengawas___jenis__75E27017] FOREIGN KEY ([jenis_keluar_id]) REFERENCES [ref].[jenis_keluar] ([jenis_keluar_id])
END
;

BEGIN
ALTER TABLE [pengawas_terdaftar] ADD CONSTRAINT [FK__pengawas___jenja__15E52B55] FOREIGN KEY ([jenjang_kepengawasan_id]) REFERENCES [ref].[jenjang_kepengawasan] ([jenjang_kepengawasan_id])
END
;

BEGIN
ALTER TABLE [pengawas_terdaftar] ADD CONSTRAINT [FK__pengawas___jenja__7AA72534] FOREIGN KEY ([jenjang_kepengawasan_id]) REFERENCES [ref].[jenjang_kepengawasan] ([jenjang_kepengawasan_id])
END
;

BEGIN
ALTER TABLE [pengawas_terdaftar] ADD CONSTRAINT [FK__pengawas___mata___77CAB889] FOREIGN KEY ([mata_pelajaran_id]) REFERENCES [ref].[mata_pelajaran] ([mata_pelajaran_id])
END
;

BEGIN
ALTER TABLE [pengawas_terdaftar] ADD CONSTRAINT [FK__pengawas___mata___1308BEAA] FOREIGN KEY ([mata_pelajaran_id]) REFERENCES [ref].[mata_pelajaran] ([mata_pelajaran_id])
END
;

BEGIN
ALTER TABLE [pengawas_terdaftar] ADD CONSTRAINT [FK__pengawas___tahun__14F1071C] FOREIGN KEY ([tahun_ajaran_id]) REFERENCES [ref].[tahun_ajaran] ([tahun_ajaran_id])
END
;

BEGIN
ALTER TABLE [pengawas_terdaftar] ADD CONSTRAINT [FK__pengawas___tahun__79B300FB] FOREIGN KEY ([tahun_ajaran_id]) REFERENCES [ref].[tahun_ajaran] ([tahun_ajaran_id])
END
;

-----------------------------------------------------------------------
-- rwy_struktural
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_struk__ptk_i__666B225D')
    ALTER TABLE [rwy_struktural] DROP CONSTRAINT [FK__rwy_struk__ptk_i__666B225D];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_struk__ptk_i__4B2D1C3C')
    ALTER TABLE [rwy_struktural] DROP CONSTRAINT [FK__rwy_struk__ptk_i__4B2D1C3C];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_struk__jabat__4C214075')
    ALTER TABLE [rwy_struktural] DROP CONSTRAINT [FK__rwy_struk__jabat__4C214075];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_struk__jabat__675F4696')
    ALTER TABLE [rwy_struktural] DROP CONSTRAINT [FK__rwy_struk__jabat__675F4696];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'rwy_struktural')
BEGIN
    DECLARE @reftable_136 nvarchar(60), @constraintname_136 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'rwy_struktural'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_136, @constraintname_136
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_136+' drop constraint '+@constraintname_136)
        FETCH NEXT from refcursor into @reftable_136, @constraintname_136
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [rwy_struktural]
END

CREATE TABLE [rwy_struktural]
(
    [riwayat_struktural_id] CHAR(16) NOT NULL,
    [ptk_id] CHAR(16) NOT NULL,
    [jabatan_ptk_id] NUMERIC(5,0) NOT NULL,
    [sk_struktural] VARCHAR(40) NOT NULL,
    [tmt_jabatan] VARCHAR(20) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [rwy_struktural_PK] PRIMARY KEY ([riwayat_struktural_id])
);

CREATE INDEX [JAB_STRU_PTK_FK] ON [rwy_struktural] ([ptk_id]);

CREATE INDEX [RWY_STRUK_LU] ON [rwy_struktural] ([Last_update]);

CREATE INDEX [RWYT_JAB_FK] ON [rwy_struktural] ([jabatan_ptk_id]);

CREATE INDEX [PK__rwy_stru__D08D8EA9D3AE7229] ON [rwy_struktural] ([riwayat_struktural_id]);

BEGIN
ALTER TABLE [rwy_struktural] ADD CONSTRAINT [FK__rwy_struk__ptk_i__666B225D] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [rwy_struktural] ADD CONSTRAINT [FK__rwy_struk__ptk_i__4B2D1C3C] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [rwy_struktural] ADD CONSTRAINT [FK__rwy_struk__jabat__4C214075] FOREIGN KEY ([jabatan_ptk_id]) REFERENCES [ref].[jabatan_tugas_ptk] ([jabatan_ptk_id])
END
;

BEGIN
ALTER TABLE [rwy_struktural] ADD CONSTRAINT [FK__rwy_struk__jabat__675F4696] FOREIGN KEY ([jabatan_ptk_id]) REFERENCES [ref].[jabatan_tugas_ptk] ([jabatan_ptk_id])
END
;

-----------------------------------------------------------------------
-- ref.akses_internet
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.akses_internet')
BEGIN
    DECLARE @reftable_137 nvarchar(60), @constraintname_137 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.akses_internet'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_137, @constraintname_137
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_137+' drop constraint '+@constraintname_137)
        FETCH NEXT from refcursor into @reftable_137, @constraintname_137
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[akses_internet]
END

CREATE TABLE [ref].[akses_internet]
(
    [akses_internet_id] SMALLINT(2,0) NOT NULL,
    [nama] VARCHAR(50) NOT NULL,
    [media] NUMERIC(3,0) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [akses_internet_PK] PRIMARY KEY ([akses_internet_id])
);

CREATE INDEX [PK__akses_in__59A68C7143C1FCB0] ON [ref].[akses_internet] ([akses_internet_id]);

-----------------------------------------------------------------------
-- vld_un
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_un__idtype__4F52B2DB')
    ALTER TABLE [vld_un] DROP CONSTRAINT [FK__vld_un__idtype__4F52B2DB];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_un__idtype__3414ACBA')
    ALTER TABLE [vld_un] DROP CONSTRAINT [FK__vld_un__idtype__3414ACBA];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_un')
BEGIN
    DECLARE @reftable_138 nvarchar(60), @constraintname_138 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_un'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_138, @constraintname_138
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_138+' drop constraint '+@constraintname_138)
        FETCH NEXT from refcursor into @reftable_138, @constraintname_138
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_un]
END

CREATE TABLE [vld_un]
(
    [un_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [app_username] VARCHAR(50) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [vld_un_PK] PRIMARY KEY ([un_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_un] ([idtype]);

CREATE INDEX [PK__vld_un__80C9C3871150061F] ON [vld_un] ([un_id],[logid]);

BEGIN
ALTER TABLE [vld_un] ADD CONSTRAINT [FK__vld_un__idtype__4F52B2DB] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_un] ADD CONSTRAINT [FK__vld_un__idtype__3414ACBA] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- ref.jenis_test
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.jenis_test')
BEGIN
    DECLARE @reftable_139 nvarchar(60), @constraintname_139 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.jenis_test'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_139, @constraintname_139
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_139+' drop constraint '+@constraintname_139)
        FETCH NEXT from refcursor into @reftable_139, @constraintname_139
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[jenis_test]
END

CREATE TABLE [ref].[jenis_test]
(
    [jenis_test_id] NUMERIC(5,0) NOT NULL,
    [jenis_test] VARCHAR(30) NOT NULL,
    [keterangan] VARCHAR(80) NULL,
    [nilai_maks] NUMERIC(8,2) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [jenis_test_PK] PRIMARY KEY ([jenis_test_id])
);

CREATE INDEX [PK__jenis_te__AE45B89ED22B3AC4] ON [ref].[jenis_test] ([jenis_test_id]);

-----------------------------------------------------------------------
-- rwy_pend_formal
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_pend___ptk_i__69478F08')
    ALTER TABLE [rwy_pend_formal] DROP CONSTRAINT [FK__rwy_pend___ptk_i__69478F08];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_pend___ptk_i__4E0988E7')
    ALTER TABLE [rwy_pend_formal] DROP CONSTRAINT [FK__rwy_pend___ptk_i__4E0988E7];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_pend___bidan__4EFDAD20')
    ALTER TABLE [rwy_pend_formal] DROP CONSTRAINT [FK__rwy_pend___bidan__4EFDAD20];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_pend___bidan__6A3BB341')
    ALTER TABLE [rwy_pend_formal] DROP CONSTRAINT [FK__rwy_pend___bidan__6A3BB341];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_pend___gelar__4D1564AE')
    ALTER TABLE [rwy_pend_formal] DROP CONSTRAINT [FK__rwy_pend___gelar__4D1564AE];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_pend___gelar__68536ACF')
    ALTER TABLE [rwy_pend_formal] DROP CONSTRAINT [FK__rwy_pend___gelar__68536ACF];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_pend___jenja__4FF1D159')
    ALTER TABLE [rwy_pend_formal] DROP CONSTRAINT [FK__rwy_pend___jenja__4FF1D159];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_pend___jenja__6B2FD77A')
    ALTER TABLE [rwy_pend_formal] DROP CONSTRAINT [FK__rwy_pend___jenja__6B2FD77A];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'rwy_pend_formal')
BEGIN
    DECLARE @reftable_140 nvarchar(60), @constraintname_140 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'rwy_pend_formal'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_140, @constraintname_140
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_140+' drop constraint '+@constraintname_140)
        FETCH NEXT from refcursor into @reftable_140, @constraintname_140
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [rwy_pend_formal]
END

CREATE TABLE [rwy_pend_formal]
(
    [riwayat_pendidikan_formal_id] CHAR(16) NOT NULL,
    [ptk_id] CHAR(16) NOT NULL,
    [bidang_studi_id] INT NOT NULL,
    [jenjang_pendidikan_id] NUMERIC(4,0) NOT NULL,
    [gelar_akademik_id] INT NULL,
    [satuan_pendidikan_formal] VARCHAR(80) NOT NULL,
    [fakultas] VARCHAR(30) NULL,
    [kependidikan] NUMERIC(3,0) NOT NULL,
    [tahun_masuk] NUMERIC(6,0) NOT NULL,
    [tahun_lulus] NUMERIC(6,0) NULL,
    [nim] VARCHAR(12) NOT NULL,
    [status_kuliah] NUMERIC(3,0) NOT NULL,
    [semester] NUMERIC(4,0) NULL,
    [ipk] NUMERIC(7,2) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [rwy_pend_formal_PK] PRIMARY KEY ([riwayat_pendidikan_formal_id])
);

CREATE INDEX [RIWAYAT_GELAR_FK] ON [rwy_pend_formal] ([gelar_akademik_id]);

CREATE INDEX [RWY_FORMAL_LU] ON [rwy_pend_formal] ([Last_update]);

CREATE INDEX [RWYT_PEND_FORMAL_BIDANGSTUDI_FK] ON [rwy_pend_formal] ([bidang_studi_id]);

CREATE INDEX [RWYT_PEND_FORMAL_JENJANG_FK] ON [rwy_pend_formal] ([jenjang_pendidikan_id]);

CREATE INDEX [RWYT_PEND_FORMAL_PTK_FK] ON [rwy_pend_formal] ([ptk_id]);

CREATE INDEX [PK__rwy_pend__C0FB01EA3B15CF31] ON [rwy_pend_formal] ([riwayat_pendidikan_formal_id]);

BEGIN
ALTER TABLE [rwy_pend_formal] ADD CONSTRAINT [FK__rwy_pend___ptk_i__69478F08] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [rwy_pend_formal] ADD CONSTRAINT [FK__rwy_pend___ptk_i__4E0988E7] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [rwy_pend_formal] ADD CONSTRAINT [FK__rwy_pend___bidan__4EFDAD20] FOREIGN KEY ([bidang_studi_id]) REFERENCES [ref].[bidang_studi] ([bidang_studi_id])
END
;

BEGIN
ALTER TABLE [rwy_pend_formal] ADD CONSTRAINT [FK__rwy_pend___bidan__6A3BB341] FOREIGN KEY ([bidang_studi_id]) REFERENCES [ref].[bidang_studi] ([bidang_studi_id])
END
;

BEGIN
ALTER TABLE [rwy_pend_formal] ADD CONSTRAINT [FK__rwy_pend___gelar__4D1564AE] FOREIGN KEY ([gelar_akademik_id]) REFERENCES [ref].[gelar_akademik] ([gelar_akademik_id])
END
;

BEGIN
ALTER TABLE [rwy_pend_formal] ADD CONSTRAINT [FK__rwy_pend___gelar__68536ACF] FOREIGN KEY ([gelar_akademik_id]) REFERENCES [ref].[gelar_akademik] ([gelar_akademik_id])
END
;

BEGIN
ALTER TABLE [rwy_pend_formal] ADD CONSTRAINT [FK__rwy_pend___jenja__4FF1D159] FOREIGN KEY ([jenjang_pendidikan_id]) REFERENCES [ref].[jenjang_pendidikan] ([jenjang_pendidikan_id])
END
;

BEGIN
ALTER TABLE [rwy_pend_formal] ADD CONSTRAINT [FK__rwy_pend___jenja__6B2FD77A] FOREIGN KEY ([jenjang_pendidikan_id]) REFERENCES [ref].[jenjang_pendidikan] ([jenjang_pendidikan_id])
END
;

-----------------------------------------------------------------------
-- ref.peran
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.peran')
BEGIN
    DECLARE @reftable_141 nvarchar(60), @constraintname_141 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.peran'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_141, @constraintname_141
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_141+' drop constraint '+@constraintname_141)
        FETCH NEXT from refcursor into @reftable_141, @constraintname_141
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[peran]
END

CREATE TABLE [ref].[peran]
(
    [peran_id] INT NOT NULL,
    [nama] VARCHAR(50) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [peran_PK] PRIMARY KEY ([peran_id])
);

CREATE INDEX [PK__peran__98D43F689D0F7E7E] ON [ref].[peran] ([peran_id]);

-----------------------------------------------------------------------
-- vld_tunjangan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_tunja__tunja__36F11965')
    ALTER TABLE [vld_tunjangan] DROP CONSTRAINT [FK__vld_tunja__tunja__36F11965];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_tunja__tunja__522F1F86')
    ALTER TABLE [vld_tunjangan] DROP CONSTRAINT [FK__vld_tunja__tunja__522F1F86];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_tunja__idtyp__513AFB4D')
    ALTER TABLE [vld_tunjangan] DROP CONSTRAINT [FK__vld_tunja__idtyp__513AFB4D];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_tunja__idtyp__35FCF52C')
    ALTER TABLE [vld_tunjangan] DROP CONSTRAINT [FK__vld_tunja__idtyp__35FCF52C];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_tunjangan')
BEGIN
    DECLARE @reftable_142 nvarchar(60), @constraintname_142 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_tunjangan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_142, @constraintname_142
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_142+' drop constraint '+@constraintname_142)
        FETCH NEXT from refcursor into @reftable_142, @constraintname_142
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_tunjangan]
END

CREATE TABLE [vld_tunjangan]
(
    [tunjangan_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_tunjangan_PK] PRIMARY KEY ([tunjangan_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_tunjangan] ([idtype]);

CREATE INDEX [VLD_TUNJANGAN_LU] ON [vld_tunjangan] ([last_update]);

CREATE INDEX [PK__vld_tunj__28018A45713882E3] ON [vld_tunjangan] ([tunjangan_id],[logid]);

BEGIN
ALTER TABLE [vld_tunjangan] ADD CONSTRAINT [FK__vld_tunja__tunja__36F11965] FOREIGN KEY ([tunjangan_id]) REFERENCES [tunjangan] ([tunjangan_id])
END
;

BEGIN
ALTER TABLE [vld_tunjangan] ADD CONSTRAINT [FK__vld_tunja__tunja__522F1F86] FOREIGN KEY ([tunjangan_id]) REFERENCES [tunjangan] ([tunjangan_id])
END
;

BEGIN
ALTER TABLE [vld_tunjangan] ADD CONSTRAINT [FK__vld_tunja__idtyp__513AFB4D] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_tunjangan] ADD CONSTRAINT [FK__vld_tunja__idtyp__35FCF52C] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- anak
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__anak__ptk_id__6C23FBB3')
    ALTER TABLE [anak] DROP CONSTRAINT [FK__anak__ptk_id__6C23FBB3];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__anak__ptk_id__50E5F592')
    ALTER TABLE [anak] DROP CONSTRAINT [FK__anak__ptk_id__50E5F592];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__anak__jenjang_pe__52CE3E04')
    ALTER TABLE [anak] DROP CONSTRAINT [FK__anak__jenjang_pe__52CE3E04];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__anak__jenjang_pe__6E0C4425')
    ALTER TABLE [anak] DROP CONSTRAINT [FK__anak__jenjang_pe__6E0C4425];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__anak__status_ana__51DA19CB')
    ALTER TABLE [anak] DROP CONSTRAINT [FK__anak__status_ana__51DA19CB];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__anak__status_ana__6D181FEC')
    ALTER TABLE [anak] DROP CONSTRAINT [FK__anak__status_ana__6D181FEC];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'anak')
BEGIN
    DECLARE @reftable_143 nvarchar(60), @constraintname_143 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'anak'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_143, @constraintname_143
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_143+' drop constraint '+@constraintname_143)
        FETCH NEXT from refcursor into @reftable_143, @constraintname_143
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [anak]
END

CREATE TABLE [anak]
(
    [anak_id] CHAR(16) NOT NULL,
    [ptk_id] CHAR(16) NOT NULL,
    [status_anak_id] NUMERIC(3,0) NOT NULL,
    [jenjang_pendidikan_id] NUMERIC(4,0) NOT NULL,
    [nisn] CHAR(10) NULL,
    [nama] VARCHAR(50) NOT NULL,
    [jenis_kelamin] CHAR(1) NOT NULL,
    [tempat_lahir] VARCHAR(20) NULL,
    [tanggal_lahir] VARCHAR(20) NOT NULL,
    [tahun_masuk] INT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [anak_PK] PRIMARY KEY ([anak_id])
);

CREATE INDEX [ANAK_JENJANG_FK] ON [anak] ([jenjang_pendidikan_id]);

CREATE INDEX [ANAK_LU] ON [anak] ([Last_update]);

CREATE INDEX [ANAK_PTK_FK] ON [anak] ([ptk_id]);

CREATE INDEX [ANAK_STATUS_FK] ON [anak] ([status_anak_id]);

CREATE INDEX [PK__anak__7561002FF4F9238B] ON [anak] ([anak_id]);

BEGIN
ALTER TABLE [anak] ADD CONSTRAINT [FK__anak__ptk_id__6C23FBB3] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [anak] ADD CONSTRAINT [FK__anak__ptk_id__50E5F592] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [anak] ADD CONSTRAINT [FK__anak__jenjang_pe__52CE3E04] FOREIGN KEY ([jenjang_pendidikan_id]) REFERENCES [ref].[jenjang_pendidikan] ([jenjang_pendidikan_id])
END
;

BEGIN
ALTER TABLE [anak] ADD CONSTRAINT [FK__anak__jenjang_pe__6E0C4425] FOREIGN KEY ([jenjang_pendidikan_id]) REFERENCES [ref].[jenjang_pendidikan] ([jenjang_pendidikan_id])
END
;

BEGIN
ALTER TABLE [anak] ADD CONSTRAINT [FK__anak__status_ana__51DA19CB] FOREIGN KEY ([status_anak_id]) REFERENCES [ref].[status_anak] ([status_anak_id])
END
;

BEGIN
ALTER TABLE [anak] ADD CONSTRAINT [FK__anak__status_ana__6D181FEC] FOREIGN KEY ([status_anak_id]) REFERENCES [ref].[status_anak] ([status_anak_id])
END
;

-----------------------------------------------------------------------
-- ref.akreditasi
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.akreditasi')
BEGIN
    DECLARE @reftable_144 nvarchar(60), @constraintname_144 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.akreditasi'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_144, @constraintname_144
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_144+' drop constraint '+@constraintname_144)
        FETCH NEXT from refcursor into @reftable_144, @constraintname_144
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[akreditasi]
END

CREATE TABLE [ref].[akreditasi]
(
    [akreditasi_id] NUMERIC(3,0) NOT NULL,
    [nama] VARCHAR(30) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [akreditasi_PK] PRIMARY KEY ([akreditasi_id])
);

CREATE INDEX [PK__akredita__5045CDEF30DF9E81] ON [ref].[akreditasi] ([akreditasi_id]);

-----------------------------------------------------------------------
-- ref.sumber_dana
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.sumber_dana')
BEGIN
    DECLARE @reftable_145 nvarchar(60), @constraintname_145 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.sumber_dana'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_145, @constraintname_145
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_145+' drop constraint '+@constraintname_145)
        FETCH NEXT from refcursor into @reftable_145, @constraintname_145
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[sumber_dana]
END

CREATE TABLE [ref].[sumber_dana]
(
    [sumber_dana_id] NUMERIC(5,0) NOT NULL,
    [nama] VARCHAR(50) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [sumber_dana_PK] PRIMARY KEY ([sumber_dana_id])
);

CREATE INDEX [PK__sumber_d__96B7EF7E2F21CFFD] ON [ref].[sumber_dana] ([sumber_dana_id]);

-----------------------------------------------------------------------
-- vld_tugas_tambahan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_tugas__ptk_t__38D961D7')
    ALTER TABLE [vld_tugas_tambahan] DROP CONSTRAINT [FK__vld_tugas__ptk_t__38D961D7];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_tugas__ptk_t__541767F8')
    ALTER TABLE [vld_tugas_tambahan] DROP CONSTRAINT [FK__vld_tugas__ptk_t__541767F8];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_tugas__idtyp__532343BF')
    ALTER TABLE [vld_tugas_tambahan] DROP CONSTRAINT [FK__vld_tugas__idtyp__532343BF];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_tugas__idtyp__37E53D9E')
    ALTER TABLE [vld_tugas_tambahan] DROP CONSTRAINT [FK__vld_tugas__idtyp__37E53D9E];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_tugas_tambahan')
BEGIN
    DECLARE @reftable_146 nvarchar(60), @constraintname_146 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_tugas_tambahan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_146, @constraintname_146
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_146+' drop constraint '+@constraintname_146)
        FETCH NEXT from refcursor into @reftable_146, @constraintname_146
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_tugas_tambahan]
END

CREATE TABLE [vld_tugas_tambahan]
(
    [ptk_tugas_tambahan_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_tugas_tambahan_PK] PRIMARY KEY ([ptk_tugas_tambahan_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_tugas_tambahan] ([idtype]);

CREATE INDEX [VLD_TUGTAM_LU] ON [vld_tugas_tambahan] ([last_update]);

CREATE INDEX [PK__vld_tuga__725CC732E99B27AE] ON [vld_tugas_tambahan] ([ptk_tugas_tambahan_id],[logid]);

BEGIN
ALTER TABLE [vld_tugas_tambahan] ADD CONSTRAINT [FK__vld_tugas__ptk_t__38D961D7] FOREIGN KEY ([ptk_tugas_tambahan_id]) REFERENCES [tugas_tambahan] ([ptk_tugas_tambahan_id])
END
;

BEGIN
ALTER TABLE [vld_tugas_tambahan] ADD CONSTRAINT [FK__vld_tugas__ptk_t__541767F8] FOREIGN KEY ([ptk_tugas_tambahan_id]) REFERENCES [tugas_tambahan] ([ptk_tugas_tambahan_id])
END
;

BEGIN
ALTER TABLE [vld_tugas_tambahan] ADD CONSTRAINT [FK__vld_tugas__idtyp__532343BF] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_tugas_tambahan] ADD CONSTRAINT [FK__vld_tugas__idtyp__37E53D9E] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- diklat
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__diklat__ptk_id__6F00685E')
    ALTER TABLE [diklat] DROP CONSTRAINT [FK__diklat__ptk_id__6F00685E];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__diklat__ptk_id__53C2623D')
    ALTER TABLE [diklat] DROP CONSTRAINT [FK__diklat__ptk_id__53C2623D];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__diklat__jenis_di__54B68676')
    ALTER TABLE [diklat] DROP CONSTRAINT [FK__diklat__jenis_di__54B68676];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__diklat__jenis_di__6FF48C97')
    ALTER TABLE [diklat] DROP CONSTRAINT [FK__diklat__jenis_di__6FF48C97];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'diklat')
BEGIN
    DECLARE @reftable_147 nvarchar(60), @constraintname_147 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'diklat'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_147, @constraintname_147
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_147+' drop constraint '+@constraintname_147)
        FETCH NEXT from refcursor into @reftable_147, @constraintname_147
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [diklat]
END

CREATE TABLE [diklat]
(
    [diklat_id] CHAR(16) NOT NULL,
    [jenis_diklat_id] INT NOT NULL,
    [ptk_id] CHAR(16) NOT NULL,
    [nama] VARCHAR(50) NOT NULL,
    [penyelenggara] VARCHAR(80) NULL,
    [tahun] NUMERIC(6,0) NOT NULL,
    [peran] VARCHAR(30) NULL,
    [tingkat] NUMERIC(4,0) NULL,
    [berapa_jam] NUMERIC(6,0) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [diklat_PK] PRIMARY KEY ([diklat_id])
);

CREATE INDEX [DIKLAT_JENIS_FK] ON [diklat] ([jenis_diklat_id]);

CREATE INDEX [DIKLAT_LU] ON [diklat] ([Updater_ID]);

CREATE INDEX [DIKLAT_PTK_FK] ON [diklat] ([ptk_id]);

CREATE INDEX [PK__diklat__22D373CFC07937E8] ON [diklat] ([diklat_id]);

BEGIN
ALTER TABLE [diklat] ADD CONSTRAINT [FK__diklat__ptk_id__6F00685E] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [diklat] ADD CONSTRAINT [FK__diklat__ptk_id__53C2623D] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [diklat] ADD CONSTRAINT [FK__diklat__jenis_di__54B68676] FOREIGN KEY ([jenis_diklat_id]) REFERENCES [ref].[jenis_diklat] ([jenis_diklat_id])
END
;

BEGIN
ALTER TABLE [diklat] ADD CONSTRAINT [FK__diklat__jenis_di__6FF48C97] FOREIGN KEY ([jenis_diklat_id]) REFERENCES [ref].[jenis_diklat] ([jenis_diklat_id])
END
;

-----------------------------------------------------------------------
-- ref.status_kepemilikan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.status_kepemilikan')
BEGIN
    DECLARE @reftable_148 nvarchar(60), @constraintname_148 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.status_kepemilikan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_148, @constraintname_148
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_148+' drop constraint '+@constraintname_148)
        FETCH NEXT from refcursor into @reftable_148, @constraintname_148
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[status_kepemilikan]
END

CREATE TABLE [ref].[status_kepemilikan]
(
    [status_kepemilikan_id] NUMERIC(3,0) NOT NULL,
    [nama] VARCHAR(20) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [status_kepemilikan_PK] PRIMARY KEY ([status_kepemilikan_id])
);

CREATE INDEX [PK__status_k__4F1B783DA780D1E6] ON [ref].[status_kepemilikan] ([status_kepemilikan_id]);

-----------------------------------------------------------------------
-- ptk_baru
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk_baru__ptk_id__70E8B0D0')
    ALTER TABLE [ptk_baru] DROP CONSTRAINT [FK__ptk_baru__ptk_id__70E8B0D0];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk_baru__ptk_id__55AAAAAF')
    ALTER TABLE [ptk_baru] DROP CONSTRAINT [FK__ptk_baru__ptk_id__55AAAAAF];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk_baru__sekola__71DCD509')
    ALTER TABLE [ptk_baru] DROP CONSTRAINT [FK__ptk_baru__sekola__71DCD509];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk_baru__sekola__569ECEE8')
    ALTER TABLE [ptk_baru] DROP CONSTRAINT [FK__ptk_baru__sekola__569ECEE8];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk_baru__tahun___5792F321')
    ALTER TABLE [ptk_baru] DROP CONSTRAINT [FK__ptk_baru__tahun___5792F321];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__ptk_baru__tahun___72D0F942')
    ALTER TABLE [ptk_baru] DROP CONSTRAINT [FK__ptk_baru__tahun___72D0F942];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ptk_baru')
BEGIN
    DECLARE @reftable_149 nvarchar(60), @constraintname_149 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ptk_baru'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_149, @constraintname_149
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_149+' drop constraint '+@constraintname_149)
        FETCH NEXT from refcursor into @reftable_149, @constraintname_149
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ptk_baru]
END

CREATE TABLE [ptk_baru]
(
    [ptk_baru_id] CHAR(16) NOT NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [tahun_ajaran_id] NUMERIC(6,0) NOT NULL,
    [nama_ptk] VARCHAR(50) NOT NULL,
    [jenis_kelamin] CHAR(1) NOT NULL,
    [nuptk] CHAR(16) NULL,
    [nik] CHAR(16) NOT NULL,
    [tempat_lahir] VARCHAR(20) NOT NULL,
    [tanggal_lahir] VARCHAR(20) NOT NULL,
    [nama_ibu_kandung] VARCHAR(50) NOT NULL,
    [sudah_diproses] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [berhasil_diproses] NUMERIC(3,0) DEFAULT ((0)) NOT NULL,
    [ptk_id] CHAR(16) NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [ptk_baru_PK] PRIMARY KEY ([ptk_baru_id])
);

CREATE INDEX [PTK_BARU_LU] ON [ptk_baru] ([Last_update]);

CREATE INDEX [PTK_BARU_PTK_FK] ON [ptk_baru] ([ptk_id]);

CREATE INDEX [PTK_BARU_SEKOLAH_FK] ON [ptk_baru] ([sekolah_id]);

CREATE INDEX [PTK_BARU_THAJARAN_FK] ON [ptk_baru] ([tahun_ajaran_id]);

CREATE INDEX [PK__ptk_baru__D67261C51E7AD6F5] ON [ptk_baru] ([ptk_baru_id]);

BEGIN
ALTER TABLE [ptk_baru] ADD CONSTRAINT [FK__ptk_baru__ptk_id__70E8B0D0] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [ptk_baru] ADD CONSTRAINT [FK__ptk_baru__ptk_id__55AAAAAF] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [ptk_baru] ADD CONSTRAINT [FK__ptk_baru__sekola__71DCD509] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [ptk_baru] ADD CONSTRAINT [FK__ptk_baru__sekola__569ECEE8] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [ptk_baru] ADD CONSTRAINT [FK__ptk_baru__tahun___5792F321] FOREIGN KEY ([tahun_ajaran_id]) REFERENCES [ref].[tahun_ajaran] ([tahun_ajaran_id])
END
;

BEGIN
ALTER TABLE [ptk_baru] ADD CONSTRAINT [FK__ptk_baru__tahun___72D0F942] FOREIGN KEY ([tahun_ajaran_id]) REFERENCES [ref].[tahun_ajaran] ([tahun_ajaran_id])
END
;

-----------------------------------------------------------------------
-- ref.jabatan_fungsional
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.jabatan_fungsional')
BEGIN
    DECLARE @reftable_150 nvarchar(60), @constraintname_150 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.jabatan_fungsional'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_150, @constraintname_150
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_150+' drop constraint '+@constraintname_150)
        FETCH NEXT from refcursor into @reftable_150, @constraintname_150
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[jabatan_fungsional]
END

CREATE TABLE [ref].[jabatan_fungsional]
(
    [jabatan_fungsional_id] NUMERIC(4,0) NOT NULL,
    [nama] VARCHAR(30) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [jabatan_fungsional_PK] PRIMARY KEY ([jabatan_fungsional_id])
);

CREATE INDEX [PK__jabatan___CA9807EF01668368] ON [ref].[jabatan_fungsional] ([jabatan_fungsional_id]);

-----------------------------------------------------------------------
-- vld_sekolah
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_sekol__sekol__55FFB06A')
    ALTER TABLE [vld_sekolah] DROP CONSTRAINT [FK__vld_sekol__sekol__55FFB06A];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_sekol__sekol__3AC1AA49')
    ALTER TABLE [vld_sekolah] DROP CONSTRAINT [FK__vld_sekol__sekol__3AC1AA49];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_sekol__idtyp__550B8C31')
    ALTER TABLE [vld_sekolah] DROP CONSTRAINT [FK__vld_sekol__idtyp__550B8C31];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_sekol__idtyp__39CD8610')
    ALTER TABLE [vld_sekolah] DROP CONSTRAINT [FK__vld_sekol__idtyp__39CD8610];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_sekolah')
BEGIN
    DECLARE @reftable_151 nvarchar(60), @constraintname_151 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_sekolah'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_151, @constraintname_151
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_151+' drop constraint '+@constraintname_151)
        FETCH NEXT from refcursor into @reftable_151, @constraintname_151
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_sekolah]
END

CREATE TABLE [vld_sekolah]
(
    [sekolah_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_sekolah_PK] PRIMARY KEY ([sekolah_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_sekolah] ([idtype]);

CREATE INDEX [VLD_SEKOLAH_LU] ON [vld_sekolah] ([last_update]);

CREATE INDEX [PK__vld_seko__BE0526F8A3637370] ON [vld_sekolah] ([sekolah_id],[logid]);

BEGIN
ALTER TABLE [vld_sekolah] ADD CONSTRAINT [FK__vld_sekol__sekol__55FFB06A] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [vld_sekolah] ADD CONSTRAINT [FK__vld_sekol__sekol__3AC1AA49] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [vld_sekolah] ADD CONSTRAINT [FK__vld_sekol__idtyp__550B8C31] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_sekolah] ADD CONSTRAINT [FK__vld_sekol__idtyp__39CD8610] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- ref.sertifikasi_iso
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.sertifikasi_iso')
BEGIN
    DECLARE @reftable_152 nvarchar(60), @constraintname_152 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.sertifikasi_iso'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_152, @constraintname_152
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_152+' drop constraint '+@constraintname_152)
        FETCH NEXT from refcursor into @reftable_152, @constraintname_152
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[sertifikasi_iso]
END

CREATE TABLE [ref].[sertifikasi_iso]
(
    [sertifikasi_iso_id] SMALLINT(2,0) NOT NULL,
    [nama] VARCHAR(20) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [sertifikasi_iso_PK] PRIMARY KEY ([sertifikasi_iso_id])
);

CREATE INDEX [PK__sertifik__50A08B2FE5C68D36] ON [ref].[sertifikasi_iso] ([sertifikasi_iso_id]);

-----------------------------------------------------------------------
-- lembaga_non_sekolah
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__lembaga_n__pengg__16D94F8E')
    ALTER TABLE [lembaga_non_sekolah] DROP CONSTRAINT [FK__lembaga_n__pengg__16D94F8E];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__lembaga_n__pengg__7B9B496D')
    ALTER TABLE [lembaga_non_sekolah] DROP CONSTRAINT [FK__lembaga_n__pengg__7B9B496D];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__lembaga_n__jenis__17CD73C7')
    ALTER TABLE [lembaga_non_sekolah] DROP CONSTRAINT [FK__lembaga_n__jenis__17CD73C7];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__lembaga_n__jenis__7C8F6DA6')
    ALTER TABLE [lembaga_non_sekolah] DROP CONSTRAINT [FK__lembaga_n__jenis__7C8F6DA6];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__lembaga_n__kode___18C19800')
    ALTER TABLE [lembaga_non_sekolah] DROP CONSTRAINT [FK__lembaga_n__kode___18C19800];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__lembaga_n__kode___7D8391DF')
    ALTER TABLE [lembaga_non_sekolah] DROP CONSTRAINT [FK__lembaga_n__kode___7D8391DF];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'lembaga_non_sekolah')
BEGIN
    DECLARE @reftable_153 nvarchar(60), @constraintname_153 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'lembaga_non_sekolah'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_153, @constraintname_153
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_153+' drop constraint '+@constraintname_153)
        FETCH NEXT from refcursor into @reftable_153, @constraintname_153
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [lembaga_non_sekolah]
END

CREATE TABLE [lembaga_non_sekolah]
(
    [lembaga_id] CHAR(16) NOT NULL,
    [nama] VARCHAR(80) NOT NULL,
    [singkatan] VARCHAR(15) NULL,
    [jenis_lembaga_id] NUMERIC(7,0) NOT NULL,
    [alamat_jalan] VARCHAR(80) NOT NULL,
    [rt] NUMERIC(4,0) NULL,
    [rw] NUMERIC(4,0) NULL,
    [nama_dusun] VARCHAR(40) NULL,
    [desa_kelurahan] VARCHAR(40) NOT NULL,
    [kode_wilayah] CHAR(8) NOT NULL,
    [kode_pos] CHAR(5) NULL,
    [lintang] NUMERIC(12,6) NULL,
    [bujur] NUMERIC(12,6) NULL,
    [nomor_telepon] VARCHAR(20) NULL,
    [nomor_fax] VARCHAR(20) NULL,
    [email] VARCHAR(50) NULL,
    [website] VARCHAR(100) NULL,
    [pengguna_id] CHAR(16) NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [lembaga_non_sekolah_PK] PRIMARY KEY ([lembaga_id])
);

CREATE INDEX [LEMBAGA_JENIS_FK] ON [lembaga_non_sekolah] ([jenis_lembaga_id]);

CREATE INDEX [LEMBAGA_KECAMATAN_FK] ON [lembaga_non_sekolah] ([kode_wilayah]);

CREATE INDEX [LEMBAGA_PIC_FK] ON [lembaga_non_sekolah] ([pengguna_id]);

CREATE INDEX [NONSEKOLAH_LU] ON [lembaga_non_sekolah] ([Last_update]);

CREATE INDEX [PK__lembaga___FDEA897489A31B46] ON [lembaga_non_sekolah] ([lembaga_id]);

BEGIN
ALTER TABLE [lembaga_non_sekolah] ADD CONSTRAINT [FK__lembaga_n__pengg__16D94F8E] FOREIGN KEY ([pengguna_id]) REFERENCES [pengguna] ([pengguna_id])
END
;

BEGIN
ALTER TABLE [lembaga_non_sekolah] ADD CONSTRAINT [FK__lembaga_n__pengg__7B9B496D] FOREIGN KEY ([pengguna_id]) REFERENCES [pengguna] ([pengguna_id])
END
;

BEGIN
ALTER TABLE [lembaga_non_sekolah] ADD CONSTRAINT [FK__lembaga_n__jenis__17CD73C7] FOREIGN KEY ([jenis_lembaga_id]) REFERENCES [ref].[jenis_lembaga] ([jenis_lembaga_id])
END
;

BEGIN
ALTER TABLE [lembaga_non_sekolah] ADD CONSTRAINT [FK__lembaga_n__jenis__7C8F6DA6] FOREIGN KEY ([jenis_lembaga_id]) REFERENCES [ref].[jenis_lembaga] ([jenis_lembaga_id])
END
;

BEGIN
ALTER TABLE [lembaga_non_sekolah] ADD CONSTRAINT [FK__lembaga_n__kode___18C19800] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

BEGIN
ALTER TABLE [lembaga_non_sekolah] ADD CONSTRAINT [FK__lembaga_n__kode___7D8391DF] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

-----------------------------------------------------------------------
-- ref.jenjang_pendidikan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.jenjang_pendidikan')
BEGIN
    DECLARE @reftable_154 nvarchar(60), @constraintname_154 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.jenjang_pendidikan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_154, @constraintname_154
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_154+' drop constraint '+@constraintname_154)
        FETCH NEXT from refcursor into @reftable_154, @constraintname_154
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[jenjang_pendidikan]
END

CREATE TABLE [ref].[jenjang_pendidikan]
(
    [jenjang_pendidikan_id] NUMERIC(4,0) NOT NULL,
    [nama] VARCHAR(25) NOT NULL,
    [jenjang_lembaga] NUMERIC(3,0) NOT NULL,
    [jenjang_orang] NUMERIC(3,0) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [jenjang_pendidikan_PK] PRIMARY KEY ([jenjang_pendidikan_id])
);

CREATE INDEX [PK__jenjang___269FFFAF780543B6] ON [ref].[jenjang_pendidikan] ([jenjang_pendidikan_id]);

-----------------------------------------------------------------------
-- vld_sarana
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_saran__saran__3CA9F2BB')
    ALTER TABLE [vld_sarana] DROP CONSTRAINT [FK__vld_saran__saran__3CA9F2BB];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_saran__saran__57E7F8DC')
    ALTER TABLE [vld_sarana] DROP CONSTRAINT [FK__vld_saran__saran__57E7F8DC];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_saran__idtyp__56F3D4A3')
    ALTER TABLE [vld_sarana] DROP CONSTRAINT [FK__vld_saran__idtyp__56F3D4A3];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_saran__idtyp__3BB5CE82')
    ALTER TABLE [vld_sarana] DROP CONSTRAINT [FK__vld_saran__idtyp__3BB5CE82];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_sarana')
BEGIN
    DECLARE @reftable_155 nvarchar(60), @constraintname_155 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_sarana'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_155, @constraintname_155
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_155+' drop constraint '+@constraintname_155)
        FETCH NEXT from refcursor into @reftable_155, @constraintname_155
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_sarana]
END

CREATE TABLE [vld_sarana]
(
    [sarana_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_sarana_PK] PRIMARY KEY ([sarana_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_sarana] ([idtype]);

CREATE INDEX [VLD_SARANA_LU] ON [vld_sarana] ([last_update]);

CREATE INDEX [PK__vld_sara__E35A4F092DB6DF12] ON [vld_sarana] ([sarana_id],[logid]);

BEGIN
ALTER TABLE [vld_sarana] ADD CONSTRAINT [FK__vld_saran__saran__3CA9F2BB] FOREIGN KEY ([sarana_id]) REFERENCES [sarana] ([sarana_id])
END
;

BEGIN
ALTER TABLE [vld_sarana] ADD CONSTRAINT [FK__vld_saran__saran__57E7F8DC] FOREIGN KEY ([sarana_id]) REFERENCES [sarana] ([sarana_id])
END
;

BEGIN
ALTER TABLE [vld_sarana] ADD CONSTRAINT [FK__vld_saran__idtyp__56F3D4A3] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_sarana] ADD CONSTRAINT [FK__vld_saran__idtyp__3BB5CE82] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- tunjangan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__tunjangan__ptk_i__73C51D7B')
    ALTER TABLE [tunjangan] DROP CONSTRAINT [FK__tunjangan__ptk_i__73C51D7B];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__tunjangan__ptk_i__5887175A')
    ALTER TABLE [tunjangan] DROP CONSTRAINT [FK__tunjangan__ptk_i__5887175A];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__tunjangan__jenis__597B3B93')
    ALTER TABLE [tunjangan] DROP CONSTRAINT [FK__tunjangan__jenis__597B3B93];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__tunjangan__jenis__74B941B4')
    ALTER TABLE [tunjangan] DROP CONSTRAINT [FK__tunjangan__jenis__74B941B4];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'tunjangan')
BEGIN
    DECLARE @reftable_156 nvarchar(60), @constraintname_156 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'tunjangan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_156, @constraintname_156
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_156+' drop constraint '+@constraintname_156)
        FETCH NEXT from refcursor into @reftable_156, @constraintname_156
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [tunjangan]
END

CREATE TABLE [tunjangan]
(
    [tunjangan_id] CHAR(16) NOT NULL,
    [ptk_id] CHAR(16) NOT NULL,
    [jenis_tunjangan_id] INT NULL,
    [nama] VARCHAR(50) NOT NULL,
    [instansi] VARCHAR(80) NULL,
    [sumber_dana] VARCHAR(30) NULL,
    [dari_tahun] NUMERIC(6,0) NOT NULL,
    [sampai_tahun] NUMERIC(6,0) NULL,
    [nominal] NUMERIC(18,2) NOT NULL,
    [status] INT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [tunjangan_PK] PRIMARY KEY ([tunjangan_id])
);

CREATE INDEX [TUNJANGAN_JENIS_FK] ON [tunjangan] ([jenis_tunjangan_id]);

CREATE INDEX [TUNJANGAN_LU] ON [tunjangan] ([Last_update]);

CREATE INDEX [TUNJANGAN_PTK_FK] ON [tunjangan] ([ptk_id]);

CREATE INDEX [PK__tunjanga__6F8205629E42ADDF] ON [tunjangan] ([tunjangan_id]);

BEGIN
ALTER TABLE [tunjangan] ADD CONSTRAINT [FK__tunjangan__ptk_i__73C51D7B] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [tunjangan] ADD CONSTRAINT [FK__tunjangan__ptk_i__5887175A] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [tunjangan] ADD CONSTRAINT [FK__tunjangan__jenis__597B3B93] FOREIGN KEY ([jenis_tunjangan_id]) REFERENCES [ref].[jenis_tunjangan] ([jenis_tunjangan_id])
END
;

BEGIN
ALTER TABLE [tunjangan] ADD CONSTRAINT [FK__tunjangan__jenis__74B941B4] FOREIGN KEY ([jenis_tunjangan_id]) REFERENCES [ref].[jenis_tunjangan] ([jenis_tunjangan_id])
END
;

-----------------------------------------------------------------------
-- demografi
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__demografi__kode___19B5BC39')
    ALTER TABLE [demografi] DROP CONSTRAINT [FK__demografi__kode___19B5BC39];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__demografi__kode___7E77B618')
    ALTER TABLE [demografi] DROP CONSTRAINT [FK__demografi__kode___7E77B618];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__demografi__tahun__1AA9E072')
    ALTER TABLE [demografi] DROP CONSTRAINT [FK__demografi__tahun__1AA9E072];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__demografi__tahun__7F6BDA51')
    ALTER TABLE [demografi] DROP CONSTRAINT [FK__demografi__tahun__7F6BDA51];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'demografi')
BEGIN
    DECLARE @reftable_157 nvarchar(60), @constraintname_157 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'demografi'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_157, @constraintname_157
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_157+' drop constraint '+@constraintname_157)
        FETCH NEXT from refcursor into @reftable_157, @constraintname_157
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [demografi]
END

CREATE TABLE [demografi]
(
    [demografi_id] CHAR(16) NOT NULL,
    [kode_wilayah] CHAR(8) NOT NULL,
    [tahun_ajaran_id] NUMERIC(6,0) NOT NULL,
    [usia_5] BIGINT(8,0) NOT NULL,
    [usia_7] BIGINT(8,0) NOT NULL,
    [usia_13] BIGINT(8,0) NOT NULL,
    [usia_16] BIGINT(8,0) NOT NULL,
    [usia_18] BIGINT(8,0) NOT NULL,
    [jumlah_penduduk] BIGINT(8,0) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [demografi_PK] PRIMARY KEY ([demografi_id])
);

CREATE INDEX [DEMOGRAFI_LU] ON [demografi] ([Last_update]);

CREATE INDEX [DEMOGRAFI_PERIODE_FK] ON [demografi] ([tahun_ajaran_id]);

CREATE INDEX [DEMOGRAFI_WILAYAH_FK] ON [demografi] ([kode_wilayah]);

CREATE INDEX [PK__demograf__EA121978E2B3493B] ON [demografi] ([demografi_id]);

BEGIN
ALTER TABLE [demografi] ADD CONSTRAINT [FK__demografi__kode___19B5BC39] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

BEGIN
ALTER TABLE [demografi] ADD CONSTRAINT [FK__demografi__kode___7E77B618] FOREIGN KEY ([kode_wilayah]) REFERENCES [ref].[mst_wilayah] ([kode_wilayah])
END
;

BEGIN
ALTER TABLE [demografi] ADD CONSTRAINT [FK__demografi__tahun__1AA9E072] FOREIGN KEY ([tahun_ajaran_id]) REFERENCES [ref].[tahun_ajaran] ([tahun_ajaran_id])
END
;

BEGIN
ALTER TABLE [demografi] ADD CONSTRAINT [FK__demografi__tahun__7F6BDA51] FOREIGN KEY ([tahun_ajaran_id]) REFERENCES [ref].[tahun_ajaran] ([tahun_ajaran_id])
END
;

-----------------------------------------------------------------------
-- ref.level_wilayah
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.level_wilayah')
BEGIN
    DECLARE @reftable_158 nvarchar(60), @constraintname_158 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.level_wilayah'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_158, @constraintname_158
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_158+' drop constraint '+@constraintname_158)
        FETCH NEXT from refcursor into @reftable_158, @constraintname_158
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[level_wilayah]
END

CREATE TABLE [ref].[level_wilayah]
(
    [id_level_wilayah] SMALLINT(2,0) NOT NULL,
    [level_wilayah] VARCHAR(15) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [level_wilayah_PK] PRIMARY KEY ([id_level_wilayah])
);

CREATE INDEX [PK__level_wi__C5E56724673B6026] ON [ref].[level_wilayah] ([id_level_wilayah]);

-----------------------------------------------------------------------
-- rwy_sertifikasi
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_serti__ptk_i__7795AE5F')
    ALTER TABLE [rwy_sertifikasi] DROP CONSTRAINT [FK__rwy_serti__ptk_i__7795AE5F];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_serti__ptk_i__5C57A83E')
    ALTER TABLE [rwy_sertifikasi] DROP CONSTRAINT [FK__rwy_serti__ptk_i__5C57A83E];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_serti__bidan__5A6F5FCC')
    ALTER TABLE [rwy_sertifikasi] DROP CONSTRAINT [FK__rwy_serti__bidan__5A6F5FCC];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_serti__bidan__75AD65ED')
    ALTER TABLE [rwy_sertifikasi] DROP CONSTRAINT [FK__rwy_serti__bidan__75AD65ED];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_serti__id_je__5B638405')
    ALTER TABLE [rwy_sertifikasi] DROP CONSTRAINT [FK__rwy_serti__id_je__5B638405];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_serti__id_je__76A18A26')
    ALTER TABLE [rwy_sertifikasi] DROP CONSTRAINT [FK__rwy_serti__id_je__76A18A26];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'rwy_sertifikasi')
BEGIN
    DECLARE @reftable_159 nvarchar(60), @constraintname_159 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'rwy_sertifikasi'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_159, @constraintname_159
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_159+' drop constraint '+@constraintname_159)
        FETCH NEXT from refcursor into @reftable_159, @constraintname_159
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [rwy_sertifikasi]
END

CREATE TABLE [rwy_sertifikasi]
(
    [riwayat_sertifikasi_id] CHAR(16) NOT NULL,
    [ptk_id] CHAR(16) NOT NULL,
    [bidang_studi_id] INT NOT NULL,
    [id_jenis_sertifikasi] NUMERIC(5,0) NOT NULL,
    [tahun_sertifikasi] NUMERIC(6,0) NOT NULL,
    [nomor_sertifikat] VARCHAR(40) NOT NULL,
    [nrg] VARCHAR(12) NULL,
    [nomor_peserta] VARCHAR(14) NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [rwy_sertifikasi_PK] PRIMARY KEY ([riwayat_sertifikasi_id])
);

CREATE INDEX [RWY_SERT_LU] ON [rwy_sertifikasi] ([Last_update]);

CREATE INDEX [RWYT_BIDANG_SERTIFIKASI_FK] ON [rwy_sertifikasi] ([bidang_studi_id]);

CREATE INDEX [RWYT_SERT_JENIS_FK] ON [rwy_sertifikasi] ([id_jenis_sertifikasi]);

CREATE INDEX [RWYT_SERT_PTK_FK] ON [rwy_sertifikasi] ([ptk_id]);

CREATE INDEX [PK__rwy_sert__BAF9354E8A1E51B6] ON [rwy_sertifikasi] ([riwayat_sertifikasi_id]);

BEGIN
ALTER TABLE [rwy_sertifikasi] ADD CONSTRAINT [FK__rwy_serti__ptk_i__7795AE5F] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [rwy_sertifikasi] ADD CONSTRAINT [FK__rwy_serti__ptk_i__5C57A83E] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [rwy_sertifikasi] ADD CONSTRAINT [FK__rwy_serti__bidan__5A6F5FCC] FOREIGN KEY ([bidang_studi_id]) REFERENCES [ref].[bidang_studi] ([bidang_studi_id])
END
;

BEGIN
ALTER TABLE [rwy_sertifikasi] ADD CONSTRAINT [FK__rwy_serti__bidan__75AD65ED] FOREIGN KEY ([bidang_studi_id]) REFERENCES [ref].[bidang_studi] ([bidang_studi_id])
END
;

BEGIN
ALTER TABLE [rwy_sertifikasi] ADD CONSTRAINT [FK__rwy_serti__id_je__5B638405] FOREIGN KEY ([id_jenis_sertifikasi]) REFERENCES [ref].[jenis_sertifikasi] ([id_jenis_sertifikasi])
END
;

BEGIN
ALTER TABLE [rwy_sertifikasi] ADD CONSTRAINT [FK__rwy_serti__id_je__76A18A26] FOREIGN KEY ([id_jenis_sertifikasi]) REFERENCES [ref].[jenis_sertifikasi] ([id_jenis_sertifikasi])
END
;

-----------------------------------------------------------------------
-- anggota_gugus
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__anggota_g__gugus__005FFE8A')
    ALTER TABLE [anggota_gugus] DROP CONSTRAINT [FK__anggota_g__gugus__005FFE8A];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__anggota_g__gugus__1B9E04AB')
    ALTER TABLE [anggota_gugus] DROP CONSTRAINT [FK__anggota_g__gugus__1B9E04AB];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__anggota_g__sekol__015422C3')
    ALTER TABLE [anggota_gugus] DROP CONSTRAINT [FK__anggota_g__sekol__015422C3];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__anggota_g__sekol__1C9228E4')
    ALTER TABLE [anggota_gugus] DROP CONSTRAINT [FK__anggota_g__sekol__1C9228E4];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'anggota_gugus')
BEGIN
    DECLARE @reftable_160 nvarchar(60), @constraintname_160 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'anggota_gugus'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_160, @constraintname_160
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_160+' drop constraint '+@constraintname_160)
        FETCH NEXT from refcursor into @reftable_160, @constraintname_160
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [anggota_gugus]
END

CREATE TABLE [anggota_gugus]
(
    [gugus_id] CHAR(16) NOT NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [anggota_gugus_PK] PRIMARY KEY ([gugus_id],[sekolah_id])
);

CREATE INDEX [ANGGOTA_GUGUS_LU] ON [anggota_gugus] ([Last_update]);

CREATE INDEX [ANGGOTA_GUGUS_SEKOLAH_FK] ON [anggota_gugus] ([sekolah_id]);

CREATE INDEX [PK__anggota___A95EA48355EF9739] ON [anggota_gugus] ([gugus_id],[sekolah_id]);

BEGIN
ALTER TABLE [anggota_gugus] ADD CONSTRAINT [FK__anggota_g__gugus__005FFE8A] FOREIGN KEY ([gugus_id]) REFERENCES [gugus_sekolah] ([gugus_id])
END
;

BEGIN
ALTER TABLE [anggota_gugus] ADD CONSTRAINT [FK__anggota_g__gugus__1B9E04AB] FOREIGN KEY ([gugus_id]) REFERENCES [gugus_sekolah] ([gugus_id])
END
;

BEGIN
ALTER TABLE [anggota_gugus] ADD CONSTRAINT [FK__anggota_g__sekol__015422C3] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [anggota_gugus] ADD CONSTRAINT [FK__anggota_g__sekol__1C9228E4] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

-----------------------------------------------------------------------
-- ref.pangkat_golongan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.pangkat_golongan')
BEGIN
    DECLARE @reftable_161 nvarchar(60), @constraintname_161 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.pangkat_golongan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_161, @constraintname_161
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_161+' drop constraint '+@constraintname_161)
        FETCH NEXT from refcursor into @reftable_161, @constraintname_161
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[pangkat_golongan]
END

CREATE TABLE [ref].[pangkat_golongan]
(
    [pangkat_golongan_id] NUMERIC(4,0) NOT NULL,
    [kode] VARCHAR(5) NOT NULL,
    [nama] VARCHAR(20) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [pangkat_golongan_PK] PRIMARY KEY ([pangkat_golongan_id])
);

CREATE INDEX [PK__pangkat___80B797927F193985] ON [ref].[pangkat_golongan] ([pangkat_golongan_id]);

-----------------------------------------------------------------------
-- vld_rwy_struktural
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_rwy_s__riway__3D9E16F4')
    ALTER TABLE [vld_rwy_struktural] DROP CONSTRAINT [FK__vld_rwy_s__riway__3D9E16F4];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_rwy_s__riway__58DC1D15')
    ALTER TABLE [vld_rwy_struktural] DROP CONSTRAINT [FK__vld_rwy_s__riway__58DC1D15];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_rwy_s__idtyp__59D0414E')
    ALTER TABLE [vld_rwy_struktural] DROP CONSTRAINT [FK__vld_rwy_s__idtyp__59D0414E];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_rwy_s__idtyp__3E923B2D')
    ALTER TABLE [vld_rwy_struktural] DROP CONSTRAINT [FK__vld_rwy_s__idtyp__3E923B2D];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_rwy_struktural')
BEGIN
    DECLARE @reftable_162 nvarchar(60), @constraintname_162 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_rwy_struktural'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_162, @constraintname_162
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_162+' drop constraint '+@constraintname_162)
        FETCH NEXT from refcursor into @reftable_162, @constraintname_162
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_rwy_struktural]
END

CREATE TABLE [vld_rwy_struktural]
(
    [riwayat_struktural_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_rwy_struktural_PK] PRIMARY KEY ([riwayat_struktural_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_rwy_struktural] ([idtype]);

CREATE INDEX [VLD_RWY_STRUK_LU] ON [vld_rwy_struktural] ([last_update]);

CREATE INDEX [PK__vld_rwy___970E018E57DE6EA6] ON [vld_rwy_struktural] ([riwayat_struktural_id],[logid]);

BEGIN
ALTER TABLE [vld_rwy_struktural] ADD CONSTRAINT [FK__vld_rwy_s__riway__3D9E16F4] FOREIGN KEY ([riwayat_struktural_id]) REFERENCES [rwy_struktural] ([riwayat_struktural_id])
END
;

BEGIN
ALTER TABLE [vld_rwy_struktural] ADD CONSTRAINT [FK__vld_rwy_s__riway__58DC1D15] FOREIGN KEY ([riwayat_struktural_id]) REFERENCES [rwy_struktural] ([riwayat_struktural_id])
END
;

BEGIN
ALTER TABLE [vld_rwy_struktural] ADD CONSTRAINT [FK__vld_rwy_s__idtyp__59D0414E] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_rwy_struktural] ADD CONSTRAINT [FK__vld_rwy_s__idtyp__3E923B2D] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- ref.jenis_prestasi
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.jenis_prestasi')
BEGIN
    DECLARE @reftable_163 nvarchar(60), @constraintname_163 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.jenis_prestasi'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_163, @constraintname_163
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_163+' drop constraint '+@constraintname_163)
        FETCH NEXT from refcursor into @reftable_163, @constraintname_163
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[jenis_prestasi]
END

CREATE TABLE [ref].[jenis_prestasi]
(
    [jenis_prestasi_id] INT NOT NULL,
    [nama] VARCHAR(50) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [jenis_prestasi_PK] PRIMARY KEY ([jenis_prestasi_id])
);

CREATE INDEX [PK__jenis_pr__8D52B0B4DB4F5166] ON [ref].[jenis_prestasi] ([jenis_prestasi_id]);

-----------------------------------------------------------------------
-- tugas_tambahan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__tugas_tam__ptk_i__797DF6D1')
    ALTER TABLE [tugas_tambahan] DROP CONSTRAINT [FK__tugas_tam__ptk_i__797DF6D1];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__tugas_tam__ptk_i__5E3FF0B0')
    ALTER TABLE [tugas_tambahan] DROP CONSTRAINT [FK__tugas_tam__ptk_i__5E3FF0B0];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__tugas_tam__sekol__7A721B0A')
    ALTER TABLE [tugas_tambahan] DROP CONSTRAINT [FK__tugas_tam__sekol__7A721B0A];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__tugas_tam__sekol__5F3414E9')
    ALTER TABLE [tugas_tambahan] DROP CONSTRAINT [FK__tugas_tam__sekol__5F3414E9];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__tugas_tam__jabat__5D4BCC77')
    ALTER TABLE [tugas_tambahan] DROP CONSTRAINT [FK__tugas_tam__jabat__5D4BCC77];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__tugas_tam__jabat__7889D298')
    ALTER TABLE [tugas_tambahan] DROP CONSTRAINT [FK__tugas_tam__jabat__7889D298];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'tugas_tambahan')
BEGIN
    DECLARE @reftable_164 nvarchar(60), @constraintname_164 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'tugas_tambahan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_164, @constraintname_164
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_164+' drop constraint '+@constraintname_164)
        FETCH NEXT from refcursor into @reftable_164, @constraintname_164
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [tugas_tambahan]
END

CREATE TABLE [tugas_tambahan]
(
    [ptk_tugas_tambahan_id] CHAR(16) NOT NULL,
    [ptk_id] CHAR(16) NOT NULL,
    [jabatan_ptk_id] NUMERIC(5,0) NOT NULL,
    [sekolah_id] CHAR(16) NOT NULL,
    [jumlah_jam] NUMERIC(4,0) NOT NULL,
    [nomor_sk] VARCHAR(40) NOT NULL,
    [tmt_tambahan] VARCHAR(20) NOT NULL,
    [tst_tambahan] VARCHAR(20) NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [tugas_tambahan_PK] PRIMARY KEY ([ptk_tugas_tambahan_id])
);

CREATE INDEX [TUG_TAMBAHAN_JAB_FK] ON [tugas_tambahan] ([jabatan_ptk_id]);

CREATE INDEX [TUGTAM_LU] ON [tugas_tambahan] ([Last_update]);

CREATE INDEX [TUGTAM_PTK_FK] ON [tugas_tambahan] ([ptk_id]);

CREATE INDEX [TUGTAM_SEKOLAH_FK] ON [tugas_tambahan] ([sekolah_id]);

CREATE INDEX [PK__tugas_ta__35DF4815516A57E0] ON [tugas_tambahan] ([ptk_tugas_tambahan_id]);

BEGIN
ALTER TABLE [tugas_tambahan] ADD CONSTRAINT [FK__tugas_tam__ptk_i__797DF6D1] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [tugas_tambahan] ADD CONSTRAINT [FK__tugas_tam__ptk_i__5E3FF0B0] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [tugas_tambahan] ADD CONSTRAINT [FK__tugas_tam__sekol__7A721B0A] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [tugas_tambahan] ADD CONSTRAINT [FK__tugas_tam__sekol__5F3414E9] FOREIGN KEY ([sekolah_id]) REFERENCES [sekolah] ([sekolah_id])
END
;

BEGIN
ALTER TABLE [tugas_tambahan] ADD CONSTRAINT [FK__tugas_tam__jabat__5D4BCC77] FOREIGN KEY ([jabatan_ptk_id]) REFERENCES [ref].[jabatan_tugas_ptk] ([jabatan_ptk_id])
END
;

BEGIN
ALTER TABLE [tugas_tambahan] ADD CONSTRAINT [FK__tugas_tam__jabat__7889D298] FOREIGN KEY ([jabatan_ptk_id]) REFERENCES [ref].[jabatan_tugas_ptk] ([jabatan_ptk_id])
END
;

-----------------------------------------------------------------------
-- akreditasi_prodi
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__akreditas__jurus__033C6B35')
    ALTER TABLE [akreditasi_prodi] DROP CONSTRAINT [FK__akreditas__jurus__033C6B35];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__akreditas__jurus__1E7A7156')
    ALTER TABLE [akreditasi_prodi] DROP CONSTRAINT [FK__akreditas__jurus__1E7A7156];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__akreditas__akred__04308F6E')
    ALTER TABLE [akreditasi_prodi] DROP CONSTRAINT [FK__akreditas__akred__04308F6E];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__akreditas__akred__1F6E958F')
    ALTER TABLE [akreditasi_prodi] DROP CONSTRAINT [FK__akreditas__akred__1F6E958F];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__akreditas__la_id__024846FC')
    ALTER TABLE [akreditasi_prodi] DROP CONSTRAINT [FK__akreditas__la_id__024846FC];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__akreditas__la_id__1D864D1D')
    ALTER TABLE [akreditasi_prodi] DROP CONSTRAINT [FK__akreditas__la_id__1D864D1D];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'akreditasi_prodi')
BEGIN
    DECLARE @reftable_165 nvarchar(60), @constraintname_165 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'akreditasi_prodi'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_165, @constraintname_165
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_165+' drop constraint '+@constraintname_165)
        FETCH NEXT from refcursor into @reftable_165, @constraintname_165
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [akreditasi_prodi]
END

CREATE TABLE [akreditasi_prodi]
(
    [akred_prodi_id] CHAR(16) NOT NULL,
    [akreditasi_id] NUMERIC(3,0) NOT NULL,
    [la_id] CHAR(5) NOT NULL,
    [jurusan_sp_id] CHAR(16) NOT NULL,
    [no_sk_akred] VARCHAR(40) NOT NULL,
    [tgl_sk_akred] VARCHAR(20) NOT NULL,
    [tgl_akhir_akred] VARCHAR(20) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [akreditasi_prodi_PK] PRIMARY KEY ([akred_prodi_id])
);

CREATE INDEX [AKRED_LEMBAGA_FK] ON [akreditasi_prodi] ([la_id]);

CREATE INDEX [AKRED_PRODI_FK] ON [akreditasi_prodi] ([jurusan_sp_id]);

CREATE INDEX [PRODI_AKREDITASI_FK] ON [akreditasi_prodi] ([akreditasi_id]);

CREATE INDEX [PK__akredita__06DAB22277B14DAA] ON [akreditasi_prodi] ([akred_prodi_id]);

BEGIN
ALTER TABLE [akreditasi_prodi] ADD CONSTRAINT [FK__akreditas__jurus__033C6B35] FOREIGN KEY ([jurusan_sp_id]) REFERENCES [jurusan_sp] ([jurusan_sp_id])
END
;

BEGIN
ALTER TABLE [akreditasi_prodi] ADD CONSTRAINT [FK__akreditas__jurus__1E7A7156] FOREIGN KEY ([jurusan_sp_id]) REFERENCES [jurusan_sp] ([jurusan_sp_id])
END
;

BEGIN
ALTER TABLE [akreditasi_prodi] ADD CONSTRAINT [FK__akreditas__akred__04308F6E] FOREIGN KEY ([akreditasi_id]) REFERENCES [ref].[akreditasi] ([akreditasi_id])
END
;

BEGIN
ALTER TABLE [akreditasi_prodi] ADD CONSTRAINT [FK__akreditas__akred__1F6E958F] FOREIGN KEY ([akreditasi_id]) REFERENCES [ref].[akreditasi] ([akreditasi_id])
END
;

BEGIN
ALTER TABLE [akreditasi_prodi] ADD CONSTRAINT [FK__akreditas__la_id__024846FC] FOREIGN KEY ([la_id]) REFERENCES [ref].[lembaga_akreditasi] ([la_id])
END
;

BEGIN
ALTER TABLE [akreditasi_prodi] ADD CONSTRAINT [FK__akreditas__la_id__1D864D1D] FOREIGN KEY ([la_id]) REFERENCES [ref].[lembaga_akreditasi] ([la_id])
END
;

-----------------------------------------------------------------------
-- ref.tahun_ajaran
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.tahun_ajaran')
BEGIN
    DECLARE @reftable_166 nvarchar(60), @constraintname_166 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.tahun_ajaran'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_166, @constraintname_166
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_166+' drop constraint '+@constraintname_166)
        FETCH NEXT from refcursor into @reftable_166, @constraintname_166
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[tahun_ajaran]
END

CREATE TABLE [ref].[tahun_ajaran]
(
    [tahun_ajaran_id] NUMERIC(6,0) NOT NULL,
    [nama] VARCHAR(10) NOT NULL,
    [periode_aktif] NUMERIC(3,0) NOT NULL,
    [tanggal_mulai] VARCHAR(20) NOT NULL,
    [tanggal_selesai] VARCHAR(20) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [tahun_ajaran_PK] PRIMARY KEY ([tahun_ajaran_id])
);

CREATE INDEX [PK__tahun_aj__9E6E10E14D4E23F6] ON [ref].[tahun_ajaran] ([tahun_ajaran_id]);

-----------------------------------------------------------------------
-- vld_rwy_sertifikasi
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_rwy_s__riway__3F865F66')
    ALTER TABLE [vld_rwy_sertifikasi] DROP CONSTRAINT [FK__vld_rwy_s__riway__3F865F66];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_rwy_s__riway__5AC46587')
    ALTER TABLE [vld_rwy_sertifikasi] DROP CONSTRAINT [FK__vld_rwy_s__riway__5AC46587];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_rwy_s__idtyp__5BB889C0')
    ALTER TABLE [vld_rwy_sertifikasi] DROP CONSTRAINT [FK__vld_rwy_s__idtyp__5BB889C0];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_rwy_s__idtyp__407A839F')
    ALTER TABLE [vld_rwy_sertifikasi] DROP CONSTRAINT [FK__vld_rwy_s__idtyp__407A839F];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_rwy_sertifikasi')
BEGIN
    DECLARE @reftable_167 nvarchar(60), @constraintname_167 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_rwy_sertifikasi'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_167, @constraintname_167
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_167+' drop constraint '+@constraintname_167)
        FETCH NEXT from refcursor into @reftable_167, @constraintname_167
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_rwy_sertifikasi]
END

CREATE TABLE [vld_rwy_sertifikasi]
(
    [riwayat_sertifikasi_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_rwy_sertifikasi_PK] PRIMARY KEY ([riwayat_sertifikasi_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_rwy_sertifikasi] ([idtype]);

CREATE INDEX [VLD_RWY_SERT_LU] ON [vld_rwy_sertifikasi] ([last_update]);

CREATE INDEX [PK__vld_rwy___FD7ABA69D953A465] ON [vld_rwy_sertifikasi] ([riwayat_sertifikasi_id],[logid]);

BEGIN
ALTER TABLE [vld_rwy_sertifikasi] ADD CONSTRAINT [FK__vld_rwy_s__riway__3F865F66] FOREIGN KEY ([riwayat_sertifikasi_id]) REFERENCES [rwy_sertifikasi] ([riwayat_sertifikasi_id])
END
;

BEGIN
ALTER TABLE [vld_rwy_sertifikasi] ADD CONSTRAINT [FK__vld_rwy_s__riway__5AC46587] FOREIGN KEY ([riwayat_sertifikasi_id]) REFERENCES [rwy_sertifikasi] ([riwayat_sertifikasi_id])
END
;

BEGIN
ALTER TABLE [vld_rwy_sertifikasi] ADD CONSTRAINT [FK__vld_rwy_s__idtyp__5BB889C0] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_rwy_sertifikasi] ADD CONSTRAINT [FK__vld_rwy_s__idtyp__407A839F] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- rwy_fungsional
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_fungs__ptk_i__7B663F43')
    ALTER TABLE [rwy_fungsional] DROP CONSTRAINT [FK__rwy_fungs__ptk_i__7B663F43];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_fungs__ptk_i__60283922')
    ALTER TABLE [rwy_fungsional] DROP CONSTRAINT [FK__rwy_fungs__ptk_i__60283922];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_fungs__jabat__611C5D5B')
    ALTER TABLE [rwy_fungsional] DROP CONSTRAINT [FK__rwy_fungs__jabat__611C5D5B];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__rwy_fungs__jabat__7C5A637C')
    ALTER TABLE [rwy_fungsional] DROP CONSTRAINT [FK__rwy_fungs__jabat__7C5A637C];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'rwy_fungsional')
BEGIN
    DECLARE @reftable_168 nvarchar(60), @constraintname_168 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'rwy_fungsional'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_168, @constraintname_168
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_168+' drop constraint '+@constraintname_168)
        FETCH NEXT from refcursor into @reftable_168, @constraintname_168
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [rwy_fungsional]
END

CREATE TABLE [rwy_fungsional]
(
    [riwayat_fungsional_id] CHAR(16) NOT NULL,
    [ptk_id] CHAR(16) NOT NULL,
    [jabatan_fungsional_id] NUMERIC(4,0) NOT NULL,
    [sk_jabfung] VARCHAR(40) NOT NULL,
    [tmt_jabatan] VARCHAR(20) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [rwy_fungsional_PK] PRIMARY KEY ([riwayat_fungsional_id])
);

CREATE INDEX [JAB_FUNG_PTK_FK] ON [rwy_fungsional] ([ptk_id]);

CREATE INDEX [RWY_FUNG_LU] ON [rwy_fungsional] ([Last_update]);

CREATE INDEX [RWYT_FUNG_FK] ON [rwy_fungsional] ([jabatan_fungsional_id]);

CREATE INDEX [PK__rwy_fung__4B08E9216ED9C96D] ON [rwy_fungsional] ([riwayat_fungsional_id]);

BEGIN
ALTER TABLE [rwy_fungsional] ADD CONSTRAINT [FK__rwy_fungs__ptk_i__7B663F43] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [rwy_fungsional] ADD CONSTRAINT [FK__rwy_fungs__ptk_i__60283922] FOREIGN KEY ([ptk_id]) REFERENCES [ptk] ([ptk_id])
END
;

BEGIN
ALTER TABLE [rwy_fungsional] ADD CONSTRAINT [FK__rwy_fungs__jabat__611C5D5B] FOREIGN KEY ([jabatan_fungsional_id]) REFERENCES [ref].[jabatan_fungsional] ([jabatan_fungsional_id])
END
;

BEGIN
ALTER TABLE [rwy_fungsional] ADD CONSTRAINT [FK__rwy_fungs__jabat__7C5A637C] FOREIGN KEY ([jabatan_fungsional_id]) REFERENCES [ref].[jabatan_fungsional] ([jabatan_fungsional_id])
END
;

-----------------------------------------------------------------------
-- ref.status_keaktifan_pegawai
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.status_keaktifan_pegawai')
BEGIN
    DECLARE @reftable_169 nvarchar(60), @constraintname_169 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.status_keaktifan_pegawai'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_169, @constraintname_169
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_169+' drop constraint '+@constraintname_169)
        FETCH NEXT from refcursor into @reftable_169, @constraintname_169
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[status_keaktifan_pegawai]
END

CREATE TABLE [ref].[status_keaktifan_pegawai]
(
    [status_keaktifan_id] NUMERIC(4,0) NOT NULL,
    [nama] VARCHAR(30) NOT NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [status_keaktifan_pegawai_PK] PRIMARY KEY ([status_keaktifan_id])
);

CREATE INDEX [PK__status_k__68B932ADC2C36D15] ON [ref].[status_keaktifan_pegawai] ([status_keaktifan_id]);

-----------------------------------------------------------------------
-- ref.pekerjaan
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ref.pekerjaan')
BEGIN
    DECLARE @reftable_170 nvarchar(60), @constraintname_170 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'ref.pekerjaan'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_170, @constraintname_170
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_170+' drop constraint '+@constraintname_170)
        FETCH NEXT from refcursor into @reftable_170, @constraintname_170
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [ref].[pekerjaan]
END

CREATE TABLE [ref].[pekerjaan]
(
    [pekerjaan_id] INT NOT NULL,
    [nama] VARCHAR(25) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    CONSTRAINT [pekerjaan_PK] PRIMARY KEY ([pekerjaan_id])
);

CREATE INDEX [PK__pekerjaa__5605003324A0F5D6] ON [ref].[pekerjaan] ([pekerjaan_id]);

-----------------------------------------------------------------------
-- vld_rwy_pend_formal
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_rwy_p__riway__416EA7D8')
    ALTER TABLE [vld_rwy_pend_formal] DROP CONSTRAINT [FK__vld_rwy_p__riway__416EA7D8];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_rwy_p__riway__5CACADF9')
    ALTER TABLE [vld_rwy_pend_formal] DROP CONSTRAINT [FK__vld_rwy_p__riway__5CACADF9];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_rwy_p__idtyp__5DA0D232')
    ALTER TABLE [vld_rwy_pend_formal] DROP CONSTRAINT [FK__vld_rwy_p__idtyp__5DA0D232];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__vld_rwy_p__idtyp__4262CC11')
    ALTER TABLE [vld_rwy_pend_formal] DROP CONSTRAINT [FK__vld_rwy_p__idtyp__4262CC11];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'vld_rwy_pend_formal')
BEGIN
    DECLARE @reftable_171 nvarchar(60), @constraintname_171 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'vld_rwy_pend_formal'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_171, @constraintname_171
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_171+' drop constraint '+@constraintname_171)
        FETCH NEXT from refcursor into @reftable_171, @constraintname_171
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [vld_rwy_pend_formal]
END

CREATE TABLE [vld_rwy_pend_formal]
(
    [riwayat_pendidikan_formal_id] CHAR(16) NOT NULL,
    [logid] CHAR(16) NOT NULL,
    [idtype] INT NOT NULL,
    [status_validasi] NUMERIC(4,0) NULL,
    [status_verifikasi] NUMERIC(4,0) NULL,
    [field_error] VARCHAR(30) NULL,
    [error_message] VARCHAR(150) NULL,
    [keterangan] VARCHAR(255) NULL,
    [create_date] DATETIME(16,3) NOT NULL,
    [last_update] DATETIME(16,3) NOT NULL,
    [expired_date] DATETIME(16,3) NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [app_username] VARCHAR(50) NULL,
    CONSTRAINT [vld_rwy_pend_formal_PK] PRIMARY KEY ([riwayat_pendidikan_formal_id],[logid])
);

CREATE INDEX [ERR_TYPE_LOG_FK] ON [vld_rwy_pend_formal] ([idtype]);

CREATE INDEX [VLD_RWY_FORMAL_LU] ON [vld_rwy_pend_formal] ([last_update]);

CREATE INDEX [PK__vld_rwy___87788ECDDBF2B738] ON [vld_rwy_pend_formal] ([riwayat_pendidikan_formal_id],[logid]);

BEGIN
ALTER TABLE [vld_rwy_pend_formal] ADD CONSTRAINT [FK__vld_rwy_p__riway__416EA7D8] FOREIGN KEY ([riwayat_pendidikan_formal_id]) REFERENCES [rwy_pend_formal] ([riwayat_pendidikan_formal_id])
END
;

BEGIN
ALTER TABLE [vld_rwy_pend_formal] ADD CONSTRAINT [FK__vld_rwy_p__riway__5CACADF9] FOREIGN KEY ([riwayat_pendidikan_formal_id]) REFERENCES [rwy_pend_formal] ([riwayat_pendidikan_formal_id])
END
;

BEGIN
ALTER TABLE [vld_rwy_pend_formal] ADD CONSTRAINT [FK__vld_rwy_p__idtyp__5DA0D232] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

BEGIN
ALTER TABLE [vld_rwy_pend_formal] ADD CONSTRAINT [FK__vld_rwy_p__idtyp__4262CC11] FOREIGN KEY ([idtype]) REFERENCES [ref].[errortype] ([idtype])
END
;

-----------------------------------------------------------------------
-- buku_alat_longitudinal
-----------------------------------------------------------------------

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__buku_alat__buku___6304A5CD')
    ALTER TABLE [buku_alat_longitudinal] DROP CONSTRAINT [FK__buku_alat__buku___6304A5CD];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__buku_alat__buku___7E42ABEE')
    ALTER TABLE [buku_alat_longitudinal] DROP CONSTRAINT [FK__buku_alat__buku___7E42ABEE];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__buku_alat__semes__62108194')
    ALTER TABLE [buku_alat_longitudinal] DROP CONSTRAINT [FK__buku_alat__semes__62108194];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type ='RI' AND name='FK__buku_alat__semes__7D4E87B5')
    ALTER TABLE [buku_alat_longitudinal] DROP CONSTRAINT [FK__buku_alat__semes__7D4E87B5];

IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'buku_alat_longitudinal')
BEGIN
    DECLARE @reftable_172 nvarchar(60), @constraintname_172 nvarchar(60)
    DECLARE refcursor CURSOR FOR
    select reftables.name tablename, cons.name constraintname
        from sysobjects tables,
            sysobjects reftables,
            sysobjects cons,
            sysreferences ref
        where tables.id = ref.rkeyid
            and cons.id = ref.constid
            and reftables.id = ref.fkeyid
            and tables.name = 'buku_alat_longitudinal'
    OPEN refcursor
    FETCH NEXT from refcursor into @reftable_172, @constraintname_172
    while @@FETCH_STATUS = 0
    BEGIN
        exec ('alter table '+@reftable_172+' drop constraint '+@constraintname_172)
        FETCH NEXT from refcursor into @reftable_172, @constraintname_172
    END
    CLOSE refcursor
    DEALLOCATE refcursor
    DROP TABLE [buku_alat_longitudinal]
END

CREATE TABLE [buku_alat_longitudinal]
(
    [buku_alat_id] CHAR(16) NOT NULL,
    [semester_id] CHAR(5) NOT NULL,
    [jumlah] INT NOT NULL,
    [status_kelaikan] NUMERIC(3,0) NOT NULL,
    [Last_update] DATETIME(16,3) NOT NULL,
    [Soft_delete] NUMERIC(3,0) NOT NULL,
    [last_sync] DATETIME(16,3) NOT NULL,
    [Updater_ID] CHAR(16) NOT NULL,
    CONSTRAINT [buku_alat_longitudinal_PK] PRIMARY KEY ([buku_alat_id],[semester_id])
);

CREATE INDEX [BUKU_ALAT_LONG_LU] ON [buku_alat_longitudinal] ([Last_update]);

CREATE INDEX [BUKU_ALAT_SEM_FK] ON [buku_alat_longitudinal] ([semester_id]);

CREATE INDEX [PK__buku_ala__61CFF7D1950018E1] ON [buku_alat_longitudinal] ([buku_alat_id],[semester_id]);

BEGIN
ALTER TABLE [buku_alat_longitudinal] ADD CONSTRAINT [FK__buku_alat__buku___6304A5CD] FOREIGN KEY ([buku_alat_id]) REFERENCES [buku_alat] ([buku_alat_id])
END
;

BEGIN
ALTER TABLE [buku_alat_longitudinal] ADD CONSTRAINT [FK__buku_alat__buku___7E42ABEE] FOREIGN KEY ([buku_alat_id]) REFERENCES [buku_alat] ([buku_alat_id])
END
;

BEGIN
ALTER TABLE [buku_alat_longitudinal] ADD CONSTRAINT [FK__buku_alat__semes__62108194] FOREIGN KEY ([semester_id]) REFERENCES [ref].[semester] ([semester_id])
END
;

BEGIN
ALTER TABLE [buku_alat_longitudinal] ADD CONSTRAINT [FK__buku_alat__semes__7D4E87B5] FOREIGN KEY ([semester_id]) REFERENCES [ref].[semester] ([semester_id])
END
;
