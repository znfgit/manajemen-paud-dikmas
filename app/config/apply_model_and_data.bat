@echo off
copy build.compile.properties build.properties
copy build\sql\data.sql build\sql\data.old.sql
type head.txt > build\sql\data.sql
type build\sql\data.old.sql >>  build\sql\data.sql
type tail.txt >> build\sql\data.sql
vendor\bin\propel-gen ./ insert-sql
