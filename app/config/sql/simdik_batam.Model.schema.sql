
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

-- ---------------------------------------------------------------------
-- agama
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `agama`;

CREATE TABLE `agama`
(
    `agama_id` SMALLINT NOT NULL,
    `nama` VARCHAR(25) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`agama_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- akreditasi
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `akreditasi`;

CREATE TABLE `akreditasi`
(
    `akreditasi_id` DECIMAL(1,0) NOT NULL,
    `nama` VARCHAR(30) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`akreditasi_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- akreditasi_prodi
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `akreditasi_prodi`;

CREATE TABLE `akreditasi_prodi`
(
    `akred_prodi_id` CHAR(36) NOT NULL,
    `akreditasi_id` DECIMAL(1,0) NOT NULL,
    `la_id` CHAR(5) NOT NULL,
    `jurusan_sp_id` CHAR(36) NOT NULL,
    `no_sk_akred` VARCHAR(40) NOT NULL,
    `tgl_sk_akred` DATE NOT NULL,
    `tgl_akhir_akred` DATE NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`akred_prodi_id`),
    INDEX `akreditasi_id` (`akreditasi_id`),
    INDEX `jurusan_sp_id` (`jurusan_sp_id`),
    INDEX `la_id` (`la_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- akreditasi_sp
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `akreditasi_sp`;

CREATE TABLE `akreditasi_sp`
(
    `akred_sp_id` CHAR(36) NOT NULL,
    `sekolah_id` CHAR(36) NOT NULL,
    `akred_sp_sk` VARCHAR(40) NOT NULL,
    `akred_sp_tmt` DATE NOT NULL,
    `akred_sp_tst` DATE NOT NULL,
    `akreditasi_id` DECIMAL(1,0) NOT NULL,
    `la_id` CHAR(5) NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`akred_sp_id`),
    INDEX `akreditasi_id` (`akreditasi_id`),
    INDEX `la_id` (`la_id`),
    INDEX `sekolah_id` (`sekolah_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- akses_internet
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `akses_internet`;

CREATE TABLE `akses_internet`
(
    `akses_internet_id` SMALLINT NOT NULL,
    `nama` VARCHAR(50) NOT NULL,
    `media` DECIMAL(1,0) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`akses_internet_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- alat_transportasi
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `alat_transportasi`;

CREATE TABLE `alat_transportasi`
(
    `alat_transportasi_id` DECIMAL(2,0) NOT NULL,
    `nama` VARCHAR(40) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`alat_transportasi_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- anak
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `anak`;

CREATE TABLE `anak`
(
    `anak_id` CHAR(36) NOT NULL,
    `ptk_id` CHAR(36) NOT NULL,
    `status_anak_id` DECIMAL(1,0) NOT NULL,
    `jenjang_pendidikan_id` DECIMAL(2,0) NOT NULL,
    `nisn` CHAR(10),
    `nama` VARCHAR(50) NOT NULL,
    `jenis_kelamin` CHAR NOT NULL,
    `tempat_lahir` VARCHAR(20),
    `tanggal_lahir` DATE NOT NULL,
    `tahun_masuk` INTEGER,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`anak_id`),
    INDEX `jenjang_pendidikan_id` (`jenjang_pendidikan_id`),
    INDEX `ptk_id` (`ptk_id`),
    INDEX `status_anak_id` (`status_anak_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- anggota_gugus
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `anggota_gugus`;

CREATE TABLE `anggota_gugus`
(
    `gugus_id` CHAR(36) NOT NULL,
    `sekolah_id` CHAR(36) NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`gugus_id`,`sekolah_id`),
    INDEX `sekolah_id` (`sekolah_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- anggota_rombel
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `anggota_rombel`;

CREATE TABLE `anggota_rombel`
(
    `anggota_rombel_id` CHAR(36) NOT NULL,
    `rombongan_belajar_id` CHAR(36) NOT NULL,
    `peserta_didik_id` CHAR(36) NOT NULL,
    `jenis_pendaftaran_id` DECIMAL(1,0) NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`anggota_rombel_id`),
    INDEX `jenis_pendaftaran_id` (`jenis_pendaftaran_id`),
    INDEX `peserta_didik_id` (`peserta_didik_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- batas_waktu_rapor
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `batas_waktu_rapor`;

CREATE TABLE `batas_waktu_rapor`
(
    `semester_id` CHAR(5) NOT NULL,
    `tgl_rapor_mulai` DATE NOT NULL,
    `tgl_rapor_selesai` DATE NOT NULL,
    `tgl_usm_mulai` DATE,
    `tgl_usm_selesai` DATE,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`semester_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- beasiswa_peserta_didik
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `beasiswa_peserta_didik`;

CREATE TABLE `beasiswa_peserta_didik`
(
    `beasiswa_peserta_didik_id` CHAR(36) NOT NULL,
    `peserta_didik_id` CHAR(36) NOT NULL,
    `jenis_beasiswa_id` INTEGER NOT NULL,
    `keterangan` VARCHAR(80) NOT NULL,
    `tahun_mulai` DECIMAL(4,0) NOT NULL,
    `tahun_selesai` DECIMAL(4,0) NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`beasiswa_peserta_didik_id`),
    INDEX `jenis_beasiswa_id` (`jenis_beasiswa_id`),
    INDEX `peserta_didik_id` (`peserta_didik_id`),
    INDEX `tahun_selesai` (`tahun_selesai`),
    INDEX `tahun_mulai` (`tahun_mulai`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- beasiswa_ptk
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `beasiswa_ptk`;

CREATE TABLE `beasiswa_ptk`
(
    `beasiswa_ptk_id` CHAR(36) NOT NULL,
    `jenis_beasiswa_id` INTEGER NOT NULL,
    `ptk_id` CHAR(36) NOT NULL,
    `keterangan` VARCHAR(80) NOT NULL,
    `tahun_mulai` DECIMAL(4,0) NOT NULL,
    `tahun_akhir` DECIMAL(4,0),
    `masih_menerima` DECIMAL(1,0) NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`beasiswa_ptk_id`),
    INDEX `jenis_beasiswa_id` (`jenis_beasiswa_id`),
    INDEX `ptk_id` (`ptk_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- bentuk_pendidikan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `bentuk_pendidikan`;

CREATE TABLE `bentuk_pendidikan`
(
    `bentuk_pendidikan_id` SMALLINT NOT NULL,
    `nama` VARCHAR(50) NOT NULL,
    `jenjang_paud` DECIMAL(1,0) NOT NULL,
    `jenjang_tk` DECIMAL(1,0) NOT NULL,
    `jenjang_sd` DECIMAL(1,0) NOT NULL,
    `jenjang_smp` DECIMAL(1,0) NOT NULL,
    `jenjang_sma` DECIMAL(1,0) NOT NULL,
    `jenjang_tinggi` DECIMAL(1,0) NOT NULL,
    `direktorat_pembinaan` VARCHAR(40),
    `aktif` DECIMAL(1,0) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`bentuk_pendidikan_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- bidang_studi
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `bidang_studi`;

CREATE TABLE `bidang_studi`
(
    `bidang_studi_id` INTEGER NOT NULL,
    `kelompok_bidang_studi_id` INTEGER,
    `kode` CHAR(3),
    `bidang_studi` VARCHAR(40) NOT NULL,
    `kelompok` DECIMAL(1,0) NOT NULL,
    `jenjang_paud` DECIMAL(1,0) NOT NULL,
    `jenjang_tk` DECIMAL(1,0) NOT NULL,
    `jenjang_sd` DECIMAL(1,0) NOT NULL,
    `jenjang_smp` DECIMAL(1,0) NOT NULL,
    `jenjang_sma` DECIMAL(1,0) NOT NULL,
    `jenjang_tinggi` DECIMAL(1,0) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`bidang_studi_id`),
    INDEX `kelompok_bidang_studi_id` (`kelompok_bidang_studi_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- bidang_usaha
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `bidang_usaha`;

CREATE TABLE `bidang_usaha`
(
    `bidang_usaha_id` CHAR(10) NOT NULL,
    `nama_bidang_usaha` VARCHAR(40) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`bidang_usaha_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- blockgrant
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `blockgrant`;

CREATE TABLE `blockgrant`
(
    `blockgrant_id` CHAR(36) NOT NULL,
    `sekolah_id` CHAR(36) NOT NULL,
    `nama` VARCHAR(50) NOT NULL,
    `tahun` DECIMAL(4,0) NOT NULL,
    `jenis_bantuan_id` INTEGER NOT NULL,
    `sumber_dana_id` DECIMAL(3,0) NOT NULL,
    `besar_bantuan` DECIMAL(15,0) NOT NULL,
    `dana_pendamping` DECIMAL(15,0) NOT NULL,
    `peruntukan_dana` VARCHAR(50),
    `asal_data` CHAR NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`blockgrant_id`),
    INDEX `jenis_bantuan_id` (`jenis_bantuan_id`),
    INDEX `sekolah_id` (`sekolah_id`),
    INDEX `sumber_dana_id` (`sumber_dana_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- buku_alat
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `buku_alat`;

CREATE TABLE `buku_alat`
(
    `buku_alat_id` CHAR(36) NOT NULL,
    `mata_pelajaran_id` INTEGER NOT NULL,
    `prasarana_id` CHAR(36),
    `sekolah_id` CHAR(36) NOT NULL,
    `tingkat_pendidikan_id` DECIMAL(2,0),
    `jenis_buku_alat_id` DECIMAL(6,0) NOT NULL,
    `buku_alat` VARCHAR(60) NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`buku_alat_id`),
    INDEX `jenis_buku_alat_id` (`jenis_buku_alat_id`),
    INDEX `mata_pelajaran_id` (`mata_pelajaran_id`),
    INDEX `prasarana_id` (`prasarana_id`),
    INDEX `sekolah_id` (`sekolah_id`),
    INDEX `tingkat_pendidikan_id` (`tingkat_pendidikan_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- buku_alat_longitudinal
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `buku_alat_longitudinal`;

CREATE TABLE `buku_alat_longitudinal`
(
    `buku_alat_id` CHAR(36) NOT NULL,
    `semester_id` CHAR(5) NOT NULL,
    `jumlah` INTEGER NOT NULL,
    `status_kelaikan` DECIMAL(1,0) NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`buku_alat_id`,`semester_id`),
    INDEX `semester_id` (`semester_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- buku_ptk
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `buku_ptk`;

CREATE TABLE `buku_ptk`
(
    `buku_id` CHAR(36) NOT NULL,
    `ptk_id` CHAR(36) NOT NULL,
    `judul_buku` VARCHAR(50) NOT NULL,
    `tahun` DECIMAL(4,0) NOT NULL,
    `penerbit` VARCHAR(30),
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`buku_id`),
    INDEX `ptk_id` (`ptk_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- demografi
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `demografi`;

CREATE TABLE `demografi`
(
    `demografi_id` CHAR(36) NOT NULL,
    `kode_wilayah` CHAR(8) NOT NULL,
    `tahun_ajaran_id` DECIMAL(4,0) NOT NULL,
    `usia_5` BIGINT NOT NULL,
    `usia_7` BIGINT NOT NULL,
    `usia_13` BIGINT NOT NULL,
    `usia_16` BIGINT NOT NULL,
    `usia_18` BIGINT NOT NULL,
    `jumlah_penduduk` BIGINT NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`demografi_id`),
    INDEX `kode_wilayah` (`kode_wilayah`),
    INDEX `tahun_ajaran_id` (`tahun_ajaran_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- diklat
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `diklat`;

CREATE TABLE `diklat`
(
    `diklat_id` CHAR(36) NOT NULL,
    `jenis_diklat_id` INTEGER NOT NULL,
    `ptk_id` CHAR(36) NOT NULL,
    `nama` VARCHAR(50) NOT NULL,
    `penyelenggara` VARCHAR(80),
    `tahun` DECIMAL(4,0) NOT NULL,
    `peran` VARCHAR(30),
    `tingkat` DECIMAL(2,0),
    `berapa_jam` DECIMAL(4,0) NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`diklat_id`),
    INDEX `jenis_diklat_id` (`jenis_diklat_id`),
    INDEX `ptk_id` (`ptk_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- dudi
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `dudi`;

CREATE TABLE `dudi`
(
    `dudi_id` CHAR(36) NOT NULL,
    `bidang_usaha_id` CHAR(10) NOT NULL,
    `nama_dudi` VARCHAR(80) NOT NULL,
    `npwp` CHAR(15),
    `telp_kantor` VARCHAR(20),
    `fax` VARCHAR(20),
    `contact_person` VARCHAR(50),
    `telp_cp` VARCHAR(20),
    `jabatan_cp` VARCHAR(40),
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`dudi_id`),
    INDEX `bidang_usaha_id` (`bidang_usaha_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- errortype
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `errortype`;

CREATE TABLE `errortype`
(
    `idtype` INTEGER NOT NULL,
    `kategori_error` INTEGER,
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- gelar_akademik
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `gelar_akademik`;

CREATE TABLE `gelar_akademik`
(
    `gelar_akademik_id` INTEGER NOT NULL,
    `kode` VARCHAR(10) NOT NULL,
    `nama` VARCHAR(40) NOT NULL,
    `posisi_gelar` DECIMAL(1,0) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`gelar_akademik_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- group_matpel
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `group_matpel`;

CREATE TABLE `group_matpel`
(
    `gmp_id` CHAR(36) NOT NULL,
    `kurikulum_id` SMALLINT NOT NULL,
    `tingkat_pendidikan_id` DECIMAL(2,0) NOT NULL,
    `nama_group` VARCHAR(40) NOT NULL,
    `jumlah_jam_maksimum` DECIMAL(2,0) NOT NULL,
    `jumlah_sks_maksimum` DECIMAL(2,0) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`gmp_id`),
    INDEX `kurikulum_id` (`kurikulum_id`),
    INDEX `tingkat_pendidikan_id` (`tingkat_pendidikan_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- gugus_sekolah
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `gugus_sekolah`;

CREATE TABLE `gugus_sekolah`
(
    `gugus_id` CHAR(36) NOT NULL,
    `sekolah_id` CHAR(36),
    `jenis_gugus_id` DECIMAL(3,0) NOT NULL,
    `nama` VARCHAR(50) NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`gugus_id`),
    INDEX `jenis_gugus_id` (`jenis_gugus_id`),
    INDEX `sekolah_id` (`sekolah_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- inpassing
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `inpassing`;

CREATE TABLE `inpassing`
(
    `inpassing_id` CHAR(36) NOT NULL,
    `pangkat_golongan_id` DECIMAL(2,0) NOT NULL,
    `ptk_id` CHAR(36),
    `no_sk_inpassing` VARCHAR(40) NOT NULL,
    `tmt_inpassing` DATE NOT NULL,
    `angka_kredit` DECIMAL(3,0) NOT NULL,
    `masa_kerja_tahun` DECIMAL(2,0) NOT NULL,
    `masa_kerja_bulan` DECIMAL(2,0) NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`inpassing_id`),
    INDEX `pangkat_golongan_id` (`pangkat_golongan_id`),
    INDEX `ptk_id` (`ptk_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jabatan_fungsional
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jabatan_fungsional`;

CREATE TABLE `jabatan_fungsional`
(
    `jabatan_fungsional_id` DECIMAL(2,0) NOT NULL,
    `nama` VARCHAR(30) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`jabatan_fungsional_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jabatan_tugas_ptk
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jabatan_tugas_ptk`;

CREATE TABLE `jabatan_tugas_ptk`
(
    `jabatan_ptk_id` DECIMAL(3,0) NOT NULL,
    `nama` VARCHAR(40) NOT NULL,
    `jabatan_utama` DECIMAL(1,0) NOT NULL,
    `tugas_tambahan_guru` DECIMAL(1,0) NOT NULL,
    `jumlah_jam_diakui` DECIMAL(2,0),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`jabatan_ptk_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jenis_bantuan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_bantuan`;

CREATE TABLE `jenis_bantuan`
(
    `jenis_bantuan_id` INTEGER NOT NULL,
    `nama` VARCHAR(50),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`jenis_bantuan_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jenis_beasiswa
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_beasiswa`;

CREATE TABLE `jenis_beasiswa`
(
    `jenis_beasiswa_id` INTEGER NOT NULL,
    `sumber_dana_id` DECIMAL(3,0) NOT NULL,
    `nama` VARCHAR(50) NOT NULL,
    `untuk_pd` DECIMAL(1,0) NOT NULL,
    `untuk_ptk` DECIMAL(1,0) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`jenis_beasiswa_id`),
    INDEX `sumber_dana_id` (`sumber_dana_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jenis_buku_alat
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_buku_alat`;

CREATE TABLE `jenis_buku_alat`
(
    `jenis_buku_alat_id` DECIMAL(6,0) NOT NULL,
    `jenis_buku_alat` VARCHAR(60) NOT NULL,
    `spm_qty_min_per_siswa` DECIMAL(3,1) NOT NULL,
    `spm_qty_min_per_sekolah` DECIMAL(4,0) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`jenis_buku_alat_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jenis_diklat
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_diklat`;

CREATE TABLE `jenis_diklat`
(
    `jenis_diklat_id` INTEGER NOT NULL,
    `nama` VARCHAR(50) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`jenis_diklat_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jenis_gugus
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_gugus`;

CREATE TABLE `jenis_gugus`
(
    `jenis_gugus_id` DECIMAL(3,0) NOT NULL,
    `jenis_gugus` VARCHAR(30) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`jenis_gugus_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jenis_keluar
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_keluar`;

CREATE TABLE `jenis_keluar`
(
    `jenis_keluar_id` CHAR NOT NULL,
    `ket_keluar` VARCHAR(40) NOT NULL,
    `keluar_pd` DECIMAL(1,0) NOT NULL,
    `keluar_ptk` DECIMAL(1,0) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`jenis_keluar_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jenis_kesejahteraan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_kesejahteraan`;

CREATE TABLE `jenis_kesejahteraan`
(
    `jenis_kesejahteraan_id` INTEGER NOT NULL,
    `nama` VARCHAR(30) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`jenis_kesejahteraan_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jenis_lembaga
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_lembaga`;

CREATE TABLE `jenis_lembaga`
(
    `jenis_lembaga_id` DECIMAL(5,0) NOT NULL,
    `nama` VARCHAR(80) NOT NULL,
    `tempat_pengawas` DECIMAL(1,0) NOT NULL,
    `simpul_pendataan` DECIMAL(1,0) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`jenis_lembaga_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jenis_pendaftaran
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_pendaftaran`;

CREATE TABLE `jenis_pendaftaran`
(
    `jenis_pendaftaran_id` DECIMAL(1,0) NOT NULL,
    `nama` VARCHAR(20) NOT NULL,
    `daftar_sekolah` DECIMAL(1,0) NOT NULL,
    `daftar_rombel` DECIMAL(1,0) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`jenis_pendaftaran_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jenis_penghargaan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_penghargaan`;

CREATE TABLE `jenis_penghargaan`
(
    `jenis_penghargaan_id` INTEGER NOT NULL,
    `nama` VARCHAR(50) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`jenis_penghargaan_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jenis_prasarana
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_prasarana`;

CREATE TABLE `jenis_prasarana`
(
    `jenis_prasarana_id` INTEGER NOT NULL,
    `nama` VARCHAR(60) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`jenis_prasarana_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jenis_prestasi
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_prestasi`;

CREATE TABLE `jenis_prestasi`
(
    `jenis_prestasi_id` INTEGER NOT NULL,
    `nama` VARCHAR(50) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`jenis_prestasi_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jenis_ptk
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_ptk`;

CREATE TABLE `jenis_ptk`
(
    `jenis_ptk_id` DECIMAL(2,0) NOT NULL,
    `jenis_ptk` VARCHAR(30) NOT NULL,
    `guru_kelas` DECIMAL(1,0) NOT NULL,
    `guru_matpel` DECIMAL(1,0) NOT NULL,
    `guru_bk` DECIMAL(1,0) NOT NULL,
    `guru_inklusi` DECIMAL(1,0) NOT NULL,
    `pengawas_satdik` DECIMAL(1,0) NOT NULL,
    `pengawas_plb` DECIMAL(1,0) NOT NULL,
    `pengawas_matpel` DECIMAL(1,0) NOT NULL,
    `pengawas_bidang` DECIMAL(1,0) NOT NULL,
    `tas` DECIMAL(1,0) NOT NULL,
    `formal` DECIMAL(1,0) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`jenis_ptk_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jenis_sarana
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_sarana`;

CREATE TABLE `jenis_sarana`
(
    `jenis_sarana_id` INTEGER NOT NULL,
    `nama` VARCHAR(30) NOT NULL,
    `kelompok` VARCHAR(50),
    `perlu_penempatan` DECIMAL(1,0) NOT NULL,
    `keterangan` VARCHAR(128),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`jenis_sarana_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jenis_sertifikasi
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_sertifikasi`;

CREATE TABLE `jenis_sertifikasi`
(
    `id_jenis_sertifikasi` DECIMAL(3,0) NOT NULL,
    `jenis_sertifikasi` VARCHAR(30) NOT NULL,
    `prof_guru` DECIMAL(1,0) NOT NULL,
    `kepala_sekolah` DECIMAL(1,0) NOT NULL,
    `laboran` DECIMAL(1,0) NOT NULL,
    `kebutuhan_khusus_id` INTEGER NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`id_jenis_sertifikasi`),
    INDEX `kebutuhan_khusus_id` (`kebutuhan_khusus_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jenis_test
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_test`;

CREATE TABLE `jenis_test`
(
    `jenis_test_id` DECIMAL(3,0) NOT NULL,
    `jenis_test` VARCHAR(30) NOT NULL,
    `keterangan` VARCHAR(80),
    `nilai_maks` DECIMAL(6,2) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`jenis_test_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jenis_tinggal
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_tinggal`;

CREATE TABLE `jenis_tinggal`
(
    `jenis_tinggal_id` DECIMAL(2,0) NOT NULL,
    `nama` VARCHAR(30) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`jenis_tinggal_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jenis_tunjangan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jenis_tunjangan`;

CREATE TABLE `jenis_tunjangan`
(
    `jenis_tunjangan_id` INTEGER NOT NULL,
    `nama` VARCHAR(50),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`jenis_tunjangan_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jenjang_kepengawasan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jenjang_kepengawasan`;

CREATE TABLE `jenjang_kepengawasan`
(
    `jenjang_kepengawasan_id` DECIMAL(2,0) NOT NULL,
    `nama` VARCHAR(50) NOT NULL,
    `jenjang_kepengawasan_tk` DECIMAL(1,0) NOT NULL,
    `jenjang_kepengawasan_sd` DECIMAL(1,0) NOT NULL,
    `jenjang_kepengawasan_smp` DECIMAL(1,0) NOT NULL,
    `jenjang_kepengawasan_sma` DECIMAL(1,0) NOT NULL,
    `jenjang_kepengawasan_smk` DECIMAL(1,0) NOT NULL,
    `jenjang_kepengawasan_slb` DECIMAL(1,0) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`jenjang_kepengawasan_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jenjang_pendidikan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jenjang_pendidikan`;

CREATE TABLE `jenjang_pendidikan`
(
    `jenjang_pendidikan_id` DECIMAL(2,0) NOT NULL,
    `nama` VARCHAR(25) NOT NULL,
    `jenjang_lembaga` DECIMAL(1,0) NOT NULL,
    `jenjang_orang` DECIMAL(1,0) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`jenjang_pendidikan_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jurusan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jurusan`;

CREATE TABLE `jurusan`
(
    `jurusan_id` VARCHAR(25) NOT NULL,
    `nama_jurusan` VARCHAR(60) NOT NULL,
    `untuk_sma` DECIMAL(1,0) NOT NULL,
    `untuk_smk` DECIMAL(1,0) NOT NULL,
    `untuk_pt` DECIMAL(1,0) NOT NULL,
    `untuk_slb` DECIMAL(1,0) NOT NULL,
    `untuk_smklb` DECIMAL(1,0) NOT NULL,
    `jurusan_induk` VARCHAR(25),
    `level_bidang_id` VARCHAR(5) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`jurusan_id`),
    INDEX `jurusan_induk` (`jurusan_induk`),
    INDEX `level_bidang_id` (`level_bidang_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jurusan_kerjasama
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jurusan_kerjasama`;

CREATE TABLE `jurusan_kerjasama`
(
    `mou_id` CHAR(36) NOT NULL,
    `jurusan_sp_id` CHAR(36) NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`mou_id`,`jurusan_sp_id`),
    INDEX `jurusan_sp_id` (`jurusan_sp_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- jurusan_sp
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `jurusan_sp`;

CREATE TABLE `jurusan_sp`
(
    `jurusan_sp_id` CHAR(36) NOT NULL,
    `sekolah_id` CHAR(36) NOT NULL,
    `kebutuhan_khusus_id` INTEGER NOT NULL,
    `jurusan_id` VARCHAR(25) NOT NULL,
    `nama_jurusan_sp` VARCHAR(60) NOT NULL,
    `sk_izin` VARCHAR(40),
    `tanggal_sk_izin` DATE,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`jurusan_sp_id`),
    INDEX `jurusan_id` (`jurusan_id`),
    INDEX `kebutuhan_khusus_id` (`kebutuhan_khusus_id`),
    INDEX `sekolah_id` (`sekolah_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- karya_tulis
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `karya_tulis`;

CREATE TABLE `karya_tulis`
(
    `karya_tulis_id` CHAR(36) NOT NULL,
    `ptk_id` CHAR(36) NOT NULL,
    `judul` VARCHAR(50) NOT NULL,
    `tahun_pembuatan` DECIMAL(4,0) NOT NULL,
    `publikasi` VARCHAR(30),
    `keterangan` VARCHAR(160),
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`karya_tulis_id`),
    INDEX `ptk_id` (`ptk_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- keahlian_laboratorium
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `keahlian_laboratorium`;

CREATE TABLE `keahlian_laboratorium`
(
    `keahlian_laboratorium_id` SMALLINT NOT NULL,
    `nama` VARCHAR(50) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`keahlian_laboratorium_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- kebutuhan_khusus
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `kebutuhan_khusus`;

CREATE TABLE `kebutuhan_khusus`
(
    `kebutuhan_khusus_id` INTEGER NOT NULL,
    `kebutuhan_khusus` VARCHAR(40) NOT NULL,
    `kk_a` DECIMAL(1,0) NOT NULL,
    `kk_b` DECIMAL(1,0) NOT NULL,
    `kk_c` DECIMAL(1,0) NOT NULL,
    `kk_c1` DECIMAL(1,0) NOT NULL,
    `kk_d` DECIMAL(1,0) NOT NULL,
    `kk_d1` DECIMAL(1,0) NOT NULL,
    `kk_e` DECIMAL(1,0) NOT NULL,
    `kk_f` DECIMAL(1,0) NOT NULL,
    `kk_h` DECIMAL(1,0) NOT NULL,
    `kk_i` DECIMAL(1,0) NOT NULL,
    `kk_j` DECIMAL(1,0) NOT NULL,
    `kk_k` DECIMAL(1,0) NOT NULL,
    `kk_n` DECIMAL(1,0) NOT NULL,
    `kk_o` DECIMAL(1,0) NOT NULL,
    `kk_p` DECIMAL(1,0) NOT NULL,
    `kk_q` DECIMAL(1,0) NOT NULL,
    `untuk_lembaga` DECIMAL(1,0) NOT NULL,
    `untuk_ptk` DECIMAL(1,0) NOT NULL,
    `untuk_pd` DECIMAL(1,0) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`kebutuhan_khusus_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- kelompok_bidang
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `kelompok_bidang`;

CREATE TABLE `kelompok_bidang`
(
    `level_bidang_id` VARCHAR(5) NOT NULL,
    `nama_level_bidang` VARCHAR(25) NOT NULL,
    `untuk_sma` DECIMAL(1,0) NOT NULL,
    `untuk_smk` DECIMAL(1,0) NOT NULL,
    `untuk_pt` DECIMAL(1,0) NOT NULL,
    `untuk_slb` DECIMAL(1,0) NOT NULL,
    `untuk_smklb` DECIMAL(1,0) NOT NULL,
    `level_bidang_induk` VARCHAR(5),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`level_bidang_id`),
    INDEX `level_bidang_induk` (`level_bidang_induk`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- kelompok_usaha
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `kelompok_usaha`;

CREATE TABLE `kelompok_usaha`
(
    `kelompok_usaha_id` CHAR(8) NOT NULL,
    `nama_kelompok_usaha` VARCHAR(60) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`kelompok_usaha_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- kesejahteraan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `kesejahteraan`;

CREATE TABLE `kesejahteraan`
(
    `kesejahteraan_id` CHAR(36) NOT NULL,
    `ptk_id` CHAR(36) NOT NULL,
    `jenis_kesejahteraan_id` INTEGER NOT NULL,
    `nama` VARCHAR(25) NOT NULL,
    `penyelenggara` VARCHAR(80) NOT NULL,
    `dari_tahun` DECIMAL(4,0) NOT NULL,
    `sampai_tahun` DECIMAL(4,0),
    `status` INTEGER,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`kesejahteraan_id`),
    INDEX `jenis_kesejahteraan_id` (`jenis_kesejahteraan_id`),
    INDEX `ptk_id` (`ptk_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- kurikulum
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `kurikulum`;

CREATE TABLE `kurikulum`
(
    `kurikulum_id` SMALLINT NOT NULL,
    `jurusan_id` VARCHAR(25),
    `jenjang_pendidikan_id` DECIMAL(2,0) NOT NULL,
    `nama_kurikulum` VARCHAR(30) NOT NULL,
    `mulai_berlaku` DATE NOT NULL,
    `sistem_sks` DECIMAL(1,0) NOT NULL,
    `total_sks` DECIMAL(3,0) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`kurikulum_id`),
    INDEX `jenjang_pendidikan_id` (`jenjang_pendidikan_id`),
    INDEX `jurusan_id` (`jurusan_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- lembaga_akreditasi
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `lembaga_akreditasi`;

CREATE TABLE `lembaga_akreditasi`
(
    `la_id` CHAR(5) NOT NULL,
    `nama` VARCHAR(80) NOT NULL,
    `la_tgl_mulai` DATE NOT NULL,
    `la_ket` VARCHAR(250),
    `alamat_jalan` VARCHAR(80) NOT NULL,
    `rt` DECIMAL(2,0),
    `rw` DECIMAL(2,0),
    `nama_dusun` VARCHAR(40),
    `desa_kelurahan` VARCHAR(40) NOT NULL,
    `kode_wilayah` CHAR(8) NOT NULL,
    `kode_pos` CHAR(5),
    `lintang` DECIMAL,
    `bujur` DECIMAL,
    `nomor_telepon` VARCHAR(20),
    `nomor_fax` VARCHAR(20),
    `email` VARCHAR(50),
    `website` VARCHAR(100),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`la_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- lembaga_non_sekolah
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `lembaga_non_sekolah`;

CREATE TABLE `lembaga_non_sekolah`
(
    `lembaga_id` CHAR(36) NOT NULL,
    `nama` VARCHAR(80) NOT NULL,
    `singkatan` VARCHAR(15),
    `jenis_lembaga_id` DECIMAL(5,0) NOT NULL,
    `alamat_jalan` VARCHAR(80) NOT NULL,
    `rt` DECIMAL(2,0),
    `rw` DECIMAL(2,0),
    `nama_dusun` VARCHAR(40),
    `desa_kelurahan` VARCHAR(40) NOT NULL,
    `kode_wilayah` CHAR(8) NOT NULL,
    `kode_pos` CHAR(5),
    `lintang` DECIMAL,
    `bujur` DECIMAL,
    `nomor_telepon` VARCHAR(20),
    `nomor_fax` VARCHAR(20),
    `email` VARCHAR(50),
    `website` VARCHAR(100),
    `pengguna_id` CHAR(36),
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`lembaga_id`),
    INDEX `jenis_lembaga_id` (`jenis_lembaga_id`),
    INDEX `kode_wilayah` (`kode_wilayah`),
    INDEX `pengguna_id` (`pengguna_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- lembaga_pengangkat
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `lembaga_pengangkat`;

CREATE TABLE `lembaga_pengangkat`
(
    `lembaga_pengangkat_id` DECIMAL(2,0) NOT NULL,
    `nama` VARCHAR(80) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`lembaga_pengangkat_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- level_wilayah
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `level_wilayah`;

CREATE TABLE `level_wilayah`
(
    `id_level_wilayah` SMALLINT NOT NULL,
    `level_wilayah` VARCHAR(15),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`id_level_wilayah`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- log_pengguna
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `log_pengguna`;

CREATE TABLE `log_pengguna`
(
    `waktu_login` DATETIME NOT NULL,
    `pengguna_id` CHAR(36) NOT NULL,
    `alamat_ip` VARCHAR(50) NOT NULL,
    `keterangan` VARCHAR(128),
    `waktu_logout` DATETIME,
    PRIMARY KEY (`waktu_login`),
    INDEX `pengguna_id` (`pengguna_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- map_bidang_mata_pelajaran
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `map_bidang_mata_pelajaran`;

CREATE TABLE `map_bidang_mata_pelajaran`
(
    `mata_pelajaran_id` INTEGER NOT NULL,
    `bidang_studi_id` INTEGER NOT NULL,
    `tahun_sertifikasi` DECIMAL(4,0) NOT NULL,
    `sesuai` DECIMAL(1,0) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`mata_pelajaran_id`,`bidang_studi_id`,`tahun_sertifikasi`),
    INDEX `bidang_studi_id` (`bidang_studi_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- mata_pelajaran
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `mata_pelajaran`;

CREATE TABLE `mata_pelajaran`
(
    `mata_pelajaran_id` INTEGER NOT NULL,
    `nama` VARCHAR(50) NOT NULL,
    `pilihan_sekolah` DECIMAL(1,0) NOT NULL,
    `pilihan_buku` DECIMAL(1,0) NOT NULL,
    `pilihan_kepengawasan` DECIMAL(1,0) NOT NULL,
    `pilihan_evaluasi` DECIMAL(1,0) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`mata_pelajaran_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- mata_pelajaran_kurikulum
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `mata_pelajaran_kurikulum`;

CREATE TABLE `mata_pelajaran_kurikulum`
(
    `kurikulum_id` SMALLINT NOT NULL,
    `mata_pelajaran_id` INTEGER NOT NULL,
    `tingkat_pendidikan_id` DECIMAL(2,0) NOT NULL,
    `gmp_id` CHAR(36),
    `jumlah_jam` DECIMAL(2,0) NOT NULL,
    `jumlah_jam_maksimum` DECIMAL(2,0) NOT NULL,
    `wajib` DECIMAL(1,0) NOT NULL,
    `sks` DECIMAL(2,0) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`kurikulum_id`,`mata_pelajaran_id`,`tingkat_pendidikan_id`),
    INDEX `gmp_id` (`gmp_id`),
    INDEX `mata_pelajaran_id` (`mata_pelajaran_id`),
    INDEX `tingkat_pendidikan_id` (`tingkat_pendidikan_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- mou
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `mou`;

CREATE TABLE `mou`
(
    `mou_id` CHAR(36) NOT NULL,
    `dudi_id` CHAR(36),
    `sekolah_id` CHAR(36) NOT NULL,
    `nomor_mou` VARCHAR(40) NOT NULL,
    `judul_mou` VARCHAR(80) NOT NULL,
    `tanggal_mulai` DATE NOT NULL,
    `tanggal_selesai` DATE NOT NULL,
    `nama_dudi` VARCHAR(80) NOT NULL,
    `npwp_dudi` CHAR(15),
    `nama_bidang_usaha` VARCHAR(40) NOT NULL,
    `telp_kantor` VARCHAR(20),
    `fax` VARCHAR(20),
    `contact_person` VARCHAR(50),
    `telp_cp` VARCHAR(20),
    `jabatan_cp` VARCHAR(40),
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`mou_id`),
    INDEX `dudi_id` (`dudi_id`),
    INDEX `sekolah_id` (`sekolah_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- mst_wilayah
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `mst_wilayah`;

CREATE TABLE `mst_wilayah`
(
    `kode_wilayah` CHAR(8) NOT NULL,
    `nama` VARCHAR(40) NOT NULL,
    `id_level_wilayah` SMALLINT NOT NULL,
    `mst_kode_wilayah` CHAR(8),
    `negara_id` CHAR(2) NOT NULL,
    `asal_wilayah` CHAR(8),
    `kode_bps` CHAR(7),
    `kode_dagri` CHAR(7),
    `kode_keu` VARCHAR(10),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`kode_wilayah`),
    INDEX `id_level_wilayah` (`id_level_wilayah`),
    INDEX `mst_kode_wilayah` (`mst_kode_wilayah`),
    INDEX `negara_id` (`negara_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- negara
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `negara`;

CREATE TABLE `negara`
(
    `negara_id` CHAR(2) NOT NULL,
    `nama` VARCHAR(45) NOT NULL,
    `luar_negeri` DECIMAL(1,0) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`negara_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- nilai_test
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `nilai_test`;

CREATE TABLE `nilai_test`
(
    `nilai_test_id` CHAR(36) NOT NULL,
    `jenis_test_id` DECIMAL(3,0) NOT NULL,
    `ptk_id` CHAR(36) NOT NULL,
    `nama` VARCHAR(50) NOT NULL,
    `penyelenggara` VARCHAR(80) NOT NULL,
    `tahun` DECIMAL(4,0) NOT NULL,
    `skor` DECIMAL(6,2) NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`nilai_test_id`),
    INDEX `jenis_test_id` (`jenis_test_id`),
    INDEX `ptk_id` (`ptk_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- pangkat_golongan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `pangkat_golongan`;

CREATE TABLE `pangkat_golongan`
(
    `pangkat_golongan_id` DECIMAL(2,0) NOT NULL,
    `kode` VARCHAR(5) NOT NULL,
    `nama` VARCHAR(20) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`pangkat_golongan_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- pekerjaan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `pekerjaan`;

CREATE TABLE `pekerjaan`
(
    `pekerjaan_id` INTEGER NOT NULL,
    `nama` VARCHAR(25),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`pekerjaan_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- pembelajaran
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `pembelajaran`;

CREATE TABLE `pembelajaran`
(
    `pembelajaran_id` CHAR(36) NOT NULL,
    `rombongan_belajar_id` CHAR(36) NOT NULL,
    `semester_id` CHAR(5) NOT NULL,
    `mata_pelajaran_id` INTEGER NOT NULL,
    `ptk_terdaftar_id` CHAR(36) NOT NULL,
    `sk_mengajar` VARCHAR(40) NOT NULL,
    `tanggal_sk_mengajar` DATE NOT NULL,
    `jam_mengajar_per_minggu` DECIMAL(2,0) NOT NULL,
    `status_di_kurikulum` DECIMAL(1,0) NOT NULL,
    `nama_mata_pelajaran` VARCHAR(50) NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`pembelajaran_id`),
    INDEX `mata_pelajaran_id` (`mata_pelajaran_id`),
    INDEX `ptk_terdaftar_id` (`ptk_terdaftar_id`),
    INDEX `rombongan_belajar_id` (`rombongan_belajar_id`),
    INDEX `semester_id` (`semester_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- pengawas_terdaftar
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `pengawas_terdaftar`;

CREATE TABLE `pengawas_terdaftar`
(
    `pengawas_terdaftar_id` CHAR(36) NOT NULL,
    `ptk_id` CHAR(36) NOT NULL,
    `lembaga_id` CHAR(36) NOT NULL,
    `tahun_ajaran_id` DECIMAL(4,0) NOT NULL,
    `nomor_surat_tugas` VARCHAR(40) NOT NULL,
    `tanggal_surat_tugas` DATE NOT NULL,
    `tmt_tugas` DATE NOT NULL,
    `mata_pelajaran_id` INTEGER,
    `bidang_studi_id` INTEGER,
    `jenjang_kepengawasan_id` DECIMAL(2,0) NOT NULL,
    `aktif_bulan_01` DECIMAL(1,0) NOT NULL,
    `aktif_bulan_02` DECIMAL(1,0) NOT NULL,
    `aktif_bulan_03` DECIMAL(1,0) NOT NULL,
    `aktif_bulan_04` DECIMAL(1,0) NOT NULL,
    `aktif_bulan_05` DECIMAL(1,0) NOT NULL,
    `aktif_bulan_06` DECIMAL(1,0) NOT NULL,
    `aktif_bulan_07` DECIMAL(1,0) NOT NULL,
    `aktif_bulan_08` DECIMAL(1,0) NOT NULL,
    `aktif_bulan_09` DECIMAL(1,0) NOT NULL,
    `aktif_bulan_10` DECIMAL(1,0) NOT NULL,
    `aktif_bulan_11` DECIMAL(1,0) NOT NULL,
    `aktif_bulan_12` DECIMAL(1,0) NOT NULL,
    `jenis_keluar_id` CHAR,
    `tgl_pengawas_keluar` DATE,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`pengawas_terdaftar_id`),
    INDEX `bidang_studi_id` (`bidang_studi_id`),
    INDEX `jenis_keluar_id` (`jenis_keluar_id`),
    INDEX `jenjang_kepengawasan_id` (`jenjang_kepengawasan_id`),
    INDEX `lembaga_id` (`lembaga_id`),
    INDEX `mata_pelajaran_id` (`mata_pelajaran_id`),
    INDEX `ptk_id` (`ptk_id`),
    INDEX `tahun_ajaran_id` (`tahun_ajaran_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- pengguna
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `pengguna`;

CREATE TABLE `pengguna`
(
    `pengguna_id` CHAR(36) NOT NULL,
    `sekolah_id` CHAR(36),
    `lembaga_id` CHAR(36),
    `yayasan_id` CHAR(36),
    `la_id` CHAR(5),
    `peran_id` INTEGER NOT NULL,
    `username` VARCHAR(50) NOT NULL,
    `password` VARCHAR(50) NOT NULL,
    `nama` VARCHAR(50) NOT NULL,
    `nip_nim` VARCHAR(9),
    `jabatan_lembaga` VARCHAR(25),
    `ym` VARCHAR(20),
    `skype` VARCHAR(20),
    `alamat` VARCHAR(80),
    `kode_wilayah` CHAR(8) NOT NULL,
    `no_telepon` VARCHAR(20),
    `no_hp` VARCHAR(20),
    `aktif` DECIMAL(1,0) NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`pengguna_id`),
    INDEX `kode_wilayah` (`kode_wilayah`),
    INDEX `lembaga_id` (`lembaga_id`),
    INDEX `peran_id` (`peran_id`),
    INDEX `sekolah_id` (`sekolah_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- penghargaan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `penghargaan`;

CREATE TABLE `penghargaan`
(
    `penghargaan_id` CHAR(36) NOT NULL,
    `tingkat_penghargaan_id` INTEGER NOT NULL,
    `ptk_id` CHAR(36) NOT NULL,
    `jenis_penghargaan_id` INTEGER NOT NULL,
    `nama` VARCHAR(50) NOT NULL,
    `tahun` DECIMAL(4,0) NOT NULL,
    `instansi` VARCHAR(80),
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`penghargaan_id`),
    INDEX `jenis_penghargaan_id` (`jenis_penghargaan_id`),
    INDEX `ptk_id` (`ptk_id`),
    INDEX `tingkat_penghargaan_id` (`tingkat_penghargaan_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- penghasilan_orangtua_wali
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `penghasilan_orangtua_wali`;

CREATE TABLE `penghasilan_orangtua_wali`
(
    `penghasilan_orangtua_wali_id` INTEGER NOT NULL,
    `nama` VARCHAR(40) NOT NULL,
    `batas_bawah` INTEGER NOT NULL,
    `batas_atas` INTEGER NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`penghasilan_orangtua_wali_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- peran
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `peran`;

CREATE TABLE `peran`
(
    `peran_id` INTEGER NOT NULL,
    `nama` VARCHAR(50),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`peran_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- peserta_didik
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `peserta_didik`;

CREATE TABLE `peserta_didik`
(
    `peserta_didik_id` CHAR(36) NOT NULL,
    `nama` VARCHAR(50),
    `jenis_kelamin` CHAR NOT NULL,
    `nisn` CHAR(10),
    `nik` CHAR(16),
    `tempat_lahir` VARCHAR(20),
    `tanggal_lahir` DATE NOT NULL,
    `agama_id` SMALLINT NOT NULL,
    `kewarganegaraan` CHAR(2) NOT NULL,
    `kebutuhan_khusus_id` INTEGER NOT NULL,
    `sekolah_id` CHAR(36) NOT NULL,
    `alamat_jalan` VARCHAR(80) NOT NULL,
    `rt` DECIMAL(2,0),
    `rw` DECIMAL(2,0),
    `nama_dusun` VARCHAR(40),
    `desa_kelurahan` VARCHAR(40) NOT NULL,
    `kode_wilayah` CHAR(8) NOT NULL,
    `kode_pos` CHAR(5),
    `jenis_tinggal_id` DECIMAL(2,0),
    `alat_transportasi_id` DECIMAL(2,0),
    `nomor_telepon_rumah` VARCHAR(20),
    `nomor_telepon_seluler` VARCHAR(20),
    `email` VARCHAR(50),
    `penerima_KPS` DECIMAL(1,0) NOT NULL,
    `no_KPS` VARCHAR(40),
    `status_data` INTEGER,
    `nama_ayah` VARCHAR(50),
    `tahun_lahir_ayah` DECIMAL(4,0),
    `jenjang_pendidikan_ayah` DECIMAL(2,0),
    `pekerjaan_id_ayah` INTEGER,
    `penghasilan_id_ayah` INTEGER,
    `kebutuhan_khusus_id_ayah` INTEGER NOT NULL,
    `nama_ibu_kandung` VARCHAR(50) NOT NULL,
    `tahun_lahir_ibu` DECIMAL(4,0),
    `jenjang_pendidikan_ibu` DECIMAL(2,0),
    `penghasilan_id_ibu` INTEGER,
    `pekerjaan_id_ibu` INTEGER,
    `kebutuhan_khusus_id_ibu` INTEGER NOT NULL,
    `nama_wali` VARCHAR(30),
    `tahun_lahir_wali` DECIMAL(4,0),
    `jenjang_pendidikan_wali` DECIMAL(2,0),
    `pekerjaan_id_wali` INTEGER,
    `penghasilan_id_wali` INTEGER,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`peserta_didik_id`),
    INDEX `agama_id` (`agama_id`),
    INDEX `alat_transportasi_id` (`alat_transportasi_id`),
    INDEX `jenis_tinggal_id` (`jenis_tinggal_id`),
    INDEX `jenjang_pendidikan_wali` (`jenjang_pendidikan_wali`),
    INDEX `jenjang_pendidikan_ibu` (`jenjang_pendidikan_ibu`),
    INDEX `jenjang_pendidikan_ayah` (`jenjang_pendidikan_ayah`),
    INDEX `kebutuhan_khusus_id_ayah` (`kebutuhan_khusus_id_ayah`),
    INDEX `kebutuhan_khusus_id_ibu` (`kebutuhan_khusus_id_ibu`),
    INDEX `kebutuhan_khusus_id` (`kebutuhan_khusus_id`),
    INDEX `kewarganegaraan` (`kewarganegaraan`),
    INDEX `kode_wilayah` (`kode_wilayah`),
    INDEX `pekerjaan_id_wali` (`pekerjaan_id_wali`),
    INDEX `pekerjaan_id_ayah` (`pekerjaan_id_ayah`),
    INDEX `pekerjaan_id_ibu` (`pekerjaan_id_ibu`),
    INDEX `penghasilan_id_ibu` (`penghasilan_id_ibu`),
    INDEX `penghasilan_id_ayah` (`penghasilan_id_ayah`),
    INDEX `penghasilan_id_wali` (`penghasilan_id_wali`),
    INDEX `sekolah_id` (`sekolah_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- peserta_didik_baru
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `peserta_didik_baru`;

CREATE TABLE `peserta_didik_baru`
(
    `pdb_id` CHAR(36) NOT NULL,
    `sekolah_id` CHAR(36) NOT NULL,
    `tahun_ajaran_id` DECIMAL(4,0) NOT NULL,
    `nama_pd` VARCHAR(50) NOT NULL,
    `jenis_kelamin` CHAR NOT NULL,
    `nisn` CHAR(10),
    `nik` CHAR(16),
    `tempat_lahir` VARCHAR(20),
    `tanggal_lahir` DATE NOT NULL,
    `nama_ibu_kandung` VARCHAR(50) NOT NULL,
    `jenis_pendaftaran_id` DECIMAL(1,0) NOT NULL,
    `sudah_diproses` DECIMAL(1,0) NOT NULL,
    `berhasil_diproses` DECIMAL(1,0) NOT NULL,
    `peserta_didik_id` CHAR(36),
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`pdb_id`),
    INDEX `jenis_pendaftaran_id` (`jenis_pendaftaran_id`),
    INDEX `sekolah_id` (`sekolah_id`),
    INDEX `tahun_ajaran_id` (`tahun_ajaran_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- peserta_didik_longitudinal
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `peserta_didik_longitudinal`;

CREATE TABLE `peserta_didik_longitudinal`
(
    `peserta_didik_id` CHAR(36) NOT NULL,
    `semester_id` CHAR(5) NOT NULL,
    `tinggi_badan` DECIMAL(3,0) NOT NULL,
    `berat_badan` DECIMAL(3,0) NOT NULL,
    `jarak_rumah_ke_sekolah` DECIMAL(1,0) NOT NULL,
    `jarak_rumah_ke_sekolah_km` DECIMAL(3,0),
    `waktu_tempuh_ke_sekolah` DECIMAL(1,0) NOT NULL,
    `menit_tempuh_ke_sekolah` DECIMAL(3,0),
    `jumlah_saudara_kandung` DECIMAL(2,0) NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`peserta_didik_id`,`semester_id`),
    INDEX `semester_id` (`semester_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- prasarana
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `prasarana`;

CREATE TABLE `prasarana`
(
    `prasarana_id` CHAR(36) NOT NULL,
    `jenis_prasarana_id` INTEGER NOT NULL,
    `sekolah_id` CHAR(36) NOT NULL,
    `kepemilikan_sarpras_id` DECIMAL(1,0) NOT NULL,
    `nama` VARCHAR(30) NOT NULL,
    `panjang` DOUBLE,
    `lebar` DOUBLE,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`prasarana_id`),
    INDEX `jenis_prasarana_id` (`jenis_prasarana_id`),
    INDEX `kepemilikan_sarpras_id` (`kepemilikan_sarpras_id`),
    INDEX `sekolah_id` (`sekolah_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- prasarana_longitudinal
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `prasarana_longitudinal`;

CREATE TABLE `prasarana_longitudinal`
(
    `prasarana_id` CHAR(36) NOT NULL,
    `semester_id` CHAR(5) NOT NULL,
    `rusak_penutup_atap` DECIMAL(6,2) NOT NULL,
    `rusak_rangka_atap` DECIMAL(6,2) NOT NULL,
    `rusak_lisplang_talang` DECIMAL(6,2) NOT NULL,
    `rusak_rangka_plafon` DECIMAL(6,2) NOT NULL,
    `rusak_penutup_listplafon` DECIMAL(6,2) NOT NULL,
    `rusak_cat_plafon` DECIMAL(6,2) NOT NULL,
    `rusak_kolom_ringbalok` DECIMAL(6,2) NOT NULL,
    `rusak_bata_dindingpengisi` DECIMAL(6,2) NOT NULL,
    `rusak_cat_dinding` DECIMAL(6,2) NOT NULL,
    `rusak_kusen` DECIMAL(6,2) NOT NULL,
    `rusak_daun_pintu` DECIMAL(6,2) NOT NULL,
    `rusak_daun_jendela` DECIMAL(6,2) NOT NULL,
    `rusak_struktur_bawah` DECIMAL(6,2) NOT NULL,
    `rusak_penutup_lantai` DECIMAL(6,2) NOT NULL,
    `rusak_pondasi` DECIMAL(6,2) NOT NULL,
    `rusak_sloof` DECIMAL(6,2) NOT NULL,
    `rusak_listrik` DECIMAL(6,2) NOT NULL,
    `rusak_airhujan_rabatan` DECIMAL(6,2) NOT NULL,
    `blob_id` CHAR(36),
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`prasarana_id`,`semester_id`),
    INDEX `semester_id` (`semester_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- prestasi
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `prestasi`;

CREATE TABLE `prestasi`
(
    `prestasi_id` CHAR(36) NOT NULL,
    `jenis_prestasi_id` INTEGER NOT NULL,
    `tingkat_prestasi_id` INTEGER NOT NULL,
    `peserta_didik_id` CHAR(36) NOT NULL,
    `nama` VARCHAR(30) NOT NULL,
    `tahun_prestasi` DECIMAL(4,0) NOT NULL,
    `penyelenggara` VARCHAR(80),
    `peringkat` INTEGER,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`prestasi_id`),
    INDEX `jenis_prestasi_id` (`jenis_prestasi_id`),
    INDEX `peserta_didik_id` (`peserta_didik_id`),
    INDEX `tingkat_prestasi_id` (`tingkat_prestasi_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- program_inklusi
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `program_inklusi`;

CREATE TABLE `program_inklusi`
(
    `id_pi` CHAR(36) NOT NULL,
    `sekolah_id` CHAR(36) NOT NULL,
    `kebutuhan_khusus_id` INTEGER NOT NULL,
    `sk_pi` VARCHAR(40) NOT NULL,
    `tmt_pi` DATE NOT NULL,
    `tst_pi` DATE,
    `ket_pi` VARCHAR(200),
    `Last_update` DATETIME NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`id_pi`),
    INDEX `kebutuhan_khusus_id` (`kebutuhan_khusus_id`),
    INDEX `sekolah_id` (`sekolah_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- ptk
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `ptk`;

CREATE TABLE `ptk`
(
    `ptk_id` CHAR(36) NOT NULL,
    `nama` VARCHAR(50) NOT NULL,
    `nip` VARCHAR(18),
    `jenis_kelamin` CHAR NOT NULL,
    `tempat_lahir` VARCHAR(20) NOT NULL,
    `tanggal_lahir` DATE NOT NULL,
    `nik` CHAR(16) NOT NULL,
    `niy_nigk` VARCHAR(30),
    `nuptk` CHAR(16),
    `status_kepegawaian_id` SMALLINT NOT NULL,
    `jenis_ptk_id` DECIMAL(2,0) NOT NULL,
    `pengawas_bidang_studi_id` INTEGER,
    `agama_id` SMALLINT NOT NULL,
    `kewarganegaraan` CHAR(2) NOT NULL,
    `alamat_jalan` VARCHAR(80) NOT NULL,
    `rt` DECIMAL(2,0),
    `rw` DECIMAL(2,0),
    `nama_dusun` VARCHAR(40),
    `desa_kelurahan` VARCHAR(40) NOT NULL,
    `kode_wilayah` CHAR(8) NOT NULL,
    `kode_pos` CHAR(5),
    `no_telepon_rumah` VARCHAR(20),
    `no_hp` VARCHAR(20),
    `email` VARCHAR(50),
    `entry_sekolah_id` CHAR(36) NOT NULL,
    `status_keaktifan_id` DECIMAL(2,0) NOT NULL,
    `sk_cpns` VARCHAR(40),
    `tgl_cpns` DATE,
    `sk_pengangkatan` VARCHAR(40),
    `tmt_pengangkatan` DATE,
    `lembaga_pengangkat_id` DECIMAL(2,0) NOT NULL,
    `pangkat_golongan_id` DECIMAL(2,0),
    `keahlian_laboratorium_id` SMALLINT,
    `sumber_gaji_id` DECIMAL(2,0) NOT NULL,
    `nama_ibu_kandung` VARCHAR(50) NOT NULL,
    `status_perkawinan` DECIMAL(1,0) NOT NULL,
    `nama_suami_istri` VARCHAR(50),
    `nip_suami_istri` CHAR(18),
    `pekerjaan_suami_istri` INTEGER NOT NULL,
    `tmt_pns` DATE,
    `sudah_lisensi_kepala_sekolah` DECIMAL(1,0) NOT NULL,
    `jumlah_sekolah_binaan` SMALLINT,
    `pernah_diklat_kepengawasan` DECIMAL(1,0) NOT NULL,
    `status_data` INTEGER,
    `mampu_handle_kk` INTEGER NOT NULL,
    `keahlian_braille` DECIMAL(1,0),
    `keahlian_bhs_isyarat` DECIMAL(1,0),
    `npwp` CHAR(15),
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`ptk_id`),
    INDEX `agama_id` (`agama_id`),
    INDEX `entry_sekolah_id` (`entry_sekolah_id`),
    INDEX `jenis_ptk_id` (`jenis_ptk_id`),
    INDEX `keahlian_laboratorium_id` (`keahlian_laboratorium_id`),
    INDEX `kewarganegaraan` (`kewarganegaraan`),
    INDEX `kode_wilayah` (`kode_wilayah`),
    INDEX `lembaga_pengangkat_id` (`lembaga_pengangkat_id`),
    INDEX `mampu_handle_kk` (`mampu_handle_kk`),
    INDEX `pangkat_golongan_id` (`pangkat_golongan_id`),
    INDEX `pekerjaan_suami_istri` (`pekerjaan_suami_istri`),
    INDEX `pengawas_bidang_studi_id` (`pengawas_bidang_studi_id`),
    INDEX `status_keaktifan_id` (`status_keaktifan_id`),
    INDEX `status_kepegawaian_id` (`status_kepegawaian_id`),
    INDEX `sumber_gaji_id` (`sumber_gaji_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- ptk_baru
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `ptk_baru`;

CREATE TABLE `ptk_baru`
(
    `ptk_baru_id` CHAR(36) NOT NULL,
    `sekolah_id` CHAR(36) NOT NULL,
    `tahun_ajaran_id` DECIMAL(4,0) NOT NULL,
    `nama_ptk` VARCHAR(50) NOT NULL,
    `jenis_kelamin` CHAR NOT NULL,
    `nuptk` CHAR(16),
    `nik` CHAR(16) NOT NULL,
    `tempat_lahir` VARCHAR(20) NOT NULL,
    `tanggal_lahir` DATE NOT NULL,
    `nama_ibu_kandung` VARCHAR(50) NOT NULL,
    `sudah_diproses` DECIMAL(1,0) NOT NULL,
    `berhasil_diproses` DECIMAL(1,0) NOT NULL,
    `ptk_id` CHAR(36),
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`ptk_baru_id`),
    INDEX `sekolah_id` (`sekolah_id`),
    INDEX `tahun_ajaran_id` (`tahun_ajaran_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- ptk_terdaftar
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `ptk_terdaftar`;

CREATE TABLE `ptk_terdaftar`
(
    `ptk_terdaftar_id` CHAR(36) NOT NULL,
    `ptk_id` CHAR(36) NOT NULL,
    `sekolah_id` CHAR(36) NOT NULL,
    `tahun_ajaran_id` DECIMAL(4,0) NOT NULL,
    `nomor_surat_tugas` VARCHAR(40) NOT NULL,
    `tanggal_surat_tugas` DATE NOT NULL,
    `tmt_tugas` DATE NOT NULL,
    `ptk_induk` DECIMAL(1,0) NOT NULL,
    `aktif_bulan_01` DECIMAL(1,0) NOT NULL,
    `aktif_bulan_02` DECIMAL(1,0) NOT NULL,
    `aktif_bulan_03` DECIMAL(1,0) NOT NULL,
    `aktif_bulan_04` DECIMAL(1,0) NOT NULL,
    `aktif_bulan_05` DECIMAL(1,0) NOT NULL,
    `aktif_bulan_06` DECIMAL(1,0) NOT NULL,
    `aktif_bulan_07` DECIMAL(1,0) NOT NULL,
    `aktif_bulan_08` DECIMAL(1,0) NOT NULL,
    `aktif_bulan_09` DECIMAL(1,0) NOT NULL,
    `aktif_bulan_10` DECIMAL(1,0) NOT NULL,
    `aktif_bulan_11` DECIMAL(1,0) NOT NULL,
    `aktif_bulan_12` DECIMAL(1,0) NOT NULL,
    `jenis_keluar_id` CHAR,
    `tgl_ptk_keluar` DATE,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`ptk_terdaftar_id`),
    INDEX `jenis_keluar_id` (`jenis_keluar_id`),
    INDEX `ptk_id` (`ptk_id`),
    INDEX `tahun_ajaran_id` (`tahun_ajaran_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- registrasi_peserta_didik
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `registrasi_peserta_didik`;

CREATE TABLE `registrasi_peserta_didik`
(
    `registrasi_id` CHAR(36) NOT NULL,
    `jurusan_sp_id` CHAR(36),
    `peserta_didik_id` CHAR(36) NOT NULL,
    `sekolah_id` CHAR(36) NOT NULL,
    `jenis_pendaftaran_id` DECIMAL(1,0) NOT NULL,
    `nipd` VARCHAR(18),
    `tanggal_masuk_sekolah` DATE NOT NULL,
    `jenis_keluar_id` CHAR,
    `tanggal_keluar` DATE,
    `keterangan` VARCHAR(128),
    `no_SKHUN` CHAR(20),
    `a_pernah_paud` DECIMAL(1,0) NOT NULL,
    `a_pernah_tk` DECIMAL(1,0) NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`registrasi_id`),
    INDEX `jenis_pendaftaran_id` (`jenis_pendaftaran_id`),
    INDEX `jenis_keluar_id` (`jenis_keluar_id`),
    INDEX `jurusan_sp_id` (`jurusan_sp_id`),
    INDEX `peserta_didik_id` (`peserta_didik_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- riwayat_gaji_berkala
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `riwayat_gaji_berkala`;

CREATE TABLE `riwayat_gaji_berkala`
(
    `riwayat_gaji_berkala_id` CHAR(36) NOT NULL,
    `ptk_id` CHAR(36) NOT NULL,
    `pangkat_golongan_id` DECIMAL(2,0) NOT NULL,
    `nomor_sk` VARCHAR(40) NOT NULL,
    `tanggal_sk` DATE NOT NULL,
    `tmt_kgb` DATE NOT NULL,
    `masa_kerja_tahun` DECIMAL(2,0) NOT NULL,
    `masa_kerja_bulan` DECIMAL(2,0) NOT NULL,
    `gaji_pokok` DECIMAL(9,0),
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`riwayat_gaji_berkala_id`),
    INDEX `pangkat_golongan_id` (`pangkat_golongan_id`),
    INDEX `ptk_id` (`ptk_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- rombongan_belajar
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `rombongan_belajar`;

CREATE TABLE `rombongan_belajar`
(
    `rombongan_belajar_id` CHAR(36) NOT NULL,
    `semester_id` CHAR(5) NOT NULL,
    `sekolah_id` CHAR(36) NOT NULL,
    `tingkat_pendidikan_id` DECIMAL(2,0) NOT NULL,
    `jurusan_sp_id` CHAR(36),
    `kurikulum_id` SMALLINT NOT NULL,
    `nama` VARCHAR(12) NOT NULL,
    `ptk_id` CHAR(36) NOT NULL,
    `prasarana_id` CHAR(36) NOT NULL,
    `moving_class` DECIMAL(1,0) NOT NULL,
    `jenis_rombel` DECIMAL(1,0) NOT NULL,
    `sks` DECIMAL(2,0) NOT NULL,
    `kebutuhan_khusus_id` INTEGER NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`rombongan_belajar_id`),
    INDEX `kebutuhan_khusus_id` (`kebutuhan_khusus_id`),
    INDEX `kurikulum_id` (`kurikulum_id`),
    INDEX `prasarana_id` (`prasarana_id`),
    INDEX `ptk_id` (`ptk_id`),
    INDEX `sekolah_id` (`sekolah_id`),
    INDEX `semester_id` (`semester_id`),
    INDEX `tingkat_pendidikan_id` (`tingkat_pendidikan_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- rwy_fungsional
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `rwy_fungsional`;

CREATE TABLE `rwy_fungsional`
(
    `riwayat_fungsional_id` CHAR(36) NOT NULL,
    `ptk_id` CHAR(36) NOT NULL,
    `jabatan_fungsional_id` DECIMAL(2,0) NOT NULL,
    `sk_jabfung` VARCHAR(40) NOT NULL,
    `tmt_jabatan` DATE NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`riwayat_fungsional_id`),
    INDEX `jabatan_fungsional_id` (`jabatan_fungsional_id`),
    INDEX `ptk_id` (`ptk_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- rwy_kepangkatan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `rwy_kepangkatan`;

CREATE TABLE `rwy_kepangkatan`
(
    `riwayat_kepangkatan_id` CHAR(36) NOT NULL,
    `ptk_id` CHAR(36) NOT NULL,
    `pangkat_golongan_id` DECIMAL(2,0) NOT NULL,
    `nomor_sk` VARCHAR(40) NOT NULL,
    `tanggal_sk` DATE NOT NULL,
    `tmt_pangkat` DATE NOT NULL,
    `masa_kerja_gol_tahun` DECIMAL(2,0) NOT NULL,
    `masa_kerja_gol_bulan` DECIMAL(2,0) NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`riwayat_kepangkatan_id`),
    INDEX `pangkat_golongan_id` (`pangkat_golongan_id`),
    INDEX `ptk_id` (`ptk_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- rwy_pend_formal
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `rwy_pend_formal`;

CREATE TABLE `rwy_pend_formal`
(
    `riwayat_pendidikan_formal_id` CHAR(36) NOT NULL,
    `ptk_id` CHAR(36) NOT NULL,
    `bidang_studi_id` INTEGER NOT NULL,
    `jenjang_pendidikan_id` DECIMAL(2,0) NOT NULL,
    `gelar_akademik_id` INTEGER,
    `satuan_pendidikan_formal` VARCHAR(80) NOT NULL,
    `fakultas` VARCHAR(30),
    `kependidikan` DECIMAL(1,0) NOT NULL,
    `tahun_masuk` DECIMAL(4,0) NOT NULL,
    `tahun_lulus` DECIMAL(4,0),
    `nim` VARCHAR(12) NOT NULL,
    `status_kuliah` DECIMAL(1,0) NOT NULL,
    `semester` DECIMAL(2,0),
    `ipk` DECIMAL(5,2) NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`riwayat_pendidikan_formal_id`),
    INDEX `bidang_studi_id` (`bidang_studi_id`),
    INDEX `gelar_akademik_id` (`gelar_akademik_id`),
    INDEX `jenjang_pendidikan_id` (`jenjang_pendidikan_id`),
    INDEX `ptk_id` (`ptk_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- rwy_sertifikasi
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `rwy_sertifikasi`;

CREATE TABLE `rwy_sertifikasi`
(
    `riwayat_sertifikasi_id` CHAR(36) NOT NULL,
    `ptk_id` CHAR(36) NOT NULL,
    `bidang_studi_id` INTEGER NOT NULL,
    `id_jenis_sertifikasi` DECIMAL(3,0) NOT NULL,
    `tahun_sertifikasi` DECIMAL(4,0) NOT NULL,
    `nomor_sertifikat` VARCHAR(40) NOT NULL,
    `nrg` VARCHAR(12),
    `nomor_peserta` VARCHAR(14),
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`riwayat_sertifikasi_id`),
    INDEX `bidang_studi_id` (`bidang_studi_id`),
    INDEX `id_jenis_sertifikasi` (`id_jenis_sertifikasi`),
    INDEX `ptk_id` (`ptk_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- rwy_struktural
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `rwy_struktural`;

CREATE TABLE `rwy_struktural`
(
    `riwayat_struktural_id` CHAR(36) NOT NULL,
    `ptk_id` CHAR(36) NOT NULL,
    `jabatan_ptk_id` DECIMAL(3,0) NOT NULL,
    `sk_struktural` VARCHAR(40) NOT NULL,
    `tmt_jabatan` DATE NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`riwayat_struktural_id`),
    INDEX `jabatan_ptk_id` (`jabatan_ptk_id`),
    INDEX `ptk_id` (`ptk_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- sanitasi
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sanitasi`;

CREATE TABLE `sanitasi`
(
    `sekolah_id` CHAR(36) NOT NULL,
    `semester_id` CHAR(5) NOT NULL,
    `sumber_air_id` DECIMAL(2,0) NOT NULL,
    `ketersediaan_air` DECIMAL(1,0) NOT NULL,
    `kecukupan_air` DECIMAL(1,0) NOT NULL,
    `minum_siswa` DECIMAL(1,0) NOT NULL,
    `memproses_air` DECIMAL(1,0) NOT NULL,
    `siswa_bawa_air` DECIMAL(1,0) NOT NULL,
    `toilet_siswa_laki` DECIMAL(2,0) NOT NULL,
    `toilet_siswa_perempuan` DECIMAL(2,0) NOT NULL,
    `toilet_siswa_kk` DECIMAL(2,0) NOT NULL,
    `toilet_siswa_kecil` DECIMAL(1,0) NOT NULL,
    `tempat_cuci_tangan` DECIMAL(2,0) NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`sekolah_id`,`semester_id`),
    INDEX `semester_id` (`semester_id`),
    INDEX `sumber_air_id` (`sumber_air_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- sarana
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sarana`;

CREATE TABLE `sarana`
(
    `sarana_id` CHAR(36) NOT NULL,
    `sekolah_id` CHAR(36) NOT NULL,
    `jenis_sarana_id` INTEGER NOT NULL,
    `prasarana_id` CHAR(36),
    `kepemilikan_sarpras_id` DECIMAL(1,0) NOT NULL,
    `nama` VARCHAR(50) NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`sarana_id`),
    INDEX `jenis_sarana_id` (`jenis_sarana_id`),
    INDEX `kepemilikan_sarpras_id` (`kepemilikan_sarpras_id`),
    INDEX `prasarana_id` (`prasarana_id`),
    INDEX `sekolah_id` (`sekolah_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- sarana_longitudinal
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sarana_longitudinal`;

CREATE TABLE `sarana_longitudinal`
(
    `sarana_id` CHAR(36) NOT NULL,
    `semester_id` CHAR(5) NOT NULL,
    `jumlah` INTEGER NOT NULL,
    `status_kelaikan` DECIMAL(1,0) NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`sarana_id`,`semester_id`),
    INDEX `semester_id` (`semester_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- sasaran_pengawasan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sasaran_pengawasan`;

CREATE TABLE `sasaran_pengawasan`
(
    `pengawas_terdaftar_id` CHAR(36) NOT NULL,
    `sekolah_id` CHAR(36) NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`pengawas_terdaftar_id`,`sekolah_id`),
    INDEX `sekolah_id` (`sekolah_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- sasaran_survey
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sasaran_survey`;

CREATE TABLE `sasaran_survey`
(
    `pengguna_id` CHAR(36) NOT NULL,
    `sekolah_id` CHAR(36) NOT NULL,
    `nomor_urut` INTEGER NOT NULL,
    `tanggal_rencana` DATE,
    `tanggal_pelaksanaan` DATE,
    `waktu_berangkat` DATETIME,
    `waktu_sampai` DATETIME,
    `waktu_mulai_survey` DATETIME,
    `waktu_selesai` DATETIME,
    `waktu_isi_form_cetak` INTEGER,
    `waktu_isi_form_elektronik` INTEGER,
    PRIMARY KEY (`pengguna_id`,`sekolah_id`),
    INDEX `sekolah_id` (`sekolah_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- sekolah
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sekolah`;

CREATE TABLE `sekolah`
(
    `sekolah_id` CHAR(36) NOT NULL,
    `nama` VARCHAR(80) NOT NULL,
    `nama_nomenklatur` VARCHAR(80),
    `nss` CHAR(12),
    `npsn` CHAR(8),
    `bentuk_pendidikan_id` SMALLINT NOT NULL,
    `alamat_jalan` VARCHAR(80) NOT NULL,
    `rt` DECIMAL(2,0),
    `rw` DECIMAL(2,0),
    `nama_dusun` VARCHAR(40),
    `desa_kelurahan` VARCHAR(40) NOT NULL,
    `kode_wilayah` CHAR(8) NOT NULL,
    `kode_pos` CHAR(5),
    `lintang` DECIMAL,
    `bujur` DECIMAL,
    `nomor_telepon` VARCHAR(20),
    `nomor_fax` VARCHAR(20),
    `email` VARCHAR(50),
    `website` VARCHAR(100),
    `kebutuhan_khusus_id` INTEGER NOT NULL,
    `status_sekolah` DECIMAL(1,0) NOT NULL,
    `sk_pendirian_sekolah` VARCHAR(40),
    `tanggal_sk_pendirian` DATE,
    `status_kepemilikan_id` DECIMAL(1,0) NOT NULL,
    `yayasan_id` CHAR(36),
    `sk_izin_operasional` VARCHAR(40),
    `tanggal_sk_izin_operasional` DATE,
    `no_rekening` VARCHAR(20),
    `nama_bank` VARCHAR(20),
    `cabang_kcp_unit` VARCHAR(40),
    `rekening_atas_nama` VARCHAR(50),
    `mbs` DECIMAL(1,0) NOT NULL,
    `luas_tanah_milik` DECIMAL(7,0) NOT NULL,
    `luas_tanah_bukan_milik` DECIMAL(7,0) NOT NULL,
    `kode_registrasi` BIGINT,
    `flag` CHAR,
    `pic_id` CHAR(36),
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`sekolah_id`),
    INDEX `bentuk_pendidikan_id` (`bentuk_pendidikan_id`),
    INDEX `kebutuhan_khusus_id` (`kebutuhan_khusus_id`),
    INDEX `kode_wilayah` (`kode_wilayah`),
    INDEX `status_kepemilikan_id` (`status_kepemilikan_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- sekolah_longitudinal
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sekolah_longitudinal`;

CREATE TABLE `sekolah_longitudinal`
(
    `sekolah_id` CHAR(36) NOT NULL,
    `semester_id` CHAR(5) NOT NULL,
    `daya_listrik` DECIMAL(6,0) NOT NULL,
    `wilayah_terpencil` DECIMAL(1,0) NOT NULL,
    `wilayah_perbatasan` DECIMAL(1,0) NOT NULL,
    `wilayah_transmigrasi` DECIMAL(1,0) NOT NULL,
    `wilayah_adat_terpencil` DECIMAL(1,0) NOT NULL,
    `wilayah_bencana_alam` DECIMAL(1,0) NOT NULL,
    `wilayah_bencana_sosial` DECIMAL(1,0) NOT NULL,
    `partisipasi_bos` DECIMAL(1,0) NOT NULL,
    `waktu_penyelenggaraan_id` DECIMAL(1,0) NOT NULL,
    `sumber_listrik_id` DECIMAL(2,0) NOT NULL,
    `sertifikasi_iso_id` SMALLINT NOT NULL,
    `akses_internet_id` SMALLINT,
    `akses_internet_2_id` SMALLINT NOT NULL,
    `blob_id` CHAR(36),
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`sekolah_id`,`semester_id`),
    INDEX `akses_internet_id` (`akses_internet_id`),
    INDEX `akses_internet_2_id` (`akses_internet_2_id`),
    INDEX `semester_id` (`semester_id`),
    INDEX `sertifikasi_iso_id` (`sertifikasi_iso_id`),
    INDEX `sumber_listrik_id` (`sumber_listrik_id`),
    INDEX `waktu_penyelenggaraan_id` (`waktu_penyelenggaraan_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- semester
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `semester`;

CREATE TABLE `semester`
(
    `semester_id` CHAR(5) NOT NULL,
    `tahun_ajaran_id` DECIMAL(4,0) NOT NULL,
    `nama` VARCHAR(20) NOT NULL,
    `semester` DECIMAL(1,0) NOT NULL,
    `periode_aktif` DECIMAL(1,0) NOT NULL,
    `tanggal_mulai` DATE NOT NULL,
    `tanggal_selesai` DATE NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`semester_id`),
    INDEX `tahun_ajaran_id` (`tahun_ajaran_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- sertifikasi_iso
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sertifikasi_iso`;

CREATE TABLE `sertifikasi_iso`
(
    `sertifikasi_iso_id` SMALLINT NOT NULL,
    `nama` VARCHAR(20) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`sertifikasi_iso_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- session_token
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `session_token`;

CREATE TABLE `session_token`
(
    `sekolah_id` CHAR(36) NOT NULL,
    `token` CHAR(10) NOT NULL,
    `date_created` DATETIME NOT NULL,
    `date_expired` DATETIME NOT NULL,
    PRIMARY KEY (`sekolah_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- status_anak
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `status_anak`;

CREATE TABLE `status_anak`
(
    `status_anak_id` DECIMAL(1,0) NOT NULL,
    `nama` VARCHAR(20) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`status_anak_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- status_keaktifan_pegawai
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `status_keaktifan_pegawai`;

CREATE TABLE `status_keaktifan_pegawai`
(
    `status_keaktifan_id` DECIMAL(2,0) NOT NULL,
    `nama` VARCHAR(30) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`status_keaktifan_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- status_kepegawaian
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `status_kepegawaian`;

CREATE TABLE `status_kepegawaian`
(
    `status_kepegawaian_id` SMALLINT NOT NULL,
    `nama` VARCHAR(30) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`status_kepegawaian_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- status_kepemilikan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `status_kepemilikan`;

CREATE TABLE `status_kepemilikan`
(
    `status_kepemilikan_id` DECIMAL(1,0) NOT NULL,
    `nama` VARCHAR(20) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`status_kepemilikan_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- status_kepemilikan_sarpras
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `status_kepemilikan_sarpras`;

CREATE TABLE `status_kepemilikan_sarpras`
(
    `kepemilikan_sarpras_id` DECIMAL(1,0) NOT NULL,
    `nama` VARCHAR(20) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`kepemilikan_sarpras_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- sumber_air
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sumber_air`;

CREATE TABLE `sumber_air`
(
    `sumber_air_id` DECIMAL(2,0) NOT NULL,
    `nama` VARCHAR(25) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`sumber_air_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- sumber_dana
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sumber_dana`;

CREATE TABLE `sumber_dana`
(
    `sumber_dana_id` DECIMAL(3,0) NOT NULL,
    `nama` VARCHAR(50) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`sumber_dana_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- sumber_gaji
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sumber_gaji`;

CREATE TABLE `sumber_gaji`
(
    `sumber_gaji_id` DECIMAL(2,0) NOT NULL,
    `nama` VARCHAR(40) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`sumber_gaji_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- sumber_listrik
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sumber_listrik`;

CREATE TABLE `sumber_listrik`
(
    `sumber_listrik_id` DECIMAL(2,0) NOT NULL,
    `nama` VARCHAR(50) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`sumber_listrik_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- sync_log
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sync_log`;

CREATE TABLE `sync_log`
(
    `sekolah_id` CHAR(36) NOT NULL,
    `begin_sync` DATETIME NOT NULL,
    `end_sync` DATETIME,
    `sync_media` CHAR NOT NULL,
    `is_success` DECIMAL(1,0) NOT NULL,
    `selisih_waktu_server` BIGINT NOT NULL,
    `alamat_ip` VARCHAR(50) NOT NULL,
    `pengguna_id` CHAR(36) NOT NULL,
    PRIMARY KEY (`sekolah_id`,`begin_sync`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- sync_session
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sync_session`;

CREATE TABLE `sync_session`
(
    `token` CHAR(36) NOT NULL,
    `sekolah_id` CHAR(36) NOT NULL,
    `pengguna_id` CHAR(36) NOT NULL,
    `create_time` DATETIME NOT NULL,
    `last_activity` DATETIME,
    PRIMARY KEY (`token`),
    INDEX `sekolah_id` (`sekolah_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- sysdiagrams
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `sysdiagrams`;

CREATE TABLE `sysdiagrams`
(
    `name` VARCHAR(255) NOT NULL,
    `principal_id` INTEGER NOT NULL,
    `diagram_id` INTEGER NOT NULL,
    `version` INTEGER,
    `definition` LONGBLOB,
    PRIMARY KEY (`diagram_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- table_sync
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `table_sync`;

CREATE TABLE `table_sync`
(
    `table_name` VARCHAR(30) NOT NULL,
    `sync_type` CHAR NOT NULL,
    `sync_seq` DECIMAL(4,0) NOT NULL,
    PRIMARY KEY (`table_name`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- table_sync_log
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `table_sync_log`;

CREATE TABLE `table_sync_log`
(
    `table_name` VARCHAR(30) NOT NULL,
    `sekolah_id` CHAR(36) NOT NULL,
    `begin_sync` DATETIME NOT NULL,
    `end_sync` DATETIME,
    `sync_media` CHAR NOT NULL,
    `is_success` DECIMAL(1,0) NOT NULL,
    `selisih_waktu_server` BIGINT NOT NULL,
    `n_create` INTEGER NOT NULL,
    `n_update` INTEGER NOT NULL,
    `n_hapus` INTEGER NOT NULL,
    `n_konflik` INTEGER NOT NULL,
    `alamat_ip` VARCHAR(50) NOT NULL,
    `pengguna_id` CHAR(36) NOT NULL,
    PRIMARY KEY (`table_name`,`sekolah_id`),
    INDEX `sekolah_id` (`sekolah_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- tahun_ajaran
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tahun_ajaran`;

CREATE TABLE `tahun_ajaran`
(
    `tahun_ajaran_id` DECIMAL(4,0) NOT NULL,
    `nama` VARCHAR(10) NOT NULL,
    `periode_aktif` DECIMAL(1,0) NOT NULL,
    `tanggal_mulai` DATE NOT NULL,
    `tanggal_selesai` DATE NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`tahun_ajaran_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- template_rapor
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `template_rapor`;

CREATE TABLE `template_rapor`
(
    `template_id` CHAR(36) NOT NULL,
    `mata_pelajaran_id` INTEGER NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`template_id`,`mata_pelajaran_id`),
    INDEX `mata_pelajaran_id` (`mata_pelajaran_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- template_un
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `template_un`;

CREATE TABLE `template_un`
(
    `template_id` CHAR(36) NOT NULL,
    `jenjang_pendidikan_id` DECIMAL(2,0) NOT NULL,
    `tahun_ajaran_id` DECIMAL(4,0) NOT NULL,
    `jurusan_id` VARCHAR(25),
    `template_ket` VARCHAR(250),
    `mp1_id` INTEGER,
    `mp2_id` INTEGER,
    `mp3_id` INTEGER,
    `mp4_id` INTEGER,
    `mp5_id` INTEGER,
    `mp6_id` INTEGER,
    `mp7_id` INTEGER,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`template_id`),
    INDEX `jenjang_pendidikan_id` (`jenjang_pendidikan_id`),
    INDEX `jurusan_id` (`jurusan_id`),
    INDEX `mp1_id` (`mp1_id`),
    INDEX `mp2_id` (`mp2_id`),
    INDEX `mp3_id` (`mp3_id`),
    INDEX `mp4_id` (`mp4_id`),
    INDEX `mp5_id` (`mp5_id`),
    INDEX `mp6_id` (`mp6_id`),
    INDEX `mp7_id` (`mp7_id`),
    INDEX `tahun_ajaran_id` (`tahun_ajaran_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- tetangga_kabkota
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tetangga_kabkota`;

CREATE TABLE `tetangga_kabkota`
(
    `kode_wilayah` CHAR(8) NOT NULL,
    `mst_kode_wilayah` CHAR(8) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`kode_wilayah`,`mst_kode_wilayah`),
    INDEX `mst_kode_wilayah` (`mst_kode_wilayah`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- tingkat_pendidikan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tingkat_pendidikan`;

CREATE TABLE `tingkat_pendidikan`
(
    `tingkat_pendidikan_id` DECIMAL(2,0) NOT NULL,
    `jenjang_pendidikan_id` DECIMAL(2,0) NOT NULL,
    `kode` VARCHAR(5) NOT NULL,
    `nama` VARCHAR(20) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`tingkat_pendidikan_id`),
    INDEX `jenjang_pendidikan_id` (`jenjang_pendidikan_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- tingkat_penghargaan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tingkat_penghargaan`;

CREATE TABLE `tingkat_penghargaan`
(
    `tingkat_penghargaan_id` INTEGER NOT NULL,
    `nama` VARCHAR(50) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`tingkat_penghargaan_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- tingkat_prestasi
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tingkat_prestasi`;

CREATE TABLE `tingkat_prestasi`
(
    `tingkat_prestasi_id` INTEGER NOT NULL,
    `nama` VARCHAR(50) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`tingkat_prestasi_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- tugas_tambahan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tugas_tambahan`;

CREATE TABLE `tugas_tambahan`
(
    `ptk_tugas_tambahan_id` CHAR(36) NOT NULL,
    `ptk_id` CHAR(36) NOT NULL,
    `jabatan_ptk_id` DECIMAL(3,0) NOT NULL,
    `sekolah_id` CHAR(36) NOT NULL,
    `jumlah_jam` DECIMAL(2,0) NOT NULL,
    `nomor_sk` VARCHAR(40) NOT NULL,
    `tmt_tambahan` DATE NOT NULL,
    `tst_tambahan` DATE,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`ptk_tugas_tambahan_id`),
    INDEX `jabatan_ptk_id` (`jabatan_ptk_id`),
    INDEX `ptk_id` (`ptk_id`),
    INDEX `sekolah_id` (`sekolah_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- tunjangan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `tunjangan`;

CREATE TABLE `tunjangan`
(
    `tunjangan_id` CHAR(36) NOT NULL,
    `ptk_id` CHAR(36) NOT NULL,
    `jenis_tunjangan_id` INTEGER,
    `nama` VARCHAR(50) NOT NULL,
    `instansi` VARCHAR(80),
    `sumber_dana` VARCHAR(30),
    `dari_tahun` DECIMAL(4,0) NOT NULL,
    `sampai_tahun` DECIMAL(4,0),
    `nominal` DECIMAL(16,2) NOT NULL,
    `status` INTEGER,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`tunjangan_id`),
    INDEX `jenis_tunjangan_id` (`jenis_tunjangan_id`),
    INDEX `ptk_id` (`ptk_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- unit_usaha
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `unit_usaha`;

CREATE TABLE `unit_usaha`
(
    `unit_usaha_id` CHAR(36) NOT NULL,
    `kelompok_usaha_id` CHAR(8) NOT NULL,
    `sekolah_id` CHAR(36) NOT NULL,
    `nama_unit_usaha` VARCHAR(80) NOT NULL,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`unit_usaha_id`),
    INDEX `kelompok_usaha_id` (`kelompok_usaha_id`),
    INDEX `sekolah_id` (`sekolah_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- unit_usaha_kerjasama
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `unit_usaha_kerjasama`;

CREATE TABLE `unit_usaha_kerjasama`
(
    `mou_id` CHAR(36) NOT NULL,
    `unit_usaha_id` CHAR(36) NOT NULL,
    `sumber_dana_id` DECIMAL(3,0) NOT NULL,
    `produk_barang_dihasilkan` VARCHAR(200),
    `jasa_produksi_dihasilkan` VARCHAR(200),
    `omzet_barang_perbulan` DECIMAL(18,0),
    `omzet_jasa_perbulan` DECIMAL(18,0),
    `prestasi_penghargaan` VARCHAR(200),
    `pangsa_pasar_produk` VARCHAR(200),
    `pangsa_pasar_jasa` VARCHAR(200),
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`mou_id`,`unit_usaha_id`),
    INDEX `sumber_dana_id` (`sumber_dana_id`),
    INDEX `unit_usaha_id` (`unit_usaha_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- versi_db
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `versi_db`;

CREATE TABLE `versi_db`
(
    `versi_id` DECIMAL(1,0) NOT NULL,
    `versi` VARCHAR(20) NOT NULL,
    `tanggal_update` DATETIME NOT NULL,
    PRIMARY KEY (`versi_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_anak
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_anak`;

CREATE TABLE `vld_anak`
(
    `anak_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`anak_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_bea_pd
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_bea_pd`;

CREATE TABLE `vld_bea_pd`
(
    `beasiswa_peserta_didik_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`beasiswa_peserta_didik_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_bea_ptk
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_bea_ptk`;

CREATE TABLE `vld_bea_ptk`
(
    `beasiswa_ptk_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`beasiswa_ptk_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_buku_ptk
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_buku_ptk`;

CREATE TABLE `vld_buku_ptk`
(
    `buku_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`buku_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_demografi
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_demografi`;

CREATE TABLE `vld_demografi`
(
    `demografi_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`demografi_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_inpassing
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_inpassing`;

CREATE TABLE `vld_inpassing`
(
    `inpassing_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`inpassing_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_jurusan_sp
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_jurusan_sp`;

CREATE TABLE `vld_jurusan_sp`
(
    `jurusan_sp_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`jurusan_sp_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_karya_tulis
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_karya_tulis`;

CREATE TABLE `vld_karya_tulis`
(
    `karya_tulis_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`karya_tulis_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_kesejahteraan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_kesejahteraan`;

CREATE TABLE `vld_kesejahteraan`
(
    `kesejahteraan_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`kesejahteraan_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_mou
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_mou`;

CREATE TABLE `vld_mou`
(
    `mou_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`mou_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_nilai_rapor
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_nilai_rapor`;

CREATE TABLE `vld_nilai_rapor`
(
    `nilai_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `app_username` VARCHAR(50),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`nilai_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_nilai_test
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_nilai_test`;

CREATE TABLE `vld_nilai_test`
(
    `nilai_test_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`nilai_test_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_nonsekolah
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_nonsekolah`;

CREATE TABLE `vld_nonsekolah`
(
    `lembaga_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`lembaga_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_pd_long
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_pd_long`;

CREATE TABLE `vld_pd_long`
(
    `peserta_didik_id` CHAR(36) NOT NULL,
    `semester_id` CHAR(5) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`peserta_didik_id`,`semester_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_pembelajaran
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_pembelajaran`;

CREATE TABLE `vld_pembelajaran`
(
    `pembelajaran_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`pembelajaran_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_penghargaan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_penghargaan`;

CREATE TABLE `vld_penghargaan`
(
    `penghargaan_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`penghargaan_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_peserta_didik
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_peserta_didik`;

CREATE TABLE `vld_peserta_didik`
(
    `peserta_didik_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`peserta_didik_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_prasarana
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_prasarana`;

CREATE TABLE `vld_prasarana`
(
    `prasarana_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`prasarana_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_prestasi
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_prestasi`;

CREATE TABLE `vld_prestasi`
(
    `prestasi_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`prestasi_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_ptk
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_ptk`;

CREATE TABLE `vld_ptk`
(
    `ptk_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`ptk_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_rombel
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_rombel`;

CREATE TABLE `vld_rombel`
(
    `rombongan_belajar_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`rombongan_belajar_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_rwy_fungsional
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_rwy_fungsional`;

CREATE TABLE `vld_rwy_fungsional`
(
    `riwayat_fungsional_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`riwayat_fungsional_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_rwy_kepangkatan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_rwy_kepangkatan`;

CREATE TABLE `vld_rwy_kepangkatan`
(
    `riwayat_kepangkatan_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`riwayat_kepangkatan_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_rwy_pend_formal
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_rwy_pend_formal`;

CREATE TABLE `vld_rwy_pend_formal`
(
    `riwayat_pendidikan_formal_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`riwayat_pendidikan_formal_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_rwy_sertifikasi
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_rwy_sertifikasi`;

CREATE TABLE `vld_rwy_sertifikasi`
(
    `riwayat_sertifikasi_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`riwayat_sertifikasi_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_rwy_struktural
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_rwy_struktural`;

CREATE TABLE `vld_rwy_struktural`
(
    `riwayat_struktural_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`riwayat_struktural_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_sarana
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_sarana`;

CREATE TABLE `vld_sarana`
(
    `sarana_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`sarana_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_sekolah
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_sekolah`;

CREATE TABLE `vld_sekolah`
(
    `sekolah_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`sekolah_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_tugas_tambahan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_tugas_tambahan`;

CREATE TABLE `vld_tugas_tambahan`
(
    `ptk_tugas_tambahan_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`ptk_tugas_tambahan_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_tunjangan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_tunjangan`;

CREATE TABLE `vld_tunjangan`
(
    `tunjangan_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`tunjangan_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_un
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_un`;

CREATE TABLE `vld_un`
(
    `un_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `app_username` VARCHAR(50),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`un_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- vld_yayasan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `vld_yayasan`;

CREATE TABLE `vld_yayasan`
(
    `yayasan_id` CHAR(36) NOT NULL,
    `logid` CHAR(36) NOT NULL,
    `idtype` INTEGER NOT NULL,
    `status_validasi` DECIMAL(2,0),
    `status_verifikasi` DECIMAL(2,0),
    `field_error` VARCHAR(30),
    `error_message` VARCHAR(150),
    `keterangan` VARCHAR(255),
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    `app_username` VARCHAR(50),
    PRIMARY KEY (`yayasan_id`,`logid`),
    INDEX `idtype` (`idtype`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- w_pending_job
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `w_pending_job`;

CREATE TABLE `w_pending_job`
(
    `kode_wilayah` CHAR(8) NOT NULL,
    `sekolah_id` CHAR(36) NOT NULL,
    `begin_sync` DATETIME NOT NULL,
    PRIMARY KEY (`kode_wilayah`,`sekolah_id`,`begin_sync`),
    INDEX `sekolah_id` (`sekolah_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- waktu_penyelenggaraan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `waktu_penyelenggaraan`;

CREATE TABLE `waktu_penyelenggaraan`
(
    `waktu_penyelenggaraan_id` DECIMAL(1,0) NOT NULL,
    `nama` VARCHAR(20) NOT NULL,
    `create_date` DATETIME NOT NULL,
    `last_update` DATETIME NOT NULL,
    `expired_date` DATETIME,
    `last_sync` DATETIME NOT NULL,
    PRIMARY KEY (`waktu_penyelenggaraan_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- wdst_sync_log
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `wdst_sync_log`;

CREATE TABLE `wdst_sync_log`
(
    `kode_wilayah` CHAR(8) NOT NULL,
    `sekolah_id` CHAR(36) NOT NULL,
    `begin_sync` DATETIME NOT NULL,
    `end_sync` DATETIME,
    `sync_media` CHAR NOT NULL,
    `is_success` DECIMAL(1,0) NOT NULL,
    `selisih_waktu_server` BIGINT NOT NULL,
    `alamat_ip` VARCHAR(50) NOT NULL,
    `pengguna_id` CHAR(36) NOT NULL,
    PRIMARY KEY (`kode_wilayah`,`sekolah_id`,`begin_sync`),
    INDEX `sekolah_id` (`sekolah_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- wilayah_pdsp_dikdas_kec
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `wilayah_pdsp_dikdas_kec`;

CREATE TABLE `wilayah_pdsp_dikdas_kec`
(
    `kec_id` INTEGER NOT NULL,
    `kode_wilayah` CHAR(6),
    `kec_` VARCHAR(80),
    `kab_` VARCHAR(80),
    `prop_` VARCHAR(80),
    PRIMARY KEY (`kec_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- wsrc_sync_log
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `wsrc_sync_log`;

CREATE TABLE `wsrc_sync_log`
(
    `kode_wilayah` CHAR(8) NOT NULL,
    `sekolah_id` CHAR(36) NOT NULL,
    `begin_sync` DATETIME NOT NULL,
    `end_sync` DATETIME,
    `sync_media` CHAR NOT NULL,
    `is_success` DECIMAL(1,0) NOT NULL,
    `selisih_waktu_server` BIGINT NOT NULL,
    `alamat_ip` VARCHAR(50) NOT NULL,
    `pengguna_id` CHAR(36) NOT NULL,
    PRIMARY KEY (`kode_wilayah`,`sekolah_id`,`begin_sync`),
    INDEX `sekolah_id` (`sekolah_id`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- wsync_session
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `wsync_session`;

CREATE TABLE `wsync_session`
(
    `token` CHAR(36) NOT NULL,
    `kode_wilayah` CHAR(8) NOT NULL,
    `pengguna_id` CHAR(36) NOT NULL,
    `create_time` DATETIME NOT NULL,
    `last_activity` DATETIME,
    PRIMARY KEY (`token`),
    INDEX `kode_wilayah` (`kode_wilayah`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- wt_dst_sync_log
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `wt_dst_sync_log`;

CREATE TABLE `wt_dst_sync_log`
(
    `kode_wilayah` CHAR(8) NOT NULL,
    `sekolah_id` CHAR(36) NOT NULL,
    `table_name` VARCHAR(30) NOT NULL,
    `begin_sync` DATETIME NOT NULL,
    `end_sync` DATETIME,
    `sync_media` CHAR NOT NULL,
    `is_success` DECIMAL(1,0) NOT NULL,
    `n_create` INTEGER NOT NULL,
    `n_update` INTEGER NOT NULL,
    `n_hapus` INTEGER NOT NULL,
    `n_konflik` INTEGER NOT NULL,
    `selisih_waktu_server` BIGINT NOT NULL,
    `alamat_ip` VARCHAR(50) NOT NULL,
    `pengguna_id` CHAR(36) NOT NULL,
    PRIMARY KEY (`kode_wilayah`,`sekolah_id`,`table_name`),
    INDEX `sekolah_id` (`sekolah_id`),
    INDEX `table_name` (`table_name`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- wt_src_sync_log
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `wt_src_sync_log`;

CREATE TABLE `wt_src_sync_log`
(
    `kode_wilayah` CHAR(8) NOT NULL,
    `sekolah_id` CHAR(36) NOT NULL,
    `table_name` VARCHAR(30) NOT NULL,
    `begin_sync` DATETIME NOT NULL,
    `end_sync` DATETIME,
    `sync_media` CHAR NOT NULL,
    `is_success` DECIMAL(1,0) NOT NULL,
    `n_create` INTEGER NOT NULL,
    `n_update` INTEGER NOT NULL,
    `n_hapus` INTEGER NOT NULL,
    `n_konflik` INTEGER NOT NULL,
    `selisih_waktu_server` BIGINT NOT NULL,
    `alamat_ip` VARCHAR(50) NOT NULL,
    `pengguna_id` CHAR(36) NOT NULL,
    PRIMARY KEY (`kode_wilayah`,`sekolah_id`,`table_name`),
    INDEX `sekolah_id` (`sekolah_id`),
    INDEX `table_name` (`table_name`)
) ENGINE=MyISAM;

-- ---------------------------------------------------------------------
-- yayasan
-- ---------------------------------------------------------------------

DROP TABLE IF EXISTS `yayasan`;

CREATE TABLE `yayasan`
(
    `yayasan_id` CHAR(36) NOT NULL,
    `nama` VARCHAR(80) NOT NULL,
    `alamat_jalan` VARCHAR(80) NOT NULL,
    `rt` DECIMAL(2,0),
    `rw` DECIMAL(2,0),
    `nama_dusun` VARCHAR(40),
    `desa_kelurahan` VARCHAR(40) NOT NULL,
    `kode_wilayah` CHAR(8) NOT NULL,
    `kode_pos` CHAR(5),
    `lintang` DECIMAL,
    `bujur` DECIMAL,
    `nomor_telepon` VARCHAR(20),
    `nomor_fax` VARCHAR(20),
    `email` VARCHAR(50),
    `website` VARCHAR(100),
    `nama_pimpinan_yayasan` VARCHAR(50) NOT NULL,
    `no_pendirian_yayasan` VARCHAR(40),
    `tanggal_pendirian_yayasan` DATE,
    `nomor_pengesahan_pn_ln` VARCHAR(30),
    `nomor_sk_bn` VARCHAR(255),
    `tanggal_sk_bn` DATE,
    `Last_update` DATETIME NOT NULL,
    `Soft_delete` DECIMAL(1,0) NOT NULL,
    `last_sync` DATETIME NOT NULL,
    `Updater_ID` CHAR(36) NOT NULL,
    PRIMARY KEY (`yayasan_id`),
    INDEX `kode_wilayah` (`kode_wilayah`)
) ENGINE=MyISAM;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
